/**
 * jQuery G2i CEP plugin v1.0
 * Tiago de Farias Silva
 */

(function ($) {
    /**
     * Return only Numbers with max-length = 8
     *
     * @param number|string str Any number or string
     * @return string String with only numbers
     */
    function cepNumbers(str) {
        return str.toString().replace(/\D/g, "").substr(0, 8);
    }

    /**
     * Returns formatted CEP
     *
     * @param number|string CEP Any number or string
     * @return string Formatted CEP string
     */
    function maskedCEP(cep) {
        var formattedCEP = "";
        var cn = cepNumbers(cep);

        if (cn.length > 5) {
            formattedCEP = cn.substr(0, 5) + "-" + cn.substr(5, 3);
        } else {
            formattedCEP = cn;
        }
        return formattedCEP;
    }

    /**
     * AutoFill inputs when a CEP is fetched
     */
    function autoFill(responseCEP, attr) {
        $("[" + attr + "]").each(function () {
            var self = $(this);
            var field = self.attr(attr);

            if (responseCEP[field]) {
                self.val(responseCEP[field]).trigger("change");
            }
        });
    }

    /**
     * PLugin instance
     */
    $.fn.cep = function (options) {
        /**
         * Default Settings
         */
        var settings = {
            autofill: true,
            autofill_attr: "data-cep",
            ajax: {
                url:  site_path+"/Cep/busca",
                requestParse: function (request) { return request; },
                //responseParse: function (response) { return $.parseJSON(response); }
            },
            init: LoadGif,
            done: CloseGif
        };

        if (typeof options === "object") {
            // Extend Options
            settings = $.extend(settings, options);
        } else if (typeof options === "function") {
            // Only "done" Callback
            settings.done = options;
        }

        this.each(function () {
            var cepElement = $(this);
            // Track any changes
            cepElement.on("keyup", function () {
                // var cep = Only CEP Numbers
                var cepNumber = cepNumbers(cepElement.val());

                // Update field value with formatted CEP
                cepElement.val(maskedCEP(cepNumber));

                // When CEP is fully typed
                // Send request and retrieve data
                if (cepNumber.length === 8) {
                    cepElement.attr("disabled", true);

                    settings.init(cepElement);

                    var data = { cep: cepNumber };
                    data = settings.ajax.requestParse(data);

                    $.get(settings.ajax.url, data, function (responseCEP) {
                        var response = settings.ajax.responseParse(responseCEP);
                        // Autofill
                        if (settings.autofill) {
                            autoFill(response, settings.autofill_attr);
                        }

                        // Execute Callback
                        settings.done(cepElement, responseCEP);
                    }).fail(function() {
                        toastr.error(msgErrorAjax);
                        CloseGif();
                      })
                    .always(function () {
                        cepElement.attr("disabled", false);
                    });
                }
            });
        });

        return this;
    };

    $(document).ready(function () {
        $("[role=\"cep\"]").each(function () {
            var self = $(this);
            var settings = {
                autofill: true,
                autofill_attr: self.attr("cep-att") != null ? self.attr("cep-att") : "data-cep",
                ajax: {
                    url: self.attr("cep-ajax-url") != null ? self.attr("cep-ajax-url") :  site_path+"/Cep/busca",
                    requestParse: self.attr("cep-ajax-requestParse") != null ? function (request) { return window[self.attr("cep-ajax-requestParse")](request); } : function (request) { return request; },
                    responseParse: self.attr("cep-ajax-responseParse") != null ? function (response) { return window[self.attr("cep-ajax-responseParse")](response); } : function (response) { return $.parseJSON(response); },
                }
            };

            self.cep(settings);
        });


    });
})(jQuery);

function getFill(element){
    $(element).find('[role="cep"]').each(function () {
        $("[role=\"cep\"]").each(function () {
            var self = $(this);
            var settings = {
                autofill: true,
                autofill_attr: self.attr("cep-att") != null ? self.attr("cep-att") : "data-cep",
                ajax: {
                    url: self.attr("cep-ajax-url") != null ? self.attr("cep-ajax-url") : site_path+"/Cep/busca",
                    requestParse: self.attr("cep-ajax-requestParse") != null ? function (request) { return window[self.attr("cep-ajax-requestParse")](request); } : function (request) { return request; },
                    responseParse: self.attr("cep-ajax-responseParse") != null ? function (response) { return window[self.attr("cep-ajax-responseParse")](response); } : function (response) { return $.parseJSON(response); },
                }
            };

            self.cep(settings);
        });
    });
}

function LoadGif(){
    var div = document.createElement("div");
    div.setAttribute('id','dialog-cep');
    var img = document.createElement("img");
    img.setAttribute("src","/prontuario/webroot/img/WindowsPhoneProgressbar.gif");
    div.appendChild(img);
    document.body.appendChild(div);
    $("#dialog-cep").css({
        "position": "fixed",
        "top":"0",
        "right":"0",
        "bottom":"0",
        "left":"0",
        "z-index":"2210",
        "background-color": "#000000",
        "opacity":"0.8",
        "filter": "alpha(opacity=80)"
    });

    $("#dialog-cep img").css({
        "position": "fixed",
        "top":"40%",
        "left":"30%"
    });
}
function CloseGif(){
    $("#dialog-cep").remove();
    $(".numero").focus();
}