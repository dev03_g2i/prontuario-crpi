
var validaPrecoProcedimento = function (form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if(txt.res == 0){
                toastr.success(txt.msg);
                Navegar('', 'back');
            }else {
                toastr.error(txt.msg);
            }
        }
    }).submit();
};

const procedimentosInConvenio = (convenio) => {
    let form = $('#frm-orcamento-proc');
    $.ajax({
        type: 'POST',
        data: { convenio },
        url: site_path + '/PrecoProcedimentos/procedimentos-in-convenio/',
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif();
        },
        success: function (txt) {
            form.find('#procedimentos').empty();
            $.each(txt, function (key, value) {
                form.find('#procedimentos').append(new Option(value, key));
            });
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            CloseGif();
            toastr.error(msgErrorAjax);
        }
    });
};

var PrecoProcedimentos = function (element) {

    $('#frm-orcamento-proc').find('#convenio').change(function () {
        if(!empty($(this).val()))
            procedimentosInConvenio($(this).val());
    });

};

const reportVlprocedimento = function (button, user_logado) {
    let url = site_path + '/' + button.attr('controller') + '/' + button.attr('action') ;
    //let url2 = site_path + '/' + button.attr('controller') + '/' + button.attr('action');
    let dados = $('#frm-orcamento-proc').serializeArray();
    let form = '';console.log(url);
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    let formulario = '<form class="temp-form" action="' + url + '" method="POST" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    $('#btn-fill').click();
    
    $('#nome-orcamento').val('');
    $('#obs-orcamento #observacoes').val('');
    
    $('#nome-orcamento').hide();
    $('#obs-orcamento').hide();
    $('#relatorio-orcamento').hide();
    
    $("#btn-refresh").click();

    click = null;
};

var showOrcamento = function () {
    $('#nome-orcamento').show();
    $('#obs-orcamento').show();
    $('#relatorio-orcamento').show();
};