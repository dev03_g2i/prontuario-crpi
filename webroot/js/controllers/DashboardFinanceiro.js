//Constantes usadas pela controller
const movesForCompanyUrl = '/dashboardFinanceiro/movesForCompany',
    companiesForGroup = '/dashboardFinanceiro/getCompaniesForGroup/',
    index = '/dashboardFinanceiro/index/',
    movesForModal = '/dashboardFinanceiro/getMovesForModal',
    billsForModal = '/dashboardFinanceiro/getBillsForModal',
    dashboardGraphic = site_path + '/dashboardFinanceiro/getGraphicData.json';

//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
        //Função para mudar a classe
        .on('click', '#changeTable', function () {
            $('#arrowDashboardSection1').toggleClass('fa-caret-down fa-caret-right');
            $("#divTable").toggleClass('d-block d-none');
        })
        //Função para pegar as empresas por grupo.
        .on('change', '#grupos', function () {
            let value = $('#grupos').val();
            getGroups(value);
        })
        // Inicio do modal da dashboard. 
        .on('click', '#line', function (event) {
            let dia = $(this).closest('tr').children('#diaModal').attr('value');
            let movimento = $('#date-for-cockpit').val();
            dia = dia + '/' + movimento;
            if (!$('#modalDashboard').length) {
                let table = ajaxForModal(dia, 'movimento');
            } else {
                $('div').remove('.modal-background, #modalDashboard');
            }
        })
        .on('click', '#receber, #pagar, #receber-convenio', function (event) {
            let dia = $(this).prevAll('#billDay').attr('value');
            let value = $(this).attr('id');
            let movimento = $('#date-for-cockpit').val();
            dia = dia + '/' + movimento;
            if (!$('#modalDashboard').length) {
                let table = ajaxForModal(dia, value);
            } else {
                $('div').remove('.modal-background, #modalDashboard');
            }
        })
        .on('click', '.modal-close', function (e) {
            $('div').remove('.modal-background, #modalDashboard');
            e.preventDefault();
        })
        // Fim do modal da dashboard
        .on('click', '#cockpit-search', function () {
            let date = '01/' + $('#date-for-cockpit').val();
            let text = $('#empresas option:selected').val() ? $('#empresas option:selected').text() : 'todas';
            let idCompany = $('#empresas option:selected').val() != 'todas' ? $('#empresas option:selected').val() : null;
            getIndex(date, text, idCompany);
        });

    loadDashboardGraphic(dashboardGraphic, null);
    //Função para pegar os dados da tabela por empresa
    // .on('change', '#empresas', function () {
    //     let value = $('#empresas').val();
    //     let text = $('#empresas option:selected').text();
    //     if (value === '') return;
    //     let url = getUrlPath(value, text);
    //     if (url !== null) loadDashboardTable(url, text);
    // })
    $(".datepicker-month").datepicker({
        format: "mm/yyyy",
        viewMode: "months",
        minViewMode: "months"
    });
});

function getGroups(value){
    if (value != '') {
        LoadGif();
        $.ajax({
            url: site_path + companiesForGroup + value + '.json',
            type: "GET",
            dataType: "json",
            success: function (data) {
                let mensagemText = '';
                $.each(data.companies, function (key, val) {
                    if (key === "1") {
                        mensagemText += "<option selected value=''>Selecione</option>";
                        mensagemText += "<option value='todas'>Todas</option>";
                    }
                    mensagemText += "<option value='" + key + "'>" + val + "</option>";
                });
                $('#empresas').prop('disabled', false);
                $('#empresas option').remove();
                $('#empresas').append(mensagemText);
                CloseGif();
            },
            error: function (xhr, status) {
                CloseGif();
                alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
            }
        });
    }
}

function ajaxForModal(dia, value) {
    let title = '';
    if (value === 'movimento') {
        title = 'Movimentos'
    } else {
        if (value === 'receber') title = 'Contas à receber - caixa';
        else if (value === 'pagar') title = 'Contas à pagar';
        else title = 'Contas à receber - convênios';
    }
    LoadGif();
    $.ajax({
        url: value === 'movimento' ? site_path + movesForModal + '.json' : site_path + billsForModal + '.json',
        type: "GET",
        data: { dia: dia, value: value ? value : undefined },
        success: function (data) {
            if (data.result.length === 0) {
                CloseGif();
                return alert("Ops, esta data não possuí dados!");
            }
            let tableHeader = '';
            let tableBody = '';
            $.each(data.result, function (objKey, obj) {
                tableHeader += '<tr>';
                tableBody += '<tr>';
                $.each(obj, function (attr, val) {
                    if (objKey === 0) tableHeader += '<th class="text-center">' + attr + '</th>';
                    tableBody += '<td align="center">' + val + '</td>';
                });
                tableHeader += '</tr>';
                tableBody += '</tr>';
            });
            let table = '<div id="modalDashboard" class="clearfix"><div class="table-modal-responsive" style="width: 80%; max-height: 80%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">';
            table += '<div class="col-sm-12 text-center" style="border-bottom: thin solid lightgray"><strong>' + title + '</strong></div><table class="table table-hover" style=""><thead>' + tableHeader + '</tr></thead>';
            table += '<tbody><tr>' + tableBody + '</tr></tbody></table></div></div>';
            CloseGif();
            $('body').append(table);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
        },
        error: function (xhr, status) {
            CloseGif();
            return alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

function getIndex(date, text, idCompany) {
    LoadGif();
    $.ajax({
        url: site_path + index,
        type: "GET",
        dataType: "html",
        data: { date: date, idCompany: idCompany },
        success: function (data) {
            var firstRow = $('<div />').append(data).find('#first-row').html();
            var secondRow = $('<div />').append(data).find('#second-row').html();
            $("#first-row").html(firstRow);
            $("#second-row").html(secondRow);
            $('#actualCompany').text(text);
            $("#divTable").removeClass('d-none');
            $("#divTable").addClass('d-block');
            $('#arrowDashboardSection1').removeClass('fa-caret-right');
            $('#arrowDashboardSection1').addClass('fa-caret-down');
            loadDashboardGraphic(dashboardGraphic, date);
        },
        error: function (xhr, status) {
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
            CloseGif();
        }
    });
}

//Função para pegar a url de acordo com a requisição
function getUrlPath(value, text) {
    let url = '';
    if (value === 'todas') {
        url = site_path + movesForCompanyUrl;
    } else if (value != '') {
        url = site_path + movesForCompanyUrl + '/' + value;
    } else if (value === 'index') {
        url = site_path + index;
    } else {
        url = null;
    }
    return url;
}

//Ajax para pegar os dados do gráfico
function loadDashboardGraphic(url, date) {
    $.ajax({
        url: url,
        type: "GET",
        data: {date: date},
        success: function (data) {
            plotGraphic(data);
            CloseGif();
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

//Função pra plotar o gráfico
function plotGraphic(data)
{
    //Função para gerar o gráfico
    var ctx = $("#myChart");

    let days = [], pay = [], receive = [], result = [], projectedBalance = [], maxValue = 0;

    if (data) {
        days = data.days;
        pay = data.pays;
        receive = data.receives;
        invoices = data.invoices;
        projectedBalance = data.projectedBalance;
    }
    
    // $.each(receive, function (key, val) {
    //     if (key === 0) maxValue = parseInt(val);
    //     else if (parseInt(val) > maxValue) maxValue = parseInt(val);
        
    //     if (Math.abs(pay[key]) > maxValue) maxValue = Math.abs(pay[key]);
    // })

    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [{
                label: 'Contas à pagar',
                data: pay,
                borderColor: '#dd4d4d',
                backgroundColor: 'rgba(236, 187, 187, 0.3)',
                type: 'line',
                lineTension: 0
            }, {
                label: 'Contas à receber caixa',
                data: receive,
                borderColor: '#42b243',
                backgroundColor: 'rgba(188, 247, 189, 0.5)',
                type: 'line',
                lineTension: 0
            }, {
                label: 'Contas à receber convenios',
                data: invoices,
                borderColor: '#54b352',
                backgroundColor: 'rgba(198, 230, 150, 0.7)',
                type: 'line',
                lineTension: 0
            }, {
                label: 'Saldo Projetado',
                data: projectedBalance,
                borderColor: '#6489c1',
                backgroundColor: 'rgba(166, 203, 238, 0.9)',
                type: 'line',
                lineTension: 0
            }],
            labels: days
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    barPercentage: 0.7
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        // min: maxValue * -1,
                        // max: maxValue
                    }
                }]
            }
        }
    });
    myChart.clear();
    myChart.update();
}

// //Função para fazer get na tabela da dashboard
// function executeFetch() {
//     fetch(url).then(response => response.json()).then(result => {
//         let tableData = getIndexOfTable();
//         tableData = getTableBody(tableData, result.openedMovesBank);
//         tableData = getTableHeader(tableData);
//         let tableResponsive = createTable(tableData);

//         $('.table-responsive thead').remove();
//         $('.table-responsive tbody').remove();
//         $('.table-responsive').append(tableResponsive);
//     }).catch(err => {
//         //TODO criar um modal bonito para informar erro ao usuário!
//         console.error('Failed retrieving information', err);
//     });
// }