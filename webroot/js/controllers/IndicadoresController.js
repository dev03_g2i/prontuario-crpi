//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
    loadTwelveMonthsCharts();
    //Função para pegar os dados da tabela por empresa
    // .on('change', '#empresas', function () {
    //     let value = $('#empresas').val();
    //     let text = $('#empresas option:selected').text();
    //     if (value === '') return;
    //     let url = getUrlPath(value, text);
    //     if (url !== null) loadDashboardTable(url, text);
    // })
    $(".datepicker-month").datepicker({
        format: "mm/yyyy",
        viewMode: "months",
        minViewMode: "months"
    });
    $('#indicators-search').click(function () {
        let month = $('#month').val();
        let year = $('#year').val();
        loadTwelveMonthsCharts(month, year);
    });
});

//Ajax para pegar os dados do gráfico
function loadTwelveMonthsCharts(month, year) {
    $.ajax({
        url: site_path + '/FinIndicadores/index.json',
        type: "GET",
        data: { month: month, year: year },
        success: function (data) {
            plotBarChart(data);
            plotLineChart(data);
            CloseGif();
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

function plotBarChart(data) {
    //Função para gerar o gráfico

    $('canvas#MovesOfBalanceBar').remove();
    $("#chart-container-moves-bar").append('<canvas id="MovesOfBalanceBar"></canvas>');
    let ctx = $("#MovesOfBalanceBar");

    let saidas = [], competencias = [], entradas = [], saldos = [];

    if (data && data.resultMoves) {
        $.each(data.resultMoves, function (objKey, obj) {
            saidas.push(obj.saidas);
            competencias.push(obj.competencia);
            entradas.push(obj.entradas);
            saldos.push(obj.saldo);
        });
    }

    let MovesOfBalanceBar = new Chart(ctx, {
        type: 'bar',
        data: {
            datasets: [{
                label: 'Entradas',
                data: entradas,
                borderColor: '#42b243',
                backgroundColor: 'rgba(34, 177, 36, 0.8)'
            }, {
                label: 'Saídas',
                data: saidas,
                borderColor: '#dd4d4d',
                backgroundColor: 'rgba(228, 16, 16, 0.8)'
            }, {
                label: 'Resultados',
                data: saldos,
                borderColor: '#000',
                backgroundColor: 'rgba(0, 0, 0, 1)'
            }],
            labels: competencias
        },
        options: {
            responsive: true,
            scales: {
                xAxes: [{
                    barPercentage: 0.7
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        // min: maxValue * -1,
                        // max: maxValue
                    }
                }]
            }
        }
    });
    MovesOfBalanceBar.update();
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function plotLineChart(data) {
    //Função para gerar o gráfico
    // $('#myChart').remove();
    $('canvas#MovesOfBalanceLine').remove();
    $("#chart-container-moves-line").append('<canvas id="MovesOfBalanceLine"></canvas>');
    var ctx = $("#MovesOfBalanceLine");

    let lineChart = data && data.resultMovesForClass ? data.resultMovesForClass : {};
    let newDataSets = [];

    $.each(lineChart.arrayMovesForClass, function (objMovesKey, objMoves) {
        let data = [];
        $.each(objMoves.moves, function (key, moves) {
            data.push(moves.saldo);
        });
        let newLine = {
            label: objMoves.descricao,
            data: data,
            borderColor: getRandomColor(),
            backgroundColor: 'rgba(255, 255, 255, 0)',
            lineTension: 0
        }
        newDataSets.push(newLine);
    });

    var MovesOfBalanceLine = new Chart(ctx, {
        type: 'line',
        data: {
            datasets: newDataSets,
            labels: lineChart.competencias
        },
        options: {
            responsive: true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        // min: maxValue * -1,
                        // max: maxValue
                    }
                }]
            }
        }
    });
    
    MovesOfBalanceLine.update();
}

// //Função para fazer get na tabela da dashboard
// function executeFetch() {
//     fetch(url).then(response => response.json()).then(result => {
//         let tableData = getIndexOfTable();
//         tableData = getTableBody(tableData, result.openedMovesBank);
//         tableData = getTableHeader(tableData);
//         let tableResponsive = createTable(tableData);

//         $('.table-responsive thead').remove();
//         $('.table-responsive tbody').remove();
//         $('.table-responsive').append(tableResponsive);
//     }).catch(err => {
//         //TODO criar um modal bonito para informar erro ao usuário!
//         console.error('Failed retrieving information', err);
//     });
// }