//Constantes usadas pela controller
const finishAccount = '/fin-contas-receber/finishAccount';

let total = 0, idContasPagamento = [];
//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
    //Função de pesquisa da index
        .on('click', '#pesquisar', function (event) {
            event.preventDefault();
            let value = $('#pesquisar').attr('id');
            let data = {
                inicio: $('#data').val(),
                fim: $('#vencimento').val(),
                planoconta: $('#planoconta-id').val(),
                fornecedor: $('#fornecedor-id').val(),
                contabilidade: $('#contabilidade-id').val(),
                situacao: $('#situacao').val(),
                status_conta: $('#status-conta').val()
            };
            let url = getUrlPath(value);
            if (url !== null) loadTable(url, data);
        })
        .on('click', '.modal-close', function(e) {
            LoadGif();
            location.reload();
        });

    $('input').on('ifChecked', function(event){
        total += parseFloat($(this).attr('valor'));
        idContasPagamento.push($(this).attr('id'));
        $('#totalSelecionado').text(total.toFixed(2));
    });

    $('input').on('ifUnchecked', function(event){
        total -= parseFloat($(this).attr('valor'));
        let id = $(this).attr('id');
        $.each(idContasPagamento, function( index, value ) {
            if (value === id) idContasPagamento.splice(index, 1);
            if (idContasPagamento.length === 0) total = 0;
        });
        $('#totalSelecionado').text(total.toFixed(2));
    });

    $('#processarContas').on('click', function(event) {
        const form = {
            bancoMovimento: $('#banco-movimento').val(), 
            documento: $('#documento').val(), 
            formaPagamento: $('#forma-pagamento').val(),
            ids: idContasPagamento
        }
        if (form.ids.length <= 0) return alert('Selecione pelo menos uma conta!');
        quitarConta(form);
    })

    //Função para alterar o valor líquido
    $('#valor-bruto, #multa, #juros, #desconto').keydown(function (event) {
        setTimeout(function () {
            let value = $('#valor-bruto').val() != null ? parseFloat($('#valor-bruto').val()) : 0;
            let fine = $('#multa').val() != null ? parseFloat($('#multa').val()) : 0;
            let interest = $('#juros').val() != null ? parseFloat($('#juros').val()) : 0;
            let deduction = $('#desconto').val() != null ? parseFloat($('#desconto').val()) : 0;
            let total = value + fine + interest - deduction;
            total = total.toString();
            total = number_format(total, 2, '.', '');
            $('#valor').val(total.toFixed(2));
        }, 300);
    });
});

var planoContas = function definePlanoContas(id_tipo_pagamento) {
    $.ajax({
        type: 'POST',
        data: {
            idTipoPagamento: id_tipo_pagamento
        },
        url: site_path + '/Contasreceber/getPlanoConta',
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif();
        },
        complete: function () {
            CloseGif();
        },
        success: function (txt) {
            $("#id_plano_contas_input").val(txt);
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

function quitarConta (form){
    LoadGif();
    $.ajax({
        url: site_path + finishAccount + '.json',
        type: "get",
        data: form,
        dataType: 'JSON',
        success: function (data) {
            if (data.result.length === 0) {
                CloseGif();
                return alert("Ops, esta data não possuí dados!");
            }
            let tableHeader = '';
            let tableBody = '';
            $.each(data.result, function (objKey, obj) {
                tableHeader += '<tr>';
                tableBody += '<tr>';
                $.each(obj, function (attr, val) {
                    if (objKey === 0) tableHeader += '<th class="text-center">'+ attr +'</th>';
                    tableBody += '<td align="center">'+ val +'</td>';
                });
                tableHeader += '</tr>';
                tableBody += '</tr>';
            });
            let table = '<div id="modalDashboard" class="clearfix"><div class="table-modal-responsive" style="width: 80%; max-height: 80%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">';
            table += '<div class="col-sm-12 text-center m-b-5"><strong>Transação realizada com sucesso - Créditos efetivados</strong></div><div class="col-sm-12 text-center" style="border-bottom: thin solid lightgray"><strong>Movimentos</strong></div><table class="table table-hover" style=""><thead>'+tableHeader+'</thead>';
            table += '<tbody><tr>'+tableBody+'</tr></tbody></table></div></div>';
            $('body').append(table);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
            $( "#btn-fill" ).trigger('click');
            total = 0;
            idContasPagamento = [];
            $('#totalSelecionado').text(total);
            CloseGif();
        },
        error: function (xhr, status) {
            CloseGif();
            return alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

var Contasreceber = function (element) {
    $("#tipoPagamentoInput").each(function () {
        if ($("#controle_plano").val() == 0) {
            planoContas($("#tipoPagamentoInput").val());
        }
    });

    $("#tipoPagamentoInput").change(function () {
        if ($("#controle_plano").val() == 0) {
            planoContas($("#tipoPagamentoInput").val());
        }
    });
};

