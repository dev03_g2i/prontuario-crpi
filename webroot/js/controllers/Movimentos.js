const dateMovesForBank = '/FinMovimentos/getMovesForBank/',
    getModalTransf = site_path + '/FinMovimentos/transferenciaModal',
    getOpenedMovesBankExceptThese = site_path + '/FinMovimentos/getOpenedMovesBankExceptThese',
    getChartOfProvider = site_path + '/fin-contas-pagar/getChartOfProvider';

//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
        //Função para pegar as empresas por grupo.
        .on('change', '#banco, #situacao', function () {
            let banco = $('#banco').val(),
                situacao = $('#situacao').val();
            if (banco != '') {
                getDataMovesForBank(banco, situacao);
            }
        })
        .on('click', '.modal-close, #cancel-transf', function (e) {
            $('div').remove('.modal-background, #modalTransfer');
            e.preventDefault();
        })
        .on('click', '#create-transf', function (e) {
            e.preventDefault();
            loadTransModal();
        })
        .on('mousedown', '#banco-movimento-origem, #banco-movimento-destino', function () {
            let ids = [];
            let selectId = this.id;
            if (selectId === 'banco-movimento-origem') {
                $('#banco-movimento-destino').val() ? ids.push($('#banco-movimento-destino').val()) : ids = ids;
            } else if (selectId === 'banco-movimento-destino') {
                $('#banco-movimento-origem').val() ? ids.push($('#banco-movimento-origem').val()) : ids = ids;
            }
            getOpenedMovesBankExcept(ids, selectId);
        })

    $('#fin-fornecedor-id').change(function () {
        if (window.location.href.includes('add')) {
            let id = $('#fin-fornecedor-id').val();
            loadChartAccountOfProvider(id);
        }
    });

    //Função para trocar de credor para cliente de acordo com os checkboxes
    $('#addMoveRadio input:radio[name=tipo]').each(function (key, value) {
        $(`input:radio[id=${value.id}]`).on('ifChecked', function (event) {
            let name = $(`#${value.id}`).attr('id'),
                valor = $(`#${value.id}`).attr('value');
            if (name === 'fornecedor') {
                $(`div[name=fin_fornecedor_id]`).toggleClass('d-none d-block');
                $(`div[name=cliente_id]`).toggleClass('d-none d-block');
                $(`div select[name=cliente_id]`).val(null);
            } else if (name === 'cliente') {
                $(`div[name=cliente_id]`).toggleClass('d-none d-block');
                $(`div[name=fin_fornecedor_id]`).toggleClass('d-none d-block');
                $(`div select[name=fin_fornecedor_id]`).val(null);
            }
        })
    });

    $('#gerar-report').click(geraRelatoriosMovimentos);
});

let geraRelatoriosMovimentos = function () {
    if ($("#movimento").val() === "") return alert('Por favor, selecione um movimento antes!');

    if (!empty(click)) {
        return false;
    }
    click = 1;

    let url = site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action');
    let dados = $('#fin-movimentos-form').serializeArray();
    let form = '';
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    let formulario = '<form class="temp-form" action="' + url + '" method="POST" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    click = null;
};

function loadTransModal() {
    LoadGif();
    $.ajax({
        url: getModalTransf,
        type: "GET",
        dataType: "html",
        success: function (data) {
            CloseGif();
            var modal = $(data).find('#modalTransfer');
            $('body').append(modal);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

function getOpenedMovesBankExcept(ids, selectId) {
    LoadGif();
    $.ajax({
        url: getOpenedMovesBankExceptThese + '.json',
        type: "GET",
        dataType: "json",
        data: { movesBankIds: ids },
        success: function (data) {
            CloseGif();
            let mensagemText = '';
            $.each(data.banco_movimentos, function (key, val) {
                mensagemText += "<option value='" + key + "'>" + val + "</option>";
            });
            $(`#${selectId}`).prop('disabled', false);
            $(`#${selectId} option`).remove();
            $(`#${selectId}`).append(mensagemText);
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

function getDataMovesForBank(banco, situacao) {
    LoadGif();
    $.ajax({
        url: site_path + dateMovesForBank + '.json',
        type: "GET",
        dataType: "json",
        data: {
            banco: banco,
            situacao: situacao
        },
        success: function (data) {
            let mensagemText = '';
            $.each(data.movesDate, function (key, val) {
                if (key === "1") {
                    mensagemText += "<option selected value=''>Selecione</option>";
                    mensagemText += "<option value='todas'>Todas</option>";
                }
                mensagemText += "<option value='" + val.id + "'>" + val.data_movimento + "</option>";
            });
            $('#movimento').prop('disabled', false);
            $('#movimento option').remove();
            $('#movimento').append(mensagemText);
            CloseGif();
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

//Função para pegar o plano de contas do fornecedor e plotar ao selecionar um na tela de add contas a pagar.
function loadChartAccountOfProvider(id) {
    $.ajax({
        url: getChartOfProvider + '.json',
        type: "GET",
        data: { id: id },
        success: function (data) {
            $("#fin-plano-conta-id").append($('<option>', {
                value: data.planoconta.id,
                text: data.planoconta.nome
            }));
            $('#select2-fin-plano-conta-id-container').attr('title', data.planoconta.nome);
            $('#select2-fin-plano-conta-id-container').text(data.planoconta.nome);
        },
        error: function (xhr, status) {
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}