var ordenacao = 0;// Ordenação dos procedimentos cadastrados

var addProcedimento = function (element, action, atendimento_id) {
    if (!empty(click)) {
        return false;
    }
    click = 1;

    if ($(element).find("#cliente-id").val() == "") {
        BootstrapDialog.alert('Selecione um Paciente!');
        return false;
    }
    if ($(element).find("#convenio-id").val() == "") {
        BootstrapDialog.alert('Selecione um convênio!');
        return false;
    }

    last_url = site_path + "/AtendimentoProcedimentos/" + action + "?convenio=" + $(element).find("#convenio-id").val() + '&atendimento_id=' + atendimento_id + '&action=' + action + "&ajax=1&first=1";
    dialog_sub = new BootstrapDialog({
        title: 'Cadastro Procedimento',
        message: $('<div></div>').load(last_url, carregar_procedimentos),
        closable: true,
        size: BootstrapDialog.SIZE_WIDE,
        buttons: [{
            label: 'Finalizar',
            cssClass: 'btn-primary btn-finaliza-procedimento hidden',
            action: function (dialogRef) {
                var modal = dialogRef.getModalBody();

                $($(modal).find("#form_procedimento")).ajaxForm({
                    beforeSubmit: function () {
                        LoadGif();
                    },
                    success: function (txt) {
                        CloseGif();
                        if (!empty(txt)) {
                            click = null;
                            dialog_sub = null;
                            dialogRef.close();
                            var filtro = {
                                procedimento_id: '',
                                convenio_id: ''
                            };
                            if (action === 'add') {
                                $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
                            } else if (action === 'edit') {
                                atualizarProcedimentos(txt);
                            }
                            toastr.success('Procedimento adicionado com sucesso!');
                        } else {
                            toastr.error(msgErrorAjax);
                        }
                    }
                }).submit();

            }
        }]

    });
    dialog_sub.open();
};

var editProcedimento = function (atendProcId, action) {
    var convenio_id = $('#convenio-id').val();
    if (action === 'edit_add') {
        last_url = site_path + "/AtendimentoProcedimentos/add?convenio=" + convenio_id + "&pos=" + atendProcId + "&action=" + action + "&ajax=1&first=1";
    } else if (action === 'edit') {
        last_url = site_path + "/AtendimentoProcedimentos/edit/" + atendProcId + "?convenio=" + convenio_id + "&action=" + action + "&ajax=1&first=1";
    }

    dialog_sub = new BootstrapDialog({
        title: 'Editar Procedimento',
        message: $('<div></div>').load(last_url, carregar_procedimentos),
        closable: true,
        size: BootstrapDialog.SIZE_WIDE,
        buttons: [{
            label: 'Finalizar',
            cssClass: 'btn-primary btn-finaliza-procedimento hidden',
            action: function (dialogRef) {
                var modal = dialogRef.getModalBody();

                $($(modal).find("#form_procedimento")).ajaxForm({
                    beforeSubmit: function () {
                        LoadGif();
                    },
                    success: function (txt) {// Retorna numero do atendimento
                        CloseGif();
                        dialogRef.close();
                        if (!empty(txt)) {
                            var filtro = {
                                procedimento_id: '',
                                convenio_id: ''
                            };
                            if (action === 'edit_add') {
                                $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
                            } else if (action === 'edit') {
                                atualizarProcedimentos(txt);
                            }
                            toastr.success('Procedimento adicionado com sucesso!');
                        } else {
                            toastr.error(msgErrorAjax);
                        }
                    }
                }).submit();

            }
        }]

    });
    dialog_sub.open();
};

var loadJqueryStep = function (element, form) {
    var total_steps = 0;
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "fade",
        labels: {
            finish: "Finalizar",
            next: "Próximo",
            previous: "Anterior",
            cancel: 'Cancelar'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, newIndex) {
            if (currentIndex == total_steps) {
                form.find(".actions .liFinish").removeClass('disabled');
                form.find(".actions .liFinish a").attr('href', '#finish');
            }
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":hidden";
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            /* Adiciona a ordenacao dos procedimento cadastrados */
            let regra = $('#precoprocedimento-regra').val();
            let rowCount = 0;
            if (!empty(regra)) {
                rowCount = $('.lista-procedimentos tbody tr.' + regra).length;
            }
            if (rowCount == 0) {
                ordenacao = 1;
            } else {
                ordenacao = rowCount + 1;
            }
            $('#ordenacao').val(ordenacao);
            /********************************/
            let btnFinalizar = form.closest('.modal-content').find('.btn-finaliza-procedimento');
            btnFinalizar.click();
            setTimeout(function () {
                $('#plus-procedimento').click();
            }, 1500);
        },
        onCanceled: function () {
            var btnCancelar = form.closest('.modal-content').find('.bootstrap-dialog-close-button button');
            btnCancelar.click();
        }
    });

    form.find('.steps li').each(function (index) {
        total_steps = index;
    });
    if (total_steps !== 0) {
        form.find(".actions a[href='#finish']").parent().addClass('disabled liFinish');
        form.find(".actions a[href='#finish']").attr({'href': 'javascript:void(0)'});
    }
};

var ajaxValidaCarteirinha = function (cliente_id, convenio_id) {

    var form = $('#frm-atendimentos');
    form.find('.btnSalveAtendimento').attr({'disabled': true, 'type': 'button'});

    if (!empty(form.find('#cliente-id').val()) && !empty(form.find('#convenio-id').val())) {
        $.ajax({
            type: 'POST',
            url: site_path + '/Clientes/verifica-carteira',
            data: {
                cliente_id: cliente_id,
                convenio_id: convenio_id
            },
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif();
            },
            success: function (txt) {
                if (txt.erro == 1) {
                    required('Atenção', txt.msg);
                    return false;
                } else {
                    form.find('.btnSalveAtendimento').attr({'disabled': false, 'type': 'submit'});
                    return true;
                }
            },
            complete: function () {
                CloseGif();
            },
            error: function () {
                toastr.error(msgErrorAjax);
            }
        });
    }

};

var checkConfiFicha = function (atendimento_id) {
    $.ajax({
        type: 'POST',
        url: site_path + '/Atendimentos/checkConfigFicha/' + atendimento_id,
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif();
        },
        success: function (txt) {
            if (txt.error == 1) {
                toastr.warning(txt.msg);
            } else {
                window.open(site_path + '/Atendimentos/report/' + atendimento_id, '_blank');
            }
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

var toggleInternacaoArea = function () {
    var tipo_atendimento_id = $("#tipo_atendimento_id_input").val();
    if (tipo_atendimento_id == 10) {
        $("#inputs_internacao").show();
    } else {
        $("#inputs_internacao").hide();
    }
};

var verificaNfs = (atendimento_id) => {
    let url_nfs = site_path + '/Nfs/index/'+atendimento_id;
    $.ajax({
        type: 'POST',
        url: site_path + '/Nfs/verificaNfs/'+atendimento_id,
        dataType: 'JSON',
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            if(data.count_nfs == 0){

                if(data.total_liquido <= 0){
                    toastr.warning("Total liquido zerado!<br> Verifique os valores dos procedimentos.");
                    return false;
                }
                
                let options = '';
                $.each(data.prestadores, function(key, value){
                    options += '<option value="'+key+'">'+value+'</option>';
                })
                msg = '<label for="prestador">Prestador</label>';
                msg += '<select class="form-control" id="prestador">'+options+'</select>';
                
                dialog_modelos = new BootstrapDialog({
                    size: BootstrapDialog.SIZE_NORMAL,
                    title: 'Emitir nota fiscal ?',
                    message: msg,
                    buttons: [{
                        label: 'Cancelar',
                        cssClass: 'btn-default',
                        action: function (dialogRef) {
                            dialogRef.close();
                        }
                    },
                    {
                        label: 'Confirmar',
                        cssClass: 'btn-primary',
                        action: function (dialogRef) {
                            let modal = dialogRef.getModalBody();
                            
                            $.ajax({
                                type: 'POST',
                                url: site_path + '/Nfs/saveNfs/'+atendimento_id,
                                data: {
                                    prestador: modal.find('#prestador').val()
                                },
                                dataType: 'JSON',
                                beforeSend: () => {
                                    LoadGif();
                                },
                                success: (data) => {
                                    if(!empty(data)){
                                        toastr.success('Nota enviada para emissão!');
                                        dialogRef.close();
                                        window.open(url_nfs, '_blank');
                                    }
                                },
                                complete: () => {
                                    CloseGif();
                                },
                                error: () => {
                                    CloseGif();
                                    toastr.error(msgErrorAjax);
                                },
                            });
                        }
                    }
                    ],
                });
                dialog_modelos.open();
            }else{
                window.open(url_nfs, '_blank');
            }
        },
        complete: () => {
            CloseGif();
        },
        error: () => {
            CloseGif();
            toastr.error(msgErrorAjax);
        },
    });
};

var cancelaAtendimento = (atendimento_id) => {
    let msg = '<label for="justificativa">Justificativa</label>';
    msg += '<textarea class="form-control" maxlength="255" id="justificativa" placeholder="Informe uma justificativa.."></textarea>';
    dialog_modelos = new BootstrapDialog({
        size: BootstrapDialog.SIZE_NORMAL,
        title: 'Atenção',
        message: msg,
        type: BootstrapDialog.TYPE_DANGER,  
        buttons: [{
            label: 'Cancelar',
            cssClass: 'btn-default',
            action: function (dialogRef) {
                dialogRef.close();
            }
        },
        {
            label: 'Confirmar',
            cssClass: 'btn-primary',
            action: function (dialogRef) {
                let modal = dialogRef.getModalBody();
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: site_path + '/Atendimentos/cancelaAtendimento/',
                    data: {
                        atendimento_id,
                        justificativa: modal.find('#justificativa').val(),
                    },
                    beforeSend: () => {
                        LoadGif();
                    },
                    success: (data) => {
                        if(data === 0){
                            dialogRef.close();
                            toastr.success("O atendimento foi cancelado com sucesso!");
                            $('#btn-fill').click();
                        }
                    },
                    complete: () => {
                        CloseGif();
                    },
                    error: () => {
                        CloseGif();
                        toastr.error(msgErrorAjax);
                    },
                });
            }
        }
        ],
    });
    dialog_modelos.open();
};
let geraRelatorioPrecoProcedimento = function (button) {

    let url = site_path + '/' + button.attr('controller') + '/' + button.attr('action');
    let dados = $('#atendimento-form').serializeArray();
    let form = '';console.log(url);
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    
    let formulario = '<form class="temp-form" action="' + url + '" method="GET" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    $('#btn-fill').click();
    click = null;
};
var Atendimentos = function (element) {
    $(element).find('#gerar-report').click(function(){
        let inicio = $(element).find('#inicio').val();
        let fim = $(element).find('#fim').val();
        let tipo = $(element).find('#relatorio').val();
        
        if (!fim || !inicio || !relatorio) return alert('Os campos períodos inicio e fim');

        let url = site_path + '/atendimentos/callsReports';
        
        let form = '';
        form += '<input class="temp-form" type="hidden" id="inicio" name="inicio" value="' + inicio + '">';
        form += '<input class="temp-form" type="hidden" id="fim" name="fim" value="' + fim + '">';
        form += '<input class="temp-form" type="hidden" id="type" name="type" value="' + tipo + '">';
        form += '<input class="temp-form" type="submit" id="report-submit">';
        let formulario = '<form class="temp-form" action="' + url + '" method="get" target="_blank">' + form + '</form>';
        $(formulario).appendTo($('.append-report'));
        $('#report-submit').click();
    })
    /* Habilita a área do formulário para internação */
    $(element).find("#tipo_atendimento_id_input").each(function () {
        toggleInternacaoArea();
    });
    $(element).find("#tipo_atendimento_id_input").change(function () {
        toggleInternacaoArea();
    });

    $(element).find('#plus-procedimento').click(function () {
        var atendimento_id = (!empty($(this).attr('atendimento_id'))) ? $(this).attr('atendimento_id') : null;
        var action = (!empty($(this).attr('action'))) ? $(this).attr('action') : null;
        addProcedimento(element, action, atendimento_id);
    });

    /* Navega no modal para contasReceber */
    $(element).find('#btnOpenFinanceiro').click(function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        Navegar(href, 'go');
    });

    /* Manipulando os valores dos Procedimentos */
    $(element).find('#form_procedimento').each(function () {
        var form = $(this);
        var convenio_id = form.find('#convenio-id').val();
        loadJqueryStep(element, form);

        form.find("#aplicar-desconto").click(function () {
            var desconto = form.find('#desconto').val();
            var total_liquido = form.find('#total-liquido').val();
            if (!empty(desconto)) {
                $.ajax({
                    type: 'POST',
                    url: site_path + '/AtendimentoProcedimentos/get-desconto',
                    data: {
                        total_liquido: total_liquido,
                        desconto: desconto
                    },
                    dataType: 'JSON',
                    beforeSend: function () {
                        LoadGif()
                    },
                    success: function (txt) {
                        if (txt.error === 0) {
                            form.find('#total-liquido, #valor-caixa').val(number_format(txt.total_liquido, 2, '.', ''));
                        } else {
                            toastr.warning(txt.msg);
                        }
                    },
                    complete: function () {
                        CloseGif();
                    },
                    error: function () {
                        toastr.error(msgErrorAjax);
                    }
                })
            }

        });

        form.find("#quantidade").bind('keyup mouseup', function () {
            var qtd = parseInt($(this).val());
            var vl_caixa_original = parseFloat(form.find('#vl-caixa-original').val());
            var vl_caixa_atualizado = vl_caixa_original * qtd;
            form.find('#valor-total, #total-liquido, #valor-caixa').val(number_format(vl_caixa_atualizado, 2, '.', ''));
        });
        form.find('#valor-unitario').on('blur', function () {
            form.find('#valor-caixa, #vl-caixa-original, #total-liquido, #valor-unitario, #valor-total').val($(this).val());
        });

        /*
            Foi feito desta maneira pois ja existe um change sendo executado para este campo.[Localizado no extension.js]
            Nas telas de edit, o ajaxGetPrecoProc estava sendo executado automaticamente.
        */
        var form_group = form.find('#procedimento-id').closest('.form-group');
        form_group.on('click', '.select2-selection', function () {
            if (!empty(click)) {
                return false;
            }
            click = 1;
            form.find('#procedimento-id').change(function () {
                $.ajax({
                    type: 'POST',
                    url: site_path + '/PrecoProcedimentos/get-preco-proc',
                    data: {
                        convenio_id: convenio_id,
                        procedimento_id: $(this).val()
                    },
                    dataType: 'JSON',
                    beforeSend: function () {

                        LoadGif()
                    },
                    success: function (txt) {
                        form.find('#codigo').val(txt.codigo);
                        form.find('#precoprocedimento-regra').val(txt.regra);
                        form.find('#valor-total, #valor-caixa, #valor-unitario, #vl-caixa-original, #total-liquido').val(number_format(txt.valor_particular, 2, '.', ''));
                        form.find('#valor-base, #valor-fatura').val(number_format(txt.valor_faturar, 2, '.', ''));
                        /* Reseta quantidade  e desconto */
                        form.find('#quantidade').val(1);
                        form.find('#desconto').val('0.00');
                        /* Se estiver dias_entrega para o procedimento, seta no datetimepicker */
                        var new_date = moment().add(txt.procedimento.dias_entrega, 'days').format('DD/MM/YYYY LT');
                        form.find("#prev-entrega-proc").attr('value', new_date);
                        form.find('#filme-reais').val(number_format(txt.filme_reais, 2, '.', ''));
                        form.find('#uco').val(number_format(txt.uco, 2, '.', ''));
                        form.find('#porte').val(number_format(txt.porte, 2, '.', ''));
                        form.find('#filme').val(number_format(txt.filme, 2, '.', ''));
                    },
                    complete: function () {
                        CloseGif();
                    },
                    error: function () {
                        toastr.error(msgErrorAjax);
                    }
                })
            });
        });

    });

    /* Atualizado os valores ao fechar modal do constasreceber */
    $(element).find('.contasreceber #atendimento-id').each(function () {
        var atendimento_id = $(this).val();
        $('.modal').on('hidden.bs.modal', function () {
            if (!empty(click)) {
                return false;
            }
            click = 1;
            somaprocedimentos(atendimento_id);
        });
    });

    // Ao selecionar um convênio, valida se precisa preencher a carteira do paciente
    // $('#frm-atendimentos').find('#convenio-id').change(function () {
    //     ajaxValidaCarteirinha($('#cliente-id').val(), $('#convenio-id').val())
    // });
};