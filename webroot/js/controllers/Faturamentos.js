var countFilter = function () {
    var filtro = $(this).closest('.filtros');
    var formSerialize = filtro.find('form').serialize();

    if (!empty(click)) {
        return false;
    }
    click = 1;
    $.ajax({
        type: 'GET',
        url: site_path + '/Faturamentos/countFilter/',
        dataType: 'JSON',
        data: formSerialize,
        success: function (txt) {
            $('.count').text(txt.count);
            $('.total').text(formatReal(txt.total));
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

var GerarRport = function (element) {
    $.ajax({
        type: 'POST',
        url: site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action'),
        dataType: 'JSON',
        data: $('#form-faturamentos').serialize(),
        beforeSend: function () {
            LoadGif();
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};
var ReportPost = function () {
    if (!empty(click)) {
        return false;
    }
    click = 1;

    if (empty($("#convenio_id_faturamento").val())) {
        BootstrapDialog.alert({
            title: 'Atenção',
            type: BootstrapDialog.TYPE_DANGER,
            message: "Selecione um convênio!"
        });
        return false;
    }

    var url = site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action');
    var dados = $('#form-faturamentos').serializeArray();
    var form = '';
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    var formulario = '<form class="temp-form" action="' + url + '" method="POST" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    click = null;
};

var ReportEquipePost = function () {
    if (!empty(click)) {
        return false;
    }
    click = 1;

    var url = site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action');
    var dados = $('#form-equipes').serializeArray();
    var form = '';
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    var formulario = '<form class="temp-form" action="' + url + '" method="POST" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    click = null;
};

var XmlPost = function () {

    if (!empty(click)) {
        return false;
    }
    click = 1;

    if (empty($('#convenio_id_faturamento').val())) {
        BootstrapDialog.alert({
            title: 'Atenção',
            type: BootstrapDialog.TYPE_DANGER,
            message: "Selecione um convênio!"
        });
        return false;
    }
    else if ($('#convenio_id_faturamento').val().length > 1) {
        BootstrapDialog.alert({
            title: 'Atenção',
            type: BootstrapDialog.TYPE_DANGER,
            message: "Só é possível gerar o XML para um convênio por vez!"
        });
        return false;
    }
    var url = site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action');
    var dados = $('#form-faturamentos').serializeArray();

    $.ajax({
        type: 'POST',
        url: url,
        data: dados,
        beforeSend: function () {
            LoadGif()
        },
        complete: function () {
            CloseGif();
        },
        success: function (res) {
            BootstrapDialog.alert({
                title: 'Atenção',
                type: BootstrapDialog.TYPE_SUCCESS,
                message: "XML Gerado com sucesso!" + "\n" + "Nome do Arquivo: " + res
            });

            window.open(site_path + '/Relatorios/baixarXml/' + res, '_blank');
        }
    });


};

var filterEquipes = () =>{
    $.ajax({
        type: 'GET',
        url: site_path + '/Faturamentos/filter-equipes',
        data: $('#form-equipes').serialize(),
        dataType: 'JSON',
        success: function (txt) {
            $(".total_geral").text(formatReal(txt.total));
            $(".contador").text(txt.contador);
            executado = 0;
        }
    })
}
;

var Faturamentos = function (element) {
    // Total numero de registros e total valor
    $(element).find('.btnCountFilter').click(countFilter);
    $(element).find('#gerar-report').click(ReportPost);
    $(element).find('#gerar-report-equipes').click(ReportEquipePost);
    $(element).find('#gerar-xml').click(XmlPost);
    $(element).find('.btnFilterEquipes').click(filterEquipes);
};


