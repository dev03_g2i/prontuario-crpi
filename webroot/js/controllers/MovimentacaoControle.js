const editMovesBankModal = site_path + '/FinMovimentos/editMovesBankModal',
      finishMovesBankModal = site_path + '/FinMovimentos/finishMovesBankModal',
      finishMovesBank = site_path + '/FinMovimentos/finishMovesBank';
    
//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
        .on('click', '.modal-close, #cancel-edit-moves-bank, #cancel-finish-moves-bank', function (e) {
            $('div').remove('.modal-background, #modalEditMovesBank, #modalFinishMovesBank' );
            e.preventDefault();
        })
        .on('click', 'button[name="edit-moves-bank"]', function (e) {
            e.preventDefault();
            let id = $(this).val();
            loadModalEditMovesBank(id);
        })
        .on('click', 'button[name="finish-moves-bank"]', function (e) {
            e.preventDefault();
            let id = $(this).val();
            loadModalFinishMovesBank(id);
        })
        .on('click', '#save-finish-moves-bank', function(e) {
            e.preventDefault();
            const form = {
                id: $('#bankMoveId').val(),
                saldoInicial: $('#saldoinicial').val(),
                saldoFinal: $('#saldoatual').val()
            }
            saveFinishMovesBank(form);
        })
});

function saveFinishMovesBank(form) {
    LoadGif();
    $.ajax({
        url: finishMovesBank + '.json',
        type: "GET",
        data: form,
        success: function (data) {
            CloseGif();
            $('.modal-close').click();
            $('#btn-fill').click();
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

function loadModalEditMovesBank(id) {
    LoadGif();
    $.ajax({
        url: editMovesBankModal,
        type: "GET",
        dataType: "html",
        data: {bankMoveId: id},
        success: function (data) {
            CloseGif();
            var modal = $(data).find('#modalEditMovesBank');
            $('body').append(modal);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

function loadModalFinishMovesBank(id) {
    LoadGif();
    $.ajax({
        url: finishMovesBankModal,
        type: "GET",
        dataType: "html",
        data: {bankMoveId: id},
        success: function (data) {
            CloseGif();
            var modal = $(data).find('#modalFinishMovesBank');
            $('body').append(modal);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}