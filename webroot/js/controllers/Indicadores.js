var chartLineProcedimeto = null;
var chartLinePaciente = null;

$(window).load(function(){

    $('.chartsGragico').each(() => {
        let chart = $(this);
        ajaxChartsGrafico(chart);
    });

    $('.chartAtendimentoGeral').each(function () {
        chartLineProcedimeto = new Chart($('#procedimentos'));
        chartLinePaciente = new Chart($('#pacientes'));
        let div = $(this).find('canvas').closest('.ibox');
        ajaxChartLineProcedimento(div);
        ajaxChartLinePaciente(div);
    });

    $('.btnAtualizaProcedimento').click(function () {
        let div = $(this).closest('.ibox');
        ajaxChartLineProcedimento(div);
    });

    $('.btnAtualizaPaciente').click(function () {
        let div = $(this).closest('.ibox');
        ajaxChartLinePaciente(div);
    });

});

/* Indicadores gerais */
var ajaxChartsGrafico = (chart) => {
    $.ajax({
        url:site_path+"/Indicadores/grafico",
        dataType:"JSON",
        type: 'POST',
        data: {
            controller: $('#controller').val()
        },
        beforeSend: function () {
            LoadGif();
        },
        success:function(txt){

            var data = {
                labels: txt.label_meses,
                datasets:[
                    {
                        label: "Atendidos",
                        data: txt.agendamento.ultimos_meses,
                        backgroundColor:"#36A2EB"
                    }
                ]
            };
            var ctx = document.getElementById("grafico");
            new Chart(ctx, {type: "line", data:data, options: {scales: {yAxes: [{ticks: {beginAtZero: true}}]}}});

            /* Atendimentos do mês atual */
            var doughnutData = {
                labels: txt.grupos,
                datasets: [{
                    data: txt.atendimento.mes_atual,
                    backgroundColor: ["#a3e1d4","#dedede","#B1B3C6", "#B9B9BD", "#C2C4DF", "#9A9FCB", "#ADB1D7", "#DEDFEC"]
                }]
            };

            var ctx4 = document.getElementById("doughnutChart").getContext("2d");
            new Chart(ctx4, {type: "doughnut", data: doughnutData, options:{responsive:true}});

            /* Atendimentos dos últimos 3 meses */
            var barData = {
                labels: txt.grupos,
                datasets: [
                    {
                        label: txt.label_meses[0],
                        backgroundColor: "rgba(220, 220, 220, 0.5)",
                        data: txt.atendimento.mes_retrasado
                    },
                    {
                        label: txt.label_meses[1],
                        backgroundColor: "rgba(26,179,148,0.5)",
                        data: txt.atendimento.mes_passado
                    },
                    {
                        label: txt.label_meses[2],
                        backgroundColor: "#dedede",
                        data: txt.atendimento.mes_atual
                    }
                ]
            };

            var ctx2 = document.getElementById("barChart").getContext("2d");
            new Chart(ctx2, {type: "bar", data: barData, options:{responsive:true}});

            /* valores */
            var dataValores = {
                labels: txt.grupos,
                datasets: [
                    {
                        label: txt.label_meses[0],
                        backgroundColor: "rgba(220, 220, 220, 0.5)",
                        data: txt.valores.mes_retrasado
                    },
                    {
                        label: txt.label_meses[1],
                        backgroundColor: "rgba(26,179,148,0.5)",
                        data: txt.valores.mes_passado
                    },
                    {
                        label: txt.label_meses[2],
                        backgroundColor: "#dedede",
                        data: txt.valores.mes_atual
                    }
                ]
            };

            var ctx3 = document.getElementById("barChartValores").getContext("2d");
            new Chart(ctx3, {type: "bar", data: dataValores, options:{responsive:true}});
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            CloseGif();
            toastr.error(msgErrorAjax);
        }
    });
};
/* Atendimento Geral */
var ajaxChartLineProcedimento = (div) => {
    $.ajax({
        url:site_path+"/Indicadores/chart-line-procedimento",
        dataType:"JSON",
        type: 'POST',
        data: {
            mes: div.find('.mes').val(),
            ano: div.find('.ano').val(),
        },
        beforeSend: function () {
            LoadGif();
        },
        success:function(txt){

            chartLineProcedimeto.destroy();
            chartchartLineProcedimetoLine = new Chart($('#procedimentos'), {
                type: 'line',
                data: {
                    labels: txt.dias,
                    datasets: txt.procedimentos.dataSets
                },
                options: {
                    title: {
                        display: true,
                        text: 'Procedimentos realizados'
                    }
                }
            });

        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            CloseGif();
            toastr.error(msgErrorAjax);
        }
    });
};
/* Atendimento Geral */
var ajaxChartLinePaciente = (div) => {
    $.ajax({
        url:site_path+"/Indicadores/chart-line-paciente",
        dataType:"JSON",
        type: 'POST',
        data: {
            mes: div.find('.mes').val(),
            ano: div.find('.ano').val(),
        },
        beforeSend: function () {
            LoadGif();
        },
        success:function(txt){

            chartLinePaciente.destroy();
            chartLinePaciente = new Chart($('#pacientes'), {
                type: 'line',
                data: {
                    labels: txt.dias,
                    datasets: txt.pacientes.dataSets
                },
                options: {
                    title: {
                        display: true,
                        text: 'Pacientes atendidos'
                    }
                }
            });

        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            CloseGif();
            toastr.error(msgErrorAjax);
        }
    });
};