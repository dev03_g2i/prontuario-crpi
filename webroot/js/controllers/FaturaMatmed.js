var openModalArtigo = function (element) {
    $(element).find('#openModalArtigo').click(function (event) {
        event.preventDefault();
        var atendProc_id = $(element).find('#atend-proc-id').val();
        var data = $(element).find('#data').val();
        if(empty(click)){
            click = 1;
            if(empty(atendProc_id)){
                required('Alerta', 'Selecione um Procedimento!');
                return false;
            }else {
                $(this).attr('data-toggle', 'modal');
                var href = $(this).attr('href');
                var new_href = href + '&atendProc_id='+atendProc_id+'&data='+data+'&first=1';
                $(this).attr('href', new_href);
            }
        }
    });
    filterTotais();
    verificaProcKit($(element).find('#atend-proc-id').val());
};

var addArtigo = function () {

    var form = $(this).closest('form').serialize();

    if (empty($('#data').val())) {
        required('Atenção', 'Informe uma data!');
        return false;
    }
    if (empty($('#artigo-id').val())) {
        required('Atenção', 'Informe um artigo!');
        return false;
    }

    $.ajax({
        type: 'POST',
        url: site_path + '/FaturaMatmed/new-artigo',
        data: form,
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if(txt != 'error'){
                var html = '<tr class="delete'+txt.id+'">';
                html += '<td>'+txt.data+'</td><td>'+txt.codigo+'</td><td>'+txt.artigo+'</td><td>'+txt.qtd+'</td>';
                html += '<td><span class="btn btn-danger btn-xs" onclick="DeletarModal(\'FaturaMatmed\', '+txt.id+')"><i class="fa fa-close"></i></span></td>';
                html += '</tr>';
                $('#show-artigos tbody').append(html);
            }
        },
        error: function () {
            CloseGif();
            toastr.error(msgErrorAjax);
        }
    });
};

var filterTotais = function () {
    var form = $('#frm-fatura-matmed').serialize();
    $.ajax({
        type: 'POST',
        url: site_path + '/FaturaMatmed/filter-totais',
        data: form,
        dataType: 'JSON',
        success: function (txt) {
            $('.total').text(formatReal(txt.total));
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

var addKitPacote = function (element) {
    if(empty(click)) {
        click = 1;
        var fatura_kit_id = $('#fatura-kit-id').val();
        var atend_proc_id = $('#atend-proc-id').val();

        if(empty(atend_proc_id)){
            required('Atenção', 'Selecione um Procedimento!');
            return false;
        }
        if(empty(fatura_kit_id)){
            required('Atenção', 'Selecione um Kit/Pacote!');
            return false;
        }
        $.ajax({
            type: 'POST',
            url: site_path + '/FaturaMatmed/add-kit-matmed',
            data: {
                fatura_kit_id: fatura_kit_id,
                atend_proc_id: atend_proc_id
            },
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif();
            },
            success: function (txt) {
                if(txt.res == 0){
                    toastr.success(txt.msg);
                    $(element).find('.btnFilterTotais').click();
                }else {
                    toastr.warning(txt.msg);
                }
            },
            complete: function () {
                CloseGif();
            },
            error: function () {
                toastr.error(msgErrorAjax);
            }
        });
    }
};

var verificaProcKit = function (proc_id) {
    $.ajax({
        type: 'POST',
        url: site_path + '/FaturaKit/verifica-proc-kit',
        data: {
            proc_id: proc_id
        },
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif();
        },
        success: function (txt) {
            $('#fatura-kit-id').empty();
            $('#fatura-kit-id').append(new Option('', ''));
            $.each(txt, function (key, value) {
                $('#fatura-kit-id').append(new Option(value, key));
            })
        },
        complete: function () {
          CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });

};

var FaturaMatmed = function (element) {
    console.log('FaturaMatmed');
    // Abre modal para adicionar artigo com a url modificada [action:index]
    openModalArtigo(element);
    // Cadastra varios artigos na modal [action:add_artigo]
    $(element).find('#btnAddArtigos').click(addArtigo);
    // Adiciona na faturaMatMed os artigos que estão no kit/pacote [action:index]
    $(element).find('#btnAddKit').click(function () {
        addKitPacote(element)
    });
    //
    $(element).find('#atend-proc-id').change(function () {
        var proc_id = $(this).val();
        verificaProcKit(proc_id);
    });
};
