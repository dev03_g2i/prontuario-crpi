//Constantes usadas pela controller
const finishAccount = '/fin-contas-pagar/finishAccount',
    quitacao = '/fin-contas-pagar/quitacao';

let total = 0, idContasPagamento = [];
//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
    .on('click', '.modal-close', function (e) {
        LoadGif();
        location.reload();
    })
    $('#search-quitacao').click(function(){
        let inicio = $('#inicio').val();
        let fim = $('#fim').val();
        let fornecedor = $('#fornecedor').val();
        let planoconta = $('#planoconta').val();
        let contabilidade = $('#contabilidade').val();
    
        let url = site_path + '/finContasPagar/quitacao';
        
        let form = '';
        form += '<input class="temp-form" type="hidden" id="inicio" name="inicio" value="' + inicio + '">';
        form += '<input class="temp-form" type="hidden" id="fim" name="fim" value="' + fim + '">';
        form += '<input class="temp-form" type="hidden" id="fornecedor" name="fornecedor" value="' + fornecedor + '">';
        form += '<input class="temp-form" type="hidden" id="planoconta" name="planoconta" value="' + planoconta + '">';
        form += '<input class="temp-form" type="hidden" id="contabilidade" name="contabilidade" value="' + contabilidade + '">';
        form += '<input class="temp-form" type="submit" id="report-submit">';
        let formulario = '<form class="temp-form" action="' + url + '" method="get">' + form + '</form>';
        $(formulario).appendTo($('.append-report'));
        $('#report-submit').click();
    })

    $('input').on('ifChecked', function (event) {
        total += parseFloat($(this).attr('valor'));
        idContasPagamento.push($(this).attr('id'));
        $('#totalSelecionado').text(total.toFixed(2));
    });

    $('input').on('ifUnchecked', function (event) {
        total -= parseFloat($(this).attr('valor'));
        let id = $(this).attr('id');
        $.each(idContasPagamento, function (index, value) {
            if (value === id) idContasPagamento.splice(index, 1);
            if (idContasPagamento.length === 0) total = 0;
        });
        $('#totalSelecionado').text(total.toFixed(2));
    });

    $('#processarContas').on('click', function (event) {
        const form = {
            bancoMovimento: $('#banco-movimento').val(),
            documento: $('#documento').val(),
            formaPagamento: $('#forma-pagamento').val(),
            dataPagamento: $('#data-pagamento').val(),
            ids: idContasPagamento
        }
        if (form.bancoMovimento === '') return alert('Selecione um movimento!');
        if (form.formaPagamento === '') return alert('Selecione uma forma de pagamento!');
        if (form.ids.length <= 0) return alert('Selecione pelo menos uma conta!');
        quitarConta(form);
    })

    $('.btnRefreshContasPagar').click(zeraTotais);
    $('.btnFiltrarContasPagar').click(calculaTotais);
    $('.btnFiltrarContasPagar').each(calculaTotais);
});

let zeraTotais = function () {
    $('.count').text('0');
    $('.total').text('R$ 0');
};

let calculaTotais = function () {
    let form = $('#contas-pagar-form').serialize();

    if (!empty(click)) {
        return false;
    }
    click = 1;
    $.ajax({
        type: 'POST',
        url: site_path + '/FinContasPagar/countContasPagar',
        data: form,
        dataType: 'JSON',
        success: function (txt) {
            $(".quantidade_reg").text(txt.count);
            $(".valor_bruto").text(formatReal(txt.valor_bruto));
            $(".valor_deducao").text(formatReal(txt.valor_com_deducao));
        }
    })
};

function quitarConta(form) {
    LoadGif();
    $.ajax({
        url: site_path + finishAccount + '.json',
        type: "get",
        data: form,
        dataType: 'JSON',
        success: function (data) {
            let tableHeader = '';
            let tableBody = '';
            $.each(data.result, function (objKey, obj) {
                tableHeader += '<tr>';
                tableBody += '<tr>';
                $.each(obj, function (attr, val) {
                    if (objKey === 0) tableHeader += '<th class="text-center">' + attr + '</th>';
                    tableBody += '<td align="center">' + val + '</td>';
                });
                tableHeader += '</tr>';
                tableBody += '</tr>';
            });
            let table = '<div id="modalDashboard" class="clearfix"><div class="table-modal-responsive" style="width: 80%; max-height: 80%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">';
            table += '<div class="col-sm-12 text-center m-b-5"><strong>Transação realizada com sucesso - Débitos efetivados</strong></div><div class="col-sm-12 text-center" style="border-bottom: thin solid lightgray"><strong>Movimentos</strong></div><table class="table table-hover" style=""><thead>' + tableHeader + '</thead>';
            table += '<tbody><tr>' + tableBody + '</tr></tbody></table></div></div>';
            $('body').append(table);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
            $("#btn-fill").trigger('click');
            total = 0;
            idContasPagamento = [];
            $('#totalSelecionado').text(total);
            CloseGif();
        },
        error: function (xhr, status) {
            CloseGif();
            return alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

//Função para carregar a table
function loadTable(url, object) {
    LoadGif();
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        data: object,
        success: function (data) {
            CloseGif();
            var result = $('<div />').append(data).find('.table-responsive');
            $(".table-responsive").html(result);
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}