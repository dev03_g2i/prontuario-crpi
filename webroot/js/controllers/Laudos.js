var assinar = function () {
    verificaAssinatura();
    BootstrapDialog.confirm({
        title: 'Atenção',
        message: "Confirma a assinatura do laudo?",
        callback: function (result) {
            if (result) {
                $('#assinar').val(1);
                $('#salvar-laudo').click();
            }
        }
    });
};

var addAnotacaoModelos = function () {
    dialog_sub = new BootstrapDialog({
        title: 'Modelo Anotação',
        message: $('<div></div>').load(site_path + "/AnotacaoModelos/add/?ajax=1&first=1", inicializar),
        closable: false,
        buttons: [{
            label: 'Cancelar',
            cssClass: 'btn-primary',
            action: function (dialogRef) {
                dialog_sub = null;
                dialogRef.close();
            }
        },
            {
                label: 'Finalizar',
                cssClass: 'btn-danger',
                action: function (dialogRef) {
                    var dialog = dialogRef.getModalBody();
                    dialog.find('#add_texto').click();
                    dialogRef.close();
                }
            }
        ]
    });
    dialog_sub.open();

    $('#add-textos').click();
};

var addTextoInAnotacao = function () {
    if (!empty($(this).val())) {
        $.ajax({
            type: 'POST',
            url: site_path + '/AnotacaoModelos/getModelo/'+$(this).val(),
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif()
            },
            success: function (txt) {
                var value = $("#anotacao-laudo").val();
                $("#anotacao-laudo").val(value +' '+ txt);
            },
            complete: function () {
                CloseGif();
            },
            error: function () {
                toastr.error(msgErrorAjax);
            }
        });
    }
};

var iniciarFinalizarExame = function () {
    $.ajax({
        type: 'POST',
        url: site_path + '/Laudos/inicarFinalizarExame/',
        dataType: 'JSON',
        data: {
            id: $(this).attr('data-id'),
            column: $(this).attr('column')
        },
        success: function (txt) {
            if(txt.res == 1){
                toastr.success(txt.msg);
                recarregar();
            }
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

var imprimirLaudos = function (event) {
    event.preventDefault();
    $('#imprimir-laudo').val(1);
    var verifica_laudo = $('#print-laudo').attr('verifica-laudo');
    if(verifica_laudo != ''){
        var href = $('#print-laudo').attr('href');
        $('#salvar-laudo').click();
        window.open(href, '_blank');
    }
};

var showPeriodos = function () {
    if($(this).val() == 4) {// Periodos
        $('#periodos').show();
        $('#periodos .col-md-4').css({'border': '1px solid red', 'border-radius': '5px'});
    }else{
        $('#periodos').hide();
    }
};

var countLaudos = function () {
    var form = $(this).closest('.filtros').find('form').serialize();
    $.ajax({
        type: 'GET',
        url: site_path + '/Laudos/countLaudos/',
        dataType: 'JSON',
        data: form,
        beforeSend: function () {
            LoadGif()
        },
        success: function (txt) {
            $('.btn-laudo-aguardando').text(txt.aguardando);
            $('.btn-laudo-exame').text(txt.exame);
            $('.btn-laudo-liberado').text(txt.liberado);
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

var carregarModelos = function (seletor_tipo, controller, action, search) {

    $(seletor_tipo).each(function () {
        if(!empty($(this).val())){
            ajaxGetModelo($(this).val());
        }
        $(this).change(function () {
            if(!empty($(this).val())){
                ajaxGetModelo($(this).val());
            }
        })
    });

    function ajaxGetModelo(grupo_id){
        $.ajax({
            type: 'POST',
            url: site_path + '/'+controller+'/'+action+'/',
            data: {
                id: grupo_id,
                search: search
            },
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif()
            },
            success: function (txt) {
                $("#modelo").empty().append('<option value="">Selecione</option>').find('option:first');
                $.each(txt, function (key, value) {
                    $('#modelo').append($("<option/>", {value: value.id, text: value.codigo}));
                })
            },
            complete: function () {
                CloseGif();
                $("#modelo").attr('load-change', 'grupo');
            },
            error: function () {
                toastr.error(msgErrorAjax);
            }
        });
    }

};

var addModeloCkeditor = function (controller, action, search) {
    if (!empty($('#modelo').val())) {

        $.ajax({
            type: 'POST',
            url: site_path + '/'+controller+'/'+action+'/',
            data: {
                id: $('#modelo').val(),
                search: search
            },
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif()
            },
            success: function (txt) {
                let editor = CKEDITOR.instances.texto;
                let $cursor = editor.getSelection();
                let fullText = '';
                let text = $('<textarea/>').html(editor.getData()).text();
                if (editor.getData() != '') {
                    fullText = `${txt.texto_html}`;
                } else {
                    fullText = `${txt.texto_html}`;
                }
                if ($cursor == 2)
                    $.when(editor.setData(fullText));
                else
                    $.when(editor.insertHtml(fullText));
            },
            complete: function () {
                CloseGif();
            },
            error: function () {
                toastr.error(msgErrorAjax);
            }
        });

    }
};

var verificaAssinatura = function (event) {
    let laudo_reedicao = $('#laudo-reedicao').val();
    /* verifica se o campo laudo_reedicao estiver marcado como 1, deixa assinar o laudo mais de uma vez */
    if(laudo_reedicao == 0){
        if(!empty($('#assinado-por').val())){
            toastr.error('Este Laudo já foi asssinado!');
            event.preventDefault();
        }
    }else{
        return true;
    }
};

var Laudos = function (element) {

    console.log('Laudos.js');
    // Carrega os modelo de acordo com o tipo [action:laudar]
    carregarModelos('#grupo', 'Textos', 'getModelo', 'all');
    // Adiciona os modelos no ckeditor [action:laudar]
    $(element).find('#btnAddModelo').click(function (event) {
        event.preventDefault();
        $('#add-textos').click();
        addModeloCkeditor('Textos', 'getModelo', 'get')
    });
    // Asssina um Laudo  [action:laudar]
    $(element).find('#btn_assinar').click(assinar);
    // Cadastra um novo modelo anotações [action:edit]
    //$(element).find('#mais_textos').click(addAnotacaoModelos);
    // Adiciona um texto no campo anotacoes [action:edit]
    $(element).find('#textos').change(addTextoInAnotacao);
    // Inicia ou finaliza um exame [action:edit]
    $(element).find('.inicarFinalizarExame').click(iniciarFinalizarExame);
    // Imprimi um laudo [action:laudar]
    $(element).find('#print-laudo').click(function (event) {
        imprimirLaudos(event);
    });
    // Ao selecionar o filtro periodos, mostra a data inicial e final [action:index]
    $(element).find('#filtro-atalho').change(showPeriodos);
    // Ao pesquisar no filtro, mostrar os contadores
    $(element).find('.btn-count-laudos, #btn-refresh').click(countLaudos);
    $(element).find('#salvar-laudo').click(function (event) {
        verificaAssinatura(event);
    });
};