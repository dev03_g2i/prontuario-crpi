
var validarAgendamento = function (event) {
    event.preventDefault();
    var frm_agenda = $('#frm-agenda');
    var inicio = frm_agenda.find('#inicio');
    var fim = frm_agenda.find('#fim');
    var grupo_id = frm_agenda.find('#grupo-id');
    var id_form = frm_agenda.attr('id');
    var agenda_id = frm_agenda.find('#agenda-id').val();
    var convenio_id = frm_agenda.find('#convenio-id');
    var cliente_id = frm_agenda.find('#cliente_id');
    var cliente_prov = frm_agenda.find('#nome-provisorio');
    var tel_prov = frm_agenda.find('#fone-provisorio');

    if (empty(cliente_id.val()) && empty(cliente_prov.val())) {
        required('Atenção', 'Selecione um cliente cadastrado ou um nome previsório!');
        return false;
    }
    if (empty(frm_agenda.find('#tipo-id').val())) {
        required('Atenção', 'Selecione o tipo de agendamento!');
        return false;
    }
    if (empty(grupo_id.val())) {
        required('Atenção', 'Informe uma agenda!');
        return false;
    }
    if (empty(frm_agenda.find("#situacao-agenda-id").val())) {
        required('Atenção', 'Selecione a situação do agendamento!');
        return false;
    }
    if (empty(convenio_id.val())) {
        required('Atenção', 'Informe um convênio!');
        return false;
    }
    if (empty(inicio.val())) {
        required('Atenção', 'Informe o periodo Inicio!');
        return false;
    }
    if (empty(fim.val())) {
        required('Atenção', 'Informe o periodo Fim!');
        return false;
    }

    var d1 = new Date(inicio.val());
    var d2 = new Date(fim.val());
    if(d2.getTime() <= d1.getTime()){
        required('Atenção', 'A data/hora fim não pode ser Menor ou Igual a data/hora inicio!');
        return false;
    }

    var validation_agenda = validationAgenda(frm_agenda.find('#grupo-id').val(), convertDatetime(frm_agenda.find('#inicio').val()), convertDatetime(frm_agenda.find('#fim').val()), agenda_id);
    validation_agenda.success(function (txt) {
        CloseGif();
        if(!empty(txt)){
            BootstrapDialog.alert({
                title: 'Atenção',
                type: BootstrapDialog.TYPE_DANGER,
                message: txt.msg
            });
            return false;
        }else{
            $(this).attr('type', 'submit');
            $('form#'+id_form).ajaxForm({
                beforeSubmit: function () {
                    LoadGif()
                },
                success: function (txt) {
                    CloseGif();
                    $('.modal-content').find('button.close').click();
                }
            }).submit();
        }
    });
};

var showOrientacoes = function () {
    var procedimentos = $('.select-procedimento').val();// Array

    if(empty(procedimentos)){
        required('Atenção', 'Selecione pelo menos um procedimento!');
        return false;
    }

    dialog_sub = new BootstrapDialog({
        title: 'Orientações',
        message: $('<div></div>').load(site_path + "/AgendaCadprocedimentos/list_orientacao/?procedimentos="+procedimentos+"&ajax=1", inicializar),
        closable: false,
        buttons:
            [
                {
                label: 'Fechar',
                cssClass: 'btn-primary',
                action: function (dialogRef) {
                    dialogRef.close();
                }
            }
        ]
    });

    if(!empty(click)){
        return false;
    }
    click = 1;
    dialog_sub.open();
};

var transferirHorario = (event, href, action) => {
    event.preventDefault();
    BootstrapDialog.show({
        title: 'Atenção',
        message: 'Tem certeza que deseja '+action+' este agendamento?',
        buttons: [{
            label: 'Não',
            cssClass: 'btn-default',
            action: function(dialog) {
                dialog.close();
            }
        }, {
            label: 'Sim',
            cssClass: 'btn-success',
            action: function(dialog) {
                $.ajax({
                    type: 'POST',
                    url: href,
                    dataType: 'JSON',
                    data: {
                        action: action
                    },
                    beforeSend: function () {
                        LoadGif();
                    },
                    success: function (txt) {
                        if(txt.error == 0)
                            toastr.success(txt.msg);
                        else
                            toastr.error(txt.msg);

                        $('#btn-fill').click();
                    },
                    complete: function () {
                        CloseGif();
                    },
                    error: function () {
                        CloseGif();
                        toastr.error(msgErrorAjax);
                    }
                });
                dialog.close();
            }
        }]
    });
};

var addTransferencia = function (eventAction) {
    $('.qtip-close ').click();
    if(!empty(transferencia)){
        $('#external-events').fadeIn('slow');
        let title_event = (eventAction == 'transferir') ? 'Horário para transferencia' : 'Copiar dados';
        $('.external-event-title').text(title_event);
        let html = '<h5>'+transferencia.grupo_agenda+'</h5>';
        html += '<a class="fc-event" style="background-color: '+transferencia.color+'">';
        html += '<div class="fc-content">';
        html += '<div class="fc-time"><span>'+transferencia.data_full+'</span></div>';
        html += '<div class="fc-title">'+transferencia.title+'</div>';
        html += '</div>';
        html += '</a>';
        html += (eventAction == 'transferir') ? '<input type="hidden" id="inputEvent" value="transferir">' : '<input type="hidden" id="inputEvent" value="copiar">';
        $('#external-events-listing').html(html);
        $('#external-events-listing a').each(function() {
            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0,  //  original position after the drag
            });
        });
    }
};

var addEncaixe = (btn) => {
    let href = btn.attr('href');
    let grupo_id = $('#grupo-id').val();
    let newHref = href+'?group='+grupo_id;
    btn.attr({'href': newHref, 'data-toggle': 'modal'});
};

var copiarAgendamento = (agenda_id) => {
    $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/copiar-agendamento/'+agenda_id,
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif();
        },
        success: function (txt) {
            $('#btn-fill').click();
            /*if(txt.error == 0)
                toastr.success(txt.msg);
            else
                toastr.error(txt.msg);
            */
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            CloseGif();
            toastr.error(msgErrorAjax);
        }
    });
};

var reservarAgendamento = () => {    
    let form = $('#frm-agenda');
    let inicio = form.find('#inicio').val();
    let fim = form.find('#fim').val();
    let grupo_id = form.find('#grupo-id').val();
    $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/reservar-agendamento',
        data: {
            inicio,
            fim,
            grupo_id,
        },
        dataType: 'JSON',
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            if(data.error == 0){
                eventos(document, grupo_id, 1);
                toastr.success(data.msg);
                $('.modal-dialog .bootstrap-dialog-close-button').click();
            }else{
                toastr.error(data.msg);
            }
        },
        complete: () => {
            CloseGif();
        },
        error: () => {
            CloseGif();
            toastr.error(msgErrorAjax);
        },
    });
};

var contadores = (grupo_id, inicio) => {
    $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/contadores/',
        data: {
            grupo_id,
            inicio,
        },
        dataType: 'JSON',
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            $('form#lista-agenda').find('.count-filaespera').text(data.fila_espera);
            $('form#lista-agenda').find('.count-anotacao').text(data.anotacao);
        },
        complete: () => {
            CloseGif();
        },
        error: () => {
            CloseGif();
            toastr.error(msgErrorAjax);
        },
    });
};

var countAnotacao = (grupo_id, inicio) => {
    $.ajax({
        type: 'POST',
        url: site_path + '/AnotacaoAgendas/count-anotacao/',
        data: {
            grupo_id,
            inicio,
        },
        dataType: 'JSON',
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            $('form#lista-agenda').find('.count-anotacao').text(data.count);
        },
        complete: () => {
            CloseGif();
        },
        error: () => {
            CloseGif();
            toastr.error(msgErrorAjax);
        },
    });
};

var Agendas = function (element) {
    // Validação ao cadastrar/editar agendamento
    $(element).find('.validation-agenda').click(validarAgendamento);
    // Mostra as orientações dos procedimentos da agenda
    $(element).find('#show-orientacoes').click(showOrientacoes);
    // Finaliza a transferencia de horarios
    /*$(element).find('.btnFinalizarTransferencia').click(function (event) {
        event.preventDefault();
        let href = $(this).attr('href');
        transferirHorario(href);
    });*/
    $(element).find('#openAnotacao').click(function (event) {
        let cont = $(element).find('.count-anotacao').text();
    });

    $('form#lista-agenda, form#fill-agendamentos').find('.openFilaEspera').click(function (event) {
        event.preventDefault();
        let form = $('form#lista-agenda, form#fill-agendamentos');
        let grupo = form.find('#grupo-id').val();
        let href = site_path + '/AgendaFilaespera/index?grupo=' + grupo + '&first=1';
        $(this).attr({ 'href': href, 'data-toggle': 'modal' });
    });
    $('form#lista-agenda, form#fill-agendamentos').find('.openAnotacao').click(function (event) {
        event.preventDefault();
        let form = $('form#lista-agenda, form#fill-agendamentos');
        let grupo = form.find('#grupo-id').val();
        let start = form.find('#start, #inicio').val();
        let href = site_path + '/AnotacaoAgendas/index?grupo=' + grupo + '&data=' + start + '&first=1';
        //window.open(href, '_blank');
        $(this).attr({ 'href': href, 'data-toggle': 'modal' });
    });
    $('form#lista-agenda, form#fill-agendamentos').find('.openParticularidades').click(function (event) {
        event.preventDefault();
        let form = $('form#lista-agenda, form#fill-agendamentos');
        let grupo = form.find('#grupo-id').val();
        if (!grupo) {
            sweetError('Atenção', 'Para adicionar uma particularidade, selecione uma agenda!', 'Fechar');
            return false;
        } else {
            let href = site_path + '/GrupoAgendas/getParticularidades?grupo=' + grupo + '&ajax=1&first=1';
            //window.open(href, '_blank');
            $(this).attr({ 'href': href, 'data-toggle': 'modal', 'data-target': '#modal_lg'});
        }
    });
    
    $('form#lista-agenda, form#fill-agendamentos').find('.openBloqueioAgenda').click(function (event) {
        event.preventDefault();
        let form = $('form#lista-agenda, form#fill-agendamentos');
        let grupo = form.find('#grupo-id').val();
        if (!grupo) {
            sweetError('Atenção', 'Para adicionar um bloqueio, selecione uma agenda!', 'Fechar');
            return false;
        } else {
            let href = site_path + '/agenda-bloqueios/index?grupo_agenda_id=' + grupo;
            $(this).attr({ 'href': href, 'data-toggle': 'modal'});
        }
    });

    $(element).find('.horarios-especiais').click(function (e) {
        e.preventDefault();

        const url = $(this).attr('href');
        const grupo_id = $(element).find('#grupo-id').val();

        if (!grupo_id) {
            sweetError('Atenção', 'Informe o campo Agenda!', 'Ok'); return;
        }

        window.open(`${url}/${grupo_id}`, '_blank');
    })
    /**
     *  Bloqueia os campos do visualizaçoã da agenda - view.ctp
     */
    $(element).find('#frm-view-agenda').each(function () {
        $(this).find('input, select, textarea').attr('disabled', true);
    });

    $(element).find('#btnCancelarTransferencia').click(function () {
        transferencia = null;
        $('#external-events').fadeOut('slow');
    });

    /**
     *  Adiciona um horário de encaixe no calendario, mesmo botão da agenda/lista - new-add
     */
    $(element).find('#btnEncaixe').click(function (event) {
        event.preventDefault();
        addEncaixe($(this));
    });

    $(element).find('#reservar-agendamento').click(reservarAgendamento);

    $('form#lista-agenda').find('#grupo-id, #start').on('change blur', function(){
        contadores($('#grupo-id').val(), $('#start').val());
    });
    $('form#lista-agenda').find('.openFilaEspera').click(function(event){
        event.preventDefault();
        let form = $('form#lista-agenda');
        let grupo = form.find('#grupo-id').val();
        let href = site_path + '/AgendaFilaespera/index?grupo='+grupo+'&first=1';            
        $(this).attr({'href': href, 'data-toggle': 'modal'});
    });
    $('form#lista-agenda').find('.openAnotacao').click(function(event){
        event.preventDefault();
        let form = $('form#lista-agenda');
        let grupo = form.find('#grupo-id').val();
        let start = form.find('#start').val();
        let href = site_path + '/AnotacaoAgendas/index?grupo='+grupo+'&data='+start+'&first=1';            
        //window.open(href, '_blank');
        $(this).attr({'href': href, 'data-toggle': 'modal'});
    });

};
var contadores = (grupo_id, inicio) => {
    $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/contadores/',
        data: {
            grupo_id,
            inicio,
        },
        dataType: 'JSON',
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            $('form#lista-agenda, form#fill-agendamentos').find('.count-filaespera').text(data.fila_espera);
            $('.count-anotacao').text(data.anotacao);
            $('.count-particularidades').text(data.qtd_particularidades);
            constroiModalFlutuante('floating-anotacoes', data.anotacoes, data.mostra_avisos_agenda_aberto)
            constroiModalFlutuante('floating-particularidades', data.particularidades, data.mostra_avisos_agenda_aberto)
        },
        complete: () => {
            CloseGif();
        },
        error: () => {
            CloseGif();
            sweetToastError(msgErrorAjax);
        },
    });
};

/**
 * Função responsável por abrir e fechar as modais da Agendas - Lista
 * @param {0 = anotacoes, 1 = particularidades} index representa o botão clicado
 */
 var checkFloatingButtons = (index) => {
    if (index == 0) {
        var modal = $('.floating-anotacoes')
    } else {
        var modal = $('.floating-particularidades')
    }
    // se a modal não tem dado, não faz sentido tentar abrir ou fechar
    if (!$(modal).hasClass('no-data')) {
        if (modal.hasClass('d-none')) {
            modal.removeClass('d-none')
        } else {
            modal.addClass('d-none')
        }
    }
}

function constroiModalFlutuante(paramModal, paramArray, mostra_avisos_agenda_aberto) {
    var body = '';
    var grupoAtual = 0;
    paramArray.forEach(element => {
        // verifica se os itens terminaram para iniciar outra DIV
    // isso é feito através do ID atual do GrupoAgenda
    if (grupoAtual != element.grupo_id) {
        if (grupoAtual != 0) {
            body += '</div>'
        }
        body += '<div>'
        // para anotacoes, o nome da agenda está dentro de Grupo Agenda
        if (paramModal == 'floating-anotacoes')
            body += "<h3>" + element.grupo_agenda.nome + '</h3>'
        else
            body += "<h3>" + element.nome + '</h3>'
        }
        body += "<p>" + element.descricao + '</p>'
        grupoAtual = element.grupo_id
    });
    // verifica se tem conteúdo na modal. 
    // Se não tem, a modal é fechada e seu clique é desativado
    if (body.length > 0) {
        $('.' + paramModal).html(body)
        $('.' + paramModal).removeClass('no-data')
        if (mostra_avisos_agenda_aberto) {
            $('.' + paramModal).removeClass('d-none')
        }
    } else {
        $('.' + paramModal).addClass('no-data')
        $('.' + paramModal).addClass('d-none')
    }
    return body;
}