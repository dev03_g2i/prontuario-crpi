var reports = function () {
        if (!empty(click)) {
            return false;
        }
        click = 1;

        var controller = $(this).attr('controller');
        var action = $(this).attr('action');
        $.ajax({
            type: 'POST',
            url: site_path + '/' + controller + '/' + action,
            data: $('#frm-produtividade').serialize(),
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif();
            },
            success: function (txt) {
                if (txt.result == 'success') {
                    var url = site_path + '/Relatorios/reportProdutividade/?data_inicio=' + $('#data-inicio').val() + '&data_fim=' + $('#data-fim').val() + '&relatorio=' + $('#relatorio').val();
                    window.open(url, '_blank');
                }
            },
            complete: function () {
                CloseGif();
                click = null;
            }
        });

    }
;

var resetContadores = function () {
    $('.contagem').text('0');
    $('.resetTotais span').text('R$ 0');
    $('#btnReceberTodos').removeAttr('data-toggle data-target');
    $('#btnReceberTodos').attr({'onclick': 'return false', 'disabled': 'true'});
};

var salvarTemp = function (element) {
    $(element).find('.btnSalvarTemp').on('click', function () {
        if (!empty(click)) {
            return false;
        }
        click = 1;

        if (empty($(element).find('#profterolens').val())) {
            toastr.error("Selecione uma unidade!");
            return false;
        }

        var controller = $(this).attr('controller');
        var action = $(this).attr('action');
        var id_form = $(this).parent().parent().attr('id');

        $.ajax({
            type: 'POST',
            url: site_path + '/' + controller + '/' + action,
            data: $('form#' + id_form).serialize(),
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif();
            },
            success: function (txt) {
                CloseGif();
                $(element).find('form#' + id_form).each(function () {
                    var caminho = $(this).attr('action');
                    var form = $(this).serialize();
                    $("div.table-responsive").loadNewGrid(caminho, form, null, carregar);
                    $(element).find('.contagem').text(txt.count);
                    $(element).find('.valor_fatura').text(formatReal(txt.valor_fatura));
                    $(element).find('.valor_caixa').text(formatReal(txt.valor_caixa));
                    $(element).find('.valor_prod_fatura').text(formatReal(txt.valor_prod_fatura));
                    $(element).find('.valor_prod_caixa').text(formatReal(txt.valor_prod_caixa));
                    $(element).find('.valor_prod_convenio').text(formatReal(txt.valor_prod_convenio));
                    $(element).find('.valor_prodclin_recebimento').text(formatReal(txt.valor_prodclin_recebimento));
                    $(element).find('.valor_recebido').text(formatReal(txt.valor_recebido));
                    $(element).find('.valor_prodcaixa_clinica').text(formatReal(txt.valor_prodcaixa_clinica));
                });
                if (txt.count > 0) {
                    $(element).find('#btnReceberTodos').removeAttr('disabled onclick');
                    $(element).find('#btnReceberTodos').attr({'data-toggle': 'modal', 'data-target': '#modal_lg'});
                }
            },
            error: function () {
                CloseGif();
                toastr.error(msgErrorAjax);
            }
        });
    });
};

var ProdutividadeProfissionais = function (element) {
    console.log('ProdutividadeProfissionais.js');
    // Salva na tabela temp_produtividade_profissionais de acordo com o filtro [action:listar]
    salvarTemp(element);
    // Reseta a contagem de atendimentos
    $(element).find('.btnRefresh').on('click', resetContadores);
    $(element).find('.gerarReport').on('click', reports);
};
