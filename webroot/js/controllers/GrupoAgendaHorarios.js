
var getHorarioGrupoAgenda = function (grupo_id) {
    $.ajax({
        type: 'POST',
        url: site_path + '/GrupoAgendas/get-horarios/'+grupo_id,
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif();
        },
        success: function (txt) {
            $('#hora-inicio').val(txt.inicio);
            $('#hora-fim').val(txt.fim);
            $('#intervalo').val(txt.intervalo);
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
};

var validaHorarios = function () {
    var form = $('#frm-gerar-horario');
    if(empty(form.find('#operacao').val())){
        toastr.warning('Informe uma operação!');
        form.find('#operacao').focus();
        return false;
    }
    if(form.find('#hora-fim').val() == form.find('#hora-inicio').val()){
        toastr.warning('A hora fim não pode ser igual a hora inicio!');
        form.find('#hora-fim').focus();
        return false;
    }

    var checkDiasSemana = form.find('input:checkbox[name="dias_semana[]"]:checked').length > 0;
    if(checkDiasSemana === false){
        toastr.warning('Selecione pelo menos 1 dia da semana!');
        form.find('.dias-semana').css({'border': '1px solid red', 'padding': '15px'});
        return false;
    }else{
        form.ajaxForm({
            beforeSubmit: function () {
                loadGifHorarios();
            },
            success: function (txt) {
                closeGifHorarios();
                if(txt == 0){
                    let urlForm = form.attr('action');
                    let urlRedirect = urlForm.replace('add', 'index');
                    window.location.href = urlRedirect
                    toastr.success('Operação realizada com sucesso!');
                }
            }
        }).submit();
    }
};

var changeOperacao= function () {
    if($(this).val() == 2){// Bloquear
        $('#nome-provisorio').parent().parent().removeClass('hidden');
        $('#tipo-id').parent().parent().addClass('hidden');
    }else{
        $('#nome-provisorio').parent().parent().addClass('hidden');
        $('#nome-provisorio').attr('required', false);
        $('#tipo-id').parent().parent().removeClass('hidden');
    }
};

var loadGifHorarios = function () {
    let html = '';
    html += '<div class="load-gif loadGifHorarios">';
    html += '<h1>Realizando operação, por favor aguarde...</h1>';
    html += '<img src="http://www.nvidia.com/docs/IO/151309/loader.gif">';
    html += '</div>';
    $('body').append(html);
};
var closeGifHorarios = function () {
    $('.load-gif').remove()
};

var GrupoAgendaHorarios = function (element) {
    // Valida o formulario de gerar horararios [action:add]
    $(element).find('#btnSalvarHorarios').click(validaHorarios);
    // Traz os horarios e o intervalo de cada medico [action:add]
    $(element).find('form#frm-gerar-horario #grupo-id').each(function () {
        getHorarioGrupoAgenda($(this).val());
        $(this).change(function () {
            getHorarioGrupoAgenda($(this).val());
        });
    });
    // Ao trocar a operação e for boqueio, mostrar input nome provisorio
    $(element).find('#operacao').change(changeOperacao);
};