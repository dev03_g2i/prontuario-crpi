//Constantes usadas pela controller
const searchForOptions = '/fin-contas-pagar/searchForOptions/',
    getCreditors = '/fin-contas-pagar/getCreditors',
    finishAccount = '/fin-contas-pagar/finishAccount',
    getChartOfProvider = site_path + '/fin-contas-pagar/getChartOfProvider';

let total = 0, idContasPagamento = [];
//Função js nativo para identificar quando a tela foi carregada, e então executar comandos jquery
window.addEventListener("load", function () {
    $(document)
    //Função de pesquisa da index
        .on('click', '#pesquisar', function () {
            event.preventDefault();
            let value = $('#pesquisar').attr('id');
            let data = {
                inicio: $('#data').val(),
                fim: $('#vencimento').val(),
                planoconta: $('#planoconta-id').val(),
                fornecedor: $('#fornecedor-id').val(),
                contabilidade: $('#contabilidade-id').val(),
                situacao: $('#situacao').val(),
                status_conta: $('#status-conta').val()
            };
            let url = getUrlPath(value);
            if (url !== null) loadTable(url, data);
        })
        .on('click', '.modal-close', function(e) {
            LoadGif();
            location.reload();
        })
        //Função para alterar o valor líquido
        .on('keydown', '#valor-bruto, #multa, #juros, #desconto', function (event) {
            setTimeout(function () {
                let value = $('#valor-bruto').val() != '' ? parseFloat($('#valor-bruto').val()) : 0;
                let fine = $('#multa').val() != '' ? parseFloat($('#multa').val()) : 0;
                let interest = $('#juros').val() != '' ? parseFloat($('#juros').val()) : 0;
                let deduction = $('#desconto').val() != '' ? parseFloat($('#desconto').val()) : 0;
                let total = value + fine + interest - deduction;
                $('#valor').val(total.toFixed(2));
            }, 300);
        });

    $('#fin-fornecedor-id').change(function() {
        if (window.location.href.includes('add')) {
            let id = $('#fin-fornecedor-id').val();
            loadChartAccountOfProvider(id);
        }
    });

    $('input').on('ifChecked', function(event){
        total += parseFloat($(this).attr('valor'));
        idContasPagamento.push($(this).attr('id'));
        $('#totalSelecionado').text(total.toFixed(2));
    });

    $('input').on('ifUnchecked', function(event){
        total -= parseFloat($(this).attr('valor'));
        let id = $(this).attr('id');
        $.each(idContasPagamento, function( index, value ) {
            if (value === id) idContasPagamento.splice(index, 1);
            if (idContasPagamento.length === 0) total = 0;
        });
        $('#totalSelecionado').text(total.toFixed(2));
    });

    $('#processarContas').on('click', function(event) {
        const form = {
            bancoMovimento: $('#banco-movimento').val(), 
            documento: $('#documento').val(), 
            formaPagamento: $('#forma-pagamento').val(),
            ids: idContasPagamento
        }
        if (form.bancoMovimento === '') return alert('Selecione um movimento!');
        if (form.formaPagamento === '') return alert('Selecione uma forma de pagamento!');
        if (form.ids.length <= 0) return alert('Selecione pelo menos uma conta!');
        quitarConta(form);
    })

    $('#gerar-report').click(geraRelatoriosContasPagar);
    $('.btnRefreshContasPagar').click(zeraTotais);
    $('.btnFiltrarContasPagar').click(calculaTotais);
    $('.btnFiltrarContasPagar').each(calculaTotais);
});

let zeraTotais = function () {
    $('.count').text('0');
    $('.total').text('R$ 0');
};

let calculaTotais = function () {
    let form = $('#contas-pagar-form').serialize();

    if (!empty(click)) {
        return false;
    }
    click = 1;
    $.ajax({
        type: 'POST',
        url: site_path + '/FinContasPagar/countContasPagar',
        data: form,
        dataType: 'JSON',
        success: function (txt) {
            $(".quantidade_reg").text(txt.count);
            $(".valor_bruto").text(formatReal(txt.valor_bruto));
            $(".valor_deducao").text(formatReal(txt.valor_com_deducao));
        }
    })
};

let geraRelatoriosContasPagar = function () {

    let url = site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action');
    let dados = $('#contas-pagar-form').serializeArray();
    let form = '';
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    let formulario = '<form class="temp-form" action="' + url + '" method="POST" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    click = null;
};

function quitarConta (form){
    LoadGif();
    $.ajax({
        url: site_path + finishAccount + '.json',
        type: "get",
        data: form,
        dataType: 'JSON',
        success: function (data) {
            if (data.result.length === 0) {
                CloseGif();
                return alert("Ops, esta data não possuí dados!");
            }
            let tableHeader = '';
            let tableBody = '';
            $.each(data.result, function (objKey, obj) {
                tableHeader += '<tr>';
                tableBody += '<tr>';
                $.each(obj, function (attr, val) {
                    if (objKey === 0) tableHeader += '<th class="text-center">'+ attr +'</th>';
                    tableBody += '<td align="center">'+ val +'</td>';
                });
                tableHeader += '</tr>';
                tableBody += '</tr>';
            });
            let table = '<div id="modalDashboard" class="clearfix"><div class="table-modal-responsive" style="width: 80%; max-height: 80%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">';
            table += '<div class="col-sm-12 text-center m-b-5"><strong>Transação realizada com sucesso - Débitos efetivados</strong></div><div class="col-sm-12 text-center" style="border-bottom: thin solid lightgray"><strong>Movimentos</strong></div><table class="table table-hover" style=""><thead>'+tableHeader+'</thead>';
            table += '<tbody><tr>'+tableBody+'</tr></tbody></table></div></div>';
            $('body').append(table);
            $('<div/>').addClass('modal-background modal-close').appendTo('body');
            $( "#btn-fill" ).trigger('click');
            total = 0;
            idContasPagamento = [];
            $('#totalSelecionado').text(total);
            CloseGif();
        },
        error: function (xhr, status) {
            CloseGif();
            return alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

//Função para pegar a url de acordo com a requisição
function getUrlPath(value) {
    let url = '';
    if (value == 'pesquisar') {
        url = site_path + searchForOptions;
    } else if (value == 'pesquisar') {
        url = site_path + getCreditors;
    } else {
        url = null;
    }
    return url;
}

//Função para carregar a table
function loadTable(url, object) {
    LoadGif();
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        data: object,
        success: function (data) {
            CloseGif();
            var result = $('<div />').append(data).find('.table-responsive').html();
            $(".table-responsive").parent().html(result);
        },
        error: function (xhr, status) {
            CloseGif();
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

//Função para pegar credores
function loadCreditors(url) {
    $.ajax({
        url: url,
        type: "GET",
        dataType: "html",
        data: object,
        success: function (data) {
            var result = $('<div />').append(data).find('.table-responsive').html();
            $(".table-responsive").html(result);
        },
        error: function (xhr, status) {
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}

//Função para pegar o plano de contas do fornecedor e plotar ao selecionar um na tela de add contas a pagar.
function loadChartAccountOfProvider(id)
{
    $.ajax({
        url: getChartOfProvider + '.json',
        type: "GET",
        data: {id: id},
        success: function (data) {
            $("#fin-plano-conta-id").append($('<option>', { 
                value: data.planoconta.id,
                text: data.planoconta.nome
            }));
            $('#select2-fin-plano-conta-id-container').attr('title', data.planoconta.nome);
            $('#select2-fin-plano-conta-id-container').text(data.planoconta.nome);
        },
        error: function (xhr, status) {
            alert("Ops, tivemos um problema, tente novamente ou entre em contato conosco!");
        }
    });
}