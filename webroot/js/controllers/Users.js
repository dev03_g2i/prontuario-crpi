var Users = function(element) {
    console.log('Users.js');
    $(element).find('#grupo_id_users').each(function() {
        togglePermitePuxarCaixar();
    });
    $(element).find('#grupo_id_users').change(function() {
        togglePermitePuxarCaixar();
    });
};

/**
 * Método para esconder o campo de caixa_outros_usuarios caso o grupo de usuário não seja adminitrador (!= 1)
 */
var togglePermitePuxarCaixar = function() {
    var grupoPrcedimentoId = $('#grupo_id_users').val();

    if (grupoPrcedimentoId != 1) {
        $("#caixa_usuarios_input").val(0);
        $("#div_caixa").fadeOut();
    } else {
        $("#div_caixa").fadeIn();
    }
};
/**
 * 1ª Documentação - 16/04/2020, por Mateus Ragazzi.
 * Atualiza o status do acesso a agenda via AJAX.
 * Caso o AJAX, não dê erro, atualiza os botões e textos.
 * Utilizado em UserGrupoAgenda/index, que é invocado pelo menu "Agendas Habilitadas" em Users/index.
 * 
 * @param {Element} user_agenda_id indica o ID do registro UserGrupoAgenda, 
 *                                 que dá/retira permissão do acesso a determinada agenda.
 */
const atualizaAcessoRelCaixa = (botao, user_id, relatorio_id) => {
    $.ajax({
        url: site_path + '/users/habilita-caixa-por-usuario/' + user_id,
        type: 'POST',
        data: {
            relatorio_id: relatorio_id
        },
        dataType: 'JSON',
        success: (data) => {
            if (data.resposta) {
                sweetSuccess('Atenção', 'Registro atualizado!', 'Fechar');
                if (botao.hasClass('btn-primary')) {
                    // se o botão tiver ativo, desativa ele
                    botao.removeClass('btn-primary');
                    botao.addClass('btn-danger');
                    botao.text(' Retirar acesso');
                    // texto.text('Possui acesso');
                } else {
                    // se o botão estiver desativo, ativa ele
                    botao.removeClass('btn-danger');
                    botao.addClass('btn-primary');
                    botao.text(' Liberar acesso');
                    // texto.text('Não possui acesso');
                }
            } else {
                sweetError('Atenção', 'Não foi possível atualizar o registro. Tente novamente!', 'Fechar');
            }
        },
        error: () => {
            sweetError('Atenção', 'Não foi possível atualizar o registro. Tente novamente!', 'Fechar');
        }
    });
}