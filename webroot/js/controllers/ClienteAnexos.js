var compararImagens = function (ids, position) {
    var width = $(window).width() / 2;
    var height = $(window).height();
    window.open(site_path + '/cliente-anexos/compare-image/' + ids[0] + '?position='+position[0], '_blank', 'fullscreen=yes,toolbar=yes,top=0,bottom=0,left='+width+',width='+width+',height='+height);
    window.open(site_path + '/cliente-anexos/compare-image/' + ids[1] + '?position='+position[1], '_blank', 'fullscreen=yes,toolbar=yes,top=0,bottom=0,left=0,width='+width+',height='+height);
};

var clearClassSelected = function () {
    $('img').each(function () {
        $(this).removeClass('img-selected');
    });
};

var abrirGaleria = function () {
    var galeria = $(this).closest('.container-galeria');
    galeria.find('ul').addClass('lightgallery');
    $('.navbar-fixed-top').hide();

    var $lightgallery = $('.lightgallery').lightGallery();
    $lightgallery.on('onCloseAfter.lg', function(event, prevIndex, index){
        galeria.find('ul').removeClass('lightgallery');
        if($lightgallery.data('lightGallery')){
            $lightgallery.data('lightGallery').destroy(true);
            $('.navbar-fixed-top').show();
        }
    });
    galeria.find('a').click();
};

var selecionarImagens = function (element) {
    var selectedImgsArr = [];
    var position = [];
    $(element).find(".container-galeria img.tipo-image").click(function(event) {
        event.preventDefault();

        var id = $(this).attr('id');
        $(this).toggleClass('img-selected');
        selectedImgsArr.push(id);
        position.push($(this).parent().attr('position'));

        if(selectedImgsArr.length == 2){
            BootstrapDialog.show({
                title: 'Atenção',
                message: 'Deseja comparar estas imagens ?',
                buttons: [{
                    label: 'Não',
                    action: function(dialog) {
                        clearClassSelected();
                        selectedImgsArr = [];
                        position = [];
                        dialog.close();
                    }
                }, {
                    label: 'Sim',
                    action: function(dialog) {
                        clearClassSelected();
                        compararImagens(selectedImgsArr, position);
                        dialog.close();
                        selectedImgsArr = [];
                        position = [];
                    }
                }]
            });
        }
    });
};

var ClienteAnexos = function (element) {
    console.log('Cliente Anexos');
    //Seleciona as imagens [action:galeria]
    selecionarImagens(element);
    //Abre a galeria [action:galeria]
    $(element).find('.btnAbrirGalreia').click(abrirGaleria);
    $(element).find('#btnTETE').click(function () {
        BootstrapDialog.confirm({
            title: 'Atenção',
            message: "Confirma a assinatura do laudo?",
            callback: function (result) {
                if (result) {
                    $(document).find('#assinar').val(1);
                    $(document).find('#salvar-laudo').click();

                }
            }
        });
    })
};