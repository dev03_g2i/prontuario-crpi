/**
 * Created by user on 10/06/2016.
 */
$(document).ready(function () {
    $.fn.modal.Constructor.prototype.enforceFocus = $.noop;
    /*$('select').select2({
     theme: 'bootstrap',
     placeholder:'Selecione'
     });*/

     //TODO new Pagination AND new Ordenation
    $(document).on('click','.paginator a, .table thead a', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        if(url != "")
        {            
            loadContent(url);
        }
        return false;
    });

    $('.modal').on('show.bs.modal', function (event) {
        var index = last_modal();
        $(this).attr('data-modal-index',parseInt(index)+1);
        LoadGif();
    });

    $(document).on('hidden.bs.modal', function (e) {
        limpar_modais()
        click = null
        CloseGif();
        $(e.target).removeData('bs.modal');
    });

    $("#modal,#modal_2,#modal_lg").on('hidden.bs.modal', function (e) {
        limpar_modais()
        g2iNavObject.navegacao= [{"p": 1, "u": site_path}];
        CloseGif();
        click = null
        var qtde_modais = last_modal()
        if(qtde_modais==0) {

            // Manter na paginação selecionada
            if($('.pagination').html()){
                var url = $('.pagination li.active a').attr('href');
                var form = $(this).parent().parent().serialize();
                //$("div.table-responsive").loadNewGrid(url, form, null, carregar);
                if(url){
                    loadContent(url);
                }else {
                    if ($("#btn-fill").html()) {
                        $("#btn-fill").click();
                    }
                    if ($("#fill-agenda").html()) {
                        $("#fill-agenda").click();
                    }
                }
            }
        }

        $(e.target).removeData('bs.modal');
    });

    $('#modal,#modal_2,#modal_lg').on('loaded.bs.modal', function (event) {
        click = null;
        CloseGif();
        $(".qtip").hide();
        var modal = modal_ativa();
        var documento = $(modal).find('.modal-content');
        if(empty($(documento).find('.close').html())){
            var close = '<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: 10px 10px 0 0"><span aria-hidden="true">&times;</span></button>';
            $(documento).prepend(close)
        }
        initDocument(documento);

        $(documento).find('#valorbruto,#juros,#multa,#desconto').blur(function () {
            calculaValor();
        });

        $(documento).find("button:submit").click(function () {
            if (empty($(this).attr('onclick')) && empty($(this).attr('listen'))) {
                SendForm($(documento).find('form'));
            }
        })

        $(documento).find("input:submit").click(function () {
            if (empty($(this).attr('onclick'))&& empty($(this).attr('listen'))) {
                SendForm($(documento).find('form'));
            }
        })
    });

//funcao indispensavel ajusta scroll modais
    $(document).on('hidden.bs.modal', function(){
        click = null;
        $(this).find('#modal_lg').each(function(){
            if ($(this).data('modal-index')){
                if ($('#modal_lg:visible').length) {
                    $('body').addClass('modal-open');
                }
                $('body').css('overflow-y', '');
            } else {
                $('body').css('overflow-y', 'hidden');
            }
        });
        $(this).find('#modal').each(function () {
            if ($(this).data('modal-index')) {
                if ($('#modal:visible').length) {
                    $('body').addClass('modal-open');
                }
                $('body').css('overflow-y', '');
            } else {
                $('body').css('overflow-y', 'hidden');
            }
        });
    });


    $('body').on('click', 'a[data-toggle=modal]', function (event) {
        g2iNavObject.navegacao = [{"p": 1, "u": site_path}];
        event.preventDefault();
        modalharef = $(this).attr('href');
        if (modalharef.indexOf("?") == -1)
            modalharef += '?'
        modalharef += '&ajax=true';
        $(this).attr('href', modalharef);
        if (modalharef.indexOf("first") != -1) {
            Acrescentar(modalharef);
        }
        if (!$(this).attr('data-target')){
            $(this).attr('data-target', '#modal');
        }

        /*
         *  Carregar scripts js quando modal terminar de carregar a view
         * */
        if(!empty($(this).attr('load-script'))){
            loadScriptInModal($(this).attr('load-script'));
        }
    });


    $('[toggle="tooltip"]').tooltip();
    $('[toggle="popover"]').popover();

    $('[mask="cpf"]').mask('000.000.000-00', {reverse: true});
    $('[mask="fone"]').mask('(00)0000-00000');
    $('[mask="cep"]').mask('00000-000');
    $('.datepicker').mask('00/00/0000');
    $('[mask="competencia"]').mask('00-0000');
    $('[mask = "month"]').mask('00');
    $('[mask = "year"]').mask('0000');
    $('.monthNumberMask').mask('00');
    $('.yearNumberMask').mask('0000');
    $('[mask="porcentagem"]').mask('000%');
    
    $('#c70760cd-a5ce-4495-9a12-185b6e33e0d3').click(function(){
        $('#btn-fill').trigger('click');
    })
    moment.locale('pt-BR');

    $('.datepicker').datetimepicker({
        format: 'L',
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        useStrict: true,
        locale: 'pt-BR',
        showClear: true,
        allowInputToggle: true
    });

    $('.datetimepicker').datetimepicker({
        locale: 'pt-br',
        sideBySide: true,
        extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
        showTodayButton: true,
        //useStrict: true, // nas paginas de edit não aparece a data no input
        showClear: true,
        allowInputToggle: false,
        widgetPositioning: {
            horizontal: 'auto',
            vertical: 'bottom'
        }
    });
    $('.timepiker').datetimepicker({
        locale: 'pt-br',
        format: 'LT'
    });
    $("[data-list='collapse']").click(function () {
        if (empty($(this).parent().attr('class'))) {
            $(this).parent().attr('class', 'active');
            $(this).children(".fa-th-large").css({
                "-webkit-transform": " rotate(360deg)",
                "transform": " rotate(360deg)",
                "-webkit-transition": "width 2s, height 2s, -webkit-transform 2s"
            });
            $(this).parent().css({"border-left": "none", "border-right": "4px solid #19AA8D"});
        } else {
            $(this).parent().removeAttr('class');
            $(this).children(".fa-th-large").removeAttr('style');
            $(this).children(".fa-th-large").css({
                "-webkit-transform": " rotate(360reg)",
                "transform": " rotate(360reg)",
                "-webkit-transition": "width 2s, height 2s, -webkit-transform 2s"
            });
            $(this).parent().css({"border-right": "none"});
        }
    });

    $("[role='open-adm']").click(function () {
        $("[role='adm']").css({"display": "block"});
    });


    $.fn.extend({
        serializeJSON: function(exclude) {
            exclude || (exclude = []);
            return _.reduce(this.serializeArray(), function(hash, pair) {
                pair.value && !(pair.name in exclude) && (hash[pair.name] = pair.value);
                return hash;
            }, {});
        }
    });

    $.fn.extend({
        openSelect: function () {
            return this.each(function (idx, domEl) {
                if (document.createEvent) {
                    var event = new MouseEvent("mousedown");
                    domEl.dispatchEvent(event);
                } else if (element.fireEvent) {
                    domEl.fireEvent("onmousedown");
                }
            });
        },
        loadGrid: function (url, args, selector, callback, loadscript) {
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            var aux = 0;
            var acrescentar = false;
            if (!empty(args)) {
                $.each(args, function (key, value) {
                    if(key=='first' && value==1){
                        acrescentar= true;
                    }
                    if(strripos(value,' ')>0){
                        value = replaceAll(value,' ','%20');
                    }
                    if (aux == 0) {
                        url += '/?' + key + '=' + value;
                    } else {
                        url += '&' + key + '=' + value;
                    }
                    aux = 1;
                });
            }
            if(acrescentar){
                Acrescentar(url)
            }
            url += ' ' + selector;

            if(!empty(loadscript)){
                return $(this).load(url, function () {
                    loadScriptInTabs(loadscript);
                });
            }else {
                if ($.isFunction(callback)) {
                    return $(this).load(url, callback);
                } else {
                    return  $(this).load(url);
                }
            }

        },

        loadNewGrid: function (url, args, selector, callback) {
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';
            var n = url.indexOf("?");
            if (n > -1) {
                url += '&'+args;
            }else{
                url += '/?'+args;
            }
            url += ' ' + selector;
            // console.log(url);
            if ($.isFunction(callback)) {
                $(this).load(url, callback);
                console.log(url)
                return  $(this).load(url, callback);
            } else {
                return $(this).load(url);
            }
        },

        newChartPost: function(url, date) {
            $.ajax({
                url:site_path+"/Estatisticas/graficoCaixaConvenioDiario/" + date,
                dataType:"JSON",
                success:function(txt){
                    $('canvas').remove('#grafico');
                    $('#graficoDiv').append('<canvas id="grafico"></canvas>');
                    $('#mesSelecionadoGrafico').text('Valores Caixas (Dia/Dia) do mês: ' + txt.mes);
                    var data = {
                        datasets:[
                            {
                                label: txt.labels[0][0],
                                data: txt.valoresCaixa,
                                backgroundColor:"rgba(0, 0, 0, 0.5)"
                            },
                            {
                                label: txt.labels[0][1],
                                data: txt.valoresTotal,
                                backgroundColor:"rgba(26,179,148,0.5)"
                            },
                        ],
                        labels: txt.dias,
                    };
                    var ctx = document.getElementById("grafico");
                    var myChart = new Chart(ctx, {
                        type: "line",
                        data:data,
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {return number_format(value) + ',00';}
                                    }
                                }]
                            }
                        }
                    });
                }
            })
            $.ajax({
                url:site_path+"/Estatisticas/graficoCaixaConveniosUltimos30Dias/" + date,
                dataType:"JSON",
                success:function(txt){
                    $('canvas').remove('#barChartCaixa');
                    $('#barChartCaixaDiv').append('<canvas id="barChartCaixa"></canvas>');
                    var data = {
                        datasets:[
                            {
                                label: txt.labels[0][0],
                                data: txt.valoresCaixa,
                                backgroundColor:"rgba(0, 0, 0, 0.5)"
                            },
                            {
                                label: txt.labels[0][1],
                                data: txt.valoresTotal,
                                backgroundColor:"rgba(26,179,148,0.5)"
                            },
                        ],
                        labels: txt.meses,
                    };
                    var ctx = document.getElementById("barChartCaixa");        
                    var myChart = new Chart(ctx, {
                        type: "line",
                        data:data,
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {return number_format(value) + ',00';}
                                    }
                                }]
                            }
                        }
                    });
                }
            })
            $.ajax({
                url:site_path+"/Estatisticas/graficoQuantidadeAtendimentosMeses/" + date,
                dataType:"JSON",
                success:function(txt){
                    $('canvas').remove('#diaryLineChart');
                    $('#diaryLineChartDiv').append('<canvas id="diaryLineChart"></canvas>');
                    $('#mesSelecionadoDiaryLineChart').text('Atendimentos do mês: ' + txt.mes);
                    var data = {
                        datasets: [],
                        labels: []
                    };
                    txt.labels.forEach(function(label, key){
                        let group = {
                            label: txt.labels[key].nome,
                            data: txt.qtdAtendimento[key],
                            backgroundColor: "rgba(255, 255, 255, 0)",
                            borderColor: txt.labels[key].color,
                            lineTension: "0.1"
                        };
                        data.datasets[key] = group;
                    });
                    data.labels = txt.dias;
                    var ctx4 = document.getElementById("diaryLineChart");        
                    new Chart(ctx4, {
                        type: "line",
                        data:data,
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {
                                            return number_format(value) + ',00';
                                        }
                                    }
                                }]
                            }
                        }
                    });
                }
            })
            $.ajax({
                url:site_path+ "/Estatisticas/graficoAtendimentoDiario/" + date,
                dataType:"JSON",
                success:function(txt){
                    $('canvas').remove('#barChart');
                    $('#barChartDiv').append('<canvas id="barChart"></canvas>');
                    var barData = {
                        labels: txt.grupo,
                        datasets: [
                            {
                                label: txt.meses[0],
                                backgroundColor: "rgba(0, 0, 0, 0.5)",
                                data: txt.mes3
                            },
                            {
                                label: txt.meses[1],
                                backgroundColor: "rgba(26,179,148,0.5)",
                                data: txt.mes2
                            },
                            {
                                label: txt.meses[2],
                                backgroundColor: "#dedede",
                                data: txt.mes1
                            } 
                        ]
                    };
                    var barOptions = {
                        responsive: true,
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function(value) {
                                        return number_format(value) + ',00';
                                    }
                                }
                            }]
                        }
                    };
                    var ctx2 = document.getElementById("barChart").getContext("2d");
                    new Chart(ctx2, {type: "bar", data: barData, options:barOptions});
                }
            })
        },

        newAnnualChartPost: function(url, date) {
            $.ajax({
                url:site_path+"/Faturamentos/getDataForAnnualBillingChart/" + date,
                dataType:"JSON",
                success:function(txt){
                    $('#porcentualNaoFaturado').text(txt.porcentualNaoFaturado + '%');
                    $('#valorNaoFaturado').text('R$' + txt.valorNaoFaturado);
                    $('#qtNaoFaturado').text(txt.qtNaoFaturado);
                    $('canvas').remove('#annualChartLine');
                    $('#graficoDiv').append('<canvas id="annualChartLine"></canvas>');
                    var data = {
                        datasets: [],
                        labels: []
                    };
                    var rgbas = [`rgba(105, 184, 202, 0.5)`, `rgba(156, 218, 205, 0.7)`];
                    txt.labels[0].forEach(function(label, key){
                        let group = {
                            label: txt.labels[0][key],
                            data: txt.totais[key].reverse(),
                            backgroundColor: rgbas[key],
                            lineTension: "0.5"
                        };
                        data.datasets[key] = group;
                    });
                    for(i = 11; i >= 0; i--) {
                        data.labels[i] = txt.meses[0][i].label;
                    };
                    var ctx = document.getElementById("annualChartLine");        
                    new Chart(ctx, {
                        type: "line",
                        data:data,
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [{
                                    gridLines: {
                                        display: false,
                                        color: "rgba(0, 0, 0, 0)"
                                    },
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {return number_format(value) + ',00';}
                                    }
                                }],
                                xAxes: [{
                                    gridLines: {
                                        display: false,
                                        color: "rgba(0, 0, 0, 0)"
                                    }
                                }]
                            }
                        }
                    });
                }
            })
        },

        loadPostGrid: function (url, form, selector, callback) {
            var ths = this
            this.each(function () {
                $(this).html('');
                $(this).html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Carregando...</h3></div>");
            });

            if (selector == '' || selector == null)
                selector = '.table-responsive';

            var n = url.indexOf("?");
            if (n > -1) {
                url += '&';
            }else{
                url += '/?';
            }
            url += '' + selector;

            if ($.isFunction(callback)) {
                $.post(url,form, function(dados){
                    var htm = $(dados).find(selector).html();
                    ths.html(htm)
                    carregar()
                });
            } else {
                return  $.post(url,form,function(dados){
                    var htm = $(dados).find(selector).html();
                    ths.html(htm)
                    carregar()
                });
            }

        }
    });
    $.fn.serializeObject = function(){
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $.fn.modal.Constructor.prototype.enforceFocus = function() {};

    $.extend({
        ListProcedimentos: function (id) {
            $("div.edit-dep").loadGrid(site_path + '/AtendimentoProcedimentos/listar/' + id, null, '.arts', null);
        },
        ListProcedimentosAdd: function () {
            var filtro = {
                procedimento_id: '',
                convenio_id: ''
            };
            $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', itens);
        },
        FormGet: function(url, args){
            var form="";
            $.each(args, function(key, value ) {
                form += '<input type="hidden" name="'+key+'" value="'+value+'">';
            });

            $('<form action="' + url + '" method="GET" target="_blank">' + form + '</form>').submit();
        }
    });

    $("#btn-fill").click(function () {
        if(empty(filtrado)) {
            filtrado=1;
            validationFilter();
            var caminho = $(this).parent().parent()[0].action;
            var form = $(this).parent().parent().serialize();
            $("div.table-responsive").loadNewGrid(caminho, form, null, carregar);
        }
    });

    $("#btn-fill-chart").click(function () {
        if(empty(filtrado)) {
            filtrado=1;
            validationFilter();
            var caminho = $(this).parent().parent()[0].action;
            var form = $(this).parent().parent().serialize();
            form = form.split(/[&=|mes|ano]/g);
            form = form.filter(Boolean);
            date = '01-' + form[0] + '-' + form[1];
            $("div.table-responsive").newChartPost(caminho, date);
            $("div#bodyCharts").css({'visibility': 'visible', 'display': 'block'});
        }
        filtrado=0;
    });

    $("#btn-fill-annual-chart").click(function () {
        if(empty(filtrado)) {
            filtrado=1;
            validationFilter();
            var caminho = $(this).parent().parent()[0].action;
            var form = $(this).parent().parent().serialize();
            form = form.split(/[&=|mes|ano]/g);
            form = form.filter(Boolean);
            date = '01-' + form[0] + '-' + form[1];
            $("div.table-responsive").newAnnualChartPost(caminho, date);
            $("div#bodyCharts").css({'visibility': 'visible', 'display': 'block'});
        }
        filtrado=0;
    });
    
    $("#btn-report").click(function () {
        var caminho = site_path+'/'+$(this).attr('controller')+'/'+$(this).attr('action');
        var form = $(this).parent().parent().serialize();

        if(empty($(this).attr('jump'))){
            if(empty($(this).parent().parent().find('#inicio').val())){
                BootstrapDialog.alert('Selecione um período inicial!');
                return false;
            }
            if(!empty($(this).parent().parent().find('#fim').html())) {
                if (empty($(this).parent().parent().find('#fim').val())) {
                    BootstrapDialog.alert('Selecione um período final!');
                    return false;
                }
            }
        }


        if(!empty(caminho.indexOf("?")) && caminho.indexOf("?") >-1){
            caminho+= '&'+form;
        }else{
            caminho += '/?'+form;
        }
        window.open(caminho, '_blank');
    });

    $("#btn-reportDois").click(function () {
        var caminho = site_path+'/'+$(this).attr('controller')+'/'+$(this).attr('action');
        var form = $(this).closest('form').serialize();
        if(!empty(caminho.indexOf("?")) && caminho.indexOf("?") >-1){
            caminho+= '&'+form;
        }else{
            caminho += '/?'+form;
        }
        window.open(caminho, '_blank');
    });

    $("#new-fill").click(function () {
        var caminho = $(this).parent().parent()[0].action;
        var form = $(this).parent().parent().serialize();
        $("div.table-responsive").loadPostGrid(caminho, form, null, null);

    });

    $("#btn-refresh").click(function () {
        var caminho = $(this).parent().parent()[0].action;
        var fr = $(this).parent().parent()[0];
        clear_all(fr)
        var form = $(this).parent().parent().serialize();
        $("div.table-responsive").loadNewGrid(caminho, form, null, null);
    });

})

function excluir(event, seletor) {
    event.preventDefault();
    var url = $(seletor).attr('href');
    BootstrapDialog.confirm({
        title: 'Atenção!',
        message: 'Tem certeza que deseja excluir este registro?',
        type: BootstrapDialog.TYPE_DANGER,
        closable: true,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Excluir',
        btnOKClass: 'btn-danger',
        callback: function (result) {
            if (result) {
                window.location.href = url
            }
        }
    });

}

function number_format(number, decimals, dec_point, thousands_point) {

    if (number == null || !isFinite(number)) {
        throw new TypeError("number is not valid");
    }

    if (!decimals) {
        var len = number.toString().split('.').length;
        decimals = len > 1 ? len : 0;
    }

    if (!dec_point) {
        dec_point = ',';
    }

    if (!thousands_point) {
        thousands_point = '.';
    }

    number = parseFloat(number).toFixed(decimals);

    number = number.replace(",", dec_point);

    var splitNum = number.split(dec_point);
    splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
    number = splitNum.join(dec_point);

    return number;
}

function LoadGif(){
    $('.load-gif').removeClass('hidden');
}

function CloseGif(){
    $('.load-gif').addClass('hidden');
}


function clear_all(document){
    $(document).find('input').each(function () {
        $(this).val('')
    });
    $(document).find('select').each(function () {
        $(this).val('')
        $(this).trigger('change');
    });
    /*
        Resolve um bug nos campos com select2.
        Esse bug é na hora que limpa os filtros e depois tenta buscar denovo.
     */
    $(document).find('select.select2').each(function () {
        $(this).val('0');
        $(this).trigger('change');
    });
    /***********************************************************************/
    $(document).find('input[type=radio]').each(function () {
        $(this).prop('checked',  false)
    });
    $(document).find('input[type=checkbox]').each(function () {
        $(this).prop('checked',  false)
    });
    //$(document).find('.clearBtn').text(0);

}

/*
 Carrega script js de acordo com atributo load-script dos links
 */
function loadScriptInModal(controller)
{
    var src = site_path + '/js/controllers/' + controller + '.js';
    $.getScript(src, function () {
    });
}
/*
 *  Carrega script js nas nav tabs
 * */
function loadScriptInTabs(controller) {
    var src = site_path + '/js/controllers/' + controller + '.js';
    $.getScript(src, function () {
    });
}
/*
 * Valida campos requeridos nos filtros
 * Se nao tiver a class .filtros nao ira funcionar
 *
 */
var validationFilter = function () {
    $('.filtros').find('input, select').each(function (index, value) {
        if($(this).attr('required')){
            var form_group = $(value).closest('.form-group');
            var label = form_group.find('label').html();
            if($(value).val() == '' || $(value).val() == null){
                required('Atenção', 'Por favor informe o campo '+label+'!');
                return false;
            }
        }
    });
};

/** Carrega conteudo de paginação - .table-responsive
 * 
 * @param String url 
 */
var loadContent = (url) => {
    $.ajax({
        type: 'GET',
        url: url,
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            let content = $('<div>').append(data).find('.table-responsive');
            $(".table-responsive").html(content);
        },
        complete: () => {
            carregar()
            CloseGif();
        },
        error: () => {
            CloseGif();
            toastr.error(msgErrorAjax);
        },
    });
}