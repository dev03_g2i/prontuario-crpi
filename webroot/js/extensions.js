/**
 * Created by user on 17/05/2016.
 */
var extension = ["jpg", "gif", "png", "jpeg", "rar", "txt", "pdf", "doc", "docx", "xls", "csv", "potx", "pot", "ppt"];
function number_format(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
            .toFixed(prec)
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

function formatReal(n) {
    return "R$ " + parseFloat(n).toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
}

function empty(mixed_var) {
    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, '', '0'];
    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }
    if (typeof mixed_var === 'object') {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            //if (mixed_var.hasOwnProperty(key)) {
            return false;
            //}
        }
        return true;
    }
    return false;
}

function strripos(haystack, needle, offset) {
    //  discuss at: http://locutus.io/php/strripos/
    // original by: Kevin van Zonneveld (http://kvz.io)
    // bugfixed by: Onno Marsman (https://twitter.com/onnomarsman)
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //    input by: saulius
    //   example 1: strripos('Kevin van Zonneveld', 'E')
    //   returns 1: 16

    haystack = (haystack + '')
        .toLowerCase()
    needle = (needle + '')
        .toLowerCase()

    var i = -1
    if (offset) {
        i = (haystack + '')
            .slice(offset)
            .lastIndexOf(needle) // strrpos' offset indicates starting point of range till end,
        // while lastIndexOf's optional 2nd argument indicates ending point of range from the beginning
        if (i !== -1) {
            i += offset
        }
    } else {
        i = (haystack + '')
            .lastIndexOf(needle)
    }
    return i >= 0 ? i : false
}

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) != -1) {
        string = string.replace(token, newtoken);
    }
    return string;
}
function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    for (var i = 0, f; f = files[i]; i++) {
        if (!f.type.match('image.*')) {
            continue;
        }
        var reader = new FileReader();
        reader.onload = (function(theFile) {
            return function(e) {
                var span = document.createElement('span');
                span.innerHTML = ['<img class="img-thumbnail img-responsive" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/>'].join('');
                document.getElementById('list').insertBefore(span, null);
            };
        })(f);
        reader.readAsDataURL(f);
    }
}

function change_photo(form) {
    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');

    $('form#' + form).ajaxForm({
        beforeSend: function () {
            documento.find(".progress").show();
            var percentVal = '0%';
            documento.find(".percent").text(percentVal)
            documento.find("[role='progressbar']").attr('style', 'width: ' + percentVal);
        },
        uploadProgress: function (event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            documento.find(".percent").text(percentVal)
            documento.find("[role='progressbar']").attr('style', 'width: ' + percentVal);
        },
        success: function (d) {
            if (d.res == 1) {
                $(modal).modal('hide')
                $("#user-image").attr('src', site_path + '/files/users/caminho/' + d.caminho_dir + '/' + d.caminho);
            } else {
                BootstrapDialog.alert({
                    type: BootstrapDialog.TYPE_DANGER,
                    message: "Erro ao alterar imagem! \n Tente novamente mais tarde!"
                });
                return false;
            }
        }
    });
}

function Checar_senhas(form) {
    if (empty($(form).find('#password').val()) || empty($(form).find('#confirmar').val())) {
        BootstrapDialog.alert({
            type: BootstrapDialog.TYPE_DANGER,
            message: "Preencha a senha e a confirmação de senha!"
        });
        return false;
    }
    if ($(form).find('#password').val() != $(form).find('#confirmar').val()) {
        BootstrapDialog.alert({
            type: BootstrapDialog.TYPE_DANGER,
            message: "As senhas não estão iguais!"
        });
        return false;

    } else {
        $('form' + form).ajaxForm({
            beforeSubmit: function () {
                LoadGif()
            },
            success: function (txt) {
                CloseGif();
                Navegar('', 'back');
            }
        }).submit()
    }
}

function upload() {
    $('#foto').click();
}

function form_campos(element) {

    $(element).find('#foto').each(function(){
        document.getElementById('foto').addEventListener('change', handleFileSelect, false);
    })

    $(element).find('#foto').change(function() {
        $(element).find('#uploadFoto').html('<output id="list" ></output>');
    });


    $(element).find('.input-file-edit-user').each(function(){
        var link_img = $(this).attr('link-img') ? "<img src='"+site_path+"/"+$(this).attr('link-img')+"' class='file-preview-image' />" : false;
        $(this).fileinput({
            maxFileCount: 1,
            overwriteInitial:false,
            initialPreview: link_img ,
            initialPreviewConfig: [
                {caption: $(this).attr('descricao-img'), width: "120px", url: site_path+"/"+$(this).attr('controller-img')+"/delete-img/"+$(this).attr('img-id')+'/?name='+$(this).attr('name'), key: $(this).attr('img-id')},
            ],
            layoutTemplates: {
                main1: "{preview}\n" +
                "<div class=\'input-group {class}\'>\n" +
                "   <div class=\'input-group-btn\'>\n" +
                "       {browse}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>"
            },
            initialPreviewAsData:true,
            allowedFileExtensions: ["jpg", "gif", "png"]

        });
    });


    $(element).find('.input-file-edit').each(function(){
        var link_img = $(this).attr('link-img') ? "<img src='"+site_path+"/"+$(this).attr('link-img')+"' class='file-preview-image' />" : false;
        $(this).fileinput({
            maxFileCount: 20,
            overwriteInitial:false,
            initialPreview: link_img,
            initialPreviewConfig: [
                {caption: $(this).attr('descricao-img'), width: "120px", url: site_path+"/"+$(this).attr('controller-img')+"/delete-img/"+$(this).attr('img-id')+'/?name='+$(this).attr('name'), key: $(this).attr('img-id')},
            ],
            layoutTemplates: {
                main1: "{preview}\n" +
                "<div class=\'input-group {class}\'>\n" +
                "   <div class=\'input-group-btn\'>\n" +
                "       {browse}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>"
            },
            initialPreviewAsData:true,
            allowedFileExtensions: ["jpg", "gif", "png", "txt", "pdf", "rar"]

        });
    })


    $(element).find('.input-file').each(function () {
        $(this).fileinput({
            showUpload: false,
            maxFileCount: 10,
            layoutTemplates: {
                main1: "{preview}\n" +
                "<div class=\'input-group {class}\'>\n" +
                "   <div class=\'input-group-btn\'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>"
            },
            allowedFileExtensions: extension
        });
    });

    $(element).find('.input-file2').each(function () {
        $(this).fileinput({
            showUpload: false,
            initialPreview: [
                $(element).find('#img-preview').html()
            ],
            initialPreviewAsData: true,
            overwriteInitial: false,
            layoutTemplates: {
                main1: "{preview}\n" +
                "<div class=\'input-group {class}\'>\n" +
                "   <div class=\'input-group-btn\'>\n" +
                "       {browse}\n" +
                "       {upload}\n" +
                "       {remove}\n" +
                "   </div>\n" +
                "   {caption}\n" +
                "</div>"
            },

            allowedFileExtensions: ["jpg", "gif", "png", "txt", "pdf", "rar", "jpeg"]
        });
    });

    $(element).find('[data="sumer"]').each(function () {
        $(this).summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['misc', ['fullscreen']]
            ],
            height: 245,                            // set editor height
            lang: 'pt-BR'
        }).on('summernote.change', function() { // Adiciona listen f nos links
            $('.note-editor a').each(function () {
                $(this).attr('listen', 'f');
            })
        });
    });

    $(element).find("[data-gallery='gal']").each(function () {
        if (empty(click)) {
            $(this).click()
            click = 1;
        }
    });

    /*$(element).find("[data-gallery='gal']").each(function () {
        $(this).click(function (event) {
            event = event || window.event;
            var target = event.target || event.srcElement,
                link = target.src ? target.parentNode : target,
                options = {index: link, event: event},
                links = $("[data-gallery='gal']")
            blueimp.Gallery(links, options);
        })
    })*/


    $(element).find('[data-cliente="cliente"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um cliente'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/findClientes/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('[data-planocontas="planocontas"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/FinPlanoContas/fill/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('[data-bancos="bancos"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/FinBancos/fill/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('[data-fornecedores="fornecedores"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/FinFornecedores/fill/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('[data-contabilidades="contabilidades"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/FinContabilidades/fill/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('[data-grupos="grupos"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/FinGrupos/fill/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('[data-full-cliente="cliente"]').each(function () {
        $(this).select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                id: '-1',
                text: 'Selecione um cliente'
            },
            minimumInputLength: 1,
            ajax: {
                url: site_path + "/Clientes/findfull/",
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 10,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });
    $(element).find('.money').each(function () {
        var aux = $(this).val();

        if (!empty(aux)) {
            $(this).val(number_format(parseFloat(aux), 2, '.', ''))
        }
        $(this).mask('000000000000000.00', {reverse: true});
    });
    $(element).find('[mask="money"]').each(function () {
        var aux = $(this).val();

        /*if (!empty(aux)) {
            $(this).val(number_format(parseFloat(aux), 2, '.', ''))
        }*/
        $(this).mask('000000000000000.00', {reverse: true});
    });
    $(element).find('[toggle="tooltip"]').each(function () {
        $(this).tooltip();
    });
    $(element).find('[mask="cpf"]').each(function () {
        $(this).mask('000.000.000-00', {reverse: true});
    });
    $(element).find('[mask="cnpj"]').each(function () {
        $(this).mask('00.000.000/0000-00', {reverse: true});
    });
    $(element).find('[mask="fone"]').each(function () {
        $(this).mask('(00)00000-0000');
    });
    $(element).find('[mask="phone"]').each(function () {
        $(this).mask('(00)00000-0000');
    });
    $(element).find('[mask="telefone"]').each(function () {
        $(this).mask('(00)0000-00000');
    });
    $(element).find('[mask="cep"]').each(function () {
        $(this).mask('00000-000');
    });
    $(element).find('[toggle="popover"]').popover();
    moment.locale('pt-BR');
    $(element).find('.datepicker').each(function () {
        
        $(this).mask('00/00/0000');
        $(this).datetimepicker({
            format: 'L',
            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
            showTodayButton: true,
            useStrict: true,
            locale: 'pt-BR',
            showClear: true,
            allowInputToggle: true
        }).on('dp.show', function (e) {
            var widget = $('.bootstrap-datetimepicker-widget:last')
            widget.addClass('piker-widget')
        })

    });
    $(element).find('.datepicker-botton').each(function () {
        $(this).mask('00/00/0000');
        $(this).datetimepicker({
            format: 'L',
            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
            showTodayButton: true,
            useStrict: true,
            locale: 'pt-BR',
            showClear: true,
            allowInputToggle: true,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        }).on('dp.show', function (e) {
            var widget = $('.bootstrap-datetimepicker-widget:last')
            widget.addClass('piker-widget')
        })

    });

    $(element).find('.datetimepicker-botton').each(function () {
        //$(this).mask('00/00/0000');
        $(this).datetimepicker({
            locale: 'pt-br',
            sideBySide: true,
            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
            //format: 'DD/MM/YYYY HH:ii',
            showTodayButton: true,
            showClear: true,
            allowInputToggle: false,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        }).on('dp.show', function (e) {
            var widget = $('.bootstrap-datetimepicker-widget:last');
            widget.addClass('piker-widget');
        })

    });

    $(element).find('.datetimepicker').each(function () {
        $(this).datetimepicker({
            locale: 'pt-br',
            sideBySide: true,
            extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
            showTodayButton: true,
            useStrict: true,
            showClear: true,
            allowInputToggle: true,
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            }
        });
    });

    $(element).find('.timepiker').each(function () {
        $(this).datetimepicker({
            locale: 'pt-br',
            format: 'LT'
        });
    });

    //TODO carregar os options dos select2-basics ao clicar
    $(element).find('[data="select2-basic"]').each(function () {
        var form_group = $(this).closest('.form-group');
        $(this).select2({
            theme: 'bootstrap',
            placeholder:'Selecione'
        });
        $(form_group).on('click', '.select2-selection', function () {
            var select = form_group.find('select');
            // Verifica se tem um atributo controller e se não foi carregado por outro ajax
            if(!empty(select.attr('controller')) && empty(select.attr('load-change'))){
                $.ajax({
                    type: 'POST',
                    url: site_path + '/'+select.attr('controller')+'/loadSelect2/',
                    dataType: 'JSON',
                    beforeSend: function () {
                        LoadGif()
                    },
                    success: function (txt) {
                        select.empty().append('<option value="">Selecione</option>').find('option:first');
                        $.when(
                            $.each(txt, function (key, value) {
                                select.append(new Option(value, key));
                            })
                        ).then(function () {
                            select.trigger('change');
                        });
                        select.select2('close');
                    },
                    complete: function () {
                        CloseGif();
                        select.select2('open');
                    },
                    error: function () {
                        toastr.error(msgErrorAjax);
                    }

                });
            }

        });
    });

    $(element).find('[data="select"]').each(function () {
        var controller = $(this).attr('controller')
        var action = $(this).attr('action')
        var parametro = $(this).attr('data-paran')
        var id = $(this).attr('data-value');
        var verifica_preco = $(this).attr('verifica-preco');
        var campo = $(this);
        if (!empty(id)) {
            $.ajax({
                url: site_path + "/" + controller + "/getedit",
                type: 'POST',
                data: {
                    id: id
                },
                dataType: 'JSON',
                success: function (txt) {
                    campo.empty()
                    campo.append('<option value="">Selecione</option>');
                    campo.append($("<option/>", {value: txt.id, text: txt.nome}));
                    campo.select2("val", txt.id);
                }
            });
        }
        campo.select2({
            language: 'pt-BR',
            theme: 'bootstrap',
            width: '100%',
            placeholder: {
                // this data 'placeholder.id' need to be null, if dont,
                // and one id lower than value in 'placeholder.id' is selected, dont be show on view.
                id: null,
                text: 'Selecione'
            },
            minimumInputLength: 3,
            ajax: {
                url: site_path + "/" + controller + "/" + action,
                dataType: 'json',
                cache: true,
                type: 'post',
                data: function (params) {
                    return {
                        termo: params.term,
                        size: 20,
                        page: params.page,
                        auxiliar: !empty(parametro) ? parametro : '',
                        verifica_preco: verifica_preco
                    };
                },
                processResults: function (data) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.dados,
                        pagination: {
                            more: data.more
                        }
                    }
                }
            }
        });
    });

    $(element).find("[data='ck']").each(function(){
        /**
         * Verifica se já existe uma instancia do editor aberta, se existir destroy a instacia e cria outra.
         */
        let name = $(this).attr('id');   
        var instance = CKEDITOR.instances[name];
        if (instance) { 
            instance.destroy(true);
            instance = null;
        }
        /************************/
        
        CKEDITOR.replace(this, {
            startupFocus : true,
            // Define the toolbar: http://docs.ckeditor.com/#!/guide/dev_toolbar
            // The standard preset from CDN which we used as a base provides more features than we need.
            // Also by default it comes with a 2-line toolbar. Here we put all buttons in a single row.

            toolbar: [
                { name: 'document', items: [ 'Print', 'Source' ] },
                { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                { name: 'styles', items: [ 'Format', 'Font', 'FontSize','PasteFromWord' ] },
                { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'CopyFormatting' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'align', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
                { name: 'insert', items: [ 'Image', 'Table'] },
                { name: 'tools', items: [ 'Maximize' ] },
                { name: 'editing', items: [ 'Scayt' ] }
            ],

            // Since we define all configuration options here, let's instruct CKEditor to not load config.js which it does by default.
            // One HTTP request less will result in a faster startup time.
            // For more information check http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-customConfig
            customConfig: '',

            // Enabling extra plugins, available in the standard-all preset: http://ckeditor.com/presets-all
            extraPlugins: 'autoembed,embedsemantic,image2,uploadimage,uploadfile,pastefromword',

            fontSize_sizes: '8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;14/14pt;16/16pt;18/18pt;20/20pt;22/22pt;24/24pt;26/26pt;28/28pt;36/36pt;48/48pt;72/72pt;',

            /*********************** File management support ***********************/
            // In order to turn on support for file uploads, CKEditor has to be configured to use some server side
            // solution with file upload/management capabilities, like for example CKFinder.
            // For more information see http://docs.ckeditor.com/#!/guide/dev_ckfinder_integration

            // Uncomment and correct these lines after you setup your local CKFinder instance.
            // filebrowserBrowseUrl: 'http://example.com/ckfinder/ckfinder.html',
            // filebrowserUploadUrl: 'http://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            /*********************** File management support ***********************/

            // Remove the default image plugin because image2, which offers captions for images, was enabled above.
            removePlugins: 'image',

            // Make the editing area bigger than default.
            height: 300,

            // An array of stylesheets to style the WYSIWYG area.
            // Note: it is recommended to keep your own styles in a separate file in order to make future updates painless.
            // contentsCss: [ 'https://cdn.ckeditor.com/4.6.1/standard-all/contents.css', '../js/ckeditor/mystyles.css' ],
            contentsCss: [ site_path+'/js/ckeditor/contents.css' ],

            // This is optional, but will let us define multiple different styles for multiple editors using the same CSS file.
            bodyClass: 'article-editor',

            // Reduce the list of block elements listed in the Format dropdown to the most commonly used.
            format_tags: 'p;h1;h2;h3;pre',

            // Simplify the Image and Link dialog windows. The "Advanced" tab is not needed in most cases.
            removeDialogTabs: 'image:advanced;link:advanced',

            // Define the list of styles which should be available in the Styles dropdown list.
            // If the "class" attribute is used to style an element, make sure to define the style for the class in "mystyles.css"
            // (and on your website so that it rendered in the same way).
            // Note: by default CKEditor looks for styles.js file. Defining stylesSet inline (as below) stops CKEditor from loading
            // that file, which means one HTTP request less (and a faster startup).
            // For more information see http://docs.ckeditor.com/#!/guide/dev_styles
            stylesSet: [
                /* Inline Styles */
                { name: 'Marker',			element: 'span', attributes: { 'class': 'marker' } },
                { name: 'Cited Work',		element: 'cite' },
                { name: 'Inline Quotation',	element: 'q' },

                /* Object Styles */
                {
                    name: 'Special Container',
                    element: 'div',
                    styles: {
                        padding: '5px 10px',
                        background: '#eee',
                        border: '1px solid #ccc'
                    }
                },
                {
                    name: 'Compact table',
                    element: 'table',
                    attributes: {
                        cellpadding: '5',
                        cellspacing: '0',
                        border: '1',
                        bordercolor: '#ccc'
                    },
                    styles: {
                        'border-collapse': 'collapse'
                    }
                },
                { name: 'Borderless Table',		element: 'table',	styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
                { name: 'Square Bulleted List',	element: 'ul',		styles: { 'list-style-type': 'square' } },

                /* Widget Styles */
                // We use this one to style the brownie picture.
                { name: 'Illustration', type: 'widget', widget: 'image', attributes: { 'class': 'image-illustration' } },
                // Media embed
                { name: '240p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-240p' } },
                { name: '360p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-360p' } },
                { name: '480p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-480p' } },
                { name: '720p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-720p' } },
                { name: '1080p', type: 'widget', widget: 'embedSemantic', attributes: { 'class': 'embed-1080p' } }
            ],
            filebrowserBrowseUrl: 'http://localhost/'+site_path+'/js/ckeditor/ckfinder/ckfinder.html',
            filebrowserUploadUrl: site_path+'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageBrowseUrl: site_path+'/js/ckeditor/ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl: site_path+'/js/ckeditor/ckfinder/ckfinder.html?type=Flash',
            filebrowserImageUploadUrl: site_path+'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl: site_path+'/js/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',

        }).on('instanceReady', function() { // Adiciona listen f nos links
            $('.cke_top a').each(function () {
                $(this).attr('listen', 'f');
            })
        });
    });
}

function input_editable(campo, element) {
    var action = !empty($(campo).attr('data-action')) ? $(campo).attr('data-action') : 'parcial-edit';
    switch ($(campo).attr('data-param')){
        case 'data':
            return $(campo).editable({
                url: site_path + '/'+$(campo).attr('data-controller')+'/'+action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                emptytext: 'Adicionar',
                title: $(campo).attr('data-title'),
                value: $(campo).attr('data-value'),
                tpl:'<input type="text" class="datepicker-botton form-control input-sm">',
                success: function (response, newValue) {
                    if(response.res=='erro') {
                        BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                    }

                }
            }).on('shown',function(){
                form_campos(element)

            });
            break;

        case 'datetime':
            return $(campo).editable({
                url: site_path + '/'+$(campo).attr('data-controller')+'/'+action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                emptytext: 'Adicionar',
                title: $(campo).attr('data-title'),
                value: $(campo).attr('data-value'),
                tpl:'<input type="text" class="datetimepicker-botton form-control input-sm">',
                success: function (response, newValue) {
                    if(response.res=='erro') {
                        BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                    }

                }
            }).on('shown',function(){
                form_campos(element);
            });
            break;

        case 'select':
            return $(campo).editable({
                url: site_path + '/'+$(campo).attr('data-controller')+'/'+action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                title: $(campo).attr('data-title'),
                select2: {
                    language: 'pt-BR',
                    theme: 'bootstrap',
                    width: "100%",
                    placeholder: {
                        id: '-1',
                        text: 'Selecione'
                    },
                    minimumInputLength : 1,
                    ajax: {
                        url: site_path+'/'+$(campo).attr('data-find')+'/fill',
                        dataType: 'json',
                        cache: true,
                        type: 'post',
                        data: function (params) {
                            return {
                                termo: params.term,
                                size: 10,
                                page: params.page
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data.dados,
                                pagination: {
                                    more: data.more
                                }
                            }
                        }
                    }
                },
                display: function(value, sourceData) {
                    var element = $(this);
                    var url = site_path+'/'+$(campo).attr('data-find')+'/getregistro'
                    $.get(url, { id: value }, function(dados){
                        if(empty(dados.res.nome)) {
                            element.html(dados.res.descricao);
                        }else{
                            element.html(dados.res.nome);
                        }
                        var parent = $($(element[0].parentElement)[0].parentElement);
                        var codigo = $(parent[0].firstChild);
                        codigo.html(dados.res.id);
                    });
                }
            }).on('shown',function(){
                form_campos(element)
            });
            break;

        case 'decimal':
            $(campo).editable({
                url: site_path + '/'+$(campo).attr('data-controller')+'/'+action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                title: $(campo).attr('data-title'),
                value: $(this).attr('data-value'),
                tpl:'<input type="text" class="form-control input-sm" mask="money">',
                success: function (response, newValue) {
                    if(response.res=='erro') {
                        BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                    }
                },
                display:function(value, source){
                    $(campo).html('R$ '+value);
                }

            }).on('shown',function(){
                form_campos(element)
            });
            break;

        default:
            $(campo).editable({
                url: site_path + '/'+$(campo).attr('data-controller')+'/'+action,
                name: $(campo).attr('data-title'),
                mode: 'inline',
                emptytext: 'Adicionar',
                title: $(campo).attr('data-title'),
                value: $(this).attr('data-value'),
                pk: $(this).attr('data-pk'),
                success: function (response, newValue) {
                    if(response.res=='erro') {
                        BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                    }
                }
            }).on('shown',function(){
                form_campos(element)
            });
            break;
    }


}