const url = window.location.href;

function number_format(number, decimals, decPoint, thousandsSep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
    var n = !isFinite(+number) ? 0 : +number
    var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
    var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
    var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
    var s = ''

    var toFixedFix = function (n, prec) {
        var k = Math.pow(10, prec)
        return '' + (Math.round(n * k) / k)
            .toFixed(prec)
    }

    // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || ''
        s[1] += new Array(prec - s[1].length + 1).join('0')
    }

    return s.join(dec)
}

function getIndexOfTable() {
    let tableData = {}, tableLinks = [], tableLabels = [],
        regex = new RegExp(/(?<==)[^"]*(?=&)/g);

    $('.table-responsive > thead > tr > th > a').each(function () {
        tableLinks.push($(this).attr('href'));
        tableLabels.push($(this).attr('href').match(regex)[0]);
    })

    tableData.tableLabels = tableLabels;
    tableData.tableLinks = tableLinks;

    return tableData;
}

function getTableBody(tableData, result) {
    let tableBody = '';

    $.each(result, function (resultIndex, obj) {
        let td = '';
        $.each(tableData.tableLabels, function (labelIndex, label) {
            let find = false;
            $.each(obj, function (objIndex, value) {
                if (label === objIndex) {
                    find = value;
                }
            });
            td += find ? "<td>" + find + "</td>" : "<td></td>";
        });
        tableBody += "<tr>" + td + "</tr>";
    });

    tableData.tableBody = tableBody;
    return tableData;
}

function getTableHeader(tableData) {
    let tableHeader = '';

    $.each(tableData.tableLabels, function (labelIndex, label) {
        tableHeader += "<th><a href=" + tableData.tableLinks[labelIndex] + ">" + label + "</a></th>";
    });

    tableData.tableHeader = tableHeader;
    return tableData;
}

function createTable(tableData) {
    let tableResponsive = "<thead><tr>" + tableData.tableHeader + "</tr></thead>";
    tableResponsive += "<tbody>" + tableData.tableBody + "</tbody>";

    return tableResponsive;
}

function empty(value) {
    if (value === '' || value === null || typeof value === "undefined" || value == null) {
        return true;
    }
    return false;
}