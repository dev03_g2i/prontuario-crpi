/**
 * Created by Tiago on 26/12/2015.
 */
function confirmar(mensagem) {
    BootstrapDialog.confirm({
        title: "Alerta",
        message: mensagem,
        type: BootstrapDialog.TYPE_INFO,
        closable: false,
        draggable: false,
        btnCancelLabel: "Não!",
        btnOKLabel: "Sim!",
        btnOKClass: "btn-info",
        callback: function (result) {
            if (result) {
                return true;
            } else {
                return false;
            }
        }
    })
}

var fil_ = {
    atendimento_procedimento: null,
    ajax: 1,
    tipo: 'f',
    historico: null
}
var sit = null;
var modal_aberta = null;
//funcao padrao
var click = null;
var filtrado = null;
var last_url = null;
var executado = 0;
var filtro = {
    procedimento_id: '',
    convenio_id: '',
    complemento: '',
    medico_id: ''
};
var transferencia = null;

var msgErrorAjax = 'Ocorreu um erro inesperado, Por favor tente mais tarde!';

// Foi para Atendimentos.js - Luciano 09/11/2017 20:11
/*function editar_iten(id) {
    var convenio_id = $('#convenio-id').val();
    last_url = site_path + "/AtendimentoProcedimentos/cadastrar/?ajax=1&first=1&convenio="+convenio_id+"&pos=" + id
    dialog_sub = new BootstrapDialog({
        title: 'Editar Procedimento',
        message: $('<div></div>').load(last_url, carregar_procedimentos),
        closable: true,
        size: BootstrapDialog.SIZE_WIDE,
        buttons: [{
            label: 'Finalizar',
            cssClass: 'btn-primary btn-finaliza-procedimento hidden',
            action: function (dialogRef) {
                click = null;
                dialog_sub = null;

                var modal = dialogRef.getModalBody();

                $($(modal).find("#form_procedimento")).ajaxForm({
                    beforeSubmit: function () {
                        LoadGif();
                    },
                    success: function (txt) {
                        CloseGif();
                        dialogRef.close();
                        var filtro = {
                            procedimento_id: '',
                            convenio_id: ''
                        };
                        $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
                    }
                }).submit();


            }
        }]

    });
    dialog_sub.open();
}*/

function loadController(element) {
    if (typeof Laudos == 'function') {
        Laudos(element);
    }
    if (typeof Contaspagar == 'function') {
        Contaspagar(element);
    }
    if (typeof Atendimentos == 'function') {
        Atendimentos(element);
    }
    if (typeof ClienteAnexos == 'function') {
        ClienteAnexos(element);
    }
    if (typeof Faturamentos == 'function') {
        Faturamentos(element);
    }
    if (typeof Agendas == 'function') {
        Agendas(element);
    }
    if (typeof FaturaMatmed == 'function') {
        FaturaMatmed(element);
    }
    if (typeof ProdutividadeProfissionais == 'function') {
        ProdutividadeProfissionais(element);
    }
    if (typeof PrecoProcedimentos == 'function') {
        PrecoProcedimentos(element);
    }
    if (typeof GrupoAgendaHorarios == 'function') {
        GrupoAgendaHorarios(element);
    }
    if (typeof Users == 'function') {
        Users(element);
    }
    if (typeof Contasreceber == 'function') {
        Contasreceber(element);
    }
    if (typeof EstqSaida == 'function') {
        EstqSaida(element);
    }
}

function initDocument(element) {
    permissoes();

    // Chama a função de acordo com arquivo js carregado.
    loadController(element);

    $(element).find('select.select2').select2({
        theme: 'bootstrap',
        placeholder: 'Selecione',
        tags: true,
    });

    /**
     *  Se for modal do prontuario, aumenta a largura, se não deixa padrão do bootstrap
     *  @autor Luciano
     */
    if ($('#prontuario-g2i').html()) {
        $(document).find('#modal_lg .modal-lg').css('width', '1200px');
    } else {
        $(document).find('#modal_lg .modal-lg').css('width', '');
    }

    /* troca o href dos links que mostram os cadastros, de acordo com cliente e solicitante selecionado */
    $(element).find('#cliente-id, #cliente_id, #solicitante-id').each(function () {
        var form_group = $(this).closest('.form-group');
        if (!empty($(this).val())) {
            changeHref($(this).val(), form_group, form_group.find('.btnView').attr('controller'), form_group.find('.btnView').attr('action'));
        }
        $(this).change(function () {
            form_group = $(this).closest('.form-group');
            changeHref($(this).val(), form_group, form_group.find('.btnView').attr('controller'), form_group.find('.btnView').attr('action'));
        });
    });

    $(element).find('form#frm-parcelas #tipopagamento').each(function () {

        ajaxGetFormaPagamento($(this).val());
        $(this).change(function () {
            ajaxGetFormaPagamento($(this).val());
        });

        function ajaxGetFormaPagamento(tipo_pagamento) {
            $.ajax({
                type: 'POST',
                url: site_path + '/TipoPagamento/getFormaPagamento/' + tipo_pagamento,
                dataType: 'JSON',
                beforeSend: function () {
                    LoadGif();
                },
                success: function (txt) {
                    CloseGif();
                    $(element).find('#tipodocumento').val(txt);
                },
                error: function () {
                    toastr.error(msgErrorAjax);
                }
            });
        }

    });

    $(element).find('#tipohistoria-id').change(function () {
        if (!empty($(this).val())) {
            var get_tipo_modelos = ajaxGetTipoHistoriaModelos('nome', $(this).val());
            get_tipo_modelos.success(function (txt) {
                $("#modelo").empty().append('<option value="">Selecione</option>').find('option:first');
                if (txt.length > 0) {
                    for (var i = 0; i < txt.length; i++) {
                        $('#modelo').append($("<option/>", {
                            value: txt[i].id,
                            text: txt[i].nome,
                            tipoConteudo: txt[i].tipo_conteudo
                        }));
                    }
                }
            });
        }
    });

    $(element).find('#add-historia-modelo').each(function () {

        var tipo_historia = $(element).find('#tipohistoria-id').val();
        if (!empty(tipo_historia)) {
            var get_tipo_modelos = ajaxGetTipoHistoriaModelos('nome', tipo_historia);
            get_tipo_modelos.success(function (txt) {
                if (txt.length > 0) {
                    for (var i = 0; i < txt.length; i++) {
                        $('#modelo').append($("<option/>", {
                            value: txt[i].id,
                            text: txt[i].nome,
                            tipoconteudo: txt[i].tipo_conteudo
                        }));
                    }
                }
            });
        }

        $(this).click(function () {
            var modelo = $('#modelo').val();
            var tipo_conteudo = $('#modelo').find('option:selected').attr('tipoconteudo');
            var form_id = $('#salve-tratamento').parent().parent().parent().attr('id');
            var get_tipo_modelos = ajaxGetTipoHistoriaModelos('modelo', modelo);

            if (empty($('#medico-id').val())) {
                required('Atenção', 'Informe um profissional!');
                return false;
            }

            if (tipo_conteudo == 2) //Tipo Formulario
            {
                if (!empty(modelo)) {

                    get_tipo_modelos.success(function (txt) {
                        // $('#descricao').summernote('code', txt.texto_procedimento);
                        var editor = CKEDITOR.instances.descricao;
                        var sel = editor.getSelection();
                        var element = sel.getStartElement();
                        sel.selectElement(element);
                        var ranges = editor.getSelection().getRanges();
                        ranges[0].setStart(element.getFirst(), 1);
                        ranges[0].setEnd(element.getFirst(), 0); //cursor

                        $.when(editor.insertHtml(txt.texto_procedimento))
                            .then(function () {
                                var s = editor.getSelection();
                                var selected_ranges = s.getRanges();
                                s.selectRanges(selected_ranges); // restore it
                            })
                        dialog_sub = new BootstrapDialog({
                            title: 'Cadastro Mastologia',
                            message: $('<div></div>').load(site_path + "/" + txt.controller + "/" + txt.action + "/?ajax=1&first&" + $('form#' + form_id).serialize(), inicializar),
                            closable: true,
                            size: BootstrapDialog.SIZE_WIDE,
                            buttons: [
                                {
                                    label: 'Cancelar',
                                    cssClass: 'btn-default',
                                    action: function (dialogRef) {
                                        dialogRef.close();
                                    }
                                },
                                {
                                    label: 'Salvar',
                                    cssClass: 'btn-primary',
                                    action: function (dialogRef) {
                                        var modal = dialogRef.getModalBody();
                                        // AjaxForm and save cliente historicos
                                        $($(modal).find('#frm-modelo-formulario')).ajaxForm({
                                            beforeSubmit: function () {
                                                LoadGif()
                                            },
                                            success: function (txt) {
                                                CloseGif();
                                                if (txt.res == 0) {
                                                    toastr.success(txt.msg);
                                                    dialogRef.close();
                                                    $('.modal-content').find('li.active a').click();
                                                } else {
                                                    toastr.error(txt.msg);
                                                }
                                            }
                                        }).submit();
                                    }
                                }
                            ]

                        });
                        dialog_sub.open();
                    });
                }
            } else if (tipo_conteudo == 1) { // Tipo texto
                //var get_tipo_modelos = ajaxGetTipoHistoriaModelos('modelo', modelo);
                get_tipo_modelos.success(function (txt) {
                    var editor = CKEDITOR.instances.descricao;
                    var sel = editor.getSelection();
                    var element = sel.getStartElement();
                    sel.selectElement(element);
                    var ranges = editor.getSelection().getRanges();
                    ranges[0].setStart(element.getFirst(), 1);
                    ranges[0].setEnd(element.getFirst(), 0); //cursor

                    $.when(editor.insertHtml(txt.modelo))
                        .then(function () {
                            var s = editor.getSelection();
                            var selected_ranges = s.getRanges();
                            s.selectRanges(selected_ranges); // restore it
                        })
                    // $('#descricao').summernote('code', txt.modelo);
                });
            }
        })

    });

    $(element).find('.visualizar-agenda').on('click', function () {
        var defaultView = $(this).attr('default-view');
        var url = site_path + '/Agendas/lista?';
        var inicio = convertDate($(element).find('#inicio').val());

        url += (!empty(inicio)) ? 'data=' + inicio : '';
        url += (!empty($(element).find('#grupo-id').val())) ? '&grupos=' + $(element).find('#grupo-id').val() : '';
        url += '&defaultView=' + defaultView;
        $(this).attr('href', url);
    });

    $(element).find('#btnAddHorarios').each(function () {

        $(this).click(function () {
            var form = $(this).parent().parent().parent().serialize();
            var inicio = convertDatetime($(element).find('#inicio').val());
            var fim = convertDatetime($(element).find('#fim').val());
            var grupo_id = $(element).find('#grupo-id').val();

            if (empty(grupo_id)) {
                required('Atenção', 'Informe a agenda!');
                return false;
            }
            if (empty(inicio)) {
                required('Atenção', 'Informe o início da agenda!');
                return false;
            }
            if (empty(fim)) {
                required('Atenção', 'Informe o fim da agenda!');
                return false;
            }
            if (fim <= inicio) {
                required('Atenção', 'A hora fim não pode ser Menor/Igual a hora inicio!');
                return false;
            }


            var validation_agenda = validationAgenda(grupo_id, inicio, fim);
            validation_agenda.success(function (txt) {
                CloseGif();
                if (txt.res > 0) {
                    BootstrapDialog.alert({
                        title: 'Atenção',
                        type: BootstrapDialog.TYPE_DANGER,
                        message: txt.msg
                    });
                    return false;
                } else {
                    $.ajax({
                        type: 'POST',
                        url: site_path + '/Agendas/save-horarios',
                        data: form,
                        dataType: 'JSON',
                        beforeSend: function () {
                            LoadGif()
                        },
                        success: function (txt) {
                            CloseGif();
                            if (txt != 'error') {
                                var html = '<tr class="delete' + txt.id + '">';
                                html += '<td>' + txt.agenda + '</td><td>' + txt.tipo + '</td><td>' + txt.inicio + '</td><td>' + txt.fim + '</td><td>' + txt.obs + '</td>';
                                html += '<td><span class="btn btn-danger btn-xs" onclick="DeletarModal(\'Agendas\', ' + txt.id + ')"><i class="fa fa-close"></i></span></td>';
                                html += '</tr>';
                                $(element).find('#show-horarios tbody').append(html);
                            } else {
                                toastr.error('Falha ao tentar cadastrar horário, Por favor tente mais tarde!');
                            }
                        }
                    });

                }

            });

        });

    });

// Não deixa cadastrar Data inicio menor que fim e vice versa
    $(element).find('form.validateDateInicioFim').each(function () {
        var form = $(this);
        form.find('#inicio').on("dp.change", function (e) {
            form.find('#fim').data("DateTimePicker").minDate(e.date);
        });
        form.find('#fim').on("dp.change", function (e) {
            form.find('#inicio').data("DateTimePicker").maxDate(e.date);
        });

        form.find('#data-inicio').on("dp.change", function (e) {
            let date = e.date;
            let newDate = moment(date._d).add(6, 'M');
            $('#data-fim').closest('.col-md-3').removeClass('hidden');
            form.find('#data-fim').data("DateTimePicker").maxDate(newDate);
        });
        /* form.find('#data-fim').on("dp.change", function(e) {
             form.find('#data-inicio').data("DateTimePicker").minDate(e.date);
         });*/

        form.find('#hora-inicio').on("dp.change", function (e) {
            form.find('#hora-fim').data("DateTimePicker").minDate(e.date);
        });
        form.find('#hora-fim').on("dp.change", function (e) {
            form.find('#hora-inicio').data("DateTimePicker").maxDate(e.date);
        });

        form.find('#intervalo').datetimepicker({
            locale: 'pt-br',
            format: 'LT',
            enabledHours: [0]
        });

    });

    $(element).find('#frm-agenda-periodos').each(function () {
        var periodo = $(this).find('#periodo').val();
        var inicio = $('#inicio');
        var fim = $('#fim');
        let form = $(this);

        inicio.val('05:00');
        fim.val('12:00');

        $('#periodo').on('change', function () {
            periodo = $(this).val();
            $('#inicio, #fim').val('');
            if (periodo == 1) {
                inicio.val('05:00');
                fim.val('12:00');
            }
            else if (periodo == 2) {
                inicio.val('12:00');
                fim.val('18:00');
            }
            else if (periodo == 3) {
                inicio.val('18:00');
                fim.val('00:00');
            }
        });

        form.find('#intervalo').datetimepicker({
            locale: 'pt-br',
            format: 'LT',
            enabledHours: [0]
        });

    });


    $(element).find('#select-procedimento, #medico-id').each(function () {
        $(this).select2({
            theme: 'bootstrap',
            placeholder: 'Selecione'
        });
    });

// Contadores Faturamentos/rec-listar
    $(element).find('#form-faturamento-reclistar').each(function () {
        var form = $(this).serialize();

        if (empty(click)) {
            ajaxCountReclisar(form);
        }

        $(this).find('.btnFiltrarReclistar').click(function () {
            var form = $(this).parent().parent().serialize();
            ajaxCountReclisar(form);
            click = 1;
        });

    });

// Produtividade profissionais
// Foi mandado para o arquivo ProdutividadeProfissionais.js
    /*$(element).find('.btnSalvarTemp').on('click', function () {
        if(!empty(click)){
            return false;
        }

        click = 1;
        var controller = $(this).attr('controller');
        var action = $(this).attr('action');
        var id_form = $(this).parent().parent().attr('id');

        $.ajax({
            type: 'POST',
            url: site_path + '/'+controller+'/'+action,
            data: $('form#'+id_form).serialize(),
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif()
            },
            success: function (txt) {
                CloseGif();
                $(element).find('form#'+id_form).each(function () {
                    var caminho = $(this).attr('action');
                    var form = $(this).serialize();
                    $("div.table-responsive").loadNewGrid(caminho, form, null, carregar);
                    $(element).find('.contagem').text(txt.count);
                    $(element).find('.valor_fatura').text(formatReal(txt.valor_fatura));
                    $(element).find('.valor_caixa').text(formatReal(txt.valor_caixa));
                    $(element).find('.valor_prod_fatura').text(formatReal(txt.valor_prod_fatura));
                    $(element).find('.valor_prod_caixa').text(formatReal(txt.valor_prod_caixa));
                    $(element).find('.valor_prod_convenio').text(formatReal(txt.valor_prod_convenio));
                    $(element).find('.valor_prodclin_recebimento').text(formatReal(txt.valor_prodclin_recebimento));
                    $(element).find('.valor_recebido').text(formatReal(txt.valor_recebido));
                    $(element).find('.valor_prodcaixa_clinica').text(formatReal(txt.valor_prodcaixa_clinica));
                })
            }
        })

    });*/
// Produtividade profissionais
    /*$(element).find('.btnRefresh').on('click', function () {
        $('.contagem').text('0');
        $('.total').text('R$ 0');
    });*/

// Faturamento Encerramento
    $(element).find('#btnEncerrar').on('click', function () {
        if (!empty(click)) {
            return false;
        }
        var objData = new Date();
        var anoAtual = objData.getFullYear();
        var arrayAnos = [
            anoAtual - 4,
            anoAtual - 3,
            anoAtual - 2,
            anoAtual - 1,
            anoAtual,
            anoAtual + 1,
            anoAtual + 2,
            anoAtual + 3,
            anoAtual + 4
        ];

        var mensagemText = "Informe o mês e o ano de referência para realizar o faturamento:" +
            "<p>" +
            "<div class='col-md-6'><select class='form-control' name ='mes_referencia' id='mes_referencia'> " +
            "   <option value='01'>Janeiro</option>" +
            "   <option value='02'>Fevereiro</option>" +
            "   <option value='03'>Março</option>" +
            "   <option value='04'>Abril</option>" +
            "   <option value='05'>Maio</option>" +
            "   <option value='06'>Junho</option>" +
            "   <option value='07'>Julho</option>" +
            "   <option value='08'>Agosto</option>" +
            "   <option value='09'>Setembro</option>" +
            "   <option value='10'>Outubro</option>" +
            "   <option value='11'>Novembro</option>" +
            "   <option value='12'>Dezembro</option>" +
            "</select></div>" +
            "<div class='col-md-6'><select class='form-control' name='ano_referencia' id='ano_referencia'>";
        for (i = 0 ; i < arrayAnos.length; i++) {
            if(arrayAnos[i] === anoAtual){
                mensagemText += "<option selected value='" + arrayAnos[i] + "'>" + arrayAnos[i] + "</option>";
            } else {
                mensagemText += "<option value='" + arrayAnos[i] + "'>" + arrayAnos[i] + "</option>";
            }
        }
        mensagemText += "</select></div><div class='clearfix'></div>" +
            "</p>";

        click = 1;
        var href = $(this).attr('href');
        $(this).removeAttr('href');
        BootstrapDialog.show({
            title: "Atenção",
            message: mensagemText,
            type: BootstrapDialog.TYPE_WARNING,
            closable: false,
            buttons: [{
                label: 'sim',
                cssClass: 'btn-warning',
                action: function (dialogRef) {
                    var novaHref = href + '?mes_referencia=' + $("#mes_referencia").val() + '&ano_referencia=' + $("#ano_referencia").val() + '&convenio_id=' +
                        $("#convenio_id_encerramento").val();
                    window.location.href = novaHref;
                }
            },
                {
                    label: 'Não',
                    cssClass: 'btn-default',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                }
            ]
        })
    })

//Faturamento Pre-faturamento
    $(element).find('.btnRefresh').on('click', function () {
        $('.count').text('0');
        $('.total').text('R$ 0');
    })

//Faturamento Encerramento
    $(element).find('.btnFiltrarEncerramento').on('click', function () {
        var form_id = $(this).parent().parent().attr('id');
        if (!empty(click)) {
            return false;
        }
        click = 1;
        $.ajax({
            type: 'GET',
            url: site_path + '/Faturamentos/salvar-temp-faturamento',
            data: $('form#' + form_id).serialize(),
            dataType: 'JSON',
            beforeSend: function () {
                LoadGif()
            },
            success: function (txt) {
                CloseGif();
                console.log(txt.total);
                $(element).find('form#' + form_id).each(function () {
                    var caminho = $(this).attr('action');
                    var form = $(this).serialize();
                    $("div.table-responsive").loadNewGrid(caminho, form, null, carregar);
                    $(element).find('.quantidade_reg').text(txt.count);
                    $(element).find('.total_geral').text(formatReal(txt.total));
                    $(element).find('.valor_material').text(formatReal(txt.valor_material));
                    $(element).find('.valor_caixa').text(formatReal(txt.valor_caixa));
                    $(element).find('.total_geral_com_material').text(formatReal(txt.total + txt.valor_material));
                });
            }
        });
    });

//Faturamento Pre-faturamento
    $(element).find('.btnFiltrar').on('click', function () {
        var form = $(this).parent().parent().serialize();

        if (!empty(click)) {
            return false;
        }
        click = 1;
        $.ajax({
            type: 'GET',
            url: site_path + '/Faturamentos/count-atendimento-procedimentos',
            data: form,
            dataType: 'JSON',
            success: function (txt) {
                //$(element).find(".atendimento_procedimentos").text(txt.count);
                $(element).find(".quantidade_reg").text(txt.count);
                $(element).find(".total_geral").text(formatReal(txt.total));
                $(element).find(".valor_caixa").text(formatReal(txt.valor_caixa));
                $(element).find(".valor_material").text(formatReal(txt.valor_material));
                $(element).find(".total_geral_com_material").text(formatReal(txt.valor_material + txt.total));
                $(element).find(".total_equipe").text(formatReal(txt.total_equipe));
                executado = 0;
            }
        })
    });

    // $(element).find('#btn-fill').on('click', function () {
    //     var form = $(this).parent().parent().serialize();
    //     if (!empty(click)) {
    //         return false;
    //     }
    //     click = 1;
    //     $.ajax({
    //         type: 'GET',
    //         url: site_path + '/Faturamentos/sum-total-valores-faturamentos_encerrados/' + 2,
    //         dataType: 'JSON',
    //         success: function (txt) {
    //             $(element).find(".total_previstoAll").text(txt.total_previstoAll);
    //             $(element).find(".total_brutoAll").text(formatReal(txt.total_brutoAll));
    //             $(element).find(".total_liquidoAll").text(formatReal(txt.total_liquidoAll));
    //             $(element).find(".total_nfsALl").text(formatReal(txt.total_nfsALl));
    //             executado = 0;
    //         }
    //     })
    // });


    // Foi colocado no arquivo controllers/Atendimentos.js
    /*$(element).find("#plus-procedimento").each(function(){
     $(this).click(function(){
     if(empty(click)){
     click = 1;
     if ($(element).find("#convenio-id").val() == "") {
     BootstrapDialog.alert('Selecione um convênio!');
     return false;
     }
     last_url = site_path + "/AtendimentoProcedimentos/cadastrar/?ajax=1&first=1&convenio=" + $(element).find("#convenio-id").val()
     dialog_sub = new BootstrapDialog({
     title: 'Cadastro Procedimento',
     message: $('<div></div>').load(last_url, carregar_procedimentos),
     closable: true,
     size: BootstrapDialog.SIZE_WIDE,
     buttons: [{
     label: 'Finalizar',
     cssClass: 'btn-primary',
     action: function (dialogRef) {
     click = null;
     dialog_sub = null;

     var modal = dialogRef.getModalBody();

     if (empty($(modal).find("#medico-id").val())) {
     required('Alerta', 'Selecione um médico');
     return false;
     }
     if (empty($(modal).find("#procedimento-id").val())) {
     required('Alerta', 'Selecione um procedimento');
     return false;
     }
     if (empty($(modal).find("#procedimento-id").val())) {
     required('Alerta', 'Selecione um procedimento');
     return false;
     }

     $($(modal).find("#form_procedimento")).ajaxForm({
     beforeSubmit: function () {
     LoadGif()
     },
     success: function (txt) {
     click = null;
     dialog_sub = null;
     CloseGif();
     dialogRef.close();
     var filtro = {
     procedimento_id: '',
     convenio_id: ''
     }
     $(element).find(".list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
     }
     }).submit();


     }
     }]

     });
     dialog_sub.open();
     }
     })
     });*/

    $(element).find("#usa-carteira").each(function () {
        $(this).change(function () {
            if ($(this).val() == 1) {
                $(element).find("#dv-carteira").show()
            } else {
                $(element).find("#dv-carteira").hide()
            }
        })
    })

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });

    $('.edit-status-periodos').each(function () {

        $(this).editable({
            url: site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action'),
            name: $(this).attr('name'),
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            source: [
                {value: 1, text: 'Sim'},
                {value: 2, text: 'Não'},
            ],
            success: function (response, newValue) {
            }
        });

    });

    $(".currency_edit").each(function () {
        $(this).editable({
            url: site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action'),
            name: $(this).attr('name'),
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            tpl: '<input type="text" class="mask form-control rec_parc input-sm dd">',
            success: function (response, newValue) {
                var filtro = {
                    procedimento_id: '',
                    convenio_id: ''
                };
                if (response.error == 'desconto') {
                    toastr.warning(response.msg);
                    return false;
                } else {
                    $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
                }

            },
        }).on('shown', function () {
            $("input.rec_parc").mask('000000000000000.00', {reverse: true});
        });
    })

    $(".text_edit").each(function () {
        $(this).editable({
            url: site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action'),
            name: $(this).attr('name'),
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            success: function (response, newValue) {
                var filtro = {
                    procedimento_id: '',
                    convenio_id: ''
                }
                $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
            }
        });
    });

    $(".currency_upd").each(function () {
        $(this).editable({
            url: site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action'),
            name: $(this).attr('name'),
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            tpl: '<input type="text" class="mask form-control rec_parc input-sm dd">',
            success: function (response, newValue) {
                if (response.error == 'desconto') {
                    toastr.warning(response.msg);
                    return false;
                } else {
                    atualizar(response.res);
                }
            }
        }).on('shown', function () {
            $("input.rec_parc").mask('000000000000000.00', {reverse: true});
        });
    });

    $(".text_upd").each(function () {
        $(this).editable({
            url: site_path + '/' + $(this).attr('controller') + '/' + $(this).attr('action'),
            name: $(this).attr('name'),
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            success: function (response, newValue) {
                atualizar(response.res)

            }
        });
    })


    var modal = modal_ativa();
    var documento = null
    if (!empty(modal)) {
        var documento = $(modal).find('.modal-content');
    }

    getFill(documento)


    $(documento).find("#atualiza-atendimento").each(function () {
        if (!empty($(this).val())) {
            atualizar($(this).val())
            $(documento).find("#atualiza-atendimento").val('')
        }
    })

    /*$(element).find("#frm-agenda").each(function () {
     var inst = $(this)
     var situacao_anterior = $(this).find('#situacao-agenda-id').val()
     if (!empty(inst.find('#agenda-id').val())) {
     $(this).find('#situacao-agenda-id').change(function () {

     var situacao = $(this).val();
     var check_atendimento = agenda_atendimento(inst.find('#agenda-id').val())
     check_atendimento.success(function (txt) {
     CloseGif();
     if (txt.res == 0) {
     if (situacao == 10 || situacao == 13 || situacao == 12) {
     inst.find('#situacao-agenda-id').val(situacao_anterior).trigger('change')
     required('Alerta', 'Não é permitido colocar nesta situcação, se não existir atendimento!');
     }
     }
     })
     })
     }
     })*/

    $(element).find('#frm-agenda').each(function () {
        var agenda_id = $(this).find('#agenda-id').val();
        if (!empty(agenda_id)) {
            var check_atendimento = agenda_atendimento(agenda_id);
            check_atendimento.success(function (txt) {
                CloseGif();
                if (txt.res == 0) {
                    var situacao = [10, 12, 13];
                    $('[name=situacao_agenda_id] option').filter(function () {
                        return ($.inArray(parseInt(this.value), situacao) > -1);
                    }).prop('disabled', true);
                }
            })
        }
    });

    $(element).find("[get='convenio']").each(function () {
        $(this).change(function () {
            $.ajax({
                type: 'POST',
                url: site_path + '/getconvenio',
                data: {
                    id: $(this).val()
                },
                dataType: 'JSON',
                beforeSend: function () {
                    LoadGif();
                },
                success: function (txt) {
                    var exist_convenio = $(element).find('#convenio-id option[value=' + txt.convenio_id + ']').length > 0;
                    if (exist_convenio) {
                        $(element).find("#convenio-id").val(txt.convenio_id).trigger('change');
                    } else {
                        $(element).find("#convenio-id").val('').trigger('change');
                    }
                    $(element).find('#idade').val(txt.idade);
                    $(element).find('#fone-provisorio').val(txt.fone);
                },
                complete: function () {
                    CloseGif();
                    // $(element).find('#agenda-fone').show();
                    form_campos(element);
                },
                error: function () {
                    CloseGif();
                    toastr.error(msgErrorAjax);
                }
            });
        })
    })

    // foi removido o link dos buttons, pois era só colocar um helper link com class btn-default que funciona o input group com link
    $(element).find('#add-cliente').click(function (event) {
        $("#modal_2").modal({
            "remote": site_path + '/cadclient/?ajax=1'
        }).show();
        $('#open-modal-addcliente').click();
    });

    $(element).find('#view-cliente').click(function (event) {
        $("#modal_2").modal({
            "remote": site_path + '/Clientes/view/' + $(this).attr('data-id') + '?ajax=1'
        }).show()
    });

    $(element).find("[data-fill='send']").each(function () {
        $(this).change(function () {
            if (!empty($(element).find("#btn-fill").html())) {
                $(element).find("#btn-fill").click()
            }
        })

    })
    $(element).find("#btn-procedimento").click(function () {
        if (!empty(click)) {
            return false;
        }

        click = 1;
        if ($(element).find("#convenio-id").val() == "") {
            BootstrapDialog.alert('Selecione um convênio!');
            $(this).val('');
            return false;
        }
        if ($(element).find("#medico-id").val() == "") {
            BootstrapDialog.alert('Selecione um profissional!');
            $(this).val('');
            return false;
        }


        filtro.procedimento_id = $(element).find("#procedimentos").val();
        filtro.convenio_id = $(element).find("#convenio-id").val();
        filtro.complemento = $(element).find("#complemento").val();
        filtro.controle = $(element).find("#controle").val();
        filtro.medico_id = $(element).find("#medico-id").val();
        filtro.quantidade = $(element).find("#quantidade").val();
        $.ajax({
            type: 'GET',
            url: site_path + '/Atendimentos/precoproc',
            data: {
                convenio_id: filtro.convenio_id,
                procedimento_id: filtro.procedimento_id
            },
            success: function (txt) {
                $(element).find("#procedimentos").val("").trigger('change');
                if (txt != 'erro') {
                    $(element).find(".list-dep").loadGrid(site_path + '/Atendimentos/proc', filtro, '.arts', getValores);
                } else {
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_WARNING,
                        message: 'Este procedimento não possui valor de cobrança cadastrado!'
                    });
                    return false;
                }
            }
        })

    });

    $(documento).find('[data-toggle="modal"]').each(function () {
        $(this).click(function () {
            if (!empty(click)) {
                return false;
            }
            click = 1;
            Acrescentar($(this).attr('href'))
            $("#modal_2").modal({
                "remote": $(this).attr('href')
            }).show()

            return false;
        })
    });

    $(element).find("#btn-edit-procedimento").click(function () {
        if (!empty(click)) {
            return false;
        }

        click = 1;
        if ($(element).find("#convenio-id").val() == "") {
            BootstrapDialog.alert('Selecione um convênio!');
            $(this).val('');
            return false;
        }
        if ($(element).find("#medico-id").val() == "") {
            BootstrapDialog.alert('Selecione um profissional!');
            $(this).val('');
            return false;
        }
        $.ajax({
            type: 'GET',
            url: site_path + '/Atendimentos/procedit',
            data: {
                procedimento_id: documento.find('#procedimento-id').val(),
                convenio_id: documento.find('#convenio-id').val(),
                complemento: documento.find('#complemento').val(),
                medico_id: documento.find('#medico-id').val(),
                atendimento_id: documento.find('#atendimento-id').val(),
                quantidade: documento.find('#quantidade').val(),
                controle: documento.find('#controle').val()
            },
            success: function (txt) {
                switch (txt) {
                    case 'preco':
                        BootstrapDialog.alert({
                            type: BootstrapDialog.TYPE_WARNING,
                            message: 'Este procedimento não possui valor de cobrança cadastrado!'
                        });
                        break;
                    case 'erro':
                        BootstrapDialog.alert({
                            type: BootstrapDialog.TYPE_DANGER,
                            message: 'Erro ao salvar registro!'
                        });
                        break;
                    default:
                        atualizar(txt)
                        break;
                }
            }
        })
    });
    $(element).find("#cancel-historico").click(function () {
        carregarAba($(".area_pront").parent('.active').attr('id'), $("[aria-controls='" + $(".area_pront").parent('.active').attr('id') + "']").attr('controller'), filtro);
    })

    $(element).find("#abrir_cadastro").click(function () {
        if (empty(click)) {
            click = 1;
            dialog_sub = new BootstrapDialog({
                title: 'Cadastro',
                message: $('<div></div>').load(site_path + "/Clientes/nview/" + $(this).attr('data-id') + "?ajax=1", inicializar),
                closable: true,
                size: BootstrapDialog.SIZE_WIDE,
                buttons: [{
                    label: 'Fechar',
                    cssClass: 'btn-primary',
                    action: function (dialogRef) {
                        click = null;
                        dialog_sub = null;
                        dialogRef.close();
                    }
                }]
            });
            dialog_sub.open();
        }
    })

    $(element).find("#add-historico").click(function () {
        $(this).hide();
        $(element).find(".add-historico").loadGrid(this.href, null, ".area_pront", loadComponentes);
        $(element).find(".fn_esconder").remove();
        return false;
    });

    $(element).find('#add-modelo').click(function () {
        if (empty($(element).find("#tipo-id").val())) {
            BootstrapDialog.alert('Selecione um tipo de documento');
            return false;
        }
        dialog_modelos = new BootstrapDialog({
            title: 'Modelos',
            message: $('<div></div>').load(site_path + "/Modelos/nadd/" + $(element).find("#tipo-id").val() + "?ajax=1", inicializar),
            closable: true,
            size: BootstrapDialog.SIZE_WIDE
        });
        dialog_modelos.open();

    });


    $(element).find(".numero_clientes").each(function () {
        var form = $(this).parent().parent().serializeObject();
        if (executado != 0) {
            return false;
        }
        $.ajax({
            type: 'GET',
            url: site_path + '/Agendas/count_clientes',
            data: form,
            dataType: 'JSON',
            success: function (txt) {

                $(element).find(".numero_clientes").text(txt)
                executado = 0;
            }
        })
    })

    $(element).find(".clientes_agendas").each(function () {
        var form = $(this).parent().parent().serializeObject();
        if (executado != 0) {
            return false;
        }
        $.ajax({
            type: 'GET',
            url: site_path + '/Agendas/count_agendas',
            data: form,
            dataType: 'JSON',
            success: function (txt) {

                $(element).find(".clientes_agendas").text(txt)
                executado = 0;
            }
        })
    });

    // Esta logica foi aplicada em Agenda/views: [index, agendas, modal]
    /*$(element).find("[data-color]").each(function () {
            var color = $(this).attr('data-color');
            $(this).children().each(function (key, value) {
                if (!$(value).hasClass('actions')) {
                    $(value).css({'color': color})
                    $(value).children('a').css({'color': color})
                }
            })
        }
    );*/


    $(documento).find("#fill-modal").click(function () {
        if (empty(filtrado)) {
            filtrado = 1;
            var caminho = $(this).parent().parent()[0].action;
            var form = $(this).parent().parent().serialize();

            $(documento).find("div.table-responsive").loadNewGrid(caminho, form, null, loadComponentes);
        }
    });

    $(documento).find("#refresh-modal").click(function () {
        var caminho = $(this).parent().parent()[0].action;
        var fr = $(this).parent().parent()[0];
        clear_all(fr)
        var form = $(this).parent().parent().serialize();
        $(documento).find("div.table-responsive").loadNewGrid(caminho, form, null, loadComponentes);
    });

    $(documento).find(".list-artigos").each(function () {
        if (executado == 0) {
            executado = 1;
            var controller = $(this).attr('data-controller')
            var filtro = {
                artigo_id: '',
                procedimento_gasto_id: '',
                quantidade: '',
                ajax: 1
            }
            $(this).loadGrid(site_path + '/' + controller + '/artigos', filtro, '.arts', loadComponentes);
        }
    })

    $(documento).find("button:submit").click(function () {
        if (empty($(this).attr('onclick')) && empty($(this).attr('listen'))) {
            SendForm(documento.find('form'));
        }
    })

    $(documento).find("input:submit").click(function () {
        if (empty($(this).attr('onclick')) && empty($(this).attr('listen'))) {
            SendForm(documento.find('form'));
        }
    })

    $(documento).find("#tabs-executores").each(function () {
        $(this).children('.active').children().click();
    })

//Faturamento Pre-faturamento
    $('input:checkbox[name="selecionados[]"]').on('ifClicked', function (event) {
        var posicao = $(this).attr('data-posicao');
        var checked = $(this).is(':checked');
        var situacao = 1;
        if (!checked) {
            $('.marcar' + posicao).addClass('bg-success');
            situacao = 2;
        } else {
            $('.marcar' + posicao).removeClass('bg-success');
        }
        ajaxSalvarSelecionado($(this).val(), null);
    });

//Faturamento Encerramento
    $('input:checkbox[name="selecionadostemp[]"]').on('ifClicked', function (event) {
        var posicao = $(this).attr('data-posicao');
        var checked = $(this).is(':checked');
        if (!checked) {
            $('.marcar' + posicao).addClass('bg-success');
        } else {
            $('.marcar' + posicao).removeClass('bg-success');
        }
        ajaxSalvarSelecionadoTemp($(this).val(), null);
    });

    $('input:checkbox[name=vincular_iten]').on('ifClicked', function (event) {
        $.ajax({
            type: 'POST',
            url: site_path + '/AtendimentoitenArtigos/vincular',
            data: {
                id: $(this).val()
            },
            success: function (txt) {
            }
        })
    });

    $('input:checkbox[name=vincular_face]').on('ifClicked', function (event) {
        $.ajax({
            type: 'POST',
            url: site_path + '/FaceArtigos/vincular',
            data: {
                id: $(this).val()
            },
            success: function (txt) {
            }
        })
    });


    $('input:radio[name=tipo]').on('ifChecked', function (event) {
        if ($(this).val() == 'd') {
            $('input:checkbox[name="regioes[]"]').iCheck('disable');
            $('input:checkbox[name="dentes[]"]').iCheck('enable');
        } else {
            $('input:checkbox[name="dentes[]"]').iCheck('disable');
            $('input:checkbox[name="regioes[]"]').iCheck('enable');
        }
    });

    $('#search-billing').on('change', function (event) {
        console.log($('#search-billing').val());
        if ($('#search-billing').val() == '') {
            $('#search-box-period-billing').css('visibility', 'hidden');
        } else {
            $('#search-box-period-billing').css('visibility', 'visible');
        }
    });


    $(documento).find("button#btn-voltar").click(function () {
        Navegar('', 'back')
    })

    $('input:checkbox[name="stodos"]').on('ifUnchecked', function (event) {
        if ($(this).val() == 'd') {
            var tipo = 'input:checkbox[name="dentes[]"]';
        } else {
            var tipo = 'input:checkbox[name="regioes[]"]';
        }
        $(tipo).each(
            function () {
                $(this).iCheck('uncheck');
            }
        );

    });
    $('input:checkbox[name="stodos"]').on('ifUnchecked', function (event) {
        if ($(this).val() == 'd') {
            var tipo1 = 'input:checkbox[name="dentes[]"]';
        } else {
            var tipo1 = 'input:checkbox[name="regioes[]"]';
        }
        $(tipo).each(
            function () {
                $(this).iCheck('uncheck');
            }
        );


    });

    $('input:checkbox[name="stodos"]').on('ifChecked', function (event) {
        if ($(this).val() == 'd') {
            var tipo = 'input:checkbox[name="dentes[]"]';
            var tipo2 = 'input:checkbox[name="regioes[]"]';

        } else {
            var tipo = 'input:checkbox[name="regioes[]"]';
            var tipo2 = 'input:checkbox[name="dentes[]"]';

        }
        $(tipo).each(
            function () {
                $(this).iCheck('check');
            }
        );

        $(tipo2).each(
            function () {
                $(this).iCheck('uncheck');
            }
        );

    });

    $(documento).find('#valorbruto,#juros,#multa,#desconto').blur(function () {
        calculaValor();
    });

    $('input:checkbox[name=financeiro_iten]').on('ifClicked', function (event) {
        $.ajax({
            type: 'POST',
            url: site_path + '/Atendimentos/vincular',
            data: {
                id: $(this).val(),
                origen: 'f',
                tipo: $(this).attr('data-tipo')
            },
            success: function (txt) {
            }
        })
    });

    $('input:checkbox[name=executor_iten]').on('ifClicked', function (event) {
        $.ajax({
            type: 'POST',
            url: site_path + '/Atendimentos/vincular',
            data: {
                id: $(this).val(),
                origen: 'e',
                tipo: $(this).attr('data-tipo')
            },
            success: function (txt) {
            }
        })
    });


    $(documento).find('input:checkbox[name=checkacesso]').on('ifClicked', function (event) {
        $.ajax({
            type: 'POST',
            url: site_path + '/GrupoControladorAcoes/permitir',
            data: {
                id: $(this).val()
            },
            success: function (txt) {
            }
        })
    });

    $(documento).find("#procedimento-gasto-id").each(function () {
        $(this).change(function () {
            if (empty($(this).val())) {
                return false;
            }
            var controller = $(this).attr('data-controller')

            var filtro = {
                artigo_id: '',
                procedimento_gasto_id: $(this).val(),
                quantidade: $(documento).find("#quantidade").val(),
                ajax: 1
            }
            $.when(
                $(documento).find(".list-artigos").loadGrid(site_path + '/' + controller + '/artigos', filtro, '.arts', loadComponentes)
            ).then(function () {
                $(documento).find("#procedimento-gasto-id").val('').trigger('change');
            })

        })
    })

    $(documento).find("#artigo_id").each(function () {
        $(this).change(function () {
            if (empty($(this).val())) {
                return false;
            }
            var controller = $(this).attr('data-controller')
            var filtro = {
                artigo_id: $(this).val(),
                quantidade: $(documento).find("#quantidade").val(),
                procedimento_gasto_id: '',
                ajax: 1

            }
            $.when(
                $(documento).find(".list-artigos").loadGrid(site_path + '/' + controller + '/artigos', filtro, '.arts', loadComponentes)
            ).then(function () {
                $(documento).find("#artigo_id").val('').trigger('change');
            })

        })
    })

    $(documento).find("[data='deletar-art']").each(function () {
        $(this).click(function () {
            var pos = $(this).attr('data-id')
            var controller = $(this).attr('data-controller')
            BootstrapDialog.confirm('Tem certeza que deseja excluir este artigo?', function (result) {
                if (result) {
                    var filtro = {
                        artigo_id: '',
                        procedimento_gasto_id: '',
                        quantidade: '',
                        ajax: 1

                    }

                    $.ajax({
                        type: 'GET',
                        url: site_path + '/' + controller + '/dell-artigo',
                        data: {
                            pos: pos
                        },
                        success: function (txt) {
                            $(documento).find(".list-artigos").loadGrid(site_path + '/' + controller + '/artigos', filtro, '.arts', loadComponentes);
                        }
                    })
                }
            });
        })
    })

    $(documento).find("#check_artigos").click(function () {
        if (!empty(click)) {
            return false;
        }
        click = 1;
        if ($(documento).find("#quantidade_artigos").val() <= 0) {
            BootstrapDialog.show({
                title: "Atenção",
                message: "Para continuar selecione algum iten!",
                type: BootstrapDialog.TYPE_WARNING,
                closable: false,
                buttons: [{
                    label: 'OK',
                    cssClass: 'btn-warning',
                    action: function (dialogRef) {
                        dialogRef.close();
                        click = null;
                    }
                }]
            })
            return false;
        }
        var form = $(documento).find("#form-artigos")
        $(form).ajaxForm({
            beforeSubmit: function () {
                LoadGif()
            },
            success: function (txt) {
                click = null;
                CloseGif();
                Navegar('', 'back');
            }
        }).submit();

    })

    $(element).find(".edit_local").each(function () {
        input_editable($(this), element)
    })

    $(documento).find(".btn-executar").each(function () {
        $(this).click(function () {
            if (empty($(this).attr('listen'))) {
                if (!empty(this.href)) {
                    Navegar(this.href, 'go');
                }
                return false;
            }
        })
    })

    $(element).find("[role='tab-exec']").click(function () {
        if (empty(click)) {
            fil_.tipo = $(this).attr('tipo')
            fil_.atendimento_procedimento = $(this).attr('data-id')
            fil_.historico = $(this).attr('data-historico')
            carregarAba2($(this).attr('aria-controls'), $(this).attr('controller'), $(this).attr('action'), fil_);
        }
        click = 1;

    })

    $(element).find("#btn-hist-tratamentos").each(function () {
        if (!empty(dialog_executor)) {
            return false;
        }

        $(this).click(function () {
            if (empty(element.find("#medico-id").val())) {
                BootstrapDialog.alert("Para continuar\n selecione um Profissional!");
                return false;
            }

            if (empty(element.find("#descricao").val())) {
                BootstrapDialog.alert("Para continuar\n informe uma descrição!");
                return false;
            }

            var id_cliente = $(element).find("#cliente-id").val()
            var url = site_path + "/Atendimentos/financeiro-executor/" + id_cliente + "/?ajax=1&historico=1";
            Acrescentar(url);
            dialog_executor = new BootstrapDialog({
                title: 'Procedimentos',
                message: $('<div></div>').load(url, inicializar),
                closable: false,
                id: 'modal_lg',
                size: BootstrapDialog.SIZE_WIDE,
                type: BootstrapDialog.TYPE_PRIMARY,
                buttons: [
                    {
                        label: 'Cancelar',
                        cssClass: 'btn-danger',
                        action: function (dialogRef) {
                            g2iNavObject.navegacao = [{"p": 1, "u": site_path}];
                            dialog_executor = null;
                            dialogRef.close();
                        }
                    },
                    {
                        label: 'Finalizar',
                        cssClass: 'btn-success',
                        action: function (dialogRef) {
                            BootstrapDialog.confirm({
                                title: 'Atenção',
                                message: "Já realizou todos os vínculos? \n Ao fechar esta janela, o procedimento será salvo!. ",
                                callback: function (result) {
                                    if (result) {
                                        $(element).find("#salve-tratamento").click();
                                        dialog_executor = null;
                                        dialogRef.close();
                                    }
                                }
                            });


                        }
                    }],
                onshown: function (dialog) {
                    dialog.getModal().removeAttr('tabindex');
                }
            });
            dialog_executor.open();
        })
    })

    $(element).find(".edit_resp").each(function () {
        var medico_id = $(this).attr('medico-id');
        var medico_nome = $(this).attr('medico-nome');
        $(this).editable({
            url: site_path + '/' + $(this).attr('data-controller') + '/parcial-edit',
            name: $(this).attr('data-title'),
            params: function (params) {
                params.tipo = $(this).attr('data-tipo')
                params.origen = $(this).attr('data-origem')
                return params;
            },
            mode: 'inline',
            title: $(this).attr('data-title'),
            select2: {
                language: 'pt-BR',
                theme: 'bootstrap',
                width: "100%",
                placeholder: {
                    id: '-1',
                    text: 'Selecione'
                },
                initSelection: function (element, callback) {
                    callback({id: medico_id, text: medico_nome});
                },
                minimumInputLength: 1,
                ajax: {
                    url: site_path + '/' + $(this).attr('data-find') + '/fill',
                    dataType: 'json',
                    cache: true,
                    type: 'post',
                    data: function (params) {
                        return {
                            termo: params.term,
                            size: 10,
                            page: params.page
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.dados,
                            pagination: {
                                more: data.more
                            }
                        }
                    }
                }
            },
            success: function (response, newValue) {
                if (response.res == 'erro') {
                    BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                } else {
                    $(element).find("#tabs-executores").each(function () {
                        $(this).children('.active').children().click();
                    })
                }
            }
        }).on('shown', function () {
            form_campos(element)
        });
    })

    $(element).find(".edit_resp_status").each(function () {
        $(this).editable({
            url: site_path + '/' + $(this).attr('data-controller') + '/parcial-finalizar',
            name: $(this).attr('data-title'),
            params: function (params) {
                params.tipo = $(this).attr('data-tipo')
                params.origen = $(this).attr('data-origem')
                return params;
            },
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            source: [
                {value: 1, text: 'Sim'},
                {value: 0, text: 'Não'}
            ],
            success: function (response, newValue) {
                if (response.res == 'erro') {
                    BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                } else {
                    $(element).find("#tabs-executores").each(function () {
                        $(this).children('.active').children().click();
                    })
                }
            }
        }).on('shown', function () {
            form_campos(element)
        });

    })
    $(element).find(".fin_tratamento").each(function () {
        $(this).editable({
            url: site_path + '/' + $(this).attr('data-controller') + '/finalizar-atendimento',
            name: $(this).attr('data-title'),
            mode: 'inline',
            title: $(this).attr('data-title'),
            value: $(this).attr('data-value'),
            source: [
                {value: 0, text: 'Em andamento'},
                {value: 1, text: 'Finalizado'}
            ],
            success: function (response, newValue) {
                if (response.res == 'erro') {
                    BootstrapDialog.alert('Erro ao salvar valor!\n Tente novamente mais tarde!');
                } else {
                    recarregar();
                }
            }
        }).on('shown', function () {
            form_campos(element)
        });

    })
    $(element).find("#indicacao_id").each(function () {
        $(this).change(function () {
            if ($(this).find('option:selected').text() == 'Outros Pacientes') {
                $(element).find('#indicacao-itens').hide()
                $(element).find('#cliente-itens').show()
                setTimeout(function () {
                    form_campos(element)
                }, 1000);
            } else {
                $(element).find('#indicacao-itens').show();
                $(element).find('#cliente-itens').hide();
                $.ajax({
                    type: 'GET',
                    url: site_path + '/IndicacaoItens/itens-select',
                    data: {
                        indicacao_id: $(this).val()
                    },
                    dataType: 'JSON',
                    success: function (txt) {
                        CloseGif();

                        if (txt == -1) {
                            BootstrapDialog.alert('Erro ao listar itens!\n Tente mais tarde!');
                            return false;
                        }

                        $(element).find('#indicacao-itens-id').empty()
                            .append('<option value="">Selecione</option>');

                        $.when(
                            $.each(txt, function (key, value) {
                                $(element).find('#indicacao-itens-id').append(new Option(value.nome, value.id));
                            })
                        ).then(function () {
                            $(element).find('#indicacao-itens-id').trigger('change');
                        })
                    }
                })
            }
        })
    })
    $(element).find('#valorbruto,#juros,#multa,#desconto').blur(function () {
        calculaValor();
    });


    $(documento).find(".area a").click(function () {
        if (empty($(this).attr('listen'))) {
            if (!empty(this.href)) {
                Navegar(this.href, 'go');

            }
            return false;
        }
    });

    /** TODO new Pagination AND new Ordenation
     *  Foi mandado para misc.js - luciano 21/03/2018 11:08
     */
    /*$(element).on('click','.paginator a, .table thead a', function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        if(url != "")
        {            
            $.ajax({
                type: 'GET',
                url: url,
                beforeSend: () => {
                    LoadGif();
                },
                success: (data) => {
                    CloseGif();
                    let content = $('<div>').append(data).find('.table-responsive');                    
                    $(".table-responsive").html(content);
                },
                error: () => {
                    CloseGif();
                    toastr.error(msgErrorAjax);
                },
            });
            //$(".table-responsive").load(url+ " .table-responsive", carregar);
        }
        return false;
    });*/
    /*

     $(element).on('click', '.ordenacao a', function (e) {
     e.preventDefault();
     var url = $(this).attr('href');
     if(url != "")
     {
     $(".table-responsive").load(url+ " .table-responsive", function(){

     })
     }
     return false;
     });
     */

    //TODO Paginação
    /* $(element).find(".paginator a").click(function () {
     if (!empty($(element).find('.area_pront').html())) {
     var caminho = this.href;
     $(element).find(".area_pront div.table-responsive").loadNewGrid(caminho, null, null, loadComponentes);
     } else {
     if (!empty(this.href)) {
     var caminho = this.href;
     var form = $(element).find('form').serialize()
     $(element).find("div.table-responsive").loadNewGrid(caminho, form, null, loadComponentes);
     }
     }
     return false;
     });*/

    //TODO ordenacao
    /*$(element).find(".table-responsive thead a").click(function () {
     if (!empty($(element).find('.area_pront').html())) {
     var caminho = this.href;
     $(element).find(".area_pront div.table-responsive").loadNewGrid(caminho, null, null, carregar);
     } else {
     if (!empty(this.href)) {
     var caminho = this.href;
     var form = $(element).find('form').serialize()
     $(element).find("div.table-responsive").loadNewGrid(caminho, form, null, carregar);
     }
     }
     return false;
     })*/

    /* Foi colocado no script controllers/Agendas.js
    $(element).find('button.validation-agenda').each(function () {

        $(this).click(function (event) {
            var inicio = $(element).find('#inicio');
            var fim = $(element).find('#fim');
            var grupo_id = $(element).find('#grupo-id');
            var id_form = $(this).parent().parent().attr('id');
            var agenda_id = $(element).find('#agenda-id').val();
            var convenio_id = $('#convenio-id');

            if (empty(grupo_id.val())) {
                grupo_id.focus();
                required('Atenção', 'Informe uma agenda!');
                return false;
            }
            if (empty(inicio.val())) {
                required('Atenção', 'Informe o periodo Inicio!');
                return false;
            }
            if (empty(fim.val())) {
                required('Atenção', 'Informe o periodo Fim!');
                return false;
            }
            if (empty(convenio_id.val())) {
                required('Atenção', 'Informe um convênio!');
                return false;
            }
            if(fim.val() <= inicio.val()){
                required('Atenção', 'A hora fim não pode ser Menor/Igual a hora inicio!');
                return false;
            }

            var validation_agenda = validationAgenda(grupo_id.val(), convertDatetime(inicio.val()), convertDatetime(fim.val()), agenda_id);
            validation_agenda.success(function (txt) {
                CloseGif();
                if(!empty(txt)){
                    BootstrapDialog.alert({
                        title: 'Atenção',
                        type: BootstrapDialog.TYPE_DANGER,
                        message: txt.msg
                    });
                    return false;
                }else{
                    $(this).attr('type', 'submit');
                    event.preventDefault();
                    $(element).find('form#'+id_form).ajaxForm({
                        beforeSubmit: function () {
                            LoadGif()
                        },
                        success: function (txt) {
                            CloseGif();
                            $('.modal-content').find('button.close').click();
                        }
                    }).submit();
                }
            });
        });
    });*/

    $(element).find("#form").each(function () {
        if ($(this).val() == 'edit') {

            if ($(element).find('#situacao-agenda-id').val() == 10) {
                if (!empty($(element).find('#button_editar'))) {
                    $(element).find("#button_editar").addClass('disabled');
                    $(element).find("#button_editar").attr('disabled', 'disabled');
                }
                if (!empty($(element).find('#btneditar'))) {
                    $(element).find("#btneditar").attr('disabled', 'disabled');
                    $(element).find("#btneditar").addClass('disabled');
                }
            }


            $(element).find('#situacao-agenda-id').change(function () {
                if ($(element).find('#cliente-id').val() == '-1') {
                    if ($(this).val() == 6) {
                        BootstrapDialog.alert("Para alterar a situação deve ter um cadastro para o paciente!");
                        $(this).val(1).trigger('change');
                    }
                    return false;
                }
                if ($(this).val() == 5 || $(this).val() == 4 || $(this).val() == 3) { // se igual a finalizado
                    if (!empty(sit)) {
                        return false;
                    }
                    if ($(element).find("#situacao-anterior").val() == 5) {
                        return false;
                    }
                    sit = $(this).val();
                    /* dialog_sub = new BootstrapDialog({
                     title: 'Finalizar Agendamento',
                     message: $('<div></div>').load(site_path + "/Agendas/finalizar/" + $(element).find('#agenda-id').val() + "?ajax=1", inicializar),
                     closable: false,
                     buttons: [{
                     label: 'Cancelar',
                     cssClass: 'btn-primary',
                     action: function (dialogRef) {
                     BootstrapDialog.confirm("Você deseja realmente cancelar esta operação? \n Se esta operação for cancelada, a anterior também será. ", function (result) {
                     if (result) {
                     dialog_sub = null;
                     sit = null;
                     dialogRef.close();
                     dialog_modelos.close();
                     }
                     });

                     }
                     },
                     {
                     label: 'Finalizar',
                     cssClass: 'btn-danger',
                     action: function (dialogRef) {
                     LoadGif();
                     var dialogFaces = dialogRef.getModalBody();
                     dialogFaces.find('#fim-agenda').ajaxForm({
                     success: function (txt) {
                     dialog_sub = null;
                     sit = null;
                     dialogRef.close();
                     var dialogAnterior = dialog_modelos.getModalBody();
                     dialogAnterior.find('#frm-agenda').ajaxForm({
                     success: function (txt) {
                     CloseGif();
                     $('#fill-agenda').click();
                     dialog_modelos.close();
                     }
                     }).submit();
                     }
                     }).submit();
                     }
                     }
                     ],
                     });
                     dialog_sub.open();*/
                }
            });
        } else {
            $(element).find('#situacao-agenda-id').change(function () {
                if ($(element).find('#cliente-id').val() == '-1') {
                    if ($(this).val() == 6) {
                        BootstrapDialog.alert("Para alterar a situação deve ter um cadastro para o paciente!");
                        $(this).val(1).trigger('change');
                    }
                    return false;
                }
            })
        }
    })

    $(element).find('#previsao').blur(function () {
        $.ajax({
            type: 'POST',
            url: site_path + '/Retornos/getdays',
            data: {
                date: $(this).val()
            },
            dataType: 'JSON',
            success: function (txt) {
                $(element).find('#dias').val(txt.result);
            }
        })
    })

    $(element).find('#dias').blur(function () {
        $.ajax({
            type: 'POST',
            url: site_path + '/Retornos/getdays',
            data: {
                dias: $(this).val()
            },
            dataType: 'JSON',
            success: function (txt) {
                $(element).find('#previsao').val(txt.result);
            }
        })
    })

    $(element).find("#add-provisorio").click(function () {

        /*$(element).find("#situacao-agenda-id").val(5).trigger('change');
        var html = "<input type='hidden' name='cliente_id' id='cliente-id' value='-1'/>";
        $(this).parent().parent().html(html);
        $(element).find(".cli").css({"display": "none"})
        $(element).find(".prov").css({"display": "block"})
        $(element).find(".prov input").closest('.form-group').addClass('required');
        $(element).find(".prov input").attr({'required': true, 'escape-validation': 1});
        $(element).find(".prov input").removeAttr('not-required');
        $(element).find("#cliente_id").attr('escape-validation', 0);*/
        //carregar()
    });

//TODO Funcao que checa retornos marcados
    /*$(element).find("[data-chech='marcados']").each(function () {
     $(this).change(function () {
     $(element).find('.retorn').css({"display": "none"})
     LoadGif();
     $.ajax({
     type: 'GET',
     url: site_path + '/Retornos/retornos_marcados',
     data: {
     cliente_id: $(this).val()
     },
     dataType: 'JSON',
     success: function (txt) {
     CloseGif();
     if (empty(txt.result)) return false;

     $(element).find('.retorn').css({"display": "block"});
     $(element).find('#retorno_agenda').empty()
     .append('<option value="">Selecione</option>')
     $.when(
     $.each(txt.result, function (key, value) {
     $(element).find('#retorno_agenda').append(new Option(value.nome, value.id));
     })
     ).then(function () {
     $(element).find('#retorno_agenda').trigger('change');
     })

     }
     })
     })
     })*/

    $(element).find('#retorno_agenda').each(function () {
        $(this).change(function () {
            if (!empty($(this).val())) {
                $(element).find('#confirma-retorno').attr('required', 'required');
            } else {
                $(element).find('#confirma-retorno').removeAttr('required');
            }

            $.ajax({
                type: 'POST',
                url: site_path + '/Retornos/procedimento',
                data: {
                    retorno_id: $(this).val()
                },
                dataType: 'JSON',
                beforeSend: function () {
                    LoadGif()
                },
                success: function (txt) {
                    CloseGif();
                    if (txt != 1) {
                        if (!empty($(element).find('#procedimento').val())) {
                            $(element).find('#procedimento').val($(element).find('#procedimento').val() + ' - ' + txt.procedimento);
                        } else {
                            $(element).find('#procedimento').val(txt.procedimento);
                        }
                    }
                }
            })
        })
    });

    $(element).find('#frm-agenda,#form-retorno').each(function () {
        var form_agenda = $(element).find('#frm-agenda');
        var form = $(this)
        $(form).find('#inicio').blur(function () {
            if (!empty($(this).val())) {
                $.ajax({
                    type: 'POST',
                    url: site_path + '/Agendas/getminutes',
                    data: {
                        date: $(this).val(),
                        grupo_id: $(form_agenda).find('#grupo-id').val()
                    },
                    dataType: 'JSON',
                    beforeSend: function () {
                        LoadGif();
                    },
                    success: function (txt) {
                        CloseGif();
                        $(form).find('#fim').val(txt.result);
                    }
                });
            }
        })
    })
    $(element).find("#prox-data").click(function () {
        if (empty(click)) {
            click = 1;
            var dat = moment($(element).find("#inicio").val(), 'DD/MM/YYYY').add(1, 'days').format('DD/MM/YYYY');
            $time = $(element).find("#inicio").data("DateTimePicker");
            $time.date(dat)
            if (!empty($(element).find("#btn-fill").html())) {
                $(element).find("#btn-fill").click();
            }
        }
    });

    $(element).find("#ant-data").click(function () {
        if (empty(click)) {
            click = 1;
            var dat = moment($(element).find("#inicio").val(), 'DD/MM/YYYY').subtract(1, 'days').format('DD/MM/YYYY');
            $time = $(element).find("#inicio").data("DateTimePicker");
            $time.date(dat)
            if (!empty($(element).find("#btn-fill").html())) {
                $(element).find("#btn-fill").click();
            }
        }
    });
    $(element).find('#fill-agendamentos').each(function () {
        var form = $(this)

        $(form).find('#inicio').on('dp.change', function (e) {
            $(element).find("#fim").val(e.date.format('L'))
            /*if (!empty($(element).find("#btn-fill").html())) {
                $(element).find("#btn-fill").click();
            }*/
        })
    })


    $(element).find('[check="agendamento"]').each(function () {
        var instancia = $(this)
        $(this).blur(function () {
            var result_check = checar_agendamentos($(element).find('#grupo-id').val(), moment($(this).val(), 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'));
            result_check.success(function (txt) {
                CloseGif();
                if (txt.res > 0) {
                    instancia.val('');
                    $(element).find('#fim').val('');
                    BootstrapDialog.alert({
                        title: 'Atenção',
                        type: BootstrapDialog.TYPE_DANGER,
                        message: "Já existe um agendamento marcado para este horário!"
                    })
                }
            })
        })
    })

    $(element).find('#list-anteriores').each(function () {
        $(this).click(function () {
            BootstrapDialog.show({
                title: 'Retornos Anteriores',
                message: $('<div></div>').load($(this).attr('href') + ' .ibox', carregar),
                closable: true,
                size: BootstrapDialog.SIZE_WIDE
            });
            return false;
        })
    });

    $(element).find('#situacao-retorno-id').each(function () {
        $(this).change(function () {
            removeAtributos();
            if ($(this).val() == 3) {
                $(element).find('#area-agenda').hide();
                $(element).find('#data-futura').show();
                $(element).find('#data-futura').html('<div class="form-group col-md-6 text"><label class="control-label" for="futuro">Data Futura</label><div class="input-group"><input type="text" name="futuro" class="form-control datepicker" id="futuro" maxlength="10" autocomplete="off" required="required" ><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="glyphicon glyphicon-calendar"></i></button></span></div></div>');
            }
            else if ($(this).val() == 2) {
                $(element).find('#data-futura').hide();
                $(element).find('#data-futura').html('');
                $(element).find('#area-agenda').show();
                $(element).find('#btn-agenda').show();

                setAtributos();
            }
            else if ($(this).val() == 4) {
                $(element).find('#btn-agenda').hide();
                $(element).find('#area-agenda').hide();
                $(element).find('#data-futura').show();
                $(element).find('#data-futura').html('<div class="form-group col-md-12 textarea"><label class="control-label" for="justificativa">Justificativa</label><textarea name="justificativa" class="form-control" id="justificativa" placeholder="Justificativa de exclusão" rows="5" required></textarea></div>');

            }

            initDocument(element)

        })
    });

    function removeAtributos() {
        $(element).find('#area-agenda input').each(function () {
            $(this).removeAttr('required');
        })
        $(element).find('#area-agenda select').each(function () {
            $(this).removeAttr('required');
        })
    }

    function setAtributos() {
        $(element).find('#area-agenda input').each(function () {
            $(this).attr('required', 'required');
        })
        $(element).find('#area-agenda select').each(function () {
            $(this).attr('required', 'required');
        })
    }

    form_campos(element);


    $('.s-procedimento').select2({
        language: 'pt-BR',
        theme: 'bootstrap',
        width: '100%',
        placeholder: {
            id: '-1',
            text: 'Selecione um Procedimento'
        },
        minimumInputLength: 3,
        ajax: {
            url: site_path + "/Procedimentos/fill",
            dataType: 'json',
            cache: true,
            type: 'post',
            data: function (params) {
                return {
                    termo: params.term,
                    size: 10,
                    page: params.page
                };
            },
            processResults: function (data) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
                return {
                    results: data.dados,
                    pagination: {
                        more: data.more
                    }
                }
            }
        }
    });


    $(element).find('#cliente-id').each(function () {
        if (empty($(this).attr('local'))) {
            $(this).change(function () {
                var fil = {
                    'cliente_id': $(this).val(),
                    'default': $(element).find('#grupo-id').val()
                };
                getOptions('ClienteGrupoRetornos', 'fill', fil, 'grupo-id');
            });
        }
    });

//funcao padrao
    $(element).find("[data='modal']").each(function () {
        $(this).click(function () {
            var caminho = site_path + '/'
                + $(this).attr('controller') + '/'
                + $(this).attr('action') + '/?ajax=1';
            var fill = $(this).attr('vinculo').split(',');
            $.when(
                $.each(fill, function (key, value) {
                    if (value.indexOf("-") > -1) {
                        caminho += '&' + value.replace('-', '_') + '='
                        caminho += $(element).find('#' + value).val()
                    } else {
                        caminho += '&' + value + '='
                        caminho += $(element).find('#' + value.replace('_', '-')).val()
                    }
                })
            ).then(function () {
                dialog_alternativo = new BootstrapDialog({
                    title: 'Cadastro',
                    message: $('<div></div>').load(caminho, inicializar),
                    closable: true
                });
                dialog_alternativo.open();
            });
        })
    })

    $(element).find('#cp5').colorpicker({
        format: 'rgba',
        horizontal: true
    });

    // Divide o calendario de acordo com grupo agenda
    $(element).find('#fill-agenda').click(function () {

        if (!empty($(element).find("#nome-provisorio").val())) {
            var fill = {
                nome_provisorio: $(element).find("#nome-provisorio").val(),
                ajax: 1,
                first: 1
            }
            var mod = $("#modal_lg")
            mod.modal('show');

            mod.find(".modal-content").loadGrid(site_path + '/Agendas/modal', fill, '.this-place', loadComponentes);
            $(element).find("#nome-provisorio").val('')
            return false;
        }

        if (!empty($(element).find("#grupo-id").val())) {
            if ($(element).find("#grupo-id").val().length > 1) {
                var aux = 1;
                $.each($(element).find("#grupo-id").val(), function (key, value) {
                    var div = document.createElement("td")
                    div.setAttribute('id', 'calendar' + value)
                    div.setAttribute('grupo-id', value)
                    if (parseInt($(element).find("#grupo-id").val().length) <= 12) {
                        var res = 12 / parseInt($(element).find("#grupo-id").val().length);
                        div.setAttribute('class', 'col-md-' + Math.round(res))
                    } else {
                        div.setAttribute('class', 'col-md-1')
                    }
                    div.setAttribute('style', 'min-width:150px')
                    if (aux == 1) {
                        $(element).find('#area_cal').html(div)
                    } else {
                        $(element).find('#area_cal').append(div);
                    }
                    aux++;
                    agenda(element, value, 2);
                })
                $(".fc-toolbar h2").css({
                    'font-size': '20px'
                })
                var clearfix = document.createElement("div")
                clearfix.setAttribute('class', 'clearfix');
                $(element).find('#area_cal').append(clearfix);
            } else {
                $.each($(element).find("#grupo-id").val(), function (key, value) {
                    var div = document.createElement("td")
                    div.setAttribute('id', 'calendar' + value)
                    div.setAttribute('grupo-id', value)
                    $(element).find('#area_cal').html(div)
                    agenda(element, value, 1);
                })

            }
        } else {
            var div = document.createElement("td")
            div.setAttribute('id', 'calendar')
            $(element).find('#area_cal').html(div)
            agenda(element, '', 1);
        }
    });

    $(element).find('#refresh-agenda').click(function () {
        var div = document.createElement("td")
        div.setAttribute('id', 'calendar')
        $(element).find('#area_cal').html(div)

        var data = $(element).find("#start").val();
        clear_all(element);
        $(element).find("#lista-agenda")[0].reset();
        $(element).find("#start").val(data);
        agenda(element, '', 1);
    });

    $(element).find('#tipoEstatisticaId').change(function () {
        let value = $(element).find('#tipoEstatisticaId').val();
        let divGrupo = $(element).find('#grupo_procedimento_estatistica');
        let divResultado = $(element).find('#tipo_resultado_estatistica');
        if (value == 10 || value == 11) {
            divGrupo.css('visibility', 'hidden');
            divGrupo.css('display', 'none');
            divResultado.css('visibility', 'hidden');
            divResultado.css('display', 'none');
        } else if (value == 9 || value == 11 || value == 13 || value == 15) {
            divGrupo.css('visibility', 'hidden');
            divGrupo.css('display', 'none');
            divResultado.css('visibility', 'hidden');
            divResultado.css('display', 'none');
        } else if (value == 8) {
            divGrupo.css('visibility', 'visible');
            divGrupo.css('display', 'inline');
            divResultado.css('visibility', 'hidden');
            divResultado.css('display', 'none');
        } else if (value == 12 || value == 14) {
            divGrupo.css('visibility', 'visible');
            divGrupo.css('display', 'inline');
            divResultado.css('visibility', 'hidden');
            divResultado.css('display', 'none');
        } else {
            divGrupo.css('visibility', 'visible');
            divGrupo.css('display', 'inline');
            divResultado.css('visibility', 'visible');
            divResultado.css('display', 'inline');
        }
    });

    $(element).find('#tipo_equipegrau').change(function () {
        let value = $(element).find('select[id=tipo_equipegrau]').val();
        $.ajax({
            type: 'GET',
            url: site_path + '/TipoEquipegrau/findById/' + value + '.json',
            dataType: 'JSON',
            success: function (txt) {
                $(element).find('#tipo_equipegrau_function_porcentual').attr('value', txt.tipoEquipegrau.porcentual);
            }
        })
    }).trigger('change');

    $(element).find('#tipo_imposto').change(function () {
        let value = $(element).find('select[id=tipo_imposto]').val();
        $.ajax({
            type: 'GET',
            url: site_path + '/convenioTipoimposto/findById/' + value + '.json',
            dataType: 'JSON',
            success: function (txt) {
                $(element).find('#porcentual_imposto').attr('value', txt.convenioTipoimposto.porcentual);
            }
        })
    }).trigger('change');
    ;
}

function check_procedimentos() {
    if (empty($("#total_procedimentos").val())) {
        required('Atenção', 'Não é permitido cadastrar atendimento sem procedimentos!');
        return false;
    } else {
        return true;
    }
}

function getTimeGroup(div_id, dateSql) {
    return $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/get-time-group',
        data: {
            id: div_id,
            date: dateSql

        },
        beforeSend: function () {
            LoadGif()
        },
        complete: function () {
            CloseGif();
        }
    });
}

function agenda(element, div_id, qtde) {
    var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iphone|ipod|ipad|android)/);
    var defaultView = $(element).find('#default-view').attr('default-view');

    var get_time_group = getTimeGroup(div_id, moment($(element).find('#start').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
    get_time_group.success(function (txt) {

        var periodos = new Array();
        var businessHours = new Array();
        var intervalo = txt.intervalo;
        if (txt.periodos.length > 0) {
            for (var i = 0; i < txt.periodos.length; i++) {
                periodos = {
                    start: txt.periodos[i].periodos.start,
                    end: txt.periodos[i].periodos.end,
                    dow: [txt.periodos[i].dia_semana]
                };                
                businessHours.push(periodos);
            }
        }

        CloseGif();

        $.when(
            $(element).find('#calendar' + div_id).each(function () {
                var tooltip = $('<div/>').qtip({
                    id: 'calendar',
                    prerender: true,
                    content: {
                        text: ' ',
                        title: {
                            button: true
                        }
                    },
                    position: {
                        my: 'center',
                        at: 'center',
                        target: $(this),
                        viewport: $(this),
                        adjust: {
                            mouse: false,
                            scroll: false
                        }
                    },
                    show: false,
                    hide: false,
                    style: {
                        classes: 'qtip-bootstrap',
                        width: 500,
                    },

                }).qtip('api');

                $(this).fullCalendar({
                    locale: 'pt-br',
                    height: 550,
                    //displayEventTime: false,
                    header: {
                        left: (qtde > 1) ? 'prev,next' : 'prev,next today',
                        center: 'title',
                        right: (qtde > 1) ? '' : 'agendaMonth,agendaWeek,agendaDay',
                    },
                    allDaySlot: false,
                    titleFormat: (qtde > 1) ? '[' + $(element).find('#grupo-id option[value=' + div_id + ']').text() + ']' : null,
                    minTime: txt.horarios.inicio,
                    maxTime: txt.horarios.fim,
                    slotLabelFormat: 'H:mm',
                    slotDuration: intervalo,
                    /*slotLabelInterval: '00:10:00',*/
                    snapDuration: intervalo,   
                    //slotEventOverlap: false, Se for false, desativa a transferencia de agendamento para uma agenda
                    defaultView: defaultView,
                    defaultDate: moment($('#start').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'),
                    selectable: true,
                    selectConstraint: 'businessHours',
                    eventConstraint: 'businessHours',
                    selectHelper: true,
                    eventOverlap: function (stillEvent, movingEvent) {
                        return stillEvent.allDay && movingEvent.allDay;
                    },
                    select: function (start, end) {
                        add_agenda(div_id, start.format(), end.format(), null);
                    },
                    dayClick: function (date, jsEvent, view) {
                        // Apenas para agenda mensal
                        if ($(element).find('.fc-agendaMonth-button').hasClass('fc-state-active')) {
                            var data = date.format();
                            window.open(site_path + '/Agendas/lista?grupos=' + div_id + '&data=' + data + '', '_blank');
                        }
                    },
                    businessHours: businessHours,
                    viewRender: function (view) {
                        $(element).find('#default-view').attr('default-view', view.name);
                        $(element).find('#calendar' + div_id + ' .fc-center h2').append(' (<strong id="qtd_pacientes">' + txt.pacientes + '</strong>)');
                    },
                    eventRender: function (event, eleme) {
                        
                        // coloca o icone do sms no evento
                        $(eleme).find('.fc-bg').prepend(event.icon);

                        eleme.bind('dblclick', function () {
                            tooltip.hide();
                            var check_agenda = agenda_atendimento(event.id);
                            check_agenda.success(function (txt) {
                                if(!txt.regra_edit_reserva.valida){
                                    toastr.warning(txt.regra_edit_reserva.msg);
                                    return false;
                                }else {
                                    if(event.event_empty){
                                        tooltip.hide();
                                        add_agenda(div_id, event.start.format(), event.end.format(), event.tipo_agenda);
                                        return false;
                                    }
                                    editar_agenda(event.id, div_id);
                                }
                            });
                            
                        });
                        eleme.bind('click', function () {
                            if(event.event_empty){
                                tooltip.hide();
                                add_agenda(div_id, event.start.format(), event.end.format(), event.tipo_agenda);
                                return false;
                            }
                            var check_agenda = agenda_atendimento(event.id);
                            var validation_agenda = validationAgenda(div_id, event.start.format(), event.end.format());
                            validation_agenda.success(function (txt) {
                                CloseGif();
                                if (txt.res > 0 && txt.erro == 'bloqueio') {
                                    BootstrapDialog.alert({
                                        title: 'Atenção',
                                        type: BootstrapDialog.TYPE_DANGER,
                                        message: txt.msg
                                    })
                                    $('#fill-agenda').click();
                                } else {
                                    check_agenda.success(function (txt) {
                                        CloseGif();
                                        if(!txt.regra_edit_reserva.valida){
                                            toastr.warning(txt.regra_edit_reserva.msg);
                                            return false;
                                        }
                                        var content = "<div class='actions'>";

                                        if(txt.situacao_agenda == 1500){//Reservado
                                            content += "<a href='Javascript:void(0)' onclick='editar_agenda(" + event.id + "," + div_id + ")' class='btn btn-warning btn-xs' toggle='tooltip' data-placement='top' title='Retirar Reserva'><i class='fa fa-unlock-alt'></i></a>";    
                                        }else {
                                            content += "<a href='Javascript:void(0)' onclick='editar_agenda(" + event.id + "," + div_id + ")' class='btn btn-success btn-xs'><i class='fa fa-pencil'></i></a>";
                                            content += "<a href='" + site_path + "/Agendas/view/" + event.id + "?ajax=1&first=1' data-toggle='modal' data-target='#modal_lg' class='btn btn-default btn-xs ' toggle='tooltip' data-placement='bottom' title='Visualizar'><i class='fa fa-file-archive-o'></i></a>";
                                            content += "<a href='" + site_path + "/LogAgendas/index/" + event.id + "' class='btn btn-default btn-xs ' toggle='tooltip' data-placement='bottom' title='Rastrear Agendamento' target='_blank'><i class='fa fa-list'></i></a>";
                                            if (txt.paciente_id == '-1') {
                                                content += "<a href='" + site_path + "/Clientes/add?agenda=" + event.id + "&ajax=1&first=1' data-toggle='modal' data-target='#modal_2' toggle='tooltip' data-placement='bottom' title='Cadastrar Paciente'  class='btn btn-primary btn-xs'><i class='fa fa-user'></i></a>";
                                            } else {
                                                content += "<a href='" + site_path + "/Clientes/edit/" + txt.paciente_id + "?agenda=" + event.id + "&ajax=1&first=1' data-toggle='modal' data-target='#modal_2' toggle='tooltip' data-placement='bottom' title='Editar Paciente'  class='btn btn-primary btn-xs'><i class='fa fa-user'></i></a>";
                                                content += "<a href='" + site_path + "/Clientes/prontuario/" + txt.paciente_id + "?agenda=" + event.id + "&ajax=1&first=1' data-toggle='modal' data-target='#modal_2' toggle='tooltip' data-placement='bottom' title='Prontuário'  class='btn btn-info btn-xs'><i class='fa fa-stethoscope'></i></a>";
                                            }
                                            if (txt.res <= 0) {
                                                if (txt.paciente_id == '-1') {
                                                    content += "<a href='Javascript:void(0)' class='btn btn-danger btn-xs ' toggle='tooltip' data-placement='bottom' title='Abrir Atendimento' onclick='required(\"Atenção\",\"Não é permitido abrir atendimento para pacientes não cadastrados!\")'><i class='fa fa-user-md'></i></a>";
                                                } else {
                                                    if (empty(txt.convenio_id)) {
                                                        content += "<a href='Javascript:void(0)' class='btn btn-danger btn-xs ' toggle='tooltip' data-placement='bottom' title='Abrir Atendimento' onclick='required(\"Atenção\",\"Não é permitido abrir atendimento sem convênio no agendamento! <br /> Por favor, edite o agendamento e adicione um convênio.\")'><i class='fa fa-user-md'></i></a>";
                                                    } else {
                                                        content += "<a href='" + site_path + "/Atendimentos/add/?agenda=" + event.id + "&ajax=1&first=1' data-toggle='modal' data-target='#modal_lg' class='btn btn-info btn-xs ' toggle='tooltip' data-placement='bottom' title='Abrir atendimento' data-id='' load-script='Atendimentos'><i class='fa fa-user-md'></i></a>";
                                                    }
                                                }
                                            } else {
                                                content += "<a href='" + site_path + "/Atendimentos/edit/" + txt.atendimento_id + "?agenda=" + event.id + "&ajax=1&first=1' data-toggle='modal' data-target='#modal_lg' class='btn btn-warning btn-xs ' toggle='tooltip' data-placement='bottom' title='Editar atendimento' load-script='Atendimentos'><i class='fa fa-user-md'></i></a>";
                                            }
    
                                            if (empty(txt.horario_gerado) || txt.paciente_id != '-1' || txt.convenio_id != '-1') {
                                                transferencia = {
                                                    agenda_id: event.id,
                                                    title: !empty(event.title) ? event.title.split('-')[0] : '',
                                                    grupo_agenda: $(element).find('#grupo-id option[value=' + div_id + ']').text(),
                                                    data_full: moment(event.start.format()).format('HH:mm') + ' - ' + moment(event.end.format()).format('HH:mm'),
                                                    color: event.color
                                                };
                                                content += '<a href="javascript:void(0)" class="btn btnTransferirAgendamento  btn-xs" onclick="addTransferencia(\'transferir\')" toggle="tooltip" data-placement="bottom" title="Transferir"><i class="fa fa-exchange"></i></a>';
                                                content += '<a href="javascript:void(0)" class="btn btnCopiarAgendameto btn-xs" onclick="addTransferencia(\'copiar\')" toggle="tooltip" data-placement="bottom" title="Copiar"><i class="fa fa-copy"></i></a>';
                                            }
    
                                            if (!empty(event.icon)) {
                                                if (event.tipo == 'respondido') {
                                                    content += "<a href='" + site_path + "/Agendas/sms/" + event.id + "?ajax=1&first=1' data-toggle='modal' data-target='#modal_lg' class='btn btn-warning btn-xs ' toggle='tooltip' data-placement='bottom' title='" + event.sms + "'>" + event.icon + "</a>";
                                                } else {
                                                    content += "<a class='btn btn-info btn-xs ' toggle='tooltip' data-placement='bottom' title='" + event.sms + "'>" + event.icon + "</a>";
                                                }
                                            }
    
                                            if(txt.situacao_agenda != 4) {
                                                content += "<a class='btn btn-success btn-xs' toggle='tooltip' data-placement='bottom' title='Confirmar Agendamento' onclick='confirmaAgendamento("+event.id+","+div_id+")'><i class='fa fa-check'></i></a>";
                                            }
    
                                            content += "</div>";
                                            content += "<br />"
                                            content += '<h4>' + event.start.format("DD/MM/YYYY HH:mm");
                                            if (!empty(event.end)) {
                                                content += ' a ' + event.end.format("DD/MM/YYYY HH:mm");
                                            }
                                            let event_title = !empty(event.title) ? event.title : "";
                                            content += '</h4>' + '<h5>' + event_title + '</h5>';
                                            //content += '</h4>' + '<h5>' + replaceAll(event.title, '.', "<br />") + '</h5>';
                                        }

                                        tooltip.set({
                                            'content.text': content
                                        }).reposition(eleme).show(eleme);

                                    })
                                }
                            });
                        });


                    },
                    eventAfterAllRender: function(view){

                        if(view.type == 'agendaDay' || view.type == 'agendaWeek'){
                            let hour = intervalo.split(':')[0];    
                            if(hour > 0){
                                $(element).find('.fc-widget-content').css('height', '150px');
                                $(window).trigger('resize');// Redimensionando o navegador manualmente
                            }
                        }else { // Apenas para agenda mensal
                            var dates = [];
                            for( cDay = view.start.clone(); cDay.isBefore(view.end) ; cDay.add(1, 'day') ){
                                var dateNum = cDay.format('YYYY-MM-DD');
                                dates.push(dateNum);
                            }
                            var ajaxGetHoursEmpty = getHoursEmpty(dates, div_id);
                            ajaxGetHoursEmpty.success(function(data){
                                for(let i=0;i<data.length;i++){
                                    let dayEl = $('.fc-day[data-date="' + data[i].date + '"]');
                                    let bg = (data[i].horarios_livres > 0) ? '28a745' : 'dc3545';
                                    let html = '<div class="event-count pull-left" style="background-color: #'+bg+'">'+data[i].horarios_livres+'</div>';
                                    dayEl.html(html);
                                }
                            });
                        }
                    },
                    eventResize: function (event) {
                        eventResizeAgenda(event.id, event.start.format(), event.end.format());
                        tooltip.hide();
                    },
                    eventDragStart: function () {
                        tooltip.hide();
                    },
                    eventDrop: function (event) {
                        eventDropAgenda(event.id, event.start.format(), event.end.format(), div_id);
                        tooltip.hide();
                    },
                    drop: function (date) {
                        let eventAction = $('#inputEvent').val();// copiar ou transferir agendamento
                        let durationInMinutes = txt.intervalo.split(':')[1];
                        let start = date.format();
                        let end = moment(start).add(durationInMinutes, 'minutes').format();

                        var drop_agenda = dropAgenda(transferencia.agenda_id, start, end, div_id, eventAction);
                        drop_agenda.success(function (txt) {
                            if (txt.error == 0) {
                                toastr.success(txt.msg);
                                $('#external-events').fadeOut('slow');
                                $('#area_cal td.fc-unthemed').each(function (index, value) {
                                    eventos(element, $(value).attr('grupo-id'), qtde);
                                });
                            }
                        });
                        tooltip.hide();
                    },
                    viewDisplay: function () {
                        tooltip.hide();
                    },
                    editable: true,
                    droppable: true,
                    eventColor: '#378006',
                    eventTextColor: '#000',
                    eventLimit: true,
                    eventLimitText: "Mais" // Default is `more` (or "more" in the lang you pick in the option)
                });
            })
        ).then(function () {
            eventos(element, div_id, qtde);
            $(element).find('.fc-prev-button').click(function () {
                $('#start').val($(element).find('#calendar' + div_id).fullCalendar('getView').start.format('DD/MM/YYYY'));
                var inicio = $(element).find('#calendar' + div_id).fullCalendar('getView').start.format();
                var fim = $(element).find('#calendar' + div_id).fullCalendar('getView').end.format();
                eventos2(element, div_id, qtde, inicio, fim)
            });

            $(element).find('.fc-next-button').click(function () {
                $('#start').val($(element).find('#calendar' + div_id).fullCalendar('getView').start.format('DD/MM/YYYY'));
                var inicio = $(element).find('#calendar' + div_id).fullCalendar('getView').start.format();
                var fim = $(element).find('#calendar' + div_id).fullCalendar('getView').end.format();
                eventos2(element, div_id, qtde, inicio, fim)
            });

        })

    });

}

function confirmaAgendamento(agenda_id, grupo_id)
{
    $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/confirma-agendamento/' + agenda_id,
        dataType: 'JSON',
        beforeSend: () => {
            LoadGif();
        },
        success: (data) => {
            if(data.error == 0) {
                toastr.success(data.msg);
                eventos(document, grupo_id, 1);
                $('.qtip').hide();
            }
            else {
                toastr.error(data.msg);
            }
        },
        complete: () => {
            CloseGif();
        },
        error: () => {
            CloseGif();
            toastr.error(msgErrorAjax);
        },
    });
}

function agenda_atendimento(id) {
    return $.ajax({
        type: 'POST',
        url: site_path + '/Atendimentos/check-agenda',
        data: {
            id: id
        },
        beforeSend: function () {
            LoadGif()
        }
    })
}

function eventResizeAgenda(agenda_id, start, end) {
    return $.ajax({
        url: site_path + '/Agendas/event-resize-agenda',
        type: 'POST',
        data: {
            start: start,
            end: end,
            id: agenda_id
        }
    });
}

function eventDropAgenda(agenda_id, start, end, grupo_id) {
    return $.ajax({
        url: site_path + '/Agendas/event-drop-agenda',
        type: 'POST',
        data: {
            start: start,
            end: end,
            id: agenda_id,
            grupo_id: grupo_id
        }
    });
}

function dropAgenda(agenda_id, start, end, grupo_id, eventAction) {
    return $.ajax({
        url: site_path + '/Agendas/drop-agenda',
        type: 'POST',
        data: {
            start: start,
            end: end,
            id: agenda_id,
            grupo_id: grupo_id,
            eventAction: eventAction
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
}
/**
 *  Traz horarios livres de acordo com mês selecionado no fullcalendar  
 * @param {*} dates 
 * @param {*} grupo_id 
 */
function getHoursEmpty(dates = array(), grupo_id)
{
    return $.ajax({
        url: site_path + '/Agendas/get-hours-empty',
        type: 'POST',
        data: {
            dates: dates,
            grupo_id: grupo_id,
        },
        beforeSend: function () {
            LoadGif();
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
}


function add_agenda(div_id, start, end, tipo_agenda) {
    var validation_agenda = validationAgenda(div_id, start, end);
    validation_agenda.success(function (txt) {
        CloseGif();
        if (txt.res > 0) {
            BootstrapDialog.alert({
                title: 'Atenção',
                type: BootstrapDialog.TYPE_DANGER,
                message: txt.msg
            })
            $('#fill-agenda').click();
        } else {
            dialog_modelos = new BootstrapDialog({
                closable: true,
                closeByBackdrop: false,
                closeByKeyboard: false,
                size: BootstrapDialog.SIZE_WIDE,
                title: 'Agendamento - Cadastrar',
                message: $('<div></div>').load(site_path + "/Agendas/add/?ajax=1&data=" + start + "&end=" + end + "&group=" + div_id + "&tipo_agenda=" + tipo_agenda, inicializar),
                buttons: [{
                    label: 'Cancelar',
                    cssClass: 'btn-default',
                    action: function (dialogRef) {
                        dialogRef.close();
                    }
                },
                {
                    icon: 'fa fa-save',
                    label: 'Salvar',
                    cssClass: 'btn-primary',
                    action: function (dialogRef) {
                        var dialogFaces = dialogRef.getModalBody();

                        if (empty(dialogFaces.find("#cliente_id").val()) && empty(dialogFaces.find("#nome-provisorio").val())) {
                            required('Atenção', 'Selecione um cliente cadastrado ou um nome previsório!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#data-nascimento").val()) && dialogFaces.find("#data-nascimento").attr('required') == 'required') {
                            required('Atenção', 'Digite a data de nascimento do paciente!');
                            return false;
                        }
                        if (!empty(dialogFaces.find('#idade').attr('required')) && dialogFaces.find('#idade').attr('required') === 'required' && empty(dialogFaces.find('#idade').val())) {
                            required('Atenção', 'Informe a idade do cliente!');
                            return false;
                        }
                        if (!empty(dialogFaces.find('#fone-provisorio').attr('required')) && dialogFaces.find('#fone-provisorio').attr('required') === 'required' && empty(dialogFaces.find('#fone-provisorio').val())) {
                            required('Atenção', 'Informe o celular!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#tipo-id").val())) {
                            required('Atenção', 'Selecione o tipo de agendamento!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#grupo-id").val())) {
                            required('Atenção', 'Selecione uma agenda!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#situacao-agenda-id").val())) {
                            required('Atenção', 'Selecione a situação do agendamento!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#convenio-id").val())) {
                            required('Atenção', 'Selecione um convênio!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#inicio").val())) {
                            required('Atenção', 'Informe o início da agenda!');
                            return false;
                        }
                        if (empty(dialogFaces.find("#fim").val())) {
                            required('Atenção', 'Informe o fim da agenda!');
                            return false;
                        }

                        if (dialogFaces.find('#fim').val() <= dialogFaces.find('#inicio').val()) {
                            required('Atenção', 'A hora fim não pode ser Menor/Igual a hora inicio!');
                            return false;
                        }

                        /** No add verifica se o atributo é obrigatório e se o valor da variável está vazio, neste caso, o usuário não preencheu o questionário obrigatório */
                        if (dialogFaces.find('.agenda-select-tipo #tipo-id option:selected').attr('data-obrigatorio-questionario') == 1 && empty(dialogFaces.find('#salvar-agenda-questionario').val())) {
                            required('Atenção', 'Preencha o Questionário. Obrigatório!');
                            return false;
                        }

                        /** No add verifica se o atributo é obrigatório e se o valor da variável está vazio, neste caso, o usuário não preencheu o procedimento obrigatório */
                        if (dialogFaces.find('.agenda-select-tipo #tipo-id option:selected').attr('data-obrigatorio-procedimentos') == 1 && empty($('#select-procedimento').val())) {
                            required('Atenção', 'Informe um ou mais Procedimentos!');
                            return false;
                        }

                        if (dialogFaces.find("#carteirinha-convenio").attr('required') == 'required') {
                            let matricula = dialogFaces.find("#carteirinha-convenio").val();
                            let regexOnlyZeros = /^0+$/.test(matricula);
                            let length = matricula.length;

                            if (regexOnlyZeros || length < 4) {
                                required('Atenção', 'Informe um Nº de Carteirinha/Matrícula válido!');
                                return false;
                            }

                            if (empty(matricula)) {
                                required('Atenção', 'Digite o Nº Carteirinha/Matrícula!');
                                return false;
                            }
                        }

                        if (empty(dialogFaces.find("#agenda-autorizacao-convenio-id").val()) && dialogFaces.find("#agenda-autorizacao-convenio-id").attr('required') == 'required') {
                            required('Atenção', 'Informe Autorização convênio!');
                            return false;
                        }

                        var validation_agenda = validationAgenda(div_id, moment($(dialogFaces).find('#inicio').val(), 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'), moment($(dialogFaces).find('#fim').val(), 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'));
                        validation_agenda.success(function (txt) {
                            if (txt.res > 0) {
                                BootstrapDialog.alert({
                                    title: 'Atenção',
                                    type: BootstrapDialog.TYPE_DANGER,
                                    message: txt.msg
                                })
                                $('#fill-agenda').click();
                            } else {
                                dialogFaces.find('#frm-agenda').ajaxForm({
                                    beforeSubmit: function () {
                                        LoadGif()
                                    },
                                    success: function (txt){
                                        CloseGif();
                                            /*$('#calendar' + div_id).fullCalendar('removeEventSources');
                                            $('#calendar' + div_id).fullCalendar('renderEvent', txt.result);*/
                                            //eventos(element, div_id, qtde);
                                            $('#fill-agenda').click();
    
                                            dialogRef.close();
                                    }
                                }).submit();
                            }
                        })
                    }
                }
                ],
                onshown: function (dialog) {
                    dialog.getModal().removeAttr('tabindex');
                },
            });
            dialog_modelos.open();
        }

    });
}

function editar_agenda(id, div_id) {
    dialog_modelos = new BootstrapDialog({
        size: BootstrapDialog.SIZE_WIDE,
        title: 'Editar Evento',
        message: $('<div></div>').load(site_path + "/Agendas/edit/" + id + "?ajax=1", inicializar),
        buttons: [{
            label: 'Cancelar',
            cssClass: 'btn-default',
            action: function (dialogRef) {
                dialogRef.close();
            }
        },
            {
                id: 'btneditar',
                label: 'Salvar',
                cssClass: 'btn-primary',
                action: function (dialogRef) {
                    var dialogFaces = dialogRef.getModalBody();

                    if (empty(dialogFaces.find("#cliente_id").val()) && empty(dialogFaces.find("#nome-provisorio").val())) {
                        required('Atenção', 'Selecione um cliente cadastrado ou um nome previsório!');
                        return false;
                    }
                    if (empty(dialogFaces.find("#tipo-id").val())) {
                        required('Atenção', 'Selecione o tipo de agendamento!');
                        return false;
                    }

                    if (!empty(dialogFaces.find('#fone-provisorio').attr('required')) && dialogFaces.find('#fone-provisorio').attr('required') === 'required' && empty(dialogFaces.find('#fone-provisorio').val())) {
                        required('Atenção', 'Informe o celular!');
                        return false;
                    }
                    
                    if (empty(dialogFaces.find("#grupo-id").val())) {
                        required('Atenção', 'Selecione uma agenda!');
                        return false;
                    }
                    if (empty(dialogFaces.find("#situacao-agenda-id").val())) {
                        required('Atenção', 'Selecione a situação do agendamento!');
                        return false;
                    }
                    if (empty(dialogFaces.find("#convenio-id").val())) {
                        required('Atenção', 'Selecione um convênio!');
                        return false;
                    }
                    if (empty(dialogFaces.find("#inicio").val())) {
                        required('Atenção', 'Informe o início da agenda!');
                        return false;
                    }
                    if (empty(dialogFaces.find("#fim").val())) {
                        required('Atenção', 'Informe o fim da agenda!');
                        return false;
                    }

                    if (dialogFaces.find('#fim').val() <= dialogFaces.find('#inicio').val()) {
                        required('Atenção', 'A hora fim não pode ser Menor/Igual a hora inicio!');
                        return false;
                    }

                    var validation_agenda = validationAgenda($(dialogFaces).find('#grupo-id').val(), moment($(dialogFaces).find('#inicio').val(), 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'), moment($(dialogFaces).find('#fim').val(), 'DD/MM/YYYY HH:mm').format('YYYY-MM-DD HH:mm'), id);
                    validation_agenda.success(function (txt) {
                        CloseGif();
                        if (txt.res > 0) {
                            BootstrapDialog.alert({
                                title: 'Atenção',
                                type: BootstrapDialog.TYPE_DANGER,
                                message: txt.msg
                            })
                        } else {
                            dialogFaces.find('#frm-agenda').ajaxForm({
                                beforeSubmit: function () {
                                    LoadGif()
                                },
                                success: function (txt) {
                                    CloseGif();
                                    $('#calendar' + div_id).fullCalendar('removeEvents', id);
                                    $('#calendar' + div_id).fullCalendar('renderEvent', txt.result);
                                    dialogRef.close();
                                }
                            }).submit();
                            $('#fill-agenda').click();
                        }
                    });
                }
            }
        ],
        onshown: function (dialog) {
            dialog.getModal().removeAttr('tabindex');
        },
    });
    dialog_modelos.open();
    $(".qtip").hide();
    setTimeout(function () {
        $(".qtip").hide();
    }, 1000);
}

function required(titulo, mensagem) {
    BootstrapDialog.alert({
        title: titulo,
        message: mensagem,
        type: BootstrapDialog.TYPE_DANGER
    })
}

function validationAgenda(div_id, start, end, agenda_id) {
    return $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/validation-agenda',
        dataType: 'JSON',
        async: false,
        data: {
            grupo_id: div_id,
            inicio: start,
            fim: end,
            agenda_id: agenda_id
        },
        beforeSend: function () {
            LoadGif()
        }

    })
}

function checar_agendamentos(agenda_id, horario) {
    return $.ajax({
        type: 'POST',
        url: site_path + '/Agendas/checar',
        dataType: 'JSON',
        async: false,
        data: {
            inicio: horario,
            grupo_id: agenda_id
        },
        beforeSend: function () {
            LoadGif()
        }

    })
}

function replaceAll(string, token, newtoken) {
    while (string.indexOf(token) != -1) {
        string = string.replace(token, newtoken);
    }
    return string;
}

function eventos2(element, div_id, qtde, start, end) {
    $(element).find('#calendar' + div_id).fullCalendar('removeEventSources');
    if (!empty($(element).find("#grupo-id").val())) {
        $(element).find(".gif-agenda").html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Atualizando...</h3></div>");
        $($(element).find('#lista-agenda')).ajaxForm({
            url: site_path + '/Agendas/eventos/',
            type: 'POST',
            data: {
                group_aux: (qtde > 1) ? div_id : null,
                startParam: start,
                endParam: end
            },
            success: function (txt) {
                $.when(
                    $(element).find('#calendar' + div_id).fullCalendar('removeEventSources')
                ).then(function () {
                    $(element).find(".gif-agenda").html("");
                    $(element).find('#calendar' + div_id).fullCalendar('addEventSource', txt)
                });

            }
        }).submit();
    }
    // Insere a quantidade de pacientes
    //$(element).find('#calendar'+div_id+' #qtd_pacientes').text(txt.pacientes);
    var get_time_group = getTimeGroup(div_id, moment($(element).find('#start').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
    get_time_group.success(function (txt) {
        $(element).find('#calendar' + div_id + ' #qtd_pacientes').text(txt.pacientes);
    });
}

function eventos(element, div_id, qtde) {

    $(element).find('#calendar' + div_id).fullCalendar('gotoDate', moment($('#start').val(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
    if (!empty($(element).find("#grupo-id").val())) {
        $(element).find(".gif-agenda").html("<div class='text-center'><h3><i class='fa fa-spinner fa-pulse'></i> Atualizando...</h3></div>");

        $($(element).find('#lista-agenda')).ajaxForm({
            url: site_path + '/Agendas/eventos/',
            type: 'POST',
            data: {
                group_aux: (qtde > 1) ? div_id : null
            },
            success: function (txt) {
                $.when(
                    $(element).find('#calendar' + div_id).fullCalendar('removeEventSources')
                ).then(function () {
                    $(element).find(".gif-agenda").html("");
                    $(element).find('#calendar' + div_id).fullCalendar('addEventSource', txt)
                })
            }
        }).submit();
    }
}

function convertDatetime(datetime) {
    if (!empty(datetime)) {
        var datestr = (datetime.split(" ")[0]).split("/");
        return datestr[2] + '-' + datestr[1] + '-' + datestr[0] + 'T' + datetime.split(" ")[1] + ':00';
    } else {
        return null;
    }
}

function convertDate(date) {
    if (!empty(date)) {
        var datestr = (date.split("/"));
        return datestr[2] + '-' + datestr[1] + '-' + datestr[0];
    } else {
        return null;
    }
}

function date_ms(data) {
    var inicio = new Date(data);
    var nowUtc = new Date(inicio.getTime() + (inicio.getTimezoneOffset() * 60000));
    return nowUtc.toLocaleString();
}

function SalveForm(form) {
    console.log(form);
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif();
        },
        success: function (txt) {
            CloseGif();
            if (txt == '-1') {
                BootstrapDialog.alert('Erro ao salvar cliente!');
                return false;
            }
            txt = jQuery.parseJSON(txt);
            var opc = '<option value=' + txt.id + ' selected>' + txt.text + '</option>';
            $("[data-up='cliente']").html(opc);
            $("[data-up='cliente']").select2({
                language: 'pt-BR',
                theme: 'bootstrap',
                width: '100%',
                placeholder: {
                    id: '-1',
                    text: 'Selecione um cliente'
                },
                minimumInputLength: 1,
                ajax: {
                    url: site_path + "/findClientes",
                    dataType: 'json',
                    cache: true,
                    type: 'post',
                    data: function (params) {
                        return {
                            termo: params.term,
                            size: 10,
                            page: params.page
                        };
                    },
                    processResults: function (data) {
                        // parse the results into the format expected by Select2.
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data
                        return {
                            results: data.dados,
                            pagination: {
                                more: data.more
                            }
                        }
                    }
                }
            }).trigger('change');
            $("#modal_2").modal('hide');
        }
    });
}

function imprimirCadastroPaciente(form) {
    var aux;
    var aux2;
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif();
        },
        success: function (txt) {
            aux = txt;
            CloseGif();
            if (txt == '-1') {
                BootstrapDialog.alert('Erro ao salvar cliente!');
                aux2 = false;
                return false;
            }
            location.href = site_path + '/Clientes/reportCadastro/' + txt.id;
            aux2 = true;
        }
    }).submit();
    if (aux2)
        location.href = site_path + '/Clientes/reportCadastro/' + aux.id;
}

function EnviarForm(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if (txt == '-1') {
                BootstrapDialog.alert('Erro ao salvar registros!');
                return false;
            } else {
                carregarAba($(".area_pront").parent('.active').attr('id'), $("[aria-controls='" + $(".area_pront").parent('.active').attr('id') + "']").attr('controller'), filtro);
            }
        }
    });
}

function PrinterForm(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if (txt == '-1') {
                BootstrapDialog.alert('Erro ao salvar registros!');
                return false;
            } else {
                var dados = {id: txt.result.id}
                var url = site_path + '/cliente-documentos/documentos.pdf?id=' + txt.result.id;

                $.FormGet(site_path + '/cliente-documentos/documentos.pdf', dados);
                carregarAba($(".area_pront").parent('.active').attr('id'), $("[aria-controls='" + $(".area_pront").parent('.active').attr('id') + "']").attr('controller'), filtro);
                window.open(url, '_blank');
            }
        }
    });
}

function AprovarRetorno(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if (txt == '-1') {
                BootstrapDialog.alert('Erro ao salvar registros!');
                return false;
            } else {
                $('.modal').modal('hide');
                if ($("#btn-fill").html()) {
                    $("#btn-fill").click();
                } else {
                    location.reload();
                }
            }
        }
    });
}

function recarregar_tela() {
    if ($("#btn-fill").html()) {
        $("#btn-fill").click();
    } else {
        location.reload();
    }
}

//funcao padrao - dar uma trabalhada
function senduploads(form, controller, action) {
    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');

    $('form#' + form).ajaxForm({
        uploadProgress: function (event, position, total, percentComplete) {
            $('form#' + form).find("input:submit").addClass("disabled");
            $('form#' + form).find("input:submit").attr("disabled", "disabled");

            $(documento).find(".progress").css({"display": "block"});
            var percentVal = percentComplete + '%';
            $(documento).find("[role='progressbar']").attr('style', 'width: ' + percentVal);
        },
        success: function (d) {
            $('form#' + form).find("input:submit").removeClass("disabled");
            $('form#' + form).find("input:submit").removeAttr("disabled");
            d = jQuery.parseJSON(d);
            if (!empty(controller)) {
                carregarAba2($(".area_pront").parent('.active').attr('id'), controller, action, d);
            } else {
                carregarAba($(".area_pront").parent('.active').attr('id'), $("[aria-controls='" + $(".area_pront").parent('.active').attr('id') + "']").attr('controller'), filtro);
            }
        }
    });
}

function formanexos(form, controller, action) {
    $('form#' + form).ajaxForm({
        uploadProgress: function (event, position, total, percentComplete) {
            $('form#' + form).find("input:submit").addClass("disabled");
            $('form#' + form).find("input:submit").attr("disabled", "disabled");
            $(".progress").css({"display": "block"});
            var percentVal = percentComplete + '%';
            $("[role='progressbar']").attr('style', 'width: ' + percentVal);
        },
        success: function (d) {
            $('form#' + form).find("input:submit").removeClass("disabled");
            $('form#' + form).find("input:submit").removeAttr("disabled");
            if (d.res != 'erro') {
                location.href = site_path + '/' + controller + '/' + action
            } else {
                required('Erro', d.msg);
            }
        }
    });
}

$(function () {

    var form_retorno = $("form#frmretornos");

    form_retorno.find("#grupo-id").change(function () {
        if (!empty(form_retorno.find("#cliente-id").val())) {
            $.ajax({
                type: 'GET',
                url: site_path + '/Retornos/marcados',
                dataType: 'JSON',
                data: {
                    cliente_id: form_retorno.find("#cliente-id").val(),
                    grupo_id: form_retorno.find("#grupo-id").val()
                },
                success: function (txt) {
                    if (!empty(txt.result)) {
                        BootstrapDialog.show({
                            title: 'Faces',
                            message: 'Já existe um retorno programado para o dia ' + txt.result.previsao,
                            closable: false,
                            buttons: [
                                {
                                    label: 'Ir para retorno',
                                    action: function (dialogRef) {
                                        location.href = site_path + '/Retornos/edit/' + txt.result.id
                                    }
                                }, {
                                    label: 'OK',
                                    action: function (dialogRef) {
                                        dialogRef.close();
                                    }

                                }
                            ]
                        });

                        form_retorno.find("#grupo-id").val('').trigger('change');
                    }
                }
            })
        }
    })
    form_retorno.find('#previsao').blur(function () {
        $.ajax({
            type: 'POST',
            url: site_path + '/Retornos/getdays',
            data: {
                date: $(this).val()
            },
            dataType: 'JSON',
            success: function (txt) {
                form_retorno.find('#dias').val(txt.result);
            }
        })
    })
    form_retorno.find('#dias').blur(function () {
        $.ajax({
            type: 'POST',
            url: site_path + '/Retornos/getdays',
            data: {
                dias: $(this).val()
            },
            dataType: 'JSON',
            success: function (txt) {
                form_retorno.find('#previsao').val(txt.result);
            }
        })
    })


    function enviarForm(form, tab) {
        $('form#' + form).ajaxForm({
            beforeSubmit: function () {
                LoadGif()
            },
            success: function (d) {
                CloseGif();
                $('#' + tab + 'Aba').html(d);
            }
        });
    }
});

function loadComponentes() {
    CloseGif();

    click = null;
    filtrado = null;
    if (!empty(dialog_modelos)) {
        initDocument(dialog_modelos.getModalBody())
        return false;
    }
    if (!empty(dialog_sub)) {
        initDocument(dialog_sub.getModalBody())
        return false;

    }

    if (!empty(dialog_executor)) {
        initDocument(dialog_executor.getModalBody())
        return false;

    }


    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');
    initDocument(documento)
}

function loadicheck() {
    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');
    initDocument(documento)
}

function send_agenda(form) {
    $("form#" + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if (txt.res == 2) {
                required('Erro', txt.msg)
            } else {
                Navegar('', 'back');
            }
        }
    })
}

function SendAtendimento(form) {
    if (empty($("#total_procedimentos").val())) {
        required('Atenção', 'Não é permitido cadastrar atendimento sem procedimentos!');
        return false;
    } else {

        $("form#" + form).ajaxForm({
            beforeSubmit: function () {
                LoadGif()
            },
            success: function (txt) {
                CloseGif();
                if (txt.res == 2) {
                    required('Erro', txt.msg)
                }else if(txt.valor_liquido){
                    Navegar(site_path + '/contasreceber/index?id=' + txt.msg, 'go');
                }else {
                    toastr.success('O Atedimento foi salvo com sucesso!');
                    $('.modal-content button.close').click();
                }
            }
        })
        return true;
    }
}

function SendForm(form) {
    $(form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            Navegar('', 'back');
            $("#fill-agenda").each(function () {
                $(this).click();
            })
        }
    });
}

function FimAgendamento(form) {
    $(form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            dialog_sub.close();
            dialog_sub = null;
            sit = null;
            var dialogAnterior = dialog_modelos.getModalBody();
            dialogAnterior.find('#frm-agenda').ajaxForm({
                success: function (txt) {
                    CloseGif();
                    $('#fill-agenda').click();
                    dialog_modelos.close();
                }
            }).submit();

        }
    })
}

function isrestauracao(procedimento) {
    $.ajax({
        type: 'POST',
        url: site_path + '/Procedimentos/isrestauracao/' + procedimento,
        data: {
            ajax: 1
        },
        dataType: 'JSON',
        success: function (txt) {
            return txt.res;
        }
    })
}

function locadradios() {
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });
}

function locadradios_edit() {

    var modal = modal_ativa();
    var documento = modal.find('.modal-content');

    initDocument(documento)
    var procedimento = documento.find('#procedimento-id').val();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });

}


var dialog_modelos = null;
var dialog_alternativo = null;
var dialog_sub = null;
var dialog_executor = null;

$(function () {
    var filtro = {
        procedimento_id: '',
        convenio_id: '',
        complemento: '',
        medico_id: ''
    };

    /* Estes campos não são mais editaveis - Luciano 13/11/2017 18:18 */
    /*$("#total-pagoato,#desconto,#percentual-desconto,#total-geral").blur(function () {
        calcular();
    });*/


    /*    $('body').on('click', '#add-modelo', function (event) {
     event.preventDefault();
     if (empty($("#tipo-id").val())) {
     BootstrapDialog.alert('Selecione um tipo de documento');
     return false;
     }
     dialog_modelos = new BootstrapDialog({
     title: 'Modelos',
     message: $('<div></div>').load(site_path + "/Modelos/nadd/" + $("#tipo-id").val() + "?ajax=1", inicializar),
     closable: true,
     size: BootstrapDialog.SIZE_WIDE
     });
     dialog_modelos.open();

     });*/

    $('body').on('change', '#tipo-id', function (event) {
        event.preventDefault();
        $.ajax({
            type: 'post',
            url: site_path + '/Modelos/fill/' + $(this).val() + '?ajax=1',
            dataType: 'JSON',
            success: function (txt) {
                $("#modelo-id").empty()
                    .append('<option value="">Selecione</option>')
                    .find('option:first');

                $.each(txt, function (key, value) {
                    $("#modelo-id").append(new Option(value.nome, value.id));
                });
            }
        });
    });
    $('body').on('change', '#modelo-id', function (event) {
        event.preventDefault();
        if (!empty($(this).val())) {
            //loadComponentes();
            set_modelo();
        }

    });
});

function save_procedimento(form) {
    $(form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            $(".li-list").show();
        }
    });
}

function carregar_procedimentos() {
    click = null;
    inicializar();
    dialog_sub.getModal().removeAttr('tabindex');

}

function inicializar() {
    if (!empty(dialog_modelos)) {
        initDocument(dialog_modelos.getModalBody())
    }
    if (!empty(dialog_sub)) {
        initDocument(dialog_sub.getModalBody())
    }

    if (!empty(dialog_executor)) {
        initDocument(dialog_executor.getModalBody())
    }

}

function set_modelo() {
    $.ajax({
        type: 'GET',
        url: site_path + '/Modelos/get/' + $("#modelo-id").val(),
        dataType: 'JSON',
        data: {
            ajax: 1
        },
        success: function (txt) {
            // Arrumar depois
            /*let concat = $('#concat-modelos').html();
            concat += txt.modelo;
            $('#concat-modelos').html(concat);
            var modelos = $('#concat-modelos').html();*/

            $('#modelo-id').val('').trigger('change');
            var editor = CKEDITOR.instances.descricao;
            var sel = editor.getSelection();
            var element = sel.getStartElement();
            sel.selectElement(element);
            var ranges = editor.getSelection().getRanges();
            ranges[0].setStart(element.getFirst(), 1);
            ranges[0].setEnd(element.getFirst(), 0); //cursor

            $.when(editor.insertHtml(txt.modelo + '<br />'))
                .then(function () {
                    var s = editor.getSelection();
                    var selected_ranges = s.getRanges();
                    s.selectRanges(selected_ranges); // restore it
                })
        }
    })
}

//funcao padrao
function formcombo(form, campo, callback) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            if (d != 'erro') {
                $("#" + campo).append(new Option(d.nome, d.id, true));
                $("#" + campo).val(d.id);
                $("#" + campo).trigger('change');
                if ($.isFunction(callback)) {
                    callback()
                }
                dialog_modelos.close();
            } else {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    type: BootstrapDialog.TYPE_DANGER,
                    message: 'Erro ao salvar registro!'
                });
            }
        }
    });
}

function formcombo_modal(form, campo, callback) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            if (d != 'erro') {
                dialog_alternativo.close();

                var modal = $('.modal');
                var documento = modal.find('.modal-content');
                documento.find("#" + campo).append(new Option(d.nome, d.id, true));
                documento.find("#" + campo).val(d.id);
                documento.find("#" + campo).trigger('change');
                if ($.isFunction(callback)) {
                    callback()
                }

            } else {
                BootstrapDialog.alert({
                    title: 'Alerta',
                    type: BootstrapDialog.TYPE_DANGER,
                    message: 'Erro ao salvar registro!'
                });
            }
        }
    });
}


function getOptions(controller, action, filtro, campo) {
    $.ajax({
        url: site_path + '/' + controller + '/' + action,
        type: 'POST',
        data: filtro,
        dataType: 'JSON',
        success: function (txt) {
            $("#" + campo).empty()
                .append('<option value="">Selecione</option>')
            $.when(
                $.each(txt, function (key, value) {
                    $("#" + campo).append(new Option(value.nome, value.id));
                })
            ).then(function () {
                if (!empty(filtro.default)) {
                    $("#" + campo).val(filtro.default);
                }
                $("#" + campo).trigger('change');
            })
        }
    })
}

function getValores() {
    carregar();
    $.ajax({
        type: 'POST',
        url: site_path + "/valores/",
        dataType: "JSON",
        beforeSend: function () {
            LoadGif();
        },
        success: function (txt) {
            $("#total-liquido").val(number_format(txt, 2, '.', ''));
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    })
}

function itens() {
    if (!empty($("#id_procedimento").val())) {
        BootstrapDialog.show({
            title: 'Itens de Cobrança',
            message: $('<div></div>').load(site_path + "/Atendimentos/itens/" + $("#id_procedimento").val(), carregar),
            closable: false,
            buttons: [{
                label: 'Concluído',
                action: function (dialogRef) {
                    var dialogItens = dialogRef.getModalBody();
                    var ids = new Array();
                    dialogItens.find(".aten-itens").each(
                        function () {
                            if ($(this).is(':checked')) {
                                ids.push($(this).val());
                            }
                        }
                    );
                    if (ids.length <= 0) {
                        BootstrapDialog.confirm({
                            title: 'Alerta',
                            message: 'Não foi selecionado nenhum item da lista! \n Se continuar o procedimento será excluido!',
                            type: BootstrapDialog.TYPE_WARNING,
                            closable: false, // <-- Default value is false
                            draggable: true, // <-- Default value is false
                            btnCancelLabel: 'Cancelar!', // <-- Default value is 'Cancel',
                            btnOKLabel: 'Ok Entendi!', // <-- Default value is 'OK',
                            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                            callback: function (result) {
                                if (result) {
                                    dell_direto($("#id_procedimento").val());
                                    load_proc();
                                    dialogRef.close();
                                }
                            }
                        });

                    } else {
                        getValores();
                        load_proc();
                        dialogRef.close();
                    }
                }
            }]
        });
    }
}

function itens_edit(id) {
    BootstrapDialog.show({
        title: 'Itens de Cobrança',
        message: $('<div></div>').load(site_path + "/Atendimentos/itens/" + id, carregar),
        closable: false,
        buttons: [{
            label: 'Concluído',
            action: function (dialogRef) {
                var dialogItens = dialogRef.getModalBody();
                var ids = new Array();
                dialogItens.find(".aten-itens").each(
                    function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).val());
                        }
                    }
                );
                if (ids.length <= 0) {
                    BootstrapDialog.confirm({
                        title: 'Alerta',
                        message: 'Não foi selecionado nenhum item da lista! \n Se continuar o procedimento será excluido!',
                        type: BootstrapDialog.TYPE_WARNING,
                        closable: false, // <-- Default value is false
                        draggable: true, // <-- Default value is false
                        btnCancelLabel: 'Cancelar!', // <-- Default value is 'Cancel',
                        btnOKLabel: 'Ok Entendi!', // <-- Default value is 'OK',
                        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                        callback: function (result) {
                            if (result) {
                                dell_edit()
                                dialogRef.close();
                            }
                        }
                    });
                } else {
                    $.when(
                        atualizar_faturar()
                    ).then(function () {
                        saveEdit()
                    })

                    dialogRef.close();
                }
            }
        }]
    });
}

function saveEdit() {
    $.ajax({
        type: 'POST',
        url: site_path + "/Orcamentos/editprocs/",
        data: {
            orcamento: $("#orc").val()
        },
        dataType: "JSON",
        success: function (txt) {
            atualizar($("#orc").val());
            $("#modal_lg").modal('hide');
        }
    })
}

function dell_edit() {
    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');
    $.ajax({
        type: 'GET',
        url: site_path + '/Atendimentos/delledit',
        success: function (txt) {
            $(documento).find('#procedimento-id').val('')
        }
    })
}

function SalvarPrev() {
    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');
    initDocument(documento)


    if (empty($(documento).find("#medico-id").val())) {
        required('Alerta', "Selecione um médico!");
        return false;
    }

    if (empty($(documento).find("#convenio").val())) {
        required('Alerta', "Selecione um convênio!");
        return false;
    }

    if (empty($(documento).find("#procedimento-id").val())) {
        required('Alerta', "Selecione um procedimento!");
        return false;
    }

    $(documento).find('#btn-editar').addClass('disabled');
    $(documento).find('#btn-editar').attr('disabled', 'disabled');
    $.ajax({
        type: 'GET',
        url: site_path + '/Atendimentos/procedit',
        data: {
            procedimento_id: $(documento).find('#procedimento-id').val(),
            medico_id: $(documento).find('#medico-id').val(),
            convenio_id: $(documento).find('#convenio').val(),
            complemento: $(documento).find('#complemento').val(),
            atendimento_id: $(documento).find('#atendimento-id').val(),
            controle: $(documento).find('#controle').val(),
            quantidade: $(documento).find('#quantidade').val(),
            codigo: $(documento).find('#input-codigo').val()
        },
        success: function (txt) {
            switch (txt) {
                case 'preco':
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_WARNING,
                        message: 'Este procedimento não possui valor de cobrança cadastrado!'
                    });
                    $(documento).find('#btn-editar').removeClass('disabled');
                    $(documento).find('#btn-editar').removeAttr('disabled');
                    break;
                case 'erro':
                    BootstrapDialog.alert({
                        type: BootstrapDialog.TYPE_DANGER,
                        message: 'Erro ao salvar registro!'
                    });
                    break;
                default:
                    $.when(
                        modal.modal('hide')
                    ).then(function () {
                        atualizar(txt)
                    })
                    break;
            }
        }
    })
}

function getValoresProcedimentos(convenio, procedimento) {
    BootstrapDialog.show({
        title: 'Itens de Cobrança',
        message: $('<div></div>').load(site_path + "/Atendimentos/itens/" + procedimento, carregar),
        closable: false,
        buttons: [{
            label: 'Concluído',
            action: function (dialogRef) {
                var dialogItens = dialogRef.getModalBody();
                var ids = new Array();
                dialogItens.find(".aten-itens").each(
                    function () {
                        if ($(this).is(':checked')) {
                            ids.push($(this).val());
                        }
                    }
                );
                if (ids.length <= 0) {
                    $("#procedimento-id").val("");
                    dialogRef.close();
                } else {
                    $.ajax({
                        type: 'POST',
                        url: site_path + "/AtendimentoProcedimentos/getvalores/",
                        data: {
                            convenio_id: convenio,
                            procedimento_id: procedimento
                        },
                        dataType: "JSON",
                        success: function (txt) {
                            $("#valor-fatura").val(txt.valor_faturar);
                            $("#valor-caixa").val(txt.valor_particular);
                        }
                    })
                    dialogRef.close();
                }
            }
        }]
    });


}


function calcular() {
    /*var total = parseFloat($("#total-geral").val());
    var pago_ato = (!empty($("#total-pagoato").val())) ? parseFloat($("#total-pagoato").val()) : 0;
    var desconto = (!empty($("#desconto").val())) ? parseFloat($("#desconto").val()) : 0;
    var percentual_desconto = (!empty($("#percentual-desconto").val())) ? parseFloat($("#percentual-desconto").val()) : 0;
    total = total - (total * (percentual_desconto / 100));
    total = (total - (pago_ato + desconto));

    $("#total-liquido").val(number_format(total, 2, '.', ''));*/
}

function dell_iten(id) {
    console.log('aqui')
    BootstrapDialog.confirm('Tem certeza que deseja excluir este registro?', function (result) {
        if (result) {
            var filtro = {
                procedimento_id: '',
                convenio_id: ''
            }
            $.ajax({
                type: 'GET',
                url: site_path + '/deletaritens/',
                data: {
                    pos: id
                }, success: function (txt) {
                    $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
                }
            })
        }
    });
}


function dell_direto(id) {
    var filtro = {
        procedimento_id: '',
        convenio_id: ''
    }
    $.ajax({
        type: 'GET',
        url: site_path + '/deletaritens/',
        data: {
            pos: id
        }, success: function (txt) {
            $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', getValores);
        }
    })
}


function load_proc() {
    var filtro = {
        procedimento_id: '',
        convenio_id: ''
    }
    $("div.list-dep").loadGrid(site_path + '/proc', filtro, '.arts', carregar);
}

function ItenAprovado() {
    BootstrapDialog.alert({
        type: BootstrapDialog.TYPE_DANGER,
        title: "Atenção",
        message: "Este procedimento já foi aprovado!\n Portanto não pode ser excluído!"
    })
}

//TODO deletar registro dentro dos modais
function DeletarModal(controller, id) {
    BootstrapDialog.confirm({
        title: 'Atenção!',
        message: 'Tem certeza que deseja excluir este registro?',
        type: BootstrapDialog.TYPE_DANGER,
        closable: true,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Excluir',
        btnOKClass: 'btn-danger',
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: 'POST',
                    dataType: 'JSON',
                    url: site_path + '/' + controller + '/delete-modal/',
                    data: {id: id},
                    success: function (txt) {
                        if (txt.res == 0) {
                            $('tr.delete' + id).fadeOut('slow', function () {
                                $(this).remove();
                                toastr.success('Registro deletado com sucesso!');
                            });
                        } else {
                            toastr.error('Falha ao tentar deletar registro, Por favor tente novamento!');
                        }
                    }
                })
            }
        }
    });
}

function Deletar(controller, id, funcao, id_funcao) {
    BootstrapDialog.confirm({
        title: 'Atenção!',
        message: 'Tem certeza que deseja excluir este registro?',
        type: BootstrapDialog.TYPE_DANGER,
        closable: true,
        draggable: true,
        btnCancelLabel: 'Cancelar',
        btnOKLabel: 'Excluir',
        btnOKClass: 'btn-danger',
        callback: function (result) {
            if (result) {
                $.ajax({
                    type: 'POST',
                    url: site_path + '/' + controller + '/delete/' + id,
                    data: {
                        check_auth: 1
                    },
                    success: function (txt) {
                        $('#btn-fill').trigger('click');
                        if (!empty(txt.res) && txt.res == 'erro') {
                            BootstrapDialog.alert({
                                title: 'Atenção!',
                                message: txt.msg,
                                type: BootstrapDialog.TYPE_DANGER
                            })
                        } else {
                            if ($.isFunction(funcao)) {
                                funcao()
                            } else {
                                switch (funcao) {
                                    case "atualizar":
                                        atualizarProcedimentos(id_funcao);
                                        break;
                                    case "carregarAba":
                                        carregarAba($(".area_pront").parent('.active').attr('id'), $("[aria-controls='" + $(".area_pront").parent('.active').attr('id') + "']").attr('controller'), filtro);
                                        break;
                                    case "procedimentos":
                                        var modal = $('#modal_lg');
                                        var documento = modal.find('.modal-content');
                                        documento.find(".this-place").loadGrid(site_path + '/ItenCobrancas/index?procedimento=' + id_funcao, null, ".this-place", loadicheck);
                                        break;
                                    case "filtrar":
                                        $('#btn-fill').click();
                                        break;
                                    case "back":
                                        Navegar('', 'back');
                                        break;
                                }
                            }
                        }
                    }
                })
            }
        }
    });
}

function FinalizarAgenda(id, funcao) {
    var check_atendimento = agenda_atendimento(id);
    check_atendimento.success(function (txt) {
        CloseGif();
        if (txt.res == 0) {
            required('Alerta', 'Não é permitido finalizar este agendamento enquanto não existir atendimento!');
        } else {
            BootstrapDialog.confirm({
                title: 'Atenção!',
                message: 'Tem certeza que deseja finalizar este atendimento?',
                type: BootstrapDialog.TYPE_DANGER,
                closable: true,
                draggable: true,
                btnCancelLabel: 'Cancelar',
                btnOKLabel: 'Finalizar',
                btnOKClass: 'btn-danger',
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: 'POST',
                            url: site_path + '/Agendas/finalizar-agenda/' + id,
                            success: function (txt) {
                                if ($.isFunction(funcao)) {
                                    funcao();
                                }
                            },
                            complete: function () {
                                $("#modal_lg button.close").click();
                            }
                        })
                    }
                }
            });
        }
    })

}

/* Atualiza a listagem de procedimentos, nas telas de edição do atendimento */
function atualizarProcedimentos(atendimento_id) {
    $(".edit-dep").loadGrid(site_path + '/AtendimentoProcedimentos/listar/' + atendimento_id, null, '.arts', carregar);
    somaprocedimentos(atendimento_id);
}

/* Atualiza a listagem dos procedimentos - Tiago */
function atualizar(id) {
    var modal = modal_ativa();
    if (!empty(modal)) {
        var documento = $(modal).find('.modal-content');
        $(documento).find("div.edit-dep").loadGrid(site_path + '/AtendimentoProcedimentos/listar/' + id, null, '.arts', carregar);

    } else {
        $("div.edit-dep").loadGrid(site_path + '/AtendimentoProcedimentos/listar/' + id, null, '.arts', carregar);
    }
    somaprocedimentos(id)
}

function atualizar_faturar() {
    $.ajax({
        type: 'POST',
        url: site_path + "/valores/",
        dataType: "JSON",
        success: function (txt) {
            return true;
        }
    })
}

function somaprocedimentos(atendimento) {
    $.ajax({
        type: 'POST',
        url: site_path + "/AtendimentoProcedimentos/somaprocedimentos/" + atendimento,
        dataType: "JSON",
        success: function (txt) {
            $("#total-liquido").val(number_format(txt.valor_caixa, 2, '.', ''));
            $(".total-liquido").text(formatReal(txt.valor_caixa));
            $("#total-pagoato").val(number_format(txt.total_recebido, 2, '.', ''));
            $("#total-areceber").val(number_format(txt.total_a_receber, 2, '.', ''));
        }
    })
}

function EnviarFormulario(form, funcao) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            if (!empty(funcao)) {
                switch (funcao) {
                    case "atualizar":
                        atualizar(d);
                        break;
                }
                $('.modal').modal('hide');
            }
        }
    });
}

function carregarAba(div_id, controller, fil, action, loadscript) {

    if (!empty(action)) {
        $("div#" + div_id).loadGrid(site_path + "/" + controller + "/" + action, fil, ".area_pront", loadComponentes, loadscript);
    } else {
        $("div#" + div_id).loadGrid(site_path + "/" + controller + "/all", fil, ".area_pront", loadComponentes);
    }
}

function carregarAba2(div_id, controller, action, fil) {
    $("div#" + div_id).loadGrid(site_path + "/" + controller + "/" + action, fil, ".area_pront", loadComponentes);
}

function continuar_itens(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            d = JSON.parse(d);
            if (d != 'erro') {
                CloseGif();
                if (!empty(d.novo)) {
                    location.href = site_path + '/Procedimentos/index';
                } else {

                    $(".this-place").loadGrid(site_path + '/ItenCobrancas/index?procedimento=' + d.procedimento + '&n=' + d.novo, null, ".this-place", loadicheck);
                }
            }
        }
    });
}

function editar_itens(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            if (d != 'erro') {
                $(".this-place").loadGrid(site_path + '/ItenCobrancas/index?procedimento=' + d, null, ".this-place", loadicheck);
            }
        }
    });
}

function salvar_faces(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            if (d == 'erro') {
                $('.modal').modal('hide');
                BootstrapDialog.alert({
                    title: 'Atenção!',
                    message: 'Ocorreu um erro ao salvar o registro!',
                    type: BootstrapDialog.TYPE_DANGER,
                });
            } else {
                d = JSON.parse(d);
                if (d.novo == 1) {
                    location.href = site_path + '/Procedimentos/index';
                } else {
                    $(".this-place").loadGrid(site_path + '/ItenCobrancas/index?procedimento=' + d.procedimento, null, ".this-place", loadicheck);
                }
            }
        }
    });
}

function salvar_procedimento(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (d) {
            CloseGif();
            if (d != 'erro') {
                $("#modal_lg").modal('show');
                CloseGif();
                $(".modal-content").loadGrid(site_path + '/ItenCobrancas/add?procedimento=' + d + '&n=1', null, ".this-place", loadicheck);
            } else {
                location.href = site_path + '/Procedimentos/index';
            }
        }
    });
}

$(function () {
    $("#stodos").click(function () {
        $('.itens').each(
            function () {
                if ($(this).is(':checked')) {
                    $(this).iCheck('uncheck');
                } else {
                    $(this).iCheck('check');
                }
            }
        );
        if ($(this).hasClass('selecionar')) {
            $(this).removeClass('selecionar');
            $(this).html('<i class="fa fa-exclamation"></i> Desmarcar Todos');
        } else {
            $(this).addClass('selecionar');
            $(this).html('<i class="fa fa-check"></i> Selecionar Todos');
        }
    });
    $("#save-tratamento").click(function () {
        var ids = new Array();
        $('.itens').each(
            function () {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            }
        );
        if (ids.length > 0) {
            BootstrapDialog.confirm({
                title: "Alerta",
                message: "Confirma a geração do tratamento a partir deste orçamento?",
                type: BootstrapDialog.TYPE_WARNING,
                closable: false,
                draggable: false,
                btnCancelLabel: "Não!",
                btnOKLabel: "Sim!",
                btnOKClass: "btn-warning",
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            type: "POST",
                            url: site_path + "/Atendimentos/nadd",
                            data: {
                                ids: ids,
                                orcamento_id: $("#orcamento_id").val()
                            },
                            success: function (txt) {
                                if (txt != 'erro') {
                                    location.href = site_path + '/Atendimentos/view/' + txt;
                                    return false;

                                    $("#modal_lg").modal({
                                        show: true,
                                        backdrop: false,
                                        keyboard: false
                                    });
                                    CloseGif();
                                    $(".modal-content").loadGrid(site_path + '/contasreceber/new-add?id=' + txt, null, ".this-place", loadicheck);

                                } else {
                                    BootstrapDialog.alert({
                                        title: 'Atenção!',
                                        message: 'Ocorreu um erro ao salvar o Trabamento!',
                                        type: BootstrapDialog.TYPE_DANGER
                                    });
                                }
                            }
                        });
                    }
                }
            })

        } else {
            BootstrapDialog.alert({
                title: 'Atenção!',
                message: 'Selecione algum item da lista!',
                type: BootstrapDialog.TYPE_WARNING
            });

            return false;
        }
    })

});

function carregar() {
    var modal = modal_ativa();
    if (!empty(modal)) {
        var documento = modal.find('.modal-content');
        initDocument(documento)
    } else {
        $(document).find('body').each(function () {
            initDocument(document)
        })
    }
    click = null;
    filtrado = null;
}

function SalvarContas() {
    var modal = modal_ativa();
    var documento = modal.find('.modal-content');
    SendForm(documento.find('form'));
}

function calculaValor() {
    var modal = modal_ativa();
    var documento = modal.find('.modal-content');
    if (!empty(documento.find('#multa').val())) {
        var multa = documento.find('#multa').val();
    } else {
        var multa = 0;
    }


    if (!empty(documento.find('#desconto').val())) {
        var desconto = documento.find('#desconto').val();
    } else {
        var desconto = 0;
    }

    if (!empty(documento.find('#valorbruto').val())) {
        var valorBruto = documento.find('#valorbruto').val();
    } else {
        var valorBruto = 0;
    }

    if (!empty(documento.find('#juros').val())) {
        var juros = documento.find('#juros').val();
    } else {
        var juros = 0;
    }
    var aux = (parseFloat(valorBruto) + parseFloat(juros) + parseFloat(multa)) - parseFloat(desconto);
    documento.find('#valor').val(aux);

}

//checar porque esta perdendo a navegacao
const g2iNavObject = {"navegacao": [{"p": 1, "u": site_path}]};

function recarregar() {
    var pos = g2iNavObject.navegacao.length;
    var res = parseInt(pos) - 1;
    var caminho = g2iNavObject.navegacao[res].u;

    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');

    $(documento).find(".this-place").loadGrid(caminho, null, ".this-place", loadicheck);

}

function Acrescentar(url) {

    var pos = g2iNavObject.navegacao.length;
    var add = true;
    $.when(
        $.each(g2iNavObject, function (key, data) {
            $.each(data, function (id, valor) {
                if (valor.u == url) {
                    add = false;
                }
            })
        })
    ).then(function () {
        if (add) {
            pos = pos + 1;
            var url2 = {"p": pos, "u": url};
            g2iNavObject.navegacao.push(url2);
        }
    })

}

function Decrementar(url, param) {
    g2iNavObject.navegacao.pop();
    Navegar(url, param);
}

function Navegar(url, param) {
    var pos = g2iNavObject.navegacao.length;
    if (param == 'go') {
        var add = true;
        $.when(
            $.each(g2iNavObject, function (key, data) {
                $.each(data, function (id, valor) {
                    if (valor.u == url) {
                        add = false;
                    }
                })
            })
        ).then(function () {
            if (add) {
                pos = pos + 1;
                var url2 = {"p": pos, "u": url};
                g2iNavObject.navegacao.push(url2);
            }
        })


    } else {
        pos = pos - 1;
        g2iNavObject.navegacao.pop();
    }

    var modal = modal_ativa();
    var documento = $(modal).find('.modal-content');

    $.each(g2iNavObject, function (key, data) {
        $.each(data, function (id, valor) {
            if (pos == 1) {
                modal.modal('hide');
            } else if (valor.p == pos) {
                if (param == 'go') {
                    if (add) {
                        $(documento).find(".this-place").loadGrid(valor.u, null, ".this-place", loadicheck);
                    }
                }
                else {
                    if (valor.u == "") {
                        return;
                    } else {
                        $(documento).find(".this-place").loadGrid(valor.u, null, ".this-place", loadicheck);
                    }
                }
            }
        });
    });
}

function last_modal() {
    var maior = 0;
    $(".modal").each(function (key, value) {
        if ($(this).hasClass('in')) {
            if (parseInt($(this).attr('data-modal-index')) > parseInt(maior)) {
                maior = $(this).attr('data-modal-index');
            }
        }
    })
    return maior;
}

function modal_ativa() {
    var maior = 0;
    var modal = null;
    $(".modal").each(function (key, value) {
        if ($(this).hasClass('in')) {
            if (parseInt($(this).attr('data-modal-index')) > parseInt(maior)) {
                maior = $(this).attr('data-modal-index');
                modal = $(this)
            }
        }
    })

    if (!empty(dialog_modelos)) {
        modal = dialog_modelos.getModal()
    }
    if (!empty(dialog_sub)) {
        modal = dialog_sub.getModal()
    }

    if (!empty(dialog_executor)) {
        modal = dialog_executor.getModal()
    }
    return modal;
}

function limpar_modais() {
    $(".modal").each(function (key, value) {
        if (!$(this).hasClass('in')) {
            var html = "<p class='text-center'><img src='" + site_path + "/img/loading.gif' alt='carregando' /></p>";
            $(this).find('.modal-content').html(html);

        }
    });
    dialog_modelos = null;
    dialog_sub = null;
    dialog_executor = null;

}

function parcelar() {
    var modal = modal_ativa();
    var documento = modal.find('.modal-content');
    $(documento.find('form')).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif()
            if (txt != 'erro') {
                Navegar('', 'back');
                return false;
                location.href = site_path + '/Atendimentos/view/' + txt;
            } else {
                BootstrapDialog.alert({
                    title: 'Atenção!',
                    message: 'Ocorreu um erro ao salvar o pagamento!',
                    type: BootstrapDialog.TYPE_DANGER
                });
            }
        }
    })
}

function cancelar(id) {
    $.ajax({
        type: 'POST',
        url: site_path + '/Atendimentos/cancelar',
        data: {
            id: id
        },
        success: function () {
            $('#modal_lg').modal('hide');
        }
    })
}

/*$(window).load(function () {
    initDocument();
})*/

$(document).ready(function () {
    $.extend({
        Report: function (report, args) {
            var url = 'http://g2isolucoes.com.br/iea/Relatorios/engine/index.php?stimulsoft_client_key=ViewerFx&stimulsoft_report_key=' + report;
            $.each(args, function (key, value) {
                url += '&' + key + '=' + encodeURIComponent(value);
            });
            window.open(url, '_blank');
        },
        ReportPost: function (report, args) {
            var url = 'http://g2isolucoes.com.br/iea/Relatorios/engine/index.php';
            var form = '<input type="hidden" name="stimulsoft_client_key" value="ViewerFx"">';
            form += '<input type="hidden" name="stimulsoft_report_key" value="' + report + '">';
            $.each(args, function (key, value) {
                form += '<input type="hidden" name="' + key + '" value="' + value + '">';
            });

            $('<form action="' + url + '" method="POST" target="_blank">' + form + '</form>').submit();
        }
    });


})

function report() {
    var data = {
        inicio: "",
        fim: ""
    };

    if (!empty($("#inicio").val())) {
        data.inicio = moment($("#inicio").val(), 'DD/MM/YYYY').format('YYYY-MM-DD')
    } else {
        data.inicio = -1;
    }
    if (!empty($("#fim").val())) {
        data.fim = moment($("#fim").val(), 'DD/MM/YYYY').format('YYYY-MM-DD')
    } else {
        data.fim = -1;
    }

    $.ReportPost('caixa', data);
}

//Faturamento Pre-faturamento
function marcarDesmarcar() {
    var marcados = new Array();
    var checked = $("input[type=checkbox][name='selecionados[]']:checked");
    var situacao = 1;
    $('input:checkbox[name="selecionados[]"]').each(function () {
        if (checked.length > 0) {
            $(checked).iCheck('uncheck');
            $(checked).removeAttr('checked');
            $("input[type=checkbox][data-posicao]").each(function (val) {
                $('.marcar' + val).removeClass('bg-success');
            })
        } else {
            $(this).iCheck("check");
            $(this).attr('checked', 'checked');
            situacao = 2;
            $("input[type=checkbox][data-posicao]").each(function (val) {
                $('.marcar' + val).addClass('bg-success');
            })
        }
        marcados.push($(this).val());
    });
    ajaxSalvarSelecionado(marcados, situacao);
}

//Faturamento Encerramento
function marcarDesmarcarTemp() {
    var marcados = new Array();
    var checked = $("input[type=checkbox][name='selecionadostemp[]']:checked");
    var situacao = 0;
    $('input:checkbox[name="selecionadostemp[]"]').each(function () {
        if (checked.length > 0) {
            $(checked).iCheck('uncheck');
            $(checked).removeAttr('checked');
            $("input[type=checkbox][data-posicao]").each(function (val) {
                $('.marcar' + val).removeClass('bg-success');
            })
        } else {
            $(this).iCheck("check");
            $(this).attr('checked', 'checked');
            situacao = 1;
            $("input[type=checkbox][data-posicao]").each(function (val) {
                $('.marcar' + val).addClass('bg-success');
            })
        }
        marcados.push($(this).val());
    });
    ajaxSalvarSelecionadoTemp(marcados, situacao);
}

//Faturamento Pre-faturamento
function ajaxSalvarSelecionado(id, situacao) {
    $.ajax({
        type: 'POST',
        url: site_path + '/Faturamentos/salvar-selecionado',
        data: {
            id: id,
            situacao: situacao
        },
        dataType: 'JSON',
        success: function (txt) {
            if (situacao == 2) {
                toastr.success('Itens Marcados!');
            } else if (situacao == 1) {
                toastr.success('Itens Desmarcados!');
            }
        }
    });
}

//Faturamento Encerramento
function ajaxSalvarSelecionadoTemp(id, situacao) {
    $.ajax({
        type: 'POST',
        url: site_path + '/Faturamentos/salvar-selecionado-temp',
        data: {
            id: id,
            situacao: situacao
        },
        dataType: 'JSON',
        success: function (txt) {
            if (situacao == 1) {
                toastr.success('Itens Marcados!');
            } else if (situacao == 0) {
                toastr.success('Itens Desmarcados!');
            }
        }
    });
}

//TODO valida cpf de acordo com a configuracao
function validaCPF(form, cpf) {
    var nascimento = $('#nascimento').val();
    if (empty(nascimento)) {
        toastr.warning('Por favor, informe o campo Nascimento!');
        $('form#' + form).find('#nascimento').focus();
        cpf.val('');
    } else {
        valida_cpf(cpf.val());
    }
}

function calc_digitos_posicoes( digitos, posicoes = 10, soma_digitos = 0 ) {

    // Garante que o valor é uma string
    digitos = digitos.toString();

    // Faz a soma dos dígitos com a posição
    // Ex. para 10 posições:
    //   0    2    5    4    6    2    8    8   4
    // x10   x9   x8   x7   x6   x5   x4   x3  x2
    //   0 + 18 + 40 + 28 + 36 + 10 + 32 + 24 + 8 = 196
    for ( var i = 0; i < digitos.length; i++  ) {
        // Preenche a soma com o dígito vezes a posição
        soma_digitos = soma_digitos + ( digitos[i] * posicoes );

        // Subtrai 1 da posição
        posicoes--;

        // Parte específica para CNPJ
        // Ex.: 5-4-3-2-9-8-7-6-5-4-3-2
        if ( posicoes < 2 ) {
            // Retorno a posição para 9
            posicoes = 9;
        }
    }

    // Captura o resto da divisão entre soma_digitos dividido por 11
    // Ex.: 196 % 11 = 9
    soma_digitos = soma_digitos % 11;

    // Verifica se soma_digitos é menor que 2
    if ( soma_digitos < 2 ) {
        // soma_digitos agora será zero
        soma_digitos = 0;
    } else {
        // Se for maior que 2, o resultado é 11 menos soma_digitos
        // Ex.: 11 - 9 = 2
        // Nosso dígito procurado é 2
        soma_digitos = 11 - soma_digitos;
    }

    // Concatena mais um dígito aos primeiro nove dígitos
    // Ex.: 025462884 + 2 = 0254628842
    var cpf = digitos + soma_digitos;

    // Retorna
    return cpf;
    
} // calc_digitos_posicoes

function valida_cpf( valor ) {

    // Garante que o valor é uma string
    valor = valor.toString();
    
    // Remove caracteres inválidos do valor
    valor = valor.replace(/[^0-9]/g, '');


    // Captura os 9 primeiros dígitos do CPF
    // Ex.: 02546288423 = 025462884
    var digitos = valor.substr(0, 9);

    // Faz o cálculo dos 9 primeiros dígitos do CPF para obter o primeiro dígito
    var novo_cpf = calc_digitos_posicoes( digitos );

    // Faz o cálculo dos 10 dígitos do CPF para obter o último dígito
    var novo_cpf = calc_digitos_posicoes( novo_cpf, 11 );

    // Verifica se o novo CPF gerado é idêntico ao CPF enviado
    if ( novo_cpf === valor ) {
        $.ajax({
            type: 'POST',
            url: site_path + '/Clientes/valida-cpf',
            data: {
                cpf: cpf.val(),
                nascimento: nascimento
            },
            dataType: 'JSON',
            success: function (txt) {
                if (!empty(txt)) {
                    toastr.error(txt.msg);
                    cpf.val('');
                    $('form#' + form).find('button[type=submit], input[type=submit]').attr({
                        disabled: 'disabled',
                        onclick: 'return false'
                    });
                    if (txt.erro == 'idade') {
                        $('form#' + form).find('#nascimento').focus();
                    } else {
                        cpf.focus();
                    }
                } else {
                    $('form#' + form).find('button[type=submit], input[type=submit]').removeAttr('disabled onclick');
                }
            }
        })
    } else {
        // CPF inválido
        return toastr.error('CPF inválido!')
    }
    
} // valida_cpf

function validaPeriodos(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if (txt.res == 0) {
                toastr.success(txt.msg);
                Navegar('', 'back');
            } else {
                toastr.error(txt.msg);
            }
        }
    }).submit();
}

function verificaDuplicidade(form) {
    $('form#' + form).ajaxForm({
        beforeSubmit: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            if (txt.res == 0) {
                toastr.success(txt.msg);
                Navegar('', 'back');
            } else {
                toastr.error(txt.msg);
            }
        }
    }).submit();
}

function ajaxCountReclisar(form) {
    $.ajax({
        type: 'POST',
        url: site_path + '/Faturamentos/count-reclistar',
        data: form,
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif()
        },
        success: function (txt) {
            CloseGif();
            $('.count').text(txt.count);
            $('.total').text(formatReal(txt.total));
            $('.valor_recebido').text(formatReal(txt.valor_recebido));
            $('.valor_rec_matmed').text(formatReal(txt.valor_rec_matmed));
            $('.diferenca').text(formatReal(txt.diferenca));
            $('.total_caixa').text(formatReal(txt.total_caixa));
        }
    })
}

function ajaxGetTipoHistoriaModelos(column, id) {
    return $.ajax({
        type: 'POST',
        url: site_path + '/TipoHistoriaModelos/get-modelo/' + id,
        data: {
            id: id,
            column: column
        },
        dataType: 'JSON',
        beforeSend: function () {
            LoadGif()
        },
        complete: function () {
            CloseGif();
        },
        error: function () {
            toastr.error(msgErrorAjax);
        }
    });
}

var changeHref = function (id, form_group, controller, action) {
    var new_href = site_path + '/' + controller + '/' + action + '/' + id;
    form_group.find('.btnView').attr('href', new_href);
};

/**
 * 1ª Documentação - 15/03/2021, por Fernanda Taso
 * Gera relatório do SUS do menu superior 
 */
let geraRelatorioPrecoProcedimentoSUS = function (button) {

    let url = site_path + '/' + button.attr('controller') + '/' + button.attr('action') + '?isSus=1';
    let dados = $('#atendimento-form').serializeArray();
    let form = '';console.log(url);
    $('.temp-form').remove();
    $.each(dados, function (index, dado) {
        form += '<input class="temp-form" type="hidden" name="' + dado.name + '" value="' + dado.value + '">';
    });
    form += '<input class="temp-form" type="submit" id="report-submit">';
    
    let formulario = '<form class="temp-form" action="' + url + '" method="GET" target="_blank">' + form + '</form>';
    $(formulario).appendTo($('.append-report'));
    $('#report-submit').click();
    $('#btn-fill').click();
    click = null;
};