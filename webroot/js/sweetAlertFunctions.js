const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

function sweetToastInfo(title) {
    toast({
        type: 'info',
        title: title
    });
}

function sweetToastSuccess(title) {
    toast({
        type: 'success',
        title: title
    });
}

function sweetToastSuccessNavegarBack(title) {
    toast({
        type: 'success',
        title: title
    });
    setTimeout(function(){ Navegar('', 'back'); }, 3000);
}

function sweetToastSuccessWithReload(title, time) {
    toast({
        type: 'success',
        title: title
    });
    if (!time)
        setTimeout(function(){ location.reload(); }, 3000);
    else
        setTimeout(function(){ location.reload(); }, time);
}

function sweetToastSuccessWithReloadFast(title) {
    toast({
        type: 'success',
        title: title
    });
    setTimeout(function(){ location.reload(); }, 500);
}

function sweetToastSuccessWithReloadTable(title) {
    toast({
        type: 'success',
        title: title
    });
    $('.modal').modal('hide');
    $('#btn-fill').click();
}

function sweetToatSuccessWithRedirectionWithoutBlank(title, url){
    toast({
        type: 'success',
        title: title
    });
    window.location.replace(url)
}

function sweetToastError(title) {
    toast({
        type: 'error',
        title: title,
        timer: 3000
    });
}

function sweetError(title, text, cancelButtonText) {
    return swal({
        type: 'error',
        title: title,
        html: text,
        showCancelButton: true,
        showConfirmButton: false,
        cancelButtonText: cancelButtonText,
    })
}

function sweetErrorEndoscopia(title, text, confirmButtonText) {
    return swal({
        type: 'error',
        title: title,
        html: text,
        width: '100%',
        showConfirmButton: true,
        confirmButtonText: confirmButtonText,
        allowOutsideClick: false,
        preConfirm: () => {
            return window.open(`${site_path}/ExportaCooperativaEndoscopiaMsFaturas/selectParaModal`);
        }
    })
}

function sweetErrorExportaRemessaBoletos(title, text, confirmButtonText) {
    return swal({
        type: 'error',
        title: title,
        html: text,
        width: '100%',
        showConfirmButton: true,
        confirmButtonText: confirmButtonText,
        allowOutsideClick: false
    })
}

function sweetInfo(title, text, cancelButtonText) {
    return swal({
        type: 'info',
        title: title,
        text: text,
        showCancelButton: true,
        showConfirmButton: false,
        cancelButtonText: cancelButtonText,
    })
}

function sweetErrorWithRedirection(title, text, confirmButtonText, url, cancelButtonText) {
    return swal({
        type: 'error',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { window.open(url, '_blank')},
        showCancelButton: true,
        cancelButtonText: cancelButtonText
    })
}

function sweetInfoWithRedirection(title, text, confirmButtonText, url, cancelButtonText) {
    return swal({
        type: 'info',
        title: title,
        html: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { window.open(url, '_blank')},
        showCancelButton: true,
        cancelButtonText: cancelButtonText
    })
}

function sweetSuccessWithCallback(title, text, confirmButtonText,callback, params = null) {
    return swal({
        type: 'success',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { if($.isFunction(callback))callback(params);},
    });
}

function sweetSuccessWithRedirection(title, text, confirmButtonText, url, cancelButtonText) {
    return swal({
        type: 'success',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { window.open(url, '_blank')},
        showCancelButton: true,
        cancelButtonText: cancelButtonText
    })
}

function sweetSuccessWithRedirectionWithoutBlank(title, text, confirmButtonText, url, cancelButtonText) {
    return swal({
        type: 'success',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { window.location.replace(url)},
        showCancelButton: true,
        cancelButtonText: cancelButtonText
    });
}

function sweetInfoWithRedirectionWithoutBlank(title, text, confirmButtonText, url, cancelButtonText) {
    return swal({
        type: 'info',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { window.location.replace(url)},
        showCancelButton: true,
        cancelButtonText: cancelButtonText
    });
}

function sweetSuccessWithReload(title, text, confirmButtonText) {
    return swal({
        type: 'success',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => { location.reload() }
    })
}

function sweetSuccessWithRedirectionAndReload(title, text, confirmButtonText, url, cancelButtonText) {
    return swal({
        type: 'success',
        title: title,
        text: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => {
            window.open(url, '_blank');
        },
        onClose: () => {
            location.reload();
        },
        showCancelButton: true,
        cancelButtonText: cancelButtonText
    })
}

function sweetDeleteInfo(controller, id, funcao, id_funcao) {
    return swal({
        type: 'info',
        title: 'Atenção',
        text: 'Tem certeza que deseja excluir este registro?',
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Excluir',
        preConfirm: () => {
            return excluirComSweetAlert(controller, id, funcao, id_funcao);
        }
    })
}

function sweetSuccess(title, text, confirmButtonText) {
    return swal({
        type: 'success',
        title: title,
        html: text,
        confirmButtonText, confirmButtonText
    });
}

function sweetReturn(title, text, confirmButtonText, url) {
    return swal({
        type: 'info',
        title: title,
        html: text,
        confirmButtonText: confirmButtonText,
        preConfirm: () => {
            window.location = url;
        },
        onClose: () => {
            window.location = url;
        },
    })
}

function sweetInfoCallback(title, text, confirmButtonText) {
    return swal({
        type: 'info',
        title: title,
        text: text,
        confirmButtonText, confirmButtonText
    });
}

function sweetInfoWithMigrateTeamAndFaturaMatMed(title, text, atendimentoProcedimentoId, atendimentoId) {
    return swal({
        type: 'info',
        title: title,
        text: text,
        showCancelButton: true,
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Migrar',
        preConfirm: () => {
            return selectProcedure(atendimentoProcedimentoId, atendimentoId);
        }
    })
}