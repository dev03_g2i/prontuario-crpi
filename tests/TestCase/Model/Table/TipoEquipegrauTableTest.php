<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoEquipegrauTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoEquipegrauTable Test Case
 */
class TipoEquipegrauTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoEquipegrauTable
     */
    public $TipoEquipegrau;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipo_equipegrau',
        'app.situacaos',
        'app.atendimento_procedimento_equipes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TipoEquipegrau') ? [] : ['className' => TipoEquipegrauTable::class];
        $this->TipoEquipegrau = TableRegistry::get('TipoEquipegrau', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TipoEquipegrau);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
