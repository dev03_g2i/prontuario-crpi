<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NfTipoRecolhimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NfTipoRecolhimentosTable Test Case
 */
class NfTipoRecolhimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NfTipoRecolhimentosTable
     */
    public $NfTipoRecolhimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nf_tipo_recolhimentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NfTipoRecolhimentos') ? [] : ['className' => NfTipoRecolhimentosTable::class];
        $this->NfTipoRecolhimentos = TableRegistry::get('NfTipoRecolhimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NfTipoRecolhimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
