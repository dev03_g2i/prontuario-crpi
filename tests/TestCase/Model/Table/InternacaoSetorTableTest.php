<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InternacaoSetorTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InternacaoSetorTable Test Case
 */
class InternacaoSetorTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InternacaoSetorTable
     */
    public $InternacaoSetor;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.internacao_setor',
        'app.situacao_cadastros',
        'app.atendimentos',
        'app.clientes',
        'app.medico_responsaveis',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.agenda_periodos',
        'app.agenda_bloqueios',
        'app.agendas',
        'app.tipo_agendas',
        'app.configuracao_periodos',
        'app.situacao_agendas',
        'app.convenios',
        'app.tipo_convenios',
        'app.preco_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.atendimento_procedimentos',
        'app.solicitantes',
        'app.situacao_faturas',
        'app.fatura_encerramentos',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.temp_faturamentos',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.dentes',
        'app.regioes',
        'app.face_itens',
        'app.faces',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.controle_financeiro',
        'app.iten_cobrancas',
        'app.situacao_laudos',
        'app.situacaos',
        'app.user_reg',
        'app.user_alt',
        'app.convenio_regracalculo',
        'app.fatura_recalcular',
        'app.user_edit',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.estado_civis',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.tipo_atendimentos',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.classificacaocontas',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.internacao_acomodacao',
        'app.internacao_carater_atendimento',
        'app.internacao_motivo_alta'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InternacaoSetor') ? [] : ['className' => InternacaoSetorTable::class];
        $this->InternacaoSetor = TableRegistry::get('InternacaoSetor', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InternacaoSetor);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
