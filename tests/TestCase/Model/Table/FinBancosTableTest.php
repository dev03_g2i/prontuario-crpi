<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinBancosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinBancosTable Test Case
 */
class FinBancosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinBancosTable
     */
    public $FinBancos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_bancos',
        'app.fin_banco_movimentos',
        'app.fin_contabilidade_bancos',
        'app.fin_movimentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinBancos') ? [] : ['className' => FinBancosTable::class];
        $this->FinBancos = TableRegistry::get('FinBancos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinBancos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
