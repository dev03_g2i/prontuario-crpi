<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConvenioImpostosParametroTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConvenioImpostosParametroTable Test Case
 */
class ConvenioImpostosParametroTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConvenioImpostosParametroTable
     */
    public $ConvenioImpostosParametro;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.convenio_impostos_parametro'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConvenioImpostosParametro') ? [] : ['className' => ConvenioImpostosParametroTable::class];
        $this->ConvenioImpostosParametro = TableRegistry::get('ConvenioImpostosParametro', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConvenioImpostosParametro);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
