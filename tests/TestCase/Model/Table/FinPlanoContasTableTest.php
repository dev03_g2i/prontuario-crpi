<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinPlanoContasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinPlanoContasTable Test Case
 */
class FinPlanoContasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinPlanoContasTable
     */
    public $FinPlanoContas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_plano_contas',
        'app.fin_contas_pagar',
        'app.fin_fornecedores',
        'app.fin_contabilidades',
        'app.fin_contabilidade_bancos',
        'app.fin_bancos',
        'app.fin_banco_movimentos',
        'app.fin_movimentos',
        'app.fin_fornecedors',
        'app.fin_contas_pagars',
        'app.fin_contas_pagar_planejamentos',
        'app.clientes',
        'app.medico_responsaveis',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.agenda_periodos',
        'app.tipo_agendas',
        'app.agenda_bloqueios',
        'app.agendas',
        'app.situacao_agendas',
        'app.convenios',
        'app.tipo_convenios',
        'app.atendimentos',
        'app.configuracao_periodos',
        'app.unidades',
        'app.fatura_encerramentos',
        'app.user_edit',
        'app.situacao_faturaencerramentos',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.preco_procedimentos',
        'app.user_reg',
        'app.user_alt',
        'app.convenio_regracalculo',
        'app.solicitantes',
        'app.solicitante_cbo',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.users_assinado',
        'app.atendimento_procedimento_equipes',
        'app.tipo_equipes',
        'app.tipo_equipegrau',
        'app.situacaos',
        'app.configuracao_apuracao',
        'app.controle_financeiro',
        'app.situacao_laudos',
        'app.temp_faturamentos',
        'app.fatura_nfs',
        'app.tipo_atendimentos',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar',
        'app.fornecedores',
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.fornecedors',
        'app.contaspagars',
        'app.contaspagar_planejamentos',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.movimento',
        'app.movimento_banco',
        'app.programacao',
        'app.deducoes',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.abatimentos_contaspagar',
        'app.abatimentos',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.internacao_acomodacao',
        'app.internacao_carater_atendimento',
        'app.internacao_motivo_alta',
        'app.internacao_setor',
        'app.internacao_leito',
        'app.fatura_recalcular',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.estado_civis',
        'app.indicacao',
        'app.religiao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.fin_contas_receber_planejamentos',
        'app.fin_grupo_contabilidades',
        'app.fin_planejamentos',
        'app.fin_tipo_pagamento',
        'app.statuses',
        'app.fin_tipo_documento',
        'app.fin_abatimentos_contas_pagar',
        'app.fin_abatimentos',
        'app.fin_abatimentos_contas_receber',
        'app.fin_contas_recebers',
        'app.fin_contas_receber',
        'app.externs',
        'app.controle_financeiros',
        'app.faturas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinPlanoContas') ? [] : ['className' => FinPlanoContasTable::class];
        $this->FinPlanoContas = TableRegistry::get('FinPlanoContas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinPlanoContas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
