<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EstqKitTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EstqKitTable Test Case
 */
class EstqKitTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\EstqKitTable
     */
    public $EstqKit;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.estq_kit',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.cliente_historicos',
        'app.clientes',
        'app.convenios',
        'app.tipo_convenios',
        'app.atendimentos',
        'app.configuracao_periodos',
        'app.agendas',
        'app.tipo_agendas',
        'app.situacao_agendas',
        'app.user_edit',
        'app.fatura_encerramentos',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.atendimento_procedimentos',
        'app.solicitantes',
        'app.solicitante_cbo',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.dentes',
        'app.regioes',
        'app.face_itens',
        'app.faces',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.users_assinado',
        'app.atendimento_procedimento_equipes',
        'app.tipo_equipes',
        'app.tipo_equipegrau',
        'app.situacaos',
        'app.configuracao_apuracao',
        'app.user_reg',
        'app.user_alt',
        'app.controle_financeiro',
        'app.iten_cobrancas',
        'app.situacao_laudos',
        'app.temp_faturamentos',
        'app.fatura_nfs',
        'app.agenda_bloqueios',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.tipo_atendimentos',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar',
        'app.fornecedores',
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.fornecedors',
        'app.contaspagars',
        'app.contaspagar_planejamentos',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.movimento',
        'app.movimento_banco',
        'app.programacao',
        'app.deducoes',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.abatimentos_contaspagar',
        'app.abatimentos',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.internacao_acomodacao',
        'app.internacao_carater_atendimento',
        'app.internacao_motivo_alta',
        'app.internacao_setor',
        'app.internacao_leito',
        'app.preco_procedimentos',
        'app.convenio_regracalculo',
        'app.fatura_recalcular',
        'app.convenio_impostos_parametro',
        'app.convenio_tipoimposto',
        'app.estado_civis',
        'app.indicacao',
        'app.religiao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.agenda_periodos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EstqKit') ? [] : ['className' => EstqKitTable::class];
        $this->EstqKit = TableRegistry::get('EstqKit', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EstqKit);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
