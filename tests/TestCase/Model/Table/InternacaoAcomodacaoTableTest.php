<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InternacaoAcomodacaoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InternacaoAcomodacaoTable Test Case
 */
class InternacaoAcomodacaoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InternacaoAcomodacaoTable
     */
    public $InternacaoAcomodacao;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.internacao_acomodacao',
        'app.situacao_cadastros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InternacaoAcomodacao') ? [] : ['className' => InternacaoAcomodacaoTable::class];
        $this->InternacaoAcomodacao = TableRegistry::get('InternacaoAcomodacao', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InternacaoAcomodacao);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
