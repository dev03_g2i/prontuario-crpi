<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContabilidadeBancosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContabilidadeBancosTable Test Case
 */
class ContabilidadeBancosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContabilidadeBancosTable
     */
    public $ContabilidadeBancos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contabilidade_bancos',
        'app.contabilidades',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.planocontas',
        'app.classificacaocontas',
        'app.fornecedors',
        'app.contaspagars'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContabilidadeBancos') ? [] : ['className' => ContabilidadeBancosTable::class];
        $this->ContabilidadeBancos = TableRegistry::get('ContabilidadeBancos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContabilidadeBancos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
