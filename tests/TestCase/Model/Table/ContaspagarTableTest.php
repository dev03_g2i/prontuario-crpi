<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContaspagarTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContaspagarTable Test Case
 */
class ContaspagarTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContaspagarTable
     */
    public $Contaspagar;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contaspagar',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar_planejamentos',
        'app.contasreceber',
        'app.contasreceber_planejamentos',
        'app.movimentos',
        'app.fornecedors',
        'app.bancos',
        'app.banco_movimentos',
        'app.contabilidade_bancos',
        'app.contabilidades',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.contaspagars',
        'app.fornecedores',
        'app.tipo_pagamento',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.abatimentos',
        'app.abatimentos_contaspagar'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Contaspagar') ? [] : ['className' => ContaspagarTable::class];
        $this->Contaspagar = TableRegistry::get('Contaspagar', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contaspagar);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
