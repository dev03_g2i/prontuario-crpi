<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConfiguracaoRelCaixaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConfiguracaoRelCaixaTable Test Case
 */
class ConfiguracaoRelCaixaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConfiguracaoRelCaixaTable
     */
    public $ConfiguracaoRelCaixa;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.configuracao_rel_caixa',
        'app.situacao_cadastros',
        'app.configuracao_rel_caixa_usuario'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConfiguracaoRelCaixa') ? [] : ['className' => ConfiguracaoRelCaixaTable::class];
        $this->ConfiguracaoRelCaixa = TableRegistry::get('ConfiguracaoRelCaixa', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConfiguracaoRelCaixa);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
