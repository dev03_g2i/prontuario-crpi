<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TextoGruposTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TextoGruposTable Test Case
 */
class TextoGruposTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TextoGruposTable
     */
    public $TextoGrupos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.texto_grupos',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.agenda_periodos',
        'app.agenda_bloqueios',
        'app.agendas',
        'app.tipo_agendas',
        'app.configuracao_periodos',
        'app.atendimentos',
        'app.clientes',
        'app.convenios',
        'app.tipo_convenios',
        'app.preco_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.atendimento_procedimentos',
        'app.situacao_faturas',
        'app.fatura_encerramentos',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.temp_faturamentos',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.dentes',
        'app.regioes',
        'app.face_itens',
        'app.faces',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.iten_cobrancas',
        'app.situacao_laudos',
        'app.situacaos',
        'app.estado_civis',
        'app.user_edit',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.tipo_atendimentos',
        'app.solicitantes',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.classificacaocontas',
        'app.situacao_agendas',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TextoGrupos') ? [] : ['className' => 'App\Model\Table\TextoGruposTable'];
        $this->TextoGrupos = TableRegistry::get('TextoGrupos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TextoGrupos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
