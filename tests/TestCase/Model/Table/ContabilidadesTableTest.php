<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContabilidadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContabilidadesTable Test Case
 */
class ContabilidadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContabilidadesTable
     */
    public $Contabilidades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.planocontas',
        'app.classificacaocontas',
        'app.fornecedors',
        'app.contaspagars',
        'app.contaspagar',
        'app.contaspagar_planejamentos',
        'app.contasreceber',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Contabilidades') ? [] : ['className' => ContabilidadesTable::class];
        $this->Contabilidades = TableRegistry::get('Contabilidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contabilidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
