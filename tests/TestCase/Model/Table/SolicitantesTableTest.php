<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SolicitantesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SolicitantesTable Test Case
 */
class SolicitantesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SolicitantesTable
     */
    public $Solicitantes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.solicitantes',
        'app.situacao_cadastros',
        'app.users',
        'app.atendimentos',
        'app.clientes',
        'app.medico_responsaveis',
        'app.convenios',
        'app.tipo_convenios',
        'app.preco_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.atendimento_procedimentos',
        'app.atendimento_itens',
        'app.dentes',
        'app.regioes',
        'app.face_itens',
        'app.faces',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.iten_cobrancas',
        'app.estado_civis',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.historico_tratamentos',
        'app.situacaos',
        'app.historico_face_artigos',
        'app.face_artigos',
        'app.procedimento_gastos',
        'app.gastos',
        'app.situacao',
        'app.gasto_itens',
        'app.estq_artigo',
        'app.usuarios',
        'app.artigos',
        'app.historico_iten_artigos',
        'app.atendimentoiten_artigos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.agendas',
        'app.tipo_agendas',
        'app.grupo_agendas',
        'app.situacao_agendas',
        'app.unidades',
        'app.tipo_atendimentos',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.classificacaocontas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Solicitantes') ? [] : ['className' => 'App\Model\Table\SolicitantesTable'];
        $this->Solicitantes = TableRegistry::get('Solicitantes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Solicitantes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
