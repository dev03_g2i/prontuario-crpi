<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConfiguracaoInternacaoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConfiguracaoInternacaoTable Test Case
 */
class ConfiguracaoInternacaoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConfiguracaoInternacaoTable
     */
    public $ConfiguracaoInternacao;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.configuracao_internacao'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConfiguracaoInternacao') ? [] : ['className' => ConfiguracaoInternacaoTable::class];
        $this->ConfiguracaoInternacao = TableRegistry::get('ConfiguracaoInternacao', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConfiguracaoInternacao);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
