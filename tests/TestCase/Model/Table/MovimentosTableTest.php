<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MovimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MovimentosTable Test Case
 */
class MovimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MovimentosTable
     */
    public $Movimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movimentos',
        'app.planocontas',
        'app.classificacaocontas',
        'app.fornecedors',
        'app.bancos',
        'app.banco_movimentos',
        'app.contabilidade_bancos',
        'app.contaspagars',
        'app.contabilidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Movimentos') ? [] : ['className' => MovimentosTable::class];
        $this->Movimentos = TableRegistry::get('Movimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Movimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
