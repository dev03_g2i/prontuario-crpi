<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConfiguracaoAtendimentoDocsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConfiguracaoAtendimentoDocsTable Test Case
 */
class ConfiguracaoAtendimentoDocsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConfiguracaoAtendimentoDocsTable
     */
    public $ConfiguracaoAtendimentoDocs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.configuracao_atendimento_docs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConfiguracaoAtendimentoDocs') ? [] : ['className' => ConfiguracaoAtendimentoDocsTable::class];
        $this->ConfiguracaoAtendimentoDocs = TableRegistry::get('ConfiguracaoAtendimentoDocs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConfiguracaoAtendimentoDocs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
