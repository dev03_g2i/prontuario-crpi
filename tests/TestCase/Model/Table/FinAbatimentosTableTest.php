<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinAbatimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinAbatimentosTable Test Case
 */
class FinAbatimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinAbatimentosTable
     */
    public $FinAbatimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_abatimentos',
        'app.fin_abatimentos_contas_pagar',
        'app.fin_contas_pagars',
        'app.fin_abatimentos_contas_receber',
        'app.fin_contas_recebers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinAbatimentos') ? [] : ['className' => FinAbatimentosTable::class];
        $this->FinAbatimentos = TableRegistry::get('FinAbatimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinAbatimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
