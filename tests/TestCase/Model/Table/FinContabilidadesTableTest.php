<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinContabilidadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinContabilidadesTable Test Case
 */
class FinContabilidadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinContabilidadesTable
     */
    public $FinContabilidades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_contabilidades',
        'app.fin_contabilidade_bancos',
        'app.fin_contas_pagar',
        'app.fin_contas_pagar_planejamentos',
        'app.fin_contas_receber_planejamentos',
        'app.fin_grupo_contabilidades',
        'app.fin_movimentos',
        'app.fin_planejamentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinContabilidades') ? [] : ['className' => FinContabilidadesTable::class];
        $this->FinContabilidades = TableRegistry::get('FinContabilidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinContabilidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
