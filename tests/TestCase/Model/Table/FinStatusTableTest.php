<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinStatusTable Test Case
 */
class FinStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinStatusTable
     */
    public $FinStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinStatus') ? [] : ['className' => FinStatusTable::class];
        $this->FinStatus = TableRegistry::get('FinStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
