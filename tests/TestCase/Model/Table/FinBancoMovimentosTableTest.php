<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinBancoMovimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinBancoMovimentosTable Test Case
 */
class FinBancoMovimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinBancoMovimentosTable
     */
    public $FinBancoMovimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_banco_movimentos',
        'app.fin_bancos',
        'app.fin_contabilidade_bancos',
        'app.fin_movimentos',
        'app.fin_plano_contas',
        'app.fin_fornecedors',
        'app.fin_contas_pagars',
        'app.fin_contabilidades',
        'app.fin_contas_pagar',
        'app.fin_contas_pagar_planejamentos',
        'app.fin_contas_receber_planejamentos',
        'app.fin_grupo_contabilidades',
        'app.fin_planejamentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinBancoMovimentos') ? [] : ['className' => FinBancoMovimentosTable::class];
        $this->FinBancoMovimentos = TableRegistry::get('FinBancoMovimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinBancoMovimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
