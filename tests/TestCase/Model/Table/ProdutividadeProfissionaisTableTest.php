<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProdutividadeProfissionaisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProdutividadeProfissionaisTable Test Case
 */
class ProdutividadeProfissionaisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProdutividadeProfissionaisTable
     */
    public $ProdutividadeProfissionais;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.produtividade_profissionais',
        'app.convenios',
        'app.tipo_convenios',
        'app.users',
        'app.grupo_usuarios',
        'app.situacao_cadastros',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.agenda_periodos',
        'app.agenda_bloqueios',
        'app.agendas',
        'app.tipo_agendas',
        'app.clientes',
        'app.estado_civis',
        'app.user_edit',
        'app.fatura_encerramentos',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.preco_procedimentos',
        'app.atendimentos',
        'app.unidades',
        'app.tipo_atendimentos',
        'app.solicitantes',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.classificacaocontas',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.temp_faturamentos',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.historico_tratamentos',
        'app.situacaos',
        'app.historico_face_artigos',
        'app.face_artigos',
        'app.procedimento_gastos',
        'app.gastos',
        'app.situacao',
        'app.gasto_itens',
        'app.estq_artigo',
        'app.usuarios',
        'app.artigos',
        'app.historico_iten_artigos',
        'app.atendimentoiten_artigos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.situacao_agendas',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProdutividadeProfissionais') ? [] : ['className' => 'App\Model\Table\ProdutividadeProfissionaisTable'];
        $this->ProdutividadeProfissionais = TableRegistry::get('ProdutividadeProfissionais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProdutividadeProfissionais);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
