<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LaudosConfiguracoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LaudosConfiguracoesTable Test Case
 */
class LaudosConfiguracoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LaudosConfiguracoesTable
     */
    public $LaudosConfiguracoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.laudos_configuracoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LaudosConfiguracoes') ? [] : ['className' => LaudosConfiguracoesTable::class];
        $this->LaudosConfiguracoes = TableRegistry::get('LaudosConfiguracoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LaudosConfiguracoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
