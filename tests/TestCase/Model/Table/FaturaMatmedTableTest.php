<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FaturaMatmedTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FaturaMatmedTable Test Case
 */
class FaturaMatmedTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FaturaMatmedTable
     */
    public $FaturaMatmed;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fatura_matmed',
        'app.artigos',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.agenda_periodos',
        'app.agenda_bloqueios',
        'app.agendas',
        'app.tipo_agendas',
        'app.configuracao_periodos',
        'app.atendimentos',
        'app.clientes',
        'app.convenios',
        'app.tipo_convenios',
        'app.preco_procedimentos',
        'app.estado_civis',
        'app.user_edit',
        'app.fatura_encerramentos',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.temp_faturamentos',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.tipo_atendimentos',
        'app.solicitantes',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.classificacaocontas',
        'app.situacao_agendas',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.situacao_laudos',
        'app.situacaos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FaturaMatmed') ? [] : ['className' => 'App\Model\Table\FaturaMatmedTable'];
        $this->FaturaMatmed = TableRegistry::get('FaturaMatmed', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FaturaMatmed);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
