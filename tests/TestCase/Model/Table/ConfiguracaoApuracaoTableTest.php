<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConfiguracaoApuracaoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConfiguracaoApuracaoTable Test Case
 */
class ConfiguracaoApuracaoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConfiguracaoApuracaoTable
     */
    public $ConfiguracaoApuracao;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.configuracao_apuracao',
        'app.situacao_cadastros',
        'app.atendimento_procedimento_equipes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConfiguracaoApuracao') ? [] : ['className' => ConfiguracaoApuracaoTable::class];
        $this->ConfiguracaoApuracao = TableRegistry::get('ConfiguracaoApuracao', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConfiguracaoApuracao);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
