<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinTipoPagamentoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinTipoPagamentoTable Test Case
 */
class FinTipoPagamentoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinTipoPagamentoTable
     */
    public $FinTipoPagamento;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_tipo_pagamento',
        'app.statuses',
        'app.tipo_documentos',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.cliente_historicos',
        'app.clientes',
        'app.convenios',
        'app.tipo_convenios',
        'app.atendimentos',
        'app.configuracao_periodos',
        'app.agendas',
        'app.tipo_agendas',
        'app.situacao_agendas',
        'app.user_edit',
        'app.fatura_encerramentos',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.preco_procedimentos',
        'app.user_reg',
        'app.user_alt',
        'app.convenio_regracalculo',
        'app.solicitantes',
        'app.solicitante_cbo',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.users_assinado',
        'app.atendimento_procedimento_equipes',
        'app.tipo_equipes',
        'app.tipo_equipegrau',
        'app.situacaos',
        'app.configuracao_apuracao',
        'app.controle_financeiro',
        'app.situacao_laudos',
        'app.temp_faturamentos',
        'app.fatura_nfs',
        'app.agenda_bloqueios',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.tipo_atendimentos',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar',
        'app.fornecedores',
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.fornecedors',
        'app.contaspagars',
        'app.contaspagar_planejamentos',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.movimento',
        'app.movimento_banco',
        'app.programacao',
        'app.deducoes',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.abatimentos_contaspagar',
        'app.abatimentos',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.internacao_acomodacao',
        'app.internacao_carater_atendimento',
        'app.internacao_motivo_alta',
        'app.internacao_setor',
        'app.internacao_leito',
        'app.fatura_recalcular',
        'app.estado_civis',
        'app.indicacao',
        'app.religiao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.agenda_periodos',
        'app.fin_contas_pagar',
        'app.fin_plano_contas',
        'app.fin_fornecedors',
        'app.fin_contabilidades',
        'app.fin_contabilidade_bancos',
        'app.fin_bancos',
        'app.fin_banco_movimentos',
        'app.fin_movimentos',
        'app.fin_contas_pagars',
        'app.fin_contas_pagar_planejamentos',
        'app.fin_contas_receber_planejamentos',
        'app.fin_grupo_contabilidades',
        'app.fin_planejamentos',
        'app.fin_tipo_pagamentos',
        'app.fin_tipo_documentos',
        'app.fin_abatimentos_contas_pagar',
        'app.fin_abatimentos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinTipoPagamento') ? [] : ['className' => FinTipoPagamentoTable::class];
        $this->FinTipoPagamento = TableRegistry::get('FinTipoPagamento', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinTipoPagamento);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
