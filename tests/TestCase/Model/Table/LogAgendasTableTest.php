<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LogAgendasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LogAgendasTable Test Case
 */
class LogAgendasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LogAgendasTable
     */
    public $LogAgendas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.log_agendas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LogAgendas') ? [] : ['className' => LogAgendasTable::class];
        $this->LogAgendas = TableRegistry::get('LogAgendas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LogAgendas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
