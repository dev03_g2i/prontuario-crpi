<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinAbatimentosContasPagarTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinAbatimentosContasPagarTable Test Case
 */
class FinAbatimentosContasPagarTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinAbatimentosContasPagarTable
     */
    public $FinAbatimentosContasPagar;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_abatimentos_contas_pagar',
        'app.fin_abatimentos',
        'app.fin_contas_pagars'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinAbatimentosContasPagar') ? [] : ['className' => FinAbatimentosContasPagarTable::class];
        $this->FinAbatimentosContasPagar = TableRegistry::get('FinAbatimentosContasPagar', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinAbatimentosContasPagar);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
