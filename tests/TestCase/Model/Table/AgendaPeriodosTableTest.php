<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AgendaPeriodosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AgendaPeriodosTable Test Case
 */
class AgendaPeriodosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AgendaPeriodosTable
     */
    public $AgendaPeriodos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.agenda_periodos',
        'app.grupo_agendas',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.medico_responsaveis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AgendaPeriodos') ? [] : ['className' => 'App\Model\Table\AgendaPeriodosTable'];
        $this->AgendaPeriodos = TableRegistry::get('AgendaPeriodos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AgendaPeriodos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
