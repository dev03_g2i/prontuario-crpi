<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InternacaoMotivoAltaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InternacaoMotivoAltaTable Test Case
 */
class InternacaoMotivoAltaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InternacaoMotivoAltaTable
     */
    public $InternacaoMotivoAlta;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.internacao_motivo_alta',
        'app.situacao_cadastros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InternacaoMotivoAlta') ? [] : ['className' => InternacaoMotivoAltaTable::class];
        $this->InternacaoMotivoAlta = TableRegistry::get('InternacaoMotivoAlta', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InternacaoMotivoAlta);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
