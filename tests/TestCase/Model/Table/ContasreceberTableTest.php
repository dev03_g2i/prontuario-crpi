<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContasreceberTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContasreceberTable Test Case
 */
class ContasreceberTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContasreceberTable
     */
    public $Contasreceber;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contasreceber',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar',
        'app.fornecedores',
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.fornecedors',
        'app.contaspagars',
        'app.contaspagar_planejamentos',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.tipo_pagamento',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.abatimentos',
        'app.abatimentos_contaspagar',
        'app.cliente_clone',
        'app.planejamentos',
        'app.externs',
        'app.controle_financeiros',
        'app.faturas',
        'app.abatimentos_contasreceber'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Contasreceber') ? [] : ['className' => ContasreceberTable::class];
        $this->Contasreceber = TableRegistry::get('Contasreceber', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contasreceber);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
