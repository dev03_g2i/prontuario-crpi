<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NfSituacaoNotasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NfSituacaoNotasTable Test Case
 */
class NfSituacaoNotasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NfSituacaoNotasTable
     */
    public $NfSituacaoNotas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nf_situacao_notas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NfSituacaoNotas') ? [] : ['className' => NfSituacaoNotasTable::class];
        $this->NfSituacaoNotas = TableRegistry::get('NfSituacaoNotas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NfSituacaoNotas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
