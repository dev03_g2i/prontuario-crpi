<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BancoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BancoTable Test Case
 */
class BancoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BancoTable
     */
    public $Banco;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banco',
        'app.contabilidade',
        'app.contasreceber',
        'app.contabilidade_banco',
        'app.movimento',
        'app.movimento_banco'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Banco') ? [] : ['className' => BancoTable::class];
        $this->Banco = TableRegistry::get('Banco', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Banco);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
