<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SolicitanteCboTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SolicitanteCboTable Test Case
 */
class SolicitanteCboTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SolicitanteCboTable
     */
    public $SolicitanteCbo;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.solicitante_cbo',
        'app.situacao_cadastros'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SolicitanteCbo') ? [] : ['className' => SolicitanteCboTable::class];
        $this->SolicitanteCbo = TableRegistry::get('SolicitanteCbo', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SolicitanteCbo);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
