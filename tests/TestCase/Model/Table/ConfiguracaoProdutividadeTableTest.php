<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConfiguracaoProdutividadeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ConfiguracaoProdutividadeTable Test Case
 */
class ConfiguracaoProdutividadeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ConfiguracaoProdutividadeTable
     */
    public $ConfiguracaoProdutividade;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.configuracao_produtividade'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ConfiguracaoProdutividade') ? [] : ['className' => ConfiguracaoProdutividadeTable::class];
        $this->ConfiguracaoProdutividade = TableRegistry::get('ConfiguracaoProdutividade', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ConfiguracaoProdutividade);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
