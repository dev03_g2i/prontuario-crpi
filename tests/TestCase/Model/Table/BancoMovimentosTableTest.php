<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BancoMovimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BancoMovimentosTable Test Case
 */
class BancoMovimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BancoMovimentosTable
     */
    public $BancoMovimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.banco_movimentos',
        'app.bancos',
        'app.contabilidade_bancos',
        'app.movimentos',
        'app.planocontas',
        'app.classificacaocontas',
        'app.fornecedors',
        'app.contaspagars',
        'app.contabilidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BancoMovimentos') ? [] : ['className' => BancoMovimentosTable::class];
        $this->BancoMovimentos = TableRegistry::get('BancoMovimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BancoMovimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
