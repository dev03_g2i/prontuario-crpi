<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinAbatimentosContasReceberTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinAbatimentosContasReceberTable Test Case
 */
class FinAbatimentosContasReceberTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinAbatimentosContasReceberTable
     */
    public $FinAbatimentosContasReceber;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_abatimentos_contas_receber',
        'app.fin_abatimentos',
        'app.fin_contas_recebers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinAbatimentosContasReceber') ? [] : ['className' => FinAbatimentosContasReceberTable::class];
        $this->FinAbatimentosContasReceber = TableRegistry::get('FinAbatimentosContasReceber', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinAbatimentosContasReceber);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
