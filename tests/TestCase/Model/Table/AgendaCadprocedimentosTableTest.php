<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AgendaCadprocedimentosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AgendaCadprocedimentosTable Test Case
 */
class AgendaCadprocedimentosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AgendaCadprocedimentosTable
     */
    public $AgendaCadprocedimentos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.agenda_cadprocedimentos',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.agenda_periodos',
        'app.agenda_bloqueios',
        'app.agenda_procedimentos',
        'app.agendas',
        'app.tipo_agendas',
        'app.clientes',
        'app.convenios',
        'app.tipo_convenios',
        'app.atendimentos',
        'app.unidades',
        'app.tipo_atendimentos',
        'app.solicitantes',
        'app.user_edit',
        'app.origens',
        'app.users_up',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.preco_procedimentos',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.classificacaocontas',
        'app.estado_civis',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.historico_tratamentos',
        'app.situacaos',
        'app.historico_face_artigos',
        'app.face_artigos',
        'app.procedimento_gastos',
        'app.gastos',
        'app.situacao',
        'app.gasto_itens',
        'app.estq_artigo',
        'app.usuarios',
        'app.artigos',
        'app.historico_iten_artigos',
        'app.atendimentoiten_artigos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.situacao_agendas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('AgendaCadprocedimentos') ? [] : ['className' => 'App\Model\Table\AgendaCadprocedimentosTable'];
        $this->AgendaCadprocedimentos = TableRegistry::get('AgendaCadprocedimentos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->AgendaCadprocedimentos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
