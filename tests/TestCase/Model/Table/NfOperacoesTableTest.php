<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NfOperacoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NfOperacoesTable Test Case
 */
class NfOperacoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NfOperacoesTable
     */
    public $NfOperacoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nf_operacoes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NfOperacoes') ? [] : ['className' => NfOperacoesTable::class];
        $this->NfOperacoes = TableRegistry::get('NfOperacoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NfOperacoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
