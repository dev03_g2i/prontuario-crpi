<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinTipoClassificacaoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinTipoClassificacaoTable Test Case
 */
class FinTipoClassificacaoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FinTipoClassificacaoTable
     */
    public $FinTipoClassificacao;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_tipo_classificacao'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FinTipoClassificacao') ? [] : ['className' => FinTipoClassificacaoTable::class];
        $this->FinTipoClassificacao = TableRegistry::get('FinTipoClassificacao', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FinTipoClassificacao);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
