<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApurarTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApurarTable Test Case
 */
class ApurarTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApurarTable
     */
    public $Apurar;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.apurar',
        'app.users',
        'app.grupo_usuarios',
        'app.situacao_cadastros',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.users_up'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Apurar') ? [] : ['className' => 'App\Model\Table\ApurarTable'];
        $this->Apurar = TableRegistry::get('Apurar', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Apurar);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
