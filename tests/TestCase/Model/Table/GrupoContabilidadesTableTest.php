<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GrupoContabilidadesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GrupoContabilidadesTable Test Case
 */
class GrupoContabilidadesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GrupoContabilidadesTable
     */
    public $GrupoContabilidades;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.grupo_contabilidades',
        'app.grupos',
        'app.contabilidades'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('GrupoContabilidades') ? [] : ['className' => GrupoContabilidadesTable::class];
        $this->GrupoContabilidades = TableRegistry::get('GrupoContabilidades', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GrupoContabilidades);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
