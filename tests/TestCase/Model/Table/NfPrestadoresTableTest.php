<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NfPrestadoresTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NfPrestadoresTable Test Case
 */
class NfPrestadoresTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NfPrestadoresTable
     */
    public $NfPrestadores;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.nf_prestadores',
        'app.tipo_logradouros',
        'app.tipo_bairros',
        'app.cidades',
        'app.tributacaos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('NfPrestadores') ? [] : ['className' => NfPrestadoresTable::class];
        $this->NfPrestadores = TableRegistry::get('NfPrestadores', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NfPrestadores);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeFind method
     *
     * @return void
     */
    public function testBeforeFind()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
