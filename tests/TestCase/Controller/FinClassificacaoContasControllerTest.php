<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FinClassificacaoContasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FinClassificacaoContasController Test Case
 */
class FinClassificacaoContasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fin_classificacao_contas',
        'app.fin_plano_contas',
        'app.fin_contas_pagar',
        'app.fin_fornecedores',
        'app.fin_contabilidades',
        'app.fin_contabilidade_bancos',
        'app.fin_bancos',
        'app.fin_banco_movimentos',
        'app.fin_movimentos',
        'app.clientes',
        'app.medico_responsaveis',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.solicitantes',
        'app.solicitante_cbo',
        'app.atendimentos',
        'app.configuracao_periodos',
        'app.agendas',
        'app.tipo_agendas',
        'app.situacao_agendas',
        'app.convenios',
        'app.tipo_convenios',
        'app.preco_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.fatura_recalcular',
        'app.user_reg',
        'app.fatura_encerramentos',
        'app.user_edit',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.atendimento_procedimentos',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.dentes',
        'app.regioes',
        'app.face_itens',
        'app.faces',
        'app.financeiro',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.users_assinado',
        'app.atendimento_procedimento_equipes',
        'app.tipo_equipes',
        'app.tipo_equipegrau',
        'app.situacaos',
        'app.configuracao_apuracao',
        'app.user_alt',
        'app.controle_financeiro',
        'app.iten_cobrancas',
        'app.situacao_laudos',
        'app.temp_faturamentos',
        'app.fatura_nfs',
        'app.convenio_regracalculo',
        'app.banco',
        'app.contabilidade',
        'app.contasreceber',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar',
        'app.fornecedores',
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.fornecedors',
        'app.contaspagars',
        'app.contaspagar_planejamentos',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.tipo_pagamento',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.abatimentos_contaspagar',
        'app.abatimentos',
        'app.cliente_clone',
        'app.programacao',
        'app.deducoes',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.contabilidade_banco',
        'app.movimento',
        'app.movimento_banco',
        'app.agenda_bloqueios',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.tipo_atendimentos',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.internacao_acomodacao',
        'app.internacao_carater_atendimento',
        'app.internacao_motivo_alta',
        'app.internacao_setor',
        'app.internacao_leito',
        'app.agenda_periodos',
        'app.estado_civis',
        'app.indicacao',
        'app.religiao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.fin_contas_pagar_planejamentos',
        'app.fin_fornecedors',
        'app.fin_contas_receber_planejamentos',
        'app.fin_grupo_contabilidades',
        'app.fin_grupos',
        'app.fin_planejamentos',
        'app.fin_tipo_pagamento',
        'app.fin_status',
        'app.fin_tipo_documento',
        'app.fin_abatimentos_contas_pagar',
        'app.fin_abatimentos',
        'app.fin_abatimentos_contas_receber',
        'app.fin_contas_recebers',
        'app.fin_contas_pagars',
        'app.fin_contas_receber'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fill method
     *
     * @return void
     */
    public function testFill()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getedit method
     *
     * @return void
     */
    public function testGetedit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
