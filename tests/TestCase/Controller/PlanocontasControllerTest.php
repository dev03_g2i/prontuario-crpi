<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PlanocontasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PlanocontasController Test Case
 */
class PlanocontasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.planocontas',
        'app.contasreceber',
        'app.classificacaocontas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fill method
     *
     * @return void
     */
    public function testFill()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getregistro method
     *
     * @return void
     */
    public function testGetregistro()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getlist method
     *
     * @return void
     */
    public function testGetlist()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
