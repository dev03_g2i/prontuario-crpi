<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ApurarController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ApurarController Test Case
 */
class ApurarControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.apurar',
        'app.users',
        'app.grupo_usuarios',
        'app.situacao_cadastros',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.users_up'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fill method
     *
     * @return void
     */
    public function testFill()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getedit method
     *
     * @return void
     */
    public function testGetedit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
