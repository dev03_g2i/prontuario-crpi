<?php
namespace App\Test\TestCase\Controller;

use App\Controller\AtendimentoProcedimentoEquipesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\AtendimentoProcedimentoEquipesController Test Case
 */
class AtendimentoProcedimentoEquipesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.atendimento_procedimento_equipes',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.medico_responsaveis',
        'app.atendimentos',
        'app.clientes',
        'app.convenios',
        'app.tipo_convenios',
        'app.preco_procedimentos',
        'app.user_reg',
        'app.fatura_encerramentos',
        'app.unidades',
        'app.situacao_faturaencerramentos',
        'app.temp_faturamentos',
        'app.user_alt',
        'app.convenio_regracalculo',
        'app.agendas',
        'app.tipo_agendas',
        'app.configuracao_periodos',
        'app.situacao_agendas',
        'app.user_edit',
        'app.agenda_bloqueios',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.fatura_recalcular',
        'app.estado_civis',
        'app.indicacao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.tipo_atendimentos',
        'app.solicitantes',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.planocontas',
        'app.contasreceber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.correcaomonetaria',
        'app.tipo_documento',
        'app.tipo_pagamento',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.programacao',
        'app.deducoes',
        'app.contaspagar',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.controle_financeiro',
        'app.classificacaocontas',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.agenda_periodos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.situacao_laudos',
        'app.situacaos',
        'app.tipo_equipes',
        'app.tipo_equipegrau',
        'app.configuracao_apuracao'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fill method
     *
     * @return void
     */
    public function testFill()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getedit method
     *
     * @return void
     */
    public function testGetedit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
