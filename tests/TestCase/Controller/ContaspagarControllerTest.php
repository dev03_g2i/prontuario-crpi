<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ContaspagarController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ContaspagarController Test Case
 */
class ContaspagarControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contaspagar',
        'app.planocontas',
        'app.classificacaocontas',
        'app.contaspagar_planejamentos',
        'app.clientes',
        'app.medico_responsaveis',
        'app.situacao_cadastros',
        'app.users',
        'app.grupo_usuarios',
        'app.grupo_agendas',
        'app.agenda_periodos',
        'app.tipo_agendas',
        'app.agenda_bloqueios',
        'app.agendas',
        'app.situacao_agendas',
        'app.convenios',
        'app.tipo_convenios',
        'app.atendimentos',
        'app.configuracao_periodos',
        'app.unidades',
        'app.fatura_encerramentos',
        'app.user_edit',
        'app.situacao_faturaencerramentos',
        'app.atendimento_procedimentos',
        'app.procedimentos',
        'app.grupo_procedimentos',
        'app.iten_cobrancas',
        'app.dentes',
        'app.faces',
        'app.regioes',
        'app.preco_procedimentos',
        'app.user_reg',
        'app.user_alt',
        'app.convenio_regracalculo',
        'app.solicitantes',
        'app.solicitante_cbo',
        'app.situacao_faturas',
        'app.situacao_recebimentos',
        'app.atendimento_itens',
        'app.face_itens',
        'app.financeiro',
        'app.cliente_historicos',
        'app.tipo_historias',
        'app.tipo_historia_modelos',
        'app.mastologias',
        'app.executor',
        'app.user__financeiro',
        'app.user__executor',
        'app.laudos',
        'app.users_assinado',
        'app.atendimento_procedimento_equipes',
        'app.tipo_equipes',
        'app.tipo_equipegrau',
        'app.situacaos',
        'app.configuracao_apuracao',
        'app.controle_financeiro',
        'app.situacao_laudos',
        'app.temp_faturamentos',
        'app.fatura_nfs',
        'app.tipo_atendimentos',
        'app.origens',
        'app.users_up',
        'app.contas_receber',
        'app.cliente_clone',
        'app.status',
        'app.anexo_financeiro',
        'app.contasreceber',
        'app.contabilidade',
        'app.banco',
        'app.contabilidade_banco',
        'app.movimento',
        'app.movimento_banco',
        'app.tipo_pagamento',
        'app.tipo_documento',
        'app.correcaomonetaria',
        'app.programacao',
        'app.deducoes',
        'app.deducoes_contaspagar',
        'app.deducoes_contasreceber',
        'app.deducoes_programacao',
        'app.fatura_matmed',
        'app.estq_artigos',
        'app.fatura_precoartigo',
        'app.fatura_kitartigo',
        'app.fatura_kit',
        'app.internacao_acomodacao',
        'app.internacao_carater_atendimento',
        'app.internacao_motivo_alta',
        'app.internacao_setor',
        'app.internacao_leito',
        'app.fatura_recalcular',
        'app.agenda_procedimentos',
        'app.agenda_cadprocedimentos',
        'app.grupo_agenda_horarios',
        'app.operacao_agenda_horarios',
        'app.estado_civis',
        'app.indicacao',
        'app.religiao',
        'app.indicacao_itens',
        'app.cliente_anexos',
        'app.tipo_anexos',
        'app.lista_anexos',
        'app.cliente_documentos',
        'app.tipo_documentos',
        'app.modelos',
        'app.lista_documentos',
        'app.cliente_responsaveis',
        'app.grau_parentescos',
        'app.fornecedors',
        'app.contabilidades',
        'app.contabilidade_bancos',
        'app.bancos',
        'app.banco_movimentos',
        'app.movimentos',
        'app.contaspagars',
        'app.contasreceber_planejamentos',
        'app.grupo_contabilidades',
        'app.grupos',
        'app.fornecedores',
        'app.abatimentos',
        'app.abatimentos_contaspagar'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test fill method
     *
     * @return void
     */
    public function testFill()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getedit method
     *
     * @return void
     */
    public function testGetedit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
