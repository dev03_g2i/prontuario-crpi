<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LaudosConfiguracoesFixture
 *
 */
class LaudosConfiguracoesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'laudar_qtd_imagens' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'laudar_qtd_filmes' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'laudar_qtd_papeis' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'laudar_cabecalho' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'laudar_numeracao_paginas' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'laudar_qtd_imagens' => 1,
            'laudar_qtd_filmes' => 1,
            'laudar_qtd_papeis' => 1,
            'laudar_cabecalho' => 1,
            'laudar_numeracao_paginas' => 1
        ],
    ];
}
