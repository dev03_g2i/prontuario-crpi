<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PrecoProcedimentosFixture
 *
 */
class PrecoProcedimentosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'convenio_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'procedimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'valor_faturar' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => '0.00', 'comment' => ''],
        'valor_particular' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => '0.00', 'comment' => ''],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'situacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'regra' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '1-Normal / 2-Quantidade', 'precision' => null, 'autoIncrement' => null],
        'codigo' => ['type' => 'string', 'length' => 15, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'id_user' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'id_situacao' => ['type' => 'index', 'columns' => ['situacao_id'], 'length' => []],
            'id_convenio' => ['type' => 'index', 'columns' => ['convenio_id'], 'length' => []],
            'id_procedimento' => ['type' => 'index', 'columns' => ['procedimento_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'convenio_id' => ['type' => 'unique', 'columns' => ['convenio_id', 'procedimento_id'], 'length' => []],
            'preco_procedimentos_ibfk_1' => ['type' => 'foreign', 'columns' => ['convenio_id'], 'references' => ['convenios', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'preco_procedimentos_ibfk_2' => ['type' => 'foreign', 'columns' => ['procedimento_id'], 'references' => ['procedimentos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'preco_procedimentos_ibfk_3' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'preco_procedimentos_ibfk_4' => ['type' => 'foreign', 'columns' => ['situacao_id'], 'references' => ['situacao_cadastros', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'convenio_id' => 1,
            'procedimento_id' => 1,
            'valor_faturar' => 1.5,
            'valor_particular' => 1.5,
            'user_id' => 1,
            'created' => '2017-05-29 16:00:48',
            'modified' => '2017-05-29 16:00:48',
            'situacao_id' => 1,
            'regra' => 1,
            'codigo' => 'Lorem ipsum d'
        ],
    ];
}
