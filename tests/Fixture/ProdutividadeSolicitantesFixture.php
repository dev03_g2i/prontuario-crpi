<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ProdutividadeSolicitantesFixture
 *
 */
class ProdutividadeSolicitantesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'convenio_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'solicitante_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'percentual_desconto' => ['type' => 'decimal', 'length' => 10, 'precision' => 0, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'percentual_repasse' => ['type' => 'decimal', 'length' => 10, 'precision' => 0, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'solicitante_id' => ['type' => 'index', 'columns' => ['solicitante_id'], 'length' => []],
            'convenio_id' => ['type' => 'index', 'columns' => ['convenio_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'produtividade_solicitantes_ibfk_1' => ['type' => 'foreign', 'columns' => ['solicitante_id'], 'references' => ['solicitantes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'produtividade_solicitantes_ibfk_2' => ['type' => 'foreign', 'columns' => ['convenio_id'], 'references' => ['convenios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'convenio_id' => 1,
            'solicitante_id' => 1,
            'percentual_desconto' => 1.5,
            'percentual_repasse' => 1.5,
            'created' => '2017-11-10 14:59:15',
            'modified' => '2017-11-10 14:59:15',
            'user_id' => 1
        ],
    ];
}
