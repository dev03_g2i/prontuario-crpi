<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NfsFixture
 *
 */
class NfsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'prestador_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tomador_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ficha' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'situacao_nota_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'situacao_rps' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '78', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'descricao_rps' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'assinatura' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'autorizacao' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'autorizacao_cancelamento' => ['type' => 'string', 'length' => 500, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numero_lote' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'numero_rps' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'numero_nfse' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_emissao_rps' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => 'CURRENT_TIMESTAMP', 'comment' => '', 'precision' => null],
        'data_processamento' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'data_cancelamento' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'mot_cancelamento' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'serie_rps' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => 'NF', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'serie_rps_substituido' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numero_rps_substituido' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'numero_nfse_substituido' => ['type' => 'biginteger', 'length' => 20, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_emissao_nfse_substituida' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'serie_prestacao' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => '99', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cnae' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'descricao_cnae' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'tipo_recolhimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '65', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cidade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'operacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '65', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tributacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '84', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'aliquota_atividade' => ['type' => 'decimal', 'length' => 11, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '5.0000', 'comment' => ''],
        'aliquota_pis' => ['type' => 'decimal', 'length' => 11, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'aliquota_cofins' => ['type' => 'decimal', 'length' => 11, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'aliquota_inss' => ['type' => 'decimal', 'length' => 11, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'aliquota_ir' => ['type' => 'decimal', 'length' => 11, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'aliquota_csll' => ['type' => 'decimal', 'length' => 11, 'precision' => 4, 'unsigned' => false, 'null' => true, 'default' => '0.0000', 'comment' => ''],
        'valor_pis' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'valor_cofins' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'valor_inss' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'valor_ir' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'valor_csll' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'valor_total_servicos' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'valor_total_deducoes' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'enviar_email' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'nfse_arquivo' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'situacao_nota_id' => ['type' => 'index', 'columns' => ['situacao_nota_id'], 'length' => []],
            'prestador_id' => ['type' => 'index', 'columns' => ['prestador_id'], 'length' => []],
            'tomador_id' => ['type' => 'index', 'columns' => ['tomador_id'], 'length' => []],
            'tipo_recolhimento_id' => ['type' => 'index', 'columns' => ['tipo_recolhimento_id'], 'length' => []],
            'operacao_id' => ['type' => 'index', 'columns' => ['operacao_id'], 'length' => []],
            'tributacao_id' => ['type' => 'index', 'columns' => ['tributacao_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'nota_fiscais_ibfk_1' => ['type' => 'foreign', 'columns' => ['situacao_nota_id'], 'references' => ['nf_situacao_notas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'nota_fiscais_ibfk_2' => ['type' => 'foreign', 'columns' => ['prestador_id'], 'references' => ['nf_prestadores', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'nota_fiscais_ibfk_3' => ['type' => 'foreign', 'columns' => ['tomador_id'], 'references' => ['nf_tomadores', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'nota_fiscais_ibfk_5' => ['type' => 'foreign', 'columns' => ['tipo_recolhimento_id'], 'references' => ['nf_tipo_recolhimentos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'nota_fiscais_ibfk_6' => ['type' => 'foreign', 'columns' => ['operacao_id'], 'references' => ['nf_operacoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'nota_fiscais_ibfk_7' => ['type' => 'foreign', 'columns' => ['tributacao_id'], 'references' => ['nf_tributacoes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'prestador_id' => 1,
            'tomador_id' => 1,
            'ficha' => 1,
            'situacao_nota_id' => 1,
            'situacao_rps' => 1,
            'descricao_rps' => 'Lorem ipsum dolor sit amet',
            'assinatura' => 'Lorem ipsum dolor sit amet',
            'autorizacao' => 'Lorem ipsum dolor sit amet',
            'autorizacao_cancelamento' => 'Lorem ipsum dolor sit amet',
            'numero_lote' => 1,
            'numero_rps' => 1,
            'numero_nfse' => 1,
            'data_emissao_rps' => '2018-05-21 16:19:19',
            'data_processamento' => '2018-05-21 16:19:19',
            'data_cancelamento' => '2018-05-21 16:19:19',
            'mot_cancelamento' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'serie_rps' => 'Lorem ipsum dolor sit amet',
            'serie_rps_substituido' => 'Lorem ipsum dolor sit amet',
            'numero_rps_substituido' => 1,
            'numero_nfse_substituido' => 1,
            'data_emissao_nfse_substituida' => '2018-05-21 16:19:19',
            'serie_prestacao' => 'Lorem ipsum dolor ',
            'cnae' => 'Lorem ipsum dolor sit amet',
            'descricao_cnae' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'tipo_recolhimento_id' => 1,
            'cidade_id' => 1,
            'operacao_id' => 1,
            'tributacao_id' => 1,
            'aliquota_atividade' => 1.5,
            'aliquota_pis' => 1.5,
            'aliquota_cofins' => 1.5,
            'aliquota_inss' => 1.5,
            'aliquota_ir' => 1.5,
            'aliquota_csll' => 1.5,
            'valor_pis' => 1.5,
            'valor_cofins' => 1.5,
            'valor_inss' => 1.5,
            'valor_ir' => 1.5,
            'valor_csll' => 1.5,
            'valor_total_servicos' => 1.5,
            'valor_total_deducoes' => 1.5,
            'enviar_email' => 1,
            'nfse_arquivo' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
