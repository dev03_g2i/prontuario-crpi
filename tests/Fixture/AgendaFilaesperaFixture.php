<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AgendaFilaesperaFixture
 *
 */
class AgendaFilaesperaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'agenda_filaespera';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'paciente' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'fone' => ['type' => 'string', 'length' => 14, 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'convenio_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_espera_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'grupo_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'situacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dt_agendado' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'convenio_id' => ['type' => 'index', 'columns' => ['convenio_id'], 'length' => []],
            'tipo_espera_id' => ['type' => 'index', 'columns' => ['tipo_espera_id'], 'length' => []],
            'grupo_id' => ['type' => 'index', 'columns' => ['grupo_id'], 'length' => []],
            'situacao_id' => ['type' => 'index', 'columns' => ['situacao_id'], 'length' => []],
            'user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'agenda_filaespera_ibfk_1' => ['type' => 'foreign', 'columns' => ['convenio_id'], 'references' => ['convenios', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'agenda_filaespera_ibfk_2' => ['type' => 'foreign', 'columns' => ['tipo_espera_id'], 'references' => ['tipo_esperaagenda', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'agenda_filaespera_ibfk_3' => ['type' => 'foreign', 'columns' => ['grupo_id'], 'references' => ['grupo_agendas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'agenda_filaespera_ibfk_4' => ['type' => 'foreign', 'columns' => ['situacao_id'], 'references' => ['situacao_cadastros', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'agenda_filaespera_ibfk_5' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'paciente' => 'Lorem ipsum dolor sit amet',
            'fone' => 'Lorem ipsum ',
            'convenio_id' => 1,
            'tipo_espera_id' => 1,
            'grupo_id' => 1,
            'situacao_id' => 1,
            'user_id' => 1,
            'dt_agendado' => '2018-05-16 16:27:08',
            'created' => '2018-05-16 16:27:08',
            'modified' => '2018-05-16 16:27:08'
        ],
    ];
}
