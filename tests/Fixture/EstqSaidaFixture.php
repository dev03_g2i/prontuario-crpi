<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EstqSaidaFixture
 *
 */
class EstqSaidaFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'estq_saida';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'data' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'artigo_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quantidade' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vl_custos' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'vl_venda' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'vl_custo_medico' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'origin' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'codigo_tiss' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'codigo_tuss' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'codigo_convenio' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'tipo_movimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'atendimento_procedimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'situacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'atendimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'total' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'qtd_recebida' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vl_recebido' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'dt_recebimento' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'exportado_fatura' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '1 - exportado | 0 - não', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'atendimento_procedimento_id' => ['type' => 'index', 'columns' => ['atendimento_procedimento_id'], 'length' => []],
            'user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
            'situacao_id' => ['type' => 'index', 'columns' => ['situacao_id'], 'length' => []],
            'atendimento_id' => ['type' => 'index', 'columns' => ['atendimento_id'], 'length' => []],
            'tipo_movimento_id' => ['type' => 'index', 'columns' => ['tipo_movimento_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'estq_saida_ibfk_1' => ['type' => 'foreign', 'columns' => ['atendimento_procedimento_id'], 'references' => ['atendimento_procedimentos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'estq_saida_ibfk_2' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'estq_saida_ibfk_3' => ['type' => 'foreign', 'columns' => ['situacao_id'], 'references' => ['situacao_cadastros', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'estq_saida_ibfk_4' => ['type' => 'foreign', 'columns' => ['atendimento_id'], 'references' => ['atendimentos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'estq_saida_ibfk_5' => ['type' => 'foreign', 'columns' => ['tipo_movimento_id'], 'references' => ['estq_tipo_movimento', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'data' => '2018-06-18',
            'artigo_id' => 1,
            'quantidade' => 1,
            'vl_custos' => 1.5,
            'vl_venda' => 1.5,
            'vl_custo_medico' => 1.5,
            'origin' => 1,
            'codigo_tiss' => 'Lorem ip',
            'codigo_tuss' => 'Lorem ip',
            'codigo_convenio' => 'Lorem ip',
            'tipo_movimento_id' => 1,
            'atendimento_procedimento_id' => 1,
            'user_id' => 1,
            'situacao_id' => 1,
            'created' => '2018-06-18 13:39:02',
            'modified' => '2018-06-18 13:39:02',
            'atendimento_id' => 1,
            'total' => 1.5,
            'qtd_recebida' => 1,
            'vl_recebido' => 1.5,
            'dt_recebimento' => '2018-06-18',
            'exportado_fatura' => 1
        ],
    ];
}
