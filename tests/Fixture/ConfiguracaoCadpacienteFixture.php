<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConfiguracaoCadpacienteFixture
 *
 */
class ConfiguracaoCadpacienteFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'configuracao_cadpaciente';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'religiao' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '1=nao mostra na tela de cadastro e nao obrigatorio, 2, mostra e nao obrig, 3-mostra e obrigatório - Padrão  = 1', 'precision' => null, 'autoIncrement' => null],
        'indicao' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '1=nao mostra e nao obrigatório, 2, mostra e nao obrig, 3-mostra e obrigatorio  - Padrão = 3', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'religiao' => 1,
            'indicao' => 1
        ],
    ];
}
