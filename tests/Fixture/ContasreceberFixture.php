<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContasreceberFixture
 *
 */
class ContasreceberFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'contasreceber';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'data' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'valor' => ['type' => 'decimal', 'length' => 15, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'planoconta_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'cliente_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'status' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'complemento' => ['type' => 'text', 'length' => 4294967295, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'data_pagamento' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'contabilidade_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_vencimento' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'documento' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'numdoc' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'parcela' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'forma_pagamento' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'boleto_mensagem' => ['type' => 'integer', 'length' => 200, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'boleto_mensagem_livre' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'boleto_barras' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'boletolinhadigitavel' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'boleto_nosso_numero' => ['type' => 'string', 'length' => 30, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'juros' => ['type' => 'float', 'length' => 15, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'multa' => ['type' => 'float', 'length' => 15, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => '0.00', 'comment' => ''],
        'boleto_cedente' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'numero_fiscal' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'data_fiscal' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'valor_bruto' => ['type' => 'decimal', 'length' => 15, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'desconto' => ['type' => 'decimal', 'length' => 15, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => '0.00', 'comment' => ''],
        'correcao_monetaria' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_pagamento' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_documento' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'numero_documento' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'planejamento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'impresso' => ['type' => 'tinyinteger', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
        'valorPagar' => ['type' => 'decimal', 'length' => 15, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'vencimento_boleto' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'perjuros' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'permulta' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'data_cadastro' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'sis_antigo_cx_boleto' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'sis_antigo_cx' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'extern_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_created' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_modified' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'controle_financeiro_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '-1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'origem_registro' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '1 = Origem Ficha , 2 = Origem encerramento faturamento', 'precision' => null, 'autoIncrement' => null],
        'fatura_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Campo para guardar o id da fatura gerado na rotina do prontuário', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'status' => ['type' => 'index', 'columns' => ['status'], 'length' => []],
            'contabilidade' => ['type' => 'index', 'columns' => ['contabilidade_id'], 'length' => []],
            'correcaomonetaria' => ['type' => 'index', 'columns' => ['correcao_monetaria'], 'length' => []],
            'idcliente' => ['type' => 'index', 'columns' => ['cliente_id'], 'length' => []],
            'idplanocontas' => ['type' => 'index', 'columns' => ['planoconta_id'], 'length' => []],
            'tipodocumento' => ['type' => 'index', 'columns' => ['tipo_documento'], 'length' => []],
            'tipopagamento' => ['type' => 'index', 'columns' => ['tipo_pagamento'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'contasreceber_ibfk_1' => ['type' => 'foreign', 'columns' => ['planoconta_id'], 'references' => ['planocontas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'contasreceber_ibfk_3' => ['type' => 'foreign', 'columns' => ['contabilidade_id'], 'references' => ['contabilidades', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'contasreceber_ibfk_4' => ['type' => 'foreign', 'columns' => ['cliente_id'], 'references' => ['cliente_clone', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'contasreceber_ibfk_5' => ['type' => 'foreign', 'columns' => ['correcao_monetaria'], 'references' => ['correcaomonetaria', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'contasreceber_ibfk_6' => ['type' => 'foreign', 'columns' => ['tipo_pagamento'], 'references' => ['tipo_pagamento', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'contasreceber_ibfk_7' => ['type' => 'foreign', 'columns' => ['tipo_documento'], 'references' => ['tipo_documento', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'data' => '2018-05-08',
            'valor' => 1.5,
            'planoconta_id' => 1,
            'cliente_id' => 1,
            'status' => 1,
            'complemento' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'data_pagamento' => '2018-05-08',
            'contabilidade_id' => 1,
            'data_vencimento' => '2018-05-08',
            'documento' => 'Lorem ipsum dolor ',
            'numdoc' => 'Lorem ipsum dolor ',
            'parcela' => 'Lorem ipsum dolor ',
            'forma_pagamento' => 1,
            'boleto_mensagem' => 1,
            'boleto_mensagem_livre' => 'Lorem ipsum dolor sit amet',
            'boleto_barras' => 'Lorem ipsum dolor sit amet',
            'boletolinhadigitavel' => 'Lorem ipsum dolor sit amet',
            'boleto_nosso_numero' => 'Lorem ipsum dolor sit amet',
            'juros' => 1,
            'multa' => 1,
            'boleto_cedente' => 1,
            'numero_fiscal' => 'Lorem ipsum dolor sit amet',
            'data_fiscal' => '2018-05-08',
            'valor_bruto' => 1.5,
            'desconto' => 1.5,
            'correcao_monetaria' => 1,
            'tipo_pagamento' => 1,
            'tipo_documento' => 1,
            'numero_documento' => 'Lorem ipsum dolor sit amet',
            'planejamento_id' => 1,
            'impresso' => 1,
            'valorPagar' => 1.5,
            'vencimento_boleto' => '2018-05-08',
            'perjuros' => 1,
            'permulta' => 1,
            'data_cadastro' => '2018-05-08 09:37:43',
            'sis_antigo_cx_boleto' => 1,
            'sis_antigo_cx' => 1,
            'extern_id' => 1,
            'user_created' => 1,
            'user_modified' => 1,
            'created' => '2018-05-08 09:37:43',
            'modified' => '2018-05-08 09:37:43',
            'controle_financeiro_id' => 1,
            'origem_registro' => 1,
            'fatura_id' => 1
        ],
    ];
}
