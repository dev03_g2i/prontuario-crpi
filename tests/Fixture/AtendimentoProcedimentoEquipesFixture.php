<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AtendimentoProcedimentoEquipesFixture
 *
 */
class AtendimentoProcedimentoEquipesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ordem' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'atendimento_procedimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'profissional_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_equipe_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_equipegrau_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'porcentagem' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'configuracao_apuracao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'obs' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '', 'precision' => null],
        'user_insert' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_update' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'situacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'profissional_id' => ['type' => 'index', 'columns' => ['profissional_id'], 'length' => []],
            'tipo_equipe_id' => ['type' => 'index', 'columns' => ['tipo_equipe_id'], 'length' => []],
            'tipo_equipegrau_id' => ['type' => 'index', 'columns' => ['tipo_equipegrau_id'], 'length' => []],
            'configuracao_apuracao_id' => ['type' => 'index', 'columns' => ['configuracao_apuracao_id'], 'length' => []],
            'user_insert' => ['type' => 'index', 'columns' => ['user_insert'], 'length' => []],
            'situacao_id' => ['type' => 'index', 'columns' => ['situacao_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'atendimento_procedimento_equipes_ibfk_1' => ['type' => 'foreign', 'columns' => ['profissional_id'], 'references' => ['medico_responsaveis', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimento_procedimento_equipes_ibfk_2' => ['type' => 'foreign', 'columns' => ['tipo_equipe_id'], 'references' => ['tipo_equipes', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimento_procedimento_equipes_ibfk_3' => ['type' => 'foreign', 'columns' => ['tipo_equipegrau_id'], 'references' => ['tipo_equipegrau', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimento_procedimento_equipes_ibfk_4' => ['type' => 'foreign', 'columns' => ['configuracao_apuracao_id'], 'references' => ['configuracao_apuracao', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimento_procedimento_equipes_ibfk_5' => ['type' => 'foreign', 'columns' => ['user_insert'], 'references' => ['users', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'atendimento_procedimento_equipes_ibfk_6' => ['type' => 'foreign', 'columns' => ['situacao_id'], 'references' => ['situacao_cadastros', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ordem' => 1,
            'atendimento_procedimento_id' => 1,
            'profissional_id' => 1,
            'tipo_equipe_id' => 1,
            'tipo_equipegrau_id' => 1,
            'porcentagem' => 1,
            'configuracao_apuracao_id' => 1,
            'obs' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
            'user_insert' => 1,
            'user_update' => 1,
            'situacao_id' => 1,
            'created' => '2018-03-15 15:35:07',
            'modified' => '2018-03-15 15:35:07'
        ],
    ];
}
