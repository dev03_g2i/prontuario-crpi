<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AbatimentosContaspagarFixture
 *
 */
class AbatimentosContaspagarFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'abatimentos_contaspagar';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'abatimento_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'contaspagar_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'valor' => ['type' => 'decimal', 'length' => 11, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'deducoes_id' => ['type' => 'index', 'columns' => ['abatimento_id'], 'length' => []],
            'contaspagar_id' => ['type' => 'index', 'columns' => ['contaspagar_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'deducoesContaspagar_ibfk_1' => ['type' => 'foreign', 'columns' => ['abatimento_id'], 'references' => ['abatimentos', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'deducoesContaspagar_ibfk_2' => ['type' => 'foreign', 'columns' => ['contaspagar_id'], 'references' => ['contaspagar', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'abatimento_id' => 1,
            'contaspagar_id' => 1,
            'valor' => 1.5
        ],
    ];
}
