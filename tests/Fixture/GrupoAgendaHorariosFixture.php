<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GrupoAgendaHorariosFixture
 *
 */
class GrupoAgendaHorariosFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'dia_semana' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '1-segunda/2-terça/...0-Domingo', 'precision' => null, 'autoIncrement' => null],
        'data_inicio' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'data_fim' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'hora_inicio' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'hora_fim' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'intervalo' => ['type' => 'time', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'grupo_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tipo_agenda_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'situacao_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'grupo_id' => ['type' => 'index', 'columns' => ['grupo_id'], 'length' => []],
            'tipo_agenda_id' => ['type' => 'index', 'columns' => ['tipo_agenda_id'], 'length' => []],
            'situacao_id' => ['type' => 'index', 'columns' => ['situacao_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'grupo_agenda_horarios_ibfk_1' => ['type' => 'foreign', 'columns' => ['grupo_id'], 'references' => ['grupo_agendas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'grupo_agenda_horarios_ibfk_2' => ['type' => 'foreign', 'columns' => ['tipo_agenda_id'], 'references' => ['tipo_agendas', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'grupo_agenda_horarios_ibfk_3' => ['type' => 'foreign', 'columns' => ['situacao_id'], 'references' => ['situacao_cadastros', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_unicode_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'dia_semana' => 1,
            'data_inicio' => '2017-11-29',
            'data_fim' => '2017-11-29',
            'hora_inicio' => '08:40:10',
            'hora_fim' => '08:40:10',
            'intervalo' => '08:40:10',
            'grupo_id' => 1,
            'tipo_agenda_id' => 1,
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => '2017-11-29 08:40:10',
            'modified' => '2017-11-29 08:40:10'
        ],
    ];
}
