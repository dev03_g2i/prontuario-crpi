<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConfiguracoesFixture
 *
 */
class ConfiguracoesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'logo' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'logo_dir' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'nome_login' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'icone' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'icone_dir' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'imagem' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'url_imagens' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'usa_arquivo' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '1 = sim 2 = nao', 'precision' => null, 'autoIncrement' => null],
        'arquivo' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'restringe_cpf' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '1 - Sim / 2 - nao', 'precision' => null, 'autoIncrement' => null],
        'duplica_cpf' => ['type' => 'string', 'length' => 14, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'idade_cpf' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'paciente_matrvalidade' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '0 - Nao obrigatorio / 1 - Obrigatorio', 'precision' => null, 'autoIncrement' => null],
        'url_image_laudo' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'matmed_fatura' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => 'Lançar MatMed  - 0=Nao/1=Sim', 'precision' => null, 'autoIncrement' => null],
        'edit_desconto' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '1 - sim | 0 - não', 'precision' => null, 'autoIncrement' => null],
        'edit_valor_caixa' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '1 - Sim | 0 - Não', 'precision' => null, 'autoIncrement' => null],
        'orientacao_proc_agenda' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '1 - Sim / 0 - Não', 'precision' => null, 'autoIncrement' => null],
        'atendimento_dt_entrega' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '1 - Sim | 0 - Não', 'precision' => null, 'autoIncrement' => null],
        'utiliza_laudos' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '1 - Sim | 0 - Não', 'precision' => null, 'autoIncrement' => null],
        'usa_filme' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => 'PrecoProcedimento - 0=Nao/1=Sim', 'precision' => null, 'autoIncrement' => null],
        'usa_porte' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => 'PrecoProcedimento - 0=Nao/1=Sim', 'precision' => null, 'autoIncrement' => null],
        'usa_uco' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => 'PrecoProcedimento - 0=Nao/1=Sim', 'precision' => null, 'autoIncrement' => null],
        'indicacao_obrigatoria' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'caixa_idplano_contas' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '283', 'comment' => '283 - Receita Tratamento', 'precision' => null, 'autoIncrement' => null],
        'mostra_caixa_plano' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 -Sim | 1 - Não', 'precision' => null, 'autoIncrement' => null],
        'caixa_editdt_entrega' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'emissao_doc_ocultatitulo' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '0', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'endereco' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cnpj' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'telefone' => ['type' => 'string', 'length' => 25, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'celular' => ['type' => 'string', 'length' => 25, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'fax' => ['type' => 'string', 'length' => 25, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'cep' => ['type' => 'string', 'length' => 25, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'agenda_interface' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '3', 'comment' => '1 - Lista | 2 - Calendario | 3 - ambos', 'precision' => null, 'autoIncrement' => null],
        'agenda_interface_lista' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '1 - Usa geração | 2 - Não usa', 'precision' => null, 'autoIncrement' => null],
        'mostra_solicitante_procedimentos' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '1 - Sim | 2 - Não', 'precision' => null, 'autoIncrement' => null],
        'imprimir_ficha_semreceber' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '0 - Não libera | 1 - libera imprimir ficha', 'precision' => null, 'autoIncrement' => null],
        'usa_filme_reais' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => '1', 'comment' => '0 - Não | 1 - Sim', 'precision' => null, 'autoIncrement' => null],
        'tiss_cnes' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => '', 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'controle_finsetor_atendproc' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'controle_finsetor_atendproc_padrao' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '-1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'controle_financsetor_receita' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'controle_financsetor_receita_padrao' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => '-1', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'logo' => 'Lorem ipsum dolor sit amet',
            'logo_dir' => 'Lorem ipsum dolor sit amet',
            'nome_login' => 'Lorem ipsum dolor sit amet',
            'icone' => 'Lorem ipsum dolor sit amet',
            'icone_dir' => 'Lorem ipsum dolor sit amet',
            'created' => '2018-02-26 14:14:53',
            'modified' => '2018-02-26 14:14:53',
            'imagem' => 1,
            'url_imagens' => 'Lorem ipsum dolor sit amet',
            'usa_arquivo' => 1,
            'arquivo' => 1,
            'restringe_cpf' => 1,
            'duplica_cpf' => 'Lorem ipsum ',
            'idade_cpf' => 1,
            'paciente_matrvalidade' => 1,
            'url_image_laudo' => 'Lorem ipsum dolor sit amet',
            'matmed_fatura' => 1,
            'edit_desconto' => 1,
            'edit_valor_caixa' => 1,
            'orientacao_proc_agenda' => 1,
            'atendimento_dt_entrega' => 1,
            'utiliza_laudos' => 1,
            'usa_filme' => 1,
            'usa_porte' => 1,
            'usa_uco' => 1,
            'indicacao_obrigatoria' => 1,
            'caixa_idplano_contas' => 1,
            'mostra_caixa_plano' => 1,
            'caixa_editdt_entrega' => 1,
            'emissao_doc_ocultatitulo' => 1,
            'endereco' => 'Lorem ipsum dolor sit amet',
            'cnpj' => 'Lorem ipsum dolor sit amet',
            'telefone' => 'Lorem ipsum dolor sit a',
            'celular' => 'Lorem ipsum dolor sit a',
            'fax' => 'Lorem ipsum dolor sit a',
            'cep' => 'Lorem ipsum dolor sit a',
            'agenda_interface' => 1,
            'agenda_interface_lista' => 1,
            'mostra_solicitante_procedimentos' => 1,
            'imprimir_ficha_semreceber' => 1,
            'usa_filme_reais' => 1,
            'tiss_cnes' => 'Lorem ipsum dolor sit amet',
            'controle_finsetor_atendproc' => 1,
            'controle_finsetor_atendproc_padrao' => 1,
            'controle_financsetor_receita' => 1,
            'controle_financsetor_receita_padrao' => 1
        ],
    ];
}
