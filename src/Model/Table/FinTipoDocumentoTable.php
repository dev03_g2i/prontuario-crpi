<?php

namespace App\Model\Table;

use App\Model\Entity\FinTipoDocumento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinTipoDocumento Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinStatus
 * @property \Cake\ORM\Association\HasMany $FinContasPagar
 */
class FinTipoDocumentoTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_tipo_documento');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->belongsTo('FinStatus', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('FinContasPagar', [
            'foreignKey' => 'fin_tipo_documento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 255)
            ->allowEmpty('descricao');

        $validator
            ->scalar('usado')
            ->allowEmpty('usado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['status_id'], 'FinStatus'));
        return $rules;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'FinTipoDocumento') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        $queryData->andWhere(['FinTipoDocumento.status_id' => 1]);
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if ($entity->isNew()) {
        }

        return true;
    }
}
