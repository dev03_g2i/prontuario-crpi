<?php
namespace App\Model\Table;

use App\Model\Entity\ConvenioImpostosParametro;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\ORM\TableRegistry;

/**
 * ConvenioImpostosParametro Model
 *

 */
class ConvenioImpostosParametroTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('convenio_impostos_parametro');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        
        $this->belongsTo('ConvenioTipoimposto', [
            'foreignKey' => 'id_tipo_imposto',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('id_convenio')
            ->requirePresence('id_convenio', 'create')
            ->notEmpty('id_convenio');

        $validator
            ->integer('id_tipo_imposto')
            ->requirePresence('id_tipo_imposto', 'create')
            ->notEmpty('id_tipo_imposto');

        $validator
            ->decimal('porcentual')
            ->requirePresence('porcentual', 'create')
            ->notEmpty('porcentual');

        $validator
            ->integer('retido')
            ->requirePresence('retido', 'create')
            ->notEmpty('retido');

        $validator
            ->scalar('observacao')
            ->allowEmpty('observacao');

        $validator
            ->integer('user_created')
            ->requirePresence('user_created', 'create')
            ->notEmpty('user_created');

        $validator
            ->integer('user_updated')
            ->allowEmpty('user_updated');

        $validator
            ->integer('convenio_id')
            ->requirePresence('convenio_id', 'create')
            ->notEmpty('convenio_id');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='ConvenioImpostosParametro') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()){}
        
        return true;
    }

    public function afterSave(Event $event)
    {
        $entity = $event->data['entity'];
        $Convenios = TableRegistry::get('Convenios');
        $convenioImpostosParametros = $this->find()
        ->select(['total_porcentual' => 'SUM(ConvenioImpostosParametro.porcentual)'])
        ->where(['ConvenioImpostosParametro.convenio_id' => $entity->convenio_id, 'ConvenioImpostosParametro.situacao_id' => 1])->first();

        $newConvenio['total_imposto'] = $convenioImpostosParametros->total_porcentual;
        $convenio = $Convenios->get($entity->convenio_id);

        $convenio = $this->Convenios->patchEntity($convenio, $newConvenio);
        
        $this->Convenios->save($convenio);
        
        return true;
    }

    public function afterDelete(Event $event)
    {
        $entity = $event->data['entity'];
        $Convenios = TableRegistry::get('Convenios');
        $convenioImpostosParametros = $this->find()
        ->select(['total_porcentual' => 'SUM(ConvenioImpostosParametro.porcentual)'])
        ->where(['ConvenioImpostosParametro.convenio_id' => $entity->convenio_id, 'ConvenioImpostosParametro.situacao_id' => 1])->first();

        $newConvenio['total_imposto'] = $convenioImpostosParametros->total_porcentual;
        $convenio = $Convenios->get($entity->convenio_id);

        $convenio = $this->Convenios->patchEntity($convenio, $newConvenio);
        
        $this->Convenios->save($convenio);

        return true;
    }
}
