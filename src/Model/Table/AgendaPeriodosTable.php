<?php
namespace App\Model\Table;

use App\Model\Entity\AgendaPeriodo;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * AgendaPeriodos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $GrupoAgendas
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users

 */
class AgendaPeriodosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('agenda_periodos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('GrupoAgendas', [
            'foreignKey' => 'grupo_agenda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoAgendas', [
            'foreignKey' => 'tipo_agenda_id'            
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('dia_semana')
            ->notEmpty('dia_semana');

        $validator
            ->integer('periodo')
            ->notEmpty('periodo');

        $validator
            ->allowEmpty('intervalo');

        $validator
            ->time('inicio')
            ->notEmpty('inicio');

        $validator
            ->time('fim')
            ->notEmpty('fim');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['grupo_agenda_id'], 'GrupoAgendas'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='AgendaPeriodos') {
              $queryData->where(['AgendaPeriodos.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
                 if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
         }
        
        return true;
    }

    public function checkPeriodos($dia_semana, $periodo, $grupo_agenda_id, $tipo_agenda_id, $inicio, $id = null)
    {
        $query = $this->find('all')
                ->where(['AgendaPeriodos.situacao_id' => 1])
                ->andWhere(['AgendaPeriodos.dia_semana' => $dia_semana])
                ->andWhere(['AgendaPeriodos.periodo' => $periodo])
                ->andWhere(['AgendaPeriodos.grupo_agenda_id' => $grupo_agenda_id])
                
                ->andWhere(['AgendaPeriodos.inicio' => $inicio])
                ->limit(1);

        if(!empty($tipo_agenda_id)) {
            $query->andWhere(['AgendaPeriodos.tipo_agenda_id' => $tipo_agenda_id]);
        }

        if(!empty($id)) {
            $query->andWhere(['AgendaPeriodos.id <> ' => $id]);
        }

        return $query->count();
    }

}
