<?php
namespace App\Model\Table;

use App\Model\Entity\GrupoAgendaHorario;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * GrupoAgendaHorarios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $GrupoAgendas
 * @property \Cake\ORM\Association\BelongsTo $TipoAgendas
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users

 */
class GrupoAgendaHorariosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('grupo_agenda_horarios');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('GrupoAgendas', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoAgendas', [
            'foreignKey' => 'tipo_agenda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Agendas', [
            'foreignKey' => 'agenda_horario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('OperacaoAgendaHorarios', [
            'foreignKey' => 'operacao_agenda_horario_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('dia_semana')
            ->requirePresence('dia_semana', 'create')
            ->notEmpty('dia_semana');

        $validator
            ->requirePresence('data_inicio', 'create')
            ->notEmpty('data_inicio');

        $validator
            ->requirePresence('data_fim', 'create')
            ->notEmpty('data_fim');

        $validator
            ->time('hora_inicio')
            ->requirePresence('hora_inicio', 'create')
            ->notEmpty('hora_inicio');

        $validator
            ->time('hora_fim')
            ->requirePresence('hora_fim', 'create')
            ->notEmpty('hora_fim');

        $validator
            ->time('intervalo')
            ->requirePresence('intervalo', 'create')
            ->notEmpty('intervalo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['grupo_id'], 'GrupoAgendas'));
        $rules->add($rules->existsIn(['tipo_agenda_id'], 'TipoAgendas'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='GrupoAgendaHorarios') {
              $queryData->where(['GrupoAgendaHorarios.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
                 if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
         }
        
        if (!empty($entity->data_inicio)) {
         $entity->data_inicio = implode('-', array_reverse(explode('/', $entity->data_inicio)));
       }
        if (!empty($entity->data_fim)) {
         $entity->data_fim = implode('-', array_reverse(explode('/', $entity->data_fim)));
       }
        return true;
    }
}
