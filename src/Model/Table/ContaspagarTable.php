<?php
namespace App\Model\Table;

use App\Model\Entity\Contaspagar;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * Contaspagar Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Planocontas
 * @property \Cake\ORM\Association\BelongsTo $Fornecedores
 * @property \Cake\ORM\Association\BelongsTo $Contabilidades
 * @property \Cake\ORM\Association\BelongsTo $TipoPagamento
 * @property \Cake\ORM\Association\BelongsTo $TipoDocumento
 * @property \Cake\ORM\Association\BelongsTo $ContaspagarPlanejamentos
 * @property \Cake\ORM\Association\HasMany $Movimentos
 * @property \Cake\ORM\Association\BelongsToMany $AbatimentosContaspagar

 */
class ContaspagarTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contaspagar');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Planocontas', [
            'foreignKey' => 'planoconta_id'
        ]);
        $this->belongsTo('Fornecedores', [
            'foreignKey' => 'fornecedor_id'
        ]);
        $this->belongsTo('Contabilidades', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->belongsTo('TipoPagamento', [
            'foreignKey' => 'tipo_pagamento_id'
        ]);
        $this->belongsTo('TipoDocumento', [
            'foreignKey' => 'tipo_documento_id'
        ]);
        $this->belongsTo('ContaspagarPlanejamentos', [
            'foreignKey' => 'contaspagar_planejamento_id'
        ]);
        $this->hasMany('Movimentos', [
            'foreignKey' => 'contaspagar_id'
        ]);
        $this->hasMany('AbatimentosContaspagar', [
            'foreignKey' => 'contaspagar_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->requirePresence('data')
            ->notEmpty('data');

        $validator
            ->requirePresence('vencimento')
            ->notEmpty('vencimento');

        $validator
            ->numeric('valor')
            ->allowEmpty('valor');

        $validator
            ->numeric('juros')
            ->allowEmpty('juros');

        $validator
            ->numeric('multa')
            ->allowEmpty('multa');

        $validator
            ->allowEmpty('data_pagamento');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 255)
            ->allowEmpty('complemento');

        $validator
            ->numeric('valor_bruto')
            ->allowEmpty('valor_bruto');

        $validator
            ->numeric('desconto')
            ->allowEmpty('desconto');

        $validator
            ->scalar('numero_documento')
            ->maxLength('numero_documento', 100)
            ->allowEmpty('numero_documento');

        $validator
            ->allowEmpty('data_cadastro');

        $validator
            ->scalar('parcela')
            ->maxLength('parcela', 20)
            ->allowEmpty('parcela');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['planoconta_id'], 'Planocontas'));
        $rules->add($rules->existsIn(['fornecedor_id'], 'Fornecedores'));
        $rules->add($rules->existsIn(['contabilidade_id'], 'Contabilidades'));
        $rules->add($rules->existsIn(['tipo_pagamento_id'], 'TipoPagamento'));
        $rules->add($rules->existsIn(['tipo_documento_id'], 'TipoDocumento'));
        $rules->add($rules->existsIn(['contaspagar_planejamento_id'], 'ContaspagarPlanejamentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Contaspagar') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            
        }
        
        if (!is_object($entity->data)) {
         $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
       }
        if (!is_object($entity->vencimento)) {
         $entity->vencimento = implode('-', array_reverse(explode('/', $entity->vencimento)));
       }
        if (!is_object($entity->data_pagamento)) {
         $entity->data_pagamento = implode('-', array_reverse(explode('/', $entity->data_pagamento)));
       }
        if(!is_object($entity->data_cadastro)){
           $inicio = new DateTime();

           $now = $inicio->format('Y-m-d H:i:s');
           $entity->data_cadastro = $now;
        }
        return true;
    }

    public function getBillsToPay()
    {
        $now = new Time();
        $month = $now->format('m');
        $year = $now->format('Y');
        
        $resultBills = $this->find()
        ->select(['contasPagar' => 'TRUNCATE(SUM(valor + ((valor * (juros/100)) 
        + (valor * (multa/100)) - (valor * (desconto/100)))), 2)',
        'dia' => "IF(DAY(data_pagamento) < 10, CONCAT('0', DAY(data_pagamento)), DAY(data_pagamento))"])
        ->where(['MONTH(data_pagamento)' => $month, 'YEAR(data_pagamento)' => $year])
        ->group(['data_pagamento']);
        
        return $resultBills;
    }
}
