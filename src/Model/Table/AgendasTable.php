<?php
namespace App\Model\Table;

use App\Controller\Component\DataComponent;
use App\Model\Entity\Agenda;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use DateTime;

/**
 * Agendas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TipoAgendas
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $MedicoResponsaveis
 * @property \Cake\ORM\Association\BelongsTo $GrupoAgendas
 * @property \Cake\ORM\Association\BelongsTo $SituacaoAgendas
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\HasMany $AgendaProcedimentos
 * @property \Cake\ORM\Association Convenios
 */
class AgendasTable extends Table
{


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('agendas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TipoAgendas', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('MedicoResponsaveis', [
            'foreignKey' => 'profissional_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('GrupoAgendas', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoAgendas', [
            'foreignKey' => 'situacao_agenda_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('UserEdit', [
            'className'=>'Users',
            'foreignKey' => 'user_update',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AgendaBloqueios', [
            'foreignKey' => 'agenda_bloqueio_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Atendimentos',[
            'foreignKey' => 'agenda_id',
        ]);
        $this->hasMany('AgendaProcedimentos',[
            'foreignKey' => 'agenda_id',
        ]);
        $this->hasMany('GrupoAgendaHorarios',[
            'foreignKey' => 'agenda_horario_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('inicio', 'create')
            ->notEmpty('inicio');

        $validator
            ->allowEmpty('fim');

        $validator
            ->allowEmpty('cliente_id');

        $validator
            ->requirePresence('grupo_id', 'create')
            ->notEmpty('grupo_id');

        $validator
            ->allowEmpty('profissional_id');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->allowEmpty('procedimento');

        $validator
            ->boolean('confirma_retorno')
            ->allowEmpty('confirma_retorno');

        $validator
            ->allowEmpty('procedimento_provisorio');
        $validator
            ->allowEmpty('convenio_provisorio');
        $validator
            ->allowEmpty('idade');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tipo_id'], 'TipoAgendas'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        //$rules->add($rules->existsIn(['profissional_id'], 'MedicoResponsaveis'));
        $rules->add($rules->existsIn(['grupo_id'], 'GrupoAgendas'));
        $rules->add($rules->existsIn(['situacao_agenda_id'], 'SituacaoAgendas'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->modified = date('Y-m-d H:i:s');

        if($entity->isNew()) {
            $entity->user_id = $_SESSION['Auth']['User']['id'];
            $entity->situacao_id = 1;

            $now = new DateTime();
            $entity->dia_semana = $now->format('w');

            if(empty($entity->cliente_id)){
                $entity->cliente_id = -1;
            }
            
        }

        if(!empty($entity->inicio)){
            if(!is_object($entity->inicio)){
                $inicio = new DateTime();
                $entity->inicio = $inicio->createFromFormat('d/m/Y H:i', $entity->inicio);
            }
        }
        if(!empty($entity->fim)){
            if(!is_object($entity->fim)){
                $fim = new DateTime();
                $entity->fim = $fim->createFromFormat('d/m/Y H:i', $entity->fim);
            }
        }

        if(!empty($entity->id)){/* Update */
            $entity->user_update = $_SESSION['Auth']['User']['id'];

            /* Se algum campo foi alterado */
            if($entity->isDirty()){
                $log = "";
                foreach($entity->getDirty() as $dirty){
                    if(!empty($entity->getOriginal($dirty)))
                        $log .= "<strong>$dirty:</strong> ".$this->prepareFields($dirty, $entity->getOriginal($dirty))." <strong> >>> </strong> ".$this->prepareFields($dirty, $entity->get($dirty))." <br>";
                }
                $this->insertLogAgendas($entity->id, $log);
            }
        }

        return true;
    }

    public function afterSave (Event $event, EntityInterface $entity) {

        $configuracoes = TableRegistry::get('Configuracoes');
        $config = $configuracoes->find()->first();
        if(!empty($entity->id)){/* Update */
            /* Só aplicar esta função apenas na agenda Lista */
            if($this->checkValidaHorario($entity->id) && $config->agenda_interface != 2 && $config->agenda_interface_lista == 1)
                $this->newAgendamentoEmpty($entity);
        }

        return true;
    }

    public function getCountAgendas($situacao,$inicio = null,$fim = null){
        $dados = $this->find()
            ->select(['total'=>'count(Agendas.id)'])
            ->where(['Agendas.situacao_id'=>1])
            ->andWhere(['Agendas.situacao_agenda_id'=>$situacao]);


        if(!empty($inicio)){
            $dados->andWhere(['Agendas.inicio >='=>$inicio]);
        }

        if(!empty($fim)){
            $dados->andWhere(['Agendas.inicio <='=>$fim]);
        }
        return $dados->first();
    }

    public function getCountAgendasMedico($codigo_medico, $situacao,$inicio = null,$fim = null){
        $dados = $this->find()
            ->select(['total'=>'count(Agendas.id)'])
            ->where(['Agendas.situacao_id'=>1, 'Agendas.grupo_id' => $codigo_medico])
            ->andWhere(['Agendas.situacao_agenda_id'=>$situacao]);


        if(!empty($inicio)){
            $dados->andWhere(['Agendas.inicio >= ' => $inicio]);
        }

        if(!empty($fim)){
            $dados->andWhere(['Agendas.inicio <= ' => $fim]);
        }
        return $dados->first();
    }

    /** checa se existe um agendamento cadastrado
     * @param integer $grupo_id
     * @param string $inicio
     * @param bool $return_object
     * @return bool/id
     * @return  null $agenda_id
     */
    public function checkAgendamento($grupo_id, $inicio, $agenda_id = null, $return_object)
    {
        $query = $this->find()
            ->contain(['SituacaoAgendas'])
            ->where([
                'Agendas.inicio' => $inicio,
                'Agendas.grupo_id' => $grupo_id,
                'Agendas.situacao_id' => 1
            ])
            ->andWhere(['SituacaoAgendas.valida_horario' => 1])
            ->limit(1);
        if(!empty($agenda_id)){
            $query->andWhere(['Agendas.id <> ' => $agenda_id]);
        }
        if($return_object){
            $agenda = $query->first();
            if(!empty($agenda))
                return $agenda;
        }else{
            $count = $query->count();
            if($count > 0)
                return false;
            else
                return true;
        }
    }

    /** Checa se horario esta bloqueado
     * @param $inicio
     * @param $grupo_id
     * @return bool
     */
    public function checkBloqueio($inicio, $grupo_id)
    {
        $query = $this->find()
            ->where(['Agendas.grupo_id' => $grupo_id])
            ->andWhere([
                'Agendas.situacao_id' => 1,
                'Agendas.situacao_agenda_id' => 1000,
                'Agendas.inicio' => $inicio
            ])->limit(1)->count();

        if($query > 0){
            return true;
        }else{
            return false;
        }
    }

    /** Checa se existe dia da semana entre o periodo de datas com o(s) dia(s) selecionado(s)
     * @param $date
     * @param $dia_semana
     * @return bool
     */
    public function checkDiaSemana($date, $dia_semana)
    {
        $date = new Time($date);
        if($date->format('w') == $dia_semana)
            return true;
        else
            return false;
    }

    /** Exclui todos os horários gerados
     * @param $agenda_horario_id
     * @return bool
     */
    public function deleteAllHorarios($agenda_horario_id)
    {
        $query = $this->updateAll([
            'situacao_id' => 2,
            'modified' => date('Y-m-d H:i:s'),
        ], ['agenda_horario_id' => $agenda_horario_id, 'convenio_id' => -1]);
        if($query)
            return true;
        else
            return false;
    }

    /** Excluir horarios por periodos
     * @param $inicio
     * @param $grupo_id
     * @return bool
     */
    public function deleteHorarios($inicio, $grupo_id)
    {
        $agenda = $this->query()->update()
            ->set([
                'situacao_id' => 2,
                'modified' => date('Y-m-d H:i:s'),
            ])->where([
                'inicio' => $inicio,
                'grupo_id' => $grupo_id,
                'convenio_id' => -1
            ])->execute();
        if($agenda){
            return true;
        }else{
            return false;
        }
    }

    /** Troca situacao da agenda para bloqueio ou agendado
     * @param $inicio
     * @param $grupo_id
     * @param $situacao
     * @param null $nome_provisorio
     * @return bool
     */
    public function updateBloqueio($inicio, $grupo_id, $situacao, $nome_provisorio = null)
    {
        $agenda = $this->query()->update()
            ->set([
                'situacao_agenda_id' => $situacao,
                'nome_provisorio' => $nome_provisorio,
                'modified' => date('Y-m-d H:i:s')
            ])->where([
                'inicio' => $inicio,
                'grupo_id' => $grupo_id,
                'convenio_id' => -1
            ])->execute();
        if($agenda){
            return true;
        }else{
            return false;
        }
    }

    /** transfere os dados de um agendamento para outro ou cadastra um novo - Calendario
     * @param $de
     * @param null $para
     * @param array $newEntity
     * @param null $eventAction
     * @return bool
     */
    public function transferirAgendamentoCalendario($de, $para = null, $newEntity = array(), $eventAction = null)
    {
        $agenda_de = $this->get($de);
        $data = [
            'tipo_id' => $agenda_de->tipo_id,
            'procedimento_concat' => $agenda_de->procedimento_concat,
            'cliente_id' => $agenda_de->cliente_id,
            'idade' => $agenda_de->idade,
            'convenio_id' => $agenda_de->convenio_id,
            'fone_provisorio' => $agenda_de->fone_provisorio,
            'observacao' => $agenda_de->observacao,
            'situacao_agenda_id' => $agenda_de->situacao_agenda_id,
            'nome_provisorio' => $agenda_de->nome_provisorio,
            'configuracao_periodo_id' => $agenda_de->configuracao_periodo_id,
            'user_udapte' => $agenda_de->user_udapte,
            'sms' => $agenda_de->sms,
            'sms_situacao' => $agenda_de->sms_situacao,
            //'agenda_horario_id' => $agenda_de->agenda_horario_id, Se transferir o horario da problema na transferencia calendario
        ];
        // Se não for um horario do tipo gerado, pode transferir o inicio e o fim da agenda
        /*if($eventAction == 'transferir' && $agenda_de->agenda_horario_id != null){
            $data['inicio']= $agenda_de->inicio;
            $data['fim']= $agenda_de->fim;
        }*/
        if(empty($para) && !empty($newEntity)){
            $data = array_merge($data, $newEntity);
            $agenda_para = $this->newEntity();
        }else{
            $agenda_para = $this->get($para);
        }

        $agenda_para = $this->patchEntity($agenda_para, $data);
        if($this->save($agenda_para)){
            // Se não for um horario do tipo gerado, não reseta o agendamento
            if($eventAction == 'transferir'){
                $this->resetaAgendamento($de, 'calendario');
            }
            return true;
        }else{
            return false;
        }
    }

    /**
     * transfere os dados de um agendamento para outro ou cadastra um novo - Lista
     *
     * @param Object $de
     * @param Object $para
     * @param String $action
     * @return void
     */
    public function transferirAgendamentoLista($de, $para, $action)
    {
        $agenda_de = $this->get($de);
        $agenda_para = $this->get($para);
        $agenda_para->tipo_id = $agenda_de->tipo_id;
        $agenda_para->procedimento_concat = $agenda_de->procedimento_concat;
        $agenda_para->cliente_id = $agenda_de->cliente_id;
        $agenda_para->idade = $agenda_de->idade;
        $agenda_para->convenio_id = $agenda_de->convenio_id;
        $agenda_para->fone_provisorio = $agenda_de->fone_provisorio;
        $agenda_para->observacao = $agenda_de->observacao;
        $agenda_para->situacao_agenda_id = $agenda_de->situacao_agenda_id;
        $agenda_para->nome_provisorio = $agenda_de->nome_provisorio;
        $agenda_para->user_udapte = $agenda_de->user_udapte;
        $agenda_para->sms = $agenda_de->sms;
        $agenda_para->sms_situacao = $agenda_de->sms_situacao;
        //$agenda_para = $this->patchEntity($agenda_para, $data);
        if($this->save($agenda_para)){
            if($action == 'transferir')
                $this->resetaAgendamento($de, 'lista');
            
            return true;
        }else{
            return false;
        }
    }


    /** Essa função atualiza o horario da @class Agenda para um novo horario passado nos parametros
     * @param $agenda_id
     * @param $inicio - ex: 0000-00-00 00:00:00
     * @param $fim - ex: 0000-00-00 00:00:00
     * @return bool
     */
    public function atualizaHorario($agenda_id, $inicio, $fim)
    {
        $agenda = $this->get($agenda_id);
        $agenda->inicio = $inicio;
        $agenda->fim = $fim;
        if($this->save($agenda))
            return true;
        else
            return false;
    }

    /** Reseta um agendamento como se fosse horário gerado
     * @param Integer $id
     * @param String $view - calenadrio | lista
     * @return bool
     */
    public function resetaAgendamento($id, $view)
    {
        $agenda = $this->get($id);
        $data = [
            'tipo_id' => 1,
            'cliente_id' => -1,
            'situacao_agenda_id' => 5,
            'observacao' => null,
            'nome_provisorio' => null,
            'fone_provisorio' => null,
            'convenio_id' => -1,
            'idade' =>null,
            'configuracao_periodo_id' => null,
            'situacao_id' => ($view == 'calendario') ? 2 : 1,
        ];
        $agenda = $this->patchEntity($agenda, $data);
        if($this->save($agenda)){
            return true;
        }else{
            return false;
        }
    }

    /** Insere um novo agendamento vazio para um determinado grupo agenda
     *  Como se fosse horario gerado
     * @param $entity
     */
    public function newAgendamentoEmpty($entity)
    {
        $agenda = $this->newEntity();
        $data = [
            'tipo_id' => 1,
            'cliente_id' => -1,
            'situacao_agenda_id' => 5,
            'observacao' => null,
            'nome_provisorio' => null,
            'fone_provisorio' => null,
            'convenio_id' => -1,
            'idade' => null,
            'configuracao_periodo_id' => null,
            'inicio' => $entity->inicio,
            'fim' => $entity->fim,
            'grupo_id' => $entity->grupo_id,
            'agenda_horario_id' => $entity->agenda_horario_id,
            'created' => $entity->created
        ];
        $agenda = $this->patchEntity($agenda, $data);
        $this->save($agenda);
    }

    /** checa se é horario vazio e se a situacao da agenda não é para validar
     * @param $agenda_id
     * @return bool
     */
    public function checkValidaHorario($agenda_id)
    {
        $agenda = $this->get($agenda_id, ['contain' => ['SituacaoAgendas']]);
        if(!empty($agenda->agenda_horario_id) && $agenda->situacao_agenda->valida_horario == 0)
            return true;
        else
            return false;
    }

    /**
     * Insere um registro na tabela de logs Agenda
     * @param Integer $agenda_id
     * @param String $log
     * @return void
     * @author Luciano
     */
    public function insertLogAgendas($agenda_id, $log = String)
    {
        $log_agendas = TableRegistry::get('LogAgendas');
        $log_agenda = $log_agendas->newEntity();
        $log_agenda->agenda_id = $agenda_id;
        $log_agenda->log = $log;
        $log_agendas->save($log_agenda);
    }

    /**
     * Prepara os campos da tabela @class Agendas para inserir na tabela @class LogAgendas
     * Ex: cliente_id = 1 >>> cliente_id = Fulano de tal,
     *     modified = 2018-03-12 16>00:00 >>> 12/03/2018 16:00:00 
     *
     * @param [type] $field_name
     * @param [type] $field_value
     * @return void
     * @author Luciano
     */
    public function prepareFields($field_name, $field_value)
    {
        switch($field_name){
            case 'situacao_agenda_id':
            case 'situacao_anterior':
            $sit = TableRegistry::get('SituacaoAgendas');
            $sit = $sit->get($field_value);
            $result = $sit->nome;   
            break;
            case 'tipo_id':
            $tipo = TableRegistry::get('TipoAgendas');
            $tipo = $tipo->get($field_value);
            $result = $tipo->nome;
            break;
            case 'cliente_id':
            $cliente = TableRegistry::get('Clientes');
            $cliente = $cliente->get($field_value);
            $result = $cliente->nome;
            break;
            case 'profissional_id':
            $prof = TableRegistry::get('MedicoResponsaveis');
            $prof = $prof->get($field_value);
            $result = $prof->nome;
            break;
            case 'convenio_id':
            $convenio = TableRegistry::get('Convenios');
            $convenio = $convenio->get($field_value);
            $result = $convenio->nome;
            break;
            case 'user_id':
            case 'user_update':
            $user = TableRegistry::get('Users');
            $user = $user->get($field_value);
            $result = $user->nome;
            break;
            case 'situacao_id':
            $sit = TableRegistry::get('SituacaoCadastros');
            $sit = $sit->get($field_value);
            $result = $sit->nome;
            break;
            case 'inicio':
            case 'fim':
            case 'created':
            case 'modified':
                if(is_object($field_value)){
                    $result = date_format($field_value, 'd/m/Y H:i:s');
                }elseif(strripos($field_value, '-')){
                    $inicio = new DateTime($field_value);
                    $result = $inicio->format('d/m/Y H:i:s');
                }else{
                    $result = $field_value;
                }
            break;
            default: $result = $field_value;
        }
        return $result;
    }

    public function getQtdPacientes($grupo_id, $date)
    {
        $query = $this->find('all')
            ->contain(['SituacaoAgendas'])
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.grupo_id' => $grupo_id])
            ->andWhere(['Agendas.inicio >= ' => $date . ' 00:01:00'])
            ->andWhere(['Agendas.inicio <= ' => $date . ' 23:59:00'])
            ->andWhere(['SituacaoAgendas.ocultar' => 0])
            ->andWhere(['Agendas.situacao_id' => 1])
            ->count();
        return $query;
    }

}
