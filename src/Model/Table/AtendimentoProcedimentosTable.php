<?php

namespace App\Model\Table;

use App\Controller\Component\AtendimentoComponent;
use App\Controller\Component\DataComponent;
use App\Model\Entity\AtendimentoProcedimento;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use DateTime;

/**
 * AtendimentoProcedimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Procedimentos
 * @property \Cake\ORM\Association\BelongsTo $Atendimentos
 * @property \Cake\ORM\Association\BelongsTo $MedicoResponsaveis
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class AtendimentoProcedimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atendimento_procedimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Procedimentos', [
            'foreignKey' => 'procedimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'atendimento_id',
            //'joinType' => 'LEFT'
        ]);
        $this->belongsTo('MedicoResponsaveis', [
            'foreignKey' => 'medico_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Solicitantes', [
            'foreignKey' => 'solicitante_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoFaturas', [
            'foreignKey' => 'situacao_fatura_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoRecebimentos', [
            'foreignKey' => 'situacao_recebimento_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('FaturaEncerramentos', [
            'foreignKey' => 'fatura_encerramento_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AtendimentoItens', [
            'foreignKey' => 'atendproc_id',
            'conditions' => ['AtendimentoItens.situacao_id' => '1']
        ]);

        $this->hasMany('Laudos', [
            'foreignKey' => 'atendimento_procedimento_id'
        ]);

        $this->hasMany('AtendimentoProcedimentoEquipes', [
            'foreignKey' => 'atendimento_procedimento_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('ControleFinanceiro', [
            'foreignKey' => 'controle_financeiro_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsToMany('ItenCobrancas', [
            'through' => 'Procedimentos',
            'joinTable' => 'iten_cobrancas'
        ]);
        $this->belongsTo('SituacaoLaudos', [
            'foreignKey' => 'situacao_laudo_id',
            'joinType' => 'LEFT'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('valor_fatura')
            ->requirePresence('valor_fatura', 'create')
            ->notEmpty('valor_fatura');

        $validator
            ->integer('quantidade')
            ->requirePresence('quantidade', 'create')
            ->notEmpty('quantidade');

        $validator
            ->decimal('desconto')
            ->allowEmpty('desconto');

        $validator
            ->numeric('porc_desconto')
            ->allowEmpty('porc_desconto');

        $validator
            ->decimal('valor_caixa')
            ->requirePresence('valor_caixa', 'create')
            ->notEmpty('valor_caixa');

        $validator
            ->allowEmpty('digitado');

        $validator
            ->integer('material')
            ->allowEmpty('material');

        $validator
            ->integer('num_controle')
            ->allowEmpty('num_controle');

        $validator
            ->decimal('valor_matmed')
            ->allowEmpty('valor_matmed');

        $validator
            ->allowEmpty('documento_guia');

        $validator
            ->allowEmpty('dt_emissao_guia');

        $validator
            ->allowEmpty('dt_autorizacao');

        $validator
            ->allowEmpty('nr_guia');

        $validator
            ->allowEmpty('autorizacao_senha');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['procedimento_id'], 'Procedimentos'));
        $rules->add($rules->existsIn(['atendimento_id'], 'Atendimentos'));
        $rules->add($rules->existsIn(['medico_id'], 'MedicoResponsaveis'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {

        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        $entity->percentual_cobranca = $this->calculaPercentualCobranca($entity);
        $entity->total = $this->calculaTotal($entity);

        if ($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        if (!empty($entity->data)) {
            if(!is_object($entity->data)){
                $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
            }
        }

        if (!empty($entity->dt_recebimento)) {
            $entity->dt_recebimento = implode('-', array_reverse(explode('/', $entity->dt_recebimento)));
        }

        if (!empty($entity->dt_emissao_guia)) {
            if(!is_object($entity->dt_emissao_guia)){
                $inicio = new DateTime();
                $entity->dt_emissao_guia = $inicio->createFromFormat('d/m/Y H:i', $entity->dt_emissao_guia);
            }
        }

        if (!empty($entity->dt_autorizacao)) {
            if(!is_object($entity->dt_autorizacao)){
                $inicio = new DateTime();
                $entity->dt_autorizacao = $inicio->createFromFormat('d/m/Y H:i', $entity->dt_autorizacao);
            }
        }

        if (!empty($entity->dt_entrega)) {
            $inicio = new DateTime();
            if (!empty($entity->dt_entrega)) {
                $now = $inicio->createFromFormat('d/m/Y H:i', $entity->dt_entrega);
                $entity->dt_entrega = $now->format('Y-m-d H:i:s');
            }else{
                $entity->dt_entrega = date('Y-m-d H:i:s');
            }
        }

        return true;
    }

    public function recalcular_base($id, $valor_base)
    {
        $atendimento_procedimentos = $this->get($id, ['contain' => ['AtendimentoItens' => ['Dentes', 'Regioes', 'FaceItens' => ['Faces']]]]);
        $this->AtendimentoItens = TableRegistry::get('AtendimentoItens');
        $valor = 0;
        foreach ($atendimento_procedimentos->atendimento_itens as $iten_cobranca) {
            $valor_fatorar = (float)$valor_base;
            if (!empty($iten_cobranca->face_itens)) {
                $valor_fatorar = (float)$valor_base * count($iten_cobranca->face_itens);
            }
            $atendimento_itens = $this->AtendimentoItens->get($iten_cobranca->id);
            $atendimento_itens->valor_faturar = $valor_fatorar;
            $valor += $valor_fatorar;
            $this->AtendimentoItens->save($atendimento_itens);
        }
        $atendimento_procedimentos->valor_fatura = $valor;
        $this->save($atendimento_procedimentos);
        return true;
    }

    /** calcula valor total
     * @param $entity
     * @return int
     */
    public function calculaTotal($entity)
    {
        $total = 0;
        if($entity->regra == 3){// Percentual
            $total = ($entity->valor_fatura * $entity->percentual_cobranca) * $entity->quantidade;
        }elseif($entity->regra == 2 && $entity->quantidade == 2){// Quantidade
            $total = $entity->valor_fatura + ($entity->valor_fatura * $entity->percentual_cobranca);
        }elseif($entity->regra == 4){
            if($entity->ordenacao > 1)
                $total = $entity->filme_reais + $entity->porte + ($entity->uco * $entity->percentual_cobranca);
            else
                $total = $entity->filme_reais + $entity->porte + $entity->uco;
        }elseif($entity->regra == 5){
            if($entity->ordenacao > 1)
                $total = $entity->filme + ($entity->uco + $entity->porte) * $entity->percentual_cobranca;
            else
                $total = $entity->filme_reais + $entity->porte + $entity->uco;
        }else{// Normal
            $total = $entity->valor_fatura * $entity->quantidade;
        }
        return $total;
    }

    /** Calcula o percentual cobranca de acordo com as regras
     * @param $entity
     * @return float|int
     */
    public function calculaPercentualCobranca($entity)
    {
        $percentual = 0;
        if($entity->regra == 1){
            $percentual = 1;
        }elseif($entity->regra == 2){
            if($entity->quantidade == 2)
                $percentual = 0.7;
            else
                $percentual = 1;
        }elseif ($entity->regra == 3){
            switch ($entity->ordenacao){
                case 1: $percentual = 1;break;
                //case 2: $percentual = 0.7;break;
                default: $percentual = 0.7;
            }
        }elseif ($entity->regra == 4){
            switch ($entity->ordenacao){
                case 1: $percentual = 1;break;
                default: $percentual = 0.7;
            }
        }elseif ($entity->regra == 5){
            switch ($entity->ordenacao){
                case 1: $percentual = 1;break;
                default: $percentual = 0.7;
            }
        }
        return $percentual;
    }

    /** Precisa receber entidade preco_procedimentos para atualizar os valores [valor_fatura, regra, total, etc...]
     * @param $entityPrecoProcedimento
     */
    public function recalculaValoresPerPeriodos($entityPrecoProcedimento, $inicio, $fim)
    {
        $query = $this->find('all')
            ->contain(['Atendimentos'])
            ->where(['AtendimentoProcedimentos.procedimento_id' => $entityPrecoProcedimento->procedimento_id, 'situacao_fatura_id < ' => 10])
            ->andWhere(['Atendimentos.convenio_id' => $entityPrecoProcedimento->convenio_id])
            ->andWhere(['AtendimentoProcedimentos.status_faturamento' => 0])
            ->andWhere(['Atendimentos.data >= ' => $inicio, 'Atendimentos.data <= ' => $fim]);
        foreach ($query as $q){
            $ap = $this->get($q->id);
            $this->reordenaProcedimentos($q->atendimento_id);
            $ap->codigo = $entityPrecoProcedimento->codigo;
            $ap->valor_fatura = $entityPrecoProcedimento->valor_faturar;
            $ap->regra = $entityPrecoProcedimento->convenio_regracalculo_id;
            $ap->filme_reais = $entityPrecoProcedimento->filme_reais;
            $ap->filme = $entityPrecoProcedimento->filme;
            $ap->uco = $entityPrecoProcedimento->uco;
            $ap->porte = $entityPrecoProcedimento->porte;
            $this->save($ap);
        }
    }

    /** Reordena os procedimentos ja salvos no Banco
     * @param $atendimento_id
     * @param $regra
     */
    public function reordenaProcedimentos($atendimento_id)
    {
        $atendProc = $this->find('all')->where([
            'AtendimentoProcedimentos.atendimento_id' => $atendimento_id,
        ])->toArray();
        $sort_procedimentos = AtendimentoComponent::sortOrdenacao($atendProc);
        $i = 0;
        foreach ($atendProc as $ap){
            $this->save($sort_procedimentos[$i]);
            $i++;
        }
        $sort_procedimentos = AtendimentoComponent::sortProcedimentosPerTotal($atendimento_id);
    }

    /** Essa função traz os dados do @class AtendimetoProcedimentos de acordo com filtro
     * @param array $data/filtro
     * @return $this
     *
     */
    public function getAtendimentoProcedimento($data = array())
    {
        $query = $this->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios', 'Users', 'InternacaoCaraterAtendimento'], 'Procedimentos' => 'GrupoProcedimentos', 'MedicoResponsaveis'])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1]);

        if (!empty($data['situacao_fatura_id'])) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_fatura_id' => $data['situacao_fatura_id']]);
        }

        if (!empty($data['data_inicio'])) {
            $data_inicio = DataComponent::DataSQL($data['data_inicio']);
            $query->andWhere(['Atendimentos.data >= ' => $data_inicio]);
        }
        if (!empty($data['data_fim'])) {
            $data_fim = DataComponent::DataSQL($data['data_fim']);;
            $query->andWhere(['Atendimentos.data <= ' => $data_fim]);
        }

        if (!empty($data['unidade_id'])) {
            $query->andWhere(['Atendimentos.unidade_id' => $data['unidade_id']]);
        }

        if (!empty($data['origen_id'])) {
            $query->andWhere(['Atendimentos.origen_id' => $data['origen_id']]);
        }

        if (!empty($data['convenios'])) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $data['convenios']]);
        }

        if (!empty($data['grupos'])) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $data['grupos']]);
        }

        if (!empty($data['procedimentos'])) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $data['procedimentos']]);
        }

        if (!empty($data['medicos'])) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $data['medicos']]);
        }

        if (!empty($data['controle'])) {
            $controle = '%' . $data['controle'] . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }
        return $query;
    }

}
