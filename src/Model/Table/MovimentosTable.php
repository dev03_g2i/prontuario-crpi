<?php
namespace App\Model\Table;

use App\Model\Entity\Movimento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * Movimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Planocontas
 * @property \Cake\ORM\Association\BelongsTo $Fornecedors
 * @property \Cake\ORM\Association\BelongsTo $Bancos
 * @property \Cake\ORM\Association\BelongsTo $Contaspagars
 * @property \Cake\ORM\Association\BelongsTo $Contabilidades
 * @property \Cake\ORM\Association\HasMany $BancoMovimentos

 */
class MovimentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('movimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Planocontas', [
            'foreignKey' => 'planoconta_id'
        ]);
        $this->belongsTo('Fornecedors', [
            'foreignKey' => 'fornecedor_id'
        ]);
        $this->belongsTo('Bancos', [
            'foreignKey' => 'banco_id'
        ]);
        $this->belongsTo('Contaspagars', [
            'foreignKey' => 'contaspagar_id'
        ]);
        $this->belongsTo('Contabilidades', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('BancoMovimentos', [
            'foreignKey' => 'movimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('situacao')
            ->maxLength('situacao')
            ->allowEmpty('situacao');

        $validator
            ->allowEmpty('data');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento')
            ->allowEmpty('complemento');

        $validator
            ->scalar('documento')
            ->maxLength('documento')
            ->allowEmpty('documento');

        $validator
            ->numeric('credito')
            ->allowEmpty('credito');

        $validator
            ->numeric('debito')
            ->allowEmpty('debito');

        $validator
            ->integer('status_lancamento')
            ->allowEmpty('status_lancamento');

        $validator
            ->scalar('categoria')
            ->maxLength('categoria')
            ->allowEmpty('categoria');

        $validator
            ->integer('idCliente')
            ->allowEmpty('idCliente');

        $validator
            ->allowEmpty('data_criacao');

        $validator
            ->allowEmpty('data_alteracao');

        $validator
            ->integer('criado_por')
            ->allowEmpty('criado_por');

        $validator
            ->integer('atualizado_por')
            ->allowEmpty('atualizado_por');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['planoconta_id'], 'Planocontas'));
        $rules->add($rules->existsIn(['fornecedor_id'], 'Fornecedors'));
        $rules->add($rules->existsIn(['banco_id'], 'Bancos'));
        $rules->add($rules->existsIn(['contaspagar_id'], 'Contaspagars'));
        $rules->add($rules->existsIn(['contabilidade_id'], 'Contabilidades'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Movimentos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if (!is_object($entity->data)) {
         $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
       }
        if(!is_object($entity->data_criacao)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_criacao);
           $entity->data_criacao = $now;
        }
         if(!is_object($entity->data_alteracao)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_alteracao);
           $entity->data_alteracao = $now;
        }
         return true;
    }

    //Função que retorna todos os Movimentos desse mês.
    public function movesOfThisMonth()
    {
        $now = new Time();
        $month = $now->format('m');
        $year = $now->format('Y');

        $moves = $this->find()
        ->select(['saidas' => 'TRUNCATE(SUM(debito), 2)', 
        'dia' => "IF(DAY(data) < 10, CONCAT('0', DAY(data)), DAY(data))",
        'entradas' => 'TRUNCATE(SUM(credito), 2)', 
        'transferencias' => 'TRUNCATE(SUM(CASE WHEN planoconta_id = -1 THEN credito - debito ELSE 0 END), 2)',
        'saldo' => 'TRUNCATE(SUM(credito) - SUM(debito), 2)'])
        ->where(['MONTH(data)' => $month, 'YEAR(data)' => $year])
        ->group(['data']);

        return $moves;
    }
}
