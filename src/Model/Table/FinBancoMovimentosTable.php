<?php

namespace App\Model\Table;

use App\Model\Entity\FinBancoMovimento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * FinBancoMovimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinBancos
 * @property \Cake\ORM\Association\HasMany $FinMovimentos
 */
class FinBancoMovimentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_banco_movimentos');

        //banco_movimento_nome é um campo virtual
        $this->displayField('banco_movimento_nome');
        $this->primaryKey('id');
        $this->belongsTo('FinBancos', [
            'foreignKey' => 'fin_banco_id'
        ]);
        $this->hasMany('FinMovimentos', [
            'foreignKey' => 'fin_banco_movimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('mes')
            ->maxLength('mes', 2)
            ->allowEmpty('mes');

        $validator
            ->scalar('ano')
            ->maxLength('ano', 4)
            ->allowEmpty('ano');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        $validator
            ->decimal('saldoInicial')
            ->allowEmpty('saldoInicial');

        $validator
            ->decimal('saldoFinal')
            ->allowEmpty('saldoFinal');

        $validator
            ->date('data')
            ->allowEmpty('data');

        $validator
            ->integer('por')
            ->allowEmpty('por');

        $validator
            ->date('encerradoDt')
            ->allowEmpty('encerradoDt');

        $validator
            ->integer('encerradoPor')
            ->allowEmpty('encerradoPor');

        $validator
            ->scalar('data_movimento')
            ->maxLength('data_movimento', 7)
            ->allowEmpty('data_movimento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fin_banco_id'], 'FinBancos'));

        return $rules;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'FinBancoMovimentos') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        $queryData->contain(['FinBancos']);
        return $queryData;
    }

    //Função publica que retorna todos os movimentos abertos pelo mês do BancoMovimento
    public function getAllOpenedMovesBank($idCompany = null, $date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->format('m');
        $year = $now->format('Y');

        $movBancos = $this->find()
            ->contain(['FinBancos' => ['FinContabilidadeBancos'], 'FinMovimentos'])
            ->where(['FinBancoMovimentos.mes LIKE' => $month, 'FinBancoMovimentos.ano LIKE' => $year, 'FinBancos.mostra_fluxo' => 1])
            ->order(['FinBancos.nome'])
            ->group(['FinBancoMovimentos.fin_banco_id']);

        if ($idCompany) {
            $movBancos->matching('FinBancos.FinContabilidadeBancos', function ($q) use ($idCompany) {
                return $q->where(['FinContabilidadeBancos.fin_contabilidade_id' => $idCompany]);
            });
        }

        return $movBancos;
    }

    public function getLastMonthFinalBalance($date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->subMonth(1)->format('m');
        if ($month === '12') {
            $year = $now->subYear(1)->format('Y');
        } else {
            $year = $now->format('Y');
        }

        $lastBankFinalBalance = $this->find()->contain(['FinBancos'])->select(['finalBalance' => 'SUM(FinBancoMovimentos.saldoFinal)'])
            ->where(['FinBancoMovimentos.mes' => $month, 'FinBancoMovimentos.ano' => $year, 'FinBancoMovimentos.status' => 'encerrado', 'FinBancos.mostra_fluxo' => 1])
            ->first();

        return $lastBankFinalBalance->finalBalance;
    }

    /**
     * Método que recebe o id de um banco movimento e o tipo de saldo ('inical' ou 'final')
     * e devolver o respectivo dado solicitado no tipo de saldo
     *
     * @param $idBancoMovimento
     * @param $tipoSaldo
     * @return null | float
     */
    public function getSaldosPorIdBancoMovimento($idBancoMovimento, $tipoSaldo)
    {
        $saldos = $this->get($idBancoMovimento, [
            'fields' => [
                'FinBancoMovimentos.id',
                'FinBancoMovimentos.saldoInicial',
                'FinBancoMovimentos.saldoFinal'
            ]
        ]);

        if ($tipoSaldo == 'inicial') {
            return $saldos['saldoInicial'];
        } else if ($tipoSaldo == 'final') {
            return $saldos['saldoFinal'];
        }
        return null;
    }

    // Função para pegar o "nomeDoBanco - periodoDaMovimentação"
    public function getOpenedMovesBankName()
    {
        $bankMoves = $this->find('list')->where(['status IS NOT ' => 'Encerrado']);
        return $bankMoves;
    }

}
