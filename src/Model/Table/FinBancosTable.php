<?php
namespace App\Model\Table;

use App\Model\Entity\FinBanco;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinBancos Model
 *
 * @property \Cake\ORM\Association\HasMany $FinBancoMovimentos
 * @property \Cake\ORM\Association\HasMany $FinContabilidadeBancos
 * @property \Cake\ORM\Association\HasMany $FinMovimentos

 */
class FinBancosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_bancos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('FinBancoMovimentos', [
            'foreignKey' => 'fin_banco_id'
        ]);
        $this->hasMany('FinContabilidadeBancos', [
            'foreignKey' => 'fin_banco_id'
        ]);
        $this->hasMany('FinMovimentos', [
            'foreignKey' => 'fin_banco_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->allowEmpty('nome');

        $validator
            ->scalar('agencia')
            ->maxLength('agencia', 50)
            ->allowEmpty('agencia');

        $validator
            ->scalar('conta')
            ->maxLength('conta', 20)
            ->allowEmpty('conta');

        $validator
            ->numeric('limite')
            ->allowEmpty('limite');

        $validator
            ->numeric('saldo')
            ->allowEmpty('saldo');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->boolean('mostra_fluxo')
            ->allowEmpty('mostra_fluxo');

        $validator
            ->boolean('mostra_saldo_geral')
            ->allowEmpty('mostra_saldo_geral');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FinBancos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
    $queryData->order(['nome' => 'asc'])->where(['FinBancos.situacao' => 1]);
    return $queryData;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }
}
