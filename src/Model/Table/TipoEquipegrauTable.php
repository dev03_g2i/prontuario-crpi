<?php
namespace App\Model\Table;

use App\Model\Entity\TipoEquipegrau;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * TipoEquipegrau Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Situacaos
 * @property \Cake\ORM\Association\HasMany $AtendimentoProcedimentoEquipes

 */
class TipoEquipegrauTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('tipo_equipegrau');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Situacaos', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AtendimentoProcedimentoEquipes', [
            'foreignKey' => 'tipo_equipegrau_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('descricao')
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->scalar('codigo_tiss')
            ->allowEmpty('codigo_tiss');

        $validator
            ->integer('user_insert')
            ->requirePresence('user_insert', 'create')
            ->notEmpty('user_insert');

        $validator
            ->integer('user_update')
            ->allowEmpty('user_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='TipoEquipegrau') {
              $queryData->where(['TipoEquipegrau.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
          }
        
        return true;
    }

    /**
     * Lista para os campos select
     *
     * @return void
     */
    public function getList()
    {
        $query = $this->find('list')->where(['TipoEquipegrau.situacao_id' => 1]);
        return $query;
    }
}
