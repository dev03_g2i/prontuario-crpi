<?php

namespace App\Model\Table;

use App\Controller\Component\DataComponent;
use App\Model\Entity\Atendimento;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Atendimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $Unidades
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $TipoAtendimentos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Solicitantes
 * @property \Cake\ORM\Association\BelongsTo $Origens
 * @property \Cake\ORM\Association\BelongsTo $ConfiguracaoPeriodos
 * @property \Cake\ORM\Association\HasMany $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $InternacaoAcomodacao
 * @property \Cake\ORM\Association\BelongsTo $InternacaoCaraterAtendimento
 * @property \Cake\ORM\Association\BelongsTo $InternacaoMotivoAlta
 * @property \Cake\ORM\Association\BelongsTo $InternacaoSetor
 * @property \Cake\ORM\Association\BelongsTo $InternacaoLeito
 * @property \Cake\ORM\Association Agendas
 */
class AtendimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atendimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ConfiguracaoPeriodos', [
            'foreignKey' => 'configuracao_periodo_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Unidades', [
            'foreignKey' => 'unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoAtendimentos', [
            'foreignKey' => 'tipoatendimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Solicitantes', [
            'foreignKey' => 'solicitante_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('UserEdit', [
            'className' => 'Users',
            'foreignKey' => 'user_update',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Agendas', [
            'foreignKey' => 'agenda_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Origens', [
            'foreignKey' => 'origen_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('AtendimentoProcedimentos', [
            'foreignKey' => 'atendimento_id',
            'conditions' => ['AtendimentoProcedimentos.situacao_id' => '1'],
            //'joinType' => 'LEFT'
        ]);

        $this->hasMany('ContasReceber', [
            'foreignKey' => 'extern_id',
            'conditions' => ['ContasReceber.status_id' => 1],
            'joinType' => 'LEFT'
        ]);

        $this->belongsToMany('Procedimentos', [
            'through' => 'AtendimentoProcedimentos',
            'joinTable' => 'procedimentos'
        ]);

        $this->belongsToMany('MedicoResponsaveis', [
            'targetForeignKey' => 'medico_id',
            'through' => 'AtendimentoProcedimentos',
            'joinTable' => 'medico_responsaveis'
        ]);

        $this->hasMany('FaturaMatmed', [
            'foreignKey' => 'atendimento_id'
        ]);

        $this->belongsTo('InternacaoAcomodacao', [
            'foreignKey' => 'internacao_acomoda_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('InternacaoCaraterAtendimento', [
            'foreignKey' => 'carater_atendimento_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('InternacaoMotivoAlta', [
            'foreignKey' => 'internacao_motivo_alta_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('InternacaoSetor', [
            'foreignKey' => 'internacao_setor_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('InternacaoLeito', [
            'foreignKey' => 'internacao_leito_id',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('hora', 'create')
            ->notEmpty('hora');

        $validator
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->decimal('total_liquido')
            ->requirePresence('total_liquido', 'create')
            ->notEmpty('total_liquido');

        $validator
            ->decimal('desconto')
            ->allowEmpty('desconto');

        $validator
            ->decimal('total_geral')
            ->requirePresence('total_geral', 'create')
            ->notEmpty('total_geral');

        $validator
            ->decimal('total_pagoato')
            ->allowEmpty('total_pagoato');

        $validator
            ->integer('num_ficha_externa')
            ->allowEmpty('num_ficha_externa');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['unidade_id'], 'Unidades'));
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        $rules->add($rules->existsIn(['tipoatendimento_id'], 'TipoAtendimentos'));
        $rules->add($rules->existsIn(['origen_id'], 'Origens'));

        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_update = $_SESSION['Auth']['User']['id'];
        if ($entity->isNew()) {
            $entity->user_id = $_SESSION['Auth']['User']['id'];
            $entity->situacao_id = 1;
        }
        if (!empty($entity->data)) {
            $data = $entity->data;
            /* Verifica se a data não é um objeto, só fazer o implode se a data for uma string ex: 00/00/0000 */
            if (!is_object($data)) {
                $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
            }

        }
        return true;
    }

    public function getItens($atendimento)
    {
        $atendimento = $this->get($atendimento, [
            'contain' => ['AtendimentoProcedimentos' => ['AtendimentoItens']]
        ]);
        $aprov = 0;
        $reprov = 0;
        if (!empty($atendimento->atendimento_procedimentos)) {
            foreach ($atendimento->atendimento_procedimentos as $anted) {
                foreach ($anted->atendimento_itens as $iten_cobranca) {
                    ($iten_cobranca->status == 0) ? $reprov++ : $aprov++;
                }
            }
            if ($reprov > 0 && $aprov > 0) {
                return 1;
            } else if ($aprov > 0 && $reprov <= 0) {
                return 2;
            } else {
                return 0;
            }
        }
    }

    public function getAtendimentos($data = array())
    {
        $query = $this->find('all')
            ->contain(['Solicitantes', 'Clientes', 'Convenios', 'Users', 'AtendimentoProcedimentos' => ['Procedimentos' => 'GrupoProcedimentos', 'MedicoResponsaveis']])
            ->matching('AtendimentoProcedimentos')
            ->matching('Procedimentos')
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1]);

        if (!empty($data['situacao_fatura_id'])) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_fatura_id' => $data['situacao_fatura_id']]);
        }

        if (!empty($data['data_inicio'])) {
            $data_inicio = DataComponent::DataSQL($data['data_inicio']);
            $query->andWhere(['Atendimentos.data >= ' => $data_inicio]);
        }
        if (!empty($data['data_fim'])) {
            $data_fim = DataComponent::DataSQL($data['data_fim']);;
            $query->andWhere(['Atendimentos.data <= ' => $data_fim]);
        }

        if (!empty($data['unidade_id'])) {
            $query->andWhere(['Atendimentos.unidade_id' => $data['unidade_id']]);
        }

        if (!empty($data['origen_id'])) {
            $query->andWhere(['Atendimentos.origen_id' => $data['origen_id']]);
        }

        if (!empty($data['convenios'])) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $data['convenios']]);
        }

        if (!empty($data['grupos'])) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $data['grupos']]);
        }

        if (!empty($data['procedimentos'])) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $data['procedimentos']]);
        }

        if (!empty($data['medicos'])) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $data['medicos']]);
        }

        if (!empty($data['controle'])) {
            $controle = '%' . $data['controle'] . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }
        return $query;
    }
}
