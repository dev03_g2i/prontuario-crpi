<?php
namespace App\Model\Table;

use App\Model\Entity\Planoconta;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Planocontas Model
 *
 */
class ClassificacaocontasTable extends Table
{


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('classificacaocontas');
        $this->displayField('descricao');
        $this->primaryKey('id');

        //$this->addBehavior('Encoding');

        $this->hasMany('Planocontas', [
            'foreignKey' => 'classificacao'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->integer('tipo')
            ->allowEmpty('tipo');

        $validator
            ->integer('considera')
            ->allowEmpty('considera');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
