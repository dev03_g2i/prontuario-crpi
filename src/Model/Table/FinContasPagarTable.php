<?php
namespace App\Model\Table;

use App\Model\Entity\FinContasPagar;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * FinContasPagar Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinPlanoContas
 * @property \Cake\ORM\Association\BelongsTo $FinFornecedores
 * @property \Cake\ORM\Association\BelongsTo $FinContabilidades
 * @property \Cake\ORM\Association\BelongsTo $FinTipoPagamentos
 * @property \Cake\ORM\Association\BelongsTo $FinTipoDocumentos
 * @property \Cake\ORM\Association\BelongsTo $FinContasPagarPlanejamentos
 * @property \Cake\ORM\Association\HasMany $FinAbatimentosContasPagar
 * @property \Cake\ORM\Association\HasMany $FinMovimentos

 */
class FinContasPagarTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_contas_pagar');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('FinPlanoContas', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->belongsTo('FinFornecedores', [
            'foreignKey' => 'fin_fornecedor_id'
        ]);
        $this->belongsTo('FinContabilidades', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->belongsTo('FinTipoPagamento', [
            'foreignKey' => 'fin_tipo_pagamento_id'
        ]);
        $this->belongsTo('FinTipoDocumento', [
            'foreignKey' => 'fin_tipo_documento_id'
        ]);
        $this->belongsTo('FinContasPagarPlanejamentos', [
            'foreignKey' => 'fin_contas_pagar_planejamento_id'
        ]);
        $this->hasMany('FinAbatimentosContasPagar', [
            'foreignKey' => 'fin_contas_pagar_id'
        ]);
        $this->hasMany('FinMovimentos', [
            'foreignKey' => 'fin_contas_pagar_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->requirePresence('data')
            ->notEmpty('data');

        $validator
            ->requirePresence('vencimento')
            ->notEmpty('vencimento');

        $validator
            ->numeric('valor')
            ->allowEmpty('valor');

        $validator
            ->numeric('juros')
            ->allowEmpty('juros');

        $validator
            ->numeric('multa')
            ->allowEmpty('multa');

        $validator
            ->allowEmpty('data_pagamento');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 255)
            ->allowEmpty('complemento');

        $validator
            ->numeric('valor_bruto')
            ->allowEmpty('valor_bruto');

        $validator
            ->numeric('desconto')
            ->allowEmpty('desconto');

        $validator
            ->scalar('numero_documento')
            ->maxLength('numero_documento', 100)
            ->allowEmpty('numero_documento');

        $validator
            ->allowEmpty('data_cadastro');

        $validator
            ->scalar('parcela')
            ->maxLength('parcela', 20)
            ->allowEmpty('parcela');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fin_plano_conta_id'], 'FinPlanoContas'));
        $rules->add($rules->existsIn(['fin_fornecedor_id'], 'FinFornecedores'));
        $rules->add($rules->existsIn(['fin_contabilidade_id'], 'FinContabilidades'));
        $rules->add($rules->existsIn(['fin_tipo_pagamento_id'], 'FinTipoPagamento'));
        $rules->add($rules->existsIn(['fin_tipo_documento_id'], 'FinTipoDocumento'));
        $rules->add($rules->existsIn(['fin_contas_pagar_planejamento_id'], 'FinContasPagarPlanejamentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FinContasPagar') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
    // $queryData->where(['FinContasPagar.situacao' => 1]);
    return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
        }
        
        if (!is_object($entity->data) && !empty($entity->data)) {
            $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
        }
        if (!is_object($entity->vencimento) && !empty($entity->vencimento)) {
            $entity->vencimento = implode('-', array_reverse(explode('/', $entity->vencimento)));
        }
        if (!is_object($entity->data_pagamento) && !empty($entity->data_pagamento)) {
            $entity->data_pagamento = implode('-', array_reverse(explode('/', $entity->data_pagamento)));
        }
        if(!is_object($entity->data_cadastro)){
            $inicio = new DateTime();

            $now = $inicio->format('Y-m-d H:i:s');
            $entity->data_cadastro = $now;
        }
        return true;
    }

    public function getBillsToPay($date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->format('m');
        $year = $now->format('Y');
        
        $resultBills = $this->find()
        ->select(['contasPagar' => 'TRUNCATE(SUM(valor + ((valor * (juros/100)) 
        + (valor * (multa/100)) - (valor * (desconto/100)))), 2)',
        'dia' => "IF(DAY(vencimento) < 10, CONCAT('0', DAY(vencimento)), DAY(vencimento))"])
        ->where(['MONTH(vencimento)' => $month, 'YEAR(vencimento)' => $year, 'FinContasPagar.data_pagamento IS NULL'])
        ->group(['vencimento']);
        
        return $resultBills;
    }

    public function getBillsToPayForModal($date = null)
    {   
        $resultBills = $this->find()
        ->contain(['FinFornecedores', 'FinPlanoContas', 'FinContabilidades', 'FinTipoDocumento'])
        ->select([
            'Vencimento' => 'IF(FinContasPagar.vencimento, DATE_FORMAT(FinContasPagar.vencimento, "%d/%m/%Y"), "")',
            'Fornecedor' => 'IF(FinContasPagar.fin_fornecedor_id, FinFornecedores.nome, "")',
            'Plano de contas' => 'IF(FinContasPagar.fin_plano_conta_id, FinPlanoContas.nome, "")',
            'Tipo de pagamento' => 'IF(FinContasPagar.fin_tipo_documento_id, FinTipoDocumento.descricao, "")',
            'Parcela' => 'IF(FinContasPagar.parcela, FinContasPagar.parcela, "")',
            'Complemento' => 'IF(FinContasPagar.complemento, FinContasPagar.complemento, "")',
            'Valor bruto' => 'FORMAT(FinContasPagar.valor_bruto, 2, "de_DE")',
            'Valor c/ ded' => 'FORMAT(FinContasPagar.valor, 2, "de_DE")'
            ])
        ->where(['FinContasPagar.vencimento' => $date, 'FinContasPagar.data_pagamento IS NULL'])
        ->order(['FinContasPagar.id']);
        
        return $resultBills;
    }
}
