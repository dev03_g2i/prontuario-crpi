<?php
namespace App\Model\Table;

use App\Model\Entity\DeducoesContasreceber;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DeducoesContasreceber Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Deducoes
 * @property \Cake\ORM\Association\BelongsTo $Contasreceber
 */
class DeducoesContasreceberTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('deducoes_contasreceber');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Deducoes', [
            'foreignKey' => 'deducoes_id'
        ]);
        $this->belongsTo('Contasreceber', [
            'foreignKey' => 'contasreceber_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('percentual')
            ->allowEmpty('percentual');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['deducoes_id'], 'Deducoes'));
        $rules->add($rules->existsIn(['contasreceber_id'], 'Contasreceber'));
        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
