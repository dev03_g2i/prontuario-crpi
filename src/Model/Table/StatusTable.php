<?php
namespace App\Model\Table;

use App\Model\Entity\Status;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Status Model
 *
 * @property \Cake\ORM\Association\HasMany $AnexoFinanceiro
 * @property \Cake\ORM\Association\HasMany $Contasreceber
 * @property \Cake\ORM\Association\HasMany $Correcaomonetaria
 * @property \Cake\ORM\Association\HasMany $TipoDocumento
 * @property \Cake\ORM\Association\HasMany $TipoPagamento
 */
class StatusTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('status');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('AnexoFinanceiro', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('Contasreceber', [
            'foreignKey' => 'status'
        ]);
        $this->hasMany('Correcaomonetaria', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('TipoDocumento', [
            'foreignKey' => 'status_id'
        ]);
        $this->hasMany('TipoPagamento', [
            'foreignKey' => 'status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('descricao');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
