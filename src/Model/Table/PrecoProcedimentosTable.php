<?php
namespace App\Model\Table;

use App\Model\Entity\PrecoProcedimento;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * PrecoProcedimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $Procedimentos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 */
class PrecoProcedimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('preco_procedimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Procedimentos', [
            'foreignKey' => 'procedimento_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('UserReg', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('UserAlt', [
            'className' => 'Users',
            'foreignKey' => 'user_update'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ConvenioRegracalculo', [
            'foreignKey' => 'convenio_regracalculo_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('valor_faturar')
            ->requirePresence('valor_faturar', 'create')
            ->notEmpty('valor_faturar');

        $validator
            ->decimal('valor_particular')
            ->requirePresence('valor_particular', 'create')
            ->notEmpty('valor_particular');

        $validator
            ->add('convenio_id', 'procedimento_id', [
                'rule' => function($value, $context) {
                    return isset($context['data']['procedimento_id']);
                },
                'message' => 'The two password you typed do not match.',
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        $rules->add($rules->existsIn(['procedimento_id'], 'Procedimentos'));
        //$rules->add($rules->isUnique(['convenio_id']));
        //$rules->add($rules->isUnique(['procedimento_id'], 'Este procedimento já pertence a este Convênio!'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        if(empty($entity->id))// INSERT
        {
            $entity->user_id = $_SESSION['Auth']['User']['id'];
        }else{// UPDATE
            $entity->user_update = $_SESSION['Auth']['User']['id'];
        }

        if($entity->isNew()) {
            $entity->situacao_id = 1;

            /*if($this->verificaProcedimento($entity->convenio_id, $entity->procedimento_id) > 0){
                $this->error = __('Já existe um Procedimento para este Convênio!');
                return false;
            }*/
        }

        if(!empty($entity->data)){
            $entity->data = implode('-',array_reverse(explode('/',$entity->data)));
        }

        return true;
    }

   /* public function afterSave (Event $event, EntityInterface $entity)
    {
        $this->AtendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $this->AtendimentoProcedimentos->recalculaValores($entity);

        return true;
    }*/

    //TODO verifica se ja existe procedimento para o Convênio
    public function verificaProcedimento($convenio_id, $procedimento_id)
    {
        return $this->find('all')->where(['convenio_id' => $convenio_id, 'procedimento_id' => $procedimento_id, 'situacao_id' => 1])->count();
    }

    /** Traz os procedimentos de um convenio especifico e que esta cadastrado na tabela preco_procedimentos
     * @param null $convenio_id
     * @return Query
     */
    public function getProcedimentosToConvenio($convenio_ids)
    {
        $query = $this->find('all')->contain(['Procedimentos'])
            ->where(['PrecoProcedimentos.situacao_id' => 1])
            ->andWhere(['PrecoProcedimentos.convenio_id IN' => $convenio_ids]);
        $procs = [];
        foreach ($query as $pp){
            $procs[$pp->procedimento->id] = $pp->procedimento->nome;
        }
        return $procs;
    }

}
