<?php

namespace App\Model\Table;

use App\Model\Entity\FinMovimento;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * FinMovimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinPlanoContas
 * @property \Cake\ORM\Association\BelongsTo $FinFornecedors
 * @property \Cake\ORM\Association\BelongsTo $FinBancos
 * @property \Cake\ORM\Association\BelongsTo $FinContasPagars
 * @property \Cake\ORM\Association\BelongsTo $FinContabilidades
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $FinBancoMovimentos
 */
class FinMovimentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_movimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('FinPlanoContas', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->belongsTo('FinFornecedores', [
            'foreignKey' => 'fin_fornecedor_id'
        ]);
        $this->belongsTo('FinBancos', [
            'foreignKey' => 'fin_banco_id'
        ]);
        $this->belongsTo('FinContasPagar', [
            'foreignKey' => 'fin_contas_pagar_id'
        ]);
        $this->belongsTo('FinContabilidades', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsTo('FinBancoMovimentos', [
            'foreignKey' => 'fin_banco_movimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('situacao')
            ->maxLength('situacao', 11)
            ->allowEmpty('situacao');

        $validator
            ->allowEmpty('data');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 255)
            ->allowEmpty('complemento');

        $validator
            ->scalar('documento')
            ->maxLength('documento', 255)
            ->allowEmpty('documento');

        $validator
            ->integer('fin_banco_movimento_id')
            ->allowEmpty('fin_banco_movimento_id');

        $validator
            ->numeric('credito')
            ->allowEmpty('credito');

        $validator
            ->numeric('debito')
            ->allowEmpty('debito');

        $validator
            ->integer('status_lancamento')
            ->allowEmpty('status_lancamento');

        $validator
            ->scalar('categoria')
            ->maxLength('categoria', 9)
            ->allowEmpty('categoria');

        $validator
            ->integer('cliente_id')
            ->allowEmpty('cliente_id');

        $validator
            ->allowEmpty('data_criacao');

        $validator
            ->allowEmpty('data_alteracao');

        $validator
            ->integer('criado_por')
            ->allowEmpty('criado_por');

        $validator
            ->integer('atualizado_por')
            ->allowEmpty('atualizado_por');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fin_plano_conta_id'], 'FinPlanoContas'));
        $rules->add($rules->existsIn(['fin_fornecedor_id'], 'FinFornecedores'));
        $rules->add($rules->existsIn(['fin_banco_id'], 'FinBancos'));
        $rules->add($rules->existsIn(['fin_contas_pagar_id'], 'FinContasPagar'));
        $rules->add($rules->existsIn(['fin_contabilidade_id'], 'FinContabilidades'));
        $rules->add($rules->existsIn(['fin_banco_movimento_id'], 'FinBancoMovimentos'));
        return $rules;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'FinMovimentos') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        $queryData->where(['FinMovimentos.situacao' => 1]);
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->criado_por = $_SESSION['Auth']['User']['id'];
        if ($entity->isNew()) {
        }

        if (!is_object($entity->data)) {
            $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
        }
        if (!is_object($entity->data_criacao)) {
            $inicio = new DateTime();

            $now = $inicio->format('Y-m-d H:i:s');
            $entity->data_criacao = $now;
        }
        if (!is_object($entity->data_alteracao)) {
            $inicio = new DateTime();

            $now = $inicio->format('Y-m-d H:i:s');
            $entity->data_alteracao = $now;
        }
        return true;
    }

    //Função que retorna todos os Movimentos desse mês.
    public function movesOfThisMonth($date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->format('m');
        $year = $now->format('Y');

        $moves = $this->find()->contain(['FinPlanoContas' => ['FinClassificacaoContas']])
            ->select(['id', 'saidas' => 'TRUNCATE(SUM(FinMovimentos.debito), 2)',
                'dia' => "IF(DAY(FinMovimentos.data) < 10, CONCAT('0', DAY(FinMovimentos.data)), DAY(FinMovimentos.data))",
                'entradas' => 'TRUNCATE(SUM(FinMovimentos.credito), 2)',
                'saldo' => 'TRUNCATE(SUM(FinMovimentos.credito) - SUM(FinMovimentos.debito), 2)', 'FinMovimentos.fin_plano_conta_id'])
            ->where(['MONTH(FinMovimentos.data)' => $month, 'YEAR(FinMovimentos.data)' => $year])
            ->group(['FinMovimentos.data'])
            ->order(['FinMovimentos.data']);

        $transfs = $this->find()->contain(['FinPlanoContas' => ['FinClassificacaoContas']])
            ->select(['dia' => "IF(DAY(FinMovimentos.data) < 10, CONCAT('0', DAY(FinMovimentos.data)), DAY(FinMovimentos.data))",
                'transferencia_d' => 'TRUNCATE(SUM(FinMovimentos.debito), 2)',
                'transferencia_c' => 'TRUNCATE(SUM(FinMovimentos.credito), 2)'])
            ->where(['MONTH(FinMovimentos.data)' => $month, 'YEAR(FinMovimentos.data)' => $year, 'FinClassificacaoContas.operacao_transferencia = ' => 1])
            ->group(['FinMovimentos.data'])
            ->order(['FinMovimentos.data']);

        return [$moves, $transfs];
    }

    public function movesOfLastTwelveMonths($date)
    {
        $date = new Time($date);

        $month = $date->format('m');
        $year = $date->format('Y');

        $moves = $this->find()->contain(['FinPlanoContas' => ['FinClassificacaoContas']])
            ->select(['saidas' => 'TRUNCATE(SUM(FinMovimentos.debito), 2)',
                'competencia' => "IF(MONTH(FinMovimentos.data) < 10, CONCAT('0', MONTH(FinMovimentos.data), '-', YEAR(FinMovimentos.data)), CONCAT(MONTH(FinMovimentos.data), '-', YEAR(FinMovimentos.data)))",
                'entradas' => 'TRUNCATE(SUM(FinMovimentos.credito), 2)',
                'saldo' => 'TRUNCATE(SUM(FinMovimentos.credito) - SUM(FinMovimentos.debito), 2)'])
            ->where(['MONTH(FinMovimentos.data)' => $month, 'YEAR(FinMovimentos.data)' => $year, 'FinClassificacaoContas.considera' => 1])
            ->first();

        return $moves;
    }

    public function movesForClassOfLastTwelveMonths($id, $date)
    {
        $date = new Time($date);

        $month = $date->format('m');
        $year = $date->format('Y');

        $movesForClass = $this->find()->contain(['FinPlanoContas' => ['FinClassificacaoContas']])
        ->select([
            'saldo' => 'IF(SUM(FinMovimentos.credito - FinMovimentos.debito) IS NOT NULL, TRUNCATE(ABS(SUM(FinMovimentos.credito - FinMovimentos.debito)), 2), "0.00")'])
        ->where(['MONTH(FinMovimentos.data)' => $month, 'YEAR(FinMovimentos.data)' => $year, 'FinPlanoContas.fin_classificacao_conta_id' => $id])
        ->first();

        return $movesForClass;
    }

    public function movesOfThisMonthGroupId($date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->format('m');
        $year = $now->format('Y');

        $moves = $this->find()
            ->contain(['FinPlanoContas', 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes', 'FinBancoMovimentos'])
            ->where(['MONTH(FinMovimentos.data)' => $month, 'YEAR(FinMovimentos.data)' => $year]);

        return $moves;
    }

    public function getMovesForPlan($ranking = null)
    {
        $moves = $this->find()
            ->contain(['FinPlanoContas'])
            ->where(['FinPlanoContas.fin_classificacao_conta_id' => $ranking])
            ->group(['FinMovimentos.fin_plano_conta_id']);

        return $moves;
    }

    public function getMovesForModal($dia)
    {
        $moves = $this->find('all')
            ->contain(['FinPlanoContas', 'FinContabilidades', 'FinFornecedores', 'FinBancos', 'FinBancoMovimentos', 'Clientes'])
            ->select([
                'NrLanc' => 'FinMovimentos.id', 'Data' => 'DATE_FORMAT(FinMovimentos.data, "%d/%m/%Y")',
                'Credor/Cliente' =>
                    'CASE
                WHEN FinMovimentos.fin_fornecedor_id IS NOT NULL and FinMovimentos.cliente_id IS NOT NULL
                    THEN CONCAT(FinFornecedores.nome, "/", Clientes.nome)
                WHEN FinMovimentos.fin_fornecedor_id IS NULL and FinMovimentos.cliente_id IS NOT NULL
                    THEN Clientes.nome
                WHEN FinMovimentos.fin_fornecedor_id IS NOT NULL and FinMovimentos.cliente_id IS NULL
                    THEN FinFornecedores.nome
                ELSE
                    ""
            END',
                'Plano de contas' => 'FinPlanoContas.nome', 'Empresa' => 'FinContabilidades.nome',
                'Documento' => 'IF(FinMovimentos.documento IS NOT NULL, FinMovimentos.documento, "")',
                'Banco/movimento' => 'CONCAT(FinBancos.nome, " - ", FinBancoMovimentos.data_movimento)',
                'Observação' => 'IF(FinMovimentos.complemento IS NOT NULL, FinMovimentos.complemento, "")', 'Crédito' => 'FORMAT(FinMovimentos.credito, 2, "de_DE")',
                'Débito' => 'FORMAT(FinMovimentos.debito, 2, "de_DE")', 'Saldo' => 'FORMAT(FinMovimentos.credito - FinMovimentos.debito, 2, "de_DE")'])
            ->where(['FinMovimentos.data' => $dia])
            ->order(['FinMovimentos.id']);

        return $moves;
    }

    public function finishAccounts($form, $type)
    {
        $moves = ($type === 'pagar') ? $this->finishPayAccounts($form) : $this->finishReceiveAccounts($form);

        return $moves;
    }

    private function finishPayAccounts($form)
    {
        $FinContasPagar = TableRegistry::get('FinContasPagar');
        $FinBancoMovimentos = TableRegistry::get('FinBancoMovimentos');

        $bancoMovimento = $FinBancoMovimentos->get($form[0]);
        $banco = $bancoMovimento->fin_banco_id;

        foreach ($form[3] as $id) {
            $contaspagar = $FinContasPagar->get($id);
            $conta['data_pagamento'] = new Time($form[4]);
            $conta['data'] = $contaspagar->data;
            $conta['vencimento'] = $contaspagar->vencimento;
            $contaspagar = $FinContasPagar->patchEntity($contaspagar, $conta);

            if ($FinContasPagar->save($contaspagar)) {
                $contasFallBack[] = $contaspagar;
            } else {
                $this->fallbackContaPagarMovimento($contasFallBack);
                return null;
            }
        }

        $contasPagar = $FinContasPagar->find()
            ->where(['FinContasPagar.id IN ' => $form[3]]);

        foreach ($contasPagar as $conta) {
            $movimento = $this->newEntity();
            $newMovimento['debito'] = $conta->valor;
            $newMovimento['documento'] = $form[1];
            $newMovimento['situacao'] = 1;
            $newMovimento['fin_contabilidade_id'] = $conta->fin_contabilidade_id;
            $newMovimento['fin_plano_conta_id'] = $conta->fin_plano_conta_id;
            $newMovimento['fin_fornecedor_id'] = $conta->fin_fornecedor_id;
            $newMovimento['fin_banco_movimento_id'] = $form[0];
            $newMovimento['fin_banco_id'] = $banco;
            $newMovimento['data'] = $conta->data_pagamento;
            $newMovimento['data_criacao'] = $conta->data_pagamento;
            $newMovimento['criado_por'] = $_SESSION['Auth']['User']['id'];
            $finMovimento = $this->patchEntity($movimento, $newMovimento);

            if ($this->save($finMovimento)) {
                $movimentosFallBack[] = $finMovimento;
            } else {
                $this->fallbackContaPagarMovimento($contasFallBack, $movimentosFallBack);
                return null;
            }
        }

        foreach ($movimentosFallBack as $movimento) {
            $ids[] = $movimento->id;
        }

        $moves = $this->find('all')
            ->contain(['FinPlanoContas', 'FinContabilidades', 'FinFornecedores', 'FinBancoMovimentos' => ['FinBancos'], 'Clientes'])
            ->select([
                'NrLanc' => 'FinMovimentos.id', 'Data' => 'DATE_FORMAT(FinMovimentos.data, "%d/%m/%Y")',
                'Credor' => 'FinFornecedores.nome',
                'Plano de contas' => 'FinPlanoContas.nome', 'Empresa' => 'FinContabilidades.nome',
                'Documento' => 'IF(FinMovimentos.documento IS NOT NULL, FinMovimentos.documento, "")',
                'Banco/movimento' => 'CONCAT(FinBancos.nome, " - ", IF(FinBancoMovimentos.mes < 10, CONCAT("0", FinBancoMovimentos.mes), FinBancoMovimentos.mes), "/", FinBancoMovimentos.ano)',
                'Observação' => 'IF(FinMovimentos.complemento IS NOT NULL, FinMovimentos.complemento, "")', 'Crédito' => 'FORMAT(FinMovimentos.credito, 2, "de_DE")',
                'Débito' => 'FORMAT(FinMovimentos.debito, 2, "de_DE")'])
            ->where(['FinMovimentos.id IN ' => $ids])
            ->order(['FinMovimentos.id']);
        
        return $moves;
    }

    private function finishReceiveAccounts($form)
    {
        $FinContasReceber = TableRegistry::get('FinContasReceber');
        $FinBancoMovimentos = TableRegistry::get('FinBancoMovimentos');

        $bancoMovimento = $FinBancoMovimentos->get($form[0]);
        $banco = $bancoMovimento->fin_banco_id;

        foreach ($form[3] as $id) {
            $contasreceber = $FinContasReceber->get($id);
            $conta['data_pagamento'] = date('Y-m-d');
            $conta['data'] = $contasreceber->data;
            $conta['vencimento'] = $contasreceber->vencimento;
            $conta['desconto'] = $contasreceber->desconto;
            $contasreceber = $FinContasReceber->patchEntity($contasreceber, $conta);

            if ($FinContasReceber->save($contasreceber)) {
                $contasFallBack[] = $contasreceber;
            } else {
                $this->fallbackContaReceberMovimento($contasFallBack);
                return null;
            }
        }

        $contasreceber = $FinContasReceber->find()
            ->where(['FinContasReceber.id IN ' => $form[3]]);

        foreach ($contasreceber as $conta) {
            $movimento = $this->newEntity();
            $newMovimento['credito'] = $conta->valor;
            $newMovimento['documento'] = $form[1];
            $newMovimento['situacao'] = 1;
            $newMovimento['fin_contabilidade_id'] = $conta->fin_contabilidade_id;
            $newMovimento['fin_plano_conta_id'] = $conta->fin_plano_conta_id;
            $newMovimento['cliente_id'] = $conta->cliente_id;
            $newMovimento['fin_banco_id'] = $banco;
            $newMovimento['fin_banco_movimento_id'] = $form[0];
            $newMovimento['data'] = $conta->data_pagamento;
            $newMovimento['data_criacao'] = $conta->data_pagamento;
            $newMovimento['criado_por'] = $_SESSION['Auth']['User']['id'];
            $finMovimento = $this->patchEntity($movimento, $newMovimento);

            if ($this->save($finMovimento)) {
                $movimentosFallBack[] = $finMovimento;
            } else {
                $this->fallbackContaReceberMovimento($contasFallBack, $movimentosFallBack);
                return null;
            }
        }

        foreach ($movimentosFallBack as $movimento) {
            $ids[] = $movimento->id;
        }

        $moves = $this->find('all')
            ->contain(['FinPlanoContas', 'FinContabilidades', 'FinBancoMovimentos' => ['FinBancos'], 'Clientes'])
            ->select([
                'NrLanc' => 'FinMovimentos.id', 'Data' => 'DATE_FORMAT(FinMovimentos.data, "%d/%m/%Y")',
                'Cliente' => 'Clientes.nome',
                'Plano de contas' => 'FinPlanoContas.nome', 'Empresa' => 'FinContabilidades.nome',
                'Documento' => 'IF(FinMovimentos.documento IS NOT NULL, FinMovimentos.documento, "")',
                'Banco/movimento' => 'CONCAT(FinBancos.nome, " - ", FinBancoMovimentos.data_movimento)',
                'Observação' => 'IF(FinMovimentos.complemento IS NOT NULL, FinMovimentos.complemento, "")', 'Crédito' => 'FORMAT(FinMovimentos.credito, 2, "de_DE")',
                'Débito' => 'FORMAT(FinMovimentos.debito, 2, "de_DE")'])
            ->where(['FinMovimentos.id IN ' => $ids])
            ->order(['FinMovimentos.id']);

        return $moves;
    }

    //Função para fazer o fallback da conta e do movimento em caso de erro
    private function fallbackContaPagarMovimento($contas, $movimentos = null)
    {
        $FinContasPagar = TableRegistry::get('FinContasPagar');

        foreach ($contas as $fallBack) {
            $contaspagar = $FinContasPagar->get($fallback->id);
            $conta['data_pagamento'] = null;
            $conta['data'] = $contaspagar->data;
            $conta['vencimento'] = $contaspagar->vencimento;
            $contaspagar = $FinContasPagar->patchEntity($contaspagar, $conta);
            $FinContasPagar->save($contaspagar);
        }

        if (isset($movimentos)) {
            foreach ($movimentos as $movimento) {
                $movimento = $this->get($fallback->id);
                $this->delete($movimento);
            }
        }

        return;
    }

    //Função para fazer o fallback da conta e do movimento em caso de erro
    private function fallbackContaReceberMovimento($contas, $movimentos = null)
    {
        $FinContasReceber = TableRegistry::get('FinContasReceber');

        foreach ($contas as $fallBack) {
            $contasreceber = $FinContasReceber->get($fallback->id);
            $conta['data_pagamento'] = null;
            $conta['data'] = $contasreceber->data;
            $conta['vencimento'] = $contasreceber->vencimento;
            $contasreceber = $FinContasReceber->patchEntity($contasreceber, $conta);
            $FinContasReceber->save($contasreceber);
        }

        if (isset($movimentos)) {
            foreach ($movimentos as $movimento) {
                $movimento = $this->get($fallback->id);
                $this->delete($movimento);
            }
        }

        return;
    }

    public function getFinMovimentoPorAno($ano)
    {
        return $query = $this->find('all')
            ->contain(['FinPlanoContas' => ['FinClassificacaoContas']])
            ->where(['YEAR(FinMovimentos.data)' => $ano])
            ->andWhere(['FinClassificacaoContas.considera' => 1])
            ->andWhere(['FinMovimentos.situacao' => 1])
            ->group('FinClassificacaoContas.tipo');

    }

    public function getValorTotalPorAno($ano, $plano_conta, $classificacao_conta, $tipo)
    {
        $total = null;
        $query = $this->getFinMovimentoPorAno($ano);

        if (!empty($plano_conta)) {
            $query->andWhere(['FinPlanoContas.id' => $plano_conta]);
        }

        if (!empty($classificacao_conta)) {
            $query->andWhere(['FinClassificacaoContas.id' => $classificacao_conta]);
        }

        if (!empty($tipo)) {
            $query->andWhere(['FinClassificacaoContas.tipo' => $tipo]);
        }

        $query->select([
            'total' => $query->func()->sum('FinMovimentos.credito - FinMovimentos.debito'),
            'tipo' => 'FinClassificacaoContas.tipo'
        ]);

        $query = $query->toArray();

        if ($query[0]['tipo'] == 1) {
            $total['entrada'] = !empty($query[0]['total']) ? $query[0]['total'] : 0;
            $total['saida'] = !empty($query[1]['total']) ? $query[1]['total'] : 0;
        } else {
            $total['saida'] = !empty($query[0]['total']) ? $query[0]['total'] : 0;
            $total['entrada'] = !empty($query[1]['total']) ? $query[1]['total'] : 0;
        }

        return $total;

    }

    public function getTotaisMesAMes($ano, $plano_conta, $classificacao_conta, $tipo)
    {
        $query = $this->getFinMovimentoPorAno($ano);

        if (!empty($plano_conta)) {
            $query->andWhere(['FinPlanoContas.id' => $plano_conta]);
        }

        if (!empty($classificacao_conta)) {
            $query->andWhere(['FinClassificacaoContas.id' => $classificacao_conta]);
        }

        if (!empty($tipo)) {
            $query->andWhere(['FinClassificacaoContas.tipo' => $tipo]);
        }

        $query->select([
            'tipo' => 'FinClassificacaoContas.tipo',
            'janeiro' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 1) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'fevereiro' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 2) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'marco' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 3) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'abril' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 4) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'maio' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 5) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'junho' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 6) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'julho' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 7) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'agosto' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 8) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'setembro' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 9) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'outubro' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 10) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'novembro' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 11) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)',
            'dezembro' => 'ROUND(SUM(CASE WHEN (MONTH(FinMovimentos.data) = 12) THEN (FinMovimentos.credito - FinMovimentos.debito) END), 2)'
        ]);

        $query = $query->toArray();

        if ($query[0]['tipo'] == 1) {
            $total['entrada'] = !empty($query[0]) ? $query[0] : 0;
            $total['saida'] = !empty($query[1]) ? $query[1] : 0;
        } else {
            $total['saida'] = !empty($query[0]) ? $query[0] : 0;
            $total['entrada'] = !empty($query[1]) ? $query[1] : 0;
        }

        return $total;

    }

}
