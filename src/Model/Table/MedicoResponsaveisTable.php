<?php
namespace App\Model\Table;

use App\Model\Entity\MedicoResponsavei;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Routing\Router;

/**
 * MedicoResponsaveis Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class MedicoResponsaveisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('medico_responsaveis');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Proffer.Proffer', [
            'caminho' => [    // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'caminho_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 100, // Width
                        'h' => 100, // Height
                        'crop' => true , // Crop will crop the image as well as resize it
                        'jpeg_quality'  => 100,
                        'png_compression_level' => 9
                    ],
                    'portrait' => [     // Define a second thumbnail
                        'w' => 60,
                        'h' => 60
                    ],
                ]
            ]
        ]);

        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ClienteHistoricos', [
            'foreignKey' => 'medico_id',
            'joinType' => 'LEFT'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('conselho');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('sigla');

        $validator
            ->requirePresence('nome_laudo', 'create')
            ->notEmpty('nome_laudo');

        $validator
            ->allowEmpty('conselho_laudo');

        $validator
            ->allowEmpty('cpf');

        $validator
            ->boolean('cadastra_agenda')
            ->allowEmpty('cadastra_agenda');

        return $validator;
    }
    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if($outer->controller != 'ClienteHistoricos')
              $queryData->where(['MedicoResponsaveis.situacao_id' => 1]);
        
        $queryData->orderAsc('MedicoResponsaveis.nome');
      return $queryData;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }
    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        if(!empty($entity->data)){
            $entity->data = implode('-',array_reverse(explode('/',$entity->data)));
        }
        return true;
    }

    public function getNomeLaudoList(){
        $dados = $this->find()
            ->select(['id'=>'id'])
            ->select(['text'=>'nome_laudo'])
            ->where(['situacao_id' => 1]);
        $result=[];
        foreach ($dados as $dado) {
            $result[$dado->id] = $dado->text;
        }
        return $result;

    }

    /**
     * Lista para os campos select
     *
     * @return void
     */
    public function getList()
    {
        $query = $this->find('list')->where(['MedicoResponsaveis.situacao_id' => 1]);
        return $query;
    }
}
