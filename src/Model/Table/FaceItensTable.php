<?php
namespace App\Model\Table;

use App\Model\Entity\FaceIten;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * FaceItens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoItens
 * @property \Cake\ORM\Association\BelongsTo $Faces
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 */
class FaceItensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('face_itens');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AtendimentoItens', [
            'foreignKey' => 'iten_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Faces', [
            'foreignKey' => 'face_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Financeiro', [
            'className'=>'MedicoResponsaveis',
            'foreignKey' => 'financeiro_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Executor', [
            'className'=>'MedicoResponsaveis',
            'foreignKey' => 'executor_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('User_Financeiro', [
            'className'=>'Users',
            'foreignKey' => 'user_financeiro',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('User_Executor', [
            'className'=>'Users',
            'foreignKey' => 'user_executor',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['iten_id'], 'AtendimentoItens'));
        $rules->add($rules->existsIn(['face_id'], 'Faces'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
            $entity->user_id = $_SESSION['Auth']['User']['id'];
        }

        return true;
    }
}
