<?php
namespace App\Model\Table;

use App\Model\Entity\FaturaRecurso;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FaturaRecursos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SituacaoFaturaRecursos
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class FaturaRecursosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fatura_recursos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SituacaoFaturaRecursos', [
            'foreignKey' => 'situacao_fatura_recurso_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AtendimentoProcedimentos', [
            'foreignKey' => 'atendimento_procedimento_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('data');

        $validator
            ->decimal('valor')
            ->allowEmpty('valor');

        $validator
            ->allowEmpty('obs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['situacao_fatura_recurso_id'], 'SituacaoFaturaRecursos'));
        $rules->add($rules->existsIn(['atendimento_procedimento_id'], 'AtendimentoProcedimentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FaturaRecursos') {
              $queryData->where(['FaturaRecursos.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
                $entity->situacao_id = 1;
          }
        
        if (!empty($entity->data)) {
         $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
       }
        return true;
    }
}
