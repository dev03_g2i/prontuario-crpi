<?php
namespace App\Model\Table;

use App\Model\Entity\FinContasReceber;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * FinContasReceber Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FinPlanoContas
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $FinContabilidades
 * @property \Cake\ORM\Association\BelongsTo $FinContasReceberPlanejamentos
 * @property \Cake\ORM\Association\BelongsTo $Faturas
 * @property \Cake\ORM\Association\HasMany $FinAbatimentosContasReceber

 */
class FinContasReceberTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_contas_receber');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FinPlanoContas', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsTo('FinContabilidades', [
            'foreignKey' => 'fin_contabilidade_id',
        ]);
        $this->belongsTo('FinContasReceberPlanejamentos', [
            'foreignKey' => 'fin_planejamento_id'
        ]);
        $this->hasMany('FinAbatimentosContasReceber', [
            'foreignKey' => 'fin_contas_receber_id'
        ]);
        $this->belongsTo('FinTipoPagamento', [
            'foreignKey' => 'fin_tipo_pagamento_id'
        ]);
        $this->belongsTo('FinTipoDocumento', [
            'foreignKey' => 'fin_tipo_documento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->requirePresence('data')
            ->notEmpty('data');

        $validator
            ->decimal('valor')
            ->allowEmpty('valor');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 255)
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('data_pagamento');

        $validator
            ->allowEmpty('vencimento');

        $validator
            ->scalar('documento')
            ->maxLength('documento', 20)
            ->allowEmpty('documento');

        $validator
            ->scalar('numdoc')
            ->maxLength('numdoc', 20)
            ->allowEmpty('numdoc');

        $validator
            ->scalar('parcela')
            ->maxLength('parcela', 20)
            ->allowEmpty('parcela');

        $validator
            ->integer('forma_pagamento')
            ->allowEmpty('forma_pagamento');

        $validator
            ->integer('boleto_mensagem')
            ->allowEmpty('boleto_mensagem');

        $validator
            ->scalar('boleto_mensagem_livre')
            ->maxLength('boleto_mensagem_livre', 200)
            ->allowEmpty('boleto_mensagem_livre');

        $validator
            ->scalar('boleto_barras')
            ->maxLength('boleto_barras', 30)
            ->allowEmpty('boleto_barras');

        $validator
            ->scalar('boletolinhadigitavel')
            ->maxLength('boletolinhadigitavel', 30)
            ->allowEmpty('boletolinhadigitavel');

        $validator
            ->scalar('boleto_nosso_numero')
            ->maxLength('boleto_nosso_numero', 30)
            ->allowEmpty('boleto_nosso_numero');

        $validator
            ->numeric('juros')
            ->allowEmpty('juros');

        $validator
            ->numeric('multa')
            ->allowEmpty('multa');

        $validator
            ->integer('boleto_cedente')
            ->allowEmpty('boleto_cedente');

        $validator
            ->scalar('numero_fiscal')
            ->maxLength('numero_fiscal', 50)
            ->allowEmpty('numero_fiscal');

        $validator
            ->allowEmpty('data_fiscal');

        $validator
            ->decimal('valor_bruto')
            ->allowEmpty('valor_bruto');

        $validator
            ->decimal('desconto')
            ->requirePresence('desconto')
            ->notEmpty('desconto');

        $validator
            ->integer('correcao_monetaria')
            ->allowEmpty('correcao_monetaria');

        $validator
            ->scalar('numero_documento')
            ->maxLength('numero_documento', 100)
            ->allowEmpty('numero_documento');

        $validator
            ->allowEmpty('impresso');

        $validator
            ->decimal('valorPagar')
            ->allowEmpty('valorPagar');

        $validator
            ->allowEmpty('vencimento_boleto');

        $validator
            ->integer('perjuros')
            ->allowEmpty('perjuros');

        $validator
            ->integer('permulta')
            ->allowEmpty('permulta');

        $validator
            ->allowEmpty('data_cadastro');

        $validator
            ->integer('sis_antigo_cx_boleto')
            ->allowEmpty('sis_antigo_cx_boleto');

        $validator
            ->integer('sis_antigo_cx')
            ->allowEmpty('sis_antigo_cx');

        $validator
            ->integer('user_created')
            ->allowEmpty('user_created');

        $validator
            ->integer('user_modified')
            ->allowEmpty('user_modified');

        $validator
            ->integer('origem_registro')
            ->allowEmpty('origem_registro');

        $validator
            ->integer('fatura_id')
            ->allowEmpty('fatura_id');

        $validator
            ->integer('extern_id')
            ->allowEmpty('extern_id');

        $validator
            ->integer('controle_financeiro_id')
            ->allowEmpty('controle_financeiro_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fin_plano_conta_id'], 'FinPlanoContas'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['fin_contabilidade_id'], 'FinContabilidades'));
        $rules->add($rules->existsIn(['fin_planejamento_id'], 'FinContasReceberPlanejamentos'));
        $rules->add($rules->existsIn(['fin_tipo_documento_id'], 'FinTipoDocumento'));
        $rules->add($rules->existsIn(['fin_tipo_pagamento_id'], 'FinTipoPagamento'));

        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FinContasReceber') {
        if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
     $queryData->where(['FinContasReceber.situacao' => 1]);
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if (!is_object($entity->data)) {
         $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
       }
        if (!is_object($entity->data_pagamento)) {
         $entity->data_pagamento = implode('-', array_reverse(explode('/', $entity->data_pagamento)));
       }
        if (!is_object($entity->vencimento)) {
         $entity->vencimento = implode('-', array_reverse(explode('/', $entity->vencimento)));
       }
        if (!is_object($entity->data_fiscal)) {
         $entity->data_fiscal = implode('-', array_reverse(explode('/', $entity->data_fiscal)));
       }
        if (!is_object($entity->vencimento_boleto)) {
            $entity->vencimento_boleto = implode('-', array_reverse(explode('/', $entity->vencimento_boleto)));
        }
        if(!is_object($entity->data_cadastro)){
            $inicio = new DateTime();

            $now = $inicio->format('Y-m-d H:i:s');
            $entity->data_cadastro = $now;
        }
         return true;
    }

    public function getBillsToReceive($date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->format('m');
        $year = $now->format('Y');
        
        $resultBills = $this->find()
        ->select(['contasReceber' => 'TRUNCATE(SUM(valor + ((valor * (juros/100)) 
        + (valor * (multa/100)) - (valor * (desconto/100)))), 2)',
        'dia' => "IF(DAY(vencimento) < 10, CONCAT('0', DAY(vencimento)), DAY(vencimento))"])
        ->where(['MONTH(vencimento)' => $month, 'YEAR(vencimento)' => $year, 'data_pagamento IS NULL'])
        ->group(['vencimento']);

        return $resultBills;
    }

    public function getBillsToReceiveForModal($date = null)
    {
        $resultBills = $this->find()
        ->contain(['Clientes', 'FinPlanoContas', 'FinContabilidades', 'FinTipoDocumento'])
        ->select([
            'Vencimento' => 'IF(FinContasReceber.vencimento, DATE_FORMAT(FinContasReceber.vencimento, "%d/%m/%Y"), "")',
            'Fornecedor' => 'IF(FinContasReceber.cliente_id, Clientes.nome, "")',
            'Plano de contas' => 'IF(FinContasReceber.fin_plano_conta_id, FinPlanoContas.nome, "")',
            'Empresas' => 'IF(FinContasReceber.fin_contabilidade_id, FinContabilidades.nome, "")',
            'Tipo documento' => 'IF(FinContasReceber.fin_tipo_documento_id, FinTipoDocumento.descricao, "")',
            'Parcela' => 'IF(FinContasReceber.parcela, FinContasReceber.parcela, "")',
            'Complemento' => 'IF(FinContasReceber.complemento, FinContasReceber.complemento, "")',
            'Valor bruto' => 'FORMAT(FinContasReceber.valor_bruto, 2, "de_DE")',
            'Valor c/ ded' => 'FORMAT(FinContasReceber.valor, 2, "de_DE")',
            'Data pgmt' => 'IF(FinContasReceber.data_pagamento, DATE_FORMAT(FinContasReceber.data_pagamento, "%d/%m/%Y"), "")'
            ])
        ->where(['FinContasReceber.vencimento' => $date, 'FinContasReceber.data_pagamento IS NULL'])
        ->order(['Clientes.nome']);

        return $resultBills;
    }
}
