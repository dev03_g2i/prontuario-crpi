<?php

namespace App\Model\Table;

use App\Model\Entity\FinContabilidade;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinContabilidades Model
 *
 * @property \Cake\ORM\Association\HasMany $FinContabilidadeBancos
 * @property \Cake\ORM\Association\HasMany $FinContasPagar
 * @property \Cake\ORM\Association\HasMany $FinContasPagarPlanejamentos
 * @property \Cake\ORM\Association\HasMany $FinContasReceberPlanejamentos
 * @property \Cake\ORM\Association\HasMany $FinGrupoContabilidades
 * @property \Cake\ORM\Association\HasMany $FinMovimentos
 * @property \Cake\ORM\Association\HasMany $FinPlanejamentos
 */
class FinContabilidadesTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_contabilidades');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('FinContabilidadeBancos', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->hasMany('FinContasPagar', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->hasMany('FinContasPagarPlanejamentos', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->hasMany('FinContasReceberPlanejamentos', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->hasMany('FinGrupoContabilidades', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->hasMany('FinMovimentos', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->hasMany('FinPlanejamentos', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 100)
            ->allowEmpty('nome');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 255)
            ->allowEmpty('descricao');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 50)
            ->allowEmpty('cep');

        $validator
            ->scalar('rua')
            ->maxLength('rua', 50)
            ->allowEmpty('rua');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 50)
            ->allowEmpty('bairro');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade', 50)
            ->allowEmpty('cidade');

        $validator
            ->scalar('uf')
            ->maxLength('uf', 2)
            ->allowEmpty('uf');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        return $validator;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'FinContabilidades') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        $queryData->andWhere(['FinContabilidades.situacao' => 1]);

        return $queryData;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if ($entity->isNew()) {
        }

        return true;
    }

    //Função que retorna todas as Contabilidades por Grupo.
    public function getCompaniesForGroup($idGroup = null)
    {
        $companies = $this->find('list')
            ->contain(['FinGrupoContabilidades']);

        if ($idGroup) {
            $companies->matching('FinGrupoContabilidades', function ($q) use ($idGroup) {
                return $q->where(['FinGrupoContabilidades.fin_grupo_id' => $idGroup]);
            });
        }

        return $companies;
    }
}