<?php
namespace App\Model\Table;

use App\Model\Entity\ClienteContrato;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * ClienteContratos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Atendimentos
 * @property \Cake\ORM\Association\BelongsTo $Contratos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $ClienteResponsaveis
 */
class ClienteContratosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cliente_contratos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'atendimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Contratos', [
            'foreignKey' => 'contrato_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ClienteResponsaveis', [
            'foreignKey' => 'responsavel_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('valor_total')
            ->allowEmpty('valor_total');

        $validator
            ->integer('meses')
            ->allowEmpty('meses');

        $validator
            ->decimal('valor_parcela')
            ->allowEmpty('valor_parcela');

        $validator
            ->integer('vencimento')
            ->allowEmpty('vencimento');

        $validator
            ->allowEmpty('primeiro_pagamento');

        $validator
            ->notEmpty('data_contrato');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atendimento_id'], 'Atendimentos'));
        $rules->add($rules->existsIn(['contrato_id'], 'Contratos'));
        $rules->add($rules->existsIn(['responsavel_id'], 'ClienteResponsaveis'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='ClienteContratos') {
              $queryData->where(['ClienteContratos.situacao_id' => 1]);
    if (!empty($outer->query)) {

           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    $data = explode('__',$key);
                    $outer->query[$data[1]] = $value;
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
                  if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
         }
        if (!empty($entity->primeiro_pagamento)) {
         $entity->primeiro_pagamento = implode('-', array_reverse(explode('/', $entity->primeiro_pagamento)));
       }
       if (!empty($entity->data_contrato)) {
         $entity->data_contrato = implode('-', array_reverse(explode('/', $entity->data_contrato)));
       }
        return true;
    }
}
