<?php
namespace App\Model\Table;

use App\Model\Entity\ListaAnexo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validation;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * ListaAnexos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ClienteAnexos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class ListaAnexosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lista_anexos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Proffer.Proffer', [
            'caminho' => [    // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'caminho_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 100, // Width
                        'h' => 100, // Height
                        'crop' => true , // Crop will crop the image as well as resize it
                'jpeg_quality'  => 100,
                'png_compression_level' => 9
            ],
            'portrait' => [     // Define a second thumbnail
                'w' => 60,
                'h' => 60
            ],
        ]
        ]
    ]);
        $this->belongsTo('ClienteAnexos', [
            'foreignKey' => 'anexo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('caminho', 'create')
            ->allowEmpty('caminho', 'update');

        $validator
            ->add('caminho',[
                'extension'=>[
                    'rule'=>[
                        'extension', ['jpeg', 'png', 'jpg','pdf','txt','rar']
                    ],
                    'message' => 'Formato inválido'
                ]
            ]);
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['anexo_id'], 'ClienteAnexos'));

        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }

        return true;
    }
}
