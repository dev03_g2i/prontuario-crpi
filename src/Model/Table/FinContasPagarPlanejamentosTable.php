<?php
namespace App\Model\Table;

use App\Model\Entity\FinContasPagarPlanejamento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinContasPagarPlanejamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $FinFornecedors
 * @property \Cake\ORM\Association\BelongsTo $FinContabilidades
 * @property \Cake\ORM\Association\BelongsTo $FinPlanoContas
 * @property \Cake\ORM\Association\HasMany $FinContasPagar

 */
class FinContasPagarPlanejamentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_contas_pagar_planejamentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->belongsTo('FinFornecedors', [
            'foreignKey' => 'fin_fornecedor_id'
        ]);
        $this->belongsTo('FinContabilidades', [
            'foreignKey' => 'fin_contabilidade_id'
        ]);
        $this->belongsTo('FinPlanoContas', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->hasMany('FinContasPagar', [
            'foreignKey' => 'fin_contas_pagar_planejamento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->integer('periodicidade')
            ->allowEmpty('periodicidade');

        $validator
            ->integer('plazo_determinado')
            ->allowEmpty('plazo_determinado');

        $validator
            ->decimal('valor_parcela')
            ->allowEmpty('valor_parcela');

        $validator
            ->scalar('complemento')
            ->allowEmpty('complemento');

        $validator
            ->integer('dia_vencimento')
            ->allowEmpty('dia_vencimento');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->integer('a_vista')
            ->allowEmpty('a_vista');

        $validator
            ->integer('correcao_monetaria')
            ->allowEmpty('correcao_monetaria');

        $validator
            ->allowEmpty('proxima_geracao');

        $validator
            ->allowEmpty('ultima_geracao');

        $validator
            ->integer('geradas')
            ->allowEmpty('geradas');

        $validator
            ->integer('tipo_pgmt')
            ->allowEmpty('tipo_pgmt');

        $validator
            ->integer('tipo_dcmt')
            ->allowEmpty('tipo_dcmt');

        $validator
            ->allowEmpty('ultimo_gerado');

        $validator
            ->integer('mes_vencimento')
            ->allowEmpty('mes_vencimento');

        $validator
            ->requirePresence('ultima_atualizacao')
            ->notEmpty('ultima_atualizacao');

        $validator
            ->allowEmpty('data_documento');

        $validator
            ->allowEmpty('primeiro_vencimento');

        $validator
            ->scalar('mumero_documento')
            ->maxLength('mumero_documento')
            ->allowEmpty('mumero_documento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['fin_fornecedor_id'], 'FinFornecedors'));
        $rules->add($rules->existsIn(['fin_contabilidade_id'], 'FinContabilidades'));
        $rules->add($rules->existsIn(['fin_plano_conta_id'], 'FinPlanoContas'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FinContasPagarPlanejamentos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if (!is_object($entity->proxima_geracao)) {
         $entity->proxima_geracao = implode('-', array_reverse(explode('/', $entity->proxima_geracao)));
       }
        if (!is_object($entity->ultima_geracao)) {
         $entity->ultima_geracao = implode('-', array_reverse(explode('/', $entity->ultima_geracao)));
       }
        if (!is_object($entity->ultimo_gerado)) {
         $entity->ultimo_gerado = implode('-', array_reverse(explode('/', $entity->ultimo_gerado)));
       }
        if(!is_object($entity->ultima_atualizacao)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->ultima_atualizacao);
           $entity->ultima_atualizacao = $now;
        }
         if (!is_object($entity->data_documento)) {
         $entity->data_documento = implode('-', array_reverse(explode('/', $entity->data_documento)));
       }
        if (!is_object($entity->primeiro_vencimento)) {
         $entity->primeiro_vencimento = implode('-', array_reverse(explode('/', $entity->primeiro_vencimento)));
       }
        return true;
    }
}
