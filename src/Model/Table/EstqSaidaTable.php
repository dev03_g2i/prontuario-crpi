<?php
namespace App\Model\Table;

use App\Model\Entity\EstqSaida;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * EstqSaida Model
 *
 * @property \Cake\ORM\Association\BelongsTo $EstqTipoMovimento
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Atendimentos

 */
class EstqSaidaTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('estq_saida');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('EstqArtigos', [
            'foreignKey' => 'artigo_id'
        ]);
        $this->belongsTo('EstqTipoMovimento', [
            'foreignKey' => 'tipo_movimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('AtendimentoProcedimentos', [
            'foreignKey' => 'atendimento_procedimento_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'atendimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->requirePresence('data')
            ->notEmpty('data');

        $validator
            ->integer('quantidade')
            ->allowEmpty('quantidade');

        $validator
            ->decimal('vl_custos')
            ->allowEmpty('vl_custos');

        $validator
            ->decimal('vl_venda')
            ->allowEmpty('vl_venda');

        $validator
            ->decimal('vl_custo_medico')
            ->allowEmpty('vl_custo_medico');

        $validator
            ->integer('origin')
            ->allowEmpty('origin');

        $validator
            ->allowEmpty('codigo_tiss');

        $validator
            ->allowEmpty('codigo_tuss');

        $validator
            ->allowEmpty('codigo_convenio');

        $validator
            ->decimal('total')
            ->allowEmpty('total');

        $validator
            ->integer('qtd_recebida')
            ->allowEmpty('qtd_recebida');

        $validator
            ->decimal('vl_recebido')
            ->allowEmpty('vl_recebido');

        $validator
            ->allowEmpty('dt_recebimento');

        $validator
            ->integer('exportado_fatura')
            ->allowEmpty('exportado_fatura');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['artigo_id'], 'EstqArtigos'));
        $rules->add($rules->existsIn(['tipo_movimento_id'], 'EstqTipoMovimento'));
        $rules->add($rules->existsIn(['atendimento_procedimento_id'], 'AtendimentoProcedimentos'));
        $rules->add($rules->existsIn(['atendimento_id'], 'Atendimentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='EstqSaida') {
              $queryData->where(['EstqSaida.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
                $entity->situacao_id = 1;
                $entity->tipo_movimento_id = 1;// Saida
          }
        
        if (!is_object($entity->data)) {
         $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
       }
        if (!is_object($entity->dt_recebimento)) {
         $entity->dt_recebimento = implode('-', array_reverse(explode('/', $entity->dt_recebimento)));
       }
        return true;
    }
}
