<?php
namespace App\Model\Table;

use App\Model\Entity\NfTomadore;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * NfTomadores Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes

 */
class NfTomadoresTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('nf_tomadores');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('razao_social')
            ->maxLength('razao_social')
            ->allowEmpty('razao_social');

        $validator
            ->scalar('inscricao_municipal')
            ->maxLength('inscricao_municipal')
            ->allowEmpty('inscricao_municipal');

        $validator
            ->scalar('cpfcnpj')
            ->maxLength('cpfcnpj')
            ->allowEmpty('cpfcnpj');

        $validator
            ->scalar('doc_tomador_estrangeiro')
            ->maxLength('doc_tomador_estrangeiro')
            ->allowEmpty('doc_tomador_estrangeiro');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('ddd')
            ->maxLength('ddd')
            ->allowEmpty('ddd');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone')
            ->allowEmpty('telefone');

        $validator
            ->scalar('texto_nf')
            ->allowEmpty('texto_nf');

        $validator
            ->integer('tipo_tomador')
            ->allowEmpty('tipo_tomador');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='NfTomadores') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
         }
        
        return true;
    }
}
