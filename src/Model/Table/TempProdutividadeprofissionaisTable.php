<?php
namespace App\Model\Table;

use App\Model\Entity\TempProdutividadeprofissionai;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * TempProdutividadeprofissionais Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Atendimentos
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $Situacaos

 */
class TempProdutividadeprofissionaisTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('temp_produtividadeprofissionais');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'atendimento_id'
        ]);
        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id'
        ]);
        $this->belongsTo('Situacaos', [
            'foreignKey' => 'situacao_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('convenio');

        $validator
            ->allowEmpty('profissional');

        $validator
            ->allowEmpty('data_atendimento');

        $validator
            ->time('hora')
            ->allowEmpty('hora');

        $validator
            ->allowEmpty('paciente');

        $validator
            ->allowEmpty('codigo');

        $validator
            ->allowEmpty('procedimento');

        $validator
            ->integer('quantidade')
            ->allowEmpty('quantidade');

        $validator
            ->decimal('valor_fatura')
            ->allowEmpty('valor_fatura');

        $validator
            ->decimal('valor_caixa')
            ->allowEmpty('valor_caixa');

        $validator
            ->decimal('valor_material')
            ->allowEmpty('valor_material');

        $validator
            ->decimal('total')
            ->allowEmpty('total');

        $validator
            ->integer('perc_recebimento')
            ->allowEmpty('perc_recebimento');

        $validator
            ->integer('perc_imposto')
            ->allowEmpty('perc_imposto');

        $validator
            ->decimal('valor_recebido_fatura')
            ->allowEmpty('valor_recebido_fatura');

        $validator
            ->decimal('valor_recebido_caixa')
            ->allowEmpty('valor_recebido_caixa');

        $validator
            ->decimal('valor_recebido_convenio')
            ->allowEmpty('valor_recebido_convenio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atendimento_id'], 'Atendimentos'));
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='TempProdutividadeprofissionais') {
              $queryData->where(['TempProdutividadeprofissionais.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
          }
        
        if (!empty($entity->data_atendimento)) {
         $entity->data_atendimento = implode('-', array_reverse(explode('/', $entity->data_atendimento)));
       }
        return true;
    }
}
