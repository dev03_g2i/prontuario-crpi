<?php
namespace App\Model\Table;

use App\Model\Entity\ClienteResponsavei;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * ClienteResponsaveis Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $GrauParentescos
 * @property \Cake\ORM\Association\BelongsTo $EstadoCivis
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class ClienteResponsaveisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cliente_responsaveis');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GrauParentescos', [
            'foreignKey' => 'grau_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('EstadoCivis', [
            'foreignKey' => 'estadocivil_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('nascionalidade');

        $validator
            ->allowEmpty('profissao');

        $validator
            ->allowEmpty('rg');

        $validator
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf');

        $validator
            ->allowEmpty('cep');

        $validator
            ->allowEmpty('endereco');

        $validator
            ->requirePresence('grau_id', 'create')
            ->notEmpty('grau_id');

        $validator
            ->allowEmpty('estadocivil_id');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('cidade');

        $validator
            ->allowEmpty('estado');

        $validator
            ->allowEmpty('telefone');

        $validator
            ->allowEmpty('celular');

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->boolean('financeiro')
            ->allowEmpty('financeiro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['grau_id'], 'GrauParentescos'));
        $rules->add($rules->existsIn(['estadocivil_id'], 'EstadoCivis'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        return true;
    }

    public function getList()
    {
        return $this->find('list')->where(['ClienteResponsaveis.situacao_id' => 1]);
    }
}
