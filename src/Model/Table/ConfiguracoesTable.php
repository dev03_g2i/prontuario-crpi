<?php

namespace App\Model\Table;

use App\Model\Entity\Configuraco;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Configuracoes Model
 *

 */
class ConfiguracoesTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('configuracoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('logo');

        $validator
            ->allowEmpty('logo_dir');

        $validator
            ->requirePresence('nome_login', 'create')
            ->notEmpty('nome_login');

        $validator
            ->allowEmpty('icone');

        $validator
            ->allowEmpty('icone_dir');

        $validator
            ->boolean('imagem')
            ->allowEmpty('imagem');

        $validator
            ->allowEmpty('url_imagens');

        $validator
            ->integer('usa_arquivo')
            ->allowEmpty('usa_arquivo');

        $validator
            ->integer('arquivo')
            ->allowEmpty('arquivo');

        $validator
            ->integer('restringe_cpf')
            ->allowEmpty('restringe_cpf');

        $validator
            ->allowEmpty('duplica_cpf');

        $validator
            ->integer('idade_cpf')
            ->allowEmpty('idade_cpf');

        return $validator;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'Configuracoes') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if ($entity->isNew()) {
        }

        return true;
    }

    public function getFirstConfiguracao()
    {
        return $this->find('all')
            ->first();
    }
}
