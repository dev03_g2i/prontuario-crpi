<?php
namespace App\Model\Table;

use App\Model\Entity\FaturaEncerramento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * FaturaEncerramentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $SituacaoFaturaencerramentos
 * @property \Cake\ORM\Association\HasMany $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\HasMany $TempFaturamentos

 */
class FaturaEncerramentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fatura_encerramentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserEdit', [
            'className'=>'Users',
            'foreignKey' => 'user_update',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Unidades', [
            'foreignKey' => 'unidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoFaturaencerramentos', [
            'foreignKey' => 'situacao_faturaencerramento_id'
        ]);
        $this->hasMany('AtendimentoProcedimentos', [
            'foreignKey' => 'fatura_encerramento_id'
        ]);
        $this->hasMany('TempFaturamentos', [
            'foreignKey' => 'fatura_encerramento_id'
        ]);
        $this->hasMany('FaturaNfs', [
            'foreignKey' => 'fatura_encerramento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('competencia');

        $validator
            ->allowEmpty('vencimento');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->allowEmpty('data_encerramento');

        $validator
            ->decimal('total_bruto')
            ->allowEmpty('total_bruto');

        $validator
            ->integer('num_exames')
            ->allowEmpty('num_exames');

        $validator
            ->allowEmpty('unidade');

        $validator
            ->decimal('taxas')
            ->allowEmpty('taxas');

        $validator
            ->decimal('descontos')
            ->allowEmpty('descontos');

        $validator
            ->integer('qtd_total')
            ->allowEmpty('qtd_total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        $rules->add($rules->existsIn(['situacao_faturaencerramento_id'], 'SituacaoFaturaencerramentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if($outer->controller=='FaturaEncerramentos') {
            $queryData->where(['FaturaEncerramentos.situacao_id' => 1]);
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if(substr_count($value, '/')){
                            if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        }else{
                            $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                        }
                    }
                }
            }
        }
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            if (!empty($_SESSION['Auth']['User']['id'])) {
                $entity->user_id = $_SESSION['Auth']['User']['id'];
            }
            $entity->situacao_id = 1;
        }

        if(!empty($entity->qtd_total)){
            $entity->num_exames = $entity->qtd_total;
        }

        if(!empty($entity->vencimento)){
            if(!is_object($entity->vencimento)){
                $inicio = new DateTime();
                $entity->vencimento = $inicio->createFromFormat('d/m/Y H:i', $entity->vencimento);
            }
        }
        if(!empty($entity->data_encerramento)){
            if(!is_object($entity->data_encerramento)){
                $inicio = new DateTime();
                $entity->data_encerramento = $inicio->createFromFormat('d/m/Y H:i', $entity->data_encerramento);
            }
        }
        if(!empty($entity->data_quitacao)){
            if(!is_object($entity->data_quitacao)){
                $inicio = new DateTime();
                $entity->data_quitacao = $inicio->createFromFormat('d/m/Y', $entity->data_quitacao);
            }
        }
        return true;
    }

    public function getInvoicesToReceive($date = null)
    {
        if (!empty($date))
            $now = new Time($date);
        else
            $now = new Time();

        $month = $now->format('m');
        $year = $now->format('Y');
        
        $resultInvoicesBills = $this->find()->contain(['Convenios'])
        ->select(['contasReceberConvenio' => 'SUM(total_bruto - (total_bruto * (Convenios.total_imposto/100)))', 
        'dia' => "IF(DAY(vencimento) < 10, CONCAT('0', DAY(vencimento)), DAY(vencimento))"])
        ->where(['MONTH(FaturaEncerramentos.vencimento)' => $month, 'YEAR(FaturaEncerramentos.vencimento)' => $year, 'FaturaEncerramentos.data_quitacao IS NULL'])
        ->group(['DAY(FaturaEncerramentos.vencimento)', 'MONTH(FaturaEncerramentos.vencimento)', 'YEAR(FaturaEncerramentos.vencimento)'])
        ->order(['FaturaEncerramentos.vencimento']);

        return $resultInvoicesBills;
    }

    public function getInvoicesToReceiveForModal($date)
    {   
        $resultBills = $this->find()
        ->contain(['Convenios'])
        ->select([
            'Vencimento' => 'IF(FaturaEncerramentos.vencimento, DATE_FORMAT(FaturaEncerramentos.vencimento, "%d/%m/%Y"), "")',
            'Competencia' => 'IF(FaturaEncerramentos.competencia_ano and FaturaEncerramentos.competencia_mes, CONCAT(FaturaEncerramentos.competencia_mes, "-", FaturaEncerramentos.competencia_ano), "")',
            'Convenio' => 'IF(FaturaEncerramentos.convenio_id, Convenios.nome, "")',
            'Valor bruto' => 'FORMAT(FaturaEncerramentos.total_bruto, 2, "de_DE")',
            'Valor previsto' => 'FORMAT(SUM(total_bruto - (total_bruto * (Convenios.total_imposto/100))), 2, "de_DE")'
        ])
        ->where(['FaturaEncerramentos.vencimento' => $date, 'FaturaEncerramentos.data_quitacao IS NULL'])
        ->order(['FaturaEncerramentos.vencimento']);
        
        return $resultBills;
    }
}
