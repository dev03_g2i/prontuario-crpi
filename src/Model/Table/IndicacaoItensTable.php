<?php
namespace App\Model\Table;

use App\Model\Entity\IndicacaoIten;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * IndicacaoItens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Indicacao
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class IndicacaoItensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('indicacao_itens');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Indicacao', [
            'foreignKey' => 'indicacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('cep');

        $validator
            ->allowEmpty('endereco');

        $validator
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('bairro');

        $validator
            ->allowEmpty('cidade');

        $validator
            ->allowEmpty('estado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['indicacao_id'], 'Indicacao'));
        return $rules;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'IndicacaoItens') {
            $queryData->where(['IndicacaoItens.situacao_id' => 1]);
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                        }

                    }
                }
            }
        }
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if ($entity->isNew()) {
            $entity->situacao_id = 1;
            if (!empty($_SESSION['Auth']['User']['id'])) {
                $entity->user_id = $_SESSION['Auth']['User']['id'];
            }
        }
        return true;
    }
}
