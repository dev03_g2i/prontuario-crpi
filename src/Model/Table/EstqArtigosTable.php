<?php
namespace App\Model\Table;

use App\Model\Entity\EstqArtigo;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * EstqArtigos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class EstqArtigosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('estq_artigos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('FaturaPrecoartigo', [
            'foreignKey' => 'artigo_id'
        ]);
        $this->hasMany('FaturaKitartigo', [
            'foreignKey' => 'artigo_id'
        ]);
        $this->hasMany('EstqKitArtigo', [
            'foreignKey' => 'artigo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('subgrupo')
            ->allowEmpty('subgrupo');

        $validator
            ->integer('controla_lote')
            ->allowEmpty('controla_lote');

        $validator
            ->allowEmpty('nome');

        $validator
            ->decimal('vlcomercial')
            ->allowEmpty('vlcomercial');

        $validator
            ->decimal('vlcusto')
            ->allowEmpty('vlcusto');

        $validator
            ->decimal('vlcustomedio')
            ->allowEmpty('vlcustomedio');

        $validator
            ->integer('unidade')
            ->allowEmpty('unidade');

        $validator
            ->allowEmpty('tamanho');

        $validator
            ->allowEmpty('usuario_dt');

        $validator
            ->integer('origem')
            ->allowEmpty('origem');

        $validator
            ->allowEmpty('codigo_livre');

        $validator
            ->boolean('ensaio')
            ->allowEmpty('ensaio');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->decimal('valor_repasse')
            ->allowEmpty('valor_repasse');

        $validator
            ->boolean('utiliza_repasse')
            ->allowEmpty('utiliza_repasse');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='EstqArtigos') {
              $queryData->where(['EstqArtigos.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
                $entity->situacao_id = 1;
          }
        
        if(!empty($entity->usuario_dt)){
           if(!is_object($entity->usuario_dt)){
               $inicio = new DateTime();
               $now = $inicio->createFromFormat('d/m/Y H:i',$entity->usuario_dt);
               $entity->usuario_dt = $now;
           }
        }
         return true;
    }
}
