<?php
namespace App\Model\Table;

use App\Model\Entity\Procedimento;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * Procedimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $GrupoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\HasMany $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\HasMany $PrecoProcedimentos
 */
class ProcedimentosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('procedimentos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('GrupoProcedimentos', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('AtendimentoProcedimentos', [
            'foreignKey' => 'procedimento_id'
        ]);
        $this->hasMany('ItenCobrancas', [
            'foreignKey' => 'procedimento_id'
        ]);
        $this->hasMany('PrecoProcedimentos', [
            'foreignKey' => 'procedimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['grupo_id'], 'GrupoProcedimentos'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        if(!empty($entity->data)){
            $entity->data = implode('-',array_reverse(explode('/',$entity->data)));
        }
        return true;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller=='Procedimentos') {
            $queryData->orderAsc('Procedimentos.nome');
        }
        return $queryData;
    }

    /** listagem para selects
     * @return $this
     */
    public function getList()
    {
        $query = $this->find('list')->where(['situacao_id' => 1])->orderAsc('nome');
        return $query;
    }
}
