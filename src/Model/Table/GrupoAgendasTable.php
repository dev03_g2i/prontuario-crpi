<?php
namespace App\Model\Table;

use App\Model\Entity\GrupoAgenda;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;

/**
 * GrupoAgendas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $MedicoResponsaveis
 */
class GrupoAgendasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('grupo_agendas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MedicoResponsaveis', [
            'foreignKey' => 'medico_id'
        ]);
        $this->belongsTo('Solicitantes', [
            'foreignKey' => 'solicitante_id'
        ]);
        $this->hasMany('AgendaPeriodos', [
            'foreignKey' => 'grupo_agenda_id'
        ]);
        $this->hasMany('AgendaBloqueios', [
            'foreignKey' => 'grupo_agenda_id'
        ]);
        $this->hasMany('GrupoAgendas', [
            'foreignKey' => 'grupo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['medico_id'], 'MedicoResponsaveis'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller!='GrupoAgendas') {
            $queryData->where(['GrupoAgendas.situacao_id' => 1]);
        }
        return $queryData;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if(!empty($_SESSION['Auth']['User']['id'])) {
                    $entity->user_id = $_SESSION['Auth']['User']['id'];
                }
            $entity->situacao_id = 1;
        }
        if(!empty($entity->data)){
            $entity->data = implode('-',array_reverse(explode('/',$entity->data)));
        }
        return true;
    }

    public function getList()
    {
        $query = $this->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        return $query;
    }
}
