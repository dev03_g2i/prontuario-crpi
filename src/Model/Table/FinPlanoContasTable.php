<?php

namespace App\Model\Table;

use App\Model\Entity\FinPlanoConta;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinPlanoContas Model
 *
 * @property \Cake\ORM\Association\HasMany $FinContasPagar
 * @property \Cake\ORM\Association\HasMany $FinContasPagarPlanejamentos
 * @property \Cake\ORM\Association\HasMany $FinContasReceber
 * @property \Cake\ORM\Association\HasMany $FinContasReceberPlanejamentos
 * @property \Cake\ORM\Association\HasMany $FinMovimentos
 * @property \Cake\ORM\Association\HasMany $FinPlanejamentos
 * @property \Cake\ORM\Association\BelongsTo $FinClassificacaoContas
 */
class FinPlanoContasTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_plano_contas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('FinContasPagar', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->hasMany('FinContasPagarPlanejamentos', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->hasMany('FinContasReceber', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->hasMany('FinContasReceberPlanejamentos', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->hasMany('FinMovimentos', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->hasMany('FinPlanejamentos', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
        $this->belongsTo('FinClassificacaoContas', [
            'foreignKey' => 'fin_classificacao_conta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->integer('fin_classificacao_conta_id')
            ->allowEmpty('fin_classificacao_conta_id');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->allowEmpty('nome');

        $validator
            ->integer('rateio')
            ->allowEmpty('rateio');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fin_classificacao_conta_id'], 'FinClassificacaoContas'));
        return $rules;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'FinPlanoContas') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        $queryData->andWhere(['FinPlanoContas.situacao' => 1]);
        return $queryData;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if ($entity->isNew()) {
        }

        return true;
    }
}
