<?php
namespace App\Model\Table;

use App\Model\Entity\Series;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Series Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Filesets

 */
class SeriesTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('series');
        $this->displayField('pk');
        $this->primaryKey('pk');

        $this->belongsTo('Study', [
            'foreignKey' => 'study_fk'
        ]);
        $this->hasMany('Instance', [
            'foreignKey' => 'series_fk'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('pk', 'create');

        $validator
            ->allowEmpty('study_fk');

        $validator
            ->allowEmpty('mpps_fk');

        $validator
            ->allowEmpty('inst_code_fk');

        $validator
            ->requirePresence('series_iuid', 'create')
            ->notEmpty('series_iuid')
            ->add('series_iuid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('series_no');

        $validator
            ->allowEmpty('modality');

        $validator
            ->allowEmpty('body_part');

        $validator
            ->allowEmpty('laterality');

        $validator
            ->allowEmpty('series_desc');

        $validator
            ->allowEmpty('institution');

        $validator
            ->allowEmpty('station_name');

        $validator
            ->allowEmpty('department');

        $validator
            ->allowEmpty('perf_physician');

        $validator
            ->allowEmpty('perf_phys_fn_sx');

        $validator
            ->allowEmpty('perf_phys_gn_sx');

        $validator
            ->allowEmpty('perf_phys_i_name');

        $validator
            ->allowEmpty('perf_phys_p_name');

        $validator
            ->allowEmpty('pps_start');

        $validator
            ->allowEmpty('pps_iuid');

        $validator
            ->allowEmpty('series_custom1');

        $validator
            ->allowEmpty('series_custom2');

        $validator
            ->allowEmpty('series_custom3');

        $validator
            ->integer('num_instances')
            ->requirePresence('num_instances', 'create')
            ->notEmpty('num_instances');

        $validator
            ->allowEmpty('src_aet');

        $validator
            ->allowEmpty('ext_retr_aet');

        $validator
            ->allowEmpty('retrieve_aets');

        $validator
            ->allowEmpty('fileset_iuid');

        $validator
            ->integer('availability')
            ->requirePresence('availability', 'create')
            ->notEmpty('availability');

        $validator
            ->integer('series_status')
            ->requirePresence('series_status', 'create')
            ->notEmpty('series_status');

        $validator
            ->allowEmpty('created_time');

        $validator
            ->allowEmpty('updated_time');

        $validator
            ->allowEmpty('series_attrs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['series_iuid']));
        $rules->add($rules->existsIn(['fileset_id'], 'Filesets'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Series') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'oviyam';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if(!empty($entity->pps_start)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->pps_start);
           $entity->pps_start = $now;
        }
         if(!empty($entity->created_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->created_time);
           $entity->created_time = $now;
        }
         if(!empty($entity->updated_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->updated_time);
           $entity->updated_time = $now;
        }
         return true;
    }
}
