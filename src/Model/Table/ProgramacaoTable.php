<?php
namespace App\Model\Table;

use App\Model\Entity\Programacao;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Programacao Model
 *
 * @property \Cake\ORM\Association\HasMany $Contasreceber
 * @property \Cake\ORM\Association\BelongsToMany $Deducoes
 */
class ProgramacaoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('programacao');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('Contasreceber', [
            'foreignKey' => 'programacao'
        ]);
        $this->belongsToMany('Deducoes', [
            'foreignKey' => 'programacao_id',
            'targetForeignKey' => 'deducoes_id',
            'joinTable' => 'deducoes_programacao'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('periodicidade')
            ->allowEmpty('periodicidade');

        $validator
            ->integer('prazoDeterminado')
            ->allowEmpty('prazoDeterminado');

        $validator
            ->decimal('valorParcela')
            ->allowEmpty('valorParcela');

        $validator
            ->integer('idCliente')
            ->allowEmpty('idCliente');

        $validator
            ->integer('idFornecedor')
            ->allowEmpty('idFornecedor');

        $validator
            ->integer('contabilidade')
            ->allowEmpty('contabilidade');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->integer('diaVencimento')
            ->allowEmpty('diaVencimento');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->integer('aVista')
            ->allowEmpty('aVista');

        $validator
            ->allowEmpty('tipo');

        $validator
            ->integer('idPlanoContas')
            ->allowEmpty('idPlanoContas');

        $validator
            ->integer('correcaoMonetaria')
            ->allowEmpty('correcaoMonetaria');

        $validator
            ->date('proximaGeracao')
            ->allowEmpty('proximaGeracao');

        $validator
            ->date('ultimaGeracao')
            ->allowEmpty('ultimaGeracao');

        $validator
            ->integer('geradas')
            ->allowEmpty('geradas');

        $validator
            ->integer('tipoPagamento')
            ->requirePresence('tipoPagamento', 'create')
            ->notEmpty('tipoPagamento');

        $validator
            ->integer('tipoDocumento')
            ->requirePresence('tipoDocumento', 'create')
            ->notEmpty('tipoDocumento');

        $validator
            ->date('ultimoGerado')
            ->allowEmpty('ultimoGerado');

        $validator
            ->integer('mesVencimento')
            ->allowEmpty('mesVencimento');

        $validator
            ->dateTime('ultima_atualizacao')
            ->requirePresence('ultima_atualizacao', 'create')
            ->notEmpty('ultima_atualizacao');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
