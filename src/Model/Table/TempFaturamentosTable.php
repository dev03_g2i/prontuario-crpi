<?php
namespace App\Model\Table;

use App\Model\Entity\TempFaturamento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * TempFaturamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Atendimentos
 * @property \Cake\ORM\Association\BelongsTo $Users

 */
class TempFaturamentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('temp_faturamentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'atendimento_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('selecionado')
            ->requirePresence('selecionado', 'create')
            ->notEmpty('selecionado');

        $validator
            ->allowEmpty('convenio');

        $validator
            ->allowEmpty('profissional');

        $validator
            ->allowEmpty('data_atendimento');

        $validator
            ->time('hora')
            ->allowEmpty('hora');

        $validator
            ->allowEmpty('paciente');

        $validator
            ->allowEmpty('matricula');

        $validator
            ->allowEmpty('codigo_tabela');

        $validator
            ->allowEmpty('procedimento');

        $validator
            ->allowEmpty('guia');

        $validator
            ->allowEmpty('senha');

        $validator
            ->integer('quantidade')
            ->allowEmpty('quantidade');

        $validator
            ->decimal('valor_fatura')
            ->allowEmpty('valor_fatura');

        $validator
            ->decimal('valor_material')
            ->allowEmpty('valor_material');

        $validator
            ->decimal('total')
            ->allowEmpty('total');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atendimento_id'], 'Atendimentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='TempFaturamentos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
         }
        
        if (!empty($entity->data_atendimento)) {
         $entity->data_atendimento = implode('-', array_reverse(explode('/', $entity->data_atendimento)));
       }
        return true;
    }
}
