<?php
namespace App\Model\Table;

use App\Model\Entity\Deduco;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Deducoes Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Contaspagar
 * @property \Cake\ORM\Association\BelongsToMany $Contasreceber
 * @property \Cake\ORM\Association\BelongsToMany $Programacao
 */
class DeducoesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('deducoes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Contaspagar', [
            'foreignKey' => 'deduco_id',
            'targetForeignKey' => 'contaspagar_id',
            'joinTable' => 'deducoes_contaspagar'
        ]);
        $this->belongsToMany('Contasreceber', [
            'foreignKey' => 'deduco_id',
            'targetForeignKey' => 'contasreceber_id',
            'joinTable' => 'deducoes_contasreceber'
        ]);
        $this->belongsToMany('Programacao', [
            'foreignKey' => 'deduco_id',
            'targetForeignKey' => 'programacao_id',
            'joinTable' => 'deducoes_programacao'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
