<?php
namespace App\Model\Table;

use App\Model\Entity\AtendimentoProcedimentoEquipe;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * AtendimentoProcedimentoEquipes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $MedicoResponsaveis
 * @property \Cake\ORM\Association\BelongsTo $TipoEquipes
 * @property \Cake\ORM\Association\BelongsTo $TipoEquipegrau
 * @property \Cake\ORM\Association\BelongsTo $ConfiguracaoApuracao
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class AtendimentoProcedimentoEquipesTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atendimento_procedimento_equipes');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AtendimentoProcedimentos', [
            'foreignKey' => 'atendimento_procedimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('MedicoResponsaveis', [
            'foreignKey' => 'profissional_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoEquipes', [
            'foreignKey' => 'tipo_equipe_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoEquipegrau', [
            'foreignKey' => 'tipo_equipegrau_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ConfiguracaoApuracao', [
            'foreignKey' => 'configuracao_apuracao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserReg', [
            'className'=>'Users',
            'foreignKey' => 'user_insert',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserAlt', [
            'className'=>'Users',
            'foreignKey' => 'user_update',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ordem')
            ->allowEmpty('ordem');

        $validator
            ->integer('porcentagem')
            ->allowEmpty('porcentagem');

        $validator
            ->scalar('obs')
            ->allowEmpty('obs');

        $validator
            ->integer('user_insert')
            ->notEmpty('user_insert');

        $validator
            ->integer('user_update')
            ->allowEmpty('user_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atendimento_procedimento_id'], 'AtendimentoProcedimentos'));
        $rules->add($rules->existsIn(['profissional_id'], 'MedicoResponsaveis'));
        $rules->add($rules->existsIn(['tipo_equipe_id'], 'TipoEquipes'));
        $rules->add($rules->existsIn(['tipo_equipegrau_id'], 'TipoEquipegrau'));
        $rules->add($rules->existsIn(['configuracao_apuracao_id'], 'ConfiguracaoApuracao'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='AtendimentoProcedimentoEquipes') {
              $queryData->where(['AtendimentoProcedimentoEquipes.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        
        if($entity->isNew()) {
            $entity->situacao_id = 1;
            $entity->user_insert = $_SESSION['Auth']['User']['id'];
        }

        if(!empty($entity->id)){/* Update */
            $entity->user_update = $_SESSION['Auth']['User']['id'];
        }
        
        return true;
    }

    /**
     * traz todas as equipes com situacao ativo
     *
     * @return void
     */
    public function findAllEquipes()
    {
        $query = $this->find('all')
        ->contain(['AtendimentoProcedimentos', 'MedicoResponsaveis', 'TipoEquipes', 'TipoEquipegrau', 'ConfiguracaoApuracao', 'SituacaoCadastros', 'UserReg', 'UserAlt'])
        ->where(['AtendimentoProcedimentoEquipes.situacao_id' => 1])->limit(1);
        return $query;
    }

    /**
     * Traz os dados do @class AtendimentoProcedimentos
     *
     * @param Integer $id
     * @return void
     */
    public function dadosAtendimento($id)
    {
        $dados_atendimento = $this->AtendimentoProcedimentos->get($id, [
            'contain' => ['Atendimentos' => ['Convenios', 'Clientes'], 'Procedimentos']
        ]);
        return $dados_atendimento;
    }
}
