<?php
namespace App\Model\Table;

use App\Model\Entity\NfPrestadore;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * NfPrestadores Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TipoLogradouros
 * @property \Cake\ORM\Association\BelongsTo $TipoBairros
 * @property \Cake\ORM\Association\BelongsTo $Cidades
 * @property \Cake\ORM\Association\BelongsTo $Tributacaos

 */
class NfPrestadoresTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('nf_prestadores');
        $this->displayField('razao_social');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TipoLogradouros', [
            'foreignKey' => 'tipo_logradouro_id'
        ]);
        $this->belongsTo('TipoBairros', [
            'foreignKey' => 'tipo_bairro_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->belongsTo('Tributacaos', [
            'foreignKey' => 'tributacao_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('razao_social')
            ->maxLength('razao_social')
            ->allowEmpty('razao_social');

        $validator
            ->scalar('inscricao_municipal')
            ->maxLength('inscricao_municipal')
            ->allowEmpty('inscricao_municipal');

        $validator
            ->scalar('cpfcnpj')
            ->maxLength('cpfcnpj')
            ->allowEmpty('cpfcnpj');

        $validator
            ->scalar('ddd')
            ->maxLength('ddd')
            ->allowEmpty('ddd');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone')
            ->allowEmpty('telefone');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('cpfcnpj_intermediario')
            ->maxLength('cpfcnpj_intermediario')
            ->allowEmpty('cpfcnpj_intermediario');

        $validator
            ->scalar('endereco')
            ->maxLength('endereco')
            ->allowEmpty('endereco');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento')
            ->allowEmpty('complemento');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro')
            ->allowEmpty('bairro');

        $validator
            ->scalar('cep')
            ->maxLength('cep')
            ->allowEmpty('cep');

        $validator
            ->boolean('atual')
            ->allowEmpty('atual');

        $validator
            ->scalar('cnae')
            ->maxLength('cnae')
            ->allowEmpty('cnae');

        $validator
            ->scalar('descricao_cnae')
            ->allowEmpty('descricao_cnae');

        $validator
            ->decimal('aliquota_atividade')
            ->allowEmpty('aliquota_atividade');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['tipo_logradouro_id'], 'TipoLogradouros'));
        $rules->add($rules->existsIn(['tipo_bairro_id'], 'TipoBairros'));
        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));
        $rules->add($rules->existsIn(['tributacao_id'], 'Tributacaos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='NfPrestadores') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }

    public function getList()
    {
        $query = $this->find('list')->where(['NfPrestadores.situacao_id' => 1]);
        return $query;
    }
}
