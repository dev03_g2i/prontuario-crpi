<?php
namespace App\Model\Table;

use App\Model\Entity\LogLaudo;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * LogLaudos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Laudos
 * @property \Cake\ORM\Association\BelongsTo $Users

 */
class LogLaudosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('log_laudos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Laudos', [
            'foreignKey' => 'laudo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserReg', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('UserAssinado', [
            'className' => 'Users',
            'foreignKey' => 'assinado_por'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('texto')
            ->allowEmpty('texto');

        $validator
            ->scalar('rtf')
            ->allowEmpty('rtf');

        $validator
            ->integer('assinado_por')
            ->allowEmpty('assinado_por');

        $validator
            ->allowEmpty('dt_assinatura');

        $validator
            ->scalar('texto_html')
            ->allowEmpty('texto_html');

        $validator
            ->integer('imagens')
            ->allowEmpty('imagens');

        $validator
            ->integer('filme')
            ->allowEmpty('filme');

        $validator
            ->integer('papel')
            ->allowEmpty('papel');

        $validator
            ->scalar('rtf_html')
            ->allowEmpty('rtf_html');

        $validator
            ->integer('paginas')
            ->allowEmpty('paginas');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['laudo_id'], 'Laudos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='LogLaudos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }

    /**
     * Insere um registro na tabela @class LogLaudos 
     * @param object $entity @class Laudos
     * @return void
     */
    public function insertLog($entity)
    {
        $log = $this->newEntity();
        $data = $entity->toArray();
        $log = $this->patchEntity($log, $data);
        $log->laudo_id = $entity->id;
        $this->save($log);
    }
}
