<?php
namespace App\Model\Table;

use App\Model\Entity\Cliente;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Routing\Router;
/**
 * Clientes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $MedicoResponsaveis
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $EstadoCivis
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Religiao
 * @property \Cake\ORM\Association\HasMany $Atendimentos
 * @property \Cake\ORM\Association\HasMany $ClienteAnexos
 * @property \Cake\ORM\Association\HasMany $ClienteDocumentos
 * @property \Cake\ORM\Association\HasMany $ClienteHistoricos
 * @property \Cake\ORM\Association\HasMany $ClienteResponsaveis
 */
class ClientesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('clientes');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Proffer.Proffer', [
            'caminho' => [    // The name of your upload field
                'root' => WWW_ROOT . 'files', // Customise the root upload folder here, or omit to use the default
                'dir' => 'caminho_dir',   // The name of the field to store the folder
                'thumbnailSizes' => [ // Declare your thumbnails
                    'square' => [   // Define the prefix of your thumbnail
                        'w' => 100, // Width
                        'h' => 100, // Height
                        'crop' => true , // Crop will crop the image as well as resize it
                        'jpeg_quality'  => 100,
                        'png_compression_level' => 9
                    ],
                    'portrait' => [     // Define a second thumbnail
                        'w' => 60,
                        'h' => 60
                    ],
                ]
            ]
        ]);

        $this->belongsTo('MedicoResponsaveis', [
            'foreignKey' => 'medico_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('EstadoCivis', [
            'foreignKey' => 'estadocivil_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('UserEdit', [
            'className'=>'Users',
            'foreignKey' => 'user_update',
            'joinType' => 'LEFT'
        ]);


        $this->belongsTo('Indicacao', [
            'foreignKey' => 'indicacao_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Religiao', [
            'foreignKey' => 'religiao_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('IndicacaoItens', [
            'foreignKey' => 'indicacao_itens_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('IndicacaoClientes', [
            'className'=>'Clientes',
            'foreignKey' => 'indicacao_paciente',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Atendimentos', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('ClienteAnexos', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('ClienteDocumentos', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('ClienteHistoricos', [
            'foreignKey' => 'cliente_id'
        ]);
        $this->hasMany('ClienteResponsaveis', [
            'foreignKey' => 'cliente_id',
            'conditions' =>['ClienteResponsaveis.situacao_id'=>1]
        ]);

        $this->hasMany('Agendas', [
            'foreignKey' => 'cliente_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('caminho',[
                'extension'=>[
                    'rule'=>[
                        'extension', ['jpeg', 'png', 'jpg']
                    ],
                    'message' => 'Formato inválido'
                ]
            ])
            ->allowEmpty('caminho');

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->requirePresence('sexo', 'create')
            ->notEmpty('sexo');

        $validator
            ->requirePresence('nascimento', 'create')
            ->notEmpty('nascimento');

//        $validator
//            ->allowEmpty('cpf');

        $validator
            ->allowEmpty('cpf');

        $validator
            ->allowEmpty('rg');

        $validator
            ->allowEmpty('telefone');

//        $validator
//            ->requirePresence('celular', 'create')
//            ->notEmpty('celular');
        $validator
           ->allowEmpty('celular');

        $validator
            ->allowEmpty('comercial');

        $validator
            ->allowEmpty('cep');

//        $validator
//            ->requirePresence('endereco', 'create')
//            ->notEmpty('endereco');

            $validator
                ->allowEmpty('endereco');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('bairro');

        $validator
            ->allowEmpty('cidade');

        $validator
            ->allowEmpty('estado');

        $validator
            ->allowEmpty('cor');

        $validator
            ->allowEmpty('profissao');

        $validator
            ->allowEmpty('matricula');

        $validator
            ->integer('cod_municipio')
            ->allowEmpty('cod_municipio');

        $validator
            ->allowEmpty('validate_carteira');

        $validator
            ->allowEmpty('nascionalidade');

        $validator
            ->allowEmpty('naturalidade');

        $validator
            ->allowEmpty('particularidades');

        $validator
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'E-mail deve ser válido'
            ])
            ->allowEmpty('email');

        $validator
            ->allowEmpty('nome_mae');

        $validator
            ->allowEmpty('nome_pai');

        $validator
            ->allowEmpty('indicacao_id');

        $validator
            ->allowEmpty('login');

        $validator
            ->allowEmpty('password');

        $validator
            ->allowEmpty('nome_conjuge');

        $validator
            ->allowEmpty('estadocivil_id');

        $validator
            ->boolean('bloqueado')
            ->allowEmpty('bloqueado');

        $validator
            ->boolean('digital')
            ->allowEmpty('difital');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->allowEmpty('recepcao');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['indicacao_id'], 'Indicacao'));
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        $rules->add($rules->existsIn(['estadocivil_id'], 'EstadoCivis'));

        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller!='Agendas' && in_array($outer->action, ['add', 'edit'])) {
            $queryData->where(['Clientes.situacao_id' => 1]);
        }
        return $queryData;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_update = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->user_id = $_SESSION['Auth']['User']['id'];
            $entity->situacao_id = 1;
            $entity->bloqueado = 0;
            $entity->cod_municipio = 0;
            //$entity->validate_carteira = date('Y-m-d H:i:s');
        }
        if(!empty($entity->validate_carteira)){
            if (!is_object($entity->validate_carteira)) {
                $tim = new Time();
                $now = $tim->createFromFormat('d/m/Y',$entity->validate_carteira);
                $entity->validate_carteira = $now->format('Y-m-d');
            }
        }

        if(!empty($entity->nascimento)){
            if (!is_object($entity->nascimento)) {
                $tim = new Time();
                $now = $tim->createFromFormat('d/m/Y',$entity->nascimento);
                $entity->nascimento = $now->format('Y-m-d');
            }
        }
        return true;
    }
}
