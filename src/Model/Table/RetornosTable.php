<?php
namespace App\Model\Table;

use App\Model\Entity\Retorno;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\Routing\Router;
/**
 * Retornos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $GrupoRetornos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoRetornos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 */
class RetornosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('retornos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GrupoRetornos', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoRetornos', [
            'foreignKey' => 'situacao_retorno_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('previsao');

        $validator
            ->integer('dias')
            ->allowEmpty('dias');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->notEmpty('cliente_id');

        $validator
            ->notEmpty('grupo_id');



        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if(!empty($outer->query)){
            foreach ($outer->query as $key => $value){
                if(!empty($value) && substr_count($key,'__')==1){
                    $queryData->andWhere([str_replace('__','.',$key) => $value]);
                }
            }

        }
        return $queryData;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['grupo_id'], 'GrupoRetornos'));
        $rules->add($rules->existsIn(['situacao_retorno_id'], 'SituacaoRetornos'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            if(!empty($_SESSION['Auth']['User']['id'])) {
                $entity->user_id = $_SESSION['Auth']['User']['id'];
            }
            $entity->situacao_id = 1;
            $entity->situacao_retorno_id = 1;
        }

            if(!empty($entity->previsao)){
                $entity->previsao = implode('-',array_reverse(explode('/',$entity->previsao)));
            }
        return true;
    }
}
