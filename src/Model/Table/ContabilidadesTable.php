<?php
namespace App\Model\Table;

use App\Model\Entity\Contabilidade;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Contabilidades Model
 *
 * @property \Cake\ORM\Association\HasMany $ContabilidadeBancos
 * @property \Cake\ORM\Association\HasMany $Contaspagar
 * @property \Cake\ORM\Association\HasMany $ContaspagarPlanejamentos
 * @property \Cake\ORM\Association\HasMany $Contasreceber
 * @property \Cake\ORM\Association\HasMany $ContasreceberPlanejamentos
 * @property \Cake\ORM\Association\HasMany $GrupoContabilidades
 * @property \Cake\ORM\Association\HasMany $Movimentos

 */
class ContabilidadesTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contabilidades');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('ContabilidadeBancos', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('Contaspagar', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('ContaspagarPlanejamentos', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('Contasreceber', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('ContasreceberPlanejamentos', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('GrupoContabilidades', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->hasMany('Movimentos', [
            'foreignKey' => 'contabilidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome')
            ->allowEmpty('nome');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao')
            ->allowEmpty('descricao');

        $validator
            ->scalar('cep')
            ->maxLength('cep')
            ->allowEmpty('cep');

        $validator
            ->scalar('rua')
            ->maxLength('rua')
            ->allowEmpty('rua');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro')
            ->allowEmpty('bairro');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade')
            ->allowEmpty('cidade');

        $validator
            ->scalar('uf')
            ->maxLength('uf')
            ->allowEmpty('uf');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Contabilidades') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }

    //Função que retorna todas as Contabilidades por Grupo.
    public function getCompaniesForGroup($idGroup = null)
    {
        $companies = $this->find('list')
        ->contain(['GrupoContabilidades']);

        if ($idGroup) {
            $companies->matching('GrupoContabilidades', function ($q) use ($idGroup) {
                return $q->where(['GrupoContabilidades.grupo_id' => $idGroup]);
            });
        }

        return $companies;
    }
}
