<?php
namespace App\Model\Table;

use App\Model\Entity\ConvenioIndicetabela;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * ConvenioIndicetabelas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $GrupoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class ConvenioIndicetabelasTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('convenio_indicetabelas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GrupoProcedimentos', [
            'foreignKey' => 'grupo_procedimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserReg', [
            'className' => 'Users',
            'foreignKey' => 'user_insert'
        ]);

        $this->belongsTo('UserAlt', [
            'className' => 'Users',
            'foreignKey' => 'user_update'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('honorario_porte')
            ->allowEmpty('honorario_porte');

        $validator
            ->decimal('operacional_uco')
            ->allowEmpty('operacional_uco');

        $validator
            ->decimal('filme')
            ->allowEmpty('filme');

        $validator
            ->integer('user_insert')
            ->allowEmpty('user_insert');

        $validator
            ->allowEmpty('user_insert_dt');

        $validator
            ->integer('user_update')
            ->allowEmpty('user_update');

        $validator
            ->allowEmpty('user_update_dt');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        $rules->add($rules->existsIn(['grupo_procedimento_id'], 'GrupoProcedimentos'));
        $rules->add($rules->isUnique(
            ['grupo_procedimento_id', 'convenio_id'],
            'Esta combinação Convênio e Grupo Procedimento já foi usada.'
        ));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if($outer->controller=='ConvenioIndicetabelas') {
            $queryData->where(['ConvenioIndicetabelas.situacao_id' => 1]);
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if(substr_count($value, '/')){
                            if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        }else{
                            $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                        }
                    }
                }
            }
        }
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        if(!empty($entity->id)){
            $entity->user_update = $_SESSION['Auth']['User']['id'];
            $entity->user_update_dt = date('Y-m-d H:i:s');
        }else{
            $entity->user_insert = $_SESSION['Auth']['User']['id'];
            $entity->user_insert_dt = date('Y-m-d H:i:s');
        }

        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }

        return true;
    }

    public function afterSave(Event $event)
    {
        $entity = $event->data['entity'];
        if(!empty($entity->honorario_porte) || !empty($entity->operacional_uco) || !empty($entity->filme)){
            $data = $entity->toArray();
            $this->calculaValorFaturar($data);
        }
        return true;
    }

    /** Calcula valor_faturar - preco_procedimentos
     * @param array $data
     */
    public function calculaValorFaturar($data = array())
    {
        $this->PrecoProcedimentos = TableRegistry::get('PrecoProcedimentos');
        $precoProc = $this->PrecoProcedimentos->find('all')
            ->contain(['Procedimentos'])
            ->where(['PrecoProcedimentos.convenio_id' => $data['convenio_id'],
                    'Procedimentos.grupo_id' => $data['grupo_procedimento_id'],
                    'PrecoProcedimentos.situacao_id' => 1,
                    'Procedimentos.situacao_id' => 1
            ]);
        foreach ($precoProc as $pp){
            $valor1 = $pp->uco * $data['operacional_uco'];
            $valor2 = $pp->porte * $data['honorario_porte'];
            $valor3 = $pp->filme * $data['filme'];
            $valor_fatura = $valor1 + $valor2 + $valor3;
            $preco_procedimento = $this->PrecoProcedimentos->get($pp->id);
            $preco_procedimento->valor_faturar = $valor_fatura;
            $this->PrecoProcedimentos->save($preco_procedimento);
        }

    }

}
