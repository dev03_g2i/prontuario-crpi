<?php
namespace App\Model\Table;

use App\Model\Entity\ClienteAnexo;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * ClienteAnexos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $TipoAnexos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 */
class ClienteAnexosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cliente_anexos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoAnexos', [
            'foreignKey' => 'tipoanexo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('ListaAnexos', [
            'foreignKey' => 'anexo_id',
            'conditions'=>['ListaAnexos.situacao_id'=>'1']
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->requirePresence('tipoanexo_id', 'create')
            ->notEmpty('tipoanexo_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['tipoanexo_id'], 'TipoAnexos'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        return true;
    }

    public function getClienteAnexosPerTipo($tipo, $cliente_id)
    {
        $query = $this->find('all')
        ->contain(['Clientes', 'TipoAnexos', 'Users', 'SituacaoCadastros','ListaAnexos'])
        ->where(['ClienteAnexos.situacao_id' => 1])
        ->andWhere(['ClienteAnexos.cliente_id' => $cliente_id])
        ->andWhere(['ClienteAnexos.tipoanexo_id' => $tipo])
        ->orderDesc('ClienteAnexos.created');
        return $query;
    }
}
