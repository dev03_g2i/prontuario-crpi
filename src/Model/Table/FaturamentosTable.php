<?php
namespace App\Model\Table;

use App\Model\Entity\Faturamento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Faturamentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Users_up
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class FaturamentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('faturamentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AtendimentoProcedimentos', [
            'foreignKey' => 'atendimento_procedimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users_up', [
            'className' => 'Users',
            'foreignKey' => 'user_up_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('numero_autorizacao', 'create')
            ->notEmpty('numero_autorizacao');

        $validator
            ->requirePresence('data_autorizacao', 'create')
            ->notEmpty('data_autorizacao');

        $validator
            ->requirePresence('numero_guia', 'create')
            ->notEmpty('numero_guia');

        $validator
            ->requirePresence('data_guia', 'create')
            ->notEmpty('data_guia');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atendimento_procedimento_id'], 'AtendimentoProcedimentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Faturamentos') {
              $queryData->where(['Faturamentos.situacao_id' => 1]);

     if(!empty($outer->query['atendimento_procedimento_id'])){
         $queryData->where(['Faturamentos.atendimento_procedimento_id' => $outer->query['atendimento_procedimento_id']]);
     }

    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
                $entity->situacao_id = 1;
          }

            if (!empty($_SESSION['Auth']['User']['id'])) {
                $entity->user_up_id = $_SESSION['Auth']['User']['id'];
            }
        if(!empty($entity->data_autorizacao)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_autorizacao);
           $entity->data_autorizacao = $now;
        }
         if(!empty($entity->data_guia)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_guia);
           $entity->data_guia = $now;
        }
         return true;
    }
}
