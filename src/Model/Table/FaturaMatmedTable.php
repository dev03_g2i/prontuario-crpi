<?php
namespace App\Model\Table;

use App\Controller\Component\MatmedComponent;
use App\Model\Entity\FaturaMatmed;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FaturaMatmed Model
 *
 * @property \Cake\ORM\Association\BelongsTo $EstqArtigos
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class FaturaMatmedTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fatura_matmed');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('EstqArtigos', [
            'foreignKey' => 'artigo_id'
        ]);
        $this->belongsTo('AtendimentoProcedimentos', [
            'foreignKey' => 'atendimento_procedimento_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'atendimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('data', 'create')
            ->notEmpty('data');

        $validator
            ->integer('quantidade')
            ->allowEmpty('quantidade');

        $validator
            ->decimal('vl_custos')
            ->allowEmpty('vl_custos');

        $validator
            ->decimal('vl_venda')
            ->allowEmpty('vl_venda');

        $validator
            ->decimal('vl_custo_medico')
            ->allowEmpty('vl_custo_medico');

        $validator
            ->integer('origin')
            ->allowEmpty('origin');

        $validator
            ->allowEmpty('codigo_tiss');

        $validator
            ->allowEmpty('codigo_tuss');

        $validator
            ->allowEmpty('codigo_convenio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['artigo_id'], 'EstqArtigos'));
        $rules->add($rules->existsIn(['atendimento_procedimento_id'], 'AtendimentoProcedimentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if($outer->controller=='FaturaMatmed') {
            $queryData->where(['FaturaMatmed.situacao_id' => 1]);
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if(substr_count($value, '/')){
                            if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        }else{
                            $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                        }
                    }
                }
            }
        }
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            if (!empty($_SESSION['Auth']['User']['id'])) {
                $entity->user_id = $_SESSION['Auth']['User']['id'];
            }
            $entity->situacao_id = 1;
        }

        
        if(!empty($entity->id)){// Update
            /* Verifica se o campo quantidade foi alterado, se sim, atualiza o total */
            if($entity->dirty('quantidade')){
                $entity->total = $this->atualizaTotal($entity->quantidade, $entity->artigo_id);
            }
        }

        if(!empty($entity->data)){
            if(!is_object($entity->data)){
                $inicio = new DateTime();
                $now = $inicio->createFromFormat('d/m/Y', $entity->data);
                $entity->data = $now;
            }
        }
        return true;
    }

    public function atualizaTotal($qtd, $artigo_id)
    {
        $dados = MatmedComponent::precoArtigo($artigo_id);
        $total = 0;
        if(!empty($dados))
            $total = $dados->valor * $qtd;

        return $total;
    }
}
