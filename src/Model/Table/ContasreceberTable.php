<?php

namespace App\Model\Table;

use App\Model\Entity\Contasreceber;
use Cake\Datasource\EntityInterface;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Contasreceber Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Planocontas
 * @property \Cake\ORM\Association\BelongsTo $ClienteClone
 * @property \Cake\ORM\Association\BelongsTo $Status
 * @property \Cake\ORM\Association\BelongsTo $Contabilidade
 * @property \Cake\ORM\Association\BelongsTo $TipoPagamento
 * @property \Cake\ORM\Association\BelongsTo $TipoDocumento
 * @property \Cake\ORM\Association\BelongsTo $Programacaos
 * @property \Cake\ORM\Association\BelongsToMany $Deducoes
 */
class ContasreceberTable extends Table
{

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */


    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contasreceber');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Planocontas', [
            'foreignKey' => 'idPlanoContas',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ClienteClone', [
            'foreignKey' => 'idCliente',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Status', [
            'foreignKey' => 'status_id'
        ]);
        $this->belongsTo('Contabilidade', [
            'foreignKey' => 'contabilidade_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoPagamento', [
            'foreignKey' => 'tipoPagamento',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TipoDocumento', [
            'foreignKey' => 'tipoDocumento',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'extern_id',
            'joinType' => 'INNER',
            'strategy' => 'select'
        ]);

        $this->belongsTo('Correcaomonetaria', [
            'foreignKey' => 'correcaoMonetaria',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Programacao', [
            'foreignKey' => 'idProgramacao'
        ]);
        $this->belongsToMany('Deducoes', [
            'foreignKey' => 'contasreceber_id',
            'targetForeignKey' => 'deducoes_id',
            'joinTable' => 'deducoes_contasreceber'
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'strategy' => 'select'
        ]);

        $this->belongsTo('UserReg', [
            'className' => 'Users',
            'foreignKey' => 'user_created',
            'joinType' => 'INNER',
            'strategy' => 'select'
        ]);

        $this->belongsTo('ControleFinanceiro', [
            'foreignKey' => 'controle_financeiro_id',
            'joinType' => 'INNER',
            'strategy' => 'select'
        ]);

        $this->belongsTo('UserAlt', [
            'className' => 'Users',
            'foreignKey' => 'user_modified',
            'joinType' => 'INNER',
            'strategy' => 'select'
        ]);
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->notEmpty('data');

        $validator
            ->decimal('valor')
            ->allowEmpty('valor');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('dataPagamento');

        $validator
            ->notEmpty('vencimento');

        $validator
            ->allowEmpty('documento');

        $validator
            ->allowEmpty('numdoc');

        $validator
            ->allowEmpty('parcela');

        $validator
            ->integer('formapagamento')
            ->allowEmpty('formapagamento');

        $validator
            ->allowEmpty('boletomsg');

        $validator
            ->allowEmpty('boletomsglivre');

        $validator
            ->allowEmpty('boletobarras');

        $validator
            ->allowEmpty('boletolinhadigitavel');

        $validator
            ->allowEmpty('boletonossonum');

        $validator
            ->numeric('juros')
            ->allowEmpty('juros');

        $validator
            ->numeric('multa')
            ->allowEmpty('multa');

        $validator
            ->integer('boletocedente')
            ->allowEmpty('boletocedente');

        $validator
            ->allowEmpty('numfiscal');

        $validator
            ->allowEmpty('datafiscal');

        $validator
            ->decimal('valorBruto')
            ->notEmpty('valorBruto');

        $validator
            ->decimal('desconto')
            ->allowEmpty('desconto');

        $validator
            ->integer('correcaoMonetaria')
            ->allowEmpty('correcaoMonetaria');

        $validator
            ->notEmpty('parcelas');


        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['idPlanoContas'], 'Planocontas'));
        $rules->add($rules->existsIn(['idCliente'], 'ClienteClone'));
        $rules->add($rules->existsIn(['contabilidade_id'], 'Contabilidade'));
        $rules->add($rules->existsIn(['tipoPagamento'], 'TipoPagamento'));
        $rules->add($rules->existsIn(['tipoDocumento'], 'TipoDocumento'));
        return $rules;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        if (empty($entity->id))// INSERT
        {
            $entity->user_created = $_SESSION['Auth']['User']['id'];
        } else {// UPDATE
            $entity->user_modified = $_SESSION['Auth']['User']['id'];
        }

        if ($entity->isNew()) {
            $entity->status_id = 1;
            $entity->data_cadastro = date('Y-m-d H:i:s');
            $entity->situacao_id = 1;
        }

        if (!empty($entity->data)) {
            $entity->data = implode('-', array_reverse(explode('/', $entity->data)));
        }

        if (!empty($entity->vencimento)) {
            $entity->vencimento = implode('-', array_reverse(explode('/', $entity->vencimento)));
        }
        if (!empty($entity->datafiscal)) {
            $entity->datafiscal = implode('-', array_reverse(explode('/', $entity->datafiscal)));
        }
        if (!empty($entity->dataPagamento)) {
            $entity->dataPagamento = implode('-', array_reverse(explode('/', $entity->dataPagamento)));
        }
        return true;
    }

    public function afterSave(Event $event, EntityInterface $entity)
    {
        /* Atualiza o campo total_pagoato da tabela atendimentos de acordo com as parcelas  */
        $outer = Router::getRequest();
        if ($outer->controller != 'Faturamentos') {
            if (!empty($entity->valor)) {
                $query = $this->find();
                $query = $query->select(['total' => $query->func()->sum('valor')])
                    ->where(['status_id' => 1, 'extern_id' => $entity->extern_id])->toArray();

                $atendimento = TableRegistry::get('Atendimentos');
                $atend = $atendimento->get($entity->extern_id);
                $atend->total_pagoato = $query[0]->total;
                $atendimento->save($atend);
            }
        }

        return true;
    }

    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function getBillsToReceive()
    {
        $now = new Time();
        $month = $now->format('m');
        $year = $now->format('Y');
        
        $resultBills = $this->find()
        ->select(['contasReceber' => 'TRUNCATE(SUM(valor + ((valor * (juros/100)) 
        + (valor * (multa/100)) - (valor * (desconto/100)))), 2)',
        'dia' => "IF(DAY(data_pagamento) < 10, CONCAT('0', DAY(data_pagamento)), DAY(data_pagamento))"])
        ->where(['MONTH(data_pagamento)' => $month, 'YEAR(data_pagamento)' => $year])
        ->group(['data_pagamento']);

        return $resultBills;
    }
}
