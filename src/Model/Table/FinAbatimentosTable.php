<?php
namespace App\Model\Table;

use App\Model\Entity\FinAbatimento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinAbatimentos Model
 *
 * @property \Cake\ORM\Association\HasMany $FinAbatimentosContasPagar
 * @property \Cake\ORM\Association\HasMany $FinAbatimentosContasReceber

 */
class FinAbatimentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_abatimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->hasMany('FinAbatimentosContasPagar', [
            'foreignKey' => 'fin_abatimento_id'
        ]);
        $this->hasMany('FinAbatimentosContasReceber', [
            'foreignKey' => 'fin_abatimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome')
            ->allowEmpty('nome');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FinAbatimentos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }
}
