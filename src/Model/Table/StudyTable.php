<?php
namespace App\Model\Table;

use App\Model\Entity\Study;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Study Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Studies
 * @property \Cake\ORM\Association\BelongsTo $StudyStatuses
 * @property \Cake\ORM\Association\BelongsTo $Filesets
 * @property \Cake\ORM\Association\HasMany $Study

 */
class StudyTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('study');
        $this->displayField('pk');
        $this->primaryKey('pk');

        $this->belongsTo('Studies', [
            'foreignKey' => 'study_id'
        ]);
        $this->belongsTo('StudyStatuses', [
            'foreignKey' => 'study_status_id'
        ]);
        $this->belongsTo('Filesets', [
            'foreignKey' => 'fileset_id'
        ]);
        $this->hasMany('Series', [
            'foreignKey' => 'study_fk'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('pk', 'create');

        $validator
            ->allowEmpty('patient_fk');

        $validator
            ->allowEmpty('accno_issuer_fk');

        $validator
            ->requirePresence('study_iuid', 'create')
            ->notEmpty('study_iuid')
            ->add('study_iuid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->allowEmpty('study_datetime');

        $validator
            ->allowEmpty('accession_no');

        $validator
            ->allowEmpty('ref_physician');

        $validator
            ->allowEmpty('ref_phys_fn_sx');

        $validator
            ->allowEmpty('ref_phys_gn_sx');

        $validator
            ->allowEmpty('ref_phys_i_name');

        $validator
            ->allowEmpty('ref_phys_p_name');

        $validator
            ->allowEmpty('study_desc');

        $validator
            ->allowEmpty('study_custom1');

        $validator
            ->allowEmpty('study_custom2');

        $validator
            ->allowEmpty('study_custom3');

        $validator
            ->allowEmpty('mods_in_study');

        $validator
            ->allowEmpty('cuids_in_study');

        $validator
            ->integer('num_series')
            ->requirePresence('num_series', 'create')
            ->notEmpty('num_series');

        $validator
            ->integer('num_instances')
            ->requirePresence('num_instances', 'create')
            ->notEmpty('num_instances');

        $validator
            ->allowEmpty('ext_retr_aet');

        $validator
            ->allowEmpty('retrieve_aets');

        $validator
            ->allowEmpty('fileset_iuid');

        $validator
            ->integer('availability')
            ->requirePresence('availability', 'create')
            ->notEmpty('availability');

        $validator
            ->integer('study_status')
            ->requirePresence('study_status', 'create')
            ->notEmpty('study_status');

        $validator
            ->allowEmpty('checked_time');

        $validator
            ->allowEmpty('created_time');

        $validator
            ->allowEmpty('updated_time');

        $validator
            ->allowEmpty('study_attrs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['study_iuid']));
        $rules->add($rules->existsIn(['study_id'], 'Studies'));
        $rules->add($rules->existsIn(['study_status_id'], 'StudyStatuses'));
        $rules->add($rules->existsIn(['fileset_id'], 'Filesets'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Study') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'oviyam';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if(!empty($entity->study_datetime)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->study_datetime);
           $entity->study_datetime = $now;
        }
         if(!empty($entity->checked_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->checked_time);
           $entity->checked_time = $now;
        }
         if(!empty($entity->created_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->created_time);
           $entity->created_time = $now;
        }
         if(!empty($entity->updated_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->updated_time);
           $entity->updated_time = $now;
        }
         return true;
    }
}
