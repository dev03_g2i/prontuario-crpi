<?php
namespace App\Model\Table;

use App\Model\Entity\Banco;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Banco Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Contabilidade
 * @property \Cake\ORM\Association\BelongsToMany $Movimento

 */
class BancoTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('banco');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsToMany('Contabilidade', [
            'foreignKey' => 'banco_id',
            'targetForeignKey' => 'contabilidade_id',
            'joinTable' => 'contabilidade_banco'
        ]);
        $this->belongsToMany('Movimento', [
            'foreignKey' => 'banco_id',
            'targetForeignKey' => 'movimento_id',
            'joinTable' => 'movimento_banco'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->allowEmpty('nome');

        $validator
            ->scalar('agencia')
            ->allowEmpty('agencia');

        $validator
            ->scalar('conta')
            ->allowEmpty('conta');

        $validator
            ->numeric('limite')
            ->allowEmpty('limite');

        $validator
            ->numeric('saldo')
            ->allowEmpty('saldo');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->boolean('mostra_fluxo')
            ->allowEmpty('mostra_fluxo');

        $validator
            ->boolean('mostra_saldo_geral')
            ->allowEmpty('mostra_saldo_geral');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Banco') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }
}
