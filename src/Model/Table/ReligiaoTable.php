<?php
namespace App\Model\Table;

use App\Model\Entity\Religiao;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Religiao Model
 *
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\HasMany $Clientes

 */
class ReligiaoTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('religiao');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id'
        ]);
        $this->hasMany('Clientes', [
            'foreignKey' => 'religiao_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('descricao')
            ->allowEmpty('descricao');

        $validator
            ->integer('user_reg')
            ->allowEmpty('user_reg');

        $validator
            ->integer('user_update')
            ->allowEmpty('user_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Religiao') {
              $queryData->where(['Religiao.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
          }
        
        return true;
    }
}
