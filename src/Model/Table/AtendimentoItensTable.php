<?php
namespace App\Model\Table;

use App\Model\Entity\AtendimentoIten;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * AtendimentoItens Model
 *
 * @property \Cake\ORM\Association\BelongsTo $AtendimentoProcedimentos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 */
class AtendimentoItensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('atendimento_itens');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('AtendimentoProcedimentos', [
            'foreignKey' => 'atendproc_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Dentes', [
            'foreignKey' => 'descricao',
            'joinType' => 'LEFT',
            'conditions'=>['AtendimentoItens.tipo'=>'d']
        ]);

        $this->belongsTo('Regioes', [
            'foreignKey' => 'descricao',
            'joinType' => 'LEFT',
            'conditions'=>['AtendimentoItens.tipo'=>'r']
        ]);
        $this->hasMany('FaceItens', [
            'foreignKey' => 'iten_id',
            'conditions'=>['FaceItens.situacao_id'=>'1']
        ]);

        $this->belongsTo('Financeiro', [
            'className'=>'MedicoResponsaveis',
            'foreignKey' => 'financeiro_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('Executor', [
            'className'=>'MedicoResponsaveis',
            'foreignKey' => 'executor_id',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('User_Financeiro', [
            'className'=>'Users',
            'foreignKey' => 'user_financeiro',
            'joinType' => 'LEFT'
        ]);

        $this->belongsTo('User_Executor', [
            'className'=>'Users',
            'foreignKey' => 'user_executor',
            'joinType' => 'LEFT'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->integer('descricao')
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['atendproc_id'], 'AtendimentoProcedimentos'));

        return $rules;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        return true;
    }
    /**
     * Retorna a situacao dependendo da quantidade de itens ativos
     * Ex: se tivar mais de um item ativo entao deixo o procedimento no atendimento, caso contrario devo desativa-lo tambem
     * application integrity.
     *
     * @param id do AtendimentoProcedimentos
     * @return situacao para AtendimentoProcedimentos
     */
    public function checar_itens($id){
        $itens = $this->find('all')
            ->where(['AtendimentoItens.atendproc_id'=>$id])
            ->andWhere(['AtendimentoItens.situacao_id'=>1])
            ->count();
        // caso nao exista mais atendimentoItens ativos para o atendimentoProcedimento entao o desativo
        if($itens>0){
            return 1;
        }else{
            return 2;
        }
    }
}
