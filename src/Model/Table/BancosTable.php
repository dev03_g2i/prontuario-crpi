<?php
namespace App\Model\Table;

use App\Model\Entity\Banco;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Bancos Model
 *
 * @property \Cake\ORM\Association\HasMany $BancoMovimentos
 * @property \Cake\ORM\Association\HasMany $ContabilidadeBancos
 * @property \Cake\ORM\Association\HasMany $Movimentos

 */
class BancosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('bancos');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('BancoMovimentos', [
            'foreignKey' => 'banco_id'
        ]);
        $this->hasMany('ContabilidadeBancos', [
            'foreignKey' => 'banco_id'
        ]);
        $this->hasMany('Movimentos', [
            'foreignKey' => 'banco_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome')
            ->allowEmpty('nome');

        $validator
            ->scalar('agencia')
            ->maxLength('agencia')
            ->allowEmpty('agencia');

        $validator
            ->scalar('conta')
            ->maxLength('conta')
            ->allowEmpty('conta');

        $validator
            ->numeric('limite')
            ->allowEmpty('limite');

        $validator
            ->numeric('saldo')
            ->allowEmpty('saldo');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->boolean('mostra_fluxo')
            ->allowEmpty('mostra_fluxo');

        $validator
            ->boolean('mostra_saldo_geral')
            ->allowEmpty('mostra_saldo_geral');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Bancos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {}
        
        return true;
    }
}
