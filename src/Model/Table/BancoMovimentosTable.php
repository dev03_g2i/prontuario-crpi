<?php
namespace App\Model\Table;

use App\Model\Entity\BancoMovimento;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;
use Cake\I18n\Time;

/**
 * BancoMovimentos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Bancos
 * @property \Cake\ORM\Association\BelongsTo $Movimentos

 */
class BancoMovimentosTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('banco_movimentos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Bancos', [
            'foreignKey' => 'banco_id'
        ]);
        $this->belongsTo('Movimentos', [
            'foreignKey' => 'movimento_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('mes')
            ->maxLength('mes')
            ->allowEmpty('mes');

        $validator
            ->scalar('ano')
            ->maxLength('ano')
            ->allowEmpty('ano');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        $validator
            ->decimal('saldoInicial')
            ->allowEmpty('saldoInicial');

        $validator
            ->decimal('saldoFinal')
            ->allowEmpty('saldoFinal');

        $validator
            ->requirePresence('data')
            ->notEmpty('data');

        $validator
            ->integer('por')
            ->allowEmpty('por');

        $validator
            ->requirePresence('encerradoDt')
            ->notEmpty('encerradoDt');

        $validator
            ->integer('encerradoPor')
            ->allowEmpty('encerradoPor');

        $validator
            ->scalar('data_movimento')
            ->maxLength('data_movimento')
            ->allowEmpty('data_movimento');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['banco_id'], 'Bancos'));
        $rules->add($rules->existsIn(['movimento_id'], 'Movimentos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='BancoMovimentos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if(!is_object($entity->data)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data);
           $entity->data = $now;
        }
         if(!is_object($entity->encerradoDt)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->encerradoDt);
           $entity->encerradoDt = $now;
        }
         return true;
    }

    //Função publica que retorna todos os movimentos abertos pelo mês do BancoMovimento
    public function getAllOpenedMovesBank($idCompany = null)
    {
        $now = new Time();
        $month = $now->format('m');
        $year = $now->format('Y');

        $movBancos = $this->find()
            ->contain(['Bancos' => ['ContabilidadeBancos'], 'Movimentos'])
            ->where(['BancoMovimentos.mes LIKE' => $month, 'BancoMovimentos.ano LIKE' => $year])
            ->andWhere(['BancoMovimentos.status NOT LIKE' => 'Encerrado'])
            ->group(['BancoMovimentos.banco_id']);
        
        // TODO esta funcionando, porém deu um erro no PHP. E no jquery, não plota a nova table na dashboard.
        if ($idCompany) {
            $movBancos->matching('Bancos.ContabilidadeBancos', function ($q) use ($idCompany) {
                return $q->where(['ContabilidadeBancos.contabilidade_id' => $idCompany]);
            });
        }

        return $movBancos;
    }

    public function getLastMonthFinalBalance()
    {
        $lastBankFinalBalance = ($this->find()->select(['finalBalance' => 'SUM(saldoFinal)'])->first())->finalBalance;

        return $lastBankFinalBalance;
    }
}
