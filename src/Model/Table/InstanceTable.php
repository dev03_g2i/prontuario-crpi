<?php
namespace App\Model\Table;

use App\Model\Entity\Instance;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Instance Model
 *

 */
class InstanceTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('instance');
        $this->displayField('pk');
        $this->primaryKey('pk');

        $this->belongsTo('Series', [
            'foreignKey' => 'series_fk'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('pk', 'create');

        $validator
            ->allowEmpty('series_fk');

        $validator
            ->allowEmpty('srcode_fk');

        $validator
            ->allowEmpty('media_fk');

        $validator
            ->requirePresence('sop_iuid', 'create')
            ->notEmpty('sop_iuid')
            ->add('sop_iuid', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->requirePresence('sop_cuid', 'create')
            ->notEmpty('sop_cuid');

        $validator
            ->allowEmpty('inst_no');

        $validator
            ->allowEmpty('content_datetime');

        $validator
            ->allowEmpty('sr_complete');

        $validator
            ->allowEmpty('sr_verified');

        $validator
            ->allowEmpty('inst_custom1');

        $validator
            ->allowEmpty('inst_custom2');

        $validator
            ->allowEmpty('inst_custom3');

        $validator
            ->allowEmpty('ext_retr_aet');

        $validator
            ->allowEmpty('retrieve_aets');

        $validator
            ->integer('availability')
            ->requirePresence('availability', 'create')
            ->notEmpty('availability');

        $validator
            ->integer('inst_status')
            ->requirePresence('inst_status', 'create')
            ->notEmpty('inst_status');

        $validator
            ->boolean('all_attrs')
            ->requirePresence('all_attrs', 'create')
            ->notEmpty('all_attrs');

        $validator
            ->boolean('commitment')
            ->requirePresence('commitment', 'create')
            ->notEmpty('commitment');

        $validator
            ->boolean('archived')
            ->requirePresence('archived', 'create')
            ->notEmpty('archived');

        $validator
            ->allowEmpty('created_time');

        $validator
            ->allowEmpty('updated_time');

        $validator
            ->allowEmpty('inst_attrs');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['sop_iuid']));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Instance') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'oviyam';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if(!empty($entity->content_datetime)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->content_datetime);
           $entity->content_datetime = $now;
        }
         if(!empty($entity->created_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->created_time);
           $entity->created_time = $now;
        }
         if(!empty($entity->updated_time)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->updated_time);
           $entity->updated_time = $now;
        }
         return true;
    }
}
