<?php
namespace App\Model\Table;

use App\Model\Entity\Fornecedore;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Fornecedores Model
 *

 */
class FornecedoresTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fornecedores');
        $this->displayField('nome');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome')
            ->allowEmpty('nome');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone')
            ->allowEmpty('telefone');

        $validator
            ->scalar('celular')
            ->maxLength('celular')
            ->allowEmpty('celular');

        $validator
            ->scalar('cep')
            ->maxLength('cep')
            ->allowEmpty('cep');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade')
            ->allowEmpty('cidade');

        $validator
            ->scalar('uf')
            ->maxLength('uf')
            ->allowEmpty('uf');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->scalar('rua')
            ->maxLength('rua')
            ->allowEmpty('rua');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro')
            ->allowEmpty('bairro');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('cnpj')
            ->maxLength('cnpj')
            ->allowEmpty('cnpj');

        $validator
            ->scalar('observacao')
            ->maxLength('observacao')
            ->allowEmpty('observacao');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->scalar('cpf')
            ->maxLength('cpf')
            ->allowEmpty('cpf');

        $validator
            ->scalar('banco')
            ->maxLength('banco')
            ->allowEmpty('banco');

        $validator
            ->scalar('agencia')
            ->maxLength('agencia')
            ->allowEmpty('agencia');

        $validator
            ->scalar('conta')
            ->maxLength('conta')
            ->allowEmpty('conta');

        $validator
            ->scalar('operacao')
            ->maxLength('operacao')
            ->allowEmpty('operacao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Fornecedores') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }
}
