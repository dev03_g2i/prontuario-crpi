<?php
namespace App\Model\Table;

use App\Model\Entity\Grupo;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Grupos Model
 *
 * @property \Cake\ORM\Association\HasMany $GrupoContabilidades

 */
class GruposTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('grupos');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('GrupoContabilidades', [
            'foreignKey' => 'grupo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao')
            ->requirePresence('descricao')
            ->notEmpty('descricao')
            ->add('descricao', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('status')
            ->requirePresence('status')
            ->notEmpty('status');

        $validator
            ->requirePresence('dtcriacao')
            ->notEmpty('dtcriacao');

        $validator
            ->scalar('criadopor')
            ->maxLength('criadopor')
            ->requirePresence('criadopor')
            ->notEmpty('criadopor');

        $validator
            ->requirePresence('dtalteracao')
            ->notEmpty('dtalteracao');

        $validator
            ->scalar('alteradopor')
            ->maxLength('alteradopor')
            ->requirePresence('alteradopor')
            ->notEmpty('alteradopor');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['descricao']));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Grupos') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        if(!is_object($entity->dtcriacao)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->dtcriacao);
           $entity->dtcriacao = $now;
        }
         if(!is_object($entity->dtalteracao)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->dtalteracao);
           $entity->dtalteracao = $now;
        }
         return true;
    }
    
    public function getAllGroups()
    {
        $groups = $this->find('list');

        return $groups;
    }
}
