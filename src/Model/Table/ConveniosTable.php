<?php
namespace App\Model\Table;

use App\Model\Entity\Convenio;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Convenios Model
 *
 * @property \Cake\ORM\Association\BelongsTo $TipoConvenios
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Atendimentos
 * @property \Cake\ORM\Association\HasMany $Clientes
 * @property \Cake\ORM\Association\HasMany $PrecoProcedimentos
 * @property \Cake\ORM\Association\HasMany $Agendas
 */
class ConveniosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('convenios');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('TipoConvenios', [
            'foreignKey' => 'tipo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Atendimentos', [
            'foreignKey' => 'convenio_id'
        ]);
        $this->hasMany('Clientes', [
            'foreignKey' => 'convenio_id'
        ]);
        $this->hasMany('PrecoProcedimentos', [
            'foreignKey' => 'convenio_id'
        ]);
        $this->hasMany('Agendas', [
            'foreignKey' => 'convenio_id',
        ]);
        $this->hasMany('FaturaRecalcular', [
            'foreignKey' => 'convenio_id'
        ]);
        $this->belongsTo('Banco', [
            'foreignKey' => 'bank_id'
        ]);
        $this->belongsTo('Planocontas', [
            'foreignKey' => 'planAcc_id'
        ]);
        $this->hasMany('ConvenioImpostosParametro', [
            'foreignKey' => 'convenio_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tipo_id'], 'TipoConvenios'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
        }
        if(!empty($entity->data)){
            $entity->data = implode('-',array_reverse(explode('/',$entity->data)));
        }
        return true;
    }
    public function beforeFind(Event $event,Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller=='Convenios') {
            $queryData->orderAsc('Convenios.nome');

            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if(substr_count($value, '/')){
                            if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        }else{
                            $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                        }
                    }
                }
            }
        }/* No controller agendas tem que aceitar convenio com situacao excluida por causa dos horarios gerados,
            esses horarios todos são cadastrados com convenio -1 = (sistema)
        */
        elseif($outer->controller !='Agendas') {
            $queryData->andWhere(['Convenios.situacao_id' => 1]);
        }
        return $queryData;
    }

    /** listagem para selects
     * @return $this
     */
    public function getList()
    {
        $query = $this->find('list')->where(['situacao_id' => 1])->orderAsc('nome');
        return $query;
    }

    public function getPrecoInstrumento($procedimento_id, $convenio_id){
        $precoProcedimentoTable = TableRegistry::get('PrecoProcedimentos');
        $precoInstrumento = $precoProcedimentoTable->find('all')
            ->select(['valor_instrumentador' => 'PrecoProcedimentos.valor_instrumentador'])
            ->where(['PrecoProcedimentos.convenio_id' => $convenio_id])
            ->andWhere(['PrecoProcedimentos.procedimento_id' => $procedimento_id])
            ->first();

        return $precoInstrumento->valor_instrumentador;
    }
}
