<?php
namespace App\Model\Table;

use App\Model\Entity\ClienteClone;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * ClienteClone Model
 *
 */
class ClienteCloneTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cliente_clone');
        $this->displayField('nome');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->allowEmpty('telefone');

        $validator
            ->allowEmpty('celular');

        $validator
            ->allowEmpty('cep');

        $validator
            ->allowEmpty('endereco');

        $validator
            ->allowEmpty('uf');

        $validator
            ->allowEmpty('cidade');

        $validator
            ->allowEmpty('bairro');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('complemento');

        $validator
            ->allowEmpty('cpf');

        $validator
            ->allowEmpty('cnpj');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->allowEmpty('rg');

        $validator
            ->dateTime('datacadastro')
            ->allowEmpty('datacadastro');

        $validator
            ->allowEmpty('email_cobranca');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            $entity->status = 1;
        }
        return true;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
