<?php
namespace App\Model\Table;

use App\Model\Entity\Planoconta;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Planocontas Model
 *
 * @property \Cake\ORM\Association\HasMany $Contaspagar
 * @property \Cake\ORM\Association\HasMany $ContaspagarPlanejamentos
 * @property \Cake\ORM\Association\HasMany $Contasreceber
 * @property \Cake\ORM\Association\HasMany $ContasreceberPlanejamentos
 * @property \Cake\ORM\Association\HasMany $Movimentos

 */
class PlanocontasTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('planocontas');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->belongsTo('Classificacaocontas', [
            'foreignKey' => 'classificacao'
        ]);
        $this->hasMany('Contaspagar', [
            'foreignKey' => 'planoconta_id'
        ]);
        $this->hasMany('ContaspagarPlanejamentos', [
            'foreignKey' => 'planoconta_id'
        ]);
        $this->hasMany('Contasreceber', [
            'foreignKey' => 'planoconta_id'
        ]);
        $this->hasMany('ContasreceberPlanejamentos', [
            'foreignKey' => 'planoconta_id'
        ]);
        $this->hasMany('Movimentos', [
            'foreignKey' => 'planoconta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->integer('classificacao')
            ->allowEmpty('classificacao');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->scalar('nome')
            ->maxLength('nome')
            ->allowEmpty('nome');

        $validator
            ->integer('rateio')
            ->allowEmpty('rateio');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Planocontas') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {}
        
        return true;
    }
}
