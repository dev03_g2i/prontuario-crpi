<?php
namespace App\Model\Table;

use App\Model\Entity\FinClassificacaoConta;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinClassificacaoContas Model
 *
 * @property \Cake\ORM\Association\HasMany $FinPlanoContas

 */
class FinClassificacaoContasTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_classificacao_contas');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->hasMany('FinPlanoContas', [
            'foreignKey' => 'fin_classificacao_conta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('descricao')
            ->allowEmpty('descricao');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->integer('tipo')
            ->allowEmpty('tipo');

        $validator
            ->boolean('considera')
            ->allowEmpty('considera');

        $validator
            ->integer('operacao_transferencia')
            ->requirePresence('operacao_transferencia')
            ->notEmpty('operacao_transferencia');

        $validator
            ->integer('criado_por')
            ->allowEmpty('criado_por');

        $validator
            ->integer('alterado_por')
            ->allowEmpty('alterado_por');

        $validator
            ->allowEmpty('data_criacao');

        $validator
            ->allowEmpty('data_alteracao');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FinClassificacaoContas') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
    $queryData->order(['FinClassificacaoContas.descricao'])->andWhere(['FinClassificacaoContas.situacao' => 1]);
    return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
            $entity->data_criacao = date('Y-m-d H:i:s');
        } else {
            $entity->data_alteracao = date('Y-m-d H:i:s');
        }
        return true;
    }
}
