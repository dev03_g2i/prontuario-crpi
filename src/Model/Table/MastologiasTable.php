<?php
namespace App\Model\Table;

use App\Model\Entity\Mastologia;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Mastologias Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ClienteHistoricos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 * @property \Cake\ORM\Association\BelongsTo $Users

 */
class MastologiasTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('mastologias');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ClienteHistoricos', [
            'foreignKey' => 'cliente_historico_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('ClienteHistoricos', [
            'foreignKey' => 'mastologia_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->boolean('nodulo_mama')
            ->allowEmpty('nodulo_mama');

        $validator
            ->boolean('nodulo_mama_direita')
            ->allowEmpty('nodulo_mama_direita');

        $validator
            ->boolean('nodulo_mama_esquerda')
            ->allowEmpty('nodulo_mama_esquerda');

        $validator
            ->allowEmpty('tempo_nodulo');

        $validator
            ->allowEmpty('crescimento');

        $validator
            ->boolean('alteracao_exame_imagem')
            ->allowEmpty('alteracao_exame_imagem');

        $validator
            ->allowEmpty('exames');

        $validator
            ->boolean('mastalgia')
            ->allowEmpty('mastalgia');

        $validator
            ->allowEmpty('tempo_mastalgia');

        $validator
            ->boolean('secrecao_mamilar')
            ->allowEmpty('secrecao_mamilar');

        $validator
            ->allowEmpty('carc_mamilar');

        $validator
            ->boolean('exame_rotina')
            ->allowEmpty('exame_rotina');

        $validator
            ->allowEmpty('outras_queixas');

        $validator
            ->integer('idade')
            ->allowEmpty('idade');

        $validator
            ->allowEmpty('g');

        $validator
            ->allowEmpty('p');

        $validator
            ->allowEmpty('a');

        $validator
            ->allowEmpty('c');

        $validator
            ->allowEmpty('dum');

        $validator
            ->allowEmpty('menarca');

        $validator
            ->allowEmpty('patologias');

        $validator
            ->allowEmpty('mac');

        $validator
            ->allowEmpty('medicacao');

        $validator
            ->allowEmpty('alergias');

        $validator
            ->integer('idade_parto')
            ->allowEmpty('idade_parto');

        $validator
            ->boolean('amamentacao')
            ->allowEmpty('amamentacao');

        $validator
            ->allowEmpty('tempo_amamentacao');

        $validator
            ->boolean('mastite')
            ->allowEmpty('mastite');

        $validator
            ->allowEmpty('localizacao');

        $validator
            ->boolean('biopsia_mama')
            ->allowEmpty('biopsia_mama');

        $validator
            ->allowEmpty('ap');

        $validator
            ->boolean('puncao_mama')
            ->allowEmpty('puncao_mama');

        $validator
            ->boolean('puncao_mama_direita')
            ->allowEmpty('puncao_mama_direita');

        $validator
            ->boolean('puncao_mama_esquerda')
            ->allowEmpty('puncao_mama_esquerda');

        $validator
            ->allowEmpty('punca_mama_obs');

        $validator
            ->allowEmpty('cirurgias');

        $validator
            ->boolean('tabagismo')
            ->allowEmpty('tabagismo');

        $validator
            ->allowEmpty('tempo_tabagismo');

        $validator
            ->allowEmpty('cigarros_dia');

        $validator
            ->allowEmpty('ultimo_preventivo');

        $validator
            ->allowEmpty('ultima_mamografia');

        $validator
            ->allowEmpty('cancer_mama_ovario');

        $validator
            ->allowEmpty('outros');

        $validator
            ->allowEmpty('peso');

        $validator
            ->allowEmpty('altura');

        $validator
            ->allowEmpty('pa');

        $validator
            ->boolean('abulamento')
            ->allowEmpty('abulamento');

        $validator
            ->boolean('abulamento_direita')
            ->allowEmpty('abulamento_direita');

        $validator
            ->boolean('abulamento_esquerda')
            ->allowEmpty('abulamento_esquerda');

        $validator
            ->boolean('retracao')
            ->allowEmpty('retracao');

        $validator
            ->boolean('retracao_direita')
            ->allowEmpty('retracao_direita');

        $validator
            ->boolean('retracao_esquerda')
            ->allowEmpty('retracao_esquerda');

        $validator
            ->allowEmpty('cam');

        $validator
            ->allowEmpty('papacao');

        $validator
            ->allowEmpty('expressao_papilar');

        $validator
            ->allowEmpty('ganglios');

        $validator
            ->allowEmpty('mamografia');

        $validator
            ->allowEmpty('ultrassonagrafia');

        $validator
            ->allowEmpty('citologia');

        $validator
            ->allowEmpty('histologia');

        $validator
            ->allowEmpty('hd');

        $validator
            ->allowEmpty('conduta');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cliente_historico_id'], 'ClienteHistoricos'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Mastologias') {
              $queryData->where(['Mastologias.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                $entity->situacao_id = 1;
                 if (!empty($_SESSION['Auth']['User']['id'])) {
                      $entity->user_id = $_SESSION['Auth']['User']['id'];
                 }
         }
        
        if(!empty($entity->dum)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->dum);
           $entity->dum = $now;
        }
         if(!empty($entity->ultimo_preventivo)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->ultimo_preventivo);
           $entity->ultimo_preventivo = $now;
        }
         if(!empty($entity->ultima_mamografia)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->ultima_mamografia);
           $entity->ultima_mamografia = $now;
        }
         return true;
    }
}
