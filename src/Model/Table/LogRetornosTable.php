<?php
namespace App\Model\Table;

use App\Model\Entity\LogRetorno;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LogRetornos Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Retornos
 * @property \Cake\ORM\Association\BelongsTo $Clientes
 * @property \Cake\ORM\Association\BelongsTo $GrupoRetornos
 * @property \Cake\ORM\Association\BelongsTo $SituacaoRetornos
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros
 */
class LogRetornosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('log_retornos');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Retornos', [
            'foreignKey' => 'retorno_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Clientes', [
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GrupoRetornos', [
            'foreignKey' => 'grupo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoRetornos', [
            'foreignKey' => 'situacao_retorno_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('previsao')
            ->allowEmpty('previsao');

        $validator
            ->integer('dias')
            ->allowEmpty('dias');

        $validator
            ->allowEmpty('observacao');

        $validator
            ->allowEmpty('justificativa');

        $validator
            ->date('data_confirmacao')
            ->allowEmpty('data_confirmacao');

        $validator
            ->integer('quem_aprovou')
            ->allowEmpty('quem_aprovou');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['retorno_id'], 'Retornos'));
        $rules->add($rules->existsIn(['cliente_id'], 'Clientes'));
        $rules->add($rules->existsIn(['grupo_id'], 'GrupoRetornos'));
        $rules->add($rules->existsIn(['situacao_retorno_id'], 'SituacaoRetornos'));
        return $rules;
    }

    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
                if(!empty($_SESSION['Auth']['User']['id'])) {
                    $entity->user_id = $_SESSION['Auth']['User']['id'];
                }
            $entity->situacao_id = 1;
        }
        if(!empty($entity->data)){
            $entity->data = implode('-',array_reverse(explode('/',$entity->data)));
        }
        return true;
    }
}
