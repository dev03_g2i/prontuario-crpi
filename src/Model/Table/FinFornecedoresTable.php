<?php

namespace App\Model\Table;

use App\Model\Entity\FinFornecedore;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FinFornecedores Model
 *

 */
class FinFornecedoresTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fin_fornecedores');
        $this->displayField('nome');
        $this->primaryKey('id');
        $this->belongsTo('FinPlanoContas', [
            'foreignKey' => 'fin_plano_conta_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->allowEmpty('nome');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 50)
            ->allowEmpty('telefone');

        $validator
            ->scalar('celular')
            ->maxLength('celular', 50)
            ->allowEmpty('celular');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 50)
            ->allowEmpty('cep');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade', 50)
            ->allowEmpty('cidade');

        $validator
            ->scalar('uf')
            ->maxLength('uf', 2)
            ->allowEmpty('uf');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->scalar('rua')
            ->maxLength('rua', 100)
            ->allowEmpty('rua');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 100)
            ->allowEmpty('bairro');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('cnpj')
            ->maxLength('cnpj', 50)
            ->allowEmpty('cnpj');

        $validator
            ->scalar('observacao')
            ->maxLength('observacao', 255)
            ->allowEmpty('observacao');

        $validator
            ->integer('situacao')
            ->allowEmpty('situacao');

        $validator
            ->scalar('cpf')
            ->maxLength('cpf', 15)
            ->allowEmpty('cpf');

        $validator
            ->scalar('banco')
            ->maxLength('banco', 100)
            ->allowEmpty('banco');

        $validator
            ->scalar('agencia')
            ->maxLength('agencia', 10)
            ->allowEmpty('agencia');

        $validator
            ->scalar('conta')
            ->maxLength('conta', 10)
            ->allowEmpty('conta');

        $validator
            ->scalar('operacao')
            ->maxLength('operacao', 10)
            ->allowEmpty('operacao');

        $validator
            ->scalar('fin_plano_conta_id')
            ->allowEmpty('fin_plano_conta_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fin_plano_conta_id'], 'FinPlanoContas'));
        return $rules;
    }

    public function beforeFind(Event $event, Query $queryData)
    {
        $outer = Router::getRequest();
        if ($outer->controller == 'FinFornecedores') {
            if (!empty($outer->query)) {
                foreach ($outer->query as $key => $value) {
                    if (!empty($value) && substr_count($key, '__') == 1) {
                        if (substr_count($value, '/')) {
                            if (substr_count($value, ':')) {
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i', $value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            } else {
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                            }
                        } else {
                            $queryData->andWhere(['UPPER(' . str_replace('__', '.', $key) . ') like' => '%' . strtoupper($value) . '%']);
                        }
                    }
                }
            }
        }
        $queryData->andWhere(['FinFornecedores.situacao' => 1])->order(['FinFornecedores.nome']);
        return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if ($entity->isNew()) {
        }

        return true;
    }
}
