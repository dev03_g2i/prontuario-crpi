<?php
namespace App\Model\Table;

use App\Model\Entity\ConvenioTipoimposto;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * ConvenioTipoimposto Model
 *
 * @property \Cake\ORM\Association\HasMany $Convenios

 */
class ConvenioTipoimpostoTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('convenio_tipoimposto');
        $this->displayField('descricao');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Convenios', [
            'foreignKey' => 'convenio_tipoimposto_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('descricao')
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->decimal('valor_inicial')
            ->requirePresence('valor_inicial', 'create')
            ->notEmpty('valor_inicial');

        $validator
            ->decimal('valor_final')
            ->requirePresence('valor_final', 'create')
            ->notEmpty('valor_final');

        $validator
            ->decimal('porcentual')
            ->requirePresence('porcentual', 'create')
            ->notEmpty('porcentual');

        $validator
            ->integer('retido')
            ->requirePresence('retido', 'create')
            ->notEmpty('retido');

        $validator
            ->scalar('observacao')
            ->allowEmpty('observacao');

        $validator
            ->integer('user_created')
            ->requirePresence('user_created', 'create')
            ->notEmpty('user_created');

        $validator
            ->integer('user_updated')
            ->allowEmpty('user_updated');

        return $validator;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='ConvenioTipoimposto') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        if($entity->isNew()) {
         }
        
        return true;
    }
}
