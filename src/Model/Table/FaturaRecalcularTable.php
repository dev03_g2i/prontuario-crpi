<?php
namespace App\Model\Table;

use App\Model\Entity\FaturaRecalcular;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * FaturaRecalcular Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Convenios
 * @property \Cake\ORM\Association\BelongsTo $SituacaoCadastros

 */
class FaturaRecalcularTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fatura_recalcular');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Convenios', [
            'foreignKey' => 'convenio_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('GrupoProcedimentos', [
            'foreignKey' => 'grupo_procedimento_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('SituacaoCadastros', [
            'foreignKey' => 'situacao_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('UserReg', [
            'className' => 'Users',
            'foreignKey' => 'user_created'
        ]);

        $this->belongsTo('UserAlt', [
            'className' => 'Users',
            'foreignKey' => 'user_update'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('periodo')
            ->allowEmpty('periodo');

        $validator
            ->integer('user_created')
            ->requirePresence('user_created', 'create')
            ->notEmpty('user_created');

        $validator
            ->integer('user_update')
            ->allowEmpty('user_update');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['convenio_id'], 'Convenios'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='FaturaRecalcular') {
              $queryData->where(['FaturaRecalcular.situacao_id' => 1]);
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];

        if(!empty($entity->id)){/* Update */
            $entity->user_update = $_SESSION['Auth']['User']['id'];
        }

        if($entity->isNew()) {/* Insert */
            $entity->situacao_id = 1;
            $entity->user_created = $_SESSION['Auth']['User']['id'];
        }

        if (!empty($entity->inicio)) {
            if (!is_object($entity->inicio)) {
                $entity->inicio = implode('-', array_reverse(explode('/', $entity->inicio)));
            }

        }
        if (!empty($entity->fim)) {
            if (!is_object($entity->fim)) {
                $entity->fim = implode('-', array_reverse(explode('/', $entity->fim)));
            }

        }
        
        return true;
    }
}
