<?php
namespace App\Model\Table;

use App\Model\Entity\Contabilidade;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contabilidade Model
 *
 * @property \Cake\ORM\Association\HasMany $Contasreceber
 * @property \Cake\ORM\Association\BelongsToMany $Banco
 */
class ContabilidadeTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('contabilidade');
        $this->displayField('nome');
        $this->primaryKey('id');

        $this->hasMany('Contasreceber', [
            'foreignKey' => 'contabilidade_id'
        ]);
        $this->belongsToMany('Banco', [
            'foreignKey' => 'contabilidade',
            'targetForeignKey' => 'banco',
            'joinTable' => 'contabilidade_banco'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('nome');

        $validator
            ->allowEmpty('descricao');

        $validator
            ->allowEmpty('cep');

        $validator
            ->allowEmpty('rua');

        $validator
            ->allowEmpty('bairro');

        $validator
            ->integer('numero')
            ->allowEmpty('numero');

        $validator
            ->allowEmpty('cidade');

        $validator
            ->allowEmpty('uf');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        return $validator;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'financeiro';
    }
}
