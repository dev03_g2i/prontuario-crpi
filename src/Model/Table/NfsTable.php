<?php
namespace App\Model\Table;

use App\Model\Entity\Nf;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Validation\Validator;
use DateTime;

/**
 * Nfs Model
 *
 * @property \Cake\ORM\Association\BelongsTo $NfPrestadores
 * @property \Cake\ORM\Association\BelongsTo $NfTomadores
 * @property \Cake\ORM\Association\BelongsTo $NfSituacaoNotas
 * @property \Cake\ORM\Association\BelongsTo $NfTipoRecolhimentos
 * @property \Cake\ORM\Association\BelongsTo $NfOperacoes
 * @property \Cake\ORM\Association\BelongsTo $NfTributacoes

 */
class NfsTable extends Table
{

    /**
     * testando geracao de codigos
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('nfs');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('NfPrestadores', [
            'foreignKey' => 'prestador_id'
        ]);
        $this->belongsTo('NfTomadores', [
            'foreignKey' => 'tomador_id'
        ]);
        $this->belongsTo('NfSituacaoNotas', [
            'foreignKey' => 'situacao_nota_id'
        ]);
        $this->belongsTo('NfTipoRecolhimentos', [
            'foreignKey' => 'tipo_recolhimento_id'
        ]);
        $this->belongsTo('NfOperacoes', [
            'foreignKey' => 'operacao_id'
        ]);
        $this->belongsTo('Atendimentos', [
            'foreignKey' => 'ficha'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id');

        $validator
            ->integer('ficha')
            ->allowEmpty('ficha');

        $validator
            ->integer('situacao_rps')
            ->allowEmpty('situacao_rps');

        $validator
            ->scalar('descricao_rps')
            ->maxLength('descricao_rps')
            ->allowEmpty('descricao_rps');

        $validator
            ->scalar('assinatura')
            ->maxLength('assinatura')
            ->allowEmpty('assinatura');

        $validator
            ->scalar('autorizacao')
            ->maxLength('autorizacao')
            ->allowEmpty('autorizacao');

        $validator
            ->scalar('autorizacao_cancelamento')
            ->maxLength('autorizacao_cancelamento')
            ->allowEmpty('autorizacao_cancelamento');

        $validator
            ->integer('numero_lote')
            ->allowEmpty('numero_lote');

        $validator
            ->integer('numero_rps')
            ->allowEmpty('numero_rps');

        $validator
            ->integer('numero_nfse')
            ->allowEmpty('numero_nfse');

        $validator
            ->allowEmpty('data_emissao_rps');

        $validator
            ->allowEmpty('data_processamento');

        $validator
            ->allowEmpty('data_cancelamento');

        $validator
            ->scalar('mot_cancelamento')
            ->allowEmpty('mot_cancelamento');

        $validator
            ->scalar('serie_rps')
            ->maxLength('serie_rps')
            ->allowEmpty('serie_rps');

        $validator
            ->scalar('serie_rps_substituido')
            ->maxLength('serie_rps_substituido')
            ->allowEmpty('serie_rps_substituido');

        $validator
            ->allowEmpty('numero_rps_substituido');

        $validator
            ->allowEmpty('numero_nfse_substituido');

        $validator
            ->allowEmpty('data_emissao_nfse_substituida');

        $validator
            ->scalar('serie_prestacao')
            ->maxLength('serie_prestacao')
            ->allowEmpty('serie_prestacao');

        $validator
            ->scalar('cnae')
            ->maxLength('cnae')
            ->allowEmpty('cnae');

        $validator
            ->scalar('descricao_cnae')
            ->allowEmpty('descricao_cnae');

        $validator
            ->decimal('aliquota_atividade')
            ->allowEmpty('aliquota_atividade');

        $validator
            ->decimal('aliquota_pis')
            ->allowEmpty('aliquota_pis');

        $validator
            ->decimal('aliquota_cofins')
            ->allowEmpty('aliquota_cofins');

        $validator
            ->decimal('aliquota_inss')
            ->allowEmpty('aliquota_inss');

        $validator
            ->decimal('aliquota_ir')
            ->allowEmpty('aliquota_ir');

        $validator
            ->decimal('aliquota_csll')
            ->allowEmpty('aliquota_csll');

        $validator
            ->decimal('valor_pis')
            ->allowEmpty('valor_pis');

        $validator
            ->decimal('valor_cofins')
            ->allowEmpty('valor_cofins');

        $validator
            ->decimal('valor_inss')
            ->allowEmpty('valor_inss');

        $validator
            ->decimal('valor_ir')
            ->allowEmpty('valor_ir');

        $validator
            ->decimal('valor_csll')
            ->allowEmpty('valor_csll');

        $validator
            ->decimal('valor_total_servicos')
            ->allowEmpty('valor_total_servicos');

        $validator
            ->decimal('valor_total_deducoes')
            ->allowEmpty('valor_total_deducoes');

        $validator
            ->boolean('enviar_email')
            ->allowEmpty('enviar_email');

        $validator
            ->scalar('nfse_arquivo')
            ->maxLength('nfse_arquivo')
            ->allowEmpty('nfse_arquivo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['prestador_id'], 'NfPrestadores'));
        $rules->add($rules->existsIn(['tomador_id'], 'NfTomadores'));
        $rules->add($rules->existsIn(['situacao_nota_id'], 'NfSituacaoNotas'));
        $rules->add($rules->existsIn(['tipo_recolhimento_id'], 'NfTipoRecolhimentos'));
        $rules->add($rules->existsIn(['operacao_id'], 'NfOperacoes'));
        return $rules;
    }

    public function beforeFind(Event $event,Query $queryData)
    {
      $outer = Router::getRequest();
     if($outer->controller=='Nfs') {
    if (!empty($outer->query)) {
           foreach ($outer->query as $key => $value) {
                if (!empty($value) && substr_count($key, '__') == 1) {
                    if(substr_count($value, '/')){
                          if(substr_count($value, ':')){
                                $data = new DateTime();
                                $value = $data->createFromFormat('d/m/Y H:i',$value);
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }else{
                                $value = implode('-', array_reverse(explode('/', $value)));
                                $queryData->andWhere([str_replace('__', '.', $key) => $value]);
                          }
                    }else{
                                $queryData->andWhere(['UPPER('.str_replace('__', '.', $key).') like' => '%'.strtoupper($value).'%']);
                    }
                }
           }
        }
     }
       return $queryData;
    }


    public function beforeSave(Event $event)
    {
        $entity = $event->data['entity'];
        $entity->user_id = $_SESSION['Auth']['User']['id'];
        if($entity->isNew()) {
            $entity->situacao_id = 1;
         }
        
        /*if(!is_object($entity->data_emissao_rps)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_emissao_rps);
           $entity->data_emissao_rps = $now;
        }
         if(!is_object($entity->data_processamento)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_processamento);
           $entity->data_processamento = $now;
        }
         if(!is_object($entity->data_cancelamento)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_cancelamento);
           $entity->data_cancelamento = $now;
        }
         if(!is_object($entity->data_emissao_nfse_substituida)){
           $inicio = new DateTime();
           $now = $inicio->createFromFormat('d/m/Y H:i',$entity->data_emissao_nfse_substituida);
           $entity->data_emissao_nfse_substituida = $now;
        }*/

         return true;
    }
}
