<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;

class DataBehavior extends Behavior
{
    function DataSQL($data) {
        try {
            if ($data != "") {
                list($d, $m, $y) = preg_split('/\//', $data);
                return sprintf('%04d-%02d-%02d', $y, $m, $d);
            }
        }catch (\Exception $e){
            return null;
        }
    }
}