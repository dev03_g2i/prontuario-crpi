<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;

class EncodingBehavior extends Behavior
{

    function afterFind(&$model, $results, $primary)
    {
        $this->r_utf8_encode($results);
        return $results;
    }

    function beforeSave(&$model)
    {
        $this->r_utf8_decode($model->data);
        return true;
    }

    function r_utf8_encode(&$array)
    {
        $keys = array_keys($array);
        var_dump($keys);
        for ($i = 0, $max = count($keys); $i < $max; $i++) {
            if (is_array($array[$keys[$i]])) {
                $this->r_utf8_encode($array[$keys[$i]]);
            } else {
                $array[$keys[$i]] = utf8_encode($array[$keys[$i]]);
            }
        }
    }

    function r_utf8_decode(&$array)
    {
        $keys = array_keys($array);
        for ($i = 0, $max = count($keys); $i < $max; $i++) {
            if (is_array($array[$keys[$i]])) {
                $this->r_utf8_decode($array[$keys[$i]]);
            } else {
                $array[$keys[$i]] = utf8_decode($array[$keys[$i]]);
            }
        }
    }

}

?>