<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfiguracaoPeriodosAgenda Entity.
 *
 * @property int $id
 * @property string $nome
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $hora_inicial
 * @property \Cake\I18n\FrozenTime $hora_final
 */
class ConfiguracaoPeriodosAgenda extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'situacao_id' => true,
        'hora_inicial' => true,
        'hora_final' => true,
        'situacao' => true,
    ];
}
