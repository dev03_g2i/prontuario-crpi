<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Laudo Entity.
 *
 * @property int $id
 * @property int $atendimento_procedimento_id
 * @property \App\Model\Entity\AtendimentoProcedimento $atendimento_procedimento
 * @property string $texto
 * @property string $rtf
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $assinado_por
 * @property \Cake\I18n\Time $dt_assinatura
 * @property string $texto_html
 * @property int $imagens
 * @property int $filmes
 * @property bool $cabecalho
 * @property int $digitado_por
 * @property int $papeis
 * @property string $rtf_html
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 */
class Laudo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
