<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;


/**
 * Cliente Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $sexo
 * @property \Cake\I18n\Time $nascimento
 * @property string $cpf
 * @property string $rg
 * @property string $telefone
 * @property string $celular
 * @property string $comercial
 * @property string $cep
 * @property string $endereco
 * @property int $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $cor
 * @property string $profissao
 * @property string $matricula
 * @property int $cod_municipio
 * @property \Cake\I18n\Time $validate_carteira
 * @property int $medico_id
 * @property \App\Model\Entity\MedicoResponsavei $medico_responsavei
 * @property int $convenio_id
 * @property \App\Model\Entity\Convenio $convenio
 * @property \App\Model\Entity\Religiao $religiao
 * @property string $nascionalidade
 * @property string $naturalidade
 * @property string $email
 * @property string $nome_mae
 * @property string $nome_pai
 * @property int $estadocivil_id
 * @property \App\Model\Entity\EstadoCivi $estado_civi
 * @property int $indicacao
 * @property string $login
 * @property string $password
 * @property bool $bloqueado
 * @property string $observacao
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Atendimento[] $atendimentos
 * @property \App\Model\Entity\ClienteAnexo[] $cliente_anexos
 * @property \App\Model\Entity\ClienteDocumento[] $cliente_documentos
 * @property \App\Model\Entity\ClienteHistorico[] $cliente_historicos
 * @property \App\Model\Entity\ClienteResponsavei[] $cliente_responsaveis
 */
class Cliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($value)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($value);
    }

    protected $_virtual = ['idade'];

    protected function _getIdade()
    {
        $idade = new Time($this->_properties['nascimento']);
        $diff = $idade->diff(new Time());
        return $diff->format("%y");
    }
    
}
