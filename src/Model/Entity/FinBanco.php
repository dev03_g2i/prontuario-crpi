<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinBanco Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $agencia
 * @property string $conta
 * @property float $limite
 * @property float $saldo
 * @property int $situacao
 * @property bool $mostra_fluxo
 * @property bool $mostra_saldo_geral
 */
class FinBanco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'agencia' => true,
        'conta' => true,
        'limite' => true,
        'saldo' => true,
        'situacao' => true,
        'mostra_fluxo' => true,
        'mostra_saldo_geral' => true,
        'fin_banco_movimentos' => true,
        'fin_contabilidade_bancos' => true,
        'fin_movimentos' => true,
    ];
}
