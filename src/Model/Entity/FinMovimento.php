<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * FinMovimento Entity.
 *
 * @property int $id
 * @property string $situacao
 * @property \Cake\I18n\FrozenDate $data
 * @property int $fin_plano_conta_id
 * @property int $fin_fornecedor_id
 * @property int $fin_banco_movimento_id
 * @property string $complemento
 * @property string $documento
 * @property float $credito
 * @property float $debito
 * @property int $fin_banco_id
 * @property int $status_lancamento
 * @property string $categoria
 * @property int $cliente_id
 * @property int $fin_contas_pagar_id
 * @property int $fin_contabilidade_id
 * @property \Cake\I18n\FrozenTime $data_criacao
 * @property \Cake\I18n\FrozenTime $data_alteracao
 * @property int $criado_por
 * @property int $atualizado_por
 */
class FinMovimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'situacao' => true,
        'data' => true,
        'fin_plano_conta_id' => true,
        'fin_fornecedor_id' => true,
        'fin_banco_movimento_id' => true,
        'complemento' => true,
        'documento' => true,
        'credito' => true,
        'debito' => true,
        'fin_banco_id' => true,
        'status_lancamento' => true,
        'categoria' => true,
        'cliente_id' => true,
        'fin_contas_pagar_id' => true,
        'fin_contabilidade_id' => true,
        'data_criacao' => true,
        'data_alteracao' => true,
        'criado_por' => true,
        'atualizado_por' => true,
        'fin_plano_conta' => true,
        'fin_fornecedor' => true,
        'fin_banco' => true,
        'fin_contas_pagar' => true,
        'fin_contabilidade' => true,
        'fin_banco_movimento' => true
    ];

    public $virtualFields = ['credor_cliente', 'banco_movimento_field'];

    //Função protegida que retorna o (credor ou|e cliente) para cada Movimento 
    protected function _getCredorCliente()
    {
        $FinMovimentos = TableRegistry::get('FinMovimentos');
        $movimento = $FinMovimentos->find()->contain(['FinFornecedores', 'Clientes'])
        ->select(['ciclano' => 
            'CASE
                WHEN FinMovimentos.fin_fornecedor_id IS NOT NULL and FinMovimentos.cliente_id IS NOT NULL
                    THEN CONCAT(FinFornecedores.nome, "/", Clientes.nome)
                WHEN FinMovimentos.fin_fornecedor_id IS NULL and FinMovimentos.cliente_id IS NOT NULL
                    THEN Clientes.nome
                WHEN FinMovimentos.fin_fornecedor_id IS NOT NULL and FinMovimentos.cliente_id IS NULL
                    THEN FinFornecedores.nome
                ELSE
                    ""
            END'])
        ->where(['FinMovimentos.id' => $this->_properties['id']])
        ->first();
        
        return $movimento->ciclano;
    }

    //Função protegida que retorna o banco movimento para cada Movimento 
    protected function _getBancoMovimentoField()
    {
        $FinMovimentos = TableRegistry::get('FinMovimentos');
        $movimento = $FinMovimentos->find()->contain(['FinBancos'])
        ->select(['banco_movimento_campo' => 'CONCAT(FinBancos.nome, " - ",
                IF(MONTH(FinMovimentos.data) < 10, CONCAT(0, MONTH(FinMovimentos.data)), MONTH(FinMovimentos.data)),"/",
                YEAR(FinMovimentos.data))'])
        ->where(['FinMovimentos.id' => $this->_properties['id']])
        ->first();

        return $movimento->banco_movimento_campo;
    }
}
