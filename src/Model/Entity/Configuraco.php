<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Configuraco Entity.
 *
 * @property int $id
 * @property $logo
 * @property string $logo_dir
 * @property string $nome_login
 * @property $icone
 * @property string $icone_dir
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $imagem
 * @property string $url_imagens
 * @property string $ficha_impressa_atendimento
 * @property int $usa_arquivo
 * @property int $arquivo
 * @property int $restringe_cpf
 * @property int $duplica_cpf
 * @property int $idade_cpf
 */
class Configuraco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
