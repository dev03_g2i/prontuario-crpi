<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mastologia Entity.
 *
 * @property int $id
 * @property int $cliente_historico_id
 * @property \App\Model\Entity\ClienteHistorico $cliente_historico
 * @property bool $nodulo_mama
 * @property bool $nodulo_mama_direita
 * @property bool $nodulo_mama_esquerda
 * @property string $tempo_nodulo
 * @property string $crescimento
 * @property bool $alteracao_exame_imagem
 * @property string $exames
 * @property bool $mastalgia
 * @property string $tempo_mastalgia
 * @property bool $secrecao_mamilar
 * @property string $carc_mamilar
 * @property bool $exame_rotina
 * @property string $outras_queixas
 * @property int $idade
 * @property string $g
 * @property string $p
 * @property string $a
 * @property string $c
 * @property \Cake\I18n\Time $dum
 * @property string $menarca
 * @property string $patologias
 * @property string $mac
 * @property string $medicacao
 * @property string $alergias
 * @property int $idade_parto
 * @property bool $amamentacao
 * @property string $tempo_amamentacao
 * @property bool $mastite
 * @property string $localizacao
 * @property bool $biopsia_mama
 * @property string $ap
 * @property bool $puncao_mama
 * @property bool $puncao_mama_direita
 * @property bool $puncao_mama_esquerda
 * @property string $punca_mama_obs
 * @property string $cirurgias
 * @property bool $tabagismo
 * @property string $tempo_tabagismo
 * @property string $cigarros_dia
 * @property \Cake\I18n\Time $ultimo_preventivo
 * @property \Cake\I18n\Time $ultima_mamografia
 * @property string $cancer_mama_ovario
 * @property string $outros
 * @property string $peso
 * @property string $altura
 * @property string $pa
 * @property bool $abulamento
 * @property bool $abulamento_direita
 * @property bool $abulamento_esquerda
 * @property bool $retracao
 * @property bool $retracao_direita
 * @property bool $retracao_esquerda
 * @property string $cam
 * @property string $papacao
 * @property string $expressao_papilar
 * @property string $ganglios
 * @property string $mamografia
 * @property string $ultrassonagrafia
 * @property string $citologia
 * @property string $histologia
 * @property string $hd
 * @property string $conduta
 * @property int $situacao_id
 * @property int $user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Mastologia extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
