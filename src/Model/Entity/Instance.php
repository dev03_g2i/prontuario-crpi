<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Instance Entity.
 *
 * @property int $pk
 * @property int $series_fk
 * @property int $srcode_fk
 * @property int $media_fk
 * @property string $sop_iuid
 * @property string $sop_cuid
 * @property string $inst_no
 * @property \Cake\I18n\Time $content_datetime
 * @property string $sr_complete
 * @property string $sr_verified
 * @property string $inst_custom1
 * @property string $inst_custom2
 * @property string $inst_custom3
 * @property string $ext_retr_aet
 * @property string $retrieve_aets
 * @property int $availability
 * @property int $inst_status
 * @property bool $all_attrs
 * @property bool $commitment
 * @property bool $archived
 * @property \Cake\I18n\Time $created_time
 * @property \Cake\I18n\Time $updated_time
 * @property string|resource $inst_attrs
 */
class Instance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'pk' => false,
    ];
}
