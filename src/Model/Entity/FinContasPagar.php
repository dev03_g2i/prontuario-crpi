<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinContasPagar Entity.
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $data
 * @property \Cake\I18n\FrozenDate $vencimento
 * @property float $valor
 * @property float $juros
 * @property float $multa
 * @property \Cake\I18n\FrozenDate $data_pagamento
 * @property int $fin_plano_conta_id
 * @property int $fin_fornecedor_id
 * @property int $situacao
 * @property string $complemento
 * @property int $fin_contabilidade_id
 * @property float $valor_bruto
 * @property float $desconto
 * @property int $fin_tipo_pagamento_id
 * @property int $fin_tipo_documento_id
 * @property string $numero_documento
 * @property \Cake\I18n\FrozenTime $data_cadastro
 * @property string $parcela
 * @property int $fin_contas_pagar_planejamento_id
 */
class FinContasPagar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'data' => true,
        'vencimento' => true,
        'valor' => true,
        'juros' => true,
        'multa' => true,
        'data_pagamento' => true,
        'fin_plano_conta_id' => true,
        'fin_fornecedor_id' => true,
        'situacao' => true,
        'complemento' => true,
        'fin_contabilidade_id' => true,
        'valor_bruto' => true,
        'desconto' => true,
        'fin_tipo_pagamento_id' => true,
        'fin_tipo_documento_id' => true,
        'numero_documento' => true,
        'data_cadastro' => true,
        'parcela' => true,
        'fin_contas_pagar_planejamento_id' => true,
        'fin_plano_conta' => true,
        'fin_fornecedor' => true,
        'fin_contabilidade' => true,
        'fin_tipo_pagamento' => true,
        'fin_tipo_documento' => true,
        'fin_contas_pagar_planejamento' => true,
        'fin_abatimentos_contas_pagar' => true,
        'fin_movimentos' => true,
    ];
}
