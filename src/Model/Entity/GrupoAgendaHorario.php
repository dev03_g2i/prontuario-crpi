<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GrupoAgendaHorario Entity.
 *
 * @property int $id
 * @property int $dia_semana
 * @property \Cake\I18n\FrozenDate $data_inicio
 * @property \Cake\I18n\FrozenDate $data_fim
 * @property \Cake\I18n\FrozenTime $hora_inicio
 * @property \Cake\I18n\FrozenTime $hora_fim
 * @property \Cake\I18n\FrozenTime $intervalo
 * @property int $grupo_id
 * @property int $tipo_agenda_id
 * @property int $situacao_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class GrupoAgendaHorario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dia_semana' => true,
        'data_inicio' => true,
        'data_fim' => true,
        'hora_inicio' => true,
        'hora_fim' => true,
        'intervalo' => true,
        'grupo_id' => true,
        'tipo_agenda_id' => true,
        'situacao_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'grupo_agenda' => true,
        'tipo_agenda' => true,
        'situacao_cadastro' => true,
        'user' => true,
        'agenda' => true,
    ];
}
