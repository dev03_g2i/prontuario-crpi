<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinAbatimentosContasPagar Entity.
 *
 * @property int $id
 * @property int $fin_abatimento_id
 * @property int $fin_contas_pagar_id
 * @property float $valor
 */
class FinAbatimentosContasPagar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fin_abatimento_id' => true,
        'fin_contas_pagar_id' => true,
        'valor' => true,
        'fin_abatimento' => true,
        'fin_contas_pagar' => true,
    ];
}
