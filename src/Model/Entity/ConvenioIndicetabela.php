<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConvenioIndicetabela Entity.
 *
 * @property int $id
 * @property int $convenio_id
 * @property int $grupo_procedimento_id
 * @property float $honorario_porte
 * @property float $operacional_uco
 * @property float $filme
 * @property int $user_insert
 * @property \Cake\I18n\FrozenTime $user_insert_dt
 * @property int $user_update
 * @property \Cake\I18n\FrozenTime $user_update_dt
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class ConvenioIndicetabela extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'convenio_id' => true,
        'grupo_procedimento_id' => true,
        'honorario_porte' => true,
        'operacional_uco' => true,
        'filme' => true,
        'user_insert' => true,
        'user_insert_dt' => true,
        'user_update' => true,
        'user_update_dt' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'convenio' => true,
        'grupo_procedimento' => true,
        'situacao_cadastro' => true,
    ];
}
