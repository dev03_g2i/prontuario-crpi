<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TempProdutividadeprofissionai Entity.
 *
 * @property int $id
 * @property int $atendimento_id
 * @property \App\Model\Entity\Atendimento $atendimento
 * @property int $convenio_id
 * @property \App\Model\Entity\Convenio $convenio
 * @property string $profissional
 * @property \Cake\I18n\Time $data_atendimento
 * @property \Cake\I18n\Time $hora
 * @property string $paciente
 * @property string $codigo
 * @property string $procedimento
 * @property int $quantidade
 * @property float $valor_fatura
 * @property float $valor_caixa
 * @property float $valor_material
 * @property float $total
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $situacao_id
 * @property \App\Model\Entity\Situacao $situacao
 * @property int $perc_recebimento
 * @property int $perc_imposto
 * @property float $valor_recebido_fatura
 * @property float $valor_recebido_caixa
 * @property float $valor_recebido_convenio
 */
class TempProdutividadeprofissionai extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
