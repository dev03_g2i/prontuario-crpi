<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Convenio Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $tiss_tipocodigo
 * @property string $tiss_registroans
 * @property string $tiss_codtabpreco
 * @property string $tiss_codcliconvenio
 * @property string $tiss_codtabmedicamento
 * @property string $tiss_codtabmaterial
 * @property string $tiss_codtaxas
 * @property int $tipo_id
 * @property \App\Model\Entity\TipoConvenio $tipo_convenio
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\Atendimento[] $atendimentos
 * @property \App\Model\Entity\PrecoProcedimento[] $preco_procedimentos
 */
class Convenio extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
