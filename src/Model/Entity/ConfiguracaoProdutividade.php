<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfiguracaoProdutividade Entity.
 *
 * @property int $id
 * @property int $usa_produtividade
 * @property int $produtividade_executante
 * @property int $produtividade_solicitante
 */
class ConfiguracaoProdutividade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'usa_produtividade' => true,
        'produtividade_executante' => true,
        'produtividade_solicitante' => true,
    ];
}
