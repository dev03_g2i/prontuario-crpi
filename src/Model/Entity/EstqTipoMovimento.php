<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EstqTipoMovimento Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property int $user_id
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class EstqTipoMovimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'user_id' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'situacao' => true,
    ];
}
