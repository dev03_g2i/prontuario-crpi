<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AtendimentoDocumento Entity.
 *
 * @property int $id
 * @property float $valor_total
 * @property int $meses
 * @property float $valor_parcela
 * @property int $vencimento
 * @property \Cake\I18n\FrozenDate $primeiro_pagamento
 * @property \Cake\I18n\FrozenDate $data_contrato
 * @property string $texto_contrato
 * @property int $atendimento_modelo_documento_id
 * @property int $atendimento_id
 * @property int $responsavel_id
 * @property int $situacao_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class AtendimentoDocumento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'valor_total' => true,
        'meses' => true,
        'valor_parcela' => true,
        'vencimento' => true,
        'primeiro_pagamento' => true,
        'data_contrato' => true,
        'texto_contrato' => true,
        'atendimento_modelo_documento_id' => true,
        'atendimento_id' => true,
        'responsavel_id' => true,
        'situacao_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'atendimento_modelo_documento' => true,
        'atendimento' => true,
        'responsavel' => true,
        'situacao' => true,
        'user' => true,
    ];
}
