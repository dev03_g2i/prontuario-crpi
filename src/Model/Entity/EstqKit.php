<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EstqKit Entity.
 *
 * @property int $id
 * @property string $nome
 * @property int $procedimento_id
 * @property int $user_id
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_update
 */
class EstqKit extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'procedimento_id' => true,
        'user_id' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'user_update' => true,
        'procedimento' => true,
        'user' => true,
        'situacao_cadastro' => true,
    ];
}
