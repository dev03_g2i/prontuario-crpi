<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfiguracaoInternacao Entity.
 *
 * @property int $id
 * @property int $carater_atendimento
 * @property int $carater_atendimento_padrao
 * @property int $acomodacao
 * @property int $acomodacao_padrao
 * @property int $motivo_alta
 * @property int $motivo_alta_padrao
 */
class ConfiguracaoInternacao extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'carater_atendimento' => true,
        'carater_atendimento_padrao' => true,
        'acomodacao' => true,
        'acomodacao_padrao' => true,
        'motivo_alta' => true,
        'motivo_alta_padrao' => true,
    ];
}
