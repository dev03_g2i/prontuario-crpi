<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AgendaFilaespera Entity.
 *
 * @property int $id
 * @property string $paciente
 * @property string $fone
 * @property int $convenio_id
 * @property int $tipo_espera_id
 * @property int $grupo_id
 * @property int $situacao_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $dt_agendado
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class AgendaFilaespera extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'paciente' => true,
        'fone' => true,
        'convenio_id' => true,
        'tipo_espera_id' => true,
        'grupo_id' => true,
        'situacao_id' => true,
        'user_id' => true,
        'dt_agendado' => true,
        'created' => true,
        'modified' => true,
        'convenio' => true,
        'tipo_espera' => true,
        'grupo' => true,
        'situacao' => true,
        'user' => true,
    ];
}
