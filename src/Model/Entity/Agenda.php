<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Agenda Entity.
 *
 * @property int $id
 * @property int $tipo_id
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 * @property int $profissional_id
 * @property \App\Model\Entity\MedicoResponsavei $medico_responsavei
 * @property int $grupo_id
 * @property \App\Model\Entity\GrupoAgenda $grupo_agenda
 * @property int $situacao_agenda_id
 * @property \App\Model\Entity\SituacaoAgenda $situacao_agenda
 * @property \Cake\I18n\Time $inicio
 * @property \Cake\I18n\Time $fim
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property string $observacao
 */
class Agenda extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
