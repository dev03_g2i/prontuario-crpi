<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TipoHistoriaModelo Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $modelo
 * @property int $tipo_historia_id
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $tipo_conteudo
 * @property string $texto_formulario
 * @property string $texto_procedimento
 * @property string $controller
 * @property string $action
 * @property \App\Model\Entity\TipoHistoriaModelo[] $tipo_historia_modelos
 */
class TipoHistoriaModelo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
