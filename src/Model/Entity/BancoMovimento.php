<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BancoMovimento Entity.
 *
 * @property int $id
 * @property int $banco_id
 * @property string $mes
 * @property string $ano
 * @property string $status
 * @property float $saldoInicial
 * @property float $saldoFinal
 * @property \Cake\I18n\Time $data
 * @property int $por
 * @property \Cake\I18n\Time $encerradoDt
 * @property int $encerradoPor
 * @property string $data_movimento
 * @property int $movimento_id
 */
class BancoMovimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'banco_id' => true,
        'mes' => true,
        'ano' => true,
        'status' => true,
        'saldoInicial' => true,
        'saldoFinal' => true,
        'data' => true,
        'por' => true,
        'encerradoDt' => true,
        'encerradoPor' => true,
        'data_movimento' => true,
        'movimento_id' => true,
        'banco' => true,
        'movimento' => true,
    ];
}
