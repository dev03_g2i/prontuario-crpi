<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * AtendimentoProcedimentoEquipe Entity.
 *
 * @property int $id
 * @property int $ordem
 * @property int $atendimento_procedimento_id
 * @property int $profissional_id
 * @property int $tipo_equipe_id
 * @property int $tipo_equipegrau_id
 * @property int $porcentagem
 * @property int $configuracao_apuracao_id
 * @property string $obs
 * @property int $user_insert
 * @property int $user_update
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class AtendimentoProcedimentoEquipe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ordem' => true,
        'atendimento_procedimento_id' => true,
        'profissional_id' => true,
        'tipo_equipe_id' => true,
        'tipo_equipegrau_id' => true,
        'porcentagem' => true,
        'configuracao_apuracao_id' => true,
        'obs' => true,
        'user_insert' => true,
        'user_update' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'atendimento_procedimento' => true,
        'medico_responsavei' => true,
        'tipo_equipe' => true,
        'tipo_equipegrau' => true,
        'configuracao_apuracao' => true,
        'situacao_cadastro' => true,
    ];

    protected $_virtual = ['valor_profissional'];

    protected function _getValorProfissional()
    {
        $atendimento_procedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $query = $atendimento_procedimentos->get($this->_properties['atendimento_procedimento_id']);
        $total = 0;
        if(!empty($query)){
            $porcentagem = $this->_properties['porcentagem'] / 100;
            $total = ($query->total + $query->valor_caixa) * $porcentagem;
        }
        return $total;
    }

}
