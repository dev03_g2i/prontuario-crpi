<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GastoIten Entity.
 *
 * @property int $id
 * @property int $gasto_id
 * @property \App\Model\Entity\Gasto $gasto
 * @property int $artigo_id
 * @property \App\Model\Entity\EstqArtigo $estq_artigo
 * @property float $quantidade
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\Situacao $situacao
 */
class GastoIten extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
