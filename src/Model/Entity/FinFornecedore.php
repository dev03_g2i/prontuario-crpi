<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinFornecedore Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $telefone
 * @property string $celular
 * @property string $cep
 * @property string $cidade
 * @property string $uf
 * @property int $numero
 * @property string $rua
 * @property string $bairro
 * @property string $email
 * @property string $cnpj
 * @property string $observacao
 * @property int $situacao
 * @property string $cpf
 * @property string $banco
 * @property string $agencia
 * @property string $conta
 * @property string $operacao
 * @property int $fin_plano_conta_id
 */
class FinFornecedore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'telefone' => true,
        'celular' => true,
        'cep' => true,
        'cidade' => true,
        'uf' => true,
        'numero' => true,
        'rua' => true,
        'bairro' => true,
        'email' => true,
        'cnpj' => true,
        'observacao' => true,
        'situacao' => true,
        'cpf' => true,
        'banco' => true,
        'agencia' => true,
        'conta' => true,
        'operacao' => true,
        'fin_plano_conta_id' => true,
        'fin_plano_conta' => true
    ];
}
