<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinContasReceber Entity.
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $data
 * @property float $valor
 * @property int $fin_plano_conta_id
 * @property int $cliente_id
 * @property int $situacao
 * @property string $complemento
 * @property \Cake\I18n\FrozenDate $data_pagamento
 * @property int $fin_contabilidade_id
 * @property \Cake\I18n\FrozenDate $vencimento
 * @property string $documento
 * @property string $numdoc
 * @property string $parcela
 * @property int $forma_pagamento
 * @property int $boleto_mensagem
 * @property string $boleto_mensagem_livre
 * @property string $boleto_barras
 * @property string $boletolinhadigitavel
 * @property string $boleto_nosso_numero
 * @property float $juros
 * @property float $multa
 * @property int $boleto_cedente
 * @property string $numero_fiscal
 * @property \Cake\I18n\FrozenDate $data_fiscal
 * @property float $valor_bruto
 * @property float $desconto
 * @property int $correcao_monetaria
 * @property int $fin_tipo_pagamento_id
 * @property int $fin_tipo_documento_id
 * @property string $numero_documento
 * @property int $fin_planejamento_id
 * @property int $impresso
 * @property float $valorPagar
 * @property \Cake\I18n\FrozenDate $vencimento_boleto
 * @property int $perjuros
 * @property int $permulta
 * @property \Cake\I18n\FrozenTime $data_cadastro
 * @property int $sis_antigo_cx_boleto
 * @property int $sis_antigo_cx
 * @property int $extern_id
 * @property int $user_created
 * @property int $user_modified
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $controle_financeiro_id
 * @property int $origem_registro
 * @property int $fatura_id
 */
class FinContasReceber extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'data' => true,
        'valor' => true,
        'fin_plano_conta_id' => true,
        'cliente_id' => true,
        'situacao' => true,
        'complemento' => true,
        'data_pagamento' => true,
        'fin_contabilidade_id' => true,
        'vencimento' => true,
        'documento' => true,
        'numdoc' => true,
        'parcela' => true,
        'forma_pagamento' => true,
        'boleto_mensagem' => true,
        'boleto_mensagem_livre' => true,
        'boleto_barras' => true,
        'boletolinhadigitavel' => true,
        'boleto_nosso_numero' => true,
        'juros' => true,
        'multa' => true,
        'boleto_cedente' => true,
        'numero_fiscal' => true,
        'data_fiscal' => true,
        'valor_bruto' => true,
        'desconto' => true,
        'correcao_monetaria' => true,
        'fin_tipo_pagamento_id' => true,
        'fin_tipo_documento_id' => true,
        'numero_documento' => true,
        'fin_planejamento_id' => true,
        'impresso' => true,
        'valorPagar' => true,
        'vencimento_boleto' => true,
        'perjuros' => true,
        'permulta' => true,
        'data_cadastro' => true,
        'sis_antigo_cx_boleto' => true,
        'sis_antigo_cx' => true,
        'extern_id' => true,
        'user_created' => true,
        'user_modified' => true,
        'created' => true,
        'modified' => true,
        'controle_financeiro_id' => true,
        'origem_registro' => true,
        'fatura_id' => true,
        'fin_plano_conta' => true,
        'cliente' => true,
        'contabilidade' => true,
        'fin_planejamento' => true,
        'extern' => true,
        'controle_financeiro' => true,
        'fatura' => true,
        'fin_abatimentos_contas_receber' => true,
    ];
}
