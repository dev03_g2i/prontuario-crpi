<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Movimento Entity.
 *
 * @property int $id
 * @property string $situacao
 * @property \Cake\I18n\FrozenDate $data
 * @property int $planoconta_id
 * @property int $fornecedor_id
 * @property string $complemento
 * @property string $documento
 * @property float $credito
 * @property float $debito
 * @property int $banco_id
 * @property int $status_lancamento
 * @property string $categoria
 * @property int $idCliente
 * @property int $contaspagar_id
 * @property int $contabilidade_id
 * @property \Cake\I18n\FrozenTime $data_criacao
 * @property \Cake\I18n\FrozenTime $data_alteracao
 * @property int $criado_por
 * @property int $atualizado_por
 */
class Movimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'situacao' => true,
        'data' => true,
        'planoconta_id' => true,
        'fornecedor_id' => true,
        'complemento' => true,
        'documento' => true,
        'credito' => true,
        'debito' => true,
        'banco_id' => true,
        'status_lancamento' => true,
        'categoria' => true,
        'idCliente' => true,
        'contaspagar_id' => true,
        'contabilidade_id' => true,
        'data_criacao' => true,
        'data_alteracao' => true,
        'criado_por' => true,
        'atualizado_por' => true,
        'planoconta' => true,
        'fornecedor' => true,
        'banco' => true,
        'contaspagar' => true,
        'contabilidade' => true,
        'banco_movimentos' => true,
    ];
}
