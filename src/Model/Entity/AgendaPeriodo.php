<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AgendaPeriodo Entity.
 *
 * @property int $id
 * @property int $dia_semana
 * @property int $periodo
 * @property \Cake\I18n\Time $intervalo
 * @property \Cake\I18n\Time $inicio
 * @property \Cake\I18n\Time $fim
 * @property int $grupo_agenda_id
 * @property \App\Model\Entity\GrupoAgenda $grupo_agenda
 * @property int $situacao_id
 * @property int $user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class AgendaPeriodo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
