<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Study Entity.
 *
 * @property int $pk
 * @property int $patient_fk
 * @property int $accno_issuer_fk
 * @property string $study_iuid
 * @property string $study_id
 * @property \Cake\I18n\Time $study_datetime
 * @property string $accession_no
 * @property string $ref_physician
 * @property string $ref_phys_fn_sx
 * @property string $ref_phys_gn_sx
 * @property string $ref_phys_i_name
 * @property string $ref_phys_p_name
 * @property string $study_desc
 * @property string $study_custom1
 * @property string $study_custom2
 * @property string $study_custom3
 * @property string $study_status_id
 * @property \App\Model\Entity\StudyStatus $study_status
 * @property string $mods_in_study
 * @property string $cuids_in_study
 * @property int $num_series
 * @property int $num_instances
 * @property string $ext_retr_aet
 * @property string $retrieve_aets
 * @property string $fileset_iuid
 * @property string $fileset_id
 * @property \App\Model\Entity\Fileset $fileset
 * @property int $availability
 * @property \Cake\I18n\Time $checked_time
 * @property \Cake\I18n\Time $created_time
 * @property \Cake\I18n\Time $updated_time
 * @property string|resource $study_attrs
 * @property \App\Model\Entity\Study[] $study
 */
class Study extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'pk' => false,
    ];
}
