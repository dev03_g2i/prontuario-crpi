<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TempFaturamento Entity.
 *
 * @property int $id
 * @property int $selecionado
 * @property int $atendimento_id
 * @property \App\Model\Entity\Atendimento $atendimento
 * @property string $convenio
 * @property string $profissional
 * @property \Cake\I18n\Time $data_atendimento
 * @property \Cake\I18n\Time $hora
 * @property string $paciente
 * @property string $matricula
 * @property string $codigo_tabela
 * @property string $procedimento
 * @property string $guia
 * @property string $senha
 * @property int $quantidade
 * @property float $valor_fatura
 * @property float $valor_material
 * @property float $total
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class TempFaturamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
