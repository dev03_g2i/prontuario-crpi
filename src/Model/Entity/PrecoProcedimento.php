<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PrecoProcedimento Entity.
 *
 * @property int $id
 * @property int $convenio_id
 * @property \App\Model\Entity\Convenio $convenio
 * @property int $procedimento_id
 * @property \App\Model\Entity\Procedimento $procedimento
 * @property float $valor_faturar
 * @property float $valor_particular
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 */
class PrecoProcedimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
