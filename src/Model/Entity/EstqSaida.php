<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EstqSaida Entity.
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $data
 * @property int $artigo_id
 * @property int $quantidade
 * @property float $vl_custos
 * @property float $vl_venda
 * @property float $vl_custo_medico
 * @property int $origin
 * @property string $codigo_tiss
 * @property string $codigo_tuss
 * @property string $codigo_convenio
 * @property int $tipo_movimento_id
 * @property int $atendimento_procedimento_id
 * @property int $user_id
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $atendimento_id
 * @property float $total
 * @property int $qtd_recebida
 * @property float $vl_recebido
 * @property \Cake\I18n\FrozenDate $dt_recebimento
 * @property int $exportado_fatura
 */
class EstqSaida extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'data' => true,
        'artigo_id' => true,
        'quantidade' => true,
        'vl_custos' => true,
        'vl_venda' => true,
        'vl_custo_medico' => true,
        'origin' => true,
        'codigo_tiss' => true,
        'codigo_tuss' => true,
        'codigo_convenio' => true,
        'tipo_movimento_id' => true,
        'atendimento_procedimento_id' => true,
        'user_id' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'atendimento_id' => true,
        'total' => true,
        'qtd_recebida' => true,
        'vl_recebido' => true,
        'dt_recebimento' => true,
        'exportado_fatura' => true,
        'estq_tipo_movimento' => true,
        'atendimento_procedimento' => true,
        'situacao_cadastro' => true,
        'estq_artigo' => true,
    ];
}
