<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * FaturaEncerramento Entity.
 *
 * @property int $id
 * @property string $competencia
 * @property \Cake\I18n\Time $vencimento
 * @property string $observacao
 * @property \Cake\I18n\Time $data_encerramento
 * @property float $total_bruto
 * @property int $num_exames
 * @property int $convenio_id
 * @property \App\Model\Entity\Convenio $convenio
 * @property float $taxas
 * @property float $descontos
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $qtd_total
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $situacao_faturaencerramento_id
 * @property \App\Model\Entity\SituacaoFaturaencerramento $situacao_faturaencerramento
 * @property int $unidade_id
 * @property \App\Model\Entity\AtendimentoProcedimento[] $atendimento_procedimentos
 * @property \App\Model\Entity\TempFaturamento[] $temp_faturamentos
 */
class FaturaEncerramento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    public $virtualFields = ['total_previsto'];

    //Função protegida que retorna o nome concatenado com a data_movimento
    protected function _getTotalPrevisto()
    {
        $FaturaEncerramentos = TableRegistry::get('FaturaEncerramentos');
        $faturaEncerramento = $FaturaEncerramentos->get($this->_properties['id']);

        $ConvenioImpostosParametro = TableRegistry::get('ConvenioImpostosParametro');
        $convenioImpostosParametro = $ConvenioImpostosParametro->find()
        ->select(['total_imposto' => 'SUM(ConvenioImpostosParametro.porcentual)'])
        ->where(['ConvenioImpostosParametro.id_convenio' => $this->_properties['convenio_id']])
        ->first();

        $total_imposto = floatval($convenioImpostosParametro->total_imposto);
        $faturaEncerramento->total_bruto = $faturaEncerramento->total_bruto - ($faturaEncerramento->total_bruto * $total_imposto/100);
        
        return $faturaEncerramento->total_bruto;
    }
}
