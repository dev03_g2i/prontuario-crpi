<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TipoEquipegrau Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property string $codigo_tiss
 * @property int $user_insert
 * @property int $user_update
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class TipoEquipegrau extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'codigo_tiss' => true,
        'user_insert' => true,
        'user_update' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'situacao' => true,
        'atendimento_procedimento_equipes' => true,
    ];
}
