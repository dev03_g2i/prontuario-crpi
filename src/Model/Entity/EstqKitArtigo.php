<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EstqKitArtigo Entity.
 *
 * @property int $id
 * @property int $fatura_kit_id
 * @property int $artigo_id
 * @property int $quantidade
 * @property int $user_id
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class EstqKitArtigo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fatura_kit_id' => true,
        'artigo_id' => true,
        'quantidade' => true,
        'user_id' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'fatura_kit' => true,
        'artigo' => true,
        'user' => true,
        'situacao' => true,
    ];
}
