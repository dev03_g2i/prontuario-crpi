<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Contaspagar Entity.
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $data
 * @property \Cake\I18n\FrozenDate $vencimento
 * @property float $valor
 * @property float $juros
 * @property float $multa
 * @property \Cake\I18n\FrozenDate $data_pagamento
 * @property int $planoconta_id
 * @property int $fornecedor_id
 * @property int $situacao
 * @property string $complemento
 * @property int $contabilidade_id
 * @property float $valor_bruto
 * @property float $desconto
 * @property int $tipo_pagamento_id
 * @property int $tipo_documento_id
 * @property string $numero_documento
 * @property \Cake\I18n\FrozenTime $data_cadastro
 * @property string $parcela
 * @property int $contaspagar_planejamento_id
 */
class Contaspagar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'data' => true,
        'vencimento' => true,
        'valor' => true,
        'juros' => true,
        'multa' => true,
        'data_pagamento' => true,
        'planoconta_id' => true,
        'fornecedor_id' => true,
        'situacao' => true,
        'complemento' => true,
        'contabilidade_id' => true,
        'valor_bruto' => true,
        'desconto' => true,
        'tipo_pagamento_id' => true,
        'tipo_documento_id' => true,
        'numero_documento' => true,
        'data_cadastro' => true,
        'parcela' => true,
        'contaspagar_planejamento_id' => true,
        'planoconta' => true,
        'fornecedore' => true,
        'contabilidade' => true,
        'tipo_pagamento' => true,
        'tipo_documento' => true,
        'contaspagar_planejamento' => true,
        'movimentos' => true,
        'abatimentos' => true,
    ];

    public $virtualFields = ['valor_deducao'];

    protected function _getValorDeducao()
    {
        $Contaspagar = TableRegistry::get('Contaspagar');
        $contaspagar = $Contaspagar->find()
        ->select(['somaValores' => 'sum(valor_bruto + juros + multa - desconto)'])
        ->where(['id' => $this->_properties['id']])
        ->first();

        return number_format($contaspagar->somaValores, 2, ',', '.');
    }
}
