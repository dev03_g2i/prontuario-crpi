<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinContabilidadeBanco Entity.
 *
 * @property int $id
 * @property int $fin_contabilidade_id
 * @property int $fin_banco_id
 */
class FinContabilidadeBanco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fin_contabilidade_id' => true,
        'fin_banco_id' => true,
        'fin_contabilidade' => true,
        'fin_banco' => true,
    ];
}
