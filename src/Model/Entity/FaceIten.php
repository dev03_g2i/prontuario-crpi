<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FaceIten Entity.
 *
 * @property int $id
 * @property int $iten_id
 * @property \App\Model\Entity\AtendimentoIten $atendimento_iten
 * @property int $face_id
 * @property \App\Model\Entity\Face $face
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FaceIten extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
