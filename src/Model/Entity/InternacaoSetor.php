<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InternacaoSetor Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property int $situacao_id
 * @property string $cor_mapa
 * @property string $cor_painel
 * @property int $user_reg
 * @property int $user_update
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class InternacaoSetor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'situacao_id' => true,
        'cor_mapa' => true,
        'cor_painel' => true,
        'user_reg' => true,
        'user_update' => true,
        'created' => true,
        'modified' => true,
        'situacao_cadastro' => true,
        'atendimentos' => true,
    ];
}
