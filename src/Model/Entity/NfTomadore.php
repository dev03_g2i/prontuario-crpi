<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NfTomadore Entity.
 *
 * @property int $id
 * @property string $razao_social
 * @property string $inscricao_municipal
 * @property string $cpfcnpj
 * @property string $doc_tomador_estrangeiro
 * @property int $tipo_logradouro_id
 * @property string $logradouro
 * @property int $numero
 * @property string $complemento
 * @property int $tipo_bairro_id
 * @property string $bairro
 * @property int $cidade_id
 * @property string $cep
 * @property string $email
 * @property string $ddd
 * @property string $telefone
 * @property string $texto_nf
 * @property int $cliente_id
 * @property int $responsavel_id
 * @property int $tipo_tomador
 */
class NfTomadore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'razao_social' => true,
        'inscricao_municipal' => true,
        'cpfcnpj' => true,
        'doc_tomador_estrangeiro' => true,
        'tipo_logradouro_id' => true,
        'logradouro' => true,
        'numero' => true,
        'complemento' => true,
        'tipo_bairro_id' => true,
        'bairro' => true,
        'cidade_id' => true,
        'cep' => true,
        'email' => true,
        'ddd' => true,
        'telefone' => true,
        'texto_nf' => true,
        'cliente_id' => true,
        'responsavel_id' => true,
        'tipo_tomador' => true,
        'tipo_logradouro' => true,
        'tipo_bairro' => true,
        'cidade' => true,
        'cliente' => true,
        'responsavel' => true,
    ];
}
