<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinTipoDocumento Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property int $status_id
 * @property string $usado
 */
class FinTipoDocumento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'status_id' => true,
        'usado' => true,
        'status' => true,
        'fin_contas_pagar' => true,
    ];
}
