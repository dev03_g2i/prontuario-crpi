<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FaturaNf Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $data
 * @property float $valor_bruto
 * @property float $imposto_nao_retido
 * @property float $imposto_retido
 * @property float $descontos
 * @property float $valor_liquido
 * @property string $obs
 * @property int $unidade_id
 * @property int $fatura_encerramento_id
 * @property \App\Model\Entity\FaturaEncerramento $fatura_encerramento
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property string $numero_nf
 */
class FaturaNf extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
