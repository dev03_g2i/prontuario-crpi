<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AgendaTel Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $address
 * @property string $district
 * @property string $zipCode
 * @property string $city
 * @property string $state
 * @property string $phones
 * @property string $email
 * @property string $category
 * @property string $observation
 */
class AgendaTel extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'address' => true,
        'district' => true,
        'zipCode' => true,
        'city' => true,
        'state' => true,
        'phones' => true,
        'email' => true,
        'category' => true,
        'observation' => true,
    ];
}
