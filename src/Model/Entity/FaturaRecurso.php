<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FaturaRecurso Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $data
 * @property float $valor
 * @property string $obs
 * @property int $situacao_fatura_recurso_id
 * @property \App\Model\Entity\SituacaoFaturaRecurso $situacao_fatura_recurso
 * @property int $atendimento_procedimento_id
 * @property \App\Model\Entity\AtendimentoProcedimento $atendimento_procedimento
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FaturaRecurso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
