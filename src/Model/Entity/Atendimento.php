<?php
namespace App\Model\Entity;

use Cake\I18n\Time;
use Cake\ORM\Entity;


/**
 * Atendimento Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $hora
 * @property \Cake\I18n\Time $data
 * @property string $observacao
 * @property string $tiss_tipoatendimento
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 * @property int $unidade_id
 * @property \App\Model\Entity\Unidade $unidade
 * @property float $total_liquido
 * @property float $desconto
 * @property float $total_geral
 * @property int $convenio_id
 * @property \App\Model\Entity\Convenio $convenio
 * @property float $total_pagoato
 * @property int $tipoatendimento_id
 * @property \App\Model\Entity\TipoAtendimento $tipo_atendimento
 * @property int $num_ficha_externa
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property float $created
 * @property float $modified
 * @property \App\Model\Entity\AtendimentoProcedimento[] $atendimento_procedimentos
 */
class Atendimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected function _getTotalAreceber()
    {
        return $this->_properties['total_liquido'] - $this->_properties['total_pagoato'];
    }

    protected function _getHoraFormatada()
    {
        $now = new Time($this->_properties['hora']);
        return $now->format('H:i:s');
    }

    protected function _getDiaAtendimento()
    {
        $now = new Time($this->_properties['data']);
        return $now->format('d');
    }
    protected function _getNumMesAtendimento()
    {
        $now = new Time($this->_properties['data']);
        return $now->format('m');
    }
    protected function _getAnoAtendimento()
    {
        $now = new Time($this->_properties['data']);
        return $now->format('Y');
    }

}
