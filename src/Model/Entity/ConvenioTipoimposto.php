<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConvenioTipoimposto Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property float $valor_inicial
 * @property float $valor_final
 * @property float $porcentual
 * @property int $retido
 * @property string $observacao
 * @property int $user_created
 * @property int $user_updated
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class ConvenioTipoimposto extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'valor_inicial' => true,
        'valor_final' => true,
        'porcentual' => true,
        'retido' => true,
        'observacao' => true,
        'user_created' => true,
        'user_updated' => true,
        'created' => true,
        'modified' => true,
        'convenios' => true,
    ];
}
