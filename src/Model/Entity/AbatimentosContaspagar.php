<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AbatimentosContaspagar Entity.
 *
 * @property int $id
 * @property int $abatimento_id
 * @property int $contaspagar_id
 * @property float $valor
 */
class AbatimentosContaspagar extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'abatimento_id' => true,
        'contaspagar_id' => true,
        'valor' => true,
        'abatimento' => true,
        'contaspagar' => true,
    ];
}
