<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogLaudo Entity.
 *
 * @property int $id
 * @property int $laudo_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string $texto
 * @property string $rtf
 * @property int $user_id
 * @property int $assinado_por
 * @property \Cake\I18n\FrozenTime $dt_assinatura
 * @property string $texto_html
 * @property int $imagens
 * @property int $filme
 * @property int $papel
 * @property string $rtf_html
 * @property int $paginas
 */
class LogLaudo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'laudo_id' => true,
        'created' => true,
        'modified' => true,
        'texto' => true,
        'rtf' => true,
        'user_id' => true,
        'assinado_por' => true,
        'dt_assinatura' => true,
        'texto_html' => true,
        'imagens' => true,
        'filme' => true,
        'papel' => true,
        'rtf_html' => true,
        'paginas' => true,
        'laudo' => true,
    ];
}
