<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OperacaoAgendaHorario Entity.
 *
 * @property int $id
 * @property string $nome
 * @property int $situacao_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class OperacaoAgendaHorario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'situacao_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'situacao_cadastro' => true,
        'user' => true,
        'grupo_agenda_horarios' => true,
    ];
}
