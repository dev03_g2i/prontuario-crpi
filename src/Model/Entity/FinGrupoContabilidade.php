<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinGrupoContabilidade Entity.
 *
 * @property int $id
 * @property int $fin_grupo_id
 * @property int $fin_contabilidade_id
 */
class FinGrupoContabilidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fin_grupo_id' => true,
        'fin_contabilidade_id' => true,
        'grupo' => true,
        'fin_contabilidade' => true,
    ];
}
