<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FaturaRecalcular Entity.
 *
 * @property int $id
 * @property int $convenio_id
 * @property string $periodo
 * @property int $user_created
 * @property int $user_update
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class FaturaRecalcular extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'convenio_id' => true,
        'inicio' => true,
        'fim' => true,
        'user_created' => true,
        'user_update' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'convenio' => true,
        'situacao_cadastro' => true,
    ];

    protected function _getPeriodo()
    {
        return $this->_properties['inicio'].' - '.$this->_properties['fim'];
    }
}
