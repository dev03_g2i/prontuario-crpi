<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinPlanoConta Entity.
 *
 * @property int $id
 * @property int $fin_classificacao_conta_id
 * @property int $situacao
 * @property string $nome
 * @property int $rateio
 */
class FinPlanoConta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fin_classificacao_conta_id' => true,
        'situacao' => true,
        'nome' => true,
        'rateio' => true,
        'fin_contas_pagar' => true,
        'fin_contas_pagar_planejamentos' => true,
        'fin_contas_receber' => true,
        'fin_contas_receber_planejamentos' => true,
        'fin_movimentos' => true,
        'fin_planejamentos' => true,
    ];
}
