<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinGrupo Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property int $situacao
 * @property \Cake\I18n\FrozenTime $dtcriacao
 * @property string $criadopor
 * @property \Cake\I18n\FrozenTime $dtalteracao
 * @property string $alteradopor
 */
class FinGrupo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'situacao' => true,
        'dtcriacao' => true,
        'criadopor' => true,
        'dtalteracao' => true,
        'alteradopor' => true,
    ];
}
