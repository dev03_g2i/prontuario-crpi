<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProfissionalCredito Entity.
 *
 * @property int $id
 * @property int $profissional_id
 * @property \App\Model\Entity\MedicoResponsavei $medico_responsavei
 * @property \Cake\I18n\Time $data
 * @property float $credito
 * @property float $debito
 * @property string $descricao
 * @property string $observacao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property bool $status
 */
class ProfissionalCredito extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
