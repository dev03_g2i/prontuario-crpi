<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Nf Entity.
 *
 * @property int $id
 * @property int $prestador_id
 * @property int $tomador_id
 * @property int $ficha
 * @property int $situacao_nota_id
 * @property int $situacao_rps
 * @property string $descricao_rps
 * @property string $assinatura
 * @property string $autorizacao
 * @property string $autorizacao_cancelamento
 * @property int $numero_lote
 * @property int $numero_rps
 * @property int $numero_nfse
 * @property \Cake\I18n\FrozenTime $data_emissao_rps
 * @property \Cake\I18n\FrozenTime $data_processamento
 * @property \Cake\I18n\FrozenTime $data_cancelamento
 * @property string $mot_cancelamento
 * @property string $serie_rps
 * @property string $serie_rps_substituido
 * @property int $numero_rps_substituido
 * @property int $numero_nfse_substituido
 * @property \Cake\I18n\FrozenTime $data_emissao_nfse_substituida
 * @property string $serie_prestacao
 * @property string $cnae
 * @property string $descricao_cnae
 * @property int $tipo_recolhimento_id
 * @property int $cidade_id
 * @property int $operacao_id
 * @property int $tributacao_id
 * @property float $aliquota_atividade
 * @property float $aliquota_pis
 * @property float $aliquota_cofins
 * @property float $aliquota_inss
 * @property float $aliquota_ir
 * @property float $aliquota_csll
 * @property float $valor_pis
 * @property float $valor_cofins
 * @property float $valor_inss
 * @property float $valor_ir
 * @property float $valor_csll
 * @property float $valor_total_servicos
 * @property float $valor_total_deducoes
 * @property bool $enviar_email
 * @property string $nfse_arquivo
 */
class Nf extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'prestador_id' => true,
        'tomador_id' => true,
        'ficha' => true,
        'situacao_nota_id' => true,
        'situacao_rps' => true,
        'descricao_rps' => true,
        'assinatura' => true,
        'autorizacao' => true,
        'autorizacao_cancelamento' => true,
        'numero_lote' => true,
        'numero_rps' => true,
        'numero_nfse' => true,
        'data_emissao_rps' => true,
        'data_processamento' => true,
        'data_cancelamento' => true,
        'mot_cancelamento' => true,
        'serie_rps' => true,
        'serie_rps_substituido' => true,
        'numero_rps_substituido' => true,
        'numero_nfse_substituido' => true,
        'data_emissao_nfse_substituida' => true,
        'serie_prestacao' => true,
        'cnae' => true,
        'descricao_cnae' => true,
        'tipo_recolhimento_id' => true,
        'cidade_id' => true,
        'operacao_id' => true,
        'tributacao_id' => true,
        'aliquota_atividade' => true,
        'aliquota_pis' => true,
        'aliquota_cofins' => true,
        'aliquota_inss' => true,
        'aliquota_ir' => true,
        'aliquota_csll' => true,
        'valor_pis' => true,
        'valor_cofins' => true,
        'valor_inss' => true,
        'valor_ir' => true,
        'valor_csll' => true,
        'valor_total_servicos' => true,
        'valor_total_deducoes' => true,
        'enviar_email' => true,
        'nfse_arquivo' => true,
        'nf_prestadore' => true,
        'nf_tomadore' => true,
        'nf_situacao_nota' => true,
        'nf_tipo_recolhimento' => true,
        'cidade' => true,
        'nf_operaco' => true,
        'nf_tributaco' => true,
    ];
}
