<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GrupoContabilidade Entity.
 *
 * @property int $id
 * @property int $grupo_id
 * @property int $contabilidade_id
 */
class GrupoContabilidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'grupo_id' => true,
        'contabilidade_id' => true,
        'grupo' => true,
        'contabilidade' => true,
    ];
}
