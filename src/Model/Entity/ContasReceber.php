<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contasreceber Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $data
 * @property float $valor
 * @property int $idPlanoContas
 * @property \App\Model\Entity\Planoconta $planoconta
 * @property int $idCliente
 * @property \App\Model\Entity\ClienteClone $cliente_clone
 * @property int $status_id
 * @property \App\Model\Entity\Status $status
 * @property string $complemento
 * @property \Cake\I18n\Time $dataPagamento
 * @property int $contabilidade_id
 * @property \App\Model\Entity\Contabilidade $contabilidade
 * @property \Cake\I18n\Time $vencimento
 * @property string $documento
 * @property string $numdoc
 * @property string $parcela
 * @property int $formapagamento
 * @property string $boletomsg
 * @property string $boletomsglivre
 * @property string $boletobarras
 * @property string $boletolinhadigitavel
 * @property string $boletonossonum
 * @property float $juros
 * @property float $multa
 * @property int $boletocedente
 * @property string $numfiscal
 * @property \Cake\I18n\Time $datafiscal
 * @property float $valorBruto
 * @property float $desconto
 * @property int $correcaoMonetaria
 * @property \App\Model\Entity\Correcaomonetarium $correcaomonetarium
 * @property int $tipoPagamento
 * @property \App\Model\Entity\TipoPagamento $tipo_pagamento
 * @property int $tipoDocumento
 * @property \App\Model\Entity\TipoDocumento $tipo_documento
 * @property string $numerodocumento
 * @property int $idProgramacao
 * @property \App\Model\Entity\Programacao $programacao
 * @property int $impresso
 * @property float $valorPagar
 * @property \Cake\I18n\Time $vencimentoBoleto
 * @property int $perjuros
 * @property int $permulta
 * @property \Cake\I18n\Time $data_cadastro
 * @property int $idSisAntigoBoleto
 * @property int $idSisAntigoCx
 * @property int $extern_id
 * @property \App\Model\Entity\Deduco[] $deducoes
 */
class Contasreceber extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
