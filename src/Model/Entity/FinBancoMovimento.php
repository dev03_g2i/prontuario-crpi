<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * FinBancoMovimento Entity.
 *
 * @property int $id
 * @property int $fin_banco_id
 * @property string $mes
 * @property string $ano
 * @property string $status
 * @property float $saldoInicial
 * @property float $saldoFinal
 * @property \Cake\I18n\Time $data
 * @property int $por
 * @property \Cake\I18n\Time $encerradoDt
 * @property int $encerradoPor
 * @property string $data_movimento
 * @property int $fin_movimento_id
 */
class FinBancoMovimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fin_banco_id' => true,
        'mes' => true,
        'ano' => true,
        'status' => true,
        'saldoInicial' => true,
        'saldoFinal' => true,
        'data' => true,
        'por' => true,
        'encerradoDt' => true,
        'encerradoPor' => true,
        'data_movimento' => true,
        'fin_banco' => true,
    ];

    public $virtualFields = ['soma_movimentos_saldos', 'banco_movimento_nome', 'calculo_saldo_final'];

    //Função protegida que retorna a soma dos saldos dos movimentos do mês/ano para cada BancoMovimento 
    protected function _getSomaMovimentosSaldos()
    {   
        $now = new Time();
        $month = $now->format('m');
        $year = $now->format('Y');

        $Movimentos = TableRegistry::get('FinMovimentos');
        $moves = $Movimentos->find()->contain(['FinBancoMovimentos'])
        ->select(['totalCredito' => 'sum(FinMovimentos.credito)', 'totalDebito' => 'sum(FinMovimentos.debito)'])
        ->where(['FinMovimentos.fin_banco_movimento_id' => $this->_properties['id']])
        ->first();

        $BancoMovimentos = TableRegistry::get('FinBancoMovimentos');
        $bankMoves = $BancoMovimentos->find()->where(['FinBancoMovimentos.id' => $this->_properties['id']])->first();
        
        return $bankMoves->saldoInicial + ($moves->totalCredito - $moves->totalDebito);
    }

    //Função protegida que retorna o saldo Final ou o saldo atual do movimento 
    protected function _getCalculoSaldoFinal()
    {
        $Movimentos = TableRegistry::get('FinMovimentos');
        $bankMove = $Movimentos->FinBancoMovimentos->get($this->_properties['id'], [
            'select' => ['saldoInicial']
        ]);

        $saldoInicial = $bankMove->saldoInicial;


        $totalMovimento =  $Movimentos->find()->contain(['FinBancoMovimentos'])
        ->select(['totalSaldo' => 'sum(FinMovimentos.credito) - sum(FinMovimentos.debito)'])
        ->where(['FinMovimentos.fin_banco_movimento_id' => $this->_properties['id']])
        ->first();
        
        return $saldoInicial + $totalMovimento->totalSaldo;
    }

    //Função protegida que retorna o nome concatenado com a data_movimento
    protected function _getBancoMovimentoNome()
    {
        $Contaspagar = TableRegistry::get('FinBancoMovimentos');
        $contaspagar = $Contaspagar->find()->contain(['FinBancos'])
        ->select(['nome' => 'CONCAT(FinBancos.nome, " - ", IF(FinBancoMovimentos.mes < 10, CONCAT("0", FinBancoMovimentos.mes), FinBancoMovimentos.mes), "/", FinBancoMovimentos.ano)'])
        ->where(['FinBancoMovimentos.id' => $this->_properties['id']])
        ->first();
        
        return $contaspagar->nome;
    }

     //Função protegida que retorna o banco movimento para cada Movimento 
     protected function _getDataMovimentoVirtual()
     {
         $month = $this->_properties['mes'] < 10 ? '0'.$this->_properties['mes'] : $this->_properties['mes'];
         $year = $this->_properties['ano'];
         $data_movimento = $month.'/'.$year;
 
         return $data_movimento;
     }
}
