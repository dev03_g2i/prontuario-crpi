<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MedicoResponsavei Entity.
 *
 * @property int $id
 * @property string $conselho
 * @property string $nome
 * @property string $sigla
 * @property string $nome_laudo
 * @property string $conselho_laudo
 * @property string $cpf
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class MedicoResponsavei extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
