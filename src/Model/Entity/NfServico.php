<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NfServico Entity.
 *
 * @property int $id
 * @property int $nota_fiscal_id
 * @property bool $tributavel
 * @property string $descricao
 * @property int $quantidade
 * @property float $valor_unitario
 * @property float $valor_total
 */
class NfServico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nota_fiscal_id' => true,
        'tributavel' => true,
        'descricao' => true,
        'quantidade' => true,
        'valor_unitario' => true,
        'valor_total' => true,
        'nota_fiscal' => true,
    ];
}
