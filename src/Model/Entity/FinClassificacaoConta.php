<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinClassificacaoConta Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property int $status
 * @property int $tipo
 * @property bool $considera
 * @property int $operacao_transferencia
 * @property int $criado_por
 * @property int $alterado_por
 * @property \Cake\I18n\FrozenTime $data_criacao
 * @property \Cake\I18n\FrozenTime $data_alteracao
 */
class FinClassificacaoConta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'status' => true,
        'tipo' => true,
        'considera' => true,
        'operacao_transferencia' => true,
        'criado_por' => true,
        'alterado_por' => true,
        'data_criacao' => true,
        'data_alteracao' => true,
    ];
}
