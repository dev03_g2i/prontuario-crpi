<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Banco Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $agencia
 * @property string $conta
 * @property float $limite
 * @property float $saldo
 * @property int $status
 * @property bool $mostra_fluxo
 * @property bool $mostra_saldo_geral
 */
class Banco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'agencia' => true,
        'conta' => true,
        'limite' => true,
        'saldo' => true,
        'status' => true,
        'mostra_fluxo' => true,
        'mostra_saldo_geral' => true,
        'banco_movimentos' => true,
        'contabilidade_bancos' => true,
        'movimentos' => true,
    ];
}
