<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HistoricoTratamento Entity.
 *
 * @property int $id
 * @property int $historico_id
 * @property \App\Model\Entity\ClienteHistorico $cliente_historico
 * @property int $atendimento_id
 * @property \App\Model\Entity\Atendimento $atendimento
 * @property int $atendimento_procedimento_id
 * @property \App\Model\Entity\AtendimentoProcedimento $atendimento_procedimento
 * @property int $iten_id
 * @property \App\Model\Entity\AtendimentoIten $atendimento_iten
 * @property int $face_id
 * @property \App\Model\Entity\Face $face
 * @property int $situacao_id
 * @property \App\Model\Entity\Situacao $situacao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class HistoricoTratamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
