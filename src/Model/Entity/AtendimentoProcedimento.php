<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * AtendimentoProcedimento Entity.
 *
 * @property int $id
 * @property int $procedimento_id
 * @property \App\Model\Entity\Procedimento $procedimento
 * @property int $atendimento_id
 * @property \App\Model\Entity\Atendimento $atendimento
 * @property float $valor_fatura
 * @property int $quantidade
 * @property float $desconto
 * @property float $porc_desconto
 * @property float $valor_caixa
 * @property string $digitado
 * @property int $material
 * @property int $num_controle
 * @property int $medico_id
 * @property \App\Model\Entity\MedicoResponsavei $medico_responsavei
 * @property float $valor_matmed
 * @property string $documento_guia
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class AtendimentoProcedimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    protected $_virtual = ['valor_total', 'total_equipe'];

    protected function _getValorTotal()
    {
        $vl_caixa_original = !empty($this->_properties['vl_caixa_original']) ? $this->_properties['vl_caixa_original'] : 0.00;
        $quantidade = !empty($this->_properties['quantidade']) ? $this->_properties['quantidade'] : 1;
        return $vl_caixa_original * $quantidade;
    }

    protected function _getTotalEquipe()
    {
        $ap_equipes = TableRegistry::get('AtendimentoProcedimentoEquipes');
        if(!empty($this->_properties['id'])){
            $query = $ap_equipes->find('all')
            ->where(['AtendimentoProcedimentoEquipes.atendimento_procedimento_id' => $this->_properties['id']])
            ->andWhere(['AtendimentoProcedimentoEquipes.situacao_id' => 1]);
            $total = 0;
            foreach($query as $q){
                $porcentagem = $q->porcentagem / 100;
                $total += ($this->_properties['total'] + $this->_properties['valor_caixa']) * $porcentagem;
            }
            return $total;   
        }else {
            return 0;
        }
    }

}
