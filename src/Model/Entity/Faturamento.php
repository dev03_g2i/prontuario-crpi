<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Faturamento Entity.
 *
 * @property int $id
 * @property int $atendimento_procedimento_id
 * @property \App\Model\Entity\AtendimentoProcedimento $atendimento_procedimento
 * @property string $numero_autorizacao
 * @property \Cake\I18n\Time $data_autorizacao
 * @property string $numero_guia
 * @property \Cake\I18n\Time $data_guia
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property int $user_up_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 */
class Faturamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
