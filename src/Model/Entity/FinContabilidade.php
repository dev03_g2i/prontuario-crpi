<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinContabilidade Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $descricao
 * @property string $cep
 * @property string $rua
 * @property string $bairro
 * @property int $numero
 * @property string $cidade
 * @property string $uf
 * @property int $situacao
 */
class FinContabilidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'descricao' => true,
        'cep' => true,
        'rua' => true,
        'bairro' => true,
        'numero' => true,
        'cidade' => true,
        'uf' => true,
        'situacao' => true,
        'fin_contabilidade_bancos' => true,
        'fin_contas_pagar' => true,
        'fin_contas_pagar_planejamentos' => true,
        'fin_contas_receber_planejamentos' => true,
        'fin_grupo_contabilidades' => true,
        'fin_movimentos' => true,
        'fin_planejamentos' => true,
    ];
}
