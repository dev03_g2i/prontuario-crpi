<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FinTipoPagamento Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property int $fin_status_id
 * @property string $usado
 * @property float $taxa
 * @property int $fin_tipo_documento_id
 * @property int $id_plano_contas_receita
 * @property int $id_plano_contas_saida
 * @property float $taxa01
 * @property float $taxa02
 * @property float $taxa03
 * @property float $taxa04
 * @property float $taxa05
 * @property float $taxa06
 * @property float $num_parcelas
 */
class FinTipoPagamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'fin_status_id' => true,
        'usado' => true,
        'taxa' => true,
        'fin_tipo_documento_id' => true,
        'id_plano_contas_receita' => true,
        'id_plano_contas_saida' => true,
        'taxa01' => true,
        'taxa02' => true,
        'taxa03' => true,
        'taxa04' => true,
        'taxa05' => true,
        'taxa06' => true,
        'num_parcelas' => true,
        'status' => true,
        'tipo_documento' => true,
        'fin_contas_pagar' => true,
    ];
}
