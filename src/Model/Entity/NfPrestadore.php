<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NfPrestadore Entity.
 *
 * @property int $id
 * @property string $razao_social
 * @property string $inscricao_municipal
 * @property string $cpfcnpj
 * @property string $ddd
 * @property string $telefone
 * @property string $email
 * @property string $cpfcnpj_intermediario
 * @property int $tipo_logradouro_id
 * @property string $endereco
 * @property int $numero
 * @property string $complemento
 * @property int $tipo_bairro_id
 * @property string $bairro
 * @property string $cep
 * @property int $cidade_id
 * @property bool $atual
 * @property string $cnae
 * @property string $descricao_cnae
 * @property int $tributacao_id
 * @property float $aliquota_atividade
 */
class NfPrestadore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'razao_social' => true,
        'inscricao_municipal' => true,
        'cpfcnpj' => true,
        'ddd' => true,
        'telefone' => true,
        'email' => true,
        'cpfcnpj_intermediario' => true,
        'tipo_logradouro_id' => true,
        'endereco' => true,
        'numero' => true,
        'complemento' => true,
        'tipo_bairro_id' => true,
        'bairro' => true,
        'cep' => true,
        'cidade_id' => true,
        'atual' => true,
        'cnae' => true,
        'descricao_cnae' => true,
        'tributacao_id' => true,
        'aliquota_atividade' => true,
        'tipo_logradouro' => true,
        'tipo_bairro' => true,
        'cidade' => true,
        'tributacao' => true,
    ];
}
