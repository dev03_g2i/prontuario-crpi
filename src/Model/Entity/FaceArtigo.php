<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FaceArtigo Entity.
 *
 * @property int $id
 * @property int $face_itens_id
 * @property \App\Model\Entity\FaceIten $face_iten
 * @property int $procedimento_gasto_id
 * @property \App\Model\Entity\ProcedimentoGasto $procedimento_gasto
 * @property int $artigo_id
 * @property \App\Model\Entity\Artigo $artigo
 * @property float $quantidade
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 */
class FaceArtigo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
