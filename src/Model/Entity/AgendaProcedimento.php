<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AgendaProcedimento Entity.
 *
 * @property int $id
 * @property int $agenda_id
 * @property \App\Model\Entity\Agenda $agenda
 * @property int $agenda_cadprocedimento_id
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\AgendaCadprocedimento[] $agenda_cadprocedimentos
 */
class AgendaProcedimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
