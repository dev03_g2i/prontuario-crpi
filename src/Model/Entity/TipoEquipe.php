<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TipoEquipe Entity.
 *
 * @property int $id
 * @property string $nome
 * @property string $codigo_tiss
 * @property int $user_insert
 * @property int $user_update
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class TipoEquipe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'codigo_tiss' => true,
        'user_insert' => true,
        'user_update' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'situacao_cadastro' => true,
        'atendimento_procedimento_equipes' => true,
    ];
}
