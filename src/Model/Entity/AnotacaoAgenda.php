<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * AnotacaoAgenda Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property \Cake\I18n\FrozenTime $data
 * @property int $grupo_id
 * @property int $situacao_id
 * @property int $user_id
 * @property int $user_update
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class AnotacaoAgenda extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'data' => true,
        'grupo_id' => true,
        'situacao_id' => true,
        'user_id' => true,
        'user_update' => true,
        'created' => true,
        'modified' => true,
        'grupo_agenda' => true,
        'situacao_cadastro' => true,
        'user' => true,
    ];
}
