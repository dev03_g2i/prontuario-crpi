<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfiguracaoRelCaixaUsuario Entity.
 *
 * @property int $id
 * @property int $user_id
 * @property int $configuracao_rel_caixa_id
 */
class ConfiguracaoRelCaixaUsuario extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'configuracao_rel_caixa_id' => true,
        'user' => true,
        'configuracao_rel_caixa' => true,
    ];
}
