<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Series Entity.
 *
 * @property int $pk
 * @property int $study_fk
 * @property int $mpps_fk
 * @property int $inst_code_fk
 * @property string $series_iuid
 * @property string $series_no
 * @property string $modality
 * @property string $body_part
 * @property string $laterality
 * @property string $series_desc
 * @property string $institution
 * @property string $station_name
 * @property string $department
 * @property string $perf_physician
 * @property string $perf_phys_fn_sx
 * @property string $perf_phys_gn_sx
 * @property string $perf_phys_i_name
 * @property string $perf_phys_p_name
 * @property \Cake\I18n\Time $pps_start
 * @property string $pps_iuid
 * @property string $series_custom1
 * @property string $series_custom2
 * @property string $series_custom3
 * @property int $num_instances
 * @property string $src_aet
 * @property string $ext_retr_aet
 * @property string $retrieve_aets
 * @property string $fileset_iuid
 * @property string $fileset_id
 * @property \App\Model\Entity\Fileset $fileset
 * @property int $availability
 * @property int $series_status
 * @property \Cake\I18n\Time $created_time
 * @property \Cake\I18n\Time $updated_time
 * @property string|resource $series_attrs
 */
class Series extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'pk' => false,
    ];
}
