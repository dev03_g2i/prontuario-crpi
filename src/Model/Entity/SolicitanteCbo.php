<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SolicitanteCbo Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property string $codigo_tiss
 * @property int $situacao_id
 * @property int $user_insert
 * @property \Cake\I18n\FrozenTime $created
 * @property int $user_update
 * @property \Cake\I18n\FrozenTime $modified
 */
class SolicitanteCbo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'codigo_tiss' => true,
        'situacao_id' => true,
        'user_insert' => true,
        'created' => true,
        'user_update' => true,
        'modified' => true,
        'situacao_cadastro' => true,
    ];
}
