<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConvenioImpostosParametro Entity.
 *
 * @property int $id
 * @property int $id_convenio
 * @property int $convenio_id
 * @property int $id_tipo_imposto
 * @property float $porcentual
 * @property int $retido
 * @property string $observacao
 * @property int $user_created
 * @property int $user_updated
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class ConvenioImpostosParametro extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id_convenio' => true,
        'convenio_id' => true,
        'id_tipo_imposto' => true,
        'porcentual' => true,
        'retido' => true,
        'observacao' => true,
        'user_created' => true,
        'user_updated' => true,
        'created' => true,
        'modified' => true,
    ];
}
