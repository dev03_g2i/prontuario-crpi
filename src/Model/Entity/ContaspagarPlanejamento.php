<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContaspagarPlanejamento Entity.
 *
 * @property int $id
 * @property int $periodicidade
 * @property int $plazo_determinado
 * @property float $valor_parcela
 * @property int $cliente_id
 * @property int $fornecedor_id
 * @property int $contabilidade_id
 * @property string $complemento
 * @property int $dia_vencimento
 * @property int $situacao
 * @property int $a_vista
 * @property int $planoconta_id
 * @property int $correcao_monetaria
 * @property \Cake\I18n\FrozenDate $proxima_geracao
 * @property \Cake\I18n\FrozenDate $ultima_geracao
 * @property int $geradas
 * @property int $tipo_pgmt
 * @property int $tipo_dcmt
 * @property \Cake\I18n\FrozenDate $ultimo_gerado
 * @property int $mes_vencimento
 * @property \Cake\I18n\Time $ultima_atualizacao
 * @property \Cake\I18n\FrozenDate $data_documento
 * @property \Cake\I18n\FrozenDate $primeiro_vencimento
 * @property string $mumero_documento
 */
class ContaspagarPlanejamento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'periodicidade' => true,
        'plazo_determinado' => true,
        'valor_parcela' => true,
        'cliente_id' => true,
        'fornecedor_id' => true,
        'contabilidade_id' => true,
        'complemento' => true,
        'dia_vencimento' => true,
        'situacao' => true,
        'a_vista' => true,
        'planoconta_id' => true,
        'correcao_monetaria' => true,
        'proxima_geracao' => true,
        'ultima_geracao' => true,
        'geradas' => true,
        'tipo_pgmt' => true,
        'tipo_dcmt' => true,
        'ultimo_gerado' => true,
        'mes_vencimento' => true,
        'ultima_atualizacao' => true,
        'data_documento' => true,
        'primeiro_vencimento' => true,
        'mumero_documento' => true,
        'cliente' => true,
        'fornecedor' => true,
        'contabilidade' => true,
        'planoconta' => true,
        'contaspagar' => true,
    ];
}
