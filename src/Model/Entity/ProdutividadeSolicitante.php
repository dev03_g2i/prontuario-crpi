<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProdutividadeSolicitante Entity.
 *
 * @property int $id
 * @property int $convenio_id
 * @property int $solicitante_id
 * @property float $percentual_desconto
 * @property float $percentual_repasse
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_id
 */
class ProdutividadeSolicitante extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'convenio_id' => true,
        'solicitante_id' => true,
        'percentual_desconto' => true,
        'percentual_repasse' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'convenio' => true,
        'solicitante' => true,
        'user' => true,
        'situacao_id' => true,
        'procedimento_id' => true
    ];
}
