<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FaturaMatmed Entity.
 *
 * @property int $id
 * @property \Cake\I18n\Time $data
 * @property int $artigo_id
 * @property \App\Model\Entity\Artigo $artigo
 * @property int $quantidade
 * @property float $vl_custos
 * @property float $vl_venda
 * @property float $vl_custo_medico
 * @property int $origin
 * @property string $codigo_tiss
 * @property string $codigo_tuss
 * @property string $codigo_convenio
 * @property int $atendimento_procedimento_id
 * @property \App\Model\Entity\AtendimentoProcedimento $atendimento_procedimento
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class FaturaMatmed extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
