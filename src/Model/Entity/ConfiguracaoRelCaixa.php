<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ConfiguracaoRelCaixa Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property string $nome_report
 * @property int $situacao_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $user_created
 * @property int $user_modified
 */
class ConfiguracaoRelCaixa extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'nome_report' => true,
        'situacao_id' => true,
        'created' => true,
        'modified' => true,
        'user_created' => true,
        'user_modified' => true,
        'situacao_cadastro' => true,
        'configuracao_rel_caixa_usuario' => true,
    ];
}
