<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClienteLigaco Entity.
 *
 * @property int $id
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 * @property int $parent_id
 * @property \App\Model\Entity\ParentClienteLigaco $parent_cliente_ligaco
 * @property int $grau_id
 * @property \App\Model\Entity\GrauParentesco $grau_parentesco
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property \Cake\I18n\Time $created
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\ChildClienteLigaco[] $child_cliente_ligacoes
 */
class ClienteLigaco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
