<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeducoesContasreceber Entity.
 *
 * @property int $id
 * @property int $deducoes_id
 * @property \App\Model\Entity\Deduco $deduco
 * @property int $contasreceber_id
 * @property \App\Model\Entity\Contasreceber $contasreceber
 * @property float $percentual
 */
class DeducoesContasreceber extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
