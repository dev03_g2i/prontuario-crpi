<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClienteHistorico Entity.
 *
 * @property int $id
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 * @property int $tipohistoria_id
 * @property string $descricao
 * @property int $medico_id
 * @property \App\Model\Entity\MedicoResponsavei $medico_responsavei
 * @property string $anotacao
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \App\Model\Entity\TipoHistoria[] $tipo_historias
 * @property \App\Model\Entity\HistoricoTratamento[] $historico_tratamentos
 * @property \App\Model\Entity\HistoricoFaceArtigo[] $historico_face_artigos
 * @property \App\Model\Entity\HistoricoItenArtigo[] $historico_iten_artigos
 */
class ClienteHistorico extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
