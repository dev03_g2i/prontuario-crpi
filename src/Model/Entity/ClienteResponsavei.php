<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClienteResponsavei Entity.
 *
 * @property int $id
 * @property int $cliente_id
 * @property \App\Model\Entity\Cliente $cliente
 * @property int $grau_id
 * @property \App\Model\Entity\GrauParentesco $grau_parentesco
 * @property string $nome
 * @property string $nascionalidade
 * @property int $estadocivil_id
 * @property \App\Model\Entity\EstadoCivi $estado_civi
 * @property string $profissao
 * @property string $rg
 * @property string $cpf
 * @property string $cep
 * @property string $endereco
 * @property int $numero
 * @property string $complemento
 * @property string $cidade
 * @property string $estado
 * @property string $telefone
 * @property string $celular
 * @property string $email
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class ClienteResponsavei extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
