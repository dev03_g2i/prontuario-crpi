<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * EstqArtigo Entity.
 *
 * @property int $id
 * @property int $subgrupo
 * @property int $controla_lote
 * @property string $nome
 * @property float $vlcomercial
 * @property float $vlcusto
 * @property float $vlcustomedio
 * @property int $unidade
 * @property string $tamanho
 * @property \Cake\I18n\Time $usuario_dt
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property int $situacao_id
 * @property \App\Model\Entity\Situacao $situacao
 * @property int $origem
 * @property string $codigo_livre
 * @property bool $ensaio
 * @property string $observacao
 * @property float $valor_repasse
 * @property bool $utiliza_repasse
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class EstqArtigo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
