<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InternacaoCaraterAtendimento Entity.
 *
 * @property int $id
 * @property string $descricao
 * @property string $codigotiss
 * @property int $situacao_id
 * @property int $user_reg
 * @property int $user_update
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class InternacaoCaraterAtendimento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'descricao' => true,
        'codigotiss' => true,
        'situacao_id' => true,
        'user_reg' => true,
        'user_update' => true,
        'created' => true,
        'modified' => true,
        'situacao_cadastro' => true,
    ];
}
