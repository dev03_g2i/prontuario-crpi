<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Programacao Entity.
 *
 * @property int $id
 * @property int $periodicidade
 * @property int $prazoDeterminado
 * @property float $valorParcela
 * @property int $idCliente
 * @property int $idFornecedor
 * @property int $contabilidade
 * @property string $complemento
 * @property int $diaVencimento
 * @property int $status
 * @property int $aVista
 * @property string $tipo
 * @property int $idPlanoContas
 * @property int $correcaoMonetaria
 * @property \Cake\I18n\Time $proximaGeracao
 * @property \Cake\I18n\Time $ultimaGeracao
 * @property int $geradas
 * @property int $tipoPagamento
 * @property int $tipoDocumento
 * @property \Cake\I18n\Time $ultimoGerado
 * @property int $mesVencimento
 * @property \Cake\I18n\Time $ultima_atualizacao
 * @property \App\Model\Entity\Contasreceber[] $contasreceber
 * @property \App\Model\Entity\Deduco[] $deducoes
 */
class Programacao extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
