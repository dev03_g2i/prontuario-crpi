<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ClienteContrato Entity.
 *
 * @property int $id
 * @property int $atendimento_id
 * @property \App\Model\Entity\Atendimento $atendimento
 * @property int $contrato_id
 * @property \App\Model\Entity\Contrato $contrato
 * @property int $situacao_id
 * @property \App\Model\Entity\SituacaoCadastro $situacao_cadastro
 * @property int $user_id
 * @property \App\Model\Entity\User $user
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property int $responsavel_id
 * @property \App\Model\Entity\ClienteResponsavei $cliente_responsavei
 * @property float $valor_total
 * @property int $meses
 * @property float $valor_parcela
 * @property int $vencimento
 * @property \Cake\I18n\Time $primeiro_pagamento
 */
class ClienteContrato extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
