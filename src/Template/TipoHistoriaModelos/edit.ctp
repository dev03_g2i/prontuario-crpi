<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Modelos - Editar Anotação</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tipoHistoriaModelos form">
                            <?= $this->Form->create($tipoHistoriaModelo) ?>
                            <fieldset>
                                <?php
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('nome');
                                echo "</div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('tipo_historia_id', ['options' => $tipoHistorias, 'empty' => 'Selecione']);
                                echo "</div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('proprietario', ['label' => 'Proprietário', 'options' => $medico_responsaveis, 'empty' => 'Selecione', 'default' => $tipoHistoriaModelo->proprietario]);
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('modelo', ['data'=>'sumer', 'rows' => 2]);
                                echo "</div>";
                                ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

