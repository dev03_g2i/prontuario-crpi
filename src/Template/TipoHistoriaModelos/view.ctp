
<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Modelos - Visualizar Anotação</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3><?= __('Modelo') ?></h3>
                    </div>
                    <div class="ibox-content">
                        <?= $tipoHistoriaModelo->modelo ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


