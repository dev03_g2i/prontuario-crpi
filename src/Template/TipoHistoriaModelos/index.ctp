<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Modelos - Anotações Clínicas</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('TipoHistoriaModelo',['type'=>'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('tipo_historia_id', ['label' => 'Tipo de procedimento', 'name' => 'TipoHistoriaModelos__tipo_historia_id', 'options' => $tipoHistorias, 'empty' => 'Selecione']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Novo Modelo', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Tipo Historia Modelos','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">

                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <!--<th><?= $this->Paginator->sort('modelo') ?></th>-->
                                        <th><?= $this->Paginator->sort('tipo_historia_id', 'Tipo de procedimento') ?></th>
                                        <th><?= $this->Paginator->sort('proprietario') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tipoHistoriaModelos as $tipoHistoriaModelo): ?>
                                        <tr>
                                            <td><?= $this->Number->format($tipoHistoriaModelo->id) ?></td>
                                            <td><?= h($tipoHistoriaModelo->nome) ?></td>
                                            <!--<td><?= $tipoHistoriaModelo->modelo ?></td>-->
                                            <td><?= $tipoHistoriaModelo->has('tipo_historia') ? $this->Html->link($tipoHistoriaModelo->tipo_historia->nome, ['controller' => 'TipoHistorias', 'action' => 'view', $tipoHistoriaModelo->tipo_historia->id]) : '' ?></td>
                                            <td>
                                                <?php
                                                    if(!empty($tipoHistoriaModelo->proprietario)):
                                                        echo $this->Medico->getMedicoName($tipoHistoriaModelo->proprietario);
                                                    else:
                                                        echo 'Público';
                                                    endif;
                                                    ?>
                                            </td>
                                            <td><?= $tipoHistoriaModelo->has('situacao_cadastro') ? $this->Html->link($tipoHistoriaModelo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoHistoriaModelo->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= $tipoHistoriaModelo->has('user') ? $this->Html->link($tipoHistoriaModelo->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoHistoriaModelo->user->id]) : '' ?></td>
                                            <td><?= h($tipoHistoriaModelo->created) ?></td>
                                            <td><?= h($tipoHistoriaModelo->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'tipoHistoriaModelos','action' => 'view', $tipoHistoriaModelo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'tipoHistoriaModelos','action' => 'edit', $tipoHistoriaModelo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary', 'target' => '_blank']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'tipoHistoriaModelos','action'=>'delete', $tipoHistoriaModelo->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>