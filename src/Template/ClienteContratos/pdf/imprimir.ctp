<!-- src/Template/Yop/pdf/view.ctp -->
<?php

    if($cliente_contrato->has('cliente_responsavei')) {
        $endereco['rua'] = $cliente_contrato->cliente_responsavei->rua;
        $endereco['numero'] = $cliente_contrato->cliente_responsavei->numero;
        $endereco['cidade'] = $cliente_contrato->cliente_responsavei->cidade;
        $endereco['estado'] = $cliente_contrato->cliente_responsavei->estado;
        $endereco['bairro'] = $cliente_contrato->cliente_responsavei->bairro;
        $endereco['complemento'] = $cliente_contrato->cliente_responsavei->complemento;
    }else{
        $endereco['rua'] = $cliente_contrato->atendimento->cliente->rua;
        $endereco['numero'] = $cliente_contrato->atendimento->cliente->numero;
        $endereco['cidade'] = $cliente_contrato->atendimento->cliente->cidade;
        $endereco['estado'] = $cliente_contrato->atendimento->cliente->estado;
        $endereco['bairro'] = $cliente_contrato->atendimento->cliente->bairro;
        $endereco['complemento'] = $cliente_contrato->atendimento->cliente->complemento;
    }

echo $this->Dompdf->css('pdf/estilo');
    $dados = [
        '{nome_responsavel}'=> $cliente_contrato->has('cliente_responsavei') ? $cliente_contrato->cliente_responsavei->nome : $cliente_contrato->atendimento->cliente->nome,
        '{nome_dependente}'=>$cliente_contrato->has('cliente_responsavei') ? $cliente_contrato->atendimento->cliente->nome : "",
        '{idade}'=>$this->Data->idade($cliente_contrato->atendimento->cliente->nascimento),
        '{cpf}' => $cliente_contrato->has('cliente_responsavei') ? $cliente_contrato->cliente_responsavei->cpf : $cliente_contrato->atendimento->cliente->cpf,
        '{rua}' => $endereco['rua'],
        '{numero}' => $endereco['numero'],
        '{cidade}' => $endereco['cidade'],
        '{estado}' => $endereco['estado'],
        '{bairro}' => $endereco['bairro'],
        '{complemento}' => $endereco['complemento'],
        '{valor}' => $this->Number->currency($cliente_contrato->valor_total),
        '{parcela}' => $this->Number->currency($cliente_contrato->valor_parcela),
        '{numero_parcela}' => $cliente_contrato->meses,
        '{vencimento}' => $cliente_contrato->vencimento,
        '{primeiro_pagamento}' => $cliente_contrato->primeiro_pagamento,
        '{dia}' => $this->Time->format($cliente_contrato->data_contrato,'dd'),
        '{mes}' => $this->Time->format($cliente_contrato->data_contrato,'MMMM'),
        '{ano}' =>$this->Time->format($cliente_contrato->data_contrato,'YYY')
    ];
?>
<div class="corpo">
    <?= $this->Replace->replace($dados,$cliente_contrato->contrato->descricao) ?>
</div>
