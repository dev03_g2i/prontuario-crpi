<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cliente Contratos</h2>
            <ol class="breadcrumb">
                <li>Cliente Contratos</li>
                <li class="active">
                    <strong> Editar Cliente Contratos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clienteContratos form">
                            <?= $this->Form->create($clienteContrato) ?>

                            <fieldset>
                                <legend><?= __('Editar Cliente Contrato') ?></legend>
                                <?php
                                echo $this->Form->input('atendimento_id', ['type' => 'hidden','value'=>$clienteContrato->atendimento_id]);
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('contrato_id', ['option' => $contratos]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('responsavel_id', ['data' => 'select', 'controller' => 'clienteResponsaveis', 'action' => 'fill', 'data-value' => $clienteContrato->responsavel_id, 'empty' => 'selecione','class'=>'disabled','readonly']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_total', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money','value'=>number_format($clienteContrato->valor_total,2,'.','')]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('meses');
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_parcela', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money','value'=>number_format($clienteContrato->valor_parcela,2,'.','')]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento');
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('primeiro_pagamento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($clienteContrato->primeiro_pagamento, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data_contrato', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($clienteContrato->data_contrato, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <button class="btn btn-info" id="btn-voltar" type="button">Voltar</button>
                                <input type="submit" class="btn btn-primary" value="Salvar" />
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

