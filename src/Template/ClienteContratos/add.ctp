<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cliente Contratos</h2>
            <ol class="breadcrumb">
                <li>Cliente Contratos</li>
                <li class="active">
                    <strong> Cadastrar Cliente Contratos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clienteContratos form">
                            <?= $this->Form->create($clienteContrato) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Cliente Contrato') ?></legend>
                                <?php
                                if (!empty($atendimento->id)) {
                                    echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $atendimento->id]);
                                } else {
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('atendimento_id', ['data' => 'select', 'controller' => 'atendimentos', 'action' => 'fill']);
                                    echo "</div>";
                                }
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('contrato_id', ['options' => $contratos,'empty'=>'Selecione']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('responsavel_id', ['label'=>'Responsável Financeiro','options' => $responsaveis, 'empty' => 'selecione']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_total', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money','value'=>number_format($atendimento->total_liquido,2,'.','')]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('meses',['label'=>'Número de parcelas' ,'value'=>count($atendimento->contas_receber)]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_parcela', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money','value'=>number_format($atendimento->contas_receber[0]->valor,2,'.','')]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento',['label'=>'Dia de Vencimento','value'=>$this->Time->format($atendimento->contas_receber[0]->vencimento,'dd')]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('primeiro_pagamento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker','value'=>$this->Time->format($atendimento->contas_receber[0]->vencimento,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data_contrato', ['empty' => true, 'type' => 'text', 'class' => 'datepicker','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <button class="btn btn-info" id="btn-voltar" type="button">Voltar</button>
                                <input type="submit" class="btn btn-primary" value="Salvar" />
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

