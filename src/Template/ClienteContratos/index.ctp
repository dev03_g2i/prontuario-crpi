<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contratos</h2>
            <ol class="breadcrumb">
                <li>Contratos</li>
                <li class="active">
                    <strong>Listagem de Contratos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Novo Contrato', ['action' => 'add','?'=>['atendimento_id'=>$atendimento_id]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Cliente Contratos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Contratos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('contrato_id',['label'=>'Contrato']) ?></th>
                                        <th><?= $this->Paginator->sort('responsavel_id') ?></th>
                                        <th><?= $this->Paginator->sort('valor_total') ?></th>
                                        <th><?= $this->Paginator->sort('meses') ?></th>
                                        <th><?= $this->Paginator->sort('valor_parcela') ?></th>
                                        <th><?= $this->Paginator->sort('vencimento') ?></th>
                                        <th><?= $this->Paginator->sort('primeiro_pagamento') ?></th>
                                        <th><?= $this->Paginator->sort('data_contrato') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($clienteContratos as $clienteContrato): ?>
                                        <tr>
                                            <td><?= $clienteContrato->has('contrato') ? $this->Html->link($clienteContrato->contrato->nome, ['controller' => 'Contratos', 'action' => 'view', $clienteContrato->contrato->id]) : '' ?></td>
                                            <td><?= $clienteContrato->has('cliente_responsavei') ? $this->Html->link($clienteContrato->cliente_responsavei->nome, ['controller' => 'ClienteResponsaveis', 'action' => 'view', $clienteContrato->cliente_responsavei->id]) : '' ?></td>
                                            <td><?= $this->Number->currency($clienteContrato->valor_total) ?></td>
                                            <td><?= $this->Number->format($clienteContrato->meses) ?></td>
                                            <td><?= $this->Number->currency($clienteContrato->valor_parcela) ?></td>
                                            <td><?= $this->Number->format($clienteContrato->vencimento) ?></td>
                                            <td><?= h($clienteContrato->primeiro_pagamento) ?></td>
                                            <td><?= h($clienteContrato->data_contrato) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('print'), ['action' => 'imprimir.pdf', '?'=>['id'=>$clienteContrato->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir', 'escape' => false, 'class' => 'btn btn-xs btn-info','target'=>'_blanck','listen'=>'f']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteContrato->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'clienteContratos\',' . $clienteContrato->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

