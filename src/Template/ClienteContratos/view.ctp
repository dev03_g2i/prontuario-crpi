
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Contratos</h2>
        <ol class="breadcrumb">
            <li>Cliente Contratos</li>
            <li class="active">
                <strong>Litagem de Cliente Contratos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Cliente Contratos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Atendimento') ?></th>
                                                                <td><?= $clienteContrato->has('atendimento') ? $this->Html->link($clienteContrato->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $clienteContrato->atendimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Contrato') ?></th>
                                                                <td><?= $clienteContrato->has('contrato') ? $this->Html->link($clienteContrato->contrato->nome, ['controller' => 'Contratos', 'action' => 'view', $clienteContrato->contrato->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $clienteContrato->has('situacao_cadastro') ? $this->Html->link($clienteContrato->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $clienteContrato->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $clienteContrato->has('user') ? $this->Html->link($clienteContrato->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteContrato->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Cliente Responsavei') ?></th>
                                                                <td><?= $clienteContrato->has('cliente_responsavei') ? $this->Html->link($clienteContrato->cliente_responsavei->nome, ['controller' => 'ClienteResponsaveis', 'action' => 'view', $clienteContrato->cliente_responsavei->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($clienteContrato->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Total') ?></th>
                                <td><?= $this->Number->format($clienteContrato->valor_total) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Meses') ?></th>
                                <td><?= $this->Number->format($clienteContrato->meses) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Parcela') ?></th>
                                <td><?= $this->Number->format($clienteContrato->valor_parcela) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vencimento') ?></th>
                                <td><?= $this->Number->format($clienteContrato->vencimento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($clienteContrato->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($clienteContrato->modified) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Primeiro Pagamento') ?></th>
                                                                <td><?= h($clienteContrato->primeiro_pagamento) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


