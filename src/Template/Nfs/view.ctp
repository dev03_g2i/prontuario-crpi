<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nfs</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Nfs</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Cód.') ?>
                                    </th>
                                    <td>
                                        <?= $nf->id ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('N° Ficha') ?>
                                    </th>
                                    <td>
                                        <?= $nf->ficha ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('N° NFse') ?>
                                    </th>
                                    <td>
                                        <?= $nf->numero_nfse ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Tomador Serviços') ?>
                                    </th>
                                    <td>
                                        <?= $nf->nf_tomadore->cliente->nome; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situação nota') ?>
                                    </th>
                                    <td>
                                        <?= $nf->nf_situacao_nota->descricao; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Data Processamento') ?>
                                    </th>
                                    <td>
                                        <?= $nf->data_processamento; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Valor Total Serviços') ?>
                                    </th>
                                    <td>
                                        <?= $nf->valor_total_servicos; ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>