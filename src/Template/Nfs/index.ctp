<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nfs</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Nf',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('situacao_nota_id', ['label' => 'Situação nota', 'options' => $situacao_nfs, 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('ficha', ['label' => 'N° Ficha', 'type' => 'text', 'name' => 'Nfs__ficha']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?php echo $this->Form->input('cliente_id', ['label' => 'Tomador serviços', 'type' => 'select', 'empty' => 'selecione', 'data-up' => 'cliente', 'data' => 'select', 'controller' => 'Clientes', 'action' => 'fill']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('id', 'Cód.') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('ficha', 'N° Ficha') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('numero_nfse', 'N° NFse') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('tomador_id', 'Tomador Serviços') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('situacao_nota_id', 'Situação nota') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('data_processamento', 'Data Processamento') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('valor_total_servicos', 'Valor Total Serviços') ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($nfs as $nf): ?>
                                        <tr>
                                            <td>
                                                <?= $nf->id; ?>
                                            </td>
                                            <td>
                                                <?= $nf->ficha; ?>
                                            </td>
                                            <td>
                                                <?= $nf->numero_nfse; ?>
                                            </td>
                                            <td>
                                                <?= $nf->nf_tomadore->cliente->nome; ?>
                                            </td>
                                            <td>
                                                <?= $nf->nf_situacao_nota->descricao; ?>
                                            </td>
                                            <td>
                                                <?= $nf->data_processamento; ?>
                                            </td>
                                            <td>
                                                <?= number_format($nf->valor_total_servicos, 2, '.', '') ?>
                                            </td>
                                            <td class="actions">
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button"
                                                        data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link($this->Html->icon('list-alt').' Detalhes', ['controller'=>'nfs','action' => 'view', $nf->id],['escape' => false]); ?></li>
                                                    <?php
                                                        if($this->Nf->verificaPdf($nf->numero_nfse) && $nf->situacao_nota_id != 2): ?>
                                                            <li><?= $this->Html->link('<i class="fa fa-print"></i> Abrir nota', $this->Nf->urlPdf($nf->numero_nfse), ['escape' => false, 'target' => '_blank']); ?></li>
                                                        <?php else: ?>
                                                            <li><?= $this->Html->link('<i class="fa fa-share"></i> Nota com erro', 'javascript:void(0)', ['escape' => false]); ?></li>
                                                    <?php endif; ?>
                                                </ul>
                                            </div>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>