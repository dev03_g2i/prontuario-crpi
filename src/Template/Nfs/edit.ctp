<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nfs</h2>
            <ol class="breadcrumb">
                <li>Nfs</li>
                <li class="active">
                    <strong>
                                                Editar Nfs
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="nfs form">
                            <?= $this->Form->create($nf) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Nf') ?></legend>
                                                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('prestador_id', ['data'=>'select','controller'=>'nfPrestadores','action'=>'fill','data-value'=>$nf->prestador_id, 'empty' => 'selecione']); ?>
                                                                                            </div>
                                                                                        <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('tomador_id', ['data'=>'select','controller'=>'nfTomadores','action'=>'fill','data-value'=>$nf->tomador_id, 'empty' => 'selecione']); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('ficha'); ?>
                                                                    </div>
                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('situacao_nota_id', ['data'=>'select','controller'=>'nfSituacaoNotas','action'=>'fill','data-value'=>$nf->situacao_nota_id, 'empty' => 'selecione']); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('situacao_rps'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao_rps'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('assinatura'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('autorizacao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('autorizacao_cancelamento'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('numero_lote'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('numero_rps'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('numero_nfse'); ?>
                                                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('data_emissao_rps', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($nf->data_emissao_rps,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                                     ?>
                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('data_processamento', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($nf->data_processamento,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                                     ?>
                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('data_cancelamento', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($nf->data_cancelamento,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                                     ?>
                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('mot_cancelamento'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('serie_rps'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('serie_rps_substituido'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('numero_rps_substituido'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('numero_nfse_substituido'); ?>
                                                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('data_emissao_nfse_substituida', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($nf->data_emissao_nfse_substituida,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                                     ?>
                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('serie_prestacao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('cnae'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao_cnae'); ?>
                                                                    </div>
                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('tipo_recolhimento_id', ['data'=>'select','controller'=>'nfTipoRecolhimentos','action'=>'fill','data-value'=>$nf->tipo_recolhimento_id, 'empty' => 'selecione']); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('cidade_id'); ?>
                                                                    </div>
                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('operacao_id', ['data'=>'select','controller'=>'nfOperacoes','action'=>'fill','data-value'=>$nf->operacao_id, 'empty' => 'selecione']); ?>
                                                                                            </div>
                                                                                        <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('tributacao_id', ['data'=>'select','controller'=>'nfTributacoes','action'=>'fill','data-value'=>$nf->tributacao_id, 'empty' => 'selecione']); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('aliquota_atividade',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('aliquota_pis',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('aliquota_cofins',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('aliquota_inss',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('aliquota_ir',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('aliquota_csll',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_pis',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_cofins',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_inss',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_ir',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_csll',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_total_servicos',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_total_deducoes',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('enviar_email'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('nfse_arquivo'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

