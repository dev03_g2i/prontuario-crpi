<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Receber Todos</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="produtividadeProfissionais form">
                            <?= $this->Form->create('receberTodos') ?>
                            <div class="col-md-4">
                                <?=$this->Form->input('dt_recebimento', ['required' => true, 'label' => 'Data', 'type' => 'text', 'class' => 'datepicker'])?>
                            </div>
                            <div class="col-md-12 text-right">
                                <?=$this->Form->button("<i class='fa fa-save'></i> Salvar", ['type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false])?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>