<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Matmed</h2>
        <ol class="breadcrumb">
            <li>Fatura Matmed</li>
            <li class="active">
                <strong>                    Cadastrar Fatura Matmed
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="faturaMatmed form">
                        <?= $this->Form->create($faturaMatmed) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Fatura Matmed') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('data', ['type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('artigo_id');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('quantidade');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('vl_custos',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('vl_venda',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('vl_custo_medico',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('origin');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('codigo_tiss');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('codigo_tuss');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('codigo_convenio');
                                echo "</div>";
echo "<div class='col-md-6'>";
    echo $this->Form->input('atendimento_procedimento_id', ['data'=>'select','controller'=>'atendimentoProcedimentos','action'=>'fill',  'empty' => 'selecione']);
echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

