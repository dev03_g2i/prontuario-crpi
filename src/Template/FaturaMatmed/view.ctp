
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Matmed</h2>
        <ol class="breadcrumb">
            <li>Fatura Matmed</li>
            <li class="active">
                <strong>Litagem de Fatura Matmed</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Matmed</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Codigo Tiss') ?></th>
                                <td><?= h($faturaMatmed->codigo_tiss) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Codigo Tuss') ?></th>
                                <td><?= h($faturaMatmed->codigo_tuss) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Codigo Convenio') ?></th>
                                <td><?= h($faturaMatmed->codigo_convenio) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento Procedimento') ?></th>
                                                                <td><?= $faturaMatmed->has('atendimento_procedimento') ? $this->Html->link($faturaMatmed->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $faturaMatmed->atendimento_procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faturaMatmed->has('user') ? $this->Html->link($faturaMatmed->user->nome, ['controller' => 'Users', 'action' => 'view', $faturaMatmed->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturaMatmed->has('situacao_cadastro') ? $this->Html->link($faturaMatmed->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaMatmed->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Artigo Id') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->artigo_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->quantidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vl Custos') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->vl_custos) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vl Venda') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->vl_venda) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vl Custo Medico') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->vl_custo_medico) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Origin') ?></th>
                                <td><?= $this->Number->format($faturaMatmed->origin) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($faturaMatmed->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaMatmed->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaMatmed->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


