<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cadastrar Artigos</h2>
            <ol class="breadcrumb">
                <li><strong>Atendimento:</strong> <?=$dados->atendimento_id?> - <?=$dados->atendimento->cliente->nome?></li><br>
                <li><strong>Procedimento:</strong> <?=$dados->procedimento->nome ?></li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row area">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendas form">
                        <?= $this->Form->create('FaturaMatmed') ?>
                        <div class="col-md-3">
                            <?php echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($dados->atendimento->data, 'dd/MM/YYYY'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                        </div>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('artigo_id', ['label' => 'Artigos', 'empty' => 'Selecione', 'options' => $artigos, 'class' => 'select2', 'data' => 'select', 'controller' => 'EstqArtigos', 'action' => 'fill']);?>
                        </div>
                        <div class="col-md-2">
                            <?php echo $this->Form->input('quantidade', ['type' => 'number', 'default' => 1, 'min' => 1, 'max' => 999]);?>
                        </div>
                        <div class="col-md-1">
                            <div style="margin-top: 20px">
                                <?php echo $this->Form->button('<i class="fa fa-plus"></i>', ['type' => 'button','id' => 'btnAddArtigos', 'class' => 'btn btn-primary dim', 'escape' => false]) ?>
                            </div>
                        </div>
                        <?php echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $dados->atendimento_id]);?>
                        <?php echo $this->Form->input('atendimento_procedimento_id', ['type' => 'hidden', 'value' => $dados->id]);?>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover" id="show-artigos">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th>Código</th>
                                <th>Nome artigo</th>
                                <th>Quantidade</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>

                    <div class="col-md-12 text-right" style="margin-top: 15px;">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Finalizar</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
