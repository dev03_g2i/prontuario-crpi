<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Produtividade Solicitantes</h2>
            <ol class="breadcrumb">
                <li>Produtividade Solicitantes</li>
                <li class="active">
                    <strong>Listagem de Produtividade Solicitantes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('ProdutividadeSolicitante', ['type' => 'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('convenio_id', ['name' => 'ProdutividadeSolicitantes__convenio_id', 'data' => 'select', 'controller' => 'convenios', 'action' => 'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class="col-md-4">
                                <?=$this->Form->input('procedimento_id', ['name'=>'procedimento_id','data'=>'select','controller'=>'procedimentos','action'=>'fill', 'empty' => 'Selecione']);?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Produtividade Solicitante', ['action' => 'add', $solicitante_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Produtividade Solicitante','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Produtividade Solicitantes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                        <th><?= $this->Paginator->sort('solicitante_id') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('percentual_desconto') ?></th>
                                        <th><?= $this->Paginator->sort('percentual_repasse') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th><?= $this->Paginator->sort('user_id', ['label' => 'Quem Cadastrou']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($produtividadeSolicitantes as $produtividadeSolicitante): ?>
                                        <tr>
                                            <td><?= $this->Number->format($produtividadeSolicitante->id) ?></td>
                                            <td><?= $produtividadeSolicitante->has('convenio') ? $this->Html->link($produtividadeSolicitante->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $produtividadeSolicitante->convenio->id]) : '' ?></td>
                                            <td><?= $produtividadeSolicitante->has('solicitante') ? $this->Html->link($produtividadeSolicitante->solicitante->nome, ['controller' => 'Solicitantes', 'action' => 'view', $produtividadeSolicitante->solicitante->id]) : '' ?></td>
                                            <td><?= $produtividadeSolicitante->has('procedimento') ? $this->Html->link($produtividadeSolicitante->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $produtividadeSolicitante->procedimento->id]) : '' ?></td>
                                            <td><?= $this->Number->format($produtividadeSolicitante->percentual_desconto) ?></td>
                                            <td><?= $this->Number->format($produtividadeSolicitante->percentual_repasse) ?></td>
                                            <td><?= h($produtividadeSolicitante->created) ?></td>
                                            <td><?= h($produtividadeSolicitante->modified) ?></td>
                                            <td><?= $produtividadeSolicitante->has('user') ? $this->Html->link($produtividadeSolicitante->user->nome, ['controller' => 'Users', 'action' => 'view', $produtividadeSolicitante->user->id]) : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'produtividadeSolicitantes', 'action' => 'view', $produtividadeSolicitante->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'produtividadeSolicitantes', 'action' => 'edit', $produtividadeSolicitante->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), ['controller' => 'produtividadeSolicitantes', 'action' => 'delete', $produtividadeSolicitante->id], ['onclick' => 'excluir(event, this)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
