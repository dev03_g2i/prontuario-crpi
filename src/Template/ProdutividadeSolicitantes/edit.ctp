<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Produtividade Solicitantes</h2>
            <ol class="breadcrumb">
                <li>Produtividade Solicitantes</li>
                <li class="active">
                    <strong>
                                                Editar Produtividade Solicitantes
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="produtividadeSolicitantes form">
                            <?= $this->Form->create($produtividadeSolicitante) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Produtividade Solicitante') ?></legend>
                                                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('convenio_id', ['data'=>'select','controller'=>'convenios','action'=>'fill','data-value'=>$produtividadeSolicitante->convenio_id]); ?>
                                                                                            </div>
                                                                                        <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('solicitante_id', ['data'=>'select','controller'=>'solicitantes','action'=>'fill','data-value'=>$produtividadeSolicitante->solicitante_id]); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('percentual_desconto',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('percentual_repasse',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

