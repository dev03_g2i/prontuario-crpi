<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Produtividade Solicitantes</h2>
            <ol class="breadcrumb">
                <li>Produtividade Solicitantes</li>
                <li class="active">
                    <strong>
                        Cadastrar Produtividade Solicitantes
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="produtividadeSolicitantes form">
                            <?= $this->Form->create($produtividadeSolicitante,['id' => 'frm-produtividadesolicitantes']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Produtividade Solicitante') ?></legend>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('convenio_id', ['data' => 'select', 'controller' => 'convenios', 'action' => 'fill']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('procedimento_id', ['data' => 'select', 'controller' => 'procedimentos', 'action' => 'fill']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('percentual_desconto', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('percentual_repasse', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'verificaDuplicidade(\'frm-produtividadesolicitantes\')']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

