
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Produtividade Solicitantes</h2>
        <ol class="breadcrumb">
            <li>Produtividade Solicitantes</li>
            <li class="active">
                <strong>Litagem de Produtividade Solicitantes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Produtividade Solicitantes</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Convenio') ?></th>
                                                                <td><?= $produtividadeSolicitante->has('convenio') ? $this->Html->link($produtividadeSolicitante->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $produtividadeSolicitante->convenio->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Solicitante') ?></th>
                                                                <td><?= $produtividadeSolicitante->has('solicitante') ? $this->Html->link($produtividadeSolicitante->solicitante->nome, ['controller' => 'Solicitantes', 'action' => 'view', $produtividadeSolicitante->solicitante->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $produtividadeSolicitante->has('user') ? $this->Html->link($produtividadeSolicitante->user->nome, ['controller' => 'Users', 'action' => 'view', $produtividadeSolicitante->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($produtividadeSolicitante->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Percentual Desconto') ?></th>
                                <td><?= $this->Number->format($produtividadeSolicitante->percentual_desconto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Percentual Repasse') ?></th>
                                <td><?= $this->Number->format($produtividadeSolicitante->percentual_repasse) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($produtividadeSolicitante->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($produtividadeSolicitante->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


