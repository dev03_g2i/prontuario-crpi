<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Log Agendas</h2>
            <ol class="breadcrumb">
                <li>Log Agendas</li>
                <li class="active">
                    <strong>
                                                Editar Log Agendas
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="logAgendas form">
                            <?= $this->Form->create($logAgenda) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Log Agenda') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('log'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

