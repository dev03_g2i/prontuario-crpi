<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Log Agendas</h2>
            <ol class="breadcrumb">
                <li>Log Agendas</li>
                <li class="active">
                    <strong>Litagem de Log Agendas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Log Agendas</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($logAgenda->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($logAgenda->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($logAgenda->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Log') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= $logAgenda->log; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      
