<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Log Agendas</h2>
            <ol class="breadcrumb">
                <li>Log Agendas</li>
                <li class="active">
                    <strong>Listagem de Log Agendas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?= $this->Paginator->sort('id') ?></th>
                                            <th><?= $this->Paginator->sort('agenda_id', 'Código Agenda') ?></th>
                                            <th><?= $this->Paginator->sort('log') ?></th>
                                            <th>
                                                <?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($logAgendas as $logAgenda): ?>
                                        <tr>
                                            <td><?= $logAgenda->id; ?></td>
                                            <td><?= $logAgenda->agenda_id; ?></td>
                                            <td class="text-wrap-td"><?= $logAgenda->log; ?></td>
                                            <td><?= h($logAgenda->created); ?></td>
                                            <td><?= h($logAgenda->modified); ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'logAgendas','action' => 'view', $logAgenda->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <!--<?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'logAgendas','action'=>'delete', $logAgenda->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'logAgendas','action' => 'edit', $logAgenda->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>-->
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>