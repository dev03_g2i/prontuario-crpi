
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Textos</h2>
        <ol class="breadcrumb">
            <li>Textos</li>
            <li class="active">
                <strong>Litagem de Textos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Textos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Codigo') ?></th>
                                <td><?= h($texto->codigo) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $texto->has('user') ? $this->Html->link($texto->user->nome, ['controller' => 'Users', 'action' => 'view', $texto->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $texto->has('situacao_cadastro') ? $this->Html->link($texto->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $texto->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Texto Grupo') ?></th>
                                                                <td><?= $texto->has('texto_grupo') ? $this->Html->link($texto->texto_grupo->id, ['controller' => 'TextoGrupos', 'action' => 'view', $texto->texto_grupo->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($texto->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($texto->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($texto->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Rtf') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($texto->rtf)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Texto') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($texto->texto)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Texto Html') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($texto->texto_html)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


