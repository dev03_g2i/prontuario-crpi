<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Textos</h2>
            <ol class="breadcrumb">
                <li>Textos</li>
                <li class="active">
                    <strong>Cadastrar Textos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="textos form">
                            <?= $this->Form->create($texto) ?>
                            <fieldset>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('grupo_id', ['data'=>'select','controller'=>'textoGrupos','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('codigo');
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('texto_html', ['data' => 'ck']);
                                echo "</div>";
                                ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

