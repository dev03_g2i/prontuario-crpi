<div class="row chartsGragico">
    <?= $this->Form->input('controller', ['type' => 'hidden', 'value' => 'index'])?>
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <i class="fa fa-clock-o"></i> Acompanhamentos
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Atendimentos dos <strong>últimos 3 meses</strong></h5>
                        </div>
                        <div class="ibox-content">
                            <div>
                                <canvas id="barChart" height="140"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Atendimentos do mês: <strong class="text-uppercase"><?=$this->Time->format(date('Y/m/d'), 'MMMM')?></strong> </h5>
                        </div>
                        <div class="ibox-content">
                            <div>
                                <canvas id="doughnutChart" height="140"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-info pull-right"><i class="fa fa-calendar"></i></span>
                            <h5>Agendamentos dos <strong>últimos 3 meses</strong></h5>
                        </div>
                        <div class="ibox-content">
                            <canvas id="grafico" width="600" height="275" ></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                                    <span class="label label-primary pull-right">
                                        <i class="fa fa-clock-o"></i></span>
                            <h5>Agendamentos</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <td class="text-left">&nbsp;</td>
                                        <td class="text-left">Dia</td>
                                        <td class="text-left">Semana</td>
                                        <td class="text-left">Mes</td>
                                    </tr>
                                    <?php $i=0;
                                    foreach ($agendas as $agenda =>$value){ ?>
                                    <tr>
                                        <td style="background-color: <?php echo ($i%2==0)?'#F2F0F0': '#dedede';?>">
                                            <?=$agenda?>
                                        </td>

                                        <?php
                                        echo '</td>';
                                        echo '<td class="text-left">'.$value['dia'].'</td>';
                                        echo '<td class="text-left">'.$value['semana'].'</td>';
                                        echo '<td class="text-left">'.$value['mes'].'</td>';
                                        echo '</tr>';
                                        $i++;
                                        }
                                        ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
