<div class="row">
    <div class="col-sm-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3>Indicadores - Pacientes</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-sm-12 no-padding">
                            <strong>Qt total de pacientes</strong>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <h2><?= $qtTotalPacientes ?></h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-sm-12 no-padding">
                            <strong>Pacientes novos <?= $stringMesAnoAtual ?></strong>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <h2><?= $qtTotalPacientesNovosAtual ?></h2>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-sm-12 no-padding">
                            <strong>Pacientes novos <?= $stringMesAnteriorAnoAtual ?></strong>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <h2><?= $qtTotalPacientesNovosAnterior ?></h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <i class="fa fa-birthday-cake"></i> Aniversariantes
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'lista', '?' => ['tipo' => 1]]) ?>" data-toggle="modal" data-target="#modal_lg">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                    <span class="label label-primary pull-right"><i
                                            class="fa fa-birthday-cake"></i></span>
                                <h5>Dia</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?= $dia ?></h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'lista', '?' => ['tipo' => 2]]) ?>" data-toggle="modal" data-target="#modal_lg">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <span class="label label-info pull-right"><i class="fa fa-birthday-cake"></i></span>
                                <h5>Semana</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?= $semana ?></h1>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-4">
                    <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'lista', '?' => ['tipo' => 3]]) ?>" data-toggle="modal" data-target="#modal_lg">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                    <span class="label label-warning pull-right">
                                        <i class="fa fa-birthday-cake"></i>
                                    </span>
                                <h5>Mês</h5>
                            </div>
                            <div class="ibox-content">
                                <h1 class="no-margins"><?= $mes ?></h1>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
