<div class="row chartAtendimentoGeral">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3>Indicadores - Atendimento Geral</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-md-2">
                                <?php echo $this->Form->input('mes', ['class' => 'mes', 'label' => 'Mês', 'options' => $meses, 'default' => date('m')]) ?>
                            </div>
                            <div class="col-md-2">
                                <?php echo $this->Form->input('ano', ['class' => 'ano', 'options' => $anos, 'default' => date('Y')]) ?>
                            </div>
                            <div class="col-md-2">
                                <?php echo $this->Form->button('<i class="fa fa-refresh"></i> Atualizar', ['class' => 'btn btn-primary btnAtualizaProcedimento', 'style' => 'margin-top: 23px']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <canvas id="procedimentos" width="100%" height="40" ></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-md-2">
                                <?php echo $this->Form->input('mes', ['class' => 'mes', 'label' => 'Mês', 'options' => $meses, 'default' => date('m')]) ?>
                            </div>
                            <div class="col-md-2">
                                <?php echo $this->Form->input('ano', ['class' => 'ano', 'options' => $anos, 'default' => date('Y')]) ?>
                            </div>
                            <div class="col-md-2">
                                <?php echo $this->Form->button('<i class="fa fa-refresh"></i> Atualizar', ['class' => 'btn btn-primary btnAtualizaPaciente', 'style' => 'margin-top: 23px']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <canvas id="pacientes" width="100%" height="40" ></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>