

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situação Cadastros</h2>
        <ol class="breadcrumb">
            <li>Situação Cadastros</li>
            <li class="active">
                <strong>Listagem de Situação Cadastros</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="situacaoCadastros">
    <h3><?= h($situacaoCadastro->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($situacaoCadastro->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Codigo') ?></th>
            <td><?= $this->Number->format($situacaoCadastro->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Criado') ?></th>
            <td><?= h($situacaoCadastro->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado') ?></th>
            <td><?= h($situacaoCadastro->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

