<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Agendas</h2>
        <ol class="breadcrumb">
            <li>Grupo Agendas</li>
            <li class="active">
                <strong>Litagem de Grupo Agendas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('Grupo Agenda', ['type' => 'get']) ?>
                        <?php
                        echo "<div class='col-md-4'>";
                        echo $this->Form->input('nome');
                        echo "</div>";
                        echo "<div class='col-md-4'>";
                        echo $this->Form->input('medico_id');
                        echo "</div>";
                        ?>
                        <div class="col-md-4" style="margin-top: 25px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Grupo Agendas', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Grupo Agendas', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Grupo Agendas') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('medico_id',['label'=>'Profissional']) ?></th>
                                    <th><?= $this->Paginator->sort('solicitante_id',['label'=>'Solicitante Padrão']) ?></th>
                                    <th><?= $this->Paginator->sort('intervalo') ?></th>
                                    <th><?= $this->Paginator->sort('inicio',['label'=>'Início agenda']) ?></th>
                                    <th><?= $this->Paginator->sort('fim',['label'=>'Fim agenda']) ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($grupoAgendas as $grupoAgenda): ?>
                                    <tr>
                                        <td><?= $this->Number->format($grupoAgenda->id) ?></td>
                                        <td><?= h($grupoAgenda->nome) ?></td>
                                        <td><?= $grupoAgenda->has('medico_responsavei') ? $grupoAgenda->medico_responsavei->nome :''; ?></td>
                                        <td><?= $grupoAgenda->has('solicitante') ? $grupoAgenda->solicitante->nome :''; ?></td>
                                        <td><?= $this->Time->format($grupoAgenda->intervalo,'HH:mm:ss') ?></td>
                                        <td><?= $this->Time->format($grupoAgenda->inicio,'HH:mm:ss') ?></td>
                                        <td><?= $this->Time->format($grupoAgenda->fim,'HH:mm:ss') ?></td>
                                        <td><?= h($grupoAgenda->created) ?></td>
                                        <td><?= h($grupoAgenda->modified) ?></td>
                                        <!--<td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-clock-o"></i>', ['controller' => 'AgendaPeriodos', 'action' => 'index', '?' => ['grupo_agenda_id' => $grupoAgenda->id,'first'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Periodos', 'escape' => false, 'class' => 'btn btn-xs btn-warning', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                            <?= $this->Html->link('<i class="fa fa-lock"></i>', ['controller' => 'AgendaBloqueios', 'action' => 'index', '?' => ['grupo_agenda_id' => $grupoAgenda->id,'first'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Bloqueio Agenda', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                            <?= $this->Html->link('<i class="fa fa-sign-in"></i>', ['controller' => 'GrupoAgendaHorarios', 'action' => 'index', $grupoAgenda->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Horários', 'escape' => false, 'class' => 'btn btn-xs btn-success', 'target' => '_blank']) ?>
                                            <?= $this->Html->link('<i class="fa fa-clock-o"></i>', ['controller' => 'Agendas', 'action' => 'add-horarios', 'first' => 1], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Horário Manual', 'escape' => false, 'class' => 'btn btn-xs btn-info', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $grupoAgenda->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                            <?= $this->Html->link(  $this->Html->icon('pencil'), ['action' => 'edit', $grupoAgenda->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $grupoAgenda->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $grupoAgenda->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>-->
                                        <td class="actions">
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link('<i class="fa fa-clock-o"></i> Períodos', ['controller' => 'AgendaPeriodos', 'action' => 'index', '?' => ['grupo_agenda_id' => $grupoAgenda->id,'first'=>1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                    <li><?= $this->Html->link('<i class="fa fa-lock"></i> Bloqueio Agenda (Calendário)', ['controller' => 'AgendaBloqueios', 'action' => 'index', '?' => ['grupo_agenda_id' => $grupoAgenda->id,'first'=>1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                    <li class="<?//= $this->Configuracao->showGeracaoHorarios()?>"><?= $this->Html->link('<i class="fa fa-sign-in"></i> Gerar, Bloquear e Desativar Horários (Lista)', ['controller' => 'GrupoAgendaHorarios', 'action' => 'index', $grupoAgenda->id], ['escape' => false, 'target' => '_blank']) ?></li>
                                                    <li class="<?= $this->Configuracao->showGeracaoHorarios()?>"><?= $this->Html->link('<i class="fa fa-clock-o"></i> Gerar Horário Manual', ['controller' => 'Agendas', 'action' => 'add-horarios', 'first' => 1], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                    <li><?= $this->Html->link($this->Html->icon('list-alt').' Detalhes', ['action' => 'view', $grupoAgenda->id], ['escape' => false]) ?></li>
                                                    <li><?= $this->Html->link(  $this->Html->icon('pencil').' Editar', ['action' => 'edit', $grupoAgenda->id], ['escape' => false]) ?></li>
                                                    <li><?= $this->Form->postLink($this->Html->icon('remove').' Deletar', ['action' => 'delete', $grupoAgenda->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $grupoAgenda->id), 'escape' => false]) ?></li>
                                                </ul>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

