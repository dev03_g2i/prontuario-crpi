<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Agendas</h2>
        <ol class="breadcrumb">
            <li>Grupo Agendas</li>
            <li class="active">
                <strong> Cadastrar Grupo Agendas
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="grupoAgendas form">
                        <?php require_once('form.ctp'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
