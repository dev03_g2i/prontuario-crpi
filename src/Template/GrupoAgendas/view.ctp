

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Agendas</h2>
        <ol class="breadcrumb">
            <li>Grupo Agendas</li>
            <li class="active">
                <strong>Litagem de Grupo Agendas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Grupo Agendas</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($grupoAgenda->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situação Cadastro') ?></th>
                                <td><?= $grupoAgenda->has('situacao_cadastro') ? $this->Html->link($grupoAgenda->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoAgenda->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quem Cadastrou') ?></th>
                                <td><?= $grupoAgenda->has('user') ? $this->Html->link($grupoAgenda->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoAgenda->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($grupoAgenda->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Medico Id') ?></th>
                                <td><?= $this->Number->format($grupoAgenda->medico_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dt. Criação') ?></th>
                                <td><?= h($grupoAgenda->created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dt. Modificação') ?></th>
                                <td><?= h($grupoAgenda->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


