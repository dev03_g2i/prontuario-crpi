<?= $this->Form->create($grupoAgenda) ?>
    <fieldset>
        <legend>
            <?= __('Cadastrar Grupo Agenda') ?>
        </legend>
        <div class='col-md-6'>
            <?php echo $this->Form->input('nome'); ?>
        </div>
        <div class='col-md-6'>
            <?php echo $this->Form->input('medico_id',['label'=>'Profissional','options'=>$medicoResponsaveis,'empty'=>'Selecione']); ?>
        </div>
        <div class='col-md-6'>
            <?php echo $this->Form->input('inicio',['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])],'value'=>'07:00']); ?>
        </div>
        <div class='col-md-6'>
            <?php echo $this->Form->input('fim',['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])],'value'=>'19:00']); ?>
        </div>
        <div class='col-md-6'>
            <?php //echo $this->Form->input('intervalo',['class' => 'timepicker', 'type' => 'text', /*'type'=>'select', 'options'=>['00:01:00'=>'00:01','00:10:00'=>'00:10','00:15:00'=>'00:15','00:20:00'=>'00:20','00:30:00'=>'00:30','00:35:00'=>'00:35'], 'empty'=>'Selecione',*/ 'required'=>'required' ]); ?>
            <?php echo $this->Form->input('intervalo',['type'=>'text', 'class' => 'timepiker', 'value' => @date_format($grupoAgenda->intervalo, 'H:i'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
        </div>
        <div class='col-md-6'>
            <?php echo $this->Form->input('solicitante_id', ['type' => 'select', 'data' => 'select', 'controller' => 'Solicitantes', 'action' => 'fill', 'empty' => 'Selecione', 'data-value' => $grupoAgenda->solicitante_id,
                'append' => [
                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f']),
                    $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Solicitantes', 'action' => 'view', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Solicitante'])
                ]
            ]); ?>
        </div>
    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
<?= $this->Form->end() ?>