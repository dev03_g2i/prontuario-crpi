<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Particularidades</h2>
            <ol class="breadcrumb">
                <li>Agendas</li>
                <li class="active">
                    <strong>Particularidades de Agendas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins p-l-15 p-r-15">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs" role="tablist">
                            <?php
                            $count = 0;
                            foreach ($grupos as $grupo) : ?>
                                <li><a class="nav-link active" data-toggle="tab" href="#tab-<?= $count++ ?>" listen='f'><?= $grupo->nome ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                        <div class="tab-content">
                            <?php
                            $count = 0;
                            foreach ($grupos as $grupo) : ?>
                                <div class="tab-pane <?= $count == 0 ? 'active' : '' ?>" role="tabpanel" id="tab-<?= $count++ ?>">
                                    <?= $this->Form->create($grupos, ['id' => 'form-particularidade' . $grupo->id]) ?>
                                    <div class="panel-body">
                                        <?= $this->Form->hidden('id', ['value' => $grupo->id]); ?>
                                        <?= $this->Form->input('particularidades', ['id' => 'particularidade'.$grupo->id, 'type'=>'textarea', 'data'=>'ck', 'ck-height' => '300px', 'value' => ($grupo->particularidades != strip_tags($grupo->particularidades)) ? str_replace('%<p></p>%', '', $grupo->particularidades) : nl2br($grupo->particularidades) ]) ?>
                                        <div class="col-md-12 text-right p-r-0">
                                            <?= $this->Html->link('Fechar', '#', ['class' => 'btn btn-default', 'onclick' => 'closeModal()']) ?>
                                            <?= $this->Form->button('Salvar', ['class' => 'btn btn-primary', 'onclick' => "EnviarFormulario('form-particularidade". $grupo->id ."', 'atualizar')"]) ?>
                                        </div>
                                    </div>
                                </div>
                                <?= $this->Form->end() ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->scriptBlock('function closeModal() {$(".close").click()}'); ?>