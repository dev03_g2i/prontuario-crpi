<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Grupo Procedimentos</li>
            <li class="active">
                <strong> Editar Grupo Procedimentos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="grupoProcedimentos form">
                        <?= $this->Form->create($grupoProcedimento) ?>
                        <fieldset>
                            <legend><?= __('Editar Grupo Procedimento') ?></legend>
                            <div class='col-md-4'>
                             <?php echo $this->Form->input('nome');?>
                            </div>
                            <div class='col-md-4'>
                             <?php echo $this->Form->input('percentual_lucro',['mask'=>'money','type'=>'text'])?>
                            </div>
                            <div class='col-md-4'>
                             <?php echo $this->Form->input('percentual_desconto',['mask'=>'money','type'=>'text'])?>
                            </div>
                            <div class="col-sm-4">
                                <label for="color" label="control-label">Cor do grupo no gráfico *</label>
                                <div id="cp5" class="input-group colorpicker-component form-group text required" required="required" title="Using format option">
                                    <input name="color" id="color" type="text" class="input-group-addon form-control" required="required" readonly value="rgba(1,1,1,1)"/>
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

