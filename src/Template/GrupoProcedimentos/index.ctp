<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Grupo Procedimentos</li>
            <li class="active">
                <strong>Listagem de Grupo Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Grupo Procedimentos', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Grupo Procedimentos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Grupo Procedimentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('percentual_lucro',['label'=>'Percentual Pagamento']) ?></th>
                                    <th><?= $this->Paginator->sort('percentual_desconto') ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($grupoProcedimentos as $grupoProcedimento): ?>
                                    <tr>
                                        <td><?= $this->Number->format($grupoProcedimento->id) ?></td>
                                        <td><?= h($grupoProcedimento->nome) ?></td>
                                        <td><?= $this->Number->toPercentage($grupoProcedimento->percentual_lucro) ?></td>
                                        <td><?= $this->Number->toPercentage($grupoProcedimento->percentual_desconto) ?></td>
                                        <td><?= h($grupoProcedimento->created) ?></td>
                                        <td><?= h($grupoProcedimento->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $grupoProcedimento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $grupoProcedimento->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $grupoProcedimento->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

