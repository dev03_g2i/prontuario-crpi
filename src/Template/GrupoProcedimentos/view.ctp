

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Grupo Procedimentos</li>
            <li class="active">
                <strong>Listagem de Grupo Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="grupoProcedimentos">
    <h3><?= h($grupoProcedimento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($grupoProcedimento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situação Cadastro') ?></th>
            <td><?= $grupoProcedimento->has('situacao_cadastro') ? $this->Html->link($grupoProcedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoProcedimento->situacao_cadastro->nome
                ]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Usuário') ?></th>
            <td><?= $grupoProcedimento->has('user') ? $this->Html->link($grupoProcedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoProcedimento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Codigo') ?></th>
            <td><?= $this->Number->format($grupoProcedimento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Criado') ?></th>
            <td><?= h($grupoProcedimento->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($grupoProcedimento->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

