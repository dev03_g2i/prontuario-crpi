
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Estq Artigos</h2>
        <ol class="breadcrumb">
            <li>Estq Artigos</li>
            <li class="active">
                <strong>Litagem de Estq Artigos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Estq Artigos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($estqArtigo->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tamanho') ?></th>
                                <td><?= h($estqArtigo->tamanho) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $estqArtigo->has('user') ? $this->Html->link($estqArtigo->user->nome, ['controller' => 'Users', 'action' => 'view', $estqArtigo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Codigo Livre') ?></th>
                                <td><?= h($estqArtigo->codigo_livre) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($estqArtigo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Subgrupo') ?></th>
                                <td><?= $this->Number->format($estqArtigo->subgrupo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Controla Lote') ?></th>
                                <td><?= $this->Number->format($estqArtigo->controla_lote) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vlcomercial') ?></th>
                                <td><?= $this->Number->format($estqArtigo->vlcomercial) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vlcusto') ?></th>
                                <td><?= $this->Number->format($estqArtigo->vlcusto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vlcustomedio') ?></th>
                                <td><?= $this->Number->format($estqArtigo->vlcustomedio) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Unidade') ?></th>
                                <td><?= $this->Number->format($estqArtigo->unidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Id') ?></th>
                                <td><?= $this->Number->format($estqArtigo->situacao_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Origem') ?></th>
                                <td><?= $this->Number->format($estqArtigo->origem) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Repasse') ?></th>
                                <td><?= $this->Number->format($estqArtigo->valor_repasse) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Usuario Dt') ?></th>
                                                                <td><?= h($estqArtigo->usuario_dt) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($estqArtigo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($estqArtigo->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Ensaio') ?>6</th>
                                <td><?= $estqArtigo->ensaio ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Utiliza Repasse') ?>6</th>
                                <td><?= $estqArtigo->utiliza_repasse ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($estqArtigo->observacao)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


