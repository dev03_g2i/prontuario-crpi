<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Artigos</h2>
            <ol class="breadcrumb">
                <li>Artigos</li>
                <li class="active">
                    <strong>Editar Artigos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="estqArtigos form">
                            <?= $this->Form->create($estqArtigo) ?>
                            <div class='col-md-6'>
                                <?php echo $this->Form->input('artigo_id', ['label' => 'Artigos', 'empty' => 'Selecione', 'options' => $artigos, 'class' => 'select2', 'data' => 'select', 'controller' => 'EstqArtigos', 'action' => 'fill', 'data-value' => $estqArtigo->id]); ?>
                            </div>
                            <div class='col-md-6'>
                                <?php echo $this->Form->input('codigo_tiss', ['label' => 'Código Tiss']); ?>
                            </div>
                            <div class='col-md-6'>
                                <?php echo $this->Form->input('codigo_tuss', ['label' => 'Código Tuss']); ?>
                            </div>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

