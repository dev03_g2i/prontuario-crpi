<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Preço Artigo</h2>
            <ol class="breadcrumb">
                <li>Preço Artigo</li>
                <li class="active">
                    <strong>Cadastrar preço artigo</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaPrecoartigo form">
                            <?= $this->Form->create($faturaPrecoartigo, ['id' => 'frm-preco-artigo']) ?>
                            <fieldset>
                                <legend><?= __('Mat/Med - Precifcação de Convênios') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('artigo_id', ['label' => 'Artigos', 'empty' => 'Selecione', 'options' => $artigos, 'class' => 'select2', 'data' => 'select', 'controller' => 'EstqArtigos', 'action' => 'fill', 'data-value' => $artigo_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('convenio_id', ['data'=>'select','controller'=>'convenios','action'=>'fill',  'empty' => 'selecione', 'required' => true, 'data-value' => $convenio_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('codigo', ['label' => 'Cod.Convênio', 'maxLenght' => 10,  'required' => true]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money',  'required' => true]);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['type' => 'button', 'class'=>'btn btn-primary', 'onclick' => 'verificaDuplicidade(\'frm-preco-artigo\')']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

