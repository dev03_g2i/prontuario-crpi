<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Mat/Med</h2>
            <h4>Preço x Convênios</h4>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaPrecoartigo',['type'=>'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('artigo_id', ['label' => 'Artigos', 'empty' => 'Selecione', 'options' => $artigos, 'class' => 'select2', 'data' => 'select', 'controller' => 'EstqArtigos', 'action' => 'fill']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('codigo',['name'=>'FaturaPrecoartigo__codigo']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Preço artigo', ['action' => 'add', '?' => ['convenio_id' => $convenio_id, 'artigo_id' => $artigo_id, 'first' => 1]],['data-toggle'=>'modal','data-target' => '#modal_lg', 'class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fatura Precoartigo') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('artigo_id') ?></th>
                                        <th><?= $this->Paginator->sort('codigo', 'Cod.Convênio') ?></th>
                                        <th><?= $this->Paginator->sort('valor') ?></th>
                                        <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaPrecoartigo as $fpa): ?>
                                        <tr>
                                            <td><?= $this->Number->format($fpa->id) ?></td>
                                            <td><?= $fpa->has('estq_artigo') ? $fpa->estq_artigo->nome : '' ?></td>
                                            <td><?= h($fpa->codigo) ?></td>
                                            <td><?= $this->Number->format($fpa->valor, ['places' => 2]) ?></td>
                                            <td><?= $fpa->has('convenio') ? $this->Html->link($fpa->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $fpa->convenio->id]) : '' ?></td>
                                            <td><?= $fpa->has('user') ? $this->Html->link($fpa->user->nome, ['controller' => 'Users', 'action' => 'view', $fpa->user->id]) : '' ?></td>
                                            <td><?= $fpa->has('situacao_cadastro') ? $this->Html->link($fpa->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $fpa->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= h($fpa->created) ?></td>
                                            <td><?= h($fpa->modified) ?></td>
                                            <td class="actions">
                                                <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaPrecoartigo','action' => 'view', $fpa->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>-->
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaPrecoartigo','action' => 'edit', $fpa->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturaPrecoartigo','action'=>'delete', $fpa->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
