
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Precoartigo</h2>
        <ol class="breadcrumb">
            <li>Fatura Precoartigo</li>
            <li class="active">
                <strong>Litagem de Fatura Precoartigo</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Precoartigo</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Codigo') ?></th>
                                <td><?= h($faturaPrecoartigo->codigo) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Convenio') ?></th>
                                                                <td><?= $faturaPrecoartigo->has('convenio') ? $this->Html->link($faturaPrecoartigo->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $faturaPrecoartigo->convenio->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faturaPrecoartigo->has('user') ? $this->Html->link($faturaPrecoartigo->user->nome, ['controller' => 'Users', 'action' => 'view', $faturaPrecoartigo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturaPrecoartigo->has('situacao_cadastro') ? $this->Html->link($faturaPrecoartigo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaPrecoartigo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaPrecoartigo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Artigo Id') ?></th>
                                <td><?= $this->Number->format($faturaPrecoartigo->artigo_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor') ?></th>
                                <td><?= $this->Number->format($faturaPrecoartigo->valor) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaPrecoartigo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaPrecoartigo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


