<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Recebimentos</h2>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="situacaoRecebimentos form">
                        <?= $this->Form->create('SituacaoRecebimentos') ?>
                        <fieldset>
                            <legend><?= __('Procedimentos') ?></legend>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('total', ['label' => 'Valor Original','prepend' => 'R$', 'type' => 'text', 'readonly' => true,'mask' => 'money', 'value' => ($atendimentoProcedimento->total)?$atendimentoProcedimento->total:'0.00'])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('situacao_recebimento_id', ['label' => 'Situação', 'type' => 'select', 'options' => $situacoes, 'default' => ($atendimentoProcedimento->situacao_recebimento_id)?$atendimentoProcedimento->situacao_recebimento_id:'', 'empty' => 'Selecione'])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('dt_recebimento', ['label' => 'Data Recebimento','type' => 'text', 'class' => 'datepicker', 'value' => ($atendimentoProcedimento->dt_recebimento)?$this->Time->format($atendimentoProcedimento->dt_recebimento, 'dd/MM/YYYY'):''])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('valor_recebido', ['label' => 'Valor Recebido','prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'value' => ($atendimentoProcedimento->valor_recebido)?$atendimentoProcedimento->valor_recebido:'0.00'])?>
                            </div>
                        </fieldset>

                        <fieldset class="<?=$this->Configuracao->MatMedFatura()?>">
                            <legend><?= __('Mat/Med') ?></legend>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('valor_material', ['label' => 'Valor Original','prepend' => 'R$', 'type' => 'text', 'readonly' => true,'mask' => 'money', 'value' => ($atendimentoProcedimento->valor_material)?$atendimentoProcedimento->valor_material:'0.00'])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('situacao_recmatmed_id', ['label' => 'Situação', 'type' => 'select', 'options' => $situacoes, 'default' => ($atendimentoProcedimento->situacao_recmatmed_id)?$atendimentoProcedimento->situacao_recmatmed_id:'', 'empty' => 'Selecione'])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('dt_recebimento', ['label' => 'Data Recebimento','type' => 'text', 'class' => 'datepicker', 'value' => ($atendimentoProcedimento->dt_recebimento)?$this->Time->format($atendimentoProcedimento->dt_recebimento, 'dd/MM/YYYY'):''])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('valor_rec_matmed', ['label' => 'Valor Recebido','prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'value' => ($atendimentoProcedimento->valor_rec_matmed)?$atendimentoProcedimento->valor_rec_matmed:'0.00'])?>
                            </div>
                        </fieldset>

                        <div class="clearfix"></div>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->button('<i class="fa fa-save"></i> Salvar',['type' => 'submit', 'class'=>'btn btn-primary']) ?>
                        </div>

                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

