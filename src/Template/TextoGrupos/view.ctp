
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Texto Grupos</h2>
        <ol class="breadcrumb">
            <li>Texto Grupos</li>
            <li class="active">
                <strong>Litagem de Texto Grupos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Texto Grupos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($textoGrupo->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $textoGrupo->has('situacao_cadastro') ? $this->Html->link($textoGrupo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $textoGrupo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $textoGrupo->has('user') ? $this->Html->link($textoGrupo->user->nome, ['controller' => 'Users', 'action' => 'view', $textoGrupo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($textoGrupo->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($textoGrupo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($textoGrupo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


