<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Texto Grupos</h2>
            <ol class="breadcrumb">
                <li>Texto Grupos</li>
                <li class="active">
                    <strong>                    Cadastrar Texto Grupos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="textoGrupos form">
                            <?= $this->Form->create($textoGrupo) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Texto Grupo') ?></legend>

                                <div class='col-md-6'>
                                    <?php echo $this->Form->input('nome'); ?>
                                </div>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

