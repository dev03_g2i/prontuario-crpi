<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Texto Grupos</h2>
            <ol class="breadcrumb">
                <li>Texto Grupos</li>
                <li class="active">
                    <strong>Litagem de Texto Grupos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('TextoGrupo',['type'=>'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('nome',['name'=>'TextoGrupos__nome']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Texto Grupos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Texto Grupos','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Texto Grupos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($textoGrupos as $textoGrupo): ?>
                                        <tr>
                                            <td><?= $this->Number->format($textoGrupo->id) ?></td>
                                            <td><?= h($textoGrupo->nome) ?></td>
                                            <td><?= $textoGrupo->has('situacao_cadastro') ? $this->Html->link($textoGrupo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $textoGrupo->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= $textoGrupo->has('user') ? $this->Html->link($textoGrupo->user->nome, ['controller' => 'Users', 'action' => 'view', $textoGrupo->user->id]) : '' ?></td>
                                            <td><?= h($textoGrupo->created) ?></td>
                                            <td><?= h($textoGrupo->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'textoGrupos','action' => 'view', $textoGrupo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'textoGrupos','action' => 'edit', $textoGrupo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'textoGrupos','action'=>'delete', $textoGrupo->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
