
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Origens</h2>
        <ol class="breadcrumb">
            <li>Origens</li>
            <li class="active">
                <strong>Litagem de Origens</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Origens</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($origen->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $origen->has('situacao_cadastro') ? $this->Html->link($origen->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $origen->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $origen->has('user') ? $this->Html->link($origen->user->nome, ['controller' => 'Users', 'action' => 'view', $origen->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem modificou') ?></th>
                                                                <td><?= $origen->has('users_up') ? $this->Html->link($origen->users_up->nome, ['controller' => 'Users', 'action' => 'view', $origen->users_up->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($origen->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($origen->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($origen->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


