<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Pacientes</h2>
            <ol class="breadcrumb">
                <li>pacientes</li>
                <li class="active">
                    <strong>Vínculos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Novo vínculo', ['action' => 'add','?'=>['cliente_id'=>$cliente_id]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Vínculo', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Cliente Ligacoes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('parent_id',['label'=>'Vínculo']) ?></th>
                                        <th><?= $this->Paginator->sort('grau_id') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Última Atualização']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($clienteLigacoes as $clienteLigaco): ?>
                                        <tr>
                                            <td><?= $clienteLigaco->has('parent_cliente_ligaco') ? $this->Html->link($clienteLigaco->parent_cliente_ligaco->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteLigaco->parent_cliente_ligaco->id],['target'=>'_blank','listen'=>'f']) : '' ?></td>
                                            <td><?= $clienteLigaco->has('grau_parentesco') ? $clienteLigaco->grau_parentesco->nome : '' ?></td>
                                            <td><?= h($clienteLigaco->created) ?></td>
                                            <td><?= h($clienteLigaco->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'clienteLigacoes\',' . $clienteLigaco->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

