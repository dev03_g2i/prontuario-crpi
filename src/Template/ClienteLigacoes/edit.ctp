<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cliente Ligacoes</h2>
            <ol class="breadcrumb">
                <li>Cliente Ligacoes</li>
                <li class="active">
                    <strong> Editar Cliente Ligacoes
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clienteLigacoes form">
                            <?= $this->Form->create($clienteLigaco) ?>
                            <fieldset>
                                <legend><?= __('Editar Cliente Ligaco') ?></legend>
                                <?php
                                echo $this->Form->input('cliente_id', ['type' => 'hidden',  'value' => $clienteLigaco->cliente_id]);

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('parent_id', ['data' => 'select', 'controller' => 'clientes', 'action' => 'fill', 'data-value' => $clienteLigaco->parent_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('grau_id', ['options' => $grau, 'empty' => 'selecione']);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

