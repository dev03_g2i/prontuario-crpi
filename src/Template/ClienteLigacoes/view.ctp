
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Ligacoes</h2>
        <ol class="breadcrumb">
            <li>Cliente Ligacoes</li>
            <li class="active">
                <strong>Litagem de Cliente Ligacoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Cliente Ligacoes</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Cliente') ?></th>
                                                                <td><?= $clienteLigaco->has('cliente') ? $this->Html->link($clienteLigaco->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteLigaco->cliente->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Parent Cliente Ligaco') ?></th>
                                                                <td><?= $clienteLigaco->has('parent_cliente_ligaco') ? $this->Html->link($clienteLigaco->parent_cliente_ligaco->id, ['controller' => 'ClienteLigacoes', 'action' => 'view', $clienteLigaco->parent_cliente_ligaco->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Grau Parentesco') ?></th>
                                                                <td><?= $clienteLigaco->has('grau_parentesco') ? $this->Html->link($clienteLigaco->grau_parentesco->nome, ['controller' => 'GrauParentescos', 'action' => 'view', $clienteLigaco->grau_parentesco->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $clienteLigaco->has('situacao_cadastro') ? $this->Html->link($clienteLigaco->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $clienteLigaco->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $clienteLigaco->has('user') ? $this->Html->link($clienteLigaco->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteLigaco->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($clienteLigaco->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($clienteLigaco->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($clienteLigaco->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Cliente Ligacoes') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($clienteLigaco->child_cliente_ligacoes)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Parent Id') ?></th>
                        <th><?= __('Grau Id') ?></th>
                        <th><?= __('Situacao Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('User Id') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($clienteLigaco->child_cliente_ligacoes as $childClienteLigacoes): ?>
                <tr>
                    <td><?= h($childClienteLigacoes->id) ?></td>
                    <td><?= h($childClienteLigacoes->cliente_id) ?></td>
                    <td><?= h($childClienteLigacoes->parent_id) ?></td>
                    <td><?= h($childClienteLigacoes->grau_id) ?></td>
                    <td><?= h($childClienteLigacoes->situacao_id) ?></td>
                    <td><?= h($childClienteLigacoes->created) ?></td>
                    <td><?= h($childClienteLigacoes->user_id) ?></td>
                    <td><?= h($childClienteLigacoes->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'ClienteLigacoes','action' => 'view', $childClienteLigacoes->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'ClienteLigacoes','action' => 'edit', $childClienteLigacoes->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'ClienteLigacoes','action' => 'delete', $childClienteLigacoes->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $clienteLigaco->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


