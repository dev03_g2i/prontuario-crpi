<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cliente</h2>
            <ol class="breadcrumb">
                <li>Vínculos</li>
                <li class="active">
                    <strong> Cadastrar </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clienteLigacoes form">
                            <?= $this->Form->create($clienteLigaco) ?>
                            <fieldset>
                                <?php
                                if(!empty($cliente_id)){
                                    echo $this->Form->input('cliente_id', ['type' => 'hidden', 'value' => $cliente_id]);
                                }else{
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('cliente_id', ['data' => 'select', 'controller' => 'clientes', 'action' => 'fill']);
                                    echo "</div>";
                                }
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('parent_id', ['label'=>'Vínculo - (Prontuário/Nome)','data' => 'select', 'controller' => 'clientes', 'action' => 'fill-prontuario']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('grau_id', ['options'=>$grau,'empty'=>'selecione']);
                                echo "</div>";

                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

