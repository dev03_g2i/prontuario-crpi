<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Preço Procedimentos</h2>
            <ol class="breadcrumb">
                <li>Preço Procedimentos</li>
                <li class="active">
                    <strong>Listagem de Preço Procedimentos</strong>
                </li>
            </ol>
            <?php if(!empty($dados_convenio)):?>
                <h4>Convênio: <?=$dados_convenio->nome?></h4>
            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <?= $this->Form->create('PrecoProcedimentos',['type'=>'get']) ?>
                            <div class="col-md-4">
                                <?= $this->Form->input('procedimento',['type' => 'select', 'class' => 'select2', 'options' => $procedimentos, 'empty' => 'Selecione', 'default' => $procedimento_id]);?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->input('grupo_procedimento',['empty'=>'selecione','options'=>$grupo_procedimentos]);?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>

                <div class="text-right">
                    <p>
                        <?php echo $this->Html->link($this->Html->icon('plus').' Cadastrar Preco Procedimentos', ['action' => 'add', '?'=>['convenio_id' => $convenio_id, 'first' => 1]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Preco Procedimentos','class'=>'btn btn-primary','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Preco Procedimentos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                                        <th><?= $this->Paginator->sort('convenio_id', ['label' => 'Convênio']) ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('codigo') ?></th>
                                        <th><?= $this->Paginator->sort('valor_faturar') ?></th>
                                        <th><?= $this->Paginator->sort('valor_particular') ?></th>
                                        <th><?= $this->Paginator->sort('regra') ?></th>
                                        <th class="<?=$this->Configuracao->showUco()?>"><?= $this->Paginator->sort('uco') ?></th>
                                        <th class="<?=$this->Configuracao->showFilme()?>"><?= $this->Paginator->sort('filme', 'Filme M2') ?></th>
                                        <th class="<?=$this->Configuracao->showPorte()?>"><?= $this->Paginator->sort('porte') ?></th>
                                        <th class="<?=$this->Configuracao->showFilmeReais()?>"><?= $this->Paginator->sort('filme_reais', 'Filme R$') ?></th>
                                        <th class="<?=$this->Faturamento->mostraTabelaPrecoInstrumentador()?>"><?= $this->Paginator->sort('valor_instrumentador', 'Vl.Instrumentador') ?></th>
                                        <th><?= $this->Paginator->sort('user_id', 'Cadastrado Por') ?></th>
                                        <th><?= $this->Paginator->sort('user_update', 'Alterado Por') ?></th>
                                        <th><?= $this->Paginator->sort('modified', 'Data/Hora') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $options = ['places' => 2];
                                    foreach ($precoProcedimentos as $precoProcedimento): ?>
                                        <tr class="delete<?=$precoProcedimento->id?>">
                                            <td><?= $this->Number->format($precoProcedimento->id) ?></td>
                                            <td><?= $precoProcedimento->has('convenio') ? $this->Html->link($precoProcedimento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $precoProcedimento->convenio->id]) : '' ?></td>
                                            <td><?= $precoProcedimento->has('procedimento') ? $this->Html->link($precoProcedimento->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $precoProcedimento->procedimento->id]) : '' ?></td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="text" data-title="codigo"
                                                   data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $precoProcedimento->codigo ?>"
                                                   listen="f"><?= $precoProcedimento->codigo ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="valor_faturar" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->valor_faturar, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->valor_faturar); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="valor_particular" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->valor_particular, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->valor_particular); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="select"
                                                   data-controller="PrecoProcedimentos" data-title="convenio_regracalculo_id"
                                                   data-source="<?=$regra_calculo?>"
                                                   data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $precoProcedimento->convenio_regracalculo_id; ?>"
                                                   listen="f"><?= $precoProcedimento->has('convenio_regracalculo') ? $precoProcedimento->convenio_regracalculo->apelido: '' ?>
                                                </a>
                                            </td>
                                            <td class="<?=$this->Configuracao->showUco()?>">
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="uco" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->uco, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->uco); ?>
                                                </a>
                                            </td>
                                            <td class="<?=$this->Configuracao->showFilme()?>">
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="filme" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->filme, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->filme); ?>
                                                </a>
                                            </td>
                                            <td class="<?=$this->Configuracao->showPorte()?>">
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="porte" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->porte, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->porte); ?>
                                                </a>
                                            </td>
                                            <td class="<?=$this->Configuracao->showFilmeReais()?>">
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="filme_reais" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->filme_reais, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->filme_reais); ?>
                                                </a>
                                            </td>
                                            <td class="<?=$this->Faturamento->mostraTabelaPrecoInstrumentador()?>">
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="PrecoProcedimentos" data-param="decimal"
                                                   data-title="valor_instrumentador" data-pk="<?= $precoProcedimento->id ?>"
                                                   data-value="<?= $this->Number->format($precoProcedimento->valor_instrumentador, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($precoProcedimento->valor_instrumentador); ?>
                                                </a>
                                            </td>
                                            <td><?= ($precoProcedimento->has('user_reg'))? $precoProcedimento->user_reg->nome : '' ?></td>
                                            <td><?= ($precoProcedimento->has('user_alt'))?$precoProcedimento->user_alt->nome : '' ?></td>
                                            <td><?= $precoProcedimento->modified ?></td>
                                            <td class="actions">
                                                <?php //$this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $precoProcedimento->id,'?'=>['first'=>1]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                                <?= $this->Form->button($this->Html->icon('remove'),['onclick'=>'DeletarModal(\'PrecoProcedimentos\','.$precoProcedimento->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

