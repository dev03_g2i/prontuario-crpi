
<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Preço Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Preço Procedimentos</li>
            <li class="active">
                <strong>Listagem de Preço Procedimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('PrecoProcedimentos',['type'=>'get']) ?>
                        <div class="col-md-4">
                            <?= $this->Form->input('procedimentos',['type'=>'select','class'=>'s-procedimento']);?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('convenio',['empty'=>'selecione','options'=>$convenios]);?>
                        </div>
                        <div class="col-md-4" style="margin-top: 25px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Preco Procedimentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Preco Procedimentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Preco Procedimentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                <th><?= $this->Paginator->sort('convenio_id', ['label' => 'Convênio']) ?></th>
                <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                <th><?= $this->Paginator->sort('valor_faturar') ?></th>
                <th><?= $this->Paginator->sort('valor_particular') ?></th>
                <th><?= $this->Paginator->sort('codigo') ?></th>
                <th><?= $this->Paginator->sort('regra') ?></th>
                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($precoProcedimentos as $precoProcedimento): ?>
            <tr>
                <td><?= $this->Number->format($precoProcedimento->id) ?></td>
                <td><?= $precoProcedimento->has('convenio') ? $this->Html->link($precoProcedimento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $precoProcedimento->convenio->id]) : '' ?></td>
                <td><?= $precoProcedimento->has('procedimento') ? $this->Html->link($precoProcedimento->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $precoProcedimento->procedimento->id]) : '' ?></td>
                <td><?= $this->Number->currency($precoProcedimento->valor_faturar) ?></td>
                <td><?= $this->Number->currency($precoProcedimento->valor_particular) ?></td>
                <td><?= $precoProcedimento->codigo ?></td>
                <td><?= ($precoProcedimento->regra ==1)?'Normal':'Quantidade'?></td>
                <td><?= $precoProcedimento->has('situacao_cadastro') ? $this->Html->link($precoProcedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $precoProcedimento->situacao_cadastro->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $precoProcedimento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $precoProcedimento->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $precoProcedimento->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

