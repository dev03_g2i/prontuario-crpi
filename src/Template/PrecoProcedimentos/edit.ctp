<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Preço Procedimentos</h2>
            <ol class="breadcrumb">
                <li>Preço Procedimentos</li>
                <li class="active">
                    <strong> Editar Preço Procedimentos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="precoProcedimentos form">
                            <?= $this->Form->create($precoProcedimento) ?>
                            <fieldset>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('convenio_id', ['options' => $convenios, 'type' => 'hidden']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('procedimento_id', ['options' => $procedimentos, 'type' => 'hidden']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('valor_faturar', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('valor_particular', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="col-md-6 <?=$this->Configuracao->showUco()?>">
                                    <?= $this->Form->input('uco', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="col-md-6 <?=$this->Configuracao->showFilme()?>">
                                    <?= $this->Form->input('filme', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="col-md-6 <?=$this->Configuracao->showPorte()?>">
                                    <?= $this->Form->input('porte', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-3'>
                                    <?= $this->Form->input('codigo', ['type' => 'text', 'maxLenght' => 15]); ?>
                                </div>
                                <div class='col-md-3'>
                                    <?= $this->Form->input('regra', ['options' => [1=>'Normal', 2=>'Quantidade']]); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

