<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Orçamento/Valores de procedimentos</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('PrecoProcedimentos',['type'=>'get', 'id' => 'frm-orcamento-proc']) ?>
                        <div class="col-md-5">
                            <?= $this->Form->input('convenio',['name' => 'convenio[]', 'label' => 'Convênios', 'class' => 'select2', 'empty'=>'selecione','multiple','options' =>$convenios]);?>
                        </div>
                        <div class="col-md-5">
                            <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'type' => 'select', 'class' => 'select2', 'options' => []]); ?>
                        </div>
                        <div class="col-md-2" style="margin-top: 23px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('chevron-down'), [
                                    'onclick' => "showOrcamento()",
                                    'type' => 'button', 'class' => 'btn btn-primary',
                                    'title' => 'Gerar Orçamento', 'escape' => false,
                                ]) ?>
                        </div>

                        <div class="col-md-5" id="nome-orcamento" style="display:none">
                            <?php echo $this->Form->input('Nome:', ['name' => 'nome', 'label' => 'Nome', 'type' => 'text']);?>
                        </div>

                        <div class="col-md-5" id="obs-orcamento" style="display:none">
                            <?php echo $this->Form->input('Observações', ['name' => 'observacoes', 'label' => 'Observações', 'type' => 'textarea']);?>
                        </div>

                        <div class="col-md-2" id="relatorio-orcamento" style="display:none">
                            <?= $this->Form->button($this->Html->icon('print'), [
                                'onclick' => "reportVlprocedimento($(this))",
                                'type' => 'button', 'class' => 'btn btn-success',
                                'title' => 'Emitir Orçamento', 'escape' => false, 'controller' => 'Relatorios',
                                'action' => 'relatorioOrcamentoVlProcedimentos'
                            ]); ?>
                        </div>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('convenio_id', 'Convenio') ?></th>
                                    <th><?= $this->Paginator->sort('procedimento_id', 'Exame') ?></th>
                                    <th><?= $this->Paginator->sort('codigo', 'Código') ?></th>
                                    <th><?= $this->Paginator->sort('valor_particular', 'Vl. Caixa/Particular') ?></th>
                                    <th><?= $this->Paginator->sort('valor_faturar', 'Vl. Fatura') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $options = ['places' => 2];
                                foreach ($precoProcedimentos as $p): ?>
                                    <?php if($fill): ?>
                                        <tr>
                                            <td><?=$p->convenio->nome;?></td>
                                            <td><?=$p->procedimento->nome; ?></td>
                                            <td><?=$p->codigo; ?></td>
                                            <td><?=$this->Number->format($p->valor_particular, $options); ?></td>
                                            <td><?=$this->Number->format($p->valor_faturar, $options); ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td><strong>Total: </strong><?=$this->Number->format($total, $options); ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

