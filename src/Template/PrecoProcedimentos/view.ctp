

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Preco Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Preco Procedimentos</li>
            <li class="active">
                <strong>Litagem de Preco Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="precoProcedimentos">
    <h3><?= h($precoProcedimento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Convenio') ?></th>
            <td><?= $precoProcedimento->has('convenio') ? $this->Html->link($precoProcedimento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $precoProcedimento->convenio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Procedimento') ?></th>
            <td><?= $precoProcedimento->has('procedimento') ? $this->Html->link($precoProcedimento->procedimento->id, ['controller' => 'Procedimentos', 'action' => 'view', $precoProcedimento->procedimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $precoProcedimento->has('user') ? $this->Html->link($precoProcedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $precoProcedimento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $precoProcedimento->has('situacao_cadastro') ? $this->Html->link($precoProcedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $precoProcedimento->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($precoProcedimento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Valor Faturar') ?></th>
            <td><?= $this->Number->format($precoProcedimento->valor_faturar) ?></td>
        </tr>
        <tr>
            <th><?= __('Valor Particular') ?></th>
            <td><?= $this->Number->format($precoProcedimento->valor_particular) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($precoProcedimento->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($precoProcedimento->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

