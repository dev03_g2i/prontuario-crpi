<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Preço Procedimentos</h2>
            <ol class="breadcrumb">
                <li>Preço Procedimentos</li>
                <li class="active">
                    <strong>Cadastrar Preço Procedimentos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="precoProcedimentos form">
                            <?= $this->Form->create($precoProcedimento, ['id' => 'frm-preco-procedimento']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Preco Procedimento') ?></legend>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('convenio_id', ['options' => $convenios, 'default' => ($convenio_id)?$convenio_id:'']); ?>
                                </div>
                                <?php
                                if (!empty($Procedimentos)):
                                    echo $this->Form->input('procedimento_id', ['value' => $Procedimentos->id, 'type' => 'hidden']);
                                else: ?>
                                    <div class='col-md-6'>
                                        <?= $this->Form->input('procedimento_id', ['class' => 'select2', 'options' => $procedimentos]); ?>
                                    </div>
                                <?php endif; ?>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('codigo', ['type' => 'text', 'maxLenght' => 15]); ?>
                                </div>
                                <div class='col-md-3'>
                                    <?= $this->Form->input('valor_faturar', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-3'>
                                    <?= $this->Form->input('valor_particular', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <div class="hr-line-dashed"></div>
                                </div>
                                <div class="col-md-6 <?=$this->Configuracao->showUco()?>">
                                    <?= $this->Form->input('uco', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="col-md-6 <?=$this->Configuracao->showFilme()?>">
                                    <?= $this->Form->input('filme', ['label' => 'Filme M2', 'type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="col-md-6 <?=$this->Configuracao->showPorte()?>">
                                    <?= $this->Form->input('porte', ['type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('regra', ['type' => 'select', 'options' => $regra_calculo, 'empty' => 'Selecione']); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'validaPrecoProcedimento("frm-preco-procedimento")']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

