<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Status Tipo Espera</h2>
            <ol class="breadcrumb">
                <li>Status Tipo Espera</li>
                <li class="active">
                    <strong>Litagem de Status Tipo Espera</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Status Tipo Espera</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Descricao') ?>
                                    </th>
                                    <td>
                                        <?= h($statusTipoEspera->descricao) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $statusTipoEspera->has('user') ? $this->Html->link($statusTipoEspera->user->nome, ['controller' => 'Users', 'action' => 'view', $statusTipoEspera->user->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situacao') ?>
                                    </th>
                                    <td>
                                        <?= $statusTipoEspera->has('situacao_cadastro') ? $this->Html->link($statusTipoEspera->situacao_cadastro->nome, ['controller' => 'Users', 'action' => 'view', $statusTipoEspera->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($statusTipoEspera->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($statusTipoEspera->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>