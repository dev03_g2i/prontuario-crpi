<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Status Tipo Espera</h2>
            <ol class="breadcrumb">
                <li>Status Tipo Espera</li>
                <li class="active">
                    <strong>
                                                Cadastrar Status Tipo Espera
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="statusTipoEspera form">
                            <?= $this->Form->create($statusTipoEspera) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Status Tipo Espera') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('situacao_id'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

