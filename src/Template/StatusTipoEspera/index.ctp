<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Status Tipo Espera</h2>
            <ol class="breadcrumb">
                <li>Status Tipo Espera</li>
                <li class="active">
                    <strong>Listagem de Status Tipo Espera</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('StatusTipoEspera',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('descricao',['name'=>'StatusTipoEspera__descricao']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('situacao_id',['name'=>'StatusTipoEspera__situacao_id']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Status Tipo Espera', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Status Tipo Espera','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Status Tipo Espera') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('descricao') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('situacao_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($statusTipoEspera as $statusTipoEspera): ?>
                                        <tr>
                                            <td>
                                                <?= h($statusTipoEspera->descricao) ?>
                                            </td>
                                            <td>
                                            <?= $statusTipoEspera->has('situacao_cadastro') ? $this->Html->link($statusTipoEspera->situacao_cadastro->nome, ['controller' => 'Users', 'action' => 'view', $statusTipoEspera->situacao_cadastro->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $statusTipoEspera->has('user') ? $this->Html->link($statusTipoEspera->user->nome, ['controller' => 'Users', 'action' => 'view', $statusTipoEspera->user->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= h($statusTipoEspera->created) ?>
                                            </td>
                                            <td>
                                                <?= h($statusTipoEspera->modified) ?>
                                            </td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'statusTipoEspera','action' => 'view', $statusTipoEspera->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'statusTipoEspera','action' => 'edit', $statusTipoEspera->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'statusTipoEspera','action'=>'delete', $statusTipoEspera->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>