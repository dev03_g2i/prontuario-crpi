<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
            <ol class="breadcrumb">
                <li>Contas a receber</li>
                <li class="active">
                    <strong>Litagem de Contas a receber</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Tratamentos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID do trat</th>
                                        <th>Valor total</th>
                                        <th>Valor pago</th>
                                        <th>Valor a pagar</th>
                                        <th>Qtde de parcelas</th>
                                        <th class="actions"><?= __('Detalhes') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($atendimentos as $atendimento):
                                        $valor_total=0;
                                        $valor_pago=0;
                                        $valor_pagar=0;
                                        $qtde_parcelas=0;
                                        if(!empty($atendimento->contas_receber)){
                                            foreach ($atendimento->contas_receber as $contas) {
                                                $valor_total+=$contas->valor;
                                                $valor_pago+=!empty($contas->dataPagamento) ? $contas->valor : 0;
                                                $valor_pagar+=empty($contas->dataPagamento) ? $contas->valor : 0;
                                                $qtde_parcelas++;
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td><?= $atendimento->id ?></td>
                                            <td><?= $this->Number->currency($valor_total) ?></td>
                                            <td><?= $this->Number->currency($valor_pago) ?></td>
                                            <td><?= $this->Number->currency($valor_pagar) ?></td>
                                            <td><?= $qtde_parcelas ?></td>

                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-tasks"></i>', ['controller'=>'Atendimentos','action' => 'view', $atendimento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info','target'=>'_blank','listen'=>'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

