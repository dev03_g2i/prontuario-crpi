<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row area contasreceber">
        <div class="col-lg-12">
            <div class="col-md-9 text-left">
                <div class="col-md-12" style="margin-top: 10px">
                    <?php
                    $cliente = '';
                    $options = ['places' => 2, 'before' => 'R$ '];
                    foreach ($contasreceber as $cr){
                        $cliente = $cr->cliente_clone->nome ? $cr->cliente_clone->nome : '';
                    }
                    ?>
                    <h4>Paciente: <?=$cliente?> </h4>
                    <h4>Atendimento n: <?=$atendimento->id?> </h4>
                    <h4>Tl.Líquido: <?=$this->Number->format($atendimento->total_liquido, $options)?> </h4>
                    <h4>Tl.Recebido: <?=$this->Number->format($atendimento->total_pagoato, $options)?> </h4>
                    <h4 class="text-danger">Tl. a receber: <?=$this->Number->format($atendimento->total_areceber, $options)?> </h4>
                    <?php echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $atendimento->id]); ?>
                </div>
            </div>
            <div class="col-md-3 text-right">
                <p style="margin-top: 5px">
                    <?= $this->Form->button(__('Fechar'), ['type' => 'button', 'class' => 'btn btn-primary', 'data-dismiss' => 'modal']) ?>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Receber', ['action' => 'new-add', '?' => ['id' => $extern_id]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Contasreceber', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Contasreceber') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('data', ['label' => 'Data Doc.']) ?></th>
                                    <th><?= $this->Paginator->sort('vencimento') ?></th>
                                    <th><?= $this->Paginator->sort('idPlanoContas', ['label' => 'Plano de Contas']) ?></th>
                                    <th><?= $this->Paginator->sort('tipoPagamento', ['label' => 'Tp. Pagamento']) ?></th>
                                    <th><?= $this->Paginator->sort('valor', ['label' => 'Valor Líquido']) ?></th>
                                    <th><?= $this->Paginator->sort('dataPagamento', ['label' => 'Dt. Pagamento']) ?></th>
                                    <th><?= $this->Paginator->sort('parcela') ?></th>
                                    <th><?= $this->Paginator->sort('user_created', 'Usuário') ?></th>
                                    <th class="<?= $this->Configuracao->showControleFinanceiroOnContasReceber() ?>"><?= __('Controle') ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($contasreceber as $contasreceber): ?>
                                    <tr>
                                        <td>
                                            <!--<a class="edit_local" href="#" data-type="text"
                                               data-controller="Contasreceber" data-param="data" data-title="data"
                                               data-pk="<?= $contasreceber->id ?>"
                                               data-value="<?= $this->Time->format($contasreceber->data, 'dd/MM/YYYY'); ?>"
                                               listen="f"><?= $this->Time->format($contasreceber->data, 'dd/MM/YYYY'); ?></a>-->
                                            <?= $this->Time->format($contasreceber->data, 'dd/MM/YYYY'); ?>
                                        </td>
                                        <td>
                                            <!--<a class="edit_local" href="#" data-type="text"
                                               data-controller="Contasreceber" data-param="data"
                                               data-title="vencimento" data-pk="<?= $contasreceber->id ?>"
                                               data-value="<?= $this->Time->format($contasreceber->vencimento, 'dd/MM/YYYY'); ?>"
                                               listen="f"><?= $this->Time->format($contasreceber->vencimento, 'dd/MM/YYYY'); ?></a>-->
                                            <?= $this->Time->format($contasreceber->vencimento, 'dd/MM/YYYY'); ?>
                                        </td>
                                        <td>
                                            <!--<a class="edit_local" href="#" data-type="select2"
                                               data-controller="Contasreceber" data-param="select"
                                               data-find="Planocontas" data-title="idPlanoContas"
                                               data-pk="<?= $contasreceber->id ?>"
                                               data-value="<?= $contasreceber->idPlanoContas; ?>"
                                               listen="f"><?= $contasreceber->planoconta->nome; ?></a>-->
                                            <?= $contasreceber->planoconta->nome; ?>
                                        </td>
                                        <td>
                                            <!--<a class="edit_local" href="#" data-type="select2"
                                               data-controller="Contasreceber" data-param="select"
                                               data-find="TipoPagamento" data-title="tipoPagamento"
                                               data-pk="<?= $contasreceber->id ?>"
                                               data-value="<?= $contasreceber->tipoPagamento; ?>"
                                               listen="f"><?= $contasreceber->tipo_pagamento->descricao; ?></a>-->
                                            <?= $contasreceber->tipo_pagamento->descricao; ?>
                                        </td>
                                        <td>
                                            <!--<a class="edit_local" href="#" data-type="text"
                                               data-controller="Contasreceber" data-param="decimal"
                                               data-title="valor" data-pk="<?= $contasreceber->id ?>"
                                               data-value="<?= $this->Number->format($contasreceber->valor, ['places' => 2, 'decimals' => '.', 'thousands' => '']) ?>"
                                               listen="f"><?= $this->Number->currency($contasreceber->valor); ?></a>-->
                                            <?= $this->Number->format($contasreceber->valor, ['places' => 2, 'decimals' => '.', 'thousands' => '']) ?>
                                        </td>
                                        <?php if (!empty($contasreceber->dataPagamento)): ?>
                                            <td>
                                                <!--<a class="edit_local" href="#" data-type="text"
                                                   data-controller="Contasreceber" data-param="data"
                                                   data-title="dataPagamento" data-pk="<?= $contasreceber->id ?>"
                                                   data-value="<?= $this->Time->format($contasreceber->dataPagamento, 'dd/MM/YYYY'); ?>"
                                                   listen="f"><?= $this->Time->format($contasreceber->dataPagamento, 'dd/MM/YYYY'); ?></a>-->
                                                <?= $this->Time->format($contasreceber->dataPagamento, 'dd/MM/YYYY'); ?>
                                            </td>
                                        <?php else: ?>
                                            <td>
                                                <!--<a class="edit_local" href="#" data-type="text"
                                                   data-controller="Contasreceber" data-param="data"
                                                   data-title="dataPagamento" data-pk="<?= $contasreceber->id ?>"
                                                   data-value="<?= date('d/m/Y'); ?>" listen="f">Não informada</a>-->
                                                Não informada
                                            </td>
                                        <?php endif; ?>
                                        <td>
                                            <!--<a class="edit_local" href="#" data-type="text"
                                               data-controller="Contasreceber" data-param="text"
                                               data-title="parcela" data-pk="<?= $contasreceber->id ?>"
                                               data-value="<?= h(empty($contasreceber->parcela) ? '1/1' : $contasreceber->parcela); ?>"
                                               listen="f"><?= h(empty($contasreceber->parcela) ? '1/1' : $contasreceber->parcela); ?></a>-->
                                            <?=$contasreceber->parcela ?>
                                        </td>
                                        <td>
                                            <?php echo @$contasreceber->user_reg->nome; ?>
                                        </td>
                                        <td class="<?= $this->Configuracao->showControleFinanceiroOnContasReceber() ?>">
                                            <?= $contasreceber->controle_financeiro->descricao ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-tasks"></i>', ['action' => 'view', $contasreceber->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                            <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $contasreceber->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



