<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contasreceber</h2>
            <ol class="breadcrumb">
                <li>Contasreceber</li>
                <li class="active">
                    <strong>Detalhes de Contas à receber</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-3 text-right">
            <h2>
                <?= $this->Form->button('<i class="fa fa-arrow-left"></i>', ['onclick' => 'Navegar(\'\',\'back\')', 'listen', 'escape' => false,'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Voltar',]) ?>
            </h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Detalhes</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th><?= __('Data') ?></th>
                                            <td><?= h($contasreceber->data) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Vencimento') ?></th>
                                            <td><?= h($contasreceber->vencimento) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Plano de contas') ?></th>
                                            <td><?= $contasreceber->has('planoconta') ? $this->Html->link($contasreceber->planoconta->nome, ['controller' => 'Planocontas', 'action' => 'view', $contasreceber->planoconta->id]) : '' ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Cliente') ?></th>
                                            <td><?= $contasreceber->has('cliente_clone') ? $this->Html->link($contasreceber->cliente_clone->nome, ['controller' => 'ClienteClone', 'action' => 'view', $contasreceber->cliente_clone->id]) : '' ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Empresa') ?></th>
                                            <td><?= $contasreceber->has('contabilidade') ? $this->Html->link($contasreceber->contabilidade->nome, ['controller' => 'Contabilidade', 'action' => 'view', $contasreceber->contabilidade->id]) : '' ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Parcela') ?></th>
                                            <td><?= h($contasreceber->parcela) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Doc Fiscal') ?></th>
                                            <td><?= h($contasreceber->numfiscal) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Tipo Pagamento') ?></th>
                                            <td><?= $contasreceber->has('tipo_pagamento') ? $this->Html->link($contasreceber->tipo_pagamento->descricao, ['controller' => 'TipoPagamento', 'action' => 'view', $contasreceber->tipo_pagamento->id]) : '' ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th><?= __('Tipo Documento') ?></th>
                                            <td><?= $contasreceber->has('tipo_documento') ? $this->Html->link($contasreceber->tipo_documento->descricao, ['controller' => 'TipoDocumento', 'action' => 'view', $contasreceber->tipo_documento->id]) : '' ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Núm documento') ?></th>
                                            <td><?= h($contasreceber->numerodocumento) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Valor') ?></th>
                                            <td><?= $this->Number->currency($contasreceber->valor) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Juros') ?></th>
                                            <td><?= $this->Number->currency($contasreceber->juros) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Multa') ?></th>
                                            <td><?= $this->Number->currency($contasreceber->multa) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Valor Bruto') ?></th>
                                            <td><?= $this->Number->currency($contasreceber->valorBruto) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Desconto') ?></th>
                                            <td><?= $this->Number->currency($contasreceber->desconto) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Data Pagamento') ?></th>
                                            <td><?= h($contasreceber->data_pagamento) ?></td>
                                        </tr>
                                        <tr>
                                            <th><?= __('Data fiscal') ?></th>
                                            <td><?= h($contasreceber->datafiscal) ?></td>

                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h3>Complemento</h3>
                                    </div>
                                    <div class="ibox-content">
                                        <?= $this->Text->autoParagraph(h($contasreceber->complemento)); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

