<div class="this-place">
    <div class=" wrapper  white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
            <ol class="breadcrumb">
                <li>Contas a receber</li>
                <li class="active">
                    <strong> Editar Contas a receber
                    </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contasreceber form">
                            <?= $this->Form->create($contasreceber,['id'=>'form-contas']) ?>
                            <fieldset>
                                <legend><?= __('Editar Contas a receber') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'label' => 'Data do documento', 'value' => $this->Time->format($contasreceber->data, 'dd-MM-Y')]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contasreceber->vencimento, 'dd-MM-Y')]);
                                echo "</div>";

                                if (empty($atendimentos->cliente_id)) {
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('idCliente', ['options' => $clienteClone]);
                                    echo "</div>";
                                } else {
                                    echo $this->Form->input('idCliente', ['type' => 'hidden', 'value' => $atendimentos->cliente_id]);
                                }

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('idPlanoContas', ['options' => $planocontas, 'label' => 'Plano de Contas']);
                                echo "</div>";


                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoDocumento', ['options' => $tipoDocumento, 'label' => 'Tipo de Documento']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoPagamento', ['options' => $tipoPagamento, 'label' => 'Forma de Pagamento']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numfiscal', ['label' => 'Número de documento fiscal']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numerodocumento', ['label' => 'Número do documento']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                if (!empty($contasreceber->datafiscal)) {
                                    echo $this->Form->input('datafiscal', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'label' => 'Data do Documento Fiscal', 'value' => $this->Time->format($contasreceber->datafiscal, 'dd-MM-Y')]);
                                } else {
                                    echo $this->Form->input('datafiscal', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'label' => 'Data do Documento Fiscal']);
                                }
                                echo "</div>";


                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valorBruto', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'label' => 'Valor Bruto']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('juros', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('multa', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('desconto', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'label' => 'Valor Líquido', 'readonly']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('correcaoMonetaria', ['options' => $correcao, 'label' => 'Atualização Monetária ', 'empty' => 'selecione']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('contabilidade_id', ['options' => $contabilidade, 'label' => 'Empresa', 'empty' => 'selecione']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                if (!empty($contasreceber->dataPagamento)) {
                                    echo $this->Form->input('dataPagamento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'label' => 'Data de Pagamento', 'value' => $this->Time->format($contasreceber->dataPagamento, 'dd-MM-Y')]);
                                } else {
                                    echo $this->Form->input('dataPagamento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'label' => 'Data de Pagamento']);
                                }
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('status_id', ['options' => $status, 'label' => 'Status', 'empty' => 'selecione']);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('complemento');
                                echo "</div>";

                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary', 'onclick' => 'EnviarForm("form-contas")']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

