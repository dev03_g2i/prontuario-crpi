<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contasreceber form">
                            <?= $this->Form->create($contasreceber, ['id' => 'frm-parcelas']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Contas a receber') ?></legend>
                                <?php $options = ['places' => 2, 'decimals' => '.', 'thousands' => ''];
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoPagamento', ['required' => true, 'label' => 'Forma de Pagamento', 'options' => $tipoPagamento]);
                                echo $this->Form->input('tipoDocumento', ['type' => 'hidden', 'label' => 'Tipo de Documento', 'value'=>1]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('parcelas', ['type' => 'number', 'min' => 1, 'value' => 1]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento', ['label' => 'Primeiro Vencimento', 'empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => date('d/m/Y')]);
                                echo "</div>";

                                echo "<div class='col-md-6 ".$this->Configuracao->showPlanoContas()."'>";
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numfiscal', ['label' => 'Número de documento fiscal','type'=>'hidden','value'=>1]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numerodocumento', ['label' => 'Número do documento','type'=>'hidden','value'=>1]);
                                echo "</div>";

                                echo $this->Form->input('idCliente', ['type' => 'hidden', 'value' => $atendimento->cliente_id]);

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valorBruto', ['label' => 'Vl.Atendimento', 'readonly' => true, 'id' => 'valBruto', 'prepend' => 'R$', 'type' => 'text', 'value' => $this->Number->format($atendimento->total_liquido, $options)]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'label' => 'Vl.Recebido', 'value' => $this->Number->format($atendimento->total_areceber, $options) ]);
                                echo "</div>";

                                echo $this->Form->input('juros', ['type' => 'hidden']);
                                echo $this->Form->input('multa', ['type' => 'hidden']);
                                echo $this->Form->input('desconto', ['type' => 'hidden']);

                                echo "<div class='clearfix'></div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['readonly' => $this->Configuracao->editCaixaDtEntrega(), 'type' => 'text', 'class' => 'datepicker', 'value' => date('d/m/Y'), 'label' => 'Dt.Entrada']);
                                echo "</div>";

                                echo "<div class='col-md-12 hidden'>";
                                echo $this->Form->input('complemento');
                                echo "</div>";

                                echo $this->Form->input('contabilidade_id', ['type' => 'hidden', 'value' => 1]);
                                echo $this->Form->input('correcaoMonetaria', ['type' => 'hidden']);
                                echo $this->Form->input('extern_id', ['type' => 'hidden', 'value' => $atendimento->id]);

                                ?>

                            </fieldset>
                            <div class="text-right">
                                <?= $this->Form->button(__('Cancelar'), ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'Navegar(\'\',\'back\')', 'listen']) ?>
                                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'parcelar()']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

