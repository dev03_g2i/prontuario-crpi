<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
            <ol class="breadcrumb">
                <li>Contas a receber</li>
                <li class="active">
                    <strong>Litagem de Contas a receber</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Contasreceber') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('extern_id', ['label' => 'Tratamento ID']) ?></th>
                                        <th><?= $this->Paginator->sort('data', ['label' => 'Data Doc.']) ?></th>
                                        <th><?= $this->Paginator->sort('vencimento') ?></th>
                                        <th><?= $this->Paginator->sort('idPlanoContas', ['label' => 'Plano de Contas']) ?></th>
                                        <th><?= $this->Paginator->sort('tipoPagamento', ['label' => 'Tp. Pagamento']) ?></th>
                                        <th><?= $this->Paginator->sort('valor', ['label' => 'Valor Líquido']) ?></th>
                                        <th><?= $this->Paginator->sort('dataPagamento', ['label' => 'Dt. Pagamento']) ?></th>
                                        <th><?= $this->Paginator->sort('parcela') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                    foreach ($contasreceber as $contasreceber): ?>
                                        <tr>
                                            <td><?= !empty($contasreceber->extern_id) ? $this->Html->link($contasreceber->extern_id, ['controller' => 'Atendimentos', 'action' => 'view', $contasreceber->extern_id],['target'=>'_blank']) : '' ?></td>
                                            <td><a class="edit_local" href="#" data-type="text" data-controller="Contasreceber" data-param="data" data-title="data" data-pk="<?= $contasreceber->id ?>" data-value="<?=$this->Time->format($contasreceber->data,'dd/MM/YYYY'); ?>" listen="f"><?= $this->Time->format($contasreceber->data,'dd/MM/YYYY'); ?></a></td>
                                            <td><a class="edit_local" href="#" data-type="text" data-controller="Contasreceber" data-param="data" data-title="vencimento" data-pk="<?= $contasreceber->id ?>" data-value="<?=$this->Time->format($contasreceber->vencimento,'dd/MM/YYYY'); ?>" listen="f"><?= $this->Time->format($contasreceber->vencimento,'dd/MM/YYYY'); ?></a></td>
                                            <td><a class="edit_local" href="#" data-type="select2" data-controller="Contasreceber" data-param="select" data-find="Planocontas" data-title="idPlanoContas" data-pk="<?= $contasreceber->id ?>" data-value="<?=$contasreceber->idPlanoContas; ?>" listen="f"><?= $contasreceber->planoconta->nome; ?></a></td>
                                            <td><a class="edit_local" href="#" data-type="select2" data-controller="Contasreceber" data-param="select" data-find="TipoPagamento" data-title="tipoPagamento" data-pk="<?= $contasreceber->id ?>" data-value="<?=$contasreceber->tipoPagamento; ?>" listen="f"><?= $contasreceber->tipo_pagamento->descricao; ?></a></td>
                                            <td><a class="edit_local" href="#" data-type="text" data-controller="Contasreceber" data-param="decimal" data-title="valor" data-pk="<?= $contasreceber->id ?>" data-value="<?=$this->Number->format($contasreceber->valor,['places' => 2,'decimals' => '.','thousands' => '']) ?>" listen="f"><?= $this->Number->currency($contasreceber->valor); ?></a></td>
                                            <?php if(!empty($contasreceber->dataPagamento)): ?>
                                                <td><a class="edit_local" href="#" data-type="text" data-controller="Contasreceber" data-param="data" data-title="dataPagamento" data-pk="<?= $contasreceber->id ?>" data-value="<?=$this->Time->format($contasreceber->dataPagamento,'dd/MM/YYYY'); ?>" listen="f"><?= $this->Time->format($contasreceber->dataPagamento,'dd/MM/YYYY'); ?></a></td>
                                            <?php else: ?>
                                                <td><a class="edit_local" href="#" data-type="text" data-controller="Contasreceber" data-param="data" data-title="dataPagamento" data-pk="<?= $contasreceber->id ?>" data-value="<?= date('d/m/Y'); ?>" listen="f">Não informada</a></td>
                                            <?php endif; ?>
                                            <td><a class="edit_local" href="#" data-type="text" data-controller="Contasreceber" data-param="text" data-title="parcela" data-pk="<?= $contasreceber->id ?>" data-value="<?= h(empty($contasreceber->parcela)?'1/1':$contasreceber->parcela);  ?>" listen="f"><?= h(empty($contasreceber->parcela)?'1/1':$contasreceber->parcela);  ?></a></td>
                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-tasks"></i>', ['action' => 'view', $contasreceber->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'nedit', $contasreceber->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

