<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contasreceber form">
                            <?= $this->Form->create($contasreceber, ['id' => 'frm-parcelas']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Contas a receber') ?></legend>
                                <?php $options = ['places' => 2, 'decimals' => '.', 'thousands' => ''];
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoPagamento', ['id' => 'tipoPagamentoInput', 'required' => true, 'label' => 'Forma de Pagamento', 'options' => $tipoPagamento]);
                                echo $this->Form->input('tipoDocumento', ['type' => 'hidden', 'label' => 'Tipo de Documento', 'value' => 1]);
                                echo $this->Form->input('controlePlanoContas', ['id' => 'controle_plano','type' => 'hidden', 'value' => $this->Configuracao->getMostraPlanoContas()]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('parcelas', ['type' => 'number', 'required' => true, 'min' => 1, 'value' => 1]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento', ['label' => 'Primeiro Vencimento', 'empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => date('d/m/Y')]);
                                echo "</div>";

                                if ($this->Configuracao->getMostraPlanoContas() == 1) {
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('idPlanoContas', ['options' => $planocontas, 'label' => 'Plano de Contas']);
                                    echo "</div>";
                                } else {
                                    echo $this->Form->input('idPlanoContas', ['id' => 'id_plano_contas_input', 'value' => $this->Configuracao->IdPlanoContas(), 'type' => 'hidden']);
                                }
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numfiscal', ['label' => 'Número de documento fiscal']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numerodocumento', ['label' => 'Número do documento']);
                                echo "</div>";

                                echo $this->Form->input('idCliente', ['type' => 'hidden', 'value' => $atendimento->cliente_id]);

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valorBruto', ['label' => 'Vl.Atendimento', 'readonly' => true, 'id' => 'valBruto', 'prepend' => 'R$', 'type' => 'text', 'value' => $this->Number->format($atendimento->total_liquido, $options)]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'label' => 'Vl.Recebido', 'value' => number_format($atendimento->total_areceber, 2, '.', '')]);
                                echo "</div>";

                                if ($this->Configuracao->mostraCampoControleFinanceiroOnContasReceber()): ?>
                                    <div class='col-md-6'>
                                        <?= $this->Form->input('controle_financeiro_id', ['type' => 'select', 'options' => $controles]); ?>
                                    </div>
                                <?php else:
                                    echo $this->Form->input('controle_financeiro_id', ['type' => 'hidden', 'value' => $controles]);
                                endif;

                                echo $this->Form->input('juros', ['type' => 'hidden']);
                                echo $this->Form->input('multa', ['type' => 'hidden']);
                                echo $this->Form->input('desconto', ['type' => 'hidden']);

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['readonly' => $this->Configuracao->editCaixaDtEntrega(), 'type' => 'text', 'class' => 'datepicker', 'value' => date('d/m/Y'), 'label' => 'Dt.Entrada']);
                                echo "</div>";
                                echo "<div class='clearfix'></div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('complemento');
                                echo "</div>";

                                echo $this->Form->input('contabilidade_id', ['type' => 'hidden', 'value' => 1]);
                                echo $this->Form->input('correcaoMonetaria', ['type' => 'hidden']);
                                echo $this->Form->input('extern_id', ['type' => 'hidden', 'value' => $atendimento->id]);

                                ?>

                            </fieldset>
                            <div class="text-right">
                                <?= $this->Form->button(__('Cancelar'), ['type' => 'button', 'class' => 'btn btn-primary', 'onclick' => 'Navegar(\'\',\'back\')', 'listen']) ?>
                                <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'parcelar()']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

