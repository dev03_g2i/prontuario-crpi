<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contasreceber form">
                            <?= $this->Form->create($contasreceber, ['id' => 'frm-parcelas']) ?>
                            <fieldset>
                                <legend><?= __('Editar Contas a receber') ?></legend>
                                <?php
                                $options = ['places' => 2, 'decimals' => '.', 'thousands' => ''];
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoPagamento', ['id' => 'tipoPagamentoInput', 'required' => true, 'options' => $tipoPagamento, 'label' => 'Forma de Pagamento']);
                                echo $this->Form->input('tipoDocumento', ['type' => 'hidden', 'label' => 'Tipo de Documento']);
                                echo $this->Form->input('controlePlanoContas', ['id' => 'controle_plano', 'type' => 'hidden', 'value' => $this->Configuracao->getMostraPlanoContas()]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('parcelas', ['type' => 'number', 'required' => true, 'min' => 1, 'value' => 1, 'readonly' => true]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento', ['label' => 'Primeiro Vencimento', 'empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contasreceber->vencimento, 'dd-MM-Y')]);
                                echo "</div>";

                                if ($this->Configuracao->getMostraPlanoContas() == 1) {
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('idPlanoContas', ['options' => $planocontas, 'label' => 'Plano de Contas']);
                                    echo "</div>";
                                } else {
                                    echo $this->Form->input('idPlanoContas', ['id' => 'id_plano_contas_input', 'type' => 'text']);
                                }

//                                echo "<div class='col-md-6 " . $this->Configuracao->showPlanoContas() . "'>";
//                                echo $this->Form->input('idPlanoContas', ['options' => $planocontas, 'label' => 'Plano de Contas']);
//                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numfiscal', ['label' => 'Número de documento fiscal']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numerodocumento', ['label' => 'Número do documento']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valorBruto', ['label' => 'Vl.Atendimento', 'readonly' => true, 'id' => 'valBruto', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'value' => $this->Number->format($atendimentos->total_liquido, $options)]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'label' => 'Vl.Recebido', 'value' => number_format($contasreceber->valor, 2, '.', '')]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['readonly' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contasreceber->data, 'dd-MM-Y'), 'label' => 'Dt.Entrada']);
                                echo "</div>";
                                if ($this->Configuracao->mostraCampoControleFinanceiroOnContasReceber()): ?>
                                    <div class='col-md-6'>
                                        <?= $this->Form->input('controle_financeiro_id', ['type' => 'select', 'options' => $controles, 'value' => $contasreceber->controle_financeiro_id]); ?>
                                    </div>
                                <?php else:
                                    echo $this->Form->input('controle_financeiro_id', ['type' => 'hidden', 'value' => $contasreceber->controle_financeiro_id]);
                                endif;
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('complemento', ['rows' => 2]);
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo "Cadastrado por: <strong>" . $contasreceber->user_reg->nome . "</strong> Em <strong>" . $contasreceber->created . "</strong> <br>";
                                echo ($contasreceber->has('user_alt')) ? "Alterado por: <strong>" . $contasreceber->user_alt->nome . "</strong> Em <strong>" . $contasreceber->modified . "</strong>" : ' ';
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->button(__('Voltar'), ['onclick' => 'Navegar(\'\',\'back\')', 'class' => 'btn btn-info', 'type' => 'button']) ?>
                                <?= $this->Form->button(__('Salvar'), ['type' => 'submit', 'class' => 'btn btn-primary', 'onclick' => 'SalvarContas()']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

