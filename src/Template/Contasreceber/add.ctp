<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas a receber</h2>
            <ol class="breadcrumb">
                <li>Contas a receber</li>
                <li class="active">
                    <strong> Cadastrar Contas a receber
                    </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contasreceber form">
                            <?= $this->Form->create($contasreceber) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Contas a receber') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => date('d/m/Y'),'label'=>'Data do documento']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('vencimento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('idPlanoContas', ['options' => $planocontas,'label'=>'Plano de Contas']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numerodocumento', ['label'=>'Número do documento']);
                                echo "</div>";

                                if(empty($atendimento->cliente_id)) {
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('idCliente', ['options' => $clienteClone]);
                                    echo "</div>";
                                }else{
                                    echo $this->Form->input('idCliente', ['type' => 'hidden','value'=>$atendimento->cliente_id]);
                                }

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoDocumento', ['options' => $tipoDocumento,'label'=>'Tipo de Documento']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoPagamento', ['options' => $tipoPagamento,'label'=>'Forma de Pagamento']);
                                echo "</div>";



                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valorBruto', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money','label'=>'Valor Bruto']);
                                echo "</div>";

                                echo $this->Form->input('juros', ['type' => 'hidden']);
                                echo $this->Form->input('multa', ['type' => 'hidden']);
                                echo $this->Form->input('desconto', ['type' => 'hidden']);

                                
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money','label'=>'Valor Líquido','readonly']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('correcaoMonetaria', ['options' => $correcao,'label'=>'Atualização Monetária ','empty'=>'selecione']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('contabilidade_id', ['options' => $contabilidade,'label'=>'Empresa','empty'=>'selecione']);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('complemento');
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

