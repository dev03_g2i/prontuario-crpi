
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Log Retornos</h2>
        <ol class="breadcrumb">
            <li>Log Retornos</li>
            <li class="active">
                <strong>                    Editar Log Retornos
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="logRetornos form">
                        <?= $this->Form->create($logRetorno) ?>
                        <fieldset>
                                                        <legend><?= __('Editar Log Retorno') ?></legend>
                                                        <?php
                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('retorno_id', ['options' => $retornos]);
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('cliente_id', ['options' => $clientes,'label'=>'Paciente']);
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('grupo_id', ['options' => $grupoRetornos]);
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                                                                            echo $this->Form->input('previsao', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($logRetorno->previsao,'dd-MM-Y')]);
                                               echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('dias');
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('situacao_retorno_id', ['options' => $situacaoRetornos]);
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('observacao');
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('justificativa');
                                            echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                                                                            echo $this->Form->input('data_confirmacao', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($logRetorno->data_confirmacao,'dd-MM-Y')]);
                                               echo "</div>";
                                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('quem_aprovou');
                                            echo "</div>";
                                                                        ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

