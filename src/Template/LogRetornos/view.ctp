

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Log Retornos</h2>
        <ol class="breadcrumb">
            <li>Log Retornos</li>
            <li class="active">
                <strong>Litagem de Log Retornos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Log Retornos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Retorno') ?></th>
                                                                <td><?= $logRetorno->has('retorno') ? $this->Html->link($logRetorno->retorno->id, ['controller' => 'Retornos', 'action' => 'view', $logRetorno->retorno->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Cliente') ?></th>
                                                                <td><?= $logRetorno->has('cliente') ? $this->Html->link($logRetorno->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $logRetorno->cliente->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Grupo Retorno') ?></th>
                                                                <td><?= $logRetorno->has('grupo_retorno') ? $this->Html->link($logRetorno->grupo_retorno->nome, ['controller' => 'GrupoRetornos', 'action' => 'view', $logRetorno->grupo_retorno->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situacao Retorno') ?></th>
                                                                <td><?= $logRetorno->has('situacao_retorno') ? $this->Html->link($logRetorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $logRetorno->situacao_retorno->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $logRetorno->has('user') ? $this->Html->link($logRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $logRetorno->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $logRetorno->has('situacao_cadastro') ? $this->Html->link($logRetorno->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $logRetorno->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($logRetorno->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dias') ?></th>
                                <td><?= $this->Number->format($logRetorno->dias) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quem Aprovou') ?></th>
                                <td><?= $this->Number->format($logRetorno->quem_aprovou) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Previsao') ?></th>
                                                                <td><?= h($logRetorno->previsao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($logRetorno->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($logRetorno->modified) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Confirmacao') ?></th>
                                                                <td><?= h($logRetorno->data_confirmacao) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($logRetorno->observacao)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Justificativa') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($logRetorno->justificativa)); ?>
                </div>
            </div>
</div>
</div>
</div>


