<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Log Retornos</h2>
        <ol class="breadcrumb">
            <li>Log Retornos</li>
            <li class="active">
                <strong>Litagem de Log Retornos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Log Retornos', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Log Retornos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Log Retornos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Cadatro']) ?></th>
                                    <th><?= $this->Paginator->sort('observacao',['label'=>'Obs.']) ?></th>
                                    <th><?= $this->Paginator->sort('grupo_id') ?></th>
                                    <th><?= $this->Paginator->sort('previsao',['label'=>'Data']) ?></th>
                                    <th><?= $this->Paginator->sort('dias') ?></th>
                                    <th><?= $this->Paginator->sort('situacao_retorno_id',['label'=>'Sit. Retorno']) ?></th>
                                    <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                                    <th><?= $this->Paginator->sort('data_confirmacao') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($logRetornos as $logRetorno): ?>
                                    <tr>
                                        <td><?= h($logRetorno->created) ?></td>
                                        <td><?= $logRetorno->cliente->observacao ?></td>
                                        <td><?= $logRetorno->has('grupo_retorno') ? $this->Html->link($logRetorno->grupo_retorno->nome, ['controller' => 'GrupoRetornos', 'action' => 'view', $logRetorno->grupo_retorno->id]) : '' ?></td>
                                        <td><?= h($logRetorno->previsao) ?></td>
                                        <td><?= $this->Number->format($logRetorno->dias) ?></td>
                                        <td><?= $logRetorno->has('situacao_retorno') ? $this->Html->link($logRetorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $logRetorno->situacao_retorno->id]) : '' ?></td>
                                        <td><?= $logRetorno->has('user') ? $this->Html->link($logRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $logRetorno->user->id]) : '' ?></td>
                                        <td><?= h($logRetorno->data_confirmacao) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

