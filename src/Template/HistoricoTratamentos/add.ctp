<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Historico Tratamentos</h2>
        <ol class="breadcrumb">
            <li>Historico Tratamentos</li>
            <li class="active">
                <strong>                    Cadastrar Historico Tratamentos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="historicoTratamentos form">
                        <?= $this->Form->create($historicoTratamento) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Historico Tratamento') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('historico_id', ['data'=>'select','controller'=>'clienteHistoricos','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('atendimento_id', ['data'=>'select','controller'=>'atendimentos','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('atendimento_procedimento_id', ['data'=>'select','controller'=>'atendimentoProcedimentos','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('iten_id', ['data'=>'select','controller'=>'atendimentoItens','action'=>'fill']);
                                echo "</div>";
echo "<div class='col-md-6'>";
    echo $this->Form->input('face_id', ['data'=>'select','controller'=>'faces','action'=>'fill',  'empty' => 'selecione']);
echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('situacao_id');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

