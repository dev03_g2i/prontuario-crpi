
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Historico Tratamentos</h2>
        <ol class="breadcrumb">
            <li>Historico Tratamentos</li>
            <li class="active">
                <strong>Litagem de Historico Tratamentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Historico Tratamentos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Cliente Historico') ?></th>
                                                                <td><?= $historicoTratamento->has('cliente_historico') ? $this->Html->link($historicoTratamento->cliente_historico->id, ['controller' => 'ClienteHistoricos', 'action' => 'view', $historicoTratamento->cliente_historico->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento') ?></th>
                                                                <td><?= $historicoTratamento->has('atendimento') ? $this->Html->link($historicoTratamento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $historicoTratamento->atendimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento Procedimento') ?></th>
                                                                <td><?= $historicoTratamento->has('atendimento_procedimento') ? $this->Html->link($historicoTratamento->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $historicoTratamento->atendimento_procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento Iten') ?></th>
                                                                <td><?= $historicoTratamento->has('atendimento_iten') ? $this->Html->link($historicoTratamento->atendimento_iten->id, ['controller' => 'AtendimentoItens', 'action' => 'view', $historicoTratamento->atendimento_iten->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Face') ?></th>
                                                                <td><?= $historicoTratamento->has('face') ? $this->Html->link($historicoTratamento->face->id, ['controller' => 'Faces', 'action' => 'view', $historicoTratamento->face->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($historicoTratamento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Id') ?></th>
                                <td><?= $this->Number->format($historicoTratamento->situacao_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($historicoTratamento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($historicoTratamento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


