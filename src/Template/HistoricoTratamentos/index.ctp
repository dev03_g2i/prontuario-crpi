<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Historico Tratamentos</h2>
            <ol class="breadcrumb">
                <li>Historico Tratamentos</li>
                <li class="active">
                    <strong>Litagem de Historico Tratamentos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('HistoricoTratamento', ['type' => 'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('historico_id', ['name' => 'HistoricoTratamentos__historico_id', 'data' => 'select', 'controller' => 'clienteHistoricos', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('atendimento_id', ['name' => 'HistoricoTratamentos__atendimento_id', 'data' => 'select', 'controller' => 'atendimentos', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('atendimento_procedimento_id', ['name' => 'HistoricoTratamentos__atendimento_procedimento_id', 'data' => 'select', 'controller' => 'atendimentoProcedimentos', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('iten_id', ['name' => 'HistoricoTratamentos__iten_id', 'data' => 'select', 'controller' => 'atendimentoItens', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('face_id', ['name' => 'HistoricoTratamentos__face_id', 'data' => 'select', 'controller' => 'faces', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('situacao_id', ['name' => 'HistoricoTratamentos__situacao_id']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Historico Tratamentos', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Historico Tratamentos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Historico Tratamentos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('historico_id') ?></th>
                                        <th><?= $this->Paginator->sort('atendimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('atendimento_procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('iten_id') ?></th>
                                        <th><?= $this->Paginator->sort('face_id') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($historicoTratamentos as $historicoTratamento): ?>
                                        <tr>
                                            <td><?= $this->Number->format($historicoTratamento->id) ?></td>
                                            <td><?= $historicoTratamento->has('cliente_historico') ? $this->Html->link($historicoTratamento->cliente_historico->id, ['controller' => 'ClienteHistoricos', 'action' => 'view', $historicoTratamento->cliente_historico->id]) : '' ?></td>
                                            <td><?= $historicoTratamento->has('atendimento') ? $this->Html->link($historicoTratamento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $historicoTratamento->atendimento->id]) : '' ?></td>
                                            <td><?= $historicoTratamento->has('atendimento_procedimento') ? $this->Html->link($historicoTratamento->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $historicoTratamento->atendimento_procedimento->id]) : '' ?></td>
                                            <td><?= $historicoTratamento->has('atendimento_iten') ? $this->Html->link($historicoTratamento->atendimento_iten->id, ['controller' => 'AtendimentoItens', 'action' => 'view', $historicoTratamento->atendimento_iten->id]) : '' ?></td>
                                            <td><?= $historicoTratamento->has('face') ? $this->Html->link($historicoTratamento->face->id, ['controller' => 'Faces', 'action' => 'view', $historicoTratamento->face->id]) : '' ?></td>
                                            <td><?= $this->Number->format($historicoTratamento->situacao_id) ?></td>
                                            <td><?= h($historicoTratamento->created) ?></td>
                                            <td><?= h($historicoTratamento->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $historicoTratamento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $historicoTratamento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'historicoTratamentos\',' . $historicoTratamento->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<teste></teste>