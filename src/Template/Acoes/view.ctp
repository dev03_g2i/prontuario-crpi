
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Acoes</h2>
        <ol class="breadcrumb">
            <li>Acoes</li>
            <li class="active">
                <strong>Litagem de Acoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Acoes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Acao') ?></th>
                                <td><?= h($aco->acao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($aco->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $aco->has('situacao_cadastro') ? $this->Html->link($aco->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $aco->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $aco->has('user') ? $this->Html->link($aco->user->nome, ['controller' => 'Users', 'action' => 'view', $aco->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($aco->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($aco->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($aco->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


