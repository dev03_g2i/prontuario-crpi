<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Acoes</h2>
        <ol class="breadcrumb">
            <li>Acoes</li>
            <li class="active">
                <strong>                    Editar Acoes
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="acoes form">
                        <?= $this->Form->create($aco) ?>
                        <fieldset>
                            <legend><?= __('Editar Aco') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('acao');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('nome');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

