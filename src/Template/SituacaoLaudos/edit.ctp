<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Laudos</h2>
        <ol class="breadcrumb">
            <li>Situacao Laudos</li>
            <li class="active">
                <strong>                    Editar Situacao Laudos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="situacaoLaudos form">
                        <?= $this->Form->create($situacaoLaudo) ?>
                        <fieldset>
                            <legend><?= __('Editar Situacao Laudo') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('nome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('situacao_id');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

