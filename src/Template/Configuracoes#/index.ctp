<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Configuracoes</h2>
            <ol class="breadcrumb">
                <li>Configuracoes</li>
                <li class="active">
                    <strong>Litagem de Configuracoes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">

                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Configuracoes', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Configuracoes', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Configuracoes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('logo') ?></th>
                                        <th><?= $this->Paginator->sort('nome_login',['label'=>'Nome']) ?></th>
                                        <th><?= $this->Paginator->sort('icone') ?></th>
                                        <th><?= $this->Paginator->sort('imagem',['label'=>'Tem imagem?']) ?></th>
                                        <th><?= $this->Paginator->sort('url_imagens',['label'=>'Url Imagens']) ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($configuracoes as $configuraco): ?>
                                        <tr>
                                            <td><?= $this->Number->format($configuraco->id) ?></td>
                                            <td><?= $this->Html->image('/files/configuracoes/logo/' . $configuraco->logo_dir . '/' . $configuraco->logo, ['class' => 'img-responsive','style'=>'max-width:100px']); ?></td>
                                            <td><?= h($configuraco->nome_login) ?></td>
                                            <td><?= $this->Html->image('/files/configuracoes/icone/' . $configuraco->icone_dir . '/' . $configuraco->icone, ['class' => 'img-responsive']); ?></td>
                                            <td><?= h($configuraco->imagem==1 ?'sim':'Não') ?></td>
                                            <td><?= h($configuraco->url_imagens) ?></td>
                                            <td><?= h($configuraco->created) ?></td>
                                            <td><?= h($configuraco->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $configuraco->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $configuraco->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'configuracoes\',' . $configuraco->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
