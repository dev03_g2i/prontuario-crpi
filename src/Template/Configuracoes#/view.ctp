
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Configuracoes</h2>
        <ol class="breadcrumb">
            <li>Configuracoes</li>
            <li class="active">
                <strong>Litagem de Configuracoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Configuracoes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Logo') ?></th>
                                <td><?= h($configuraco->logo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Logo Dir') ?></th>
                                <td><?= h($configuraco->logo_dir) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nome Login') ?></th>
                                <td><?= h($configuraco->nome_login) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Icone') ?></th>
                                <td><?= h($configuraco->icone) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Icone Dir') ?></th>
                                <td><?= h($configuraco->icone_dir) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($configuraco->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($configuraco->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($configuraco->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


