<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Configuracoes</h2>
        <ol class="breadcrumb">
            <li>Configuracoes</li>
            <li class="active">
                <strong>                    Editar Configuracoes
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="configuracoes form">
                        <?= $this->Form->create($configuraco,['type'=>'file','id'=>'form-config']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Configurações') ?></legend>
                            <div class='col-md-6'>
                                <?= $this->Form->input('nome_login',['label'=>'Nome']); ?>
                            </div>

                            <div class='col-md-6'>
                                <?= $this->Form->input('url_imagens',['label'=>'Url das imagens']); ?>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="logo">Logo</label>
                                    <input id="logo" type="file" name="caminho" class="input-file-edit" link-img="/files/configuracoes/logo/<?= $configuraco->logo_dir ?>/<?= $configuraco->logo ?>" descricao-img=""
                                           controller-img="Configuracoes" img-id="<?= $configuraco->id ?>" data-overwrite-initial="false" >
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="icone">Icone</label>
                                    <input id="icone" type="file" name="icone" class="input-file-edit" link-img="/files/configuracoes/icone/<?= $configuraco->icone_dir ?>/<?= $configuraco->icone ?>" descricao-img=""
                                           controller-img="Configuracoes" img-id="<?= $configuraco->id ?>" data-overwrite-initial="false" >
                                </div>
                            </div>

                            <div class='col-md-6'>
                                <?= $this->Form->input('usa_carteira', ['label' => 'Usa Carteira?','type'=>'select','options'=>['1'=>'Sim','2'=>'Não'],'empty'=>'selecione']); ?>
                            </div>

                            <div class='col-md-6' id="dv-carteira" style="<?= $configuraco->usa_carteira==1 ? '': 'display: none'?>">
                                <?= $this->Form->input('carteira', ['label' => 'Início Carteira']); ?>
                            </div>
                            <div class='col-md-6'>
                                <?= $this->Form->input('imagem',['label'=>' Tem imagem?']); ?>
                            </div>
                        </fieldset>
                        <div class="progress" style="display: none;">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                <span class="sr-only">0% Complete</span>
                            </div>
                        </div>


                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary','onclick' => 'formanexos("form-config","configuracoes","index")']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

