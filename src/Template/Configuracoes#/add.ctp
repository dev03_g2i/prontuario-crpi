<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Configurações</h2>
            <ol class="breadcrumb">
                <li>Configurações</li>
                <li class="active">
                    <strong> Cadastrar Configurações
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="configuracoes form">
                            <?= $this->Form->create($configuraco,['type'=>'file','id'=>'form-config']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Configurações') ?></legend>
                                <?php
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('nome_login',['label'=>'Nome']);
                                echo "</div>";
                                ?>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="logo">Logo</label>
                                        <input id="logo" type="file" name="logo" class="input-file"
                                               data-overwrite-initial="false" data-min-file-count="1">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="icone">Icone</label>
                                        <input id="icone" type="file" name="icone" class="input-file"
                                               data-overwrite-initial="false" data-min-file-count="1">
                                    </div>
                                </div>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('imagem',['label'=>' Tem imagem?']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('url_imagens',['label'=>'Url das imagens']);
                                echo "</div>";
                                ?>
                            </fieldset>

                            <div class="progress" style="display: none;">
                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                    <span class="sr-only">0% Complete</span>
                                </div>
                            </div>

                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary','onclick' => 'formanexos("form-config","configuracoes","index")']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

