<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Recebimento-Conferência</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Faturamento',['type'=>'get', 'id' => 'form-faturamento-reclistar']) ?>

                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_inicio', [ 'label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('unidade_id',['label' => 'Unidade', 'options' => $unidades, 'default' => 1]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('origen_id', ['label' => 'Origem', 'options' => $origens]);?>
                    </div>
                    <!--<div class='col-md-3'>
                        <?php echo $this->Form->input('convenio_id',['name' => 'convenios[]', 'multiple', 'label' => 'Convênio', 'empty' => 'Selecione', 'options' => $convenios, 'class' => 'select2']);?>
                    </div>-->
                    <div class="col-md-3">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']);?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_fim', ['label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('medico_id', ['name' => 'medicos[]', 'multiple', 'label' => 'Médico Executor', 'empty' => 'Selecione', 'options' => $medicos, 'class' => 'select2']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('controle', ['type' => 'text']);?>
                    </div>
                    <!--<div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_recebimento_id', ['label' => 'Situação', 'options' => $situacao_recebimentos, 'empty' => 'Todos']);?>
                    </div>-->
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_datas', ['label' => 'Datas', 'options' => ['data'=>'Data Atendimento', 'dt_recebimento'=>'Data Recebimento'], 'default' => 'data']);?>
                    </div>
                    <div class="col-md-3 text-left" style="margin-top: 22px;">
                        <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default btnFiltrarReclistar','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        <button type="button" class="btn btn-primary m-r-sm count" toggle="tooltip", data-placement="bottom", title="Qtd. Atendimentos">0</button>
                        <!--<button type="button" class="btn btn-info m-r-sm total" toggle="tooltip", data-placement="bottom", title="Total Geral">R$ 0</button>-->
                    </div>

                    <?php echo $this->Form->input('faturaEncerramentoId', ['type' => 'hidden', 'value' => $faturaEncerramentoId])?>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Total Geral </label><br>
                                <span class="valor_prod_fatura total">R$ 0</span>
                            </div>
                            <div class="col-md-4">
                                <label>Vl.Recebido</label><br>
                                <span class="valor_prod_convenio valor_recebido">R$ 0</span>
                            </div>
                            <div class="col-md-4">
                                <label>Vl.Recebido Mat/Med</label><br>
                                <span class="valor_prod_convenio valor_rec_matmed">R$ 0</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-4">
                            <label>Total Caixa </label><br>
                            <span class="total_caixa">R$ 0</span>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-right">
                            <label>Diferença</label><br>
                            <span class="valor_prodclin_recebimento diferenca">R$ 0</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-4">
                        <?php //$this->Form->button($this->Html->icon('check'), ['name' => 'check[]', 'onclick' => 'marcarDesmarcar()', 'type'=>'button', 'class' => 'btn btn-success','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Marcar/Desmarcar Todos','escape' => false]) ?>
                    </div>
                    <div class="col-md-4 text-center">
                        <?php //echo $this->Form->button('<i class="fa fa-check"></i> Finalizar', ['type' => 'button', 'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Finalizar','class'=>'btn btn-info','id' => 'btnFinalizarFaturamento', 'escape' => false]) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php
                        echo $this->Form->dropdownButton('<i class="fa fa-list"></i> Receber Todos', [
                            $this->Html->link('Vl.Faturado', ['action' => 'rec-receber-todos', $faturaEncerramentoId, 'first' => 1, 'column_valor' => 'total'], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']),
                            $this->Html->link('Vl.Caixa', ['action' => 'rec-receber-todos', $faturaEncerramentoId, 'first' => 1, 'column_valor' => 'valor_caixa'], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']),
                        ], ['class' => 'btn-primary']);

                        //echo $this->Html->link('<i class=""></i> Receber Todos', ['action' => 'rec-receber-todos', $faturaEncerramentoId, 'first' => 1, 'ajax' => 1], ['type' => 'button', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Receber Todos','class'=>'btn btn-primary', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg'])

                        ?>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('atendimento_id') ?></th>
                                <th><?= $this->Paginator->sort('Atendimentos.data', 'Data') ?></th>
                                <!--<th><?= $this->Paginator->sort('horario') ?></th>-->
                                <th><?= $this->Paginator->sort('Atendimentos.cliente_id', 'Paciente') ?></th>
                                <!--<th><?= $this->Paginator->sort('convenio') ?></th>-->
                                <th><?= $this->Paginator->sort('MedicoResponsaveis.id', 'Profissional') ?></th>
                                <!--<th><?= $this->Paginator->sort('matricula') ?></th>-->
                                <th><?= $this->Paginator->sort('codigo') ?></th>
                                    <th><?= $this->Paginator->sort('Procedimentos.id', 'Procedimento') ?></th>
                                <th><?= $this->Paginator->sort('nr_guia') ?></th>
                                <th><?= $this->Paginator->sort('autorizacao_senha') ?></th>
                                <th><?= $this->Paginator->sort('quantidade', 'Qtd') ?></th>
                                <th class="valor-prod-fatura"><?= $this->Paginator->sort('total') ?></th>
                                <th><?= $this->Paginator->sort('valor_caixa', 'Vl.Caixa')?></th>
                                <th ><?= $this->Paginator->sort('dt_recebimento', 'Dt.Recebimento') ?></th>
                                <th class="valor-prod-convenio"><?= $this->Paginator->sort('valor_recebido', 'Vl.Recebido') ?></th>
                                <th class="valor-prod-convenio"><?= $this->Paginator->sort('valor_rec_matmed', 'Vl.Recebido Mat/Med') ?></th>
                                <th><?= $this->Paginator->sort('situacao_recebimento_id', 'Situação Rec.Procedimento') ?></th>
                                <th><?= $this->Paginator->sort('situacao_recmatmed_id', 'Situação Rec.Mat/Med') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($atendimentoProcedimentos as $atendimentoProcedimento): ?>
                                <tr>
                                    <td><?=$atendimentoProcedimento->atendimento_id?></td>
                                    <td><?=$this->Time->format($atendimentoProcedimento->atendimento->data, 'dd/MM/YYYY')?></td>
                                    <!--<td><?=$this->Time->format($atendimentoProcedimento->atendimento->hora, 'HH:mm')?></td>-->
                                    <td><?=$atendimentoProcedimento->atendimento->cliente->nome?></td>
                                    <!--<td><?=$atendimentoProcedimento->atendimento->convenio->nome?></td>-->
                                    <td><?=$atendimentoProcedimento->medico_responsavei->nome?></td>
                                    <!--<td><?=$atendimentoProcedimento->matricula?></td>-->
                                    <td><?=$atendimentoProcedimento->codigo?></td>
                                    <td><?=$atendimentoProcedimento->procedimento->nome?></td>
                                    <td><?=$atendimentoProcedimento->nr_guia?></td>
                                    <td><?=$atendimentoProcedimento->autorizacao_senha?></td>
                                    <td><?=$atendimentoProcedimento->quantidade?></td>
                                    <td class="valor_prod_fatura"><?=number_format($atendimentoProcedimento->total, 2, '.', '')?></td>
                                    <td><?=number_format($atendimentoProcedimento->valor_caixa, 2, '.', '')?></td>
                                    <td><?=$this->Time->format($atendimentoProcedimento->dt_recebimento, 'dd/MM/YYYY')?></td>
                                    <td class="valor_prod_convenio"><?=number_format($atendimentoProcedimento->valor_recebido, 2,'.','')?></td>
                                    <td class="valor_prod_convenio"><?=number_format($atendimentoProcedimento->valor_rec_matmed, 2,'.','')?></td>
                                    <td><?=$this->Faturamento->situacaoRecebimento($atendimentoProcedimento->situacao_recebimento_id) ?></td>
                                    <td><?=$this->Faturamento->situacaoRecebimento($atendimentoProcedimento->situacao_recmatmed_id) ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-prontuario">
                                                <li><?= $this->Html->link('<i class=""></i> Informar Recebmento', ['controller' => 'SituacaoRecebimentos', 'action' => 'situacao', $atendimentoProcedimento->id, '?'=>['first'=>1,'ajax'=>1]],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                <li><?=$this->Html->link('<i class="fa fa-user-md"></i> Mat/Med - Fatura', ['controller' => 'FaturaMatmed', 'action' => 'index', $atendimentoProcedimento->atendimento_id], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?></li>
                                                <li><?= $this->Html->link('<i class=""></i> Recursos de Glosas', ['controller' => 'FaturaRecursos', 'action' => 'index', $atendimentoProcedimento->id, '?' => ['first' => 1]],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php  endforeach;?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                    <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>