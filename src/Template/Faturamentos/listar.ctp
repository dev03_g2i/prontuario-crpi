<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Encerramento</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Faturamento',['type'=>'get', 'id' => 'form-faturamentos']) ?>

                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_inicio', ['required' => 'required', 'label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('unidade_id',['label' => 'Unidade', 'options' => $unidades, 'default' => 1]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('origen_id', ['label' => 'Origem', 'empty' => 'Selecione', 'options' => $origens]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('convenio_id',['id' => 'convenio_id_encerramento','required' => 'required', 'name' => 'convenio', 'label' => 'Convênio', 'empty' => 'Selecione', 'options' => $convenios]);?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_fim', ['required' => 'required', 'label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']);?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('medico_id', ['name' => 'medicos[]', 'multiple', 'label' => 'Médico Executor', 'empty' => 'Selecione', 'options' => $medicos, 'class' => 'select2']);?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('controle', ['type' => 'text']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_fatura_id', ['label' => 'Conferência', 'options' => $situacao_faturas, 'empty' => 'Todos']);?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('ordenar_por', ['options' => ['TempFaturamentos.atendimento_id' => 'Atendimento', 'TempFaturamentos.data_atendimento' => 'Data', 'TempFaturamentos.paciente' => 'Paciente'], 'empty' => 'Selecione']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 fill-content-search">
                        <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default btnFiltrarEncerramento','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default btnRefresh','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                    </div>


                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Qtd. </label><br>
                                <span class="quantidade_reg">0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Tl.Caixa </label><br>
                                <span class="valor_caixa">R$ 0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Tl.Material </label><br>
                                <span class="valor_material">R$ 0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Vl.Total </label><br>
                                <span class="total_geral">R$ 0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Vl.Total + Tl.Material </label><br>
                                <span class="total_geral_com_material">R$ 0</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-4">
                        <?= $this->Form->button($this->Html->icon('check'), ['name' => 'check[]', 'onclick' => 'marcarDesmarcarTemp()', 'type'=>'button', 'class' => 'btn btn-success','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Marcar/Desmarcar Todos','escape' => false]) ?>
                    </div>
                    <div class="col-md-4 text-center">
                        <?php //echo $this->Form->button('<i class="fa fa-check"></i> Finalizar', ['type' => 'button', 'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Finalizar','class'=>'btn btn-info','id' => 'btnFinalizarFaturamento', 'escape' => false]) ?>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php echo $this->Html->link('<i class="fa fa-close"></i> Finalizar', ['action' => 'encerramento'], ['type' => 'button', 'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Encerrar','class'=>'btn btn-primary', 'id' => 'btnEncerrar', 'escape' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('Faturar') ?></th>
                                <th><?= $this->Paginator->sort('Conf.') ?></th>
                                <th><?= $this->Paginator->sort('atendimento') ?></th>
                                <th><?= $this->Paginator->sort('data') ?></th>
                                <!--<th><?= $this->Paginator->sort('horario') ?></th>-->
                                <th><?= $this->Paginator->sort('paciente') ?></th>
                                <th><?= $this->Paginator->sort('convenio') ?></th>
                                <th><?= $this->Paginator->sort('profissional') ?></th>
                                <th><?= $this->Paginator->sort('matricula') ?></th>
                                <th><?= $this->Paginator->sort('codigo') ?></th>
                                <th><?= $this->Paginator->sort('procedimento') ?></th>
                                <th><?= $this->Paginator->sort('guia') ?></th>
                                <th><?= $this->Paginator->sort('senha') ?></th>
                                <th><?= $this->Paginator->sort('quantidade', 'Qtd') ?></th>
                                <th><?= $this->Paginator->sort('valor_fatura', 'Vl.Fatura') ?></th>
                                <th><?= $this->Paginator->sort('valor_caixa', 'Vl.Caixa') ?></th>
                                <th><?= $this->Paginator->sort('material', 'Vl.Material') ?></th>
                                <th><?= $this->Paginator->sort('total') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                if($tempFaturamento):
                            $i=0;
                            foreach ($atendimentoProcedimentos as $atendimentoProcedimento):?>
                                <tr class="marcar<?=$i?> <?=($atendimentoProcedimento->selecionado==1)?' bg-success':''?>">
                                    <td>
                                        <?=$this->Form->checkbox('selecionadotemp', ['data-posicao' => $i,'name' => 'selecionadostemp[]', 'value' => $atendimentoProcedimento->id, ($atendimentoProcedimento->selecionado ==1)?'checked':'' ]);?>
                                    </td>
                                    <td>
                                        <?=$this->Form->checkbox('selecionado', [($atendimentoProcedimento->selecionado ==1)?'checked':'', 'disabled' => 'disabled' ]);?>
                                    </td>
                                    <td><?=$atendimentoProcedimento->atendimento_id?></td>
                                    <td><?=$this->Time->format($atendimentoProcedimento->data_atendimento, 'dd/MM/YYYY')?></td>
                                    <!--<td><?=$this->Time->format($atendimentoProcedimento->hora, 'HH:mm')?></td>-->
                                    <td><?=$atendimentoProcedimento->paciente?></td>
                                    <td><?=$atendimentoProcedimento->convenio?></td>
                                    <td><?=$atendimentoProcedimento->profissional?></td>
                                    <td><?=$atendimentoProcedimento->matricula?></td>
                                    <td><?=$atendimentoProcedimento->codigo_tabela?></td>
                                    <td><?=$atendimentoProcedimento->procedimento?></td>
                                    <td><?=$atendimentoProcedimento->guia?></td>
                                    <td><?=$atendimentoProcedimento->senha?></td>
                                    <td><?=$atendimentoProcedimento->quantidade?></td>
                                    <td><?=number_format($atendimentoProcedimento->valor_fatura, 2,'.','')?></td>
                                    <td><?=number_format($atendimentoProcedimento->valor_caixa, 2,'.','')?></td>
                                    <td><?=number_format($atendimentoProcedimento->valor_material, 2,'.','')?></td>
                                    <td><?=number_format($atendimentoProcedimento->total, 2, '.', '')?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-prontuario">
                                                <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Abrir atendimento', ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentoProcedimento->atendimento_id],['escape' => false, 'target' => '_blank']) ?></li>
                                                <li><?= $this->Html->link('Mat/Med - Fatura', ['controller' => 'FaturaMatmed', 'action' => 'index', $atendimentoProcedimento->atendimento_id],['escape' => false, 'target' => '_blank']) ?></li>
                                                <li><?= $this->Html->link('Guia SADT', [],['escape' => false]) ?></li>
                                                <li><?= $this->Html->link('Guia outras despesas', [],['escape' => false]) ?></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php  $i++; endforeach; endif;?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?php if($tempFaturamento):?>
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>