
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Faturamentos</h2>
        <ol class="breadcrumb">
            <li>Faturamentos</li>
            <li class="active">
                <strong>Litagem de Faturamentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Faturamentos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Atendimento Procedimento') ?></th>
                                                                <td><?= $faturamento->has('atendimento_procedimento') ? $this->Html->link($faturamento->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $faturamento->atendimento_procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero Autorizacao') ?></th>
                                <td><?= h($faturamento->numero_autorizacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero Guia') ?></th>
                                <td><?= h($faturamento->numero_guia) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faturamento->has('user') ? $this->Html->link($faturamento->user->nome, ['controller' => 'Users', 'action' => 'view', $faturamento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem modificou') ?></th>
                                                                <td><?= $faturamento->has('users_up') ? $this->Html->link($faturamento->users_up->nome, ['controller' => 'Users', 'action' => 'view', $faturamento->users_up->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturamento->has('situacao_cadastro') ? $this->Html->link($faturamento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturamento->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturamento->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Autorizacao') ?></th>
                                                                <td><?= h($faturamento->data_autorizacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Guia') ?></th>
                                                                <td><?= h($faturamento->data_guia) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturamento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturamento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


