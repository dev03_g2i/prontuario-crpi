<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Listagem de Faturas</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Faturamento',['type'=>'get']) ?>
                        <div class="col-sm-12 no-padding">
                            <div class='col-sm-4'>
                                <?= $this->Form->input('convenio_id', ['name' => 'convenio_id', 'options' => $convenios, 'empty' => 'Selecione']);?>
                            </div>
                            <div class='col-sm-4'>
                                <?= $this->Form->input('situacao_faturaencerramento_id', ['label' => 'Situação','type' => 'select', 'data' => 'select', 'controller' => 'SituacaoFaturaencerramentos', 'action' => 'fill']); ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $this->Form->input('have_nfs', ['label' => 'Com Nfs?', 'id' => 'have_nfs', 'name' => 'have_nfs', 'options' => [1 => 'Não', 2 => 'Sim'], 'empty' => 'Selecione']);?>
                            </div>
                        </div>
                        <div class="col-sm-12 no-padding">
                            <div class="col-sm-3">
                                <?= $this->Form->input('search-billing', ['label' => 'Buscar por data de', 'id' => 'search-billing', 'name' => 'search-billing', 'options' => [1 => 'Vencimento', 2 => 'Encer/Entrega'], 'empty' => 'Selecione']);?>
                            </div>
                            <div class="col-sm-6 no-padding" id="search-box-period-billing" style="visibility: hidden">
                                <div class="col-sm-6">
                                    <?= $this->Form->input('periodo_busca_inicio', ['id' => 'periodo_busca_inicio', 'label' => 'Inicio', 'type' => 'text', 'class' => 'datepicker']); ?>
                                </div>
                                <div class="col-sm-6">
                                    <?= $this->Form->input('periodo_busca_fim', ['id' => 'periodo_busca_fim', 'label' => 'Fim', 'type' => 'text', 'class' => 'datepicker']); ?>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins m-t-25">
            <div class="clearfix">
                <div class="table-responsive">
                    <?php if($countFaturas > 0): ?>
                    <div class="ibox-content m-b-25">
                        <table class="table table-hover" style="font-size: 12px">
                            <thead>
                                <tr>
                                    <th>Total bruto</th>
                                    <th>Total previsto</th>
                                    <th>Total recebido</th>
                                    <th>Total nfs</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <span class="total_brutoAll">
                                            <?= number_format($totalBrutoAll, 2, ',', '.')?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="total_previstoAll">
                                            <?= number_format($totalPrevistoAll, 2, ',', '.')?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="total_recebidoAll">
                                            <?= number_format($totalRecebidoAll, 2, ',', '.')?>
                                        </span>
                                    </td>
                                    <td>
                                        <span class="total_nfsAll">
                                            <?= number_format($totalNfs, 2, ',', '.')?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="ibox-content">
                        <table class="table table-hover" style="font-size: 11px">
                            <thead>
                                <tr>
                                    <th>
                                        <?= $this->Paginator->sort('id', 'Id') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('convenio_id', 'Convênio') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('competencia') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('data_encerramento ', 'Dt.Encer/Entrega') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('vencimento') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('qtd_total', 'Qtd.Total') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('total_bruto', 'Ttl.Bruto') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('total_previsto', 'Ttl.previsto') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('taxas') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('descontos') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('valor_recebido', 'Ttl.Rec.Conferência') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('total_nfs', 'Ttl.Nfs') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('vl_received_bank', 'Vlr.Rec.Banco')?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('Data Quitação', 'Dt. Quitação') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('situação') ?>
                                    </th>
                                    <!--<th><?= $this->Paginator->sort('num_exames', 'Num.Exames') ?></th>-->
                                    <th class="actions">
                                        <?= __('Ações') ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($faturaEncerramentos as $faturaEncerramento):?>
                                <tr class="delete<?=$faturaEncerramento->id?>">
                                    <td>
                                        <?= h($faturaEncerramento->id)?>
                                    </td>
                                    <td>
                                        <?= $faturaEncerramento->has('convenio') ? $this->Html->link($faturaEncerramento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $faturaEncerramento->convenio->id]) : '' ?>
                                    </td>
                                    <td>
                                        <?= h($faturaEncerramento->competencia) ?>
                                    </td>
                                    <td>
                                        <?= h($faturaEncerramento->data_encerramento) ?>
                                    </td>
                                    <td>
                                        <?= h($faturaEncerramento->vencimento) ?>
                                    </td>
                                    <td>
                                        <?= h($faturaEncerramento->qtd_total) ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $faturaEncerramento->total_bruto ? h(number_format($faturaEncerramento->total_bruto, 2, ',', '.')) : '0,00' ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $faturaEncerramento->total_bruto ? h(number_format($this->Faturamento->calculoValorPrevisto($faturaEncerramento->convenio->id, $faturaEncerramento->total_bruto), 2, ',', '.')) : '0,00' ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $faturaEncerramento->taxas ? h(number_format($faturaEncerramento->taxas, 2, ',', '.')) : '0,00' ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $faturaEncerramento->descontos ? h(number_format($faturaEncerramento->descontos, 2, ',', '.')) : '0,00' ?>
                                    </td>
                                    <td class="text-right">
                                        <?= h(number_format($this->Faturamento->somaValorRecebido($faturaEncerramento->id), 2, ',', '.')); ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $faturaEncerramento->total_bruto ? h(number_format($this->Faturamento->calculoTotalNfs($faturaEncerramento->id), 2, ',', '.')) : '0,00'  ?>
                                    </td>
                                    <td class="text-right">
                                        <?= $faturaEncerramento->vl_received_bank ? h(number_format($faturaEncerramento->vl_received_bank, 2, ',', '.')) : '0,00' ?>
                                    </td>
                                    <td>
                                        <?= h($faturaEncerramento->data_quitacao) ?>
                                    </td>
                                    <td>
                                        <?= $this->Faturamento->getSituationName($faturaEncerramento->situacao_faturaencerramento_id); ?>
                                    </td>
                                    <!--<td><?= h($faturaEncerramento->num_exames) ?></td>-->
                                    <td class="actions">
                                        <div class="dropdown">
                                            <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                                <span class="fa fa-list"></span> Opções
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-prontuario">
                                                <li>
                                                    <?= $this->Html->link('<i class="fa fa-edit"></i> Editar', ['controller' => 'FaturaEncerramentos', 'action' => 'edit', $faturaEncerramento->id],['escape' => false, 'target' => '_blank']) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class="fa fa-money"></i> Recebimentos', ['action' => 'rec-listar', $faturaEncerramento->id],['escape' => false, 'target' => '_blank']) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Notas Fiscais', ['controller' => 'fatura_nfs', 'action' => 'index', '?' => ['unidade_id' => $faturaEncerramento->unidade_id, 'fatura_id' => $faturaEncerramento->id]],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class="fa fa-trash-o"></i> Excluir Fatura',  'javascript:void(0)', ['onclick'=>'DeletarModal(\'Faturamentos\','.$faturaEncerramento->id.')', 'escape' => false]) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class=""></i> Imprimir', ['controller'=>'Faturamentos', 'action'=>'report-fatura', $faturaEncerramento->id],['escape' => false, 'target'=>'_black']) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class=""></i> Recalcular', [],['escape' => false]) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class=""></i> Gerar TISS/XML', [],['escape' => false]) ?>
                                                </li>
                                                <li>
                                                    <?= $this->Html->link('<i class=""></i> Retorno TISS/XML', [],['escape' => false]) ?>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div style="text-align:center">
                        <div class="paginator">
                            <ul class="pagination">
                                <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                            </ul>
                            <p>
                                <?= $this->Paginator->counter() ?>
                            </p>
                        </div>
                    </div>
                    <?php else : ?>
                    <div class="text-center">
                        <strong>Não foram encontradas faturas com esse filtro!</strong>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>