<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Faturamentos</h2>
        <ol class="breadcrumb">
            <li>Faturamentos</li>
            <li class="active">
                <strong>Litagem de Faturamentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('Faturamento',['type'=>'get']) ?>
                        <?php
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('atendimento_procedimento_id', ['name'=>'Faturamentos__atendimento_procedimento_id','data'=>'select','controller'=>'atendimentoProcedimentos','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('numero_autorizacao',['name'=>'Faturamentos__numero_autorizacao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                    echo $this->Form->input('data_autorizacao', ['name'=>'Faturamentos__data_autorizacao','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('numero_guia',['name'=>'Faturamentos__numero_guia']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                    echo $this->Form->input('data_guia', ['name'=>'Faturamentos__data_guia','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Faturamentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Faturamentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Faturamentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('atendimento_procedimento_id') ?></th>
                <th><?= $this->Paginator->sort('numero_autorizacao') ?></th>
                <th><?= $this->Paginator->sort('data_autorizacao') ?></th>
                <th><?= $this->Paginator->sort('numero_guia') ?></th>
                <th><?= $this->Paginator->sort('data_guia') ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                <th><?= $this->Paginator->sort('user_up_id',['label'=>'Quem Modificou']) ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($faturamentos as $faturamento): ?>
            <tr>
                <td><?= $this->Number->format($faturamento->id) ?></td>
                <td><?= $faturamento->has('atendimento_procedimento') ? $this->Html->link($faturamento->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $faturamento->atendimento_procedimento->id]) : '' ?></td>
                <td><?= h($faturamento->numero_autorizacao) ?></td>
                <td><?= h($faturamento->data_autorizacao) ?></td>
                <td><?= h($faturamento->numero_guia) ?></td>
                <td><?= h($faturamento->data_guia) ?></td>
                <td><?= h($faturamento->created) ?></td>
                <td><?= h($faturamento->modified) ?></td>
                <td><?= $faturamento->has('user') ? $this->Html->link($faturamento->user->nome, ['controller' => 'Users', 'action' => 'view', $faturamento->user->id]) : '' ?></td>
                <td><?= $faturamento->has('users_up') ? $this->Html->link($faturamento->users_up->nome, ['controller' => 'Users', 'action' => 'view', $faturamento->users_up->id]) : '' ?></td>
                <td><?= $faturamento->has('situacao_cadastro') ? $this->Html->link($faturamento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturamento->situacao_cadastro->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturamentos','action' => 'view', $faturamento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturamentos','action' => 'edit', $faturamento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturamentos','action'=>'delete', $faturamento->id],['onclick'=>'Deletar(\'faturamentos\','.$faturamento->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
