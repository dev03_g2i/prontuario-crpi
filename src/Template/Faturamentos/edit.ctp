<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Faturamentos</h2>
            <ol class="breadcrumb">
                <li>Faturamentos</li>
                <li class="active">
                    <strong> Editar Faturamentos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturamentos form">
                            <?= $this->Form->create($faturamento) ?>
                            <fieldset>
                                <legend><?= __('Editar Faturamento') ?></legend>
                                <?= $this->Form->input('atendimento_procedimento_id', ['type'=>'hidden','value' => $faturamento->atendimento_procedimento_id]);?>

                                <div class='col-md-6'>
                                    <?= $this->Form->input('numero_autorizacao',['label'=>'Número Autorização']);?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data_autorizacao', ['label'=>'Data autorização','type' => 'text', 'class' => 'datetimepicker', 'value' => $this->Time->format($faturamento->data_autorizacao, 'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('numero_guia',['label'=>'Número Guia']);?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data_guia', ['label'=>'Data Emissão Guia','type' => 'text', 'class' => 'datetimepicker', 'value' => $this->Time->format($faturamento->data_guia, 'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                                </div>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->button(__('Voltar'), ['type'=>'button','class' => 'btn btn-default','onclick'=>'Navegar(\'\',\'back\')']) ?>
                                <?= $this->Form->button(__('Salvar'), ['type'=>'submit','class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

