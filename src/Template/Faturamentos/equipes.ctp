<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Faturamento - Equipe(Profissionais)</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Faturamento', ['type' => 'get', 'id' => 'form-equipes']) ?>

                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_inicio', ['required' => 'required', 'label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('unidade_id', ['label' => 'Unidade', 'options' => $unidades, 'empty' => 'Selecione', 'default' => 1]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('origen_id', ['label' => 'Origem', 'empty' => 'Selecione', 'options' => $origens]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('convenio_id', ['id' => 'convenio_id_faturamento', 'name' => 'convenios[]', 'multiple', 'label' => 'Convênio', 'empty' => 'Selecione', 'options' => $convenios, 'class' => 'select2']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_fim', ['required' => 'required', 'label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('medico_id', ['name' => 'medicos[]', 'multiple', 'label' => 'Profissional Equipe', 'empty' => 'Selecione', 'options' => $medicos, 'class' => 'select2']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('controle', ['type' => 'text']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_fatura_id', ['label' => 'Situação', 'options' => $situacao_faturas, 'empty' => 'Todos']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                            'options' => $tiposRelatorios,
                            'append' => [
                                $this->Form->button($this->Html->icon('print'), ['id' => 'gerar-report-equipes',
                                    'type' => 'button', 'class' => 'btn btn-success',
                                    'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'relatorios',
                                    'action' => 'conferenciaFaturamentoEquipes'])]
                        ]) ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('ordenar_por', ['options' => [1 => 'Atendimento', 2 => 'Data', 3 => 'Paciente'], 'empty' => 'Selecione']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('tipo_equipe', ['label' => 'Equipe/Tipo', 'options' => $tipo_equipes, 'empty' => 'Selecione']); ?>
                    </div>
                    <div class="col-md-12 text-center fill-content-search">
                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default btnFilterEquipes', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default btnRefresh', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        <button type="button" class="btn btn-primary m-r-sm contador" toggle="tooltip"
                                data-placement="bottom" title="Qtd. Atendimentos">0
                        </button>
                    </div>

                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Total Profissional </label><br>
                                <span class="total_geral">R$ 0</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">

            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('conferencia') ?></th>
                                <th><?= $this->Paginator->sort('atendimento') ?></th>
                                <th><?= $this->Paginator->sort('data') ?></th>
                                <th><?= $this->Paginator->sort('carater_atendimento', 'Carater Atendimento') ?></th>
                                <th><?= $this->Paginator->sort('paciente') ?></th>
                                <th><?= $this->Paginator->sort('convenio') ?></th>
                                <th><?= $this->Paginator->sort('profissional', 'Prof.Equipe') ?></th>
                                <th><?= $this->Paginator->sort('matricula') ?></th>
                                <th><?= $this->Paginator->sort('codigo') ?></th>
                                <th><?= $this->Paginator->sort('procedimento') ?></th>
                                <th><?= $this->Paginator->sort('senha') ?></th>
                                <th><?= $this->Paginator->sort('quantidade', 'Qtd') ?></th>
                                <th><?= $this->Paginator->sort('percentual_cobranca', 'Perc. Cobrança') ?></th>
                                <th><?= $this->Paginator->sort('tipo_equipe_id', 'Equipe/Tipo') ?></th>
                                <th><?= $this->Paginator->sort('tipo_equipegrau_id', 'Grau') ?></th>
                                <th><?= $this->Paginator->sort('porcentagem') ?></th>
                                <th><?= $this->Paginator->sort('valor_fatura', 'Vl.Fatura') ?></th>
                                <th><?= $this->Paginator->sort('valor_caixa', 'Vl.Caixa') ?></th>
                                <th><?= $this->Paginator->sort('total', 'Vl.Profissional') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            if ($atendimentoProcedimentos != null):
                                foreach ($atendimentoProcedimentos as $atendimentoProcedimento): ?>
                                    <tr class="marcar<?= $i ?> <?= ($atendimentoProcedimento->atendimento_procedimento->situacao_fatura_id == 2) ? ' bg-success' : '' ?>">
                                        <td><?= $this->Form->checkbox('selecionado', ['data-posicao' => $i, 'name' => 'selecionados[]', 'value' => $atendimentoProcedimento->atendimento_procedimento->id, ($atendimentoProcedimento->atendimento_procedimento->situacao_fatura_id == 2) ? 'checked' : '']); ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->atendimento->id ?></td>
                                        <td><?= $this->Time->format($atendimentoProcedimento->atendimento_procedimento->atendimento->data, 'dd/MM/YYYY') ?></td>
                                        <td><?= !empty($atendimentoProcedimento->atendimento_procedimento->atendimento->internacao_carater_atendimento->descricao) ? $atendimentoProcedimento->atendimento_procedimento->atendimento->internacao_carater_atendimento->descricao : 'Não Informado' ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->atendimento->cliente->nome ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->atendimento->convenio->nome ?></td>
                                        <td><?= $atendimentoProcedimento->medico_responsavei->nome ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->matricula ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->codigo ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->procedimento->nome ?></td>
                                        </td>
                                        <td>
                                            <a class="edit_local" href="#" data-type="text"
                                               data-controller="AtendimentoProcedimentos" data-action="parcial-edit-ap"
                                               data-param="text" data-title="autorizacao_senha"
                                               data-pk="<?= $atendimentoProcedimento->atendimento_procedimento->id ?>"
                                               data-value="<?= $atendimentoProcedimento->atendimento_procedimento->autorizacao_senha ?>"
                                               listen="f"><?= $atendimentoProcedimento->atendimento_procedimento->autorizacao_senha ?>
                                            </a>
                                        </td>
                                        <td><?= $atendimentoProcedimento->atendimento_procedimento->quantidade ?></td>
                                        <td><?= $this->Number->toPercentage($atendimentoProcedimento->atendimento_procedimento->percentual_cobranca * 100) ?></td>
                                        <td><?= $atendimentoProcedimento->tipo_equipe->nome ?></td>
                                        <td><?= $atendimentoProcedimento->tipo_equipegrau->descricao ?></td>
                                        <td><?= $this->Number->toPercentage($atendimentoProcedimento->porcentagem) ?></td>
                                        <td><?= number_format($atendimentoProcedimento->atendimento_procedimento->valor_fatura, 2, '.', '') ?></td>
                                        <td><?= number_format($atendimentoProcedimento->atendimento_procedimento->valor_caixa, 2, '.', '') ?></td>
                                        <!--<td><?= number_format($this->Faturamento->calculaTotalProfissional($atendimentoProcedimento->id), 2, '.', '') ?></td>-->
                                        <td><?= number_format($atendimentoProcedimento->valor_profissional, 2, '.', '') ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button"
                                                        data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Abrir atendimento', ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentoProcedimento->atendimento_procedimento->atendimento_id], ['escape' => false, 'target' => '_blank']) ?></li>
                                                    <li class="<?= $this->Configuracao->usaEquipe(); ?>">
                                                        <?= $this->Html->link('<i class="fa fa-users"></i> Equipe', ['controller' => 'AtendimentoProcedimentoEquipes', 'action' => 'index', $atendimentoProcedimento->atendimento_procedimento->id], ['escape' => false, 'target' => '_blank']); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; endforeach; endif; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?php if ($atendimentoProcedimentos): ?>
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="append-report hidden">

</div>
