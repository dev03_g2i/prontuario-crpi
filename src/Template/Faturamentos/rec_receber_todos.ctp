<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Receber todas faturas</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaNfs form">
                            <?= $this->Form->create('receberTodasFaturas') ?>
                            <fieldset>
                                <div class="col-md-4">
                                    <?php echo $this->Form->input('dt_recebimento', ['required' => true, 'label' => 'Data Recebimento', 'class' => 'datepicker', 'type' => 'text']);?>
                                </div>
                                <div class="col-md-4">
                                    <?php echo $this->Form->input('situacao_recebimento_id', ['required' => true, 'label' => 'Situação', 'options' => $situacaoRecebimentos, 'empty' => 'Selecione']);?>
                                </div>
                                <div class="col-md-4" style="margin-top: 23px">
                                    <?= $this->Form->submit(__('Receber'),['class'=>'btn btn-primary']) ?>
                                </div>

                            </fieldset>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>