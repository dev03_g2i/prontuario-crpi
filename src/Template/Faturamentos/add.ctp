<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Faturamentos</h2>
            <ol class="breadcrumb">
                <li>Faturamentos</li>
                <li class="active">
                    <strong> Cadastrar Faturamentos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturamentos form">
                            <?= $this->Form->create($faturamento) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Faturamento') ?></legend>
                                <?php if(!empty($atend_proc_id)): ?>
                                    <?= $this->Form->input('atendimento_procedimento_id', ['type' => 'hidden', 'value' => $atend_proc_id]);?>
                                <?php else:?>
                                    <div class='col-md-6'>
                                        <?= $this->Form->input('atendimento_procedimento_id', ['data' => 'select', 'controller' => 'atendimentoProcedimentos', 'action' => 'fill']);?>
                                    </div>
                                <?php endif;?>

                                <div class='col-md-6'>
                                    <?= $this->Form->input('numero_autorizacao',['label'=>'Número Autorização']);?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data_autorizacao', ['label'=>'Data autorização','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('numero_guia',['label'=>'Número Guia']);?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data_guia', ['label'=>'Data Emissão Guia','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->button(__('Voltar'), ['type'=>'button','class' => 'btn btn-default','onclick'=>'Navegar(\'\',\'back\')']) ?>
                                <?= $this->Form->button(__('Salvar'), ['type'=>'submit','class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

