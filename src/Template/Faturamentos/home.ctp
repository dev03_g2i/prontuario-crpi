<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Pré-Faturamento</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Faturamento', ['type' => 'get', 'id' => 'form-faturamentos']) ?>

                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_inicio', ['required' => 'required', 'label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('unidade_id', ['label' => 'Unidade', 'options' => $unidades, 'empty' => 'Selecione', 'default' => 1]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('origen_id', ['label' => 'Origem', 'empty' => 'Selecione', 'options' => $origens]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('convenio_id', ['id' => 'convenio_id_faturamento', 'name' => 'convenios[]', 'multiple', 'label' => 'Convênio', 'empty' => 'Selecione', 'options' => $convenios, 'class' => 'select2']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_fim', ['required' => 'required', 'label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('medico_id', ['name' => 'medicos[]', 'multiple', 'label' => 'Médico Executor', 'empty' => 'Selecione', 'options' => $medicos, 'class' => 'select2']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('controle', ['type' => 'text']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_fatura_id', ['label' => 'Situação', 'options' => $situacao_faturas, 'empty' => 'Todos']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                            'options' => $tiposRelatorios,
                            'append' => [
                                $this->Form->button($this->Html->icon('print'), ['id' => 'gerar-report',
                                    'type' => 'button', 'class' => 'btn btn-success',
                                    'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'relatorios',
                                    'action' => 'conferenciaFaturamento'])]
                        ]) ?>
                    </div>
                    <div class='col-md-3'>
                        <?= $this->Form->input('xml', ['label' => 'XML', 'type' => 'select',
                            'options' => $versoesXml,
                            'append' => [
                                $this->Form->button($this->Html->icon('list'), ['id' => 'gerar-xml',
                                    'type' => 'button', 'class' => 'btn btn-warning',
                                    'title' => 'Gerar XML', 'escape' => false, 'controller' => 'relatorios',
                                    'action' => 'faturamentoXml'])]
                        ]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('ordenar_por', ['options' => ['Atendimentos.id' => 'Atendimento', 'Atendimentos.data' => 'Data', 'Clientes.nome' => 'Paciente'], 'empty' => 'Selecione']); ?>
                    </div>
                    <div class="col-md-12 text-center fill-content-search">
                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default btnFiltrar', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default btnRefresh', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        <!--                        <button type="button" class="btn btn-primary m-r-sm atendimento_procedimentos" toggle="tooltip"-->
                        <!--                                , data-placement="bottom" , title="Qtd. Atendimentos">0-->
                        <!--                        </button>-->
                    </div>


                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-2">
                                <label>Qtd. </label><br>
                                <span class="quantidade_reg">0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Tl.Caixa </label><br>
                                <span class="valor_caixa">R$ 0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Tl.Material </label><br>
                                <span class="valor_material">R$ 0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Vl.Total </label><br>
                                <span class="total_geral">R$ 0</span>
                            </div>
                            <div class="col-md-2">
                                <label>Vl.Total + Tl.Material </label><br>
                                <span class="total_geral_com_material">R$ 0</span>
                            </div>
                            <?php if ($this->Faturamento->mostraFaturaEquipe()): ?>
                                <div class="col-md-2">
                                    <label>Vl.Total Equipe</label><br>
                                    <span class="total_equipe">R$ 0</span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-3">
                        <?= $this->Form->button($this->Html->icon('check'), ['name' => 'check[]', 'onclick' => 'marcarDesmarcar()', 'type' => 'button', 'class' => 'btn btn-success', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Marcar/Desmarcar Todos', 'escape' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('conferencia') ?></th>
                                <th><?= $this->Paginator->sort('atendimento') ?></th>
                                <?php if ($this->Faturamento->mostraFaturaEquipe()): ?>
                                    <th><?= $this->Paginator->sort('carater_atendimento') ?></th>
                                <?php endif; ?>
                                <th><?= $this->Paginator->sort('data') ?></th>
                                <!--<th><?= $this->Paginator->sort('horario') ?></th>-->
                                <th><?= $this->Paginator->sort('paciente') ?></th>
                                <th><?= $this->Paginator->sort('convenio') ?></th>
                                <th><?= $this->Paginator->sort('profissional') ?></th>
                                <th><?= $this->Paginator->sort('matricula') ?></th>
                                <th><?= $this->Paginator->sort('codigo') ?></th>
                                <th><?= $this->Paginator->sort('procedimento') ?></th>
                                <th><?= $this->Paginator->sort('guia') ?></th>
                                <th><?= $this->Paginator->sort('senha') ?></th>
                                <th><?= $this->Paginator->sort('quantidade', 'Qtd') ?></th>
                                <th><?= $this->Paginator->sort('percentual_cobranca', 'Perc. Cobrança') ?></th>
                                <th><?= $this->Paginator->sort('valor_fatura', 'Vl.Fatura') ?></th>
                                <th><?= $this->Paginator->sort('valor_caixa', 'Vl.Caixa') ?></th>
                                <th><?= $this->Paginator->sort('valor_material', 'Vl.Material') ?></th>
                                <th><?= $this->Paginator->sort('total') ?></th>
                                <?php if ($this->Faturamento->mostraFaturaEquipe()): ?>
                                    <th><?= $this->Paginator->sort('total_equipe') ?></th>
                                <?php endif; ?>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            if ($atendimentoProcedimentos != null):
                                foreach ($atendimentoProcedimentos as $atendimentoProcedimento): ?>
                                    <tr class="marcar<?= $i ?> <?= ($atendimentoProcedimento->situacao_fatura_id == 2) ? ' bg-success' : '' ?>">
                                        <td><?= $this->Form->checkbox('selecionado', ['data-posicao' => $i, 'name' => 'selecionados[]', 'value' => $atendimentoProcedimento->id, ($atendimentoProcedimento->situacao_fatura_id == 2) ? 'checked' : '']); ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento->id ?></td>
                                        <?php if ($this->Faturamento->mostraFaturaEquipe()): ?>
                                            <td><?= !empty($atendimentoProcedimento->atendimento->internacao_carater_atendimento->descricao) ? $atendimentoProcedimento->atendimento->internacao_carater_atendimento->descricao : '' ?></td>
                                        <?php endif; ?>
                                        <td><?= $this->Time->format($atendimentoProcedimento->atendimento->data, 'dd/MM/YYYY') ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento->cliente->nome ?></td>
                                        <td><?= $atendimentoProcedimento->atendimento->convenio->nome ?></td>
                                        <td><?= $atendimentoProcedimento->medico_responsavei->nome ?></td>
                                        <td><?= $atendimentoProcedimento->matricula ?></td>
                                        <td><?= $atendimentoProcedimento->codigo ?></td>
                                        <td><?= $atendimentoProcedimento->procedimento->nome ?></td>
                                        <!--<td><?= $atendimentoProcedimento->nr_guia ?></td>-->
                                        <td>
                                            <a class="edit_local" href="#" data-type="text"
                                               data-controller="AtendimentoProcedimentos" data-action="parcial-edit-ap"
                                               data-param="text" data-title="nr_guia"
                                               data-pk="<?= $atendimentoProcedimento->id ?>"
                                               data-value="<?= $atendimentoProcedimento->nr_guia ?>"
                                               listen="f"><?= $atendimentoProcedimento->nr_guia ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a class="edit_local" href="#" data-type="text"
                                               data-controller="AtendimentoProcedimentos" data-action="parcial-edit-ap"
                                               data-param="text" data-title="autorizacao_senha"
                                               data-pk="<?= $atendimentoProcedimento->id ?>"
                                               data-value="<?= $atendimentoProcedimento->autorizacao_senha ?>"
                                               listen="f"><?= $atendimentoProcedimento->autorizacao_senha ?>
                                            </a>
                                        </td>
                                        <td><?= $atendimentoProcedimento->quantidade ?></td>
                                        <td><?= $this->Number->toPercentage($atendimentoProcedimento->percentual_cobranca * 100) ?></td>
                                        <td><?= number_format($atendimentoProcedimento->valor_fatura, 2, '.', '') ?></td>
                                        <td><?= number_format($atendimentoProcedimento->valor_caixa, 2, '.', '') ?></td>
                                        <td>
                                            <a class="edit_local" href="#" data-type="text"
                                               data-controller="AtendimentoProcedimentos" data-action="parcial-edit-ap"
                                               data-param="decimal" data-title="valor_material"
                                               data-pk="<?= $atendimentoProcedimento->id ?>"
                                               data-value="<?= number_format($atendimentoProcedimento->valor_material, 2, '.', '') ?>"
                                               listen="f">
                                                <?= number_format($atendimentoProcedimento->valor_material, 2, '.', '') ?>
                                            </a>
                                        </td>
                                        <td><?= number_format($atendimentoProcedimento->total, 2, '.', '') ?></td>
                                        <?php if ($this->Faturamento->mostraFaturaEquipe()): ?>
                                            <td><?= number_format($atendimentoProcedimento->total_equipe, 2, '.', '') ?></td>
                                        <?php endif; ?>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button"
                                                        data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Abrir atendimento', ['controller' => 'Atendimentos', 'action' => 'edit', $atendimentoProcedimento->atendimento_id], ['escape' => false, 'target' => '_blank']) ?></li>
                                                    <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Mat/Med - Fatura', ['controller' => 'FaturaMatmed', 'action' => 'index', $atendimentoProcedimento->atendimento_id], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?></li>
                                                    <li><?= $this->Html->link('Guia SADT', [], ['escape' => false]) ?></li>
                                                    <li><?= $this->Html->link('Guia outras despesas', [], ['escape' => false]) ?></li>
                                                    <li class="<?= $this->Configuracao->usaEquipe(); ?>">
                                                        <?= $this->Html->link('<i class="fa fa-users"></i> Equipe', ['controller' => 'AtendimentoProcedimentoEquipes', 'action' => 'index', $atendimentoProcedimento->id], ['escape' => false, 'target' => '_blank']); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; endforeach; endif; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?php if ($atendimentoProcedimentos): ?>
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="append-report hidden">

</div>
<?php echo $this->Html->script('controllers/Faturamentos',['block' => 'scriptBottom']);?>