<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Faturamento anual</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area no-padding m-t-25">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Selecione o fim do periodo</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="filtros ">
                                <?= $this->Form->create('Estatisticas', ['type' => 'get','id'=>'form-estatisticas']) ?>
                                <div class="col-md-3">
                                    <?= $this->Form->input('mes',['label' => 'Mês', 'type'=>'select', 'options' => $meses, 'required' => 'required', 'value' => $formatedDateMes]);?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Form->input('ano', ['label' => 'Ano', 'type' => 'select', 'options' => $anos, 'required' => 'required', 'value' => $formatedDateAno]); ?>
                                </div>
                                <div class="col-md-12" style="margin-top: 28px">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill-annual-chart', 'class' => 'btn btn-default', 'escape' => false]) ?>
                                    <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                            </div>
                        </div>
                    </div>
                    <div class="" id="bodyCharts" style="visibility: hidden; display: none;">
                        <div class="table-responsive">
                            <div class="ibox ibox-content float-e-margins">
                                <div class="ibox-title">
                                    <h5>Valores dos ultimos 12 meses</h5>
                                </div>
                                <div class="ibox-content">
                                    <div id="graficoDiv">
                                        <canvas id="annualChartLine"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title col-sm-12">
                                            <div class="col-sm-7 no-padding">
                                                <strong>R$ Não faturado</strong>
                                            </div>
                                            <div class="col-sm-offset-1 col-sm-4 no-padding text-right">
                                                <span style="border-radius: 7%; background-color: #b4dbe4; padding: 1px 10px 1px 10px; color: white;"><strong>Anual</strong></span>
                                            </div>
                                        </div>
                                        <div class="ibox-content col-sm-12">
                                            <div class="col-sm-4 no-padding">
                                                <h2 id="valorNaoFaturado"></h2>
                                                <small>Não faturado</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title col-sm-12">
                                            <div class="col-sm-7 no-padding">
                                                <strong>Qt Não faturado</strong>
                                            </div>
                                            <div class="col-sm-offset-1 col-sm-4 no-padding text-right">
                                                <span style="border-radius: 7%; background-color: #bae5dc; padding: 1px 10px 1px 10px; color: white;"><strong>Anual</strong></span>
                                            </div>
                                        </div>
                                        <div class="ibox-content col-sm-12">
                                            <div class="col-sm-4 no-padding">
                                                <h2 id="qtNaoFaturado"></h2>
                                                <small>Qt Não faturado</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title col-sm-12">
                                            <div class="col-sm-7 no-padding">
                                                <strong>Pct não faturado</strong>
                                            </div>
                                            <div class="col-sm-offset-1 col-sm-4 no-padding text-right">
                                            <span style="border-radius: 7%; background-color: #e4b4b4; padding: 1px 10px 1px 10px; color: white;"><strong>Anual</strong></span>
                                            </div>
                                        </div>
                                        <div class="ibox-content col-sm-12">
                                            <div class="col-sm-4 no-padding">
                                                <h2 id="porcentualNaoFaturado"></h2>
                                                <small>Pct não faturado</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>