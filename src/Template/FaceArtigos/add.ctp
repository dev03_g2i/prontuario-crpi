<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estoque</h2>
            <ol class="breadcrumb">
                <li>Dispensação</li>
                <li class="active">
                    <strong>Cadastrar</strong>
                </li>
            </ol>
        </div>
        <div class="text-right">
            <a href="Javascript:void(0)" onclick="Navegar('','back')" class="btn btn-default btn-xs"><?= $this->Html->icon('arrow-left') ?></a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faceArtigos form">
                            <?= $this->Form->create($faceArtigo,['id'=>'form-artigos']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Face Artigo') ?></legend>
                                <?php


                                if(!empty($face_itens)){
                                    echo $this->Form->input('face_itens_id', ['type' => 'hidden', 'value' => $face_itens->id]);
                                }else{
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('face_itens_id', ['data' => 'select', 'controller' => 'faceItens', 'action' => 'fill']);
                                    echo "</div>";
                                }
                                if(!empty($gastos)){
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('procedimento_gasto_id', ['label'=>'Ficha/Kit','empty' => 'selecione','options'=>$gastos,'data-controller'=>'FaceArtigos']);
                                    echo "</div>";
                                }else {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('procedimento_gasto_id', ['label'=>'Ficha/Kit','data' => 'select', 'controller' => 'procedimentoGastos', 'action' => 'fill', 'empty' => 'selecione']);
                                    echo "</div>";
                                }

                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('artigo_id',['id'=>'artigo_id','data' => 'select', 'controller' => 'EstqArtigo', 'action' => 'fill', 'empty' => 'selecione','data-controller'=>'FaceArtigos']);
                                echo "</div>";

                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('quantidade',['value'=>1]);
                                echo "</div>";


                                echo "<div class='ibox float-e-margins'>";
                                echo "<div class='ibox-title'>";
                                echo "<h5>Artigos</h5>";
                                echo "</div>";

                                echo "<div class='ibox-content'>";
                                echo '<div class="table-responsive list-artigos col-md-12" data-controller="FaceArtigos">
                                                 <table class="table table-hover "></table>
                                              </div>';
                                echo "<div class='clearfix'></div>";
                                echo "</div>";
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['type'=>'button','class' => 'btn btn-primary','id'=>'check_artigos']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

