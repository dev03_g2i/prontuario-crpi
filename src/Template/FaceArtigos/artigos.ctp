<div class="arts">
    <div class="table-responsive">
        <table class="table table-hover ">
            <tr>
                <th>Codigo</th>
                <th>Artigo</th>
                <th>Quantidade</th>
                <th>&nbsp;</th>
            </tr>
            <?php
            foreach ($artigos as $r => $item) {
                echo '<td>' . $item['artigo_codigo'] . '</td>';
                echo '<td>' . $item['artigo_nome'] . '</td>';
                echo '<td><a class="edit_local" href="#" data-type="text" data-controller="FaceArtigos" data-param="text" data-title="quantidade" data-pk="'.$r.'" data-value="'.$item['quantidade'].'" listen="f">'.$item['quantidade'].'</a></td>';
                echo '<td>';
                    echo $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['data' => 'deletar-art','data-controller'=>'FaceArtigos','data-id'=>$r, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar artigo', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']);
                echo '</td>';
                echo '</tr>';
            }
            ?>
        </table>
        <input type="hidden" id="quantidade_artigos" value="<?= count($artigos)?>" />
    </div>
</div>
