
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Face Artigos</h2>
        <ol class="breadcrumb">
            <li>Face Artigos</li>
            <li class="active">
                <strong>Litagem de Face Artigos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Face Artigos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Face Iten') ?></th>
                                                                <td><?= $faceArtigo->has('face_iten') ? $this->Html->link($faceArtigo->face_iten->id, ['controller' => 'FaceItens', 'action' => 'view', $faceArtigo->face_iten->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Procedimento Gasto') ?></th>
                                                                <td><?= $faceArtigo->has('procedimento_gasto') ? $this->Html->link($faceArtigo->procedimento_gasto->id, ['controller' => 'ProcedimentoGastos', 'action' => 'view', $faceArtigo->procedimento_gasto->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faceArtigo->has('user') ? $this->Html->link($faceArtigo->user->nome, ['controller' => 'Users', 'action' => 'view', $faceArtigo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faceArtigo->has('situacao_cadastro') ? $this->Html->link($faceArtigo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faceArtigo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faceArtigo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Artigo Id') ?></th>
                                <td><?= $this->Number->format($faceArtigo->artigo_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($faceArtigo->quantidade) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faceArtigo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faceArtigo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


