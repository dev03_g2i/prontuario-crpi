<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tratamentos</h2>
            <ol class="breadcrumb">
                <li>Tratamentos</li>
                <li class="active">
                    <strong>Procedimentos/Faces</strong>
                </li>
            </ol>
        </div>
        <?php if($is_prontuario): ?>
            <div class="text-right">
                <a href="Javascript:void(0)" onclick="Navegar('','back')" class="btn btn-default btn-xs"><?= $this->Html->icon('arrow-left') ?></a>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Tratamentos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Procedimento</th>
                                        <th>Tipo</th>
                                        <th>Descrição</th>
                                        <th>Faces</th>
                                        <th>Profissional</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                        foreach ($atendimento_itens->face_itens as $f):
                                            echo '<tr>';
                                                echo '<td>'.$atendimento_itens->atendimento_procedimento->procedimento->nome.'</td>';
                                                echo '<td>'.(($atendimento_itens->tipo=='d') ? 'Dente' :'Região').'</td>';
                                                echo '<td>'.($atendimento_itens->has('dente') ? $atendimento_itens->dente->descricao :$atendimento_itens->regio->descricao).'</td>';
                                                echo '<td>'.($f->has('face') ? $f->face->nome :'Sem face').'</td>';
                                                echo '<td>'.$atendimento_itens->atendimento_procedimento->medico_responsavei->nome.'</td>';
                                                echo '<td class="actions">'. $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'FaceArtigos','action' => 'index','?'=>['faceiten_id'=>$f->id,'first'=>1,'ajax'=>1,'historico'=>$is_historico]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Estoque/Dispensação', 'escape' => false, 'class' => 'btn btn-xs btn-info']).'</td>';
                                            echo '</tr>';
                                        endforeach;
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>