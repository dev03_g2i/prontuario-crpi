<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estoque</h2>
            <ol class="breadcrumb">
                <li>Dispensação</li>
                <li class="active">
                    <strong>Listagem</strong>
                </li>
            </ol>
        </div>
        <div class="text-right">
            <a href="Javascript:void(0)" onclick="Navegar('','back')" class="btn btn-default btn-xs"><?= $this->Html->icon('arrow-left') ?></a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Dispensação', ['action' => 'add', '?' => ['faceiten_id' => $faceiten_id]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Dispensação', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Lista') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('artigo_id',['Código Artigo']) ?></th>
                                        <th><?= $this->Paginator->sort('artigo_id') ?></th>
                                        <th><?= $this->Paginator->sort('quantidade') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <?php if($is_historico): ?>
                                        <th><?= __('Vincular') ?></th>
                                        <?php endif; ?>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faceArtigos as $faceArtigo): ?>
                                        <tr>
                                            <td><?= $faceArtigo->has('artigo') ? $this->Html->link($faceArtigo->artigo->codigo_livre, ['controller' => 'EstqArtigo', 'action' => 'view', $faceArtigo->artigo->id]) : '' ?></td>
                                            <td><?= $faceArtigo->has('artigo') ? $this->Html->link($faceArtigo->artigo->nome, ['controller' => 'EstqArtigo', 'action' => 'view', $faceArtigo->artigo->id]) : '' ?></td>
                                            <td><?= $this->Number->format($faceArtigo->quantidade) ?></td>
                                            <td><?= h($faceArtigo->created) ?></td>
                                            <td><?= h($faceArtigo->modified) ?></td>
                                        <?php if($is_historico): ?>
                                            <td>
                                                <label>
                                                    <input type="checkbox" name="vincular_face" value="<?= $faceArtigo->id ?>" <?= in_array($faceArtigo->id,$vinculos) ? 'checked' : '' ?>>
                                                </label>
                                            </td>
                                        <?php endif; ?>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $faceArtigo->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'faceArtigos\',' . $faceArtigo->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
