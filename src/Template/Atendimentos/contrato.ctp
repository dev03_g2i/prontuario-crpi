<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimentos</h2>
        <ol class="breadcrumb">
            <li>Atendimentos</li>
            <li class="active">
                <strong>Lista de Contratos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?= $this->Form->create($atendimento,['type'=>'get','target'=>'_blanck','url'=>['controller'=>'Contratos','action'=>'imprimir.pdf?']]) ?>
                    <fieldset>
                        <?php
                            echo $this->Form->input('atendimento_id', ['type' =>'hidden','value'=>$atendimento->id]);
                        echo "<div class='col-md-12'>";
                            echo $this->Form->input('contrato_id', ['options' =>$contrato, 'empty' => 'Selecione','required']);
                        echo "</div>";
                        echo '<div class="col-md-12 text-right">';
                            echo $this->Form->submit(__('Imprimir'), ['type'=>'submit','class' => 'btn btn-primary']);
                        echo "</div>";
                        ?>

                    </fieldset>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>