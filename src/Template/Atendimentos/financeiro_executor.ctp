<div class="this-place">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Atendimentos</h2>
            <ol class="breadcrumb">
                <li>Atendimentos</li>
                <li class="active">
                    <strong>Procedimentos/Atendimentos</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Atendimentos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Atendimento</th>
                                        <th>Procedimento</th>
                                        <th>Tipo</th>
                                        <th>Descrição</th>
                                        <th>Faces</th>
                                        <th>Profissional</th>
                                        <th>Status</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    foreach ($atendimentos as $atendimento):
                                        if (!empty($atendimento->atendimento_procedimentos)):
                                            foreach ($atendimento->atendimento_procedimentos as $anted):
                                                foreach ($anted->atendimento_itens as $iten_cobranca): ?>
                                                    <tr>
                                                        <td><?= h($atendimento->id) ?></td>
                                                        <td><?= h($anted->procedimento->nome) ?></td>
                                                        <?php
                                                        if ($iten_cobranca->tipo == 'd') {
                                                            echo '<td>Dentes</td>';
                                                            echo '<td>';
                                                            echo $iten_cobranca->has('dente') ? $this->Html->link($iten_cobranca->dente->descricao, ['controller' => 'Dentes', 'action' => 'view', $iten_cobranca->dente->id]) : '';
                                                            echo '</td>';
                                                        } else if ($iten_cobranca->tipo == 'r') {
                                                            echo '<td>Região</td>';
                                                            echo '<td>';
                                                            echo $iten_cobranca->has('regio') ? $this->Html->link($iten_cobranca->regio->nome, ['controller' => 'Regioes', 'action' => 'view', $iten_cobranca->regio->id]) : '';
                                                            echo '</td>';
                                                        }

                                                        echo '<td>';
                                                        $aux = 0;
                                                        $is_face = 0;
                                                        if (!empty($iten_cobranca->face_itens)) {
                                                            $is_face=1;
                                                            $face = "";
                                                            foreach ($iten_cobranca->face_itens as $f) {
                                                                $aux++;
                                                                $face .= $f->face->nome . ",";
                                                            }
                                                            echo substr($face, 0, strlen($face) - 1);
                                                        } else {
                                                            echo "Sem faces";
                                                        }
                                                        echo '</td>';

                                                        $controller_artigos = $is_face==0 ? 'AtendimentoitenArtigos' : 'FaceArtigos';
                                                        $action_artigos= $is_face==0 ? 'index' : 'all';

                                                        ?>
                                                        <td><?= $anted->has('medico_responsavei') ? $anted->medico_responsavei->nome : '' ?></td>
                                                        <td><a class="fin_tratamento" href="#" data-type="select" data-controller="Atendimentos" data-title="finalizado" data-pk="<?= $atendimento->id ?>" data-value="<?= $atendimento->finalizado ?>" listen="f"><?= $atendimento->finalizado==0 ?'Em andamento' :'Finalizado'; ?></a></td>
                                                        <td class="actions">
                                                            <?= $this->Html->link('<i class="fa fa-stack-exchange"></i>', ['controller'=>$controller_artigos,'action' => $action_artigos, '?'=>['atendimentoiten_id'=>$iten_cobranca->id,'ajax'=>1,'pront'=>1,'historico'=>$is_historico]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Estoque/Dispensação', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                            <?= $this->Html->link('<i class="fa fa-book"></i>', ['action' => 'executar', $anted->id,'?'=>['aba'=>'financeiro','historico'=>$is_historico]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Financeiro', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                                            <?= $this->Html->link('<i class="fa fa-book"></i>', ['action' => 'executar', $anted->id,'?'=>['aba'=>'executor','historico'=>$is_historico]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Executor', 'escape' => false, 'class' => 'btn btn-xs btn-warning']) ?>
                                                        </td>

                                                    </tr>
                                                <?php endforeach;
                                            endforeach;
                                        endif;
                                    endforeach;
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>