<div class="this-place">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" id="tabs-executores">
        <li role="presentation" class="<?= $aba=='financeiro'? 'active' :''; ?>">
            <a href="#aba-financ" aria-controls="aba-financ" tipo="f" role="tab-exec"
               action="resp-financeiro" data-toggle="tab" data-id="<?= $atendimento_procedimento->id ?>"
               controller="Atendimentos" data-historico="<?=$is_historico?>" >Financeiro</a>
        </li>
        <li role="presentation" class="<?= $aba=='executor'? 'active' :''; ?>">
            <a href="#aba-executor" aria-controls="aba-executor" role="tab-exec" tipo="e"
               action="resp-executor" data-toggle="tab" data-id="<?= $atendimento_procedimento->id ?>"
               controller="Atendimentos" data-historico="<?=$is_historico?>">Executor</a>
        </li>
        <div class="text-right">
            <a href="Javascript:void(0)" onclick="Navegar('','back')" class="btn btn-default btn-xs"><?= $this->Html->icon('arrow-left') ?></a>
        </div>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?= $aba=='financeiro'? 'active' :''; ?>" id="aba-financ">
        </div>
        <div role="tabpanel" class="tab-pane <?= $aba=='executor'? 'active' :''; ?>" id="aba-executor">
        </div>
    </div>
</div>

