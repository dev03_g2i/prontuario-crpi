<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Editar Atendimento</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-lg-12">

            <div class="atendimentos form">
                <?= $this->Form->create($atendimento) ?>
                <fieldset>

                    <div class='ibox float-e-margins'>
                        <div class='ibox-title'>
                            <h5>Dados</h5>
                        </div>
                        <div class='ibox-content'>
                            <div class='col-md-12'>
                                <div class='col-md-2'>
                                    <?php echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($atendimento->data, 'dd-MM-Y')]); ?>
                                </div>
                                <div class='col-md-2'>
                                    <?php echo $this->Form->input('hora', ['type' => 'text', 'class' => 'timepiker', 'value' => $this->Time->format($atendimento->hora, 'HH:m')]); ?>
                                </div>
                                <div class='col-md-2'>
                                    <?php echo $this->Form->input('unidade_id', ['options' => $unidades, 'default' => 1, 'required' => 'required']); ?>
                                </div>
                                <div class='col-md-2'>
                                    <?php echo $this->Form->input('tipoatendimento_id', ['options' => $tipoAtendimentos, 'required' => 'required', 'label' => 'Tipo Atendimento', 'id' => 'tipo_atendimento_id_input']); ?>
                                </div>
                                <div class='col-md-2'>
                                    <?php echo $this->Form->input('configuracao_periodo_id', ['options' => $configPeriodos, 'label' => 'Período']); ?>
                                </div>
                                <div class='col-md-2'>
                                    <?php echo $this->Form->input('origen_id', ['options' => $origens, 'label' => 'Origem', 'empty' => 'selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?php
                                    if ($atendimento->acertado == 1) {
                                        echo $this->Form->input('cliente_id', ['label' => 'Paciente', 'empty' => 'selecione', 'data-cliente' => 'cliente', 'options' => $clientes, 'readonly',
                                            'append' => [
                                                $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Clientes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Paciente']),
                                                $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Clientes', 'action' => 'nview', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Paciente'])
                                            ]
                                        ]);
                                    } else {
                                        echo $this->Form->input('cliente_id', ['label' => 'Paciente', 'empty' => 'selecione', 'data-cliente' => 'cliente', 'data-up' => 'cliente', 'options' => $clientes, 'get' => 'convenio',
                                            'append' => [
                                                $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Clientes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Paciente']),
                                                $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Clientes', 'action' => 'nview', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Paciente'])
                                            ]
                                            , 'required' => 'required'
                                        ]);
                                    }
                                    ?>
                                </div>
                                <div class='col-md-3'>
                                    <?php echo $this->Form->input('solicitante_id', ['type' => 'select', 'data' => 'select', 'data-value' => $atendimento->solicitante_id, 'controller' => 'Solicitantes', 'action' => 'fill', 'required' => 'required', 'empty' => 'Selecione',
                                        'append' => [
                                            $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f']),
                                            $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Solicitantes', 'action' => 'view', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Solicitante'])
                                        ]
                                    ]); ?>
                                </div>
                                <div class='col-md-3'>
                                    <?php echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'selecione', 'required' => 'required']); ?>
                                </div>
                                <div class='col-md-12'>
                                    <?php echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2]); ?>
                                </div>
                            </div>
                            <div class='clearfix'></div>
                        </div>
                    </div>

                    <?php if ($this->Configuracao->mostraAreaInternacao()): ?>
                        <div class="ibox float-e-margins" id="inputs_internacao">
                            <div class="ibox-content">
                                <div class="row">
                                    <?php if ($this->Configuracao->mostraCampoCaraterAtendimentoOnAtendimentos()): ?>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('carater_atendimento_id', ['options' => $caraterAtendimentos, 'value' => $atendimento->carater_atendimento_id]); ?>
                                        </div>
                                    <?php else: ?>
                                        <?php echo $this->Form->input('carater_atendimento_id', ['type' => 'hidden', 'value' => $this->Configuracao->getValorCaraterAtendimentoPadrao()]); ?>
                                    <?php endif; ?>

                                    <?php if ($this->Configuracao->mostraInternacaoAcomodacaoOnAtendimentos()): ?>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('internacao_acomoda_id', ['label' => 'Internação Acomodação', 'options' => $acomodacoes, 'value' => $atendimento->internacao_acomoda_id]); ?>
                                        </div>
                                    <?php else: ?>
                                        <?php echo $this->Form->input('internacao_acomodacao_id', ['type' => 'hidden', 'value' => $this->Configuracao->getValorInternacaoAcomodacaoPadrao()]); ?>
                                    <?php endif; ?>

                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('internacao_previa', ['label' => 'Internação Prévia', 'options' => ['Sim' => 'Sim', 'Não' => 'Não'], 'default' => 'Não', 'value' => $atendimento->internacao_previa, 'required' => 'required']); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('internacao_previa_data', ['label' => 'Internação Prévia Data', 'type' => 'text', 'class' => 'datepicker', 'value' => (empty($atendimento->internacao_previa_data) ? '' : date_format($atendimento->internacao_previa_data, 'd/m/Y'))]); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('internacao_data_hora_entrada', ['label' => 'Internação Dt/Hr Entrada', 'type' => 'text', 'class' => 'datetimepicker', 'value' => (empty($atendimento->internacao_data_hora_entrada) ? '' : date_format($atendimento->internacao_data_hora_entrada, 'd/m/Y H:i'))]); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('internacao_setor_id', ['label' => 'Internação Setor', 'options' => $internacaoSetores, 'value' => $atendimento->internacao_setor_id, 'empty' => 'Selecione']); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('internacao_leito_id', ['label' => 'Internação Leito', 'options' => $internacaoLeitos, 'value' => $atendimento->internacao_leito_id, 'empty' => 'Selecione']); ?>
                                    </div>

                                    <?php if ($this->Configuracao->mostraMotivoAltaOnAtendimentos()): ?>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('internacao_motivo_alta_id', ['label' => 'Internação Motivo Alta', 'options' => $motivosAlta, 'value' => $atendimento->internacao_motivo_alta_id, 'empty' => 'Selecione']); ?>
                                        </div>
                                    <?php else: ?>
                                        <?php echo $this->Form->input('internacao_motivo_alta_id', ['type' => 'hidden', 'value' => $this->Configuracao->getValorMotivoAltaPadrao()]); ?>
                                    <?php endif; ?>

                                    <div class="col-md-4">
                                        <?php echo $this->Form->input('internacao_data_hora_saida', ['label' => 'Internação Dt/Hr Saída', 'type' => 'text', 'class' => 'datetimepicker', 'value' => (empty($atendimento->internacao_data_hora_saida) ? '' : date_format($atendimento->internacao_data_hora_saida, 'd/m/Y H:i'))]); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <div class='clearfix'></div>

                    <div class='ibox float-e-margins'>
                        <div class='ibox-title'>
                            <h5>Procedimentos</h5>
                            <div class="text-right">
                                <p>
                                    <?php //echo $this->Html->link('<i class="fa fa-plus"></i> Novo Procedimento', ["controller"=>"AtendimentoProcedimentos","action" => "nadd",$atendimento->id,'?'=>['first'=>1,'ajax'=>1]], ["toggle" => "tooltip", "data-placement" => "bottom", "title" => "Cadastrar Procedimento", "class" => "btn btn-primary btn-sm dim", "escape" => false,"data-toggle"=>"modal","data-target"=>"#modal_lg"]);?>
                                    <?php echo $this->Form->button('<i class="fa fa-plus"></i> Procedimento', ['type' => 'button', 'class' => 'btn btn-primary btn-sm dim', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Adicionar Procedimento', 'id' => 'plus-procedimento', 'action' => 'edit', 'atendimento_id' => $atendimento->id]); ?>
                                </p>
                            </div>
                        </div>
                        <div class='ibox-content'>
                            <div class='col-lg-12'>
                                <div class="edit-dep lista-procedimentos"></div>
                            </div>
                            <div class='clearfix'></div>
                        </div>
                        <div class="ibox-content">
                            <div class="col-lg-12">
                                <p class="text-left" style="padding: 10px; margin-top: -15px"><strong><span
                                                class="total-liquido"></span></strong></p>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>


                    <div class='clearfix'></div>
                    <div class='ibox float-e-margins'>
                        <div class='ibox-title'>
                            <h5>Valores</h5>
                        </div>
                        <div class='ibox-content'>
                            <div class='col-md-12'>
                                <?php
                                $options = ['places' => 2];
                                ?>
                                <div class='col-md-4'>
                                    <?php echo $this->Form->input('total_liquido', ['label' => 'Total Líquido', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?php echo $this->Form->input('total_pagoato', ['label' => 'Total Recebido', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                </div>
                                <div class='col-md-4 text-danger'>
                                    <?php echo $this->Form->input('total_areceber', ['label' => 'Total a receber', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true, 'value' => $this->Number->format($atendimento->total_areceber, $options)]); ?>
                                </div>
                                <div class='col-md-12'>
                                    <?php
                                    echo '<br /><small>Cadastrado por:<strong>' . $atendimento->user->nome . '</strong>  Em:<strong>' . $this->Time->format($atendimento->created, 'dd/MM/YYYY HH:mm') . '</strong></small><br />';
                                    if (!empty($atendimento->user_edit)) {
                                        echo '<small>Alterado por:<strong>' . $atendimento->user_edit->nome . '</strong>  Em:<strong>' . $this->Time->format($atendimento->modified, 'dd/MM/YYYY HH:mm') . '</strong></small>';
                                    }
                                    ?>
                                </div>

                                <?php echo $this->Form->input('orc', ['type' => 'hidden', 'value' => $atendimento->id]); ?>
                                <div class="col-md-12 text-right">
                                    <div class='col-md-1 pull-right'>
                                        <?php
                                        if (!empty($agenda)) {
                                            echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary', 'onclick' => 'SendForm(\'form\')']);
                                        } else {
                                            echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false]);
                                        }
                                        ?>
                                    </div>

                                    <div class='col-md-2 pull-right'>
                                        <?php if (!empty($agenda)) {
                                            echo $this->Html->link('<i class="fa fa-money"></i> Financeiro', ['controller' => 'contasreceber', 'action' => 'index', '?' => ['id' => $atendimento->id]], ['id' => 'btnOpenFinanceiro', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Financeiro', 'escape' => false, 'class' => 'btn btn-danger']);
                                        } else {
                                            echo $this->Html->link('<i class="fa fa-money"></i> Financeiro', ['controller' => 'contasreceber', 'action' => 'index', '?' => ['id' => $atendimento->id, 'first' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Financeiro', 'escape' => false, 'class' => 'btn btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']);
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class='clearfix'></div>
                        </div>
                    </div>
                </fieldset>
                <?= $this->Form->end() ?>
            </div>
        </div>


    </div>

    <?php
    /* Ao abrir por modal não funciona o scriptBlock do cakephp */
    if (!empty($agenda)):?>
        <script>
            $(function () {
                atualizarProcedimentos('<?=$atendimento->id?>');
            });
        </script>
    <?php else:
        echo $this->Html->scriptBlock(
            ' $(function(){
                atualizarProcedimentos(' . $atendimento->id . ');
              });',
            ['inline' => true, 'block' => 'scriptLast']
        );
    endif;
    ?>
</div>