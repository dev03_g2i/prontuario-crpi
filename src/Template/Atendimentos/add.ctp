<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cadastrar Atendimento</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="atendimentos form">
                <?= $this->Form->create($atendimento, ['id' => 'frm-atendimentos']) ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => (empty($agenda) ? date('d/m/Y') : date_format($agenda->inicio, 'd/m/Y'))]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('hora', ['type' => 'text', 'class' => 'timepiker', 'value' => (empty($agenda) ? date('H:s') : $this->Time->format($agenda->inicio, 'HH:mm'))]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('unidade_id', ['options' => $unidades, 'default' => 1, 'required' => 'required']); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('tipoatendimento_id', ['options' => $tipoAtendimentos, 'required' => 'required', 'label' => 'Tipo Atendimento', 'default' => 1, 'id' => 'tipo_atendimento_id_input']); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('origen_id', ['options' => $origens, 'label' => 'Origem', 'empty' => 'selecione']); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php
                                            echo $this->Form->input('cliente_id', ['label' => 'Paciente', 'type' => 'select', 'empty' => 'selecione', 'data-up' => 'cliente', 'data' => 'select', 'controller' => 'Clientes', 'local' => 'atendimento', 'action' => 'fill', 'data-value' => (empty($agenda) ? '' : $agenda->cliente_id),
                                                'append' => [
                                                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Clientes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Paciente']),
                                                    $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Clientes', 'action' => 'nview', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Paciente'])
                                                ],
                                                'required' => 'required'
                                            ]);
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('solicitante_id', ['type' => 'select', 'data' => 'select', 'controller' => 'Solicitantes', 'action' => 'fill', 'required' => 'required', 'empty' => 'Selecione', 'data-value' => $solicitante_id,
                                                'append' => [
                                                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f']),
                                                    $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Solicitantes', 'action' => 'view', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Solicitante'])
                                                ]
                                            ]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'selecione', 'required' => 'required', 'default' => (empty($agenda->convenio_id) ? null : $agenda->convenio_id)]); ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2, 'value' => (empty($agenda) ? null : $agenda->observacao)]);
                                            echo $this->Form->input('tipo', ['type' => 'hidden', 'value' => 2]);
                                            echo $this->Form->input('agenda_id', ['type' => 'hidden', 'value' => (!empty($agenda) ? $agenda->id : null)]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if ($this->Configuracao->mostraAreaInternacao()): ?>
                    <div class="row" id="inputs_internacao">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div class="row">
                                        <?php if ($this->Configuracao->mostraCampoCaraterAtendimentoOnAtendimentos()): ?>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('carater_atendimento_id', ['options' => $caraterAtendimentos, 'empty' => 'Selecione']); ?>
                                            </div>
                                        <?php else: ?>
                                            <?php echo $this->Form->input('carater_atendimento_id', ['type' => 'hidden', 'value' => $this->Configuracao->getValorCaraterAtendimentoPadrao()]); ?>
                                        <?php endif; ?>

                                        <?php if ($this->Configuracao->mostraInternacaoAcomodacaoOnAtendimentos()): ?>
                                            <div class="col-md-4">
                                                <?php echo $this->Form->input('internacao_acomoda_id', ['label' => 'Internação Acomodação', 'options' => $acomodacoes, 'empty' => 'Selecione']); ?>
                                            </div>
                                        <?php else: ?>
                                            <?php echo $this->Form->input('internacao_acomoda_id', ['type' => 'hidden', 'value' => $this->Configuracao->getValorInternacaoAcomodacaoPadrao()]); ?>
                                        <?php endif; ?>

                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('internacao_previa', ['label' => 'Internação Prévia', 'options' => ['Sim' => 'Sim', 'Não' => 'Não'], 'default' => 'Não']); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('internacao_previa_data', ['label' => 'Internação Prévia Data', 'type' => 'text', 'class' => 'datepicker']); ?>
                                        </div>

                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('internacao_data_hora_entrada', ['label' => 'Internação Dt/Hr Entrada', 'type' => 'text', 'class' => 'datetimepicker']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="col-md-6">
                                    <h5>Procedimentos</h5>
                                </div>
                                <div class="col-md-6 text-right">
                                    <?php echo $this->Form->button('<i class="fa fa-plus"></i> Procedimento', ['type' => 'button', 'class' => 'btn btn-primary btn-sm dim', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Adicionar Procedimento', 'id' => 'plus-procedimento', 'action' => 'add']); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="list-dep scrol-procedimentos lista-procedimentos"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Valores</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <!--<div class="col-md-6">
                                            <?php echo $this->Form->input('total_geral', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo $this->Form->input('desconto', ['prepend' => 'R$', 'type' => 'hidden', 'mask' => 'money']); ?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo $this->Form->input('percentual_desconto', ['prepend' => '%', 'type' => 'hidden']); ?>
                                        </div>-->
                                        <div class='col-md-6'>
                                            <?php echo $this->Form->input('total_liquido', ['label' => 'Total Líquido', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?php echo $this->Form->input('total_pagoato', ['label' => 'Total Recebido', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <?php
                                            if (!empty($agenda)) {
                                                echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'type' => 'submit', 'onclick' => 'return SendAtendimento(\'frm-atendimentos\')']);
                                            } else {
                                                echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['escape' => false, 'type' => 'submit', 'class' => 'btn btn-primary btnSalveAtendimento', 'onclick' => 'return check_procedimentos()']);
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <?= $this->Form->end() ?>
            </div>

        </div>
    </div>

    <?php
    echo $this->Html->scriptBlock(
        ' $(function(){
                $.ListProcedimentosAdd();
            });',
        ['inline' => false, 'block' => 'scriptLast']
    );
    ?>

</div>