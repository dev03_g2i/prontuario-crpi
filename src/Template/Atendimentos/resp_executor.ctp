
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Procedimento</th>
                                    <th>Tipo</th>
                                    <th>Descrição</th>
                                    <th>Face</th>
                                    <th>Executor</th>
                                    <th>Data cadastro</th>
                                    <th>Quem cadastrou</th>
                                    <th>Finalizado</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($atendimento_procedimento->atendimento_itens as $itens):
                                if(!empty($itens->face_itens)) {
                                foreach ($itens->face_itens as $f) {
                                    ?>
                                    <tr>
                                        <td><?= h($atendimento_procedimento->procedimento->nome) ?></td>
                                        <?php
                                        if ($itens->tipo == 'd') {
                                            echo '<td>Dentes</td>';
                                            echo '<td>';
                                            echo $itens->has('dente') ? $itens->dente->descricao : '';
                                            echo '</td>';
                                        } else if ($itens->tipo == 'r') {
                                            echo '<td>Região</td>';
                                            echo '<td>';
                                            echo $itens->has('regio') ? $itens->regio->nome : '';
                                            echo '</td>';
                                        }
                                        ?>
                                        <td><?=$f->face->nome ?></td>
                                        <td><a class="edit_resp" href="#" data-type="select2" data-controller="Atendimentos" data-origem="e" data-param="select" data-find="MedicoResponsaveis" medico-id="<?=$atendimento_procedimento->medico_responsavei->id; ?>" medico-nome="<?=$atendimento_procedimento->medico_responsavei->nome; ?>" data-tipo="face" data-title="executor_id" data-pk="<?= $f->id ?>" data-value="<?=!empty($f->executor->nome) ? $f->executor->nome : '' ?>" listen="f"><?= !empty($f->executor->nome) ? $f->executor->nome : 'Não vinculado'; ?></a></td>
                                        <td><?=$f->data_executor?></td>
                                        <td><?= $f->has('user__executor') ? $f->user__executor->nome : '' ?></td>
                                        <td><a class="<?= empty($f->financeiro->nome) ? '' : 'edit_resp_status' ?>" href="Javascript:void(0)" data-type="select" data-controller="Atendimentos" data-origem="e" data-tipo="face" data-title="finalizado" data-pk="<?= $f->id ?>" data-value="<?=$f->finalizado==1 ? 1 :0 ?>" listen="f"><?= $f->finalizado==1 ?'Sim' :'Não'; ?></a></td>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="executor_iten" data-tipo="face" value="<?= $f->id ?>" <?= in_array($f->id,$executor_face) ? 'checked' : '' ?> <?= empty($f->executor->nome) ? 'disabled' : '' ?> >
                                            </label>
                                        </td>
                                    </tr>
                            <?php }}else{
                                    ?>
                                    <tr>
                                        <td><?= h($atendimento_procedimento->procedimento->nome) ?></td>
                                        <?php
                                        if($itens->tipo=='d'){
                                            echo '<td>Dentes</td>';
                                            echo '<td>';
                                            echo $itens->has('dente') ? $itens->dente->descricao : '';
                                            echo '</td>';
                                        }else if($itens->tipo=='r'){
                                            echo '<td>Região</td>';
                                            echo '<td>';
                                            echo $itens->has('regio') ? $itens->regio->nome : '';
                                            echo '</td>';
                                        }
                                        ?>
                                        <td>Sem Faces</td>
                                        <td><a class="edit_resp" href="#" data-type="select2" data-controller="Atendimentos" data-param="select" data-find="MedicoResponsaveis" data-origem="e" data-tipo="iten" data-title="executor_id" data-pk="<?= $itens->id ?>" medico-id="<?=$atendimento_procedimento->medico_responsavei->id; ?>" medico-nome="<?=$atendimento_procedimento->medico_responsavei->nome; ?>" data-value="<?=!empty($itens->executor->nome) ?$itens->executor->nome : '';  ?>" listen="f"><?= !empty($itens->executor->nome) ? $itens->executor->nome : 'Não vinculado'; ?></a></td>
                                        <td><?= $itens->data_executor?></td>
                                        <td><?= $itens->has('user__executor') ? $itens->user__executor->nome : '' ?></td>
                                        <td><a class="<?= empty($itens->financeiro->nome) ? '' : 'edit_resp_status' ?>" href="Javascript:void(0)" data-type="select" data-controller="Atendimentos" data-param="select" data-find="MedicoResponsaveis" data-origem="e" data-tipo="iten" data-title="status" data-pk="<?= $itens->id ?>" data-value="<?=$itens->status  ?>" listen="f"><?= $itens->status==1 ?'Sim' :'Não'; ?></a></td>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="executor_iten" data-tipo="iten" value="<?= $itens->id ?>" <?= in_array($itens->id,$executor_iten) ? 'checked' : '' ?> <?= empty($itens->executor->nome) ? 'disabled' : '' ?>>
                                            </label>
                                        </td>
                                    </tr>
                                <?php }
                                endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>