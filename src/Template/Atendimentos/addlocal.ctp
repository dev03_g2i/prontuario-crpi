<div class="itens">
    <div class="table-responsive">
        <table class="table table-hover ">
            <tr>
                <th>&nbsp;</th>
                <th>Descrição</th>
            </tr>
            <?php
            foreach ($faces as $face): ?>
            <tr>
                <td>
                   <label class="checkbox-inline">
                     <input type="checkbox" name="faces" id="face-<?= $face->id ?>" value="<?=$face->id ?>" class="iten-faces">

                   </label>
                </td>
                <td>
                    <?= $face->nome ?>
                </td>
            </tr>

            <?php endforeach; ?>

        </table>
    </div>
</div>
<input type="hidden" class="id-session" value="<?= $id_session ?>" />
<input type="hidden" class="id-iten" value="<?= $id_iten ?>" />