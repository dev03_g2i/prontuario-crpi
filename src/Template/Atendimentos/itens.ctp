<div class="itens">
    <div class="table-responsive">
        <table class="table table-hover ">
            <tr>
                <th>&nbsp;</th>
                <th>Tipo</th>
                <th>Descrição</th>
            </tr>
            <?php
            foreach ($itens as $item): ?>
            <tr>
                <td>
                   <label class="checkbox-inline">
                     <input type="checkbox" name="itens[]" id="iten-<?= $item->id ?>" value="<?=$item->tipo.'-'.$item->id ?>" class="aten-itens item">
                   </label>
                </td>
                <?php
                if($item->tipo=='d'){
                    echo '<td>Dentes</td>';
                    echo '<td>';
                    echo $item->has('dente') ? $item->dente->descricao: '';
                    echo '</td>';
                }else if($item->tipo=='r'){
                    echo '<td>Região</td>';
                    echo '<td>';
                    echo $item->has('regio') ? $item->regio->nome: '';
                    echo '</td>';
                }
            echo '</tr>';
            endforeach;
            ?>
        </table>
    </div>
</div>
