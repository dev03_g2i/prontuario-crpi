<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimentos</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div>
                    <?= $this->Form->create('Atendimentos', ['type' => 'get','id' => 'atendimento-form']) ?>
                    <div class='col-md-4'>
                        <?php echo $this->Form->input('inicio', ['label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-4'>
                        <?php echo $this->Form->input('fim', ['label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-4'>
                        <?php echo $this->Form->input('convenio_id', ['name' => 'convenios', 'label' => 'Convênio', 'options' => $convenios, 'class' => 'select2', 'value' => $convenios[29]]); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-4'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <?php echo $this->Form->input('isSus', ['type' => 'hidden', 'value' => 1]); ?>

                    <div class="col-md-4">
                    <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                            'options' => $tiposRelatorios,
                            'append' => [
                                $this->Form->button($this->Html->icon('print'), ['onclick' => 'geraRelatorioPrecoProcedimentoSUS($(this))',
                                    'type' => 'button', 'class' => 'btn btn-info',
                                    'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'atendimentos',
                                    'action' => 'callsReports'])
                                    ]
                            ]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="append-report hidden">

</div>
<?php
echo $this->Html->script('controllers/Atendimentos', ['block' => 'scriptBottom']);
?>