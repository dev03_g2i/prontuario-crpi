<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3>Atendimentos</h3>
                <ol class="breadcrumb">
                    <li>Atendimentos</li>
                    <li class="active">
                        <strong>Lista de Atendimentos</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros ">
                    <?= $this->Form->create('Atendimentos',['type'=>'get']) ?>
                    <div class="col-md-4">
                        <?= $this->Form->input('clientes',['type'=>'select','data-cliente'=>"cliente",'empty'=>'Selecione','options'=>[@$clientes->id=>@$clientes->nome],'default'=>@$clientes->id,'label'=>'Paciente']);?>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->input('convenio_id',['type'=>'select','empty'=>'Selecione','options'=>$convenio]);?>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->input('tipoatendimento_id',['type'=>'select','empty'=>'Selecione','options'=>$tipoatendimento]);?>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->input('data',['type'=>'text','class'=>"datepicker",'append' => [$this->Form->button($this->Html->icon("calendar"),['type'=>'button'])]]);?>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->input('acertado',['options'=>[0=>"Não",1=>"Sim"],'empty'=>'selecione']);?>
                    </div>
                    <div class="col-md-4">
                        <?= $this->Form->input('tipo',['options'=>[0=>"Orçamento",1=>"Atendimento"],'empty'=>'selecione']);?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-4" >
                        <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>


        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Atendimentos') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('id',['label'=>'Código']) ?></th>
                                <th><?= $this->Paginator->sort('tipo') ?></th>
                                <th><?= $this->Paginator->sort('data') ?></th>
                                <th><?= $this->Paginator->sort('cliente_id') ?></th>
                                <th><?= $this->Paginator->sort('total_liquido') ?></th>
                                <th><?= $this->Paginator->sort('desconto') ?></th>
                                <th><?= $this->Paginator->sort('total_geral') ?></th>
                                <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                <th><?= $this->Paginator->sort('total_pagoato') ?></th>
                                <th><?= $this->Paginator->sort('tipoatendimento_id', ['label' => 'Tipo atendimento']) ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($atendimentos as $atendimento): ?>
                                <tr>
                                    <td><?= $atendimento->id ?></td>
                                    <td><?= $this->Time->format($atendimento->data, 'd/MM/Y') ?></td>
                                    <td><?= ($atendimento->tipo) ? 'Atendimento' :'Orçamento' ?></td>
                                    <td><?= $atendimento->has('cliente') ? $this->Html->link($atendimento->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $atendimento->cliente->id]) : '' ?></td>
                                    <td><?= $this->Number->format($atendimento->total_liquido,['places' => 2,'before' => 'R$ ','escape' => false,'decimals' => '.','thousands' => ',']) ?></td>
                                    <td><?= $this->Number->format($atendimento->desconto,['places' => 2,'before' => 'R$ ','escape' => false,'decimals' => '.','thousands' => ',']) ?></td>
                                    <td><?= $this->Number->format($atendimento->total_geral,['places' => 2,'before' => 'R$ ','escape' => false,'decimals' => '.','thousands' => ',']) ?></td>
                                    <td><?= $atendimento->has('convenio') ? $this->Html->link($atendimento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $atendimento->convenio->id]) : '' ?></td>
                                    <td><?= $this->Number->format($atendimento->total_pagoato,['places' => 2,'before' => 'R$ ','escape' => false,'decimals' => '.','thousands' => ',']) ?></td>
                                    <td><?= $atendimento->has('tipo_atendimento') ? $this->Html->link($atendimento->tipo_atendimento->nome, ['controller' => 'TipoAtendimentos', 'action' => 'view', $atendimento->tipo_atendimento->id]) : '' ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link('<i class="fa fa-file-archive-o"></i>', ['action' => 'view', $atendimento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes do Atendimento', 'escape' => false, 'class' => 'btn btn-xs btn-warning']) ?>
                                        <?= $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'edit', $atendimento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                        <?= $this->Form->postLink('<i class="fa fa-times"></i>', ['action' => 'delete', $atendimento->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $atendimento->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



