<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimentos</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Atendimentos', ['type' => 'get','id' => 'atendimento-form']) ?>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('inicio', ['required' => 'required', 'label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('fim', ['required' => 'required', 'label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('unidade_id', ['label' => 'Unidade', 'options' => $unidades, 'empty' => 'Selecione', 'default' => 1]); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('convenio_id', ['name' => 'convenios[]', 'multiple', 'label' => 'Convênio', 'empty' => 'Selecione', 'options' => $convenios, 'class' => 'select2']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('medico_id', ['name' => 'medicos[]', 'multiple', 'label' => 'Médico Executor', 'empty' => 'Selecione', 'options' => $medicos, 'class' => 'select2']); ?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('controle', ['type' => 'text']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('ordenar_por', ['options' => ['Atendimentos.id' => 'Atendimento', 'Atendimentos.data' => 'Data', 'Clientes.nome' => 'Paciente'], 'empty' => 'Selecione']); ?>
                    </div>
                    <div class="col-md-3">
                  <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                            'options' => $tiposRelatorios,
                            'append' => [
                                $this->Form->button($this->Html->icon('print'), ['onclick' => 'geraRelatorioPrecoProcedimento($(this))',
                                    'type' => 'button', 'class' => 'btn btn-info',
                                    'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'atendimentos',
                                    'action' => 'callsReports'])
                                    ]
                            ]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 text-center fill-content-search">
                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default btnRefresh', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        <!--                        <button type="button" class="btn btn-primary m-r-sm atendimento_procedimentos" toggle="tooltip"-->
                        <!--                                , data-placement="bottom" , title="Qtd. Atendimentos">0-->
                        <!--                        </button>-->
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('id', 'N° A.') ?></th>
                                <th><?= $this->Paginator->sort('grupo') ?></th>
                                <th><?= $this->Paginator->sort('exame') ?></th>
                                <th><?= $this->Paginator->sort('paciente') ?></th>
                                <th><?= $this->Paginator->sort('convenio') ?></th>
                                <th><?= $this->Paginator->sort('res_nome', 'Médico') ?></th>
                                <th><?= $this->Paginator->sort('entrega') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            if ($atendimentos != null):
                                foreach ($atendimentos as $atendimento): ?>
                                    <tr class="marcar<?= $i ?> <?= ($atendimento->situacao_fatura_id == 2) ? ' bg-success' : '' ?>">
                                        <td><?= $atendimento->id ?></td>
                                        <td><?= $atendimento->grupo ?></td>
                                        <td><?= $atendimento->exame ?></td>
                                        <td><?= $atendimento->paciente ?></td>
                                        <td><?= $atendimento->convenio ?></td>
                                        <td><?= $atendimento->res_nome ?></td>
                                        <td><?= $atendimento->entrega ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button"
                                                        data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link('Ver/Editar', ['controller' => 'Atendimentos', 'action' => 'edit', $atendimento->id], ['escape' => false, 'target' => '_blank']) ?></li>
                                                    <!-- <li><?= $this->Html->link('Deletar', ['controller' => 'Atendimentos', 'action' => 'delete', $atendimento->id], ['escape' => false]); ?></li> -->
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; endforeach; endif; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?php if ($atendimentos): ?>
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="append-report hidden">

</div>
<?php
echo $this->Html->script('controllers/Atendimentos', ['block' => 'scriptBottom']);
?>