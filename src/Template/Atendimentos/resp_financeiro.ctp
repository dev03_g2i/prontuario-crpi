
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Procedimento</th>
                                    <th>Tipo</th>
                                    <th>Descrição</th>
                                    <th>Face</th>
                                    <th>Financeiro</th>
                                    <th>Data cadastro</th>
                                    <th>Quem cadastrou</th>
                                    <th>Finalizado</th>
                                    <th>Vincular</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($atendimento_procedimento->atendimento_itens as $itens):
                                if(!empty($itens->face_itens)) {
                                foreach ($itens->face_itens as $f) {
                                    ?>
                                    <tr>
                                        <td><?= h($atendimento_procedimento->procedimento->nome) ?></td>
                                        <?php
                                        if ($itens->tipo == 'd') {
                                            echo '<td>Dentes</td>';
                                            echo '<td>';
                                            echo $itens->has('dente') ? $itens->dente->descricao : '';
                                            echo '</td>';
                                        } else if ($itens->tipo == 'r') {
                                            echo '<td>Região</td>';
                                            echo '<td>';
                                            echo $itens->has('regio') ? $itens->regio->nome : '';
                                            echo '</td>';
                                        }
                                        ?>
                                        <td><?=$f->face->nome ?></td>
                                        <td><a class="edit_resp" href="#" data-type="select2" data-origem="f" data-controller="Atendimentos" data-param="select" data-find="MedicoResponsaveis" medico-id="<?=$atendimento_procedimento->medico_responsavei->id; ?>" medico-nome="<?=$atendimento_procedimento->medico_responsavei->nome; ?>" data-tipo="face" data-title="financeiro_id" data-pk="<?= $f->id ?>" data-value="<?=!empty($f->financeiro->nome) ? $f->financeiro->nome : '' ?>" listen="f"><?= !empty($f->financeiro->nome) ? $f->financeiro->nome : 'Não vinculado'; ?></a></td>
                                        <td><?=$f->data_financeiro ?></td>
                                        <td><?= $f->has('user__financeiro') ? $f->user__financeiro->nome : '' ?></td>
                                        <td><a class="<?= empty($f->financeiro->nome) ? '' : 'edit_resp_status' ?>" href="Javascript:void(0)" data-type="select" data-origem="f" data-controller="Atendimentos" data-tipo="face" data-title="finalizado" data-pk="<?= $f->id ?>" data-value="<?=$f->finalizado==1 ? 1 :0 ?>" listen="f"><?= $f->finalizado==1 ?'Sim' :'Não'; ?></a></td>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="financeiro_iten" data-tipo="face" value="<?= $f->id ?>" <?= in_array($f->id,$financeiro_face) ? 'checked' : '' ?> <?= empty($f->financeiro->nome) ? 'disabled' : '' ?> >
                                            </label>
                                        </td>
                                    </tr>
                            <?php }}else{
                                    ?>
                                    <tr>
                                        <td><?= h($atendimento_procedimento->procedimento->nome) ?></td>
                                        <?php
                                        if($itens->tipo=='d'){
                                            echo '<td>Dentes</td>';
                                            echo '<td>';
                                            echo $itens->has('dente') ? $itens->dente->descricao : '';
                                            echo '</td>';
                                        }else if($itens->tipo=='r'){
                                            echo '<td>Região</td>';
                                            echo '<td>';
                                            echo $itens->has('regio') ? $itens->regio->nome : '';
                                            echo '</td>';
                                        }
                                        ?>
                                        <td>Sem Faces</td>
                                        <td><a class="edit_resp" href="#" data-type="select2" data-controller="Atendimentos" data-origem="f" data-param="select" data-find="MedicoResponsaveis" medico-id="<?=$atendimento_procedimento->medico_responsavei->id; ?>" medico-nome="<?=$atendimento_procedimento->medico_responsavei->nome; ?>" data-tipo="iten" data-title="financeiro_id" data-pk="<?= $itens->id ?>" data-value="<?=!empty($itens->financeiro->nome) ?$itens->financeiro->nome : ''  ?>" listen="f"><?= !empty($itens->financeiro->nome) ? $itens->financeiro->nome : 'Não vinculado'; ?></a></td>
                                        <td><?= $itens->data_financeiro ?></td>
                                        <td><?= $itens->has('user__financeiro') ? $itens->user__financeiro->nome : '' ?></td>
                                        <td><a class="<?= empty($itens->financeiro->nome) ? '' : 'edit_resp_status' ?>" href="Javascript:void(0)" data-type="select" data-origem="f" data-controller="Atendimentos" data-param="select" data-find="MedicoResponsaveis" data-tipo="iten" data-title="status" data-pk="<?= $itens->id ?>" data-value="<?=$itens->status  ?>" listen="f"><?= $itens->status==1 ?'Sim' :'Não'; ?></a></td>
                                        <td>
                                            <label>
                                                <input type="checkbox" name="financeiro_iten" data-tipo="iten" value="<?= $itens->id ?>" <?= in_array($itens->id,$financeiro_iten) ? 'checked' : '' ?> <?= empty($itens->financeiro->nome) ? 'disabled' : '' ?>>
                                            </label>
                                        </td>
                                    </tr>
                                <?php }
                                endforeach; ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>