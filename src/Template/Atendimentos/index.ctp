<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3>Atendimentos</h3>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros ">
                    <?= $this->Form->create('Atendimentos', ['type' => 'get']) ?>
                        <div class="col-md-4">
                            <?= $this->Form->input('numero_atendimento', ['label' => 'N° Atendimento']); ?>
                        </div>
                        <div class="col-md-4">
                            <!-- ID AtendimentoProcedimento -->
                            <?= $this->Form->input('exame_integracao', ['label' => 'Exame/Integração', 'type' => 'text']); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('clientes', ['type' => 'select', 'data-cliente' => "cliente", 'empty' => 'Selecione', 'options' => [@$clientes->id => @$clientes->nome], 'default' => @$clientes->id, 'label' => 'Paciente']); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('convenio_id', ['type' => 'select', 'empty' => 'Selecione', 'options' => $convenio]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('tipoatendimento_id', ['type' => 'select', 'empty' => 'Selecione', 'options' => $tipoatendimento]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('data', ['type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('acertado', ['options' => [2 => "Não", 1 => "Sim"], 'empty' => 'selecione']); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('finalizado', ['label' => 'Status', 'options' => [3 => "Todos", 2 => "Em andamento", 1 => "Finalizado"], 'empty' => 'selecione', 'default' => 2]); ?>
                        </div>
                        <div class="col-md-12 fill-content-search">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                    <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['class' => 'btn btn-primary', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo', 'target' => '_blank', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
        <div class="ibox float-e-margins">
            <div class="ibox-title">
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <?= $this->Paginator->sort('id', ['label' => 'Código']) ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('exame', ['label' => 'Exame']) ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('data') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('cliente_id', 'Paciente') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('total_liquido') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('total_areceber', 'Total a Receber') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('convenio_id') ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('tipoatendimento_id', ['label' => 'Tipo']) ?>
                                    </th>
                                    <th>
                                        <?= $this->Paginator->sort('finalizado', ['label' => 'Status']) ?>
                                    </th>
                                    <th class="actions">
                                        <?= __('Ações') ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($atendimentos as $atendimento): ?>
                                <tr>
                                    <td>
                                        <?= $atendimento->id ?>
                                    </td>
                                    <td>
                                        <?= (!empty($atendimento->atendimento_procedimentos)) ? $atendimento->atendimento_procedimentos[0]->id : '' ?>
                                    </td>
                                    <td>
                                        <?= $atendimento->data ?>
                                    </td>
                                    <td>
                                        <?= $atendimento->has('cliente') ? $this->Html->link($atendimento->cliente->nome, ['controller' => 'Clientes', 'action' => 'nview', $atendimento->cliente->id]) : '' ?>
                                    </td>
                                    <td>
                                        <?= $this->Number->format($atendimento->total_liquido, ['places' => 2, 'before' => 'R$ ', 'escape' => false, 'decimals' => '.', 'thousands' => ',']) ?>
                                    </td>
                                    <td class="text-danger">
                                        <?= $this->Number->format($atendimento->total_areceber, ['places' => 2, 'before' => 'R$ ', 'escape' => false, 'decimals' => '.', 'thousands' => ',']) ?>
                                    </td>
                                    <td>
                                        <?= $atendimento->has('convenio') ? $this->Html->link($atendimento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $atendimento->convenio->id]) : '' ?>
                                    </td>
                                    <td>
                                        <?= $atendimento->has('tipo_atendimento') ? $this->Html->link($atendimento->tipo_atendimento->nome, ['controller' => 'TipoAtendimentos', 'action' => 'view', $atendimento->tipo_atendimento->id]) : '' ?>
                                    </td>
                                    <td>
                                        <a class="fin_tratamento" href="#" data-type="select" data-controller="Atendimentos"
                                            data-title="finalizado" data-pk="<?= $atendimento->id ?>" data-value="<?= !empty($atendimento->finalizado) ? 1 : 0 ?>"
                                            listen="f">
                                            <?= $atendimento->finalizado == 0 ? 'Em andamento' : 'Finalizado'; ?>
                                        </a>
                                    </td>

                                    <td class="actions">

                                        <div class="col-md-8 col-lg-8 text-right">
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                                                    <span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li>
                                                        <?php /* echo $this->Html->link('<i class="fa fa-file-text-o"></i> Contratos', ['controller'=>'ClienteContratos','action' => 'index','?'=> ['ClienteContratos__atendimento_id'=>$atendimento->id,'first'=>1]], ['escape' => false, 'class' => ($atendimento->acertado==1 ? "" : "disabled"),'data-toggle'=>'modal','data-target'=>'#modal_lg',]) */ ?>
                                                        <?= $this->Html->link('<i class="fa fa-print"></i> Ficha', 'javascript:void(0)', ['escape' => false, 'onclick' => "checkConfiFicha($atendimento->id)"]) ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-file-archive-o"></i> Visualizar', ['action' => 'view', $atendimento->id], ['escape' => false, 'target' => '_blank']) ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-pencil"></i> Editar', ['action' => 'edit', $atendimento->id], ['escape' => false, 'target' => '_blank']) ?>
                                                    </li>
													<?php  if ($_SESSION['Auth']['User']['grupo_id'] == 1) : ?>
														<li>
															<?= $this->Html->link('<i class="fa fa-remove"></i> Cancelar', 'javascript:void(0)', ['onclick' => "cancelaAtendimento($atendimento->id)", 'escape' => false, 'listen' => 'f']) ?>
														</li>
													<?php  endif; ?>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Nota Fiscal', 'javascript:void(0)', ['onclick' => "verificaNfs($atendimento->id)", 'escape' => false]) ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-medkit"></i> Laudo', ['controller' => 'laudos', 'action' => 'index', '?' => ['atendimento_id' => $atendimento->id]], ['escape' => false, 'target' => '_blank']) ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-user-md"></i> Mat/Med - Fatura', ['controller' => 'FaturaMatmed', 'action' => 'index', $atendimento->id], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?>
                                                    </li>
                                                    <?php if($this->Configuracao->habilitarEstoque()): ?>
                                                        <li>
                                                            <?= $this->Html->link('<i class="fa fa-user-md"></i> Mat/Med - Dispensação', ['controller' => 'EstqSaida', 'action' => 'index', $atendimento->id], ['escape' => false, 'target' => '_blank']); ?>
                                                        </li>
                                                    <?php endif; ?>
                                                    <li>
                                                        <?php
                                                            if($this->Configuracao->hablitaAtendimentoDocs()): 
                                                                echo $this->Html->link('<i class="fa fa-file-text-o"></i> Documentos', ['controller' => 'AtendimentoDocumentos', 'action' => 'index', $atendimento->id], ['escape' => false, 'target' => '_blank']);
                                                            endif;    
                                                        ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-2">
                                            <?= $this->Html->link('<i class="fa fa-money"></i>', ['controller' => 'contasreceber', 'action' => 'index', '?' => ['id' => $atendimento->id, 'first' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Financeiro', 'load-script' => 'Contasreceber', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                        </div>

                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                                <p>
                                                    <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                                </p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>