<div class="arts">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Procedimento</th>
                <th>Código</th>
                <th>Caixa</th>
                <th>Desconto R$</th>
                <th>Profissional</th>
                <th>Complemento</th>
                <th class="<?= $this->Configuracao->showControleFinanceiroOnAtendimentos() ?>">Controle</th>
                <th>Qtd</th>
                <th class="<?= $this->Configuracao->showPrevEntrega() ?>">Prev.Entrega</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php $options = ['places' => 2, 'before' => 'R$ '];
            foreach ($itens as $r => $item): ?>
                <tr class="<?= $item['regra'] ?>">
                    <td><?= $item['procedimento'] ?></td>
                    <td><?= (!empty($item['codigo']) ? $item['codigo'] : 'Não encontrado') ?></td>
                    <td><?= $this->Number->format($item['valor_caixa'], $options); ?></td>
                    <td><?= $this->Number->format($item['desconto'], $options); ?></td>
                    <td><?= $item['medico_nome'] ?></td>
                    <td><?= @$item['complemento'] ?></td>
                    <td class="<?= $this->Configuracao->showControleFinanceiroOnAtendimentos() ?>">
                        <?= $item['controle'] ?></td>
                    <td><?= $item['quantidade'] ?></td>
                    <td class="<?= $this->Configuracao->showPrevEntrega() ?>"><?= $this->Time->format($item['prev_entrega_proc'], 'dd/MM/YYYY HH:mm'); ?></td>
                    <td>
                        <a href="Javascript:void(0)" onclick="editProcedimento(<?= $r ?>, 'edit_add')"
                           class="btn btn-info btn-xs" listen="f"><i class="fa fa-pencil"></i></a>
                        <a href="Javascript:void(0)" onclick="dell_iten(<?= $r ?>)" class="btn btn-danger btn-xs"
                           listen="f"><i class="fa fa-times"></i></a>

                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    <input type="hidden" id="id_procedimento" value="<?= $id_procedimento ?>"/>
    <input type="hidden" id="total_procedimentos" value="<?= count($itens) ?>"/>
</div>
