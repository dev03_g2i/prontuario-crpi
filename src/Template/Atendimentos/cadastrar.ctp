<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Atendimentos</h2>
            <!--<ol class="breadcrumb">
                <li>Atendimentos</li>
                <li class="active">
                    <strong> Cadastrar Atendimentos</strong>
                </li>
            </ol>-->
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="atendimentos form">
                <?= $this->Form->create($atendimento, ['id' => 'frm-atendimentos']) ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Dados</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => (empty($agenda) ? date('d/m/Y') : $this->Time->format($agenda->inicio, 'dd/MM/Y'))]);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('hora', ['type' => 'text', 'class' => 'timepiker', 'value' => (empty($agenda) ? date('H:s') : $this->Time->format($agenda->inicio, 'HH:mm'))]);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('unidade_id', ['options' => $unidades, 'default' => 1, 'required' => 'required']);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('tipoatendimento_id', ['options' => $tipoAtendimentos, 'required' => 'required', 'label' => 'Tipo Atendimento']);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php
                                            echo $this->Form->input('cliente_id', ['label'=>'Paciente','type' => 'select', 'empty' => 'selecione', 'data' => 'select','data-up'=>'cliente', 'controller' => 'Clientes', 'local' => 'atendimento', 'action' => 'fill', 'get'=>'convenio','data-value' => (empty($agenda) ? '' : $agenda->cliente_id),
                                                'append' => [
                                                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Clientes', 'action' => 'nadd'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f'])
                                                ],
                                                'required' => 'required'
                                            ]);
                                            ?>
                                        </div>
                                        <div class="col-md-4">

                                            <?php echo $this->Form->input('solicitante_id', ['type'=>'select','data' => 'select','controller'=>'Solicitantes', 'action'=>'fill', 'required' => 'required','empty'=>'Selecione',
                                                'append' => [
                                                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f'])
                                                ]
                                            ]);?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'selecione', 'required' => 'required', 'default' => (empty($agenda->convenio_id) ? null : $agenda->convenio_id)]);?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2, 'value' => (empty($agenda) ? null : $agenda->observacao)]);
                                            echo $this->Form->input('tipo', ['type' => 'hidden', 'value' => 2]);
                                            echo $this->Form->input('agenda_id', ['type' => 'hidden', 'value' => (!empty($agenda) ? $agenda->id : null)]);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <div class="col-md-6">
                                    <h5>Procedimentos</h5>
                                </div>
                                <div class="col-md-6 text-right">
                                    <?php echo $this->Form->button('<i class="fa fa-plus"></i> Procedimento',['type'=>'button','class'=>'btn btn-primary btn-sm dim','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Adicionar Procedimento','id'=>'plus-procedimento']); ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12" style='margin-bottom: 10px;margin-top: 10px'>
                                        <div class="table-responsive list-dep col-md-12">
                                            <table class="table table-hover">
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Valores</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-md-6">
                                            <?php echo $this->Form->input('total_geral', ['prepend' => 'R$', 'type' => 'hidden', 'mask' => 'money']);?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo $this->Form->input('desconto', ['prepend' => 'R$', 'type' => 'hidden', 'mask' => 'money']);?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo $this->Form->input('percentual_desconto', ['prepend' => '%', 'type' => 'hidden']);?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?php echo $this->Form->input('total_pagoato', ['label' => 'Total Recebido', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]);?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo $this->Form->input('total_liquido', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 col-md-offset-8 text-right">
                                        <?php  echo $this->Form->button('<i class="fa fa-close"></i> Cancelar', ['class' => 'btn btn-danger', 'type' => 'button', 'onclick' => 'Navegar(\'\',\'back\')']);?>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'type' => 'submit', 'onclick' => 'return SendAtendimento(\'frm-atendimentos\')']);?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <?= $this->Form->end() ?>
            </div>

        </div>
    </div>
</div>
<?php
echo $this->Html->scriptBlock(
    ' $(function(){
            $.ListProcedimentosAdd();
        });',
    ['inline' => false, 'block' => 'scriptLast']
);
?>
