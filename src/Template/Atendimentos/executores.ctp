<div class="this-place">
    <div class="white-bg ">
        <div class="col-lg-5">
            <div class="col-md-12">
                <h2>Financeiro/Executor</h2>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist" id="tabs-executores">
                            <li role="presentation" class="active">
                                <a href="#aba-financeiro" aria-controls="aba-financeiro" tipo="f" role="tab"
                                   action="resp-financeiro" data-toggle="tab"
                                   controller="Atendimentos">Financeiro</a>
                            </li>
                            <li role="presentation">
                                <a href="#aba-executor" aria-controls="aba-executor" role="tab" tipo="e"
                                   action="resp-executor" data-toggle="tab"
                                   controller="Atendimentos">Executor</a>
                            </li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="aba-financeiro"></div>
                            <div role="tabpanel" class="tab-pane" id="aba-executor"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var filtro = {
            atendimento_procedimento: '<?=$atendimento_procedimento->id?>',
            ajax: 1,
            tipo: 'f'
        }
        $(function () {
            carregarAba2("aba-financeiro", "Atendimentos", "resp-financeiro", filtro);
            $("[role='tab']").click(function () {
                filtro.tipo = $(this).attr('tipo')
                carregarAba2($(this).attr('aria-controls'), $(this).attr('controller'), $(this).attr('action'), filtro);
            });
            $('body').on('click', ".area_pront a", function (event) {
                if (empty($(this).attr('listen'))) {
                    $("div#" + $(".area_pront").parent('.active').attr('id')).loadGrid(this.href, null, ".area_pront", loadComponentes);
                    return false;
                }
            })
        })


    </script>

</div>