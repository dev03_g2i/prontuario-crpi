<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-8">
            <h2>Atendimentos</h2>
            <ol class="breadcrumb">
                <li>Atendimentos</li>
                <li class="active">
                    <strong> Editar Atendimento</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">
            <?php
            echo '<br /><small>Cadastrado por:<strong>' . $atendimento->user->nome . '</strong>  Em:<strong>' . $atendimento->created . '</strong></small><br />';
            if (!empty($atendimento->user_edit)) {
                echo '<small>Alterado por:<strong>' . $atendimento->user_edit->nome . '</strong>  Em:<strong>' . $atendimento->modified . '</strong></small>';
            }
            ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row area">
        <div class="col-lg-12">

            <div class="atendimentos form">
                <?= $this->Form->create($atendimento, ['id' => 'form-edit-atendimento']) ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Dados Cliente</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($atendimento->data, 'dd-MM-Y')]);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('hora', ['type' => 'text', 'class' => 'timepiker', 'value' => $this->Time->format($atendimento->hora, 'HH:m')]);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('unidade_id', ['options' => $unidades, 'default' => 1, 'required' => 'required']);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('tipoatendimento_id', ['options' => $tipoAtendimentos, 'required' => 'required', 'label' => 'Tipo Atendimento']);?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php
                                            if ($atendimento->acertado == 1) {
                                                echo $this->Form->input('cliente_id', ['label' => 'Paciente', 'empty' => 'selecione', 'data-cliente' => 'cliente', 'options' => $clientes, 'readonly'
                                                ]);
                                            } else {
                                                echo $this->Form->input('cliente_id', ['label' => 'Paciente', 'empty' => 'selecione', 'data-cliente' => 'cliente', 'data-up' => 'cliente', 'options' => $clientes, 'get' => 'convenio',
                                                    'append' => [
                                                        $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Clientes', 'action' => 'nadd'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f'])
                                                    ], 'required' => 'required'
                                                ]);
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-4">
                                            <?php echo $this->Form->input('solicitante_id', ['type'=>'select','data' => 'select','data-value'=>$atendimento->solicitante_id,'controller'=>'Solicitantes', 'action'=>'fill', 'required' => 'required','empty'=>'Selecione',
                                                'append' => [
                                                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f'])
                                                ]
                                            ]);?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'selecione', 'required' => 'required']);?>
                                        </div>
                                        <div class="col-md-6">
                                            <?php echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2]);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Médico/Procedimentos</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="col-md-3">
                                            <?php echo $this->Form->input('medico_id', ['options' => $medicos, 'empty' => 'selecione', 'required' => 'required', 'label' => 'Profissional']);?>
                                        </div>
                                        <div class="col-md-3">
                                            <?php echo $this->Form->input('procedimento_id', ['type' => 'select', 'data' => 'select', 'controller' => 'Procedimentos', 'action' => 'fill']);?>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo $this->Form->input('complemento', ['type' => 'text']);?>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo $this->Form->input('controle', ['type' => 'text']);?>
                                        </div>
                                        <div class="col-md-1">
                                            <?php echo $this->Form->input('quantidade', ['label' => 'Qtd', 'type' =>'number', 'default' => 1, 'min' => 1]);?>
                                        </div>
                                        <div class="col-md-1" style='margin-top: 23px'>
                                            <?php echo $this->Form->button('<i class="fa fa-plus"></i>', ['type' => 'button', 'class' => 'btn btn-primary btn-sm dim', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Adicionar Procedimento', 'id' => 'btn-edit-procedimento']);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Procedimentos</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-lg-12" style='margin-bottom: 10px;margin-top: 10px'>
                                                <div class="table-responsive edit-dep  col-md-12"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Valores</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="col-md-6">
                                                    <?php echo $this->Form->input('total_geral', ['prepend' => 'R$', 'type' => 'hidden', 'mask' => 'money', 'readonly' => true]);?>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php echo $this->Form->input('desconto', ['prepend' => 'R$', 'type' => 'hidden', 'mask' => 'money']);?>
                                                </div>
                                                <div class="col-md-3">
                                                    <?php echo $this->Form->input('percentual_desconto', ['prepend' => '%', 'type' => 'hidden']);?>
                                                </div>
                                                <div class='col-md-6'>
                                                    <?php echo $this->Form->input('total_pagoato', ['label' => 'Total Recebido', 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]);?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $this->Form->input('total_liquido', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'readonly' => true]); ?>
                                                </div>
                                                <div class="col-md-2 col-md-offset-8 text-right">
                                                    <?php echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary', 'onclick' => 'SendForm(\'form\')']);?>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php echo $this->Html->link('<i class="fa fa-money"></i> Financeiro', ['controller' => 'contasreceber', 'action' => 'index', '?' => ['id' => $atendimento->id, 'first' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Financeiro', 'escape' => false, 'class' => 'btn btn-danger', 'data-navegar' => 'go']);?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $atendimento->id, 'id'=>'atualiza-atendimento']);?>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>

                </div>
            </div>
        </div>
</div>
<script>
    $(function () {
        atualizar('<?=$atendimento->id?>');
    });
</script>
