<div class="white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimentos</h2>
        <ol class="breadcrumb">
            <li>Atendimentos</li>
            <li class="active">
                <strong>Detalhes do Atendimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">

                <div class="row">
                    <div class="col-md-4">
                        <div class='ibox-title'>
                            <h5>Dados Cliente</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="vertical-table">
                                    <tr>
                                        <th style="min-width: 120px;"><?= __('Código') ?></th>
                                        <td><?= $this->Number->format($atendimento->id) ?></td>
                                    </tr>
                                    <tr>
                                        <th><?= __('Nome') ?></th>
                                        <td><?= $atendimento->has('cliente') ? $this->Html->link($atendimento->cliente->nome, ['controller' => 'Clientes', 'action' => 'nview', $atendimento->cliente->id], ['target' => '_blank']) : '' ?></td>
                                    </tr>
                                    <tr>
                                        <th><?= __('Convênio') ?></th>
                                        <td><?= $atendimento->has('convenio') ? $this->Html->link($atendimento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $atendimento->convenio->id]) : '' ?></td>
                                    </tr>

                                    <tr>
                                        <th><?= __('Solicitante') ?></th>
                                        <td><?= $atendimento->has('solicitante') ? $this->Html->link($atendimento->solicitante->nome, ['controller' => 'Solicitantes', 'action' => 'view', $atendimento->solicitante->id]) : '' ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class='ibox-title'>
                            <h5>Dados Atendimento</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="vertical-table">
                                    <tr>
                                        <th style="min-width: 160px;"><?= __('Tipo de Atendimento') ?></th>
                                        <td style="min-width: 100px;"> <?= $atendimento->has('tipo_atendimento') ? $this->Html->link($atendimento->tipo_atendimento->nome, ['controller' => 'TipoAtendimentos', 'action' => 'view', $atendimento->tipo_atendimento->id]) : '' ?></td>
                                        <th style="min-width: 140px;"><?= __('Total Líquido') ?></th>
                                        <td><?= $this->Number->format($atendimento->total_liquido, ['places' => 2, 'before' => 'R$ ']) ?></td>
                                    </tr>
                                    <tr>
                                        <th><?= __('Período') ?></th>
                                        <td><?= ($atendimento->has('configuracao_periodo')) ? $atendimento->configuracao_periodo->nome : '' ?></td>
                                        <th><?= __('Total Recebido') ?></th>
                                        <td><?= $this->Number->format($atendimento->total_pagoato, ['places' => 2, 'before' => 'R$ ']) ?></td>
                                    </tr>
                                    <tr>
                                        <th><?= __('Hora') ?></th>
                                        <td><?= $this->Time->format($atendimento->hora, 'H:m') ?></td>
                                        <th><?= __('Total a receber') ?></th>
                                        <td><?= $this->Number->format($atendimento->total_areceber, ['places' => 2, 'before' => 'R$ ']) ?></td>
                                    </tr>
                                    <tr>
                                        <th><?= __('Data') ?></th>
                                        <td><?= $this->Time->format($atendimento->data, 'dd/MM/Y') ?></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class='ibox-title'>
                            <h5>Responsaveis</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="vertical-table">
                                    <tr>
                                        <th>Cadastro:</th>
                                        <td> <?php echo $atendimento->user->nome . ' - ' . $atendimento->created ?></td>
                                    </tr>
                                    <tr style="line-height: 50px">
                                        <th>Alteração:</th>
                                        <td style="text-indent: 5px"> <?php echo $atendimento->user->nome . ' - ' . $atendimento->modified ?></td>
                                    </tr>
                                    <tr>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class='ibox-title'>
                            <h5>Observação</h5>
                        </div>
                        <div class="ibox-content">
                            <?= $this->Text->autoParagraph(h($atendimento->observacao)); ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class='ibox-title'>
                            <h5>Procedimentos</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th><?= __('Procedimento') ?></th>
                                        <th><?= __('Caixa') ?></th>
                                        <th><?= __('valor Faturar') ?></th>
                                        <th><?= __('Profissional') ?></th>
                                        <th><?= __('Complemento') ?></th>
                                        <th class="<?= $this->Configuracao->showControleFinanceiroOnAtendimentos() ?>"><?= __('Controle') ?></th>
                                        <th class="<?= $this->Configuracao->showPrevEntrega() ?>"><?= __('Prev.Entrega') ?></th>
                                        <th><?= __('Conferido') ?></th>
                                        <th><?= __('N° Fatura') ?></th>
                                        <th><?= __('Detalhes Fatura') ?></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <?php
                                    $valor = 0;
                                    $valor_fatura = 0;
                                    if (!empty($atendimento->atendimento_procedimentos)):
                                        foreach ($atendimento->atendimento_procedimentos as $anted):
                                            $valor += $anted->valor_caixa;
                                            $valor_fatura += $anted->valor_fatura;
                                            ?>
                                            <tr>
                                                <td><?= $anted->procedimento->nome ?></td>
                                                <td><?= $this->Number->format($anted->valor_caixa, ['places' => 2, 'before' => 'R$ ']) ?></td>
                                                <td><?= $this->Number->format($anted->valor_fatura, ['places' => 2, 'before' => 'R$ ']) ?></td>
                                                <td><?= $anted->medico_responsavei->nome ?></td>
                                                <td><?= $anted->complemento ?></td>
                                                <td class="<?= $this->Configuracao->showControleFinanceiroOnAtendimentos() ?>">
                                                    <?= $anted->controle_financeiro->descricao ?></td>
                                                <td class="<?= $this->Configuracao->showPrevEntrega() ?>
                                                "><?= $anted->prev_entrega_proc ?></td>
                                                <td><?= ($anted->situacao_fatura_id == 1) ? 'Não <i class="text-danger fa fa-close"></i>' : 'Sim <i class="text-info fa fa-check"></i>' ?></td>
                                                <td><?= $anted->fatura_encerramento_id ?></td>
                                                <td>
                                                    <?php
                                                    if (!empty($anted->situacao_recebimento_id)) {
                                                        echo $this->Html->link('detalhes', ['controller' => 'AtendimentoProcedimentos', 'action' => 'situacaoRecebimento', $anted->id], ['data-toggle' => 'modal', 'data-target' => '#modal_lg']);
                                                    }
                                                    ?>
                                                </td>
                                                <td class="actions">
                                                    <div class="dropdown">
                                                        <button class="btn btn-info btn-xs dropdown-toggle"
                                                                type="button" data-toggle="dropdown"><span
                                                                    class="fa fa-list"></span> Opções <span
                                                                    class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu dropdown-prontuario">
                                                            <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Mat/Med - Fatura', ['controller' => 'FaturaMatmed', 'action' => 'index', $anted->atendimento_id], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?></li>
                                                            <li class="<?=$this->Configuracao->usaEquipe(); ?>">
                                                                <?= $this->Html->link('<i class="fa fa-users"></i> Equipe', ['controller' => 'AtendimentoProcedimentoEquipes', 'action' => 'index', $anted->id], ['escape' => false, 'target' => '_blank']); ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach;; endif; ?>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <strong><?= $this->Number->format(' ' . $valor, ['places' => 2, 'before' => 'R$ ']) ?></strong>
                                        </td>
                                        <td colspan="3">
                                            <strong><?= $this->Number->format($valor_fatura, ['places' => 2, 'before' => 'R$ ']) ?></strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class='col-md-4 pull-right'>
                                <!--<?= $this->Html->link('<i class="fa fa-file-text-o"></i> Imprimir Ficha', ['controller' => 'ClienteContratos', 'action' => 'index', '?' => ['ClienteContratos__atendimento_id' => $atendimento->id, 'first' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir Ficha', 'escape' => false, 'class' => 'btn btn-info ' . ($atendimento->acertado == 1 ? "" : "disabled"), 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>-->
                                <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Imprimir Ficha', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-info', 'onclick' => "checkConfiFicha($atendimento->id)"]) ?>
                                <?= $this->Html->link('<i class="fa fa-money"></i> Financeiro', ['controller' => 'contasreceber', 'action' => 'index', '?' => ['id' => $atendimento->id, 'first' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Financeiro', 'escape' => false, 'class' => 'btn btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal_lg', 'id' => 'view-financeiro']); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php if ($this->Configuracao->mostraAreaInternacao() && $atendimento->tipoatendimento_id == 10): ?>
                        <div class="col-md-12">
                            <div class='ibox-title'>
                                <h5>Dados Internação</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th><?= __('Carater Atendimento') ?></th>
                                            <th><?= __('Acomodação') ?></th>
                                            <th><?= __('Internação Prévia') ?></th>
                                            <th><?= __('Internação Prévia Dta.') ?></th>
                                            <th><?= __('Internação Dt./Hr Entrada') ?></th>
                                            <th><?= __('Setor') ?></th>
                                            <th><?= __('Leito') ?></th>
                                            <th><?= __('Internação Motivo Alta') ?></th>
                                            <th><?= __('Internação Dt./Hr Saída') ?></th>
                                        </tr>
                                        <tr>
                                            <td><?= $atendimento->has('internacao_carater_atendimento') ? $atendimento->internacao_carater_atendimento->descricao : '' ?></td>
                                            <td><?= $atendimento->has('internacao_acomodacao') ? $atendimento->internacao_acomodacao->descricao : ''  ?></td>
                                            <td><?= $atendimento->internacao_previa?></td>
                                            <td><?= $atendimento->internacao_previa_data ?></td>
                                            <td><?= $atendimento->internacao_data_hora_entrada ?></td>
                                            <td><?= $atendimento->has('internacao_setor') ? $atendimento->internacao_setor->descricao : '' ?></td>
                                            <td><?= $atendimento->has('internacao_leito') ? $atendimento->internacao_leito->descricao : '' ?></td>
                                            <td><?= $atendimento->has('internacao_motivo_altum') ? $atendimento->internacao_motivo_altum->descricao : '' ?></td>
                                            <td><?= $atendimento->internacao_data_hora_saida ?></td>
                                        </tr>

                                    </table>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
if (!empty($open_fin)) {
    echo $this->Html->scriptBlock(
        '$(window).load(function() {
                $("#view-financeiro").click();
            })',
        ['inline' => false, 'block' => 'scriptLast']
    );
}
echo $this->Html->scriptBlock(
    ' 
             loadicheck();
            ',
    ['inline' => false, 'block' => 'scriptLast']
);

/*if (!empty($_SESSION['v_parcelas'])) {
    echo $this->Html->scriptBlock(
        '$(window).load(function() {
           $("#view-financeiro").click();    
        })',
        ['inline' => false, 'block' => 'scriptLast']
    );
}*/
?>