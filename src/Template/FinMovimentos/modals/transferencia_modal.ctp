<div id="modalTransfer" class="clearfix">
    <div class="ibox-content" style="min-width: 50%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">
        <div class="text-left">
            <?= $this->Form->create($movimento) ?>
            <fieldset>
                <legend>
                    <?= __('Cadastrar Transferencia') ?>
                </legend>
                <div class="col-sm-12">
                    <?= $this->Form->input('banco_movimento_origem', ['label' => 'Origem', 'required' => 'required', 'type' => 'select', 'class' => 'select2', 'empty' => 'Selecione', 'options' => $banco_movimentos]); ?>
                </div>
                <div class="col-sm-12">
                    <?= $this->Form->input('banco_movimento_destino', ['label' => 'Destino', 'required' => 'required', 'type' => 'select', 'empty' => 'Selecione', 'options' => $banco_movimentos]); ?>
                </div>
                <div class="col-sm-12">
                    <?= $this->Form->input('data', ['label' => 'Data documento', 'required' => 'required', 'type' => 'text', 'class' => "datepicker", 'value' => date('d/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                </div>
                <div class='col-md-12'>
                    <?= $this->Form->input('contabilidade_id', ['label' => 'Empresas', 'required' => 'required', 'type' => 'select', 'empty' => 'Selecione', 'options' => $contabilidades]); ?>
                </div>
                <div class='col-md-12'>
                    <?=$this->Form->input('documento', ['required' => 'required']); ?>
                </div>
                <div class='col-md-12'>
                    <?= $this->Form->input('valor', ['type' => 'text', 'label' => 'Valor', 'mask' => 'money', 'default' => '0,00', 'prepend' => [$this->Form->button('R$', ['type' => 'button'])]]); ?>
                </div>
                <div class='col-md-12'>
                    <?=$this->Form->input('complemento', ['label' => 'Observação', 'type' => 'textarea' ]); ?>
                </div>
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <buttom class="btn btn-default" id="cancel-transf">Cancelar</buttom>
                        <input type="submit" class="btn btn-primary" value="Salvar">
                    </div>
                </div>
            <div class="clearfix"></div>
            <?= $this->Form->end() ?>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('.datepicker').each(function () {
                
                $(this).mask('00/00/0000');
                $(this).datetimepicker({
                    format: 'L',
                    extraFormats: ['YYYY-MM-DD HH:mm:ss', 'YYYY-MM-DD'],
                    showTodayButton: true,
                    useStrict: true,
                    locale: 'pt-BR',
                    showClear: true,
                    allowInputToggle: true
                }).on('dp.show', function (e) {
                    var widget = $('.bootstrap-datetimepicker-widget:last')
                    widget.addClass('piker-widget')
                })

            });
            $('[mask="money"]').each(function () {
                var aux = $(this).val();

                /*if (!empty(aux)) {
                    $(this).val(number_format(parseFloat(aux), 2, '.', ''))
                }*/
                $(this).mask('000000000000000.00', {reverse: true});
            });
        })
    </script>
</div>