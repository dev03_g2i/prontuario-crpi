<div id="modalFinishMovesBank" class="clearfix">
    <div class="ibox-content" style="min-width: 50%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">
        <div class="text-left">
            <fieldset>
                <legend>
                    <?= __('Finzalizar Movimentação') ?>
                </legend>
                <div class="col-sm-12">
                    <input id="bankMoveId" type="text" value=<?= $bankMove->id ?> class='d-none'>
                </div>
                <div class="col-sm-12">
                    <?= $this->Form->input('saldoInicial', ['label' => 'Saldo Inicial', 'readonly' => 'readonly', 'type' => 'text', 'value' => number_format($bankMove->saldoInicial, 2, ',', '.')]); ?>
                </div>
                <div class="col-sm-12">
                        <?= $this->Form->input('saldoAtual', ['label' => 'Saldo Atual', 'readonly' => 'readonly', 'type' => 'text', 'value' => number_format($bankMove->calculoSaldoFinal, 2, ',', '.')]); ?>
                    </div>
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <buttom class="btn btn-default" id="cancel-finish-moves-bank">Cancelar</buttom>
                        <buttom class="btn btn-primary" id="save-finish-moves-bank">Finalizar</buttom>
                    </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>