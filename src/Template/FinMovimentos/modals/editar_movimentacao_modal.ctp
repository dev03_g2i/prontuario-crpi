<div id="modalEditMovesBank" class="clearfix">
    <div class="ibox-content" style="min-width: 50%; z-index: 90;border: thin solid lightgray; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background-color: white; text-align: center; padding: 20px;">
        <div class="text-left">
            <fieldset>
                <legend>
                    <?= __('Movimentação') ?>
                </legend>
                <div class="col-sm-12">
                    <?= $this->Form->input('saldoInicial', ['label' => 'Saldo Inicial', 'readonly' => 'readonly', 'type' => 'text', 'default' => number_format($bankMove->saldoInicial, 2, ',', '.')]); ?>
                </div>
                <?php if($bankMove->status === 'Aberto'):?>
                    <div class="col-sm-12">
                        <?= $this->Form->input('saldoAtual', ['label' => 'Saldo Atual', 'readonly' => 'readonly', 'type' => 'text', 'default' => number_format($bankMove->calculoSaldoFinal, 2, ',', '.')]); ?>
                    </div>
                <?php else: ?>
                    <div class="col-sm-12">
                        <?= $this->Form->input('saldoFinal', ['label' => 'Saldo Final', 'readonly' => 'readonly', 'type' => 'text', 'default' => isset($bankMove->saldoFinal) ? number_format($bankMove->saldoFinal, 2 , ',', '.') : '0,00']); ?>
                    </div>
                    <?php if(isset($endUser)):?>
                        <div class="col-sm-12">
                            <strong><?= ' Encerrado por: '.$endUser->nome.' dia '.date_format($bankMove->encerradoDt, 'd/m/Y').' às '.date_format($bankMove->encerradoDt, 'H:i:s') ?></strong>
                        </div>
                    <?php endif;?>
                <?php endif;?>
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <buttom class="btn btn-default" id="cancel-edit-moves-bank">Sair</buttom>
                    </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>