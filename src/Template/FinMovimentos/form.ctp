<div class="finMovimentos form">
    <?= $this->Form->create($finMovimento) ?>
        <fieldset>
            <div class="row">
                <div class='col-md-6'>
                    <div class="form-group select">
                        <label class="control-label" for="fin-banco-id">Banco *</label>
                        <select name="fin_banco_movimento_id" class="form-control" required="required" id="fin-banco-id">
                            <?php if(isset($finMovimento->fin_banco_id)):?>
                                <option value="<?= $finMovimento->fin_banco_movimento->id?>"><?= $finMovimento->fin_banco_movimento->banco_movimento_nome?></option>
                            <?php else: ?>
                                <option value="">selecione</option>
                            <?php endif;?>
                            <?php foreach($bancoMovimentos as $bancoMovimento):?>
                                <option value="<?= $bancoMovimento->id ?>"><?= $bancoMovimento->banco_movimento_nome ?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div class='col-md-6'>
                    <?=$this->Form->input('data', ['label' => 'Data do documento', 'value'=>$this->Time->format($finMovimento->data,'dd/MM/Y'), 'required' => 'required', 'type'=>'text', 'class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                </div>
            </div>
            <div class="row">
                <?php if(isset($finMovimento->fin_fornecedor_id) || isset($finMovimento->cliente_id)): ?>
                    <div class="col-md-6" id="addMoveRadio">
                        <label for="tipo">Tipo</label>
                        <div class="radio">
                                <?php if(isset($finMovimento->cliente_id)): ?>
                                    <div class="iradio_square-green" style="position: relative;">
                                        <input type="radio" name="tipo" value="0" id="cliente" checked="checked" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"/>
                                    </div> Crédito
                                    <div class="iradio_square-green" style="position: relative;">
                                        <input type="radio" name="tipo" value="1" id="fornecedor" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"/>
                                    </div> Débito
                                <?php else: ?>
                                    <div class="iradio_square-green" style="position: relative;">
                                        <input type="radio" name="tipo" value="0" id="cliente" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"/>
                                    </div> Crédito
                                    <div class="iradio_square-green" style="position: relative;">
                                        <input type="radio" name="tipo" value="1" id="fornecedor" checked="checked" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"/>
                                    </div> Débito
                                <?php endif; ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-md-6" id="addMoveRadio">
                        <label for="tipo">Tipo</label>
                        <div class="radio">
                            <?php if($finMovimento->credito === 0 && $finMovimento->debito != 0): ?>
                                <div class="iradio_square-green" style="position: relative;">
                                    <input type="radio" name="tipo" value="0" id="cliente" checked="checked" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"
                                    />
                                </div> Crédito
                                <div class="iradio_square-green" style="position: relative;">
                                    <input type="radio" name="tipo" value="1" id="fornecedor" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"
                                    />
                                </div> Débito
                            <?php elseif ($finMovimento->debito === 0 && $finMovimento->credito != 0): ?>
                                <div class="iradio_square-green" style="position: relative;">
                                    <input type="radio" name="tipo" value="0" id="cliente" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"
                                    />
                                </div> Crédito
                                <div class="iradio_square-green" style="position: relative;">
                                    <input type="radio" name="tipo" value="1" id="fornecedor" checked="checked" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"
                                    />
                                </div> Débito
                            <?php else:?>
                                <div class="iradio_square-green" style="position: relative;">
                                    <input type="radio" name="tipo" value="0" id="cliente" checked="checked" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"
                                    />
                                </div> Crédito
                                <div class="iradio_square-green" style="position: relative;">
                                    <input type="radio" name="tipo" value="1" id="fornecedor" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"
                                    />
                                </div> Débito
                            <?php endif;?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class='col-md-6'>
                    <?=$this->Form->input('fin_plano_conta_id', ['label' => 'Plano de contas', 'required' => 'required', 'data'=>'select', 'controller'=>'finPlanoContas', 'action'=>'fill', 'data-value' => $finMovimento->fin_plano_conta_id, 'empty' => 'selecione']); ?>
                </div>
            </div>
            <div class="row">
                <?php if(isset($finMovimento->fin_fornecedor_id) || isset($finMovimento->cliente_id)): ?>
                    <!-- Inicio cliente/fornecedor -->
                    <?php if(isset($finMovimento->cliente_id)): ?>
                        <div class='col-md-6 d-none' name='fin_fornecedor_id'>
                            <?=$this->Form->input('fin_fornecedor_id', ['label' => 'Credor', 'data'=>'select', 'controller'=>'finFornecedores', 'action' => 'fill', 'data-value' => $finMovimento->fin_fornecedor_id, 'empty' => 'Selecione']); ?>
                        </div>
                        <div class='col-md-6 d-block' name="cliente_id">
                            <?=$this->Form->input('cliente_id', ['label' => 'Cliente', 'data'=>'select', 'controller'=>'clientes', 'action' => 'fill', 'data-value' => $finMovimento->cliente_id, 'empty' => 'Selecione']); ?>
                        </div>
                    <?php else: ?>
                        <div class='col-md-6 d-block' name='fin_fornecedor_id'>
                            <?=$this->Form->input('fin_fornecedor_id', ['label' => 'Credor', 'data'=>'select', 'controller'=>'finFornecedores', 'action' => 'fill', 'data-value' => $finMovimento->fin_fornecedor_id, 'empty' => 'Selecione']); ?>
                        </div>
                        <div class='col-md-6 d-none' name="cliente_id">
                            <?=$this->Form->input('cliente_id', ['label' => 'Cliente', 'data'=>'select', 'controller'=>'clientes', 'action' => 'fill', 'data-value' => $finMovimento->cliente_id, 'empty' => 'Selecione']); ?>
                        </div>
                    <?php endif; ?>
                    <!-- Fim cliente/fornecedor -->
                <?php else: ?>
                    <!-- Inicio cliente/fornecedor -->
                    <div class='col-md-6 d-none' name='fin_fornecedor_id'>
                        <?=$this->Form->input('fin_fornecedor_id', ['label' => 'Credor', 'data'=>'select', 'controller'=>'finFornecedores', 'action' => 'fill', 'data-value' => $finMovimento->fin_fornecedor_id, 'empty' => 'Selecione']); ?>
                    </div>
                    <div class='col-md-6 d-block' name="cliente_id">
                        <?=$this->Form->input('cliente_id', ['label' => 'Cliente', 'data'=>'select', 'controller'=>'clientes', 'action' => 'fill', 'data-value' => $finMovimento->cliente_id, 'empty' => 'Selecione']); ?>
                    </div>
                    <!-- Fim cliente/fornecedor -->
                <?php endif; ?>
                <div class='col-md-6'>
                    <?=$this->Form->input('valor', ['type' => 'text', 'label' => 'Valor', 'value' => !empty($finMovimento->credito) ? number_format($finMovimento->credito, 2) : number_format($finMovimento->debito, 2), 'mask' => 'money', 'prepend' => [$this->Form->button('<strong>R$</strong>', ['type' => 'button'])]]); ?>
                </div>
            </div>
            <div class="row">
                <div class='col-md-6'>
                    <?=$this->Form->input('fin_contabilidade_id', ['label' => 'Empresa', 'required' => 'required', 'type' => 'select', 'options' => $contabilidades, 'empty' => 'selecione']); ?>
                </div>
                <div class='col-md-6'>
                    <?=$this->Form->input('documento'); ?>
                </div>
            </div>
            <div class="row">
                <div class='col-md-6'>
                    <?=$this->Form->input('complemento', ['label' => 'Observação', 'type' => 'textarea']); ?>
                </div>
            </div>
        </fieldset>
        <div class="col-md-12 text-right">
            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
        </div>
        <div class="clearfix"></div>
        <?= $this->Form->end() ?>
</div>