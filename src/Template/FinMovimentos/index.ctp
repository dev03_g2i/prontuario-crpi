<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Movimentos</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinMovimentos', ['type' => 'get', 'id' => 'fin-movimentos-form']) ?>
                            <div class="col-sm-12 no-padding">
                                <div class="col-sm-3">
                                    <?= $this->Form->input('banco', ['label' => 'Banco', 'type' => 'select', 'empty' => 'Selecione', 'options' => $bancos, 'required' => 'required']); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('situacao', ['label' => 'Situação', 'type' => 'select', 'empty' => 'Selecione', 'default' => 1, 'options' => [2 => 'Encerrado', 1 => 'Aberto'], 'required' => 'required']); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('movimento', ['label' => 'Movimento', 'type' => 'select', 'empty' => 'Selecione', 'disabled' => 'disabled', 'required' => 'required']); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <?= $this->Form->input('inicio', ['label' => 'Inicio', 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class="col-sm-3">
                                <?= $this->Form->input('fim', ['label' => 'Fim', 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-sm-3'>
                                <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                                    'options' => $tiposRelatorios,
                                    'append' => [
                                        $this->Form->button($this->Html->icon('print'), ['id' => 'gerar-report',
                                            'type' => 'button', 'class' => 'btn btn-success',
                                            'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'relatorios',
                                            'action' => 'financeiroMovimentos'])]
                                ]) ?>
                            </div>
                            <div class="col-sm-12 text-center p-t-25">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row no-padding table-responsive">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <div class="col-md-3">
                            <label>Saldo inicial</label>
                            </br>
                            <strong><?= isset($saldoInicial) ? number_format($saldoInicial, 2, ',', '.') : '0,00' ?></strong>
                        </div>
                        <div class="col-md-3">
                            <label>Total crédito</label>
                            </br>
                            <strong><?= isset($totalCredito) ? number_format($totalCredito, 2, ',', '.') : '0,00' ?></strong>
                        </div>
                        <div class="col-md-3">
                            <label>Total débito</label>
                            </br>
                            <strong><?= isset($totalDebito) ? number_format($totalDebito, 2, ',', '.') : '0,00' ?></strong>
                        </div>
                        <div class="col-md-3">
                            <label>Saldo final</label>
                            </br>
                            <strong><?= isset($saldoFinal) ? number_format($saldoFinal, 2, ',', '.') : '0,00' ?></strong>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <section class="col-sm-12 no-padding connectedSortable">
            <div class="col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding p-r-10">
                    <div class="ibox-content">
                        <div class="col-sm-12 ibox-content btn-group p-b-15 p-l-20">
                            <div class="col-sm-2 no-padding">
                                <h3>Conciliar Movimentação</h3>
                            </div>
                            <div class="col-sm-8 col-sm-offset-2 no-padding text-right">
                                <?= $this->Html->link('<i class="fa fa-plus-circle"></i> Transferência', '', ['id' => 'create-transf', 'escape' => false, 'class' => 'btn btn-info', 'target' => '_blank']) ?>
                                <?= $this->Html->link('<i class="fa fa-plus-circle"></i> Lançar', '/finMovimentos/add', ['escape' => false, 'class' => 'btn btn-primary', 'target' => '_blank']) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="p-t-10 no-padding background-white">
                                    <?php if (isset($finMovimentos)): ?>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <?= 'Nr.lanc' ?>
                                                </th>
                                                <th>
                                                    <?= 'Data' ?>
                                                </th>
                                                <th>
                                                    <?= 'Credor/Cliente' ?>
                                                </th>
                                                <th>
                                                    <?= 'Plano de contas' ?>
                                                </th>
                                                <th>
                                                    <?= 'Empresa' ?>
                                                </th>
                                                <th>
                                                    <?= 'Documento' ?>
                                                </th>
                                                <th>
                                                    <?= 'Banco/Movimento' ?>
                                                </th>
                                                <th>
                                                    <?= 'Observação' ?>
                                                </th>
                                                <th class="text-right">
                                                    <?= 'Crédito' ?>
                                                </th>
                                                <th class="text-right">
                                                    <?= 'Débito' ?>
                                                </th>
                                                <th class="text-right">
                                                    <?= 'Saldo' ?>
                                                </th>
                                                <th>
                                                    <?= __('Ações') ?>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($finMovimentos as $key => $movimento): ?>
                                                <tr>
                                                    <td>
                                                        <?= isset($movimento->id) ? $movimento->id : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->data) ? date_format($movimento->data, 'd/m/Y') : 'Sem data' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->credor_cliente) ? $movimento->credor_cliente : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->fin_plano_conta) ? $movimento->fin_plano_conta->nome : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->fin_contabilidade) ? $movimento->fin_contabilidade->nome : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->documento) ? $movimento->documento : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->fin_banco) && isset($movimento->fin_banco_movimento) ? $movimento->fin_banco->nome . ' - ' . $movimento->fin_banco_movimento->data_movimento : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->complemento) ? $movimento->complemento : '' ?>
                                                    </td>
                                                    <td class="d-credit" align="right">
                                                        <?= isset($movimento->credito) ? number_format($movimento->credito, 2, ',', '.') : '0,00'; ?>
                                                    </td>
                                                    <td class="d-debit" align="right">
                                                        <?= isset($movimento->debito) ? number_format($movimento->debito, 2, ',', '.') : '0,00'; ?>
                                                    </td>
                                                    <?php if ($movimento->saldo <= 0): ?>
                                                        <td class="d-debit" align="right">
                                                            <?= isset($movimento->saldo) ? number_format($movimento->saldo, 2, ',', '.') : '0,00' ?>
                                                        </td>
                                                    <?php else: ?>
                                                        <td class="d-credit" align="right">
                                                            <?= isset($movimento->saldo) ? number_format($movimento->saldo, 2, ',', '.') : '0,00' ?>
                                                        </td>
                                                    <?php endif; ?>
                                                    <td class="actions" style="white-space:nowrap">
                                                        <div class="no-padding dropdown">
                                                            <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                            <ul class="dropdown-menu dropdown-menu-left">
                                                                <li>
                                                                    <?= $this->Html->link('Visualizar', ['action' => 'view', $movimento->id], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Editar', ['action' => 'edit', $movimento->id], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Editar Rateio', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Anexos', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Excluir', '#Javascript:void(0)', ['onclick' => 'Deletar(\'FinMovimentos\',' . $movimento->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false,  'listen' => 'f']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Vinculos', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('controllers/Movimentos', ['block' => 'scriptBottom']);
?>
