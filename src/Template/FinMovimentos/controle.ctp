<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Movimentações</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinMovimentos', ['type' => 'get', 'id' => 'fin-movimentos-form']) ?>
                                <div class="col-sm-4 no-padding">
                                    <div class="col-sm-4">
                                        <?= $this->Form->input('month', ['label' => 'Mês', 'type' => 'text', 'mask' => 'month']); ?>
                                    </div>
                                    <div class="col-sm-8">
                                        <?= $this->Form->input('year', ['label' => 'Ano', 'type' => 'text', 'mask' => 'year']); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <?= $this->Form->input('bank', ['label' => 'Banco', 'type' => 'select', 'empty' => 'Selecione', 'options' => $banks]); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $this->Form->input('situation', ['label' => 'Situação', 'type' => 'select', 'empty' => 'Selecione', 'options' => [1 => 'Aberto', 2 => 'Encerrado']]); ?>
                                </div>
                                <div class="col-sm-12 text-center p-t-25">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row no-padding table-responsive">
        <section class="col-sm-12 no-padding connectedSortable">
            <div class="col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding p-r-10">
                    <div class="ibox-content">
                        <div class="col-sm-12 ibox-content btn-group p-b-15 p-l-20">
                            <div class="col-sm-2 no-padding">
                                <h3>Movimentações</h3>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="p-t-10 no-padding background-white">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <?= 'Movimentação' ?>
                                                </th>
                                                <th>
                                                    <?= 'mes' ?>
                                                </th>
                                                <th>
                                                    <?= 'ano' ?>
                                                </th>
                                                <th>
                                                    <?= 'situacao' ?>
                                                </th>
                                                <th>
                                                    <?= __('Ações') ?>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($bankMoves as $bankMove): ?>
                                            <tr>
                                                <td>
                                                    <?= isset($bankMove->banco_movimento_nome) ? $bankMove->banco_movimento_nome : '' ?>
                                                </td>
                                                <td>
                                                    <?= isset($bankMove->mes) ? $bankMove->mes : '' ?>
                                                </td>
                                                <td>
                                                    <?= isset($bankMove->ano) ? $bankMove->ano : '' ?>
                                                </td>
                                                <td>
                                                    <?= isset($bankMove->status) ? $bankMove->status : '' ?>
                                                </td>
                                                <td class="actions" style="white-space:nowrap">
                                                    <div class="no-padding dropdown">
                                                        <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                            <ul class="dropdown-menu dropdown-menu-left">
                                                                <li>
                                                                    <?= $this->Form->button('Visualizar Movimentação', ['type' => 'button', 'class' => 'btn btn-default', 'style' => 'width: 100%', 'name' => 'edit-moves-bank', 'value' => $bankMove->id]) ?>
                                                                </li>
                                                                <?php if($bankMove->status === 'Aberto'):?>
                                                                    <li>
                                                                        <?= $this->Form->button('Finalizar Movimentação', ['type' => 'button', 'class' => 'btn btn-default', 'style' => 'width: 100%', 'name' => 'finish-moves-bank', 'value' => $bankMove->id]) ?>
                                                                    </li>
                                                                <?php endif;?>
                                                            </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('controllers/MovimentacaoControle', ['block' => 'scriptBottom']);
?>

