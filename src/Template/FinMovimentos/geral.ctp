<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Movimentos</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinMovimentos', ['type' => 'get', 'id' => 'fin-movimentos-form']) ?>
                            <div class="col-sm-12 no-padding">
                                <div class="col-sm-3">
                                    <?= $this->Form->input('inicio', ['label' => 'Inicio', 'type' => 'text', 'required' => 'required', 'class' => "datepicker", 'default' => $initialDate, 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('fim', ['label' => 'Fim', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'default' => $finalDate, 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('banco', ['label' => 'Banco', 'type' => 'select', 'empty' => 'Selecione', 'options' => $bancos]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('contabilidade', ['label' => 'Empresas', 'type' => 'select', 'empty' => 'Selecione', 'options' => $contabilidades]); ?>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="col-sm-3">
                                    <?= $this->Form->input('planoconta', ['label' => 'Plano de contas', 'type' => 'select', 'data-planocontas' => "planocontas", 'empty' => 'Selecione', 'options' => [@$fin_plano_contas->id => @$fin_plano_contas->nome], 'default' => @$fin_plano_contas->id]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('fornecedor', ['label' => 'Credores', 'type' => 'select', 'data-fornecedores' => "fornecedores", 'empty' => 'Selecione', 'options' => [@$fin_fornecedores->id => @$fin_fornecedores->nome], 'default' => @$fin_fornecedores->id]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('cliente', ['label' => 'Cliente', 'type' => 'select', 'data-cliente' => "cliente", 'empty' => 'Selecione', 'options' => [@$clientes->id => @$clientes->nome], 'default' => @$clientes->id]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('documento', ['label' => 'Documento']); ?>
                                </div>
                            </div>
                            <div class='col-sm-3'>
                                <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                                    'options' => $tiposRelatorios,
                                    'append' => [
                                        $this->Form->button($this->Html->icon('print'), ['id' => 'gerar-report',
                                            'type' => 'button', 'class' => 'btn btn-success',
                                            'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'relatorios',
                                            'action' => 'financeiroMovimentosGeral'])]])
                                ?>
                            </div>
                            <div class="col-sm-12 text-center p-t-25">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row no-padding table-responsive">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <div class="col-md-2">
                            <label>Saldo inicial</label>
                            </br>
                            <strong>
                                <?= isset($saldoInicial) ? number_format($saldoInicial, 2, ',', '.') : '0,00' ?>
                            </strong>
                        </div>
                        <div class="col-md-2">
                            <label>Tl. Crédito</label>
                            </br>
                            <strong>
                                <?= isset($totalCredito) ? number_format($totalCredito, 2, ',', '.') : '0,00' ?>
                            </strong>
                        </div>
                        <div class="col-md-2">
                            <label>Tl. Débito</label>
                            </br>
                            <strong>
                                <?= isset($totalDebito) ? number_format($totalDebito, 2, ',', '.') : '0,00' ?>
                            </strong>
                        </div>

                        <div class="col-md-2">
                            <label>Tl. Transf Crédito</label>
                            </br>
                            <strong>
                                <?= isset($totalTransfC) ? number_format($totalTransfC, 2, ',', '.') : '0,00' ?>
                            </strong>
                        </div>
                        <div class="col-md-2">
                            <label>Tl. Transf Débito</label>
                            </br>
                            <strong>
                                <?= isset($totalTransfD) ? number_format($totalTransfD, 2, ',', '.') : '0,00' ?>
                            </strong>
                        </div>

                        <div class="col-md-2">
                            <label>Saldo final</label>
                            </br>
                            <strong>
                                <?= isset($saldoFinal) ? number_format($saldoFinal, 2, ',', '.') : '0,00' ?>
                            </strong>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <section class="col-sm-12 no-padding connectedSortable">
            <div class="col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding">
                    <div class="ibox float-e-margins">
                        <div class="col-sm-12 ibox-content btn-group p-b-0 p-l-20">
                            <div class="col-sm-2 no-padding">
                                <h3>Movimentação geral</h3>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <?php if (isset($finMovimentos)): ?>
                                <div class="clearfix">
                                    <div class="p-t-10 no-padding background-white">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Nr.lanc</th>
                                                <th>Data</th>
                                                <th>Banco</th>
                                                <th>Movimento</th>
                                                <th>Credor/Cliente</th>
                                                <th>Plano de contas</th>
                                                <th>Empresa</th>
                                                <th>Documento</th>
                                                <th>Observação</th>
                                                <th class="text-right">Crédito</th>
                                                <th class="text-right">Débito</th>
                                                <!-- <th>Saldo</th> -->
                                                <th>
                                                    <?= __('Ações') ?>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($finMovimentos as $key => $movimento): ?>
                                                <tr>
                                                    <td>
                                                        <?= isset($movimento->id) ? $movimento->id : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->data) ? date_format($movimento->data, 'd/m/Y') : 'Sem data' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->fin_banco) ? $movimento->fin_banco->nome : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->data) ? date_format($movimento->data, 'm') . '/' . date_format($movimento->data, 'Y') : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->credor_cliente) ? $movimento->credor_cliente : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->fin_plano_conta) ? $movimento->fin_plano_conta->nome : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->fin_contabilidade) ? $movimento->fin_contabilidade->nome : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->documento) ? $movimento->documento : '' ?>
                                                    </td>
                                                    <td>
                                                        <?= isset($movimento->complemento) ? $movimento->complemento : '' ?>
                                                    </td>
                                                    <td class="d-credit" align="right">
                                                        <?= isset($movimento->credito) ? number_format($movimento->credito, 2, ',', '.') : '0,00'; ?>
                                                    </td>
                                                    <td class="d-debit" align="right">
                                                        <?= isset($movimento->debito) ? number_format($movimento->debito, 2, ',', '.') : '0,00'; ?>
                                                    </td>
                                                    <!-- <?php if ($movimento->saldo <= 0): ?>
                                                    <td class="d-debit">
                                                        <?= isset($movimento->saldo) ? number_format($movimento->saldo, 2, ',', '.') : '0,00'; ?>
                                                    </td>
                                                    <?php else: ?>
                                                    <td class="d-credit">
                                                        <?= isset($movimento->saldo) ? number_format($movimento->saldo, 2, ',', '.') : '0,00'; ?>
                                                    </td>
                                                    <?php endif; ?> -->
                                                    <td class="actions" style="white-space:nowrap">
                                                        <div class="no-padding dropdown">
                                                            <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                            <ul class="dropdown-menu dropdown-menu-left">
                                                                <li>
                                                                    <?= $this->Html->link('Visualizar', ['action' => 'view', $movimento->id], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Editar', ['action' => 'edit', $movimento->id], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Editar Rateio', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Anexos', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Excluir', '#Javascript:void(0)', ['onclick' => 'Deletar(\'FinMovimentos\',' . $movimento->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'listen' => 'f']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Vinculos', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                        <div style="text-align:center">
                                            <div class="paginator">
                                                <ul class="pagination">
                                                    <?php if ($this->Paginator->numbers()): ?>
                                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                                    <?php endif; ?>
                                                    <?= $this->Paginator->numbers() ?>
                                                    <?php if ($this->Paginator->numbers()): ?>
                                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                                    <?php endif; ?>
                                                    <p>
                                                        <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                                    </p>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('controllers/Movimentos', ['block' => 'scriptBottom']);
?>
