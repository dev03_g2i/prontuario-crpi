<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Movimentos</h2>
            <ol class="breadcrumb">
                <li class="active">
                    <strong>Editar movimento</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finMovimentos form">
                            <?php require_once ('form.ctp'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script('controllers/Movimentos',['block' => 'scriptBottom']);
?>