
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Movimentos</h2>
        <ol class="breadcrumb">
            <li>Fin Movimentos</li>
            <li class="active">
                <strong>Litagem de Fin Movimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Movimentos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Situacao') ?></th>
                                <td><?= h($finMovimento->situacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Plano Conta') ?></th>
                                                                <td><?= $finMovimento->has('fin_plano_conta') ? $this->Html->link($finMovimento->fin_plano_conta->nome, ['controller' => 'FinPlanoContas', 'action' => 'view', $finMovimento->fin_plano_conta->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Documento') ?></th>
                                <td><?= h($finMovimento->documento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Banco') ?></th>
                                                                <td><?= $finMovimento->has('fin_banco') ? $this->Html->link($finMovimento->fin_banco->id, ['controller' => 'FinBancos', 'action' => 'view', $finMovimento->fin_banco->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Categoria') ?></th>
                                <td><?= h($finMovimento->categoria) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Contabilidade') ?></th>
                                                                <td><?= $finMovimento->has('fin_contabilidade') ? $this->Html->link($finMovimento->fin_contabilidade->nome, ['controller' => 'FinContabilidades', 'action' => 'view', $finMovimento->fin_contabilidade->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finMovimento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Fin Fornecedor Id') ?></th>
                                <td><?= $this->Number->format($finMovimento->fin_fornecedor_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Credito') ?></th>
                                <td><?= $this->Number->format($finMovimento->credito) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Debito') ?></th>
                                <td><?= $this->Number->format($finMovimento->debito) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status Lancamento') ?></th>
                                <td><?= $this->Number->format($finMovimento->status_lancamento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('IdCliente') ?></th>
                                <td><?= $this->Number->format($finMovimento->idCliente) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Fin Contas Pagar Id') ?></th>
                                <td><?= $this->Number->format($finMovimento->fin_contas_pagar_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Criado Por') ?></th>
                                <td><?= $this->Number->format($finMovimento->criado_por) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Atualizado Por') ?></th>
                                <td><?= $this->Number->format($finMovimento->atualizado_por) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($finMovimento->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Criacao') ?></th>
                                                                <td><?= h($finMovimento->data_criacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Alteracao') ?></th>
                                                                <td><?= h($finMovimento->data_alteracao) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Complemento') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($finMovimento->complemento)); ?>
                </div>
            </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Banco Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finMovimento->fin_banco_movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th><?= __('Mes') ?></th>
                        <th><?= __('Ano') ?></th>
                        <th><?= __('Status') ?></th>
                        <th><?= __('SaldoInicial') ?></th>
                        <th><?= __('SaldoFinal') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Por') ?></th>
                        <th><?= __('EncerradoDt') ?></th>
                        <th><?= __('EncerradoPor') ?></th>
                        <th><?= __('Data Movimento') ?></th>
                        <th><?= __('Fin Movimento Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finMovimento->fin_banco_movimentos as $finBancoMovimentos): ?>
                <tr>
                    <td><?= h($finBancoMovimentos->id) ?></td>
                    <td><?= h($finBancoMovimentos->fin_banco_id) ?></td>
                    <td><?= h($finBancoMovimentos->mes) ?></td>
                    <td><?= h($finBancoMovimentos->ano) ?></td>
                    <td><?= h($finBancoMovimentos->status) ?></td>
                    <td><?= h($finBancoMovimentos->saldoInicial) ?></td>
                    <td><?= h($finBancoMovimentos->saldoFinal) ?></td>
                    <td><?= h($finBancoMovimentos->data) ?></td>
                    <td><?= h($finBancoMovimentos->por) ?></td>
                    <td><?= h($finBancoMovimentos->encerradoDt) ?></td>
                    <td><?= h($finBancoMovimentos->encerradoPor) ?></td>
                    <td><?= h($finBancoMovimentos->data_movimento) ?></td>
                    <td><?= h($finBancoMovimentos->fin_movimento_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinBancoMovimentos','action' => 'view', $finBancoMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinBancoMovimentos','action' => 'edit', $finBancoMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinBancoMovimentos','action' => 'delete', $finBancoMovimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finMovimento->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


