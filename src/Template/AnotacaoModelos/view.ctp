
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Anotacao Modelos</h2>
        <ol class="breadcrumb">
            <li>Anotacao Modelos</li>
            <li class="active">
                <strong>Litagem de Anotacao Modelos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Anotacao Modelos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($anotacaoModelo->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $anotacaoModelo->has('user') ? $this->Html->link($anotacaoModelo->user->nome, ['controller' => 'Users', 'action' => 'view', $anotacaoModelo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $anotacaoModelo->has('situacao_cadastro') ? $this->Html->link($anotacaoModelo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $anotacaoModelo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($anotacaoModelo->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($anotacaoModelo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($anotacaoModelo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Modelo') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($anotacaoModelo->modelo)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


