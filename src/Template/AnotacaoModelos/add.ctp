<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Anotacao Modelos</h2>
            <ol class="breadcrumb">
                <li>Anotacao Modelos</li>
                <li class="active">
                    <strong>                    Cadastrar Anotacao Modelos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="anotacaoModelos form">
                            <?= $this->Form->create($anotacaoModelo) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Anotacao Modelo') ?></legend>

                                <div class='col-md-12'>
                                    <?php echo $this->Form->input('nome'); ?>
                                </div>
                                <div class='col-md-12'>
                                    <?php echo $this->Form->input('modelo'); ?>
                                </div>


                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['id' => 'add_texto', 'class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

