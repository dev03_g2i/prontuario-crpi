<div class="white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Procedimentos</li>
            <li class="active">
                <strong>Listagem de Procedimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Procedimentos', ['type' => 'get']) ?>
                        <div class="col-md-4">
                            <?= $this->Form->input('nome', ['label' => 'Nome Procedimento']); ?>
                        </div>
                        <div class="col-md-4" style="margin-top: 25px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Procedimentos', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Procedimentos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Procedimentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('grupo_id') ?></th>
                                    <th><?= $this->Paginator->sort('restauracao', ['label' => 'Restauração']) ?></th>
                                    <th><?= $this->Paginator->sort('anotacao_folha', ['label' => 'Anotação Folha']) ?></th>
                                    <th class="<?=$this->Configuracao->showPrevEntrega()?>"><?= $this->Paginator->sort('dias_entrega', ['label' => 'Dias para Entrega']) ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($procedimentos as $procedimento): ?>
                                    <tr>
                                        <td><?= $this->Number->format($procedimento->id) ?></td>
                                        <td><?= h($procedimento->nome) ?></td>
                                        <td><?= $procedimento->has('grupo_procedimento') ? $this->Html->link($procedimento->grupo_procedimento->nome, ['controller' => 'GrupoProcedimentos', 'action' => 'view', $procedimento->grupo_procedimento->id]) : '' ?></td>
                                        <td><?= ($procedimento->restauracao) ? "sim" : "não" ?></td>
                                        <td><?= h($procedimento->anotacao_folha) ?></td>
                                        <td class="<?=$this->Configuracao->showPrevEntrega()?>"><?= h($procedimento->dias_entrega) ?></td>
                                        <td><?= h($procedimento->created) ?></td>
                                        <td><?= h($procedimento->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('usd'), ['controller' => 'PrecoProcedimentos', 'action' => 'all', '?' => ['procedimento' => $procedimento->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Valores de Procedimentos', 'escape' => false, 'class' => 'btn btn-xs btn-primary', 'target' => '_blank']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $procedimento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'Procedimentos\',' . $procedimento->id . ',recarregar_tela)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'ProcedimentoGastos','action' => 'index', '?'=>['procedimento_id'=>$procedimento->id,'ajax'=>1,'first'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Ficha Técnica/Dispensação', 'escape' => false, 'class' => 'btn btn-xs btn-success','data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

