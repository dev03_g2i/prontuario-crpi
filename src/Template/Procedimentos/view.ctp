

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Procedimentos</li>
            <li class="active">
                <strong>Litagem de Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="procedimentos">
    <h3><?= h($procedimento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($procedimento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Grupo Procedimento') ?></th>
            <td><?= $procedimento->has('grupo_procedimento') ? $this->Html->link($procedimento->grupo_procedimento->id, ['controller' => 'GrupoProcedimentos', 'action' => 'view', $procedimento->grupo_procedimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $procedimento->has('user') ? $this->Html->link($procedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $procedimento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $procedimento->has('situacao_cadastro') ? $this->Html->link($procedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $procedimento->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($procedimento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($procedimento->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($procedimento->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Atendimento Procedimentos') ?></h4>
        <?php if (!empty($procedimento->atendimento_procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Procedimento Id') ?></th>
                <th><?= __('Atendimento Id') ?></th>
                <th><?= __('Valor Fatura') ?></th>
                <th><?= __('Quantidade') ?></th>
                <th><?= __('Desconto') ?></th>
                <th><?= __('Porc Desconto') ?></th>
                <th><?= __('Valor Caixa') ?></th>
                <th><?= __('Digitado') ?></th>
                <th><?= __('Material') ?></th>
                <th><?= __('Num Controle') ?></th>
                <th><?= __('Medico Id') ?></th>
                <th><?= __('Valor Matmed') ?></th>
                <th><?= __('Documento Guia') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($procedimento->atendimento_procedimentos as $atendimentoProcedimentos): ?>
            <tr>
                <td><?= h($atendimentoProcedimentos->id) ?></td>
                <td><?= h($atendimentoProcedimentos->procedimento_id) ?></td>
                <td><?= h($atendimentoProcedimentos->atendimento_id) ?></td>
                <td><?= h($atendimentoProcedimentos->valor_fatura) ?></td>
                <td><?= h($atendimentoProcedimentos->quantidade) ?></td>
                <td><?= h($atendimentoProcedimentos->desconto) ?></td>
                <td><?= h($atendimentoProcedimentos->porc_desconto) ?></td>
                <td><?= h($atendimentoProcedimentos->valor_caixa) ?></td>
                <td><?= h($atendimentoProcedimentos->digitado) ?></td>
                <td><?= h($atendimentoProcedimentos->material) ?></td>
                <td><?= h($atendimentoProcedimentos->num_controle) ?></td>
                <td><?= h($atendimentoProcedimentos->medico_id) ?></td>
                <td><?= h($atendimentoProcedimentos->valor_matmed) ?></td>
                <td><?= h($atendimentoProcedimentos->documento_guia) ?></td>
                <td><?= h($atendimentoProcedimentos->situacao_id) ?></td>
                <td><?= h($atendimentoProcedimentos->user_id) ?></td>
                <td><?= h($atendimentoProcedimentos->created) ?></td>
                <td><?= h($atendimentoProcedimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'AtendimentoProcedimentos','action' => 'view', $atendimentoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'AtendimentoProcedimentos','action' => 'edit', $atendimentoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'AtendimentoProcedimentos','action' => 'delete', $atendimentoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $procedimento->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Preco Procedimentos') ?></h4>
        <?php if (!empty($procedimento->preco_procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Convenio Id') ?></th>
                <th><?= __('Procedimento Id') ?></th>
                <th><?= __('Valor Faturar') ?></th>
                <th><?= __('Valor Particular') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($procedimento->preco_procedimentos as $precoProcedimentos): ?>
            <tr>
                <td><?= h($precoProcedimentos->id) ?></td>
                <td><?= h($precoProcedimentos->convenio_id) ?></td>
                <td><?= h($precoProcedimentos->procedimento_id) ?></td>
                <td><?= h($precoProcedimentos->valor_faturar) ?></td>
                <td><?= h($precoProcedimentos->valor_particular) ?></td>
                <td><?= h($precoProcedimentos->user_id) ?></td>
                <td><?= h($precoProcedimentos->created) ?></td>
                <td><?= h($precoProcedimentos->modified) ?></td>
                <td><?= h($precoProcedimentos->situacao_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'PrecoProcedimentos','action' => 'view', $precoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'PrecoProcedimentos','action' => 'edit', $precoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'PrecoProcedimentos','action' => 'delete', $precoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $procedimento->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

