<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Procedimentos</li>
            <li class="active">
                <strong> Cadastrar Procedimentos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="procedimentos form">
                        <?= $this->Form->create($procedimento,['id'=>'frm-procedimento']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Procedimento') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('grupo_id', ['options' => $grupoProcedimentos]);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('anotacao_folha', ['label' => 'Anotação Folha']);
                            echo "</div>";
                            echo "<div class='col-md-6 ".$this->Configuracao->showPrevEntrega()."'>";
                            echo $this->Form->input('dias_entrega', ['label' => 'Dias para Entrega']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('restauracao',['label'=>' Restauração?']);
                            echo "</div>";
                            ?>
                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

