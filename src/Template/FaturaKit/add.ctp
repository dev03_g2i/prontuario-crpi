<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Kit</h2>
            <ol class="breadcrumb">
                <li>Fatura Kit</li>
                <li class="active">
                    <strong>
                        Cadastrar Fatura Kit
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaKit form">
                            <?= $this->Form->create($faturaKit) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Fatura Kit') ?></legend>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('nome'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('procedimento_id', ['data'=>'select','controller'=>'procedimentos','action'=>'fill',  'empty' => 'selecione']); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

