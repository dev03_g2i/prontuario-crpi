<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Kit</h2>
            <ol class="breadcrumb">
                <li>Fatura Kit</li>
                <li class="active">
                    <strong>Litagem de Fatura Kit</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Fatura Kit</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Id') ?></th>
                                    <td><?= $this->Number->format($faturaKit->id) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Nome') ?></th>
                                    <td><?= h($faturaKit->nome) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Procedimento') ?></th>
                                    <td>
                                        <?= $faturaKit->has('procedimento') ? $this->Html->link($faturaKit->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $faturaKit->procedimento->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Cadastrado Por') ?>
                                    </th>
                                    <td>
                                        <?= $faturaKit->has('user_reg') ? $this->Html->link($faturaKit->user_reg->nome, ['controller' => 'Users', 'action' => 'view', $faturaKit->user_id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Alterado Por') ?>
                                    </th>
                                    <td>
                                        <?= $faturaKit->has('user_alt') ? $this->Html->link($faturaKit->user_alt->nome, ['controller' => 'Users', 'action' => 'view', $faturaKit->user_update]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situação Cadastro') ?>
                                    </th>
                                    <td>
                                        <?= $faturaKit->has('situacao_cadastro') ? $this->Html->link($faturaKit->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaKit->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($faturaKit->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($faturaKit->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>