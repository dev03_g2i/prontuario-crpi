<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Kit</h2>
            <ol class="breadcrumb">
                <li>Fatura Kit</li>
                <li class="active">
                    <strong>Litagem de Fatura Kit</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaKit',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?=$this->Form->input('nome',['name'=>'FaturaKit__nome']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('procedimento_id', ['name'=>'FaturaKit__procedimento_id','data'=>'select','controller'=>'procedimentos','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Fatura Kit', ['action' => 'add', 'first'],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fatura Kit','class'=>'btn btn-primary','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fatura Kit') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Cadastrado Por']) ?></th>
                                        <th><?= $this->Paginator->sort('user_update',['label'=>'Alterado Por']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaKit as $faturaKit): ?>
                                        <tr>
                                            <td><?= $this->Number->format($faturaKit->id) ?></td>
                                            <td><?= h($faturaKit->nome) ?></td>
                                            <td><?= $faturaKit->has('procedimento') ? $this->Html->link($faturaKit->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $faturaKit->procedimento->id]) : '' ?></td>
                                            <td><?= $faturaKit->has('user_reg') ? $this->Html->link($faturaKit->user_reg->nome, ['controller' => 'Users', 'action' => 'view', $faturaKit->user_id]) : '' ?> - <?=$faturaKit->created ?></td>
                                            <td><?= $faturaKit->has('user_alt') ? $this->Html->link($faturaKit->user_alt->nome, ['controller' => 'Users', 'action' => 'view', $faturaKit->user_update]).' - '.$faturaKit->modified : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaKit','action' => 'view', $faturaKit->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaKit','action' => 'edit', $faturaKit->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link('<i class="fa fa-medkit"></i>', ['controller'=>'faturaKitartigo','action' => 'index', 'fatura_kit_id' => $faturaKit->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Adicionar artigos','escape' => false,'class'=>'btn btn-xs btn-success', 'target' => '_blank']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturaKit','action'=>'delete', $faturaKit->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
