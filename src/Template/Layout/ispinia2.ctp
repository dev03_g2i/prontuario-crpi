<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    if(!empty($title)){
        $this->assign('title', $title);
    }
    ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('favicon.ico', '/img/g2i.png', ['type' => 'icon']) ?>
    <?php echo $this->element('header_ispinia2'); ?>

</head>

<body class="top-navigation" data-spy="scroll">
<div id="wrapper">
    <div class="row border-bottom white-bg">
        <?=$this->element('menu_ispinia2')?>
    </div>
    <div id="page-wrapper" class="gray-bg" style="margin-top: 20px;">
        <?= $this->Flash->render('auth') ?>
        <?= $this->Flash->render() ?>
        <div class="wrapper wrapper-content">
            <?= $this->fetch('content') ?>
        </div>
        <div class="footer">
            <div><strong>Copyright</strong> G2i &copy; 2014-<?= date('Y') ?></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php echo $this->element('permissoes'); ?>
<?php echo $this->element('footer_ispinia2'); ?>
<?php echo $this->element('modais'); ?>
<div class="load-gif hidden"><div class="img-gif"><?=$this->Html->image('gif-g2i.gif')?></div></div>
<div class="append-report"></div>
<script type="text/javascript">
    $(document).ready(function(){
        LoadGif();
    })
    $(window).load(function(){
        initDocument(document);
        CloseGif();
    })
</script>

<!--Start of Tawk.to Script-->
<?php if($this->Configuracao->usaChat()) : ?>
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    Tawk_API.visitor = {
        name : '<?= ($this->Configuracao->getNomeUser()) ?>',
        email : '<?= ($this->Configuracao->getEmailUser()) ?>'
    };
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5abcef3bd7591465c7090a0b/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
<?php endif; ?>
</body>
</html>