<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    if(!empty($title)){
        $this->assign('title', $title);
    }
    ?>
    <title>
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('favicon.ico', '/img/g2i.png', ['type' => 'icon']) ?>
    <?php echo $this->element('header_ispinia'); ?>

</head>

<body>
<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <?php echo $this->element('menu_ispinia'); ?>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg" >
<!--        inclui o menu superior-->
        <?php echo $this->element('menu_topo'); ?>
        <?= $this->Flash->render('auth') ?>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
        <div class="footer fixed">
            <div>
                <strong>Copyright</strong> G2i &copy; 2014-<?= date('Y') ?>
            </div>
        </div>
</div>
</div>
<div class="clearfix"></div>
<?php echo $this->element('permissoes'); ?>
<?php echo $this->element('footer_ispinia'); ?>
<?php echo $this->element('modais'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        LoadGif();
    })
    $(window).load(function(){
        initDocument(document)
        CloseGif();
    })
</script>
</body>

</html>
