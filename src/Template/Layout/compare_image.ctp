<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?php echo $this->element('header_ispinia2'); ?>

    <style type="text/css">
        .lg-close {display: none !important;}
    </style>
</head>
<body>
<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>
<?php echo $this->element('permissoes'); ?>
<?php echo $this->element('footer_ispinia2'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#lightgallery').lightGallery();

    });
</script>
</body>
</html>