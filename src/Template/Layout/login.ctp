<!DOCTYPE html>
<html>
<head>
	<title>
        G2i Medical
    </title>
<?= $this->Html->meta('favicon.ico','/img/g2i.png',['type'=>'icon']) ?>
<?php echo $this->element('header_login'); ?>
</head>
<body class="gray-bg">
<header class="main-header">
    <nav id="nav" class="navbar navbar-default navbar-static-top" role="navigation" style="background-color: #fff; padding: 10px 0 7px 0;">
        <div class="container">
            <div class="navbar-header">
                <a href="<?php echo $this->Url->build('http://www.grupog2i.com.br') ?>">
                    <?= $this->Html->image('logo.fw.png',['class'=>'img-responsive','alt'=>'logo'])?>
                </a>
            </div>
            <div class="navbar-header" style="float: right; margin-top: 30px;">
                <a href="<?php echo $this->Url->build('http://www.grupog2i.com.br') ?>" style="font-family: 'open sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; color: #063759; font-size: 18px">
                    www.grupog2i.com.br
                </a>
            </div>
        </div>
    </nav>
</header>
<?= $this->Flash->render() ?>
<?= $this->Flash->render('auth') ?>

<div style="max-width: 500px; margin: auto;text-align: center;padding-top: 50px;">
    <h4 class="logo-light"><?= $configurar->nome_login; ?></h4>
</div>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <?= $this->fetch('content') ?>
</div>
<footer class="main-footer" style="margin-top: 70px">
    <div class="footer-area">
        <div class="container">
            <div class="row" style="text-align: left; font-size: 14px">
                <div class="col-sm-8">
                    <div class="col-sm-12">
                        Av. Afonso Pena, 3504 - 15 Andar - Sala 151 Centro
                    </div>
                    <div class="col-sm-12">
                        Campo Grande - MS.
                    </div>
                    <div class="col-sm-12">
                        <a href="https://goo.gl/maps/FjFZTkEK4qC2" style="color: #063759;" target="_blank">Ver no mapa</a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="col-sm-12">
                        <i class="fa fa-phone" style="font-size: 14px"></i>
                        <span>+55 67 3027 7847</span>
                        </br>
                        <i class="fa fa-phone" style="font-size: 14px"></i>
                        <span>+55 67 3029 7847</span>
                    </div>
                    <div class="col-sm-12">
                        <a href="mailto:contato@grupog2i..com.br">suporte@grupog2i.com.br</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container text-center">
            <strong>Copyright</strong> G2i &copy; 2014-<?= date('Y') ?>
        </div>
    </div>
</footer>
<?php echo $this->element('footer_login'); ?>
</body>
</html>
