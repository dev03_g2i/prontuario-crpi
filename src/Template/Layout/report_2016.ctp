<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?php echo $this->element('header_report_2016'); ?>

 
</head>
<body>
<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>

</body>
</html>