<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Retornos</h2>
        <ol class="breadcrumb">
            <li>Grupo Retornos</li>
            <li class="active">
                <strong>Litagem de Grupo Retornos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Grupo Retornos', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Grupo Retornos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Grupo Retornos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('user_id', ['label' => 'Quem Cadastrou']) ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($grupoRetornos as $grupoRetorno): ?>
                                    <tr>
                                        <td><?= $this->Number->format($grupoRetorno->id) ?></td>
                                        <td><?= h($grupoRetorno->nome) ?></td>
                                        <td><?= $grupoRetorno->has('user') ? $this->Html->link($grupoRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoRetorno->user->id]) : '' ?></td>
                                        <td><?= h($grupoRetorno->created) ?></td>
                                        <td><?= h($grupoRetorno->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Retornos', 'action' => 'index', '?' => ['grupo' => $grupoRetorno->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Retornos', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $grupoRetorno->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $grupoRetorno->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $grupoRetorno->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

