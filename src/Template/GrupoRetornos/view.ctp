<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Retornos</h2>
        <ol class="breadcrumb">
            <li>Grupo Retornos</li>
            <li class="active">
                <strong>Litagem de Grupo Retornos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Grupo Retornos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($grupoRetorno->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quem Cadastrou') ?></th>
                                <td><?= $grupoRetorno->has('user') ? $this->Html->link($grupoRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoRetorno->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situação Cadastro') ?></th>
                                <td><?= $grupoRetorno->has('situacao_cadastro') ? $this->Html->link($grupoRetorno->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoRetorno->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($grupoRetorno->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dt. Criação') ?></th>
                                <td><?= h($grupoRetorno->created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dt. Modificação') ?></th>
                                <td><?= h($grupoRetorno->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


