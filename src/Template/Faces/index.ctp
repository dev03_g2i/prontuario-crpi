<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Faces</h2>
        <ol class="breadcrumb">
            <li>Faces</li>
            <li class="active">
                <strong>Litagem de Faces</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Faces', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Faces', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Faces') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                    <th><?= $this->Paginator->sort('user_id') ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($faces as $face): ?>
                                    <tr>
                                        <td><?= $this->Number->format($face->id) ?></td>
                                        <td><?= h($face->nome) ?></td>
                                        <td><?= $face->has('situacao_cadastro') ? $this->Html->link($face->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $face->situacao_cadastro->id]) : '' ?></td>
                                        <td><?= $face->has('user') ? $this->Html->link($face->user->nome, ['controller' => 'Users', 'action' => 'view', $face->user->id]) : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $face->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $face->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $face->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $face->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

