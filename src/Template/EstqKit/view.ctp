<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estq Kit</h2>
            <ol class="breadcrumb">
                <li>Estq Kit</li>
                <li class="active">
                    <strong>Litagem de Estq Kit</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Estq Kit</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Nome') ?>
                                    </th>
                                    <td>
                                        <?= h($estqKit->nome) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Procedimento') ?>
                                    </th>
                                    <td>
                                        <?= $estqKit->has('procedimento') ? $this->Html->link($estqKit->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $estqKit->procedimento->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situação Cadastro') ?>
                                    </th>
                                    <td>
                                        <?= $estqKit->has('situacao_cadastro') ? $this->Html->link($estqKit->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $estqKit->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($estqKit->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $estqKit->has('user_reg') ? $this->Html->link($estqKit->user_reg->nome, ['controller' => 'Users', 'action' => 'view', $estqKit->user_reg->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Alterado Por') ?>
                                    </th>
                                    <td>
                                    <?= $estqKit->has('user_alt') ? $this->Html->link($estqKit->user_alt->nome, ['controller' => 'Users', 'action' => 'view', $estqKit->user_alt->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($estqKit->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($estqKit->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>