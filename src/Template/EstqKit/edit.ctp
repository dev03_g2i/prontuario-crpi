<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estq Kit</h2>
            <ol class="breadcrumb">
                <li>Estq Kit</li>
                <li class="active">
                    <strong>
                        Editar Estq Kit
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="estqKit form">
                            <?= $this->Form->create($estqKit) ?>
                            <?php require_once('form.ctp'); ?>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>