<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Kit</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('EstqKit',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?=$this->Form->input('nome',['name'=>'EstqKit__nome']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('procedimento_id', ['class' => 'select2', 'name' => 'EstqKit__procedimento_id',   'options' => $procedimentos, 'empty' => 'Selecione']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add', 'first'],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar','class'=>'btn btn-primary','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Cadastrado Por']) ?></th>
                                        <th><?= $this->Paginator->sort('user_update',['label'=>'Alterado Por']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($estqKit as $ek): ?>
                                        <tr>
                                            <td><?= $this->Number->format($ek->id) ?></td>
                                            <td><?= h($ek->nome) ?></td>
                                            <td><?= $ek->has('procedimento') ? $this->Html->link($ek->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $ek->procedimento->id]) : '' ?></td>
                                            <td><?= $ek->has('user_reg') ? $this->Html->link($ek->user_reg->nome, ['controller' => 'Users', 'action' => 'view', $ek->user_id]) : '' ?> - <?=$ek->created ?></td>
                                            <td><?= $ek->has('user_alt') ? $this->Html->link($ek->user_alt->nome, ['controller' => 'Users', 'action' => 'view', $ek->user_update]).' - '.$ek->modified : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'EstqKit','action' => 'view', $ek->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'EstqKit','action' => 'edit', $ek->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <!--<?= $this->Html->link('<i class="fa fa-medkit"></i>', ['controller'=>'EstqKit','action' => 'index', 'fatura_kit_id' => $ek->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Adicionar artigos','escape' => false,'class'=>'btn btn-xs btn-success', 'target' => '_blank']) ?>-->
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'EstqKit','action'=>'delete', $ek->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
