<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Recursos</h2>
            <ol class="breadcrumb">
                <li>Fatura Recursos</li>
                <li class="active">
                    <strong>Listagem de Fatura Recursos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaRecurso',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('data', ['name'=>'FaturaRecursos__data','empty' => true,'type'=>'text','class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('situacao_fatura_recurso_id', ['name' => 'FaturaRecursos__situacao_fatura_recurso_id', 'label' => 'Situação', 'options' => $sitFaturaRecursos, 'empty' => 'Selecione']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'fill-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'refresh-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Fatura Recursos',  ['action' => 'add', $atendProc_id], ['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fatura Recursos','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fatura Recursos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('data') ?></th>
                                        <th><?= $this->Paginator->sort('valor') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_fatura_recurso_id', 'Situação') ?></th>
                                        <th><?= $this->Paginator->sort('obs', 'Observação') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaRecursos as $faturaRecurso): ?>
                                        <tr>
                                            <td><?= $this->Number->format($faturaRecurso->id) ?></td>
                                            <td><?= h($faturaRecurso->data) ?></td>
                                            <td><?= $this->Number->format($faturaRecurso->valor) ?></td>
                                            <td><?= $faturaRecurso->has('situacao_fatura_recurso') ? $faturaRecurso->situacao_fatura_recurso->nome : '' ?></td>
                                            <td><?= $faturaRecurso->obs ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaRecursos','action' => 'view', $faturaRecurso->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaRecursos','action' => 'edit', $faturaRecurso->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturaRecursos','action'=>'delete', $faturaRecurso->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
