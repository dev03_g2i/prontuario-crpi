<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Recursos</h2>
            <ol class="breadcrumb">
                <li>Fatura Recursos</li>
                <li class="active">
                    <strong>
                        Editar Fatura Recursos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaRecursos form">
                            <?= $this->Form->create($faturaRecurso) ?>
                            <fieldset>
                                <legend><?= __('Editar Fatura Recurso') ?></legend>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('data', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($faturaRecurso->data,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('valor',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('situacao_fatura_recurso_id', ['name' => 'FaturaRecursos__situacao_fatura_recurso_id', 'label' => 'Situação', 'options' => $sitFaturaRecursos, 'empty' => 'Selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('obs', ['rows' => 2]); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

