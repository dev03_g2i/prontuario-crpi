
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Recursos</h2>
        <ol class="breadcrumb">
            <li>Fatura Recursos</li>
            <li class="active">
                <strong>Litagem de Fatura Recursos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Recursos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Situacao Fatura Recurso') ?></th>
                                                                <td><?= $faturaRecurso->has('situacao_fatura_recurso') ? $this->Html->link($faturaRecurso->situacao_fatura_recurso->id, ['controller' => 'SituacaoFaturaRecursos', 'action' => 'view', $faturaRecurso->situacao_fatura_recurso->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento Procedimento') ?></th>
                                                                <td><?= $faturaRecurso->has('atendimento_procedimento') ? $this->Html->link($faturaRecurso->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $faturaRecurso->atendimento_procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faturaRecurso->has('user') ? $this->Html->link($faturaRecurso->user->nome, ['controller' => 'Users', 'action' => 'view', $faturaRecurso->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturaRecurso->has('situacao_cadastro') ? $this->Html->link($faturaRecurso->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaRecurso->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaRecurso->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor') ?></th>
                                <td><?= $this->Number->format($faturaRecurso->valor) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($faturaRecurso->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaRecurso->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaRecurso->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Obs') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($faturaRecurso->obs)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


