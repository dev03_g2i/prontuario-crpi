
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Documentos</h2>
        <ol class="breadcrumb">
            <li>Atendimento Documentos</li>
            <li class="active">
                <strong>Litagem de Atendimento Documentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Atendimento Documentos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Atendimento Modelo Documento') ?></th>
                                                                <td><?= $atendimentoDocumento->has('atendimento_modelo_documento') ? $this->Html->link($atendimentoDocumento->atendimento_modelo_documento->id, ['controller' => 'AtendimentoModeloDocumentos', 'action' => 'view', $atendimentoDocumento->atendimento_modelo_documento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento') ?></th>
                                                                <td><?= $atendimentoDocumento->has('atendimento') ? $this->Html->link($atendimentoDocumento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $atendimentoDocumento->atendimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $atendimentoDocumento->has('user') ? $this->Html->link($atendimentoDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $atendimentoDocumento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Total') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->valor_total) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Meses') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->meses) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Parcela') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->valor_parcela) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Vencimento') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->vencimento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Responsavel Id') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->responsavel_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Id') ?></th>
                                <td><?= $this->Number->format($atendimentoDocumento->situacao_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Primeiro Pagamento') ?></th>
                                                                <td><?= h($atendimentoDocumento->primeiro_pagamento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Contrato') ?></th>
                                                                <td><?= h($atendimentoDocumento->data_contrato) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($atendimentoDocumento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($atendimentoDocumento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Texto Contrato') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($atendimentoDocumento->texto_contrato)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


