<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-md-6">
            <h2>Atendimento Documentos</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
               <!--<div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('AtendimentoDocumento',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('valor_total',['name'=>'AtendimentoDocumentos__valor_total','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('meses',['name'=>'AtendimentoDocumentos__meses']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('valor_parcela',['name'=>'AtendimentoDocumentos__valor_parcela','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('vencimento',['name'=>'AtendimentoDocumentos__vencimento']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?= $this->Form->input('primeiro_pagamento', ['name'=>'AtendimentoDocumentos__primeiro_pagamento','empty' => true,'type'=>'text','class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?= $this->Form->input('data_contrato', ['name'=>'AtendimentoDocumentos__data_contrato','empty' => true,'type'=>'text','class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?= $this->Form->input('atendimento_modelo_documento_id', ['name'=>'AtendimentoDocumentos__atendimento_modelo_documento_id','data'=>'select','controller'=>'atendimentoModeloDocumentos','action'=>'fill', 'empty' => 'Selecione']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?= $this->Form->input('atendimento_id', ['name'=>'AtendimentoDocumentos__atendimento_id','data'=>'select','controller'=>'atendimentos','action'=>'fill', 'empty' => 'Selecione']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('responsavel_id',['name'=>'AtendimentoDocumentos__responsavel_id']); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('situacao_id',['name'=>'AtendimentoDocumentos__situacao_id']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>-->
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add', $atendimento_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Atendimento Documentos','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('atendimento_modelo_documento_id', 'Documento') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('responsavel_id', 'Responsavel') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('data_emissao') ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($atendimentoDocumentos as $atendimentoDocumento): ?>
                                        <tr class="delete<?=$atendimentoDocumento->id?>">
                                            <td>
                                                <?= $atendimentoDocumento->has('atendimento_modelo_documento') ? $this->Html->link($atendimentoDocumento->atendimento_modelo_documento->nome, ['controller' => 'AtendimentoModeloDocumentos', 'action' => 'view', $atendimentoDocumento->atendimento_modelo_documento->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoDocumento->has('cliente_responsavei') ? $this->Html->link($atendimentoDocumento->cliente_responsavei->nome, ['controller' => 'ClienteResponsaveis', 'action' => 'view', $atendimentoDocumento->cliente_responsavei->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoDocumento->data_emissao ?>
                                            </td>
                                            <td class="actions">
                                                <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'atendimentoDocumentos','action' => 'view', $atendimentoDocumento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>-->
                                                <?= $this->Html->link('<i class="fa fa-paste"></i>', ['controller'=>'atendimentoDocumentos','action' => 'edit', $atendimentoDocumento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Abrir/Editar Documento','escape' => false,'class'=>'btn btn-xs btn-primary']) ?> 
                                                <?= $this->Form->button($this->Html->icon('remove'),['onclick'=>'DeletarModal(\'AtendimentoDocumentos\','.$atendimentoDocumento->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>