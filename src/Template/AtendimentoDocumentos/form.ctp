<div class='col-md-6'>
    <?=$this->Form->input('atendimento_modelo_documento_id', ['label' => 'Documento', 'options' => $documentos, 'empty' => 'Selecione']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('responsavel_id', ['label' => 'Responsavel', 'options' => $responsaveis, 'empty' => 'Selecione']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('data_emissao', ['label' => 'Dt. Emissão', 'type'=>'text','class'=>'datepicker', 'value' => @date_format($atendimentoDocumento->data_emissao, 'd/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
</div>

<!--<div class='col-md-6'>
    <?=$this->Form->input('valor_total',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('meses'); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('valor_parcela',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('parcelas',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('vencimento'); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('primeiro_pagamento', ['empty' => true,'type'=>'text','class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
</div>-->
<?php if($this->template == 'edit'): ?>
    <div class='col-md-12'>
        <?=$this->Form->input('texto_documento', ['data' => 'ck']); ?>
    </div>
<?php endif; ?>