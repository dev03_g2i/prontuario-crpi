<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitante Cbo</h2>
            <ol class="breadcrumb">
                <li>Solicitante Cbo</li>
                <li class="active">
                    <strong>
                                                Editar Solicitante Cbo
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="solicitanteCbo form">
                            <?= $this->Form->create($solicitanteCbo) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Solicitante Cbo') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('codigo_tiss'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_insert'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_update'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

