
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Solicitante Cbo</h2>
        <ol class="breadcrumb">
            <li>Solicitante Cbo</li>
            <li class="active">
                <strong>Litagem de Solicitante Cbo</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Solicitante Cbo</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($solicitanteCbo->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Codigo Tiss') ?></th>
                                <td><?= h($solicitanteCbo->codigo_tiss) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $solicitanteCbo->has('situacao_cadastro') ? $this->Html->link($solicitanteCbo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $solicitanteCbo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($solicitanteCbo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Insert') ?></th>
                                <td><?= $this->Number->format($solicitanteCbo->user_insert) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Update') ?></th>
                                <td><?= $this->Number->format($solicitanteCbo->user_update) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($solicitanteCbo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($solicitanteCbo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


