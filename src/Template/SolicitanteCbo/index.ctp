<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitante Cbo</h2>
            <ol class="breadcrumb">
                <li>Solicitante Cbo</li>
                <li class="active">
                    <strong>Listagem de Solicitante Cbo</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('SolicitanteCbo',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('descricao',['name'=>'SolicitanteCbo__descricao']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('codigo_tiss',['name'=>'SolicitanteCbo__codigo_tiss']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('user_insert',['name'=>'SolicitanteCbo__user_insert']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('user_update',['name'=>'SolicitanteCbo__user_update']); ?>
                                                                                    </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Solicitante Cbo', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Solicitante Cbo','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Solicitante Cbo') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('descricao') ?></th>
                                                                                <th><?= $this->Paginator->sort('codigo_tiss') ?></th>
                                                                                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('user_insert') ?></th>
                                                                                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                                                                <th><?= $this->Paginator->sort('user_update') ?></th>
                                                                                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($solicitanteCbo as $solicitanteCbo): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($solicitanteCbo->id) ?></td>
                                                                                <td><?= h($solicitanteCbo->descricao) ?></td>
                                                                                <td><?= h($solicitanteCbo->codigo_tiss) ?></td>
                                                                                <td><?= $solicitanteCbo->has('situacao_cadastro') ? $this->Html->link($solicitanteCbo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $solicitanteCbo->situacao_cadastro->id]) : '' ?></td>
                                                                                <td><?= $this->Number->format($solicitanteCbo->user_insert) ?></td>
                                                                                <td><?= h($solicitanteCbo->created) ?></td>
                                                                                <td><?= $this->Number->format($solicitanteCbo->user_update) ?></td>
                                                                                <td><?= h($solicitanteCbo->modified) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'solicitanteCbo','action' => 'view', $solicitanteCbo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'solicitanteCbo','action' => 'edit', $solicitanteCbo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'solicitanteCbo','action'=>'delete', $solicitanteCbo->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
