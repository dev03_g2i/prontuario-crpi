<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agenda Fila de espera</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('AgendaFilaespera',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('tipo_espera_id',['label' => 'Status', 'name'=>'AgendaFilaespera__tipo_espera_id', 'options' => $tipo_espera, 'empty' => 'Selecione']); ?>
                                </div>
                                <div class="col-md-4" style="margin-top: 23px;">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'fill-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'refresh-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>              

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="text-right">
                            <p>
                                <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Agenda Filaespera','class'=>'btn btn-primary','escape' => false]) ?>
                            </p>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('paciente') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('fone') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('convenio_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('tipo_espera_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('grupo_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('dt_agendado', 'Dt.Interesse') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($agendaFilaespera as $agendaFilaespera): ?>
                                        <tr class="delete<?=$agendaFilaespera->id?>">
                                            <td>
                                                <?= h($agendaFilaespera->paciente) ?>
                                            </td>
                                            <td>
                                                <?= h($agendaFilaespera->fone) ?>
                                            </td>
                                            <td>
                                                <?= $agendaFilaespera->has('convenio') ? $this->Html->link($agendaFilaespera->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $agendaFilaespera->convenio->id]) : '' ?>
                                            </td>
                                            <td>
                                            <?= $agendaFilaespera->has('tipo_esperaagenda') ? $this->Html->link($agendaFilaespera->tipo_esperaagenda->descricao, ['controller' => 'TipoEsperaagenda', 'action' => 'view', $agendaFilaespera->tipo_esperaagenda->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $agendaFilaespera->has('grupo_agenda') ? $this->Html->link($agendaFilaespera->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agendaFilaespera->grupo_agenda->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= h($agendaFilaespera->dt_agendado) ?>
                                            </td>
                                            <td>
                                                <?= $agendaFilaespera->has('user') ? $this->Html->link($agendaFilaespera->user->nome, ['controller' => 'Users', 'action' => 'view', $agendaFilaespera->user->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= h($agendaFilaespera->created) ?>
                                            </td>
                                            <td>
                                                <?= h($agendaFilaespera->modified) ?>
                                            </td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'agendaFilaespera','action' => 'view', $agendaFilaespera->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'agendaFilaespera','action' => 'edit', $agendaFilaespera->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Form->button($this->Html->icon('remove'), ['onclick'=>'DeletarModal(\'agendaFilaespera\','.$agendaFilaespera->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>