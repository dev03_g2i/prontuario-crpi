<div class='col-md-6'>
    <?=$this->Form->input('paciente'); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('fone', ['mask' => 'fone']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('convenio_id', ['data'=>'select','controller'=>'convenios','action'=>'fill']); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('tipo_espera_id', ['options' => $tipo_espera, 'default' => 1]); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('grupo_id', ['options' => $grupos, 'empty' => 'Selecione', 'required' => true]); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('dt_agendado', ['label' => 'Dt.Interesse', 'class' => 'datetimepicker', 'type' => 'text', 'required' => true]); ?>
</div>
<div class="col-md-12 text-right">
    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
</div>