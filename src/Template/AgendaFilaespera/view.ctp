<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agenda Filaespera</h2>
            <ol class="breadcrumb">
                <li>Agenda Filaespera</li>
                <li class="active">
                    <strong>Listagem de Agenda Filaespera</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Agenda Filaespera</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Paciente') ?>
                                    </th>
                                    <td>
                                        <?= h($agendaFilaespera->paciente) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Fone') ?>
                                    </th>
                                    <td>
                                        <?= h($agendaFilaespera->fone) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Convenio') ?>
                                    </th>
                                    <td>
                                        <?= $agendaFilaespera->has('convenio') ? $this->Html->link($agendaFilaespera->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $agendaFilaespera->convenio->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $agendaFilaespera->has('user') ? $this->Html->link($agendaFilaespera->user->nome, ['controller' => 'Users', 'action' => 'view', $agendaFilaespera->user->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Tipo Espera') ?>
                                    </th>
                                    <td>
                                        <?= $agendaFilaespera->has('tipo_esperaagenda') ? $this->Html->link($agendaFilaespera->tipo_esperaagenda->descricao, ['controller' => 'TipoEsperaagenda', 'action' => 'view', $agendaFilaespera->tipo_esperaagenda->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Grupo') ?>
                                    </th>
                                    <td>
                                        <?= $agendaFilaespera->has('grupo_agenda') ? $this->Html->link($agendaFilaespera->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agendaFilaespera->grupo_agenda->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situacao') ?>
                                    </th>
                                    <td>
                                    <?= $agendaFilaespera->has('situacao_cadastro') ? $this->Html->link($agendaFilaespera->situacao_cadastro->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agendaFilaespera->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt.Interesse') ?>
                                    </th>
                                    <td>
                                        <?= h($agendaFilaespera->dt_agendado) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($agendaFilaespera->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($agendaFilaespera->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>