<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agenda Filaespera</h2>
            <ol class="breadcrumb">
                <li>Agenda Filaespera</li>
                <li class="active">
                    <strong>
                        Cadastrar Agenda Filaespera
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="agendaFilaespera form">
                            <?= $this->Form->create($agendaFilaespera) ?>
                                <?php require_once('form.ctp'); ?>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>