<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Gastos</h2>
            <ol class="breadcrumb">
                <li>Gastos</li>
                <li class="active">
                    <strong>Litagem de Gastos</strong>
                </li>
            </ol>
        </div>
        <div class="text-right">
            <a href="Javascript:void(0)" onclick="Navegar('','back')" class="btn btn-default btn-xs"><?= $this->Html->icon('arrow-left') ?></a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Ficha</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Nome') ?></th>
                                    <th><?= __('Dt. Criação') ?></th>
                                    <th><?= __('Dt. Modificação') ?></th>
                                </tr>
                                <tr>
                                    <td><?= h($gasto->nome) ?></td>
                                    <td><?= h($gasto->created) ?></td>
                                    <td><?= h($gasto->modified) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3><?= __('Observacao') ?></h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(h($gasto->observacao)); ?>
                    </div>
                </div>
                <?php if(!empty($gasto->gasto_itens)) : ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Intens</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Artigo') ?></th>
                                    <th><?= __('Quantidade') ?></th>
                                    <th><?= __('Dt. Criação') ?></th>
                                    <th><?= __('Ult. Atualização') ?></th>
                                </tr>
                                <?php foreach ($gasto->gasto_itens as $g): ?>
                                    <tr>
                                        <td><?= $g->has('estq_artigo') ? h($g->estq_artigo->nome) : '' ?></td>
                                        <td><?= h($g->quantidade) ?></td>
                                        <td><?= h($g->created) ?></td>
                                        <td><?= h($g->modified) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                    </div>
                </div>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>


