
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Documentos</h2>
        <ol class="breadcrumb">
            <li>Lista Documentos</li>
            <li class="active">
                <strong>Cadastrar Lista Documentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="listaDocumentos form">
    <?= $this->Form->create($listaDocumento,['type' => 'file','id'=>'form-resp']) ?>
    <fieldset>
                <legend><?= __('Cadastrar Lista Documento') ?></legend>
                <?php
                if(!empty($documento_id)){
                    echo $this->Form->input('documento_id', ['value' => $documento_id,'type'=>'hidden']);
                }else {
                    echo "<div class='col-md-6'>";
                    echo $this->Form->input('documento_id', ['options' => $clienteDocumentos]);
                    echo "</div>";
                }

        ?>
        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="form-group">
                <input id="files" type="file" name="caminho[]" multiple class="input-file"
                       data-overwrite-initial="false" data-min-file-count="1">
            </div>
        </div>
    </fieldset>
    <div class="progress" style="display: none;">
        <div class="progress-bar progress-bar-striped active" role="progressbar"
             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span class="sr-only">0% Complete</span>
        </div>
    </div>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'senduploads("form-resp","ListaDocumentos","index")']) ?>

    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

