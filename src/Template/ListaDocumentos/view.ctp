

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Documentos</h2>
        <ol class="breadcrumb">
            <li>Lista Documentos</li>
            <li class="active">
                <strong>Litagem de Lista Documentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="listaDocumentos">
    <h3><?= h($listaDocumento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Cliente Documento') ?></th>
            <td><?= $listaDocumento->has('cliente_documento') ? $this->Html->link($listaDocumento->cliente_documento->descricao, ['controller' => 'ClienteDocumentos', 'action' => 'view', $listaDocumento->cliente_documento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caminho') ?></th>
            <td><?= h($listaDocumento->caminho) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $listaDocumento->has('situacao_cadastro') ? $this->Html->link($listaDocumento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $listaDocumento->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $listaDocumento->has('user') ? $this->Html->link($listaDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $listaDocumento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($listaDocumento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($listaDocumento->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($listaDocumento->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

