<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Documentos</h2>
        <ol class="breadcrumb">
            <li>Lista Documentos</li>
            <li class="active">
                <strong>Litagem de Lista Documentos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Novo Documento', ['action' => 'add', $cliente_documento->id], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Documento', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Lista Documentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('caminho', ['label' => 'Descrição']) ?></th>
                                    <th><?= $this->Paginator->sort('caminho', ['label' => 'Arquivo']) ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Data cadastro']) ?></th>
                                    <th><?= $this->Paginator->sort('user_id', ['label' => 'Quem cadastrou']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($listaDocumentos as $listaDocumento): ?>
                                    <tr>
                                        <td><?= h($listaDocumento->caminho) ?></td>
                                        <td><?php
                                            $ext = explode('.', $listaDocumento->caminho);
                                            if (in_array($ext[1], ['jpg', 'gif', 'png'])) {
                                                echo $this->Html->image('/files/listadocumentos/caminho/' . $listaDocumento->caminho_dir . '/portrait_' . $listaDocumento->caminho);
                                            } else {
                                                echo $this->Html->image('/img/default.png');
                                            }
                                            ?>
                                        </td>
                                        <td><?= h($listaDocumento->created) ?></td>
                                        <td><?= $listaDocumento->has('user') ? $this->Html->link($listaDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $listaDocumento->user->id]) : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'ListaDocumentos\',' . $listaDocumento->id . ',\'carregarAba\',' . $cliente_documento->cliente_id . ')', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

