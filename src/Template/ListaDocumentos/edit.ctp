
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Documentos</h2>
        <ol class="breadcrumb">
            <li>Lista Documentos</li>
            <li class="active">
                <strong>                        Editar Lista Documentos
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="listaDocumentos form">
    <?= $this->Form->create($listaDocumento) ?>
    <fieldset>
                <legend><?= __('Editar Lista Documento') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('documento_id', ['options' => $clienteDocumentos]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('caminho');
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('situacao_id', ['options' => $situacaoCadastros]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('user_id', ['options' => $users]);
                echo "</div>";
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

