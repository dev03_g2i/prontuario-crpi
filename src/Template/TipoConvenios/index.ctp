
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Convênios</h2>
        <ol class="breadcrumb">
            <li>Tipo Convênios</li>
            <li class="active">
                <strong>Listagem de Tipo Convênios</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Tipo Convênios', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Tipo Convênios','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Tipo Convênios') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipoConvenios as $tipoConvenio): ?>
            <tr>
                <td><?= $this->Number->format($tipoConvenio->id) ?></td>
                <td><?= h($tipoConvenio->nome) ?></td>
                <td><?= $tipoConvenio->has('user') ? $this->Html->link($tipoConvenio->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoConvenio->user->id]) : '' ?></td>
                <td><?= $tipoConvenio->has('situacao_cadastro') ? $this->Html->link($tipoConvenio->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoConvenio->situacao_cadastro->id]) : '' ?></td>
                <td><?= h($tipoConvenio->created) ?></td>
                <td><?= h($tipoConvenio->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $tipoConvenio->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $tipoConvenio->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $tipoConvenio->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

