

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Convenios</h2>
        <ol class="breadcrumb">
            <li>Tipo Convenios</li>
            <li class="active">
                <strong>Litagem de Tipo Convenios</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="tipoConvenios">
    <h3><?= h($tipoConvenio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($tipoConvenio->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $tipoConvenio->has('user') ? $this->Html->link($tipoConvenio->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoConvenio->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $tipoConvenio->has('situacao_cadastro') ? $this->Html->link($tipoConvenio->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoConvenio->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tipoConvenio->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($tipoConvenio->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($tipoConvenio->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

