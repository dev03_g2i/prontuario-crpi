
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Controladores</h2>
        <ol class="breadcrumb">
            <li>Controladores</li>
            <li class="active">
                <strong>Litagem de Controladores</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Controladores</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Controlador') ?></th>
                                <td><?= h($controladore->controlador) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($controladore->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $controladore->has('situacao_cadastro') ? $this->Html->link($controladore->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $controladore->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $controladore->has('user') ? $this->Html->link($controladore->user->nome, ['controller' => 'Users', 'action' => 'view', $controladore->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($controladore->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($controladore->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($controladore->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


