<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Controladores</h2>
        <ol class="breadcrumb">
            <li>Controladores</li>
            <li class="active">
                <strong>                    Editar Controladores
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="controladores form">
                        <?= $this->Form->create($controladore) ?>
                        <fieldset>
                            <legend><?= __('Editar Controladore') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('controlador');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('nome');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

