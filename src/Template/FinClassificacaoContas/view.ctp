
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Classificacao Contas</h2>
        <ol class="breadcrumb">
            <li>Fin Classificacao Contas</li>
            <li class="active">
                <strong>Litagem de Fin Classificacao Contas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Classificacao Contas</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($finClassificacaoConta->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finClassificacaoConta->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao') ?></th>
                                <td><?= $this->Number->format($finClassificacaoConta->situacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tipo') ?></th>
                                <td><?= $this->Number->format($finClassificacaoConta->tipo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Operacao Transferencia') ?></th>
                                <td><?= $this->Number->format($finClassificacaoConta->operacao_transferencia) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Criado Por') ?></th>
                                <td><?= $this->Number->format($finClassificacaoConta->criado_por) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Alterado Por') ?></th>
                                <td><?= $this->Number->format($finClassificacaoConta->alterado_por) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Criacao') ?></th>
                                                                <td><?= h($finClassificacaoConta->data_criacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Alteracao') ?></th>
                                                                <td><?= h($finClassificacaoConta->data_alteracao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Considera') ?>6</th>
                                <td><?= $finClassificacaoConta->considera ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Plano Contas') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finClassificacaoConta->fin_plano_contas)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Classificacao Conta Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Nome') ?></th>
                        <th><?= __('Rateio') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finClassificacaoConta->fin_plano_contas as $finPlanoContas): ?>
                <tr>
                    <td><?= h($finPlanoContas->id) ?></td>
                    <td><?= h($finPlanoContas->fin_classificacao_conta_id) ?></td>
                    <td><?= h($finPlanoContas->situacao) ?></td>
                    <td><?= h($finPlanoContas->nome) ?></td>
                    <td><?= h($finPlanoContas->rateio) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinPlanoContas','action' => 'view', $finPlanoContas->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinPlanoContas','action' => 'edit', $finPlanoContas->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinPlanoContas','action' => 'delete', $finPlanoContas->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finClassificacaoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


