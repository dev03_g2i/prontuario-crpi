<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fin Classificacao Contas</h2>
            <ol class="breadcrumb">
                <li>Fin Classificacao Contas</li>
                <li class="active">
                    <strong>
                                                Editar Fin Classificacao Contas
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finClassificacaoContas form">
                            <?= $this->Form->create($finClassificacaoConta) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Fin Classificacao Conta') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('situacao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('tipo'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('considera'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('operacao_transferencia'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('criado_por'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('alterado_por'); ?>
                                                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('data_criacao', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($finClassificacaoConta->data_criacao,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                                     ?>
                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('data_alteracao', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($finClassificacaoConta->data_alteracao,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                                     ?>
                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

