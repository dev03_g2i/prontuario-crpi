<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Convenio Impostos</h2>
            <ol class="breadcrumb">
                <li>Convenio</li>
                <li class="active">
                    <strong>Impostos</strong>
                </li>
            </ol>
            <strong>Convênio: <?= $convenio->nome ?></strong>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Convenio Impostos Parametro') ?></h5>
                        <div class="text-right btnAdd">
                            <p>
                                <?= $this->Html->link($this->Html->icon('plus').' Cadastrar imposto para o convênio', ['action' => 'add', $convenio->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Convenio Impostos Parametro','class'=>'btn btn-primary','escape' => false]) ?>
                            </p>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('id_convenio', ['label' => 'Convênio']) ?></th>
                                        <th><?= $this->Paginator->sort('id_tipo_imposto', ['label' => 'Imposto']) ?></th>
                                        <th><?= $this->Paginator->sort('porcentual') ?></th>
                                        <th><?= $this->Paginator->sort('retido') ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($convenioImpostosParametro as $convenioImpostosParametro): ?>
                                            <tr>
                                                <td><?= $this->Number->format($convenioImpostosParametro->id) ?></td>
                                                <td><?= $convenioImpostosParametro->convenio->nome ?></td>
                                                <td><?= $convenioImpostosParametro->convenio_tipoimposto->descricao ?></td>
                                                <td><?= $this->Number->format($convenioImpostosParametro->porcentual).'%' ?></td>
                                                <td><?= $convenioImpostosParametro->retido == 1 ? 'Sim' : 'Não' ?></td>
                                                <td><?= $convenioImpostosParametro->created = date("d/m/y") ?></td>
                                                <td class="actions">
                                                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'convenioImpostosParametro','action' => 'view', $convenioImpostosParametro->id, $convenio->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'convenioImpostosParametro','action' => 'edit', $convenioImpostosParametro->id, $convenio->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                    <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'convenioImpostosParametro','action'=>'delete', $convenioImpostosParametro->id, $convenio->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
