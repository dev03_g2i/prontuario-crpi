
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Convenio Impostos Parametro</h2>
        <ol class="breadcrumb">
            <li>Convenio Impostos Parametro</li>
            <li class="active">
                <strong>Litagem de Convenio Impostos Parametro</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Convenio Impostos Parametro</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id Convenio') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->id_convenio) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id Tipo Imposto') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->id_tipo_imposto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Porcentual') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->porcentual) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Retido') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->retido) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Created') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->user_created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Updated') ?></th>
                                <td><?= $this->Number->format($convenioImpostosParametro->user_updated) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($convenioImpostosParametro->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($convenioImpostosParametro->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($convenioImpostosParametro->observacao)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


