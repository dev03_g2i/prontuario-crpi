<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Convenio Impostos Parametro</h2>
            <ol class="breadcrumb">
                <li>Convenio Impostos Parametro</li>
                <li class="active">
                    <strong>Editar Imposto para o convênio</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="convenioImpostosParametro form">
                            <?= $this->Form->create($convenioImpostosParametro) ?>
                                <fieldset>
                                    <legend><?= __("Editar imposto para $convenio->nome") ?></legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('id_convenio', ['readonly', 'type' => 'text', 'label' => 'Convênio', 'value' => $convenio->nome]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?= $this->Form->input('id_tipo_imposto', ['label' => 'Impostos', 'type' => 'select', 'empty' => 'Selecione', 'options' => $impostos, 'default' => 1]); ?>
                                    </div>
                                    <div class="col-sm-12 no-padding">
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('porcentual', ['type' => 'number', 'max' => 100, 'min' => 0, 'append' => '%']); ?>
                                        </div>
                                        <div class='col-sm-6'>
                                            <div class="col-sm-12 no-padding">
                                                <label>Retido</label>    
                                            </div>
                                            <div class="col-sm-6 no-padding">
                                                <div class="col-sm-3 no-padding m-t-5">
                                                    <input id="retido" type="radio" value="1" name="retido">
                                                    <strong>Sim</strong>
                                                </div>
                                                <div class="col-sm-3 no-padding m-t-5">
                                                    <input id="retido" type="radio" value="2" name="retido" checked>
                                                    <strong>Não</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-12 m-t-10'>
                                        <?=$this->Form->input('observacao'); ?>
                                    </div>
                                </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

