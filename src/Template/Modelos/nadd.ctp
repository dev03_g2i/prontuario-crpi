<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Modelos - Cadastrar</h2>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="modelos form">
                        <?= $this->Form->create($modelo,['id'=>'form-modelo']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Modelo') ?></legend>
                            <?php
                            if(!empty($tipo_id)){
                                echo $this->Form->input('tipo_id', ['value' => $tipo_id,'type'=>'hidden']);
                            }else {
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('tipo_id', ['options' => $tipoDocumentos]);
                                echo "</div>";
                            }
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('proprietario', ['label' => 'Proprietario', 'options' => $medico_responsaveis, 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('nome',['label'=>'Nome do modelo']);
                            echo "</div>";
                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('modelo',['data'=>'sumer']);
                            echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary','onclick' => 'formcombo("form-modelo","modelo-id",set_modelo )']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

