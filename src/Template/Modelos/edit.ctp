<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Modelos</h2>
            <ol class="breadcrumb">
                <li>Modelos</li>
                <li class="active">
                    <strong> Editar Modelos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="modelos form">
                            <?= $this->Form->create($modelo) ?>
                            <fieldset>
                                <legend><?= __('Editar Modelo') ?></legend>
                                <?php
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('tipo_id', ['options' => $tipoDocumentos,'disabled'=>'disabled']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome', ['label' => 'Nome Modelo']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('proprietario', ['label' => 'Proprietario', 'options' => $medico_responsaveis, 'empty' => 'Selecione']);
                                    echo "</div>";
                                    echo "<div class='col-md-12'>";
                                    echo $this->Form->input('modelo', ['data' => 'sumer']);
                                    echo "</div>";
                                ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <input type="button" value="Voltar" class="btn btn-primary" onclick="Navegar('','back')" />
                                <input type="submit" value="Salvar" class="btn btn-primary" />
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

