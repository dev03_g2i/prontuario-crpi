<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Modelos - Visualizar</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="modelos">
                            <div class="table-responsive">
                                <h3>Detalhes</h3>
                                <table class="table table-hover">
                                    <tr>
                                        <th><?= __('Tipo Documento') ?></th>
                                        <th><?= __('Nome') ?></th>
                                        <th><?= __('Situacao Cadastro') ?></th>
                                        <th><?= __('Quem cadastrou') ?></th>
                                        <th><?= __('Dt Cadastro') ?></th>
                                        <th><?= __('Últ. Atualização') ?></th>
                                    </tr>
                                    <tr>
                                        <td><?= $modelo->has('tipo_documento') ? $this->Html->link($modelo->tipo_documento->nome, ['controller' => 'TipoDocumentos', 'action' => 'view', $modelo->tipo_documento->id]) : '' ?></td>
                                        <td><?= h($modelo->nome) ?></td>
                                        <td><?= $modelo->has('situacao_cadastro') ? $this->Html->link($modelo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $modelo->situacao_cadastro->id]) : '' ?></td>
                                        <td><?= $modelo->has('user') ? $this->Html->link($modelo->user->nome, ['controller' => 'Users', 'action' => 'view', $modelo->user->id]) : '' ?></td>
                                        <td><?= h($modelo->created) ?></td>
                                        <td><?= h($modelo->modified) ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6"><?= html_entity_decode($modelo->modelo) ?></td>
                                    </tr>
                                </table>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Voltar'), ['class' => 'btn btn-primary','onclick'=>'Navegar(\'\',\'back\')', 'type'=>'button']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

