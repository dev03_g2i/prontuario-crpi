<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Modelos</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Modelos', ['action' => 'add', $tipo_id], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Modelos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('tipo_id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('proprietario', 'Proprietário') ?></th>
                                        <th><?= $this->Paginator->sort('user_id', ['label' => 'Quem cadastrou']) ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Data Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Data Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($modelos as $modelo): ?>
                                        <tr>
                                            <td><?= $modelo->has('tipo_documento') ? $this->Html->link($modelo->tipo_documento->nome, ['controller' => 'TipoDocumentos', 'action' => 'view', $modelo->tipo_documento->id]) : '' ?></td>
                                            <td><?= h($modelo->nome) ?></td>
                                            <td>
                                                <?php if(!empty($modelo->proprietario)):
                                                    echo $this->Medico->getMedicoName($modelo->proprietario);
                                                else:
                                                    echo 'Público';
                                                endif;?>
                                            </td>
                                            <td><?= $modelo->has('user') ? $this->Html->link($modelo->user->nome, ['controller' => 'Users', 'action' => 'view', $modelo->user->id]) : '' ?></td>
                                            <td><?= h($modelo->created) ?></td>
                                            <td><?= h($modelo->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $modelo->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $modelo->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'Modelos\',' . $modelo->id . ',recarregar,' . $tipo_id . ')', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

