<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Documentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Documentos</li>
            <li class="active">
                <strong>Detalhes do Documentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Data') ?></th>
                                <th><?= __('Paciente') ?></th>
                                <th><?= __('Tipo Documento') ?></th>
                                <th><?= __('Quem cadastrou') ?></th>
                                <th><?= __('Data Modificação') ?></th>
                            </tr>
                            <tr>
                                <td><?= h($clienteDocumento->created) ?></td>
                                <td><?= $clienteDocumento->has('cliente') ? $this->Html->link($clienteDocumento->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteDocumento->cliente->id]) : '' ?></td>
                                <td><?= $clienteDocumento->has('tipo_documento') ? $this->Html->link($clienteDocumento->tipo_documento->nome, ['controller' => 'TipoDocumentos', 'action' => 'view', $clienteDocumento->tipo_documento->id]) : '' ?></td>
                                <td><?= $clienteDocumento->has('user') ? $this->Html->link($clienteDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteDocumento->user->id]) : '' ?></td>
                                <td><?= h($clienteDocumento->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Descrição') ?></th>
                                <td colspan="5"><?= html_entity_decode($clienteDocumento->descricao) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

