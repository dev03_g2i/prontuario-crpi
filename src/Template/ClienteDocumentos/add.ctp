<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Documentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Documentos</li>
            <li class="active">
                <strong> Cadastrar Documentos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <div class="clienteDocumentos form">
                        <?= $this->Form->create($clienteDocumento, ['id' => 'form-resp']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Documento') ?></legend>
                            <?php
                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('created', ['label'=>'Dt. emissão','type' => 'text', 'class' => 'datetimepicker', 'value' => date('Y-m-d H:i:s')]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('medico_id', ['options' => $medicos, 'label' => 'Profissional', 'empty' => 'Selecione','required'=>'required','default'=> @$user_logado['codigo_medico']]);
                            echo "</div>";
                            echo "<div class='col-md-2'>";
                            echo $this->Form->input('tipo_id', ['options' => $tipoDocumentos, 'label' => 'Tipo', 'empty' => 'Selecione']);
                            echo "</div>";

                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('modelo_id', ['label' => 'Modelo', 'empty' => 'Selecione',
                                'append' => [
                                    $this->Form->button('<span class="glyphicon glyphicon-plus-sign"></span>', [
                                        'id' => 'add-modelo', 'escape' => false, 'toggle' => 'tooltip','type'=>'button', 'data-placement' => 'bottom', 'title' => 'Novo Modelo'
                                    ])
                                ]]);
                            echo "</div>";?>

                            <div class="col-md-12 text-right jaja">
                                <input type="submit" value="Imprimir" class="btn btn-primary" onclick="PrinterForm('form-resp')" />
                                <input type="submit" value="Salvar" class="btn btn-primary" onclick="EnviarForm('form-resp')" />
                            </div>

                            <?php echo "<div class='col-md-12'>";
                            echo $this->Form->input('descricao', ['label' => 'Descrição','data'=>'ck']);
                            echo "</div>";

                            echo $this->Form->input('cliente_id', ['type' => 'hidden', 'value' => $cliente]);
                            ?>
                            <div class="clearfix"></div>

                        </fieldset>

                        <div id="concat-modelos" class="hidden"></div>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

