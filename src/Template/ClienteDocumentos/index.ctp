
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Documentos</h2>
        <ol class="breadcrumb">
            <li>Cliente Documentos</li>
            <li class="active">
                <strong>Listagem de Cliente Documentos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Cliente Documentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Cliente Documentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Cliente Documentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id',['label'=>'Código']) ?></th>
                <th><?= $this->Paginator->sort('cliente_id') ?></th>
                <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($clienteDocumentos as $clienteDocumento): ?>
            <tr>
                <td><?= $this->Number->format($clienteDocumento->id) ?></td>
                <td><?= $clienteDocumento->has('cliente') ? $this->Html->link($clienteDocumento->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteDocumento->cliente->id]) : '' ?></td>
                <td><?= $clienteDocumento->has('user') ? $this->Html->link($clienteDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteDocumento->user->id]) : '' ?></td>
                <td><?= h($clienteDocumento->descricao) ?></td>
                <td><?= $clienteDocumento->has('situacao_cadastro') ? $this->Html->link($clienteDocumento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $clienteDocumento->situacao_cadastro->id]) : '' ?></td>
                <td><?= $clienteDocumento->has('tipo_documento') ? $this->Html->link($clienteDocumento->tipo_documento->nome, ['controller' => 'TipoDocumentos', 'action' => 'view', $clienteDocumento->tipo_documento->id]) : '' ?></td>
                <td><?= h($clienteDocumento->created) ?></td>
                <td><?= h($clienteDocumento->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteDocumento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $clienteDocumento->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $clienteDocumento->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

