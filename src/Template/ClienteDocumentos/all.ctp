
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Documentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Documentos</li>
            <li class="active">
                <strong>Listagem de Paciente/Documentos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right" style="padding-top: 20px">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Novo', ['action' => 'add',$cliente_id],['id' => 'add-documento','data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Documentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
                                    <th><?= $this->Paginator->sort('tipo_id') ?></th>
                                    <th><?= $this->Paginator->sort('medico_id', ['label' => 'Profissional']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Data de modificação']) ?></th>
                                    <th class="actions"><?= __('Opções') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($clienteDocumentos as $clienteDocumento): ?>
                                    <tr class="delete<?=$clienteDocumento->id?>">
                                        <td><?= h($clienteDocumento->created) ?></td>
                                        <th><?= $clienteDocumento->has('tipo_documento') ? $clienteDocumento->tipo_documento->nome: '' ?></th>
                                        <th><?= $clienteDocumento->has('medico_responsavei') ? $clienteDocumento->medico_responsavei->nome: '' ?></th>
                                        <td><?= h($clienteDocumento->modified) ?></td>
                                        <td class="actions">
                                            <?php 
                                                if($clienteDocumento->tipo_documento->id == 15) {
                                                    echo $this->Html->link($this->Html->icon('print'), ['action' => 'report', '?' => ['id' => $clienteDocumento->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f', 'target' => '_blank']);
                                                }else{
                                                    echo $this->Html->link($this->Html->icon('print'), ['action' => $config_doc->nome_modelo.'.pdf','?'=>['id'=>$clienteDocumento->id ]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir', 'escape' => false, 'class' => 'btn btn-xs btn-danger','listen'=>'f','target'=>'_blank']);
                                                }
                                                echo $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteDocumento->id,$cliente_id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']);
                                                //echo $this->Html->link($this->Html->icon('remove'),['action' => 'delete', $clienteDocumento->id], ['onclick'=>'DeletarModal(\'clienteDocumentos\','.$clienteDocumento->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']);
                                                echo $this->Form->button($this->Html->icon('remove'),['onclick'=>'DeletarModal(\'clienteDocumentos\','.$clienteDocumento->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']);
                                             ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

