
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Documentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Documentos</li>
            <li class="active">
                <strong>                        Editar Documentos
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                <div class="clienteDocumentos form">
                <?= $this->Form->create($clienteDocumento,['id'=>'form-resp']) ?>
                <fieldset>
                    <legend><?= __('Editar Documento') ?></legend>
                    <?php
                    echo "<div class='col-md-4'>";
                    echo $this->Form->input('medico_id', ['options' => $medicos, 'label' => 'Profissional', 'empty' => 'Selecione','required'=>'required']);
                    echo "</div>";
                    echo "<div class='col-md-4'>";
                    echo $this->Form->input('tipo_id', ['options' => $tipoDocumentos, 'label' => 'Tipo de Documento', 'empty' => 'Selecione']);
                    echo "</div>";
                    echo "<div class='col-md-4'>";
                    echo $this->Form->input('modelo_id', ['label' => 'Modelo de documento', 'empty' => 'Selecione','options'=>$modelos,
                        'append' => [
                            $this->Form->button('<span class="glyphicon glyphicon-plus-sign"></span>', [
                                'id' => 'add-modelo', 'escape' => false, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo Modelo'
                            ])
                        ]]);
                    echo "</div>";

                    echo "<div class='col-md-4'>";
                    echo $this->Form->input('created', ['label'=>'Dt. emissão','type' => 'text', 'class' => 'datetimepicker', 'value' => $this->Time->format($clienteDocumento->created, 'dd/MM/Y HH:mm')]);
                    echo "</div>";
                    $descricao = ($clienteDocumento->descricao != strip_tags($clienteDocumento->descricao)) ? $clienteDocumento->descricao : nl2br($clienteDocumento->descricao);
                    echo "<div class='col-md-12'>";
                    echo $this->Form->input('descricao', ['label' => 'Descrição','data'=>'ck', 'value' => $descricao]);
                    echo "</div>";
                    echo $this->Form->input('cliente_id', ['type' => 'hidden']);
                    ?>
                </fieldset>
    <div class="col-md-12 text-right">
        <div class="col-md-12 text-right">
            <input type="submit" value="Imprimir" class="btn btn-primary" onclick="PrinterForm('form-resp')" />
            <input type="submit" value="Salvar" class="btn btn-primary" onclick="EnviarForm('form-resp')" />
        </div>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

