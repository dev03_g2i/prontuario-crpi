<!-- src/Template/Yop/pdf/view.ctp -->
<?php
echo $this->Dompdf->css('pdf/estilo');
?>
<?php $this->start('header'); ?>
<div class="pront-doc-02">
<div class="topo">
    <div class="logo">
        <?= $this->Dompdf->image('docs/logo.jpg', ['class' => 'img-responsive']) ?>
    </div>
    <div class="col-md-12 meio">
        <div class="paciente">
            <p>Paciente: &nbsp;&nbsp;<?= $documentos->has('cliente') ? $documentos->cliente->nome : '' ?></p>
            <div class="col-md-6">Idade:<?= $documentos->has('cliente') ? $this->Data->idade($documentos->cliente->nascimento) : '' ?>
            </div>
            <div class="col-md-6">  
                Sexo: <?= $documentos->has('cliente') ?
                    (($documentos->cliente->sexo == 'M') ? 'Masculino' :
                        (($documentos->cliente->sexo == 'F') ? 'Feminino' : NULL))
                    : '' ?>
            </div>

        </div>

        <div class="clearfix"></div>
        <div class="titulo">
            <?php if($this->Configuracao->showDocTitulo()):
                echo $documentos->tipo_documento->nome;
            endif;
            ?>
        </div>
    </div>
</div>
<?php $this->end(); ?>
<div class="corpo">
    <?= $documentos->descricao ?>
</div>
<br/>
<div class="assinatura">
    <h3 style="text-align: left">Campo Grande(MS), <?= $this->Time->format($documentos->created, 'dd/MM/Y') ?>.</h3>
    <br />
    <div class="clearfix"></div>
    <h2><?= $documentos->has('medico_responsavei') ? $documentos->medico_responsavei->nome_laudo : '' ?><br/>
        <?= $documentos->has('medico_responsavei') ? $documentos->medico_responsavei->conselho_laudo : '' ?>
    </h2>
</div>

<div class="footer fixed text-center">

</div>
</div>

