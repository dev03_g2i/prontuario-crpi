<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tipo Equipegrau</h2>
            <ol class="breadcrumb">
                <li>Tipo Equipegrau</li>
                <li class="active">
                    <strong>
                                                Cadastrar Tipo Equipegrau
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tipoEquipegrau form">
                            <?= $this->Form->create($tipoEquipegrau) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Tipo Equipegrau') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('codigo_tiss'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_insert'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_update'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('situacao_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('porcentual'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

