
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Equipegrau</h2>
        <ol class="breadcrumb">
            <li>Tipo Equipegrau</li>
            <li class="active">
                <strong>Litagem de Tipo Equipegrau</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Tipo Equipegrau</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($tipoEquipegrau->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Codigo Tiss') ?></th>
                                <td><?= h($tipoEquipegrau->codigo_tiss) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($tipoEquipegrau->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Insert') ?></th>
                                <td><?= $this->Number->format($tipoEquipegrau->user_insert) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Update') ?></th>
                                <td><?= $this->Number->format($tipoEquipegrau->user_update) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Id') ?></th>
                                <td><?= $this->Number->format($tipoEquipegrau->situacao_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Porcentual') ?></th>
                                <td><?= $this->Number->format($tipoEquipegrau->porcentual) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($tipoEquipegrau->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($tipoEquipegrau->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Atendimento Procedimento Equipes') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($tipoEquipegrau->atendimento_procedimento_equipes)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Ordem') ?></th>
                        <th><?= __('Atendimento Procedimento Id') ?></th>
                        <th><?= __('Profissional Id') ?></th>
                        <th><?= __('Tipo Equipe Id') ?></th>
                        <th><?= __('Tipo Equipegrau Id') ?></th>
                        <th><?= __('Porcentagem') ?></th>
                        <th><?= __('Configuracao Apuracao Id') ?></th>
                        <th><?= __('Obs') ?></th>
                        <th><?= __('User Insert') ?></th>
                        <th><?= __('User Update') ?></th>
                        <th><?= __('Situacao Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($tipoEquipegrau->atendimento_procedimento_equipes as $atendimentoProcedimentoEquipes): ?>
                <tr>
                    <td><?= h($atendimentoProcedimentoEquipes->id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->ordem) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->atendimento_procedimento_id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->profissional_id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->tipo_equipe_id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->tipo_equipegrau_id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->porcentagem) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->configuracao_apuracao_id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->obs) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->user_insert) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->user_update) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->situacao_id) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->created) ?></td>
                    <td><?= h($atendimentoProcedimentoEquipes->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'AtendimentoProcedimentoEquipes','action' => 'view', $atendimentoProcedimentoEquipes->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'AtendimentoProcedimentoEquipes','action' => 'edit', $atendimentoProcedimentoEquipes->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'AtendimentoProcedimentoEquipes','action' => 'delete', $atendimentoProcedimentoEquipes->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $tipoEquipegrau->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


