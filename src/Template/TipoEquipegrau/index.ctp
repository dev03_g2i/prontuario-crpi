<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tipo Equipegrau</h2>
            <ol class="breadcrumb">
                <li>Tipo Equipegrau</li>
                <li class="active">
                    <strong>Listagem de Tipo Equipegrau</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('TipoEquipegrau',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('descricao',['name'=>'TipoEquipegrau__descricao']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('codigo_tiss',['name'=>'TipoEquipegrau__codigo_tiss']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('user_insert',['name'=>'TipoEquipegrau__user_insert']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('user_update',['name'=>'TipoEquipegrau__user_update']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('situacao_id',['name'=>'TipoEquipegrau__situacao_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('porcentual',['name'=>'TipoEquipegrau__porcentual']); ?>
                                                                                    </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Tipo Equipegrau', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Tipo Equipegrau','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Tipo Equipegrau') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('descricao') ?></th>
                                                                                <th><?= $this->Paginator->sort('codigo_tiss') ?></th>
                                                                                <th><?= $this->Paginator->sort('user_insert') ?></th>
                                                                                <th><?= $this->Paginator->sort('user_update') ?></th>
                                                                                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                                                                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                                                                <th><?= $this->Paginator->sort('porcentual') ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($tipoEquipegrau as $tipoEquipegrau): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($tipoEquipegrau->id) ?></td>
                                                                                <td><?= h($tipoEquipegrau->descricao) ?></td>
                                                                                <td><?= h($tipoEquipegrau->codigo_tiss) ?></td>
                                                                                <td><?= $this->Number->format($tipoEquipegrau->user_insert) ?></td>
                                                                                <td><?= $this->Number->format($tipoEquipegrau->user_update) ?></td>
                                                                                <td><?= $this->Number->format($tipoEquipegrau->situacao_id) ?></td>
                                                                                <td><?= h($tipoEquipegrau->created) ?></td>
                                                                                <td><?= h($tipoEquipegrau->modified) ?></td>
                                                                                <td><?= $this->Number->format($tipoEquipegrau->porcentual) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'tipoEquipegrau','action' => 'view', $tipoEquipegrau->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'tipoEquipegrau','action' => 'edit', $tipoEquipegrau->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'tipoEquipegrau','action'=>'delete', $tipoEquipegrau->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
