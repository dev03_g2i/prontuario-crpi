
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Anexos</h2>
        <ol class="breadcrumb">
            <li>Tipo Anexos</li>
            <li class="active">
                <strong>Listagem de Tipo Anexos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Tipo Anexos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Tipo Anexos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Tipo Anexos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tipoAnexos as $tipoAnexo): ?>
            <tr>
                <td><?= $this->Number->format($tipoAnexo->id) ?></td>
                <td><?= h($tipoAnexo->nome) ?></td>
                <td><?= $tipoAnexo->has('user') ? $this->Html->link($tipoAnexo->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoAnexo->user->id]) : '' ?></td>
                <td><?= $tipoAnexo->has('situacao_cadastro') ? $this->Html->link($tipoAnexo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoAnexo->situacao_cadastro->id]) : '' ?></td>
                <td><?= h($tipoAnexo->created) ?></td>
                <td><?= h($tipoAnexo->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $tipoAnexo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $tipoAnexo->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $tipoAnexo->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

