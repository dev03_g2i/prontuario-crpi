

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Anexos</h2>
        <ol class="breadcrumb">
            <li>Tipo Anexos</li>
            <li class="active">
                <strong>Litagem de Tipo Anexos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="tipoAnexos">
    <h3><?= h($tipoAnexo->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($tipoAnexo->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $tipoAnexo->has('user') ? $this->Html->link($tipoAnexo->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoAnexo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $tipoAnexo->has('situacao_cadastro') ? $this->Html->link($tipoAnexo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoAnexo->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tipoAnexo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($tipoAnexo->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($tipoAnexo->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

