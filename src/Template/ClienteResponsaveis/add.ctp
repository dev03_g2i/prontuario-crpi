<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Paciente/Responsaveis</h2>
            <ol class="breadcrumb">
                <li>Paciente/Responsaveis</li>
                <li class="active">
                    <strong>Cadastrar Responsavel</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clienteResponsaveis form">
                            <?= $this->Form->create($clienteResponsavei) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Responsavel') ?></legend>
                                <?php
                                if(!empty($cliente)) {
                                    echo $this->Form->input('cliente_id', ['type' => 'hidden', 'value' => $cliente]);
                                }else{
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('cliente_id', ['options' => $clientes,'label'=>'Paciente']);
                                    echo "</div>";
                                }
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('grau_id', ['options' => $grauParentescos]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('nome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('nascionalidade', ['label' => 'Nacionalidade']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('estadocivil_id', ['options' => $estadoCivis, 'empty' => 'Selecione','label'=>'Estado civil']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('profissao', ['label' => 'Profissão']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('rg');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('cpf', ['mask' => 'cpf']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('cep', ['mask' => 'cep', 'role' => 'cep']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('endereco', ['label' => 'Endereço', 'data-cep' => 'endereco']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numero', ['babel' => 'Número']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('complemento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('bairro', ['data-cep' => 'bairro']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('cidade', ['data-cep' => 'cidade']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('estado', ['data-cep' => 'uf']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('telefone', ['mask' => 'fone']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('celular', ['mask' => 'fone']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('email');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('financeiro',['label'=>' Responsável Financeiro?']);
                                echo "</div>";

                                ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

