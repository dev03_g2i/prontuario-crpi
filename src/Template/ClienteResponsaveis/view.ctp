<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Responsavel</h2>
        <ol class="breadcrumb">
            <li>Paciente/Responsavel</li>
            <li class="active">
                <strong>Litagem de Responsaveis</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="clienteResponsaveis">
    <h3><?= h($clienteResponsavei->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Paciente') ?></th>
            <td><?= $clienteResponsavei->has('cliente') ? $this->Html->link($clienteResponsavei->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteResponsavei->cliente->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Grau Parentesco') ?></th>
            <td><?= $clienteResponsavei->has('grau_parentesco') ? $this->Html->link($clienteResponsavei->grau_parentesco->nome, ['controller' => 'GrauParentescos', 'action' => 'view', $clienteResponsavei->grau_parentesco->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($clienteResponsavei->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Nascionalidade') ?></th>
            <td><?= h($clienteResponsavei->nascionalidade) ?></td>
        </tr>
        <tr>
            <th><?= __('Estado Civi') ?></th>
            <td><?= $clienteResponsavei->has('estado_civi') ? $this->Html->link($clienteResponsavei->estado_civi->nome, ['controller' => 'EstadoCivis', 'action' => 'view', $clienteResponsavei->estado_civi->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Profissao') ?></th>
            <td><?= h($clienteResponsavei->profissao) ?></td>
        </tr>
        <tr>
            <th><?= __('Rg') ?></th>
            <td><?= h($clienteResponsavei->rg) ?></td>
        </tr>
        <tr>
            <th><?= __('Cpf') ?></th>
            <td><?= h($clienteResponsavei->cpf) ?></td>
        </tr>
        <tr>
            <th><?= __('Cep') ?></th>
            <td><?= h($clienteResponsavei->cep) ?></td>
        </tr>
        <tr>
            <th><?= __('Endereco') ?></th>
            <td><?= h($clienteResponsavei->endereco) ?></td>
        </tr>
        <tr>
            <th><?= __('Complemento') ?></th>
            <td><?= h($clienteResponsavei->complemento) ?></td>
        </tr>
        <tr>
            <th><?= __('Cidade') ?></th>
            <td><?= h($clienteResponsavei->cidade) ?></td>
        </tr>
        <tr>
            <th><?= __('Estado') ?></th>
            <td><?= h($clienteResponsavei->estado) ?></td>
        </tr>
        <tr>
            <th><?= __('Telefone') ?></th>
            <td><?= h($clienteResponsavei->telefone) ?></td>
        </tr>
        <tr>
            <th><?= __('Celular') ?></th>
            <td><?= h($clienteResponsavei->celular) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($clienteResponsavei->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $clienteResponsavei->has('situacao_cadastro') ? $this->Html->link($clienteResponsavei->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $clienteResponsavei->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $clienteResponsavei->has('user') ? $this->Html->link($clienteResponsavei->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteResponsavei->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($clienteResponsavei->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Numero') ?></th>
            <td><?= $this->Number->format($clienteResponsavei->numero) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($clienteResponsavei->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($clienteResponsavei->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

