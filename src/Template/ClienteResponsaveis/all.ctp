<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Paciente/Responsaveis</h2>
            <ol class="breadcrumb">
                <li>Paciente/Responsaveis</li>
                <li class="active">
                    <strong>Litagem de Paciente/Responsaveis</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Novo', ['action' => 'nadd', $cliente_id], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo Responsavel', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id', ['label' => 'Código']) ?></th>
                                        <th><?= $this->Paginator->sort('grau_id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('telefone') ?></th>
                                        <th><?= $this->Paginator->sort('celular') ?></th>
                                        <th><?= $this->Paginator->sort('email') ?></th>
                                        <th><?= $this->Paginator->sort('financeiro',['label'=>'Resp. Finaneiro?']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($clienteResponsaveis as $clienteResponsavei): ?>
                                        <tr>
                                            <td><?= $this->Number->format($clienteResponsavei->id) ?></td>
                                            <td><?= $clienteResponsavei->has('grau_parentesco') ? $this->Html->link($clienteResponsavei->grau_parentesco->nome, ['controller' => 'GrauParentescos', 'action' => 'view', $clienteResponsavei->grau_parentesco->id]) : '' ?></td>
                                            <td><?= h($clienteResponsavei->nome) ?></td>
                                            <td><?= h($clienteResponsavei->telefone) ?></td>
                                            <td><?= h($clienteResponsavei->celular) ?></td>
                                            <td><?= h($clienteResponsavei->email) ?></td>
                                            <td><?= h($clienteResponsavei->financeiro==1?'Sim':'Não') ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteResponsavei->id, $cliente_id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'ClienteResponsaveis\',' . $clienteResponsavei->id . ',\'carregarAba\' ,' . $cliente_id . ')', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

