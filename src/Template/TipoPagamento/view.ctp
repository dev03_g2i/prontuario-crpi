

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Pagamento</h2>
        <ol class="breadcrumb">
            <li>Tipo Pagamento</li>
            <li class="active">
                <strong>Litagem de Tipo Pagamento</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="tipoPagamento">
    <h3><?= h($tipoPagamento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($tipoPagamento->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tipoPagamento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Status Id') ?></th>
            <td><?= $this->Number->format($tipoPagamento->status_id) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Usado') ?></h4>
        <?= $this->Text->autoParagraph(h($tipoPagamento->usado)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Contasreceber') ?></h4>
        <?php if (!empty($tipoPagamento->contasreceber)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Data') ?></th>
                <th><?= __('Valor') ?></th>
                <th><?= __('Plano Id') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Status Id') ?></th>
                <th><?= __('Complemento') ?></th>
                <th><?= __('Data Pagamento') ?></th>
                <th><?= __('Contabilidade Id') ?></th>
                <th><?= __('Vencimento') ?></th>
                <th><?= __('Documento') ?></th>
                <th><?= __('Numdoc') ?></th>
                <th><?= __('Parcela') ?></th>
                <th><?= __('Formapagamento') ?></th>
                <th><?= __('Boletomsg') ?></th>
                <th><?= __('Boletomsglivre') ?></th>
                <th><?= __('Boletobarras') ?></th>
                <th><?= __('Boletolinhadigitavel') ?></th>
                <th><?= __('Boletonossonum') ?></th>
                <th><?= __('Juros') ?></th>
                <th><?= __('Multa') ?></th>
                <th><?= __('Boletocedente') ?></th>
                <th><?= __('Numfiscal') ?></th>
                <th><?= __('Datafiscal') ?></th>
                <th><?= __('ValorBruto') ?></th>
                <th><?= __('Desconto') ?></th>
                <th><?= __('Correcao Monetaria') ?></th>
                <th><?= __('Tipo Pagamento Id') ?></th>
                <th><?= __('Tipo Documento Id') ?></th>
                <th><?= __('Numerodocumento') ?></th>
                <th><?= __('Programacao Id') ?></th>
                <th><?= __('Impresso') ?></th>
                <th><?= __('Valor Pagar') ?></th>
                <th><?= __('Vencimento Boleto') ?></th>
                <th><?= __('Perjuros') ?></th>
                <th><?= __('Permulta') ?></th>
                <th><?= __('Data Cadastro') ?></th>
                <th><?= __('IdSisAntigoBoleto') ?></th>
                <th><?= __('IdSisAntigoCx') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tipoPagamento->contasreceber as $contasreceber): ?>
            <tr>
                <td><?= h($contasreceber->id) ?></td>
                <td><?= h($contasreceber->data) ?></td>
                <td><?= h($contasreceber->valor) ?></td>
                <td><?= h($contasreceber->plano_id) ?></td>
                <td><?= h($contasreceber->cliente_id) ?></td>
                <td><?= h($contasreceber->status_id) ?></td>
                <td><?= h($contasreceber->complemento) ?></td>
                <td><?= h($contasreceber->data_pagamento) ?></td>
                <td><?= h($contasreceber->contabilidade_id) ?></td>
                <td><?= h($contasreceber->vencimento) ?></td>
                <td><?= h($contasreceber->documento) ?></td>
                <td><?= h($contasreceber->numdoc) ?></td>
                <td><?= h($contasreceber->parcela) ?></td>
                <td><?= h($contasreceber->formapagamento) ?></td>
                <td><?= h($contasreceber->boletomsg) ?></td>
                <td><?= h($contasreceber->boletomsglivre) ?></td>
                <td><?= h($contasreceber->boletobarras) ?></td>
                <td><?= h($contasreceber->boletolinhadigitavel) ?></td>
                <td><?= h($contasreceber->boletonossonum) ?></td>
                <td><?= h($contasreceber->juros) ?></td>
                <td><?= h($contasreceber->multa) ?></td>
                <td><?= h($contasreceber->boletocedente) ?></td>
                <td><?= h($contasreceber->numfiscal) ?></td>
                <td><?= h($contasreceber->datafiscal) ?></td>
                <td><?= h($contasreceber->valorBruto) ?></td>
                <td><?= h($contasreceber->desconto) ?></td>
                <td><?= h($contasreceber->correcao_monetaria) ?></td>
                <td><?= h($contasreceber->tipo_pagamento_id) ?></td>
                <td><?= h($contasreceber->tipo_documento_id) ?></td>
                <td><?= h($contasreceber->numerodocumento) ?></td>
                <td><?= h($contasreceber->programacao_id) ?></td>
                <td><?= h($contasreceber->impresso) ?></td>
                <td><?= h($contasreceber->valor_pagar) ?></td>
                <td><?= h($contasreceber->vencimento_boleto) ?></td>
                <td><?= h($contasreceber->perjuros) ?></td>
                <td><?= h($contasreceber->permulta) ?></td>
                <td><?= h($contasreceber->data_cadastro) ?></td>
                <td><?= h($contasreceber->idSisAntigoBoleto) ?></td>
                <td><?= h($contasreceber->idSisAntigoCx) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Contasreceber','action' => 'view', $contasreceber->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Contasreceber','action' => 'edit', $contasreceber->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Contasreceber','action' => 'delete', $contasreceber->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $tipoPagamento->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

