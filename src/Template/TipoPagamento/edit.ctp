
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Pagamento</h2>
        <ol class="breadcrumb">
            <li>Tipo Pagamento</li>
            <li class="active">
                <strong>                        Editar Tipo Pagamento
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="tipoPagamento form">
    <?= $this->Form->create($tipoPagamento) ?>
    <fieldset>
                <legend><?= __('Editar Tipo Pagamento') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                echo $this->Form->input('descricao');
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('status_id');
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('usado');
                echo "</div>";
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

