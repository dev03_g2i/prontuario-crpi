

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Face Itens</h2>
        <ol class="breadcrumb">
            <li>Face Itens</li>
            <li class="active">
                <strong>Litagem de Face Itens</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="faceItens">
    <h3><?= h($faceIten->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Atendimento Iten') ?></th>
            <td><?= $faceIten->has('atendimento_iten') ? $this->Html->link($faceIten->atendimento_iten->id, ['controller' => 'AtendimentoItens', 'action' => 'view', $faceIten->atendimento_iten->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Face') ?></th>
            <td><?= $faceIten->has('face') ? $this->Html->link($faceIten->face->id, ['controller' => 'Faces', 'action' => 'view', $faceIten->face->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $faceIten->has('user') ? $this->Html->link($faceIten->user->nome, ['controller' => 'Users', 'action' => 'view', $faceIten->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $faceIten->has('situacao_cadastro') ? $this->Html->link($faceIten->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faceIten->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($faceIten->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($faceIten->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($faceIten->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

