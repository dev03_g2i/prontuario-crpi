
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Face Itens</h2>
        <ol class="breadcrumb">
            <li>Face Itens</li>
            <li class="active">
                <strong>Litagem de Face Itens</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Face Itens', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Face Itens','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Face Itens') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('iten_id') ?></th>
                <th><?= $this->Paginator->sort('face_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($faceItens as $faceIten): ?>
            <tr>
                <td><?= $this->Number->format($faceIten->id) ?></td>
                <td><?= $faceIten->has('atendimento_iten') ? $this->Html->link($faceIten->atendimento_iten->id, ['controller' => 'AtendimentoItens', 'action' => 'view', $faceIten->atendimento_iten->id]) : '' ?></td>
                <td><?= $faceIten->has('face') ? $this->Html->link($faceIten->face->id, ['controller' => 'Faces', 'action' => 'view', $faceIten->face->id]) : '' ?></td>
                <td><?= $faceIten->has('user') ? $this->Html->link($faceIten->user->nome, ['controller' => 'Users', 'action' => 'view', $faceIten->user->id]) : '' ?></td>
                <td><?= $faceIten->has('situacao_cadastro') ? $this->Html->link($faceIten->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faceIten->situacao_cadastro->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $faceIten->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $faceIten->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $faceIten->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $faceIten->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

