
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Face Itens</h2>
        <ol class="breadcrumb">
            <li>Face Itens</li>
            <li class="active">
                <strong>                        Cadastrar Face Itens
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="faceItens form">
    <?= $this->Form->create($faceIten) ?>
    <fieldset>
                <legend><?= __('Cadastrar Face Iten') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('iten_id', ['options' => $atendimentoItens]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('face_id', ['options' => $faces]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('user_id', ['options' => $users]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('situacao_id', ['options' => $situacaoCadastros]);
                echo "</div>";
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

