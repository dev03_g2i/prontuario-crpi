<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Anexos</h2>
        <ol class="breadcrumb">
            <li>Lista Anexos</li>
            <li class="active">
                <strong>Litagem de Lista Anexos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Novo Anexo', ['action' => 'add', $cliente_anexo->id], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Anexos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Lista Anexos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('Descrição') ?></th>
                                    <th><?= $this->Paginator->sort('Arquivo') ?></th>
                                    <th><?= $this->Paginator->sort('Data de Cadastro') ?></th>
                                    <th><?= $this->Paginator->sort('Quem cadastrou') ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($listaAnexos as $listaAnexo): ?>
                                    <tr class="delete<?=$listaAnexo->id?>">
                                        <td><?= h($listaAnexo->descricao_anexo) ?></td>
                                        <td><?php
                                            $ext = explode('.', $listaAnexo->caminho);
                                            if (in_array($ext[1], ['jpg', 'gif', 'png'])) {
                                                echo $this->Html->image('/files/listaanexos/caminho/' . $listaAnexo->caminho_dir . '/portrait_' . $listaAnexo->caminho);
                                            } else {
                                                echo $this->Html->image('/img/default.png');
                                            }
                                            ?>
                                        </td>
                                        <td><?= h($listaAnexo->created) ?></td>
                                        <td><?= $listaAnexo->has('user') ? $this->Html->link($listaAnexo->user->nome, ['controller' => 'Users', 'action' => 'view', $listaAnexo->user->id]) : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $listaAnexo->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar Anexo', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Html->link($this->Html->icon('remove'), 'javascript:void(0)', ['onclick'=>'DeletarModal(\'ListaAnexos\','.$listaAnexo->id.')', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen'=>'f']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

