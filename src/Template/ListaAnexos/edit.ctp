<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Anexos</h2>
        <ol class="breadcrumb">
            <li>Lista Anexos</li>
            <li class="active">
                <strong> Editar Lista Anexos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="listaAnexos form">
                        <?= $this->Form->create($listaAnexo, ['id' => 'form-resp']) ?>
                        <fieldset>
                            <legend><?= __('Editar Lista Anexo') ?></legend>
                            <?php
                            echo $this->Form->input('anexo_id', ['type' => 'hidden', 'value' => $listaAnexo->anexo_id]);

                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('descricao_anexo', ['label' => 'Descrição']);
                            echo "</div>";
                            ?>
                        </fieldset>

                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary','onclick' => 'senduploads("form-resp","ListaAnexos","index")']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

