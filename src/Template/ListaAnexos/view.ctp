

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Lista Anexos</h2>
        <ol class="breadcrumb">
            <li>Lista Anexos</li>
            <li class="active">
                <strong>Litagem de Lista Anexos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="listaAnexos">
    <h3><?= h($listaAnexo->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Cliente Anexo') ?></th>
            <td><?= $listaAnexo->has('cliente_anexo') ? $this->Html->link($listaAnexo->cliente_anexo->descricao, ['controller' => 'ClienteAnexos', 'action' => 'view', $listaAnexo->cliente_anexo->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Caminho') ?></th>
            <td><?= h($listaAnexo->caminho) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $listaAnexo->has('situacao_cadastro') ? $this->Html->link($listaAnexo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $listaAnexo->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $listaAnexo->has('user') ? $this->Html->link($listaAnexo->user->nome, ['controller' => 'Users', 'action' => 'view', $listaAnexo->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($listaAnexo->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($listaAnexo->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($listaAnexo->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

