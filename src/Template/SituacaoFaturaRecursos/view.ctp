
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Fatura Recursos</h2>
        <ol class="breadcrumb">
            <li>Situacao Fatura Recursos</li>
            <li class="active">
                <strong>Litagem de Situacao Fatura Recursos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Situacao Fatura Recursos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($situacaoFaturaRecurso->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $situacaoFaturaRecurso->has('user') ? $this->Html->link($situacaoFaturaRecurso->user->nome, ['controller' => 'Users', 'action' => 'view', $situacaoFaturaRecurso->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $situacaoFaturaRecurso->has('situacao_cadastro') ? $this->Html->link($situacaoFaturaRecurso->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $situacaoFaturaRecurso->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($situacaoFaturaRecurso->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($situacaoFaturaRecurso->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($situacaoFaturaRecurso->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fatura Recursos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($situacaoFaturaRecurso->fatura_recursos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Valor') ?></th>
                        <th><?= __('Obs') ?></th>
                        <th><?= __('Situacao Fatura Recurso Id') ?></th>
                        <th><?= __('Atendimento Procedimento Id') ?></th>
                        <th><?= __('User Id') ?></th>
                        <th><?= __('Situacao Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($situacaoFaturaRecurso->fatura_recursos as $faturaRecursos): ?>
                <tr>
                    <td><?= h($faturaRecursos->id) ?></td>
                    <td><?= h($faturaRecursos->data) ?></td>
                    <td><?= h($faturaRecursos->valor) ?></td>
                    <td><?= h($faturaRecursos->obs) ?></td>
                    <td><?= h($faturaRecursos->situacao_fatura_recurso_id) ?></td>
                    <td><?= h($faturaRecursos->atendimento_procedimento_id) ?></td>
                    <td><?= h($faturaRecursos->user_id) ?></td>
                    <td><?= h($faturaRecursos->situacao_id) ?></td>
                    <td><?= h($faturaRecursos->created) ?></td>
                    <td><?= h($faturaRecursos->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FaturaRecursos','action' => 'view', $faturaRecursos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FaturaRecursos','action' => 'edit', $faturaRecursos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FaturaRecursos','action' => 'delete', $faturaRecursos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $situacaoFaturaRecurso->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


