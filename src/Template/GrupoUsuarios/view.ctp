

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Usuarios</h2>
        <ol class="breadcrumb">
            <li>Grupo Usuarios</li>
            <li class="active">
                <strong>Litagem de Grupo Usuarios</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="grupoUsuarios">
    <h3><?= h($grupoUsuario->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($grupoUsuario->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $grupoUsuario->has('situacao_cadastro') ? $this->Html->link($grupoUsuario->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoUsuario->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($grupoUsuario->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($grupoUsuario->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($grupoUsuario->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

