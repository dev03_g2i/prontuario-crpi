<div class=" wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Usuários</h2>
        <ol class="breadcrumb">
            <li>Grupo Usuários</li>
            <li class="active">
                <strong>Listagem de Grupo Usuários</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Grupo Usuários', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Grupo Usuarios','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Grupo Usuários') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id', ['label' => 'Código']) ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grupoUsuarios as $grupoUsuario): ?>
            <tr>
                <td><?= $this->Number->format($grupoUsuario->id) ?></td>
                <td><?= h($grupoUsuario->nome) ?></td>
                <td><?= $grupoUsuario->has('situacao_cadastro') ? $this->Html->link($grupoUsuario->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoUsuario->situacao_cadastro->id]) : '' ?></td>
                <td><?= h($grupoUsuario->created) ?></td>
                <td><?= h($grupoUsuario->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('lock'), ['controller'=>'GrupoControladores','action' => 'index','?'=>['grupo_id' => $grupoUsuario->id]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Permissões','escape' => false,'class'=>'btn btn-xs btn-info','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $grupoUsuario->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $grupoUsuario->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $grupoUsuario->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

