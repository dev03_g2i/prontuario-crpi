<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Agendas</h2>
        <ol class="breadcrumb">
            <li>Tipo Agendas</li>
            <li class="active">
                <strong>Litagem de Tipo Agendas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Tipo Agendas</h3>
                        <table class="table table-hover">
                            <tr>
                                <th>
                                    <?= __('Nome') ?>
                                </th>
                                <td>
                                    <?= h($tipoAgenda->nome) ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Cor') ?>
                                </th>
                                <td>
                                    <input type="color" value="<?=$tipoAgenda->cor?>">
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Cor Sala de Espera') ?>
                                </th>
                                <td>
                                    <input type="color" value="<?=$tipoAgenda->cor_salaespera?>">
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Quem Cadastrou') ?>
                                </th>
                                <td>
                                    <?= $tipoAgenda->has('user') ? $this->Html->link($tipoAgenda->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoAgenda->user->id]) : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Situação Cadastro') ?>
                                </th>
                                <td>
                                    <?= $tipoAgenda->has('situacao_cadastro') ? $this->Html->link($tipoAgenda->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoAgenda->situacao_cadastro->id]) : '' ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Id') ?>
                                </th>
                                <td>
                                    <?= $this->Number->format($tipoAgenda->id) ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Dt. Criação') ?>
                                </th>
                                <td>
                                    <?= h($tipoAgenda->created) ?>
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    <?= __('Dt. Modificação') ?>
                                </th>
                                <td>
                                    <?= h($tipoAgenda->modified) ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>