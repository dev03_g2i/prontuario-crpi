<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Agendas</h2>
        <ol class="breadcrumb">
            <li>Tipo Agendas</li>
            <li class="active">
                <strong> Editar Tipo Agendas
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="tipoAgendas form">
                        <?= $this->Form->create($tipoAgenda) ?>
                            <fieldset>
                                <legend>
                                    <?= __('Editar Tipo Agenda') ?>
                                </legend>
                                   <?php require_once('form.ctp'); ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>