<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Agendas</h2>
        <ol class="breadcrumb">
            <li>Tipo Agendas</li>
            <li class="active">
                <strong>Litagem de Tipo Agendas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Tipo Agendas', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Tipo Agendas','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= __('Tipo Agendas') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>
                                            <?= $this->Paginator->sort('id') ?>
                                        </th>
                                        <th>
                                            <?= $this->Paginator->sort('nome') ?>
                                        </th>
                                        <th><?= $this->Paginator->sort('cor') ?></th>
                                        <th><?= $this->Paginator->sort('cor_salaespera') ?></th>
                                        <th>
                                            <?= $this->Paginator->sort('user_id') ?>
                                        </th>
                                        <th>
                                            <?= $this->Paginator->sort('situacao_id') ?>
                                        </th>
                                        <th>
                                            <?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?>
                                        </th>
                                        <th>
                                            <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                        </th>
                                        <th class="actions">
                                            <?= __('Ações') ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tipoAgendas as $tipoAgenda): ?>
                                    <tr>
                                        <td>
                                            <?= $this->Number->format($tipoAgenda->id) ?>
                                        </td>
                                        <td>
                                            <?= h($tipoAgenda->nome) ?>
                                        </td>
                                        <td><input type="color" value="<?=$tipoAgenda->cor?>"></td>
                                        <td><input type="color" value="<?=$tipoAgenda->cor_salaespera?>"></td>
                                        <td>
                                            <?= $tipoAgenda->has('user') ? $this->Html->link($tipoAgenda->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoAgenda->user->id]) : '' ?>
                                        </td>
                                        <td>
                                            <?= $tipoAgenda->has('situacao_cadastro') ? $this->Html->link($tipoAgenda->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoAgenda->situacao_cadastro->id]) : '' ?>
                                        </td>
                                        <td>
                                            <?= h($tipoAgenda->created) ?>
                                        </td>
                                        <td>
                                            <?= h($tipoAgenda->modified) ?>
                                        </td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $tipoAgenda->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $tipoAgenda->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), ['action' => 'delete', $tipoAgenda->id], ['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                                <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                    </ul>
                                    <p>
                                        <?= $this->Paginator->counter() ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>