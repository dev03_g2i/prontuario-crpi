<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Prestadores</h2>
            <ol class="breadcrumb">
                <li>Nf Prestadores</li>
                <li class="active">
                    <strong>Listagem de Nf Prestadores</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('NfPrestadore',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('razao_social',['name'=>'NfPrestadores__razao_social']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('inscricao_municipal',['name'=>'NfPrestadores__inscricao_municipal']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cpfcnpj',['name'=>'NfPrestadores__cpfcnpj']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('ddd',['name'=>'NfPrestadores__ddd']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('telefone',['name'=>'NfPrestadores__telefone']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('email',['name'=>'NfPrestadores__email']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cpfcnpj_intermediario',['name'=>'NfPrestadores__cpfcnpj_intermediario']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tipo_logradouro_id',['name'=>'NfPrestadores__tipo_logradouro_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('endereco',['name'=>'NfPrestadores__endereco']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('numero',['name'=>'NfPrestadores__numero']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('complemento',['name'=>'NfPrestadores__complemento']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tipo_bairro_id',['name'=>'NfPrestadores__tipo_bairro_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('bairro',['name'=>'NfPrestadores__bairro']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cep',['name'=>'NfPrestadores__cep']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cidade_id',['name'=>'NfPrestadores__cidade_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('atual',['name'=>'NfPrestadores__atual']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cnae',['name'=>'NfPrestadores__cnae']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tributacao_id',['name'=>'NfPrestadores__tributacao_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('aliquota_atividade',['name'=>'NfPrestadores__aliquota_atividade','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                                        </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Nf Prestadores', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Nf Prestadores','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Nf Prestadores') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('razao_social') ?></th>
                                                                                <th><?= $this->Paginator->sort('inscricao_municipal') ?></th>
                                                                                <th><?= $this->Paginator->sort('cpfcnpj') ?></th>
                                                                                <th><?= $this->Paginator->sort('ddd') ?></th>
                                                                                <th><?= $this->Paginator->sort('telefone') ?></th>
                                                                                <th><?= $this->Paginator->sort('email') ?></th>
                                                                                <th><?= $this->Paginator->sort('cpfcnpj_intermediario') ?></th>
                                                                                <th><?= $this->Paginator->sort('tipo_logradouro_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('endereco') ?></th>
                                                                                <th><?= $this->Paginator->sort('numero') ?></th>
                                                                                <th><?= $this->Paginator->sort('complemento') ?></th>
                                                                                <th><?= $this->Paginator->sort('tipo_bairro_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('bairro') ?></th>
                                                                                <th><?= $this->Paginator->sort('cep') ?></th>
                                                                                <th><?= $this->Paginator->sort('cidade_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('atual') ?></th>
                                                                                <th><?= $this->Paginator->sort('cnae') ?></th>
                                                                                <th><?= $this->Paginator->sort('tributacao_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('aliquota_atividade') ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($nfPrestadores as $nfPrestadore): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($nfPrestadore->id) ?></td>
                                                                                <td><?= h($nfPrestadore->razao_social) ?></td>
                                                                                <td><?= h($nfPrestadore->inscricao_municipal) ?></td>
                                                                                <td><?= h($nfPrestadore->cpfcnpj) ?></td>
                                                                                <td><?= h($nfPrestadore->ddd) ?></td>
                                                                                <td><?= h($nfPrestadore->telefone) ?></td>
                                                                                <td><?= h($nfPrestadore->email) ?></td>
                                                                                <td><?= h($nfPrestadore->cpfcnpj_intermediario) ?></td>
                                                                                <td><?= $this->Number->format($nfPrestadore->tipo_logradouro_id) ?></td>
                                                                                <td><?= h($nfPrestadore->endereco) ?></td>
                                                                                <td><?= $this->Number->format($nfPrestadore->numero) ?></td>
                                                                                <td><?= h($nfPrestadore->complemento) ?></td>
                                                                                <td><?= $this->Number->format($nfPrestadore->tipo_bairro_id) ?></td>
                                                                                <td><?= h($nfPrestadore->bairro) ?></td>
                                                                                <td><?= h($nfPrestadore->cep) ?></td>
                                                                                <td><?= $this->Number->format($nfPrestadore->cidade_id) ?></td>
                                                                                <td><?= h($nfPrestadore->atual) ?></td>
                                                                                <td><?= h($nfPrestadore->cnae) ?></td>
                                                                                <td><?= $this->Number->format($nfPrestadore->tributacao_id) ?></td>
                                                                                <td><?= $this->Number->format($nfPrestadore->aliquota_atividade) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'nfPrestadores','action' => 'view', $nfPrestadore->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'nfPrestadores','action' => 'edit', $nfPrestadore->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'nfPrestadores','action'=>'delete', $nfPrestadore->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
