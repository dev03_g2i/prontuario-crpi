
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Nf Prestadores</h2>
        <ol class="breadcrumb">
            <li>Nf Prestadores</li>
            <li class="active">
                <strong>Litagem de Nf Prestadores</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Nf Prestadores</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Razao Social') ?></th>
                                <td><?= h($nfPrestadore->razao_social) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Inscricao Municipal') ?></th>
                                <td><?= h($nfPrestadore->inscricao_municipal) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cpfcnpj') ?></th>
                                <td><?= h($nfPrestadore->cpfcnpj) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Ddd') ?></th>
                                <td><?= h($nfPrestadore->ddd) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Telefone') ?></th>
                                <td><?= h($nfPrestadore->telefone) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Email') ?></th>
                                <td><?= h($nfPrestadore->email) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cpfcnpj Intermediario') ?></th>
                                <td><?= h($nfPrestadore->cpfcnpj_intermediario) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Endereco') ?></th>
                                <td><?= h($nfPrestadore->endereco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Complemento') ?></th>
                                <td><?= h($nfPrestadore->complemento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td><?= h($nfPrestadore->bairro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($nfPrestadore->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cnae') ?></th>
                                <td><?= h($nfPrestadore->cnae) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tipo Logradouro Id') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->tipo_logradouro_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tipo Bairro Id') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->tipo_bairro_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cidade Id') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->cidade_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tributacao Id') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->tributacao_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Aliquota Atividade') ?></th>
                                <td><?= $this->Number->format($nfPrestadore->aliquota_atividade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Atual') ?>6</th>
                                <td><?= $nfPrestadore->atual ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Descricao Cnae') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($nfPrestadore->descricao_cnae)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


