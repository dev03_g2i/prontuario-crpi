

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Agendas</h2>
        <ol class="breadcrumb">
            <li>Situacao Agendas</li>
            <li class="active">
                <strong>Litagem de Situacao Agendas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="situacaoAgendas">
    <h3><?= h($situacaoAgenda->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($situacaoAgenda->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $situacaoAgenda->has('situacao_cadastro') ? $this->Html->link($situacaoAgenda->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $situacaoAgenda->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $situacaoAgenda->has('user') ? $this->Html->link($situacaoAgenda->user->nome, ['controller' => 'Users', 'action' => 'view', $situacaoAgenda->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($situacaoAgenda->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($situacaoAgenda->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($situacaoAgenda->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

