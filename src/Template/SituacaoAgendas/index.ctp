<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Agendas</h2>
        <ol class="breadcrumb">
            <li>Situacao Agendas</li>
            <li class="active">
                <strong>Litagem de Situacao Agendas</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Situacao Agendas') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('cor') ?></th>
                                    <th><?= $this->Paginator->sort('cor_atendimento') ?></th>
                                    <th><?= $this->Paginator->sort('user_id',['label'=>'Usuário']) ?></th>
                                    <th><?= $this->Paginator->sort('created',['label'=>'Dt. Cadastro']) ?></th>
                                    <th><?= $this->Paginator->sort('ocultar', 'Oculto') ?></th>
                                    <th class="actions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($situacaoAgendas as $situacaoAgenda): ?>
                                    <tr>
                                        <td><?= $this->Number->format($situacaoAgenda->id) ?></td>
                                        <td><?= h($situacaoAgenda->nome) ?></td>
                                        <td><button class="btn " style="background-color: <?= h($situacaoAgenda->cor) ?>;color: #fff;">Cor</button> </td>
                                        <td><button class="btn " style="background-color: <?= h($situacaoAgenda->cor_atendimento) ?>;color: #fff;">Cor Atendimento</button> </td>
                                        <td><?= $situacaoAgenda->has('user') ? $this->Html->link($situacaoAgenda->user->nome, ['controller' => 'Users', 'action' => 'view', $situacaoAgenda->user->id]) : '' ?></td>
                                        <td><?= h($situacaoAgenda->created) ?></td>
                                        <td><?=($situacaoAgenda->ocultar==0)?'Não':'Sim'; ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $situacaoAgenda->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

