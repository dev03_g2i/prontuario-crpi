<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Agendas</h2>
        <ol class="breadcrumb">
            <li>Situacao Agendas</li>
            <li class="active">
                <strong> Editar Situacao Agendas
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="situacaoAgendas form">
                        <?= $this->Form->create($situacaoAgenda)?>
                        <fieldset>
                            <legend><?= __('Editar Situacao Agenda') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('cor',['type'=>'color']);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('cor_atendimento',['type'=>'color']);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('ocultar',['options' => [0=>'Não',1=>'Sim'], 'selected' => $situacaoAgenda->ocultar]);
                            echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

