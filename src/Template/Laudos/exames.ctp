<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Histórico de Exames</h2>
            <ol class="breadcrumb">
                <li>Paciente: <strong><?=$paciente->nome; ?></strong></li>

            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Laudos') ?></h5>
                    </div>

                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th><?= $this->Paginator->sort('"id"',['label'=>'Arq.']) ?></th>
                                        <th><?= $this->Paginator->sort('ficha') ?></th>
                                        <th><?= $this->Paginator->sort('dtficha',['label'=>'Data Ficha']) ?></th>
                                        <th><?= $this->Paginator->sort('dt_entrega',['label'=>'Data Entrega']) ?></th>
                                        <th><?= $this->Paginator->sort('situacao',['label'=>'Situação']) ?></th>
                                        <th><?= $this->Paginator->sort('grupo_nome',['label'=>'Grupo']) ?></th>
                                        <th><?= $this->Paginator->sort('nome_exame',['label'=>'Exame']) ?></th>
                                        <th><?= $this->Paginator->sort('convenio_nome',['label'=>'Convênio']) ?></th>
                                        <th><?= $this->Paginator->sort('profissional_nome',['label'=>'Profissional']) ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($atendProc as $ap):?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><?=$ap->id?></td>
                                            <td><?=$ap->atendimento_id?></td>
                                            <td><?=$ap->atendimento->data?></td>
                                            <td><?=$ap->dt_entrega?></td>
                                            <td><?=($ap->has('situacao_laudo'))?$ap->situacao_laudo->nome:''?></td>
                                            <td><?=$ap->procedimento->grupo_procedimento->nome?></td>
                                            <td><?=$ap->procedimento->nome?></td>
                                            <td><?=$ap->atendimento->convenio->nome?></td>
                                            <td><?=$ap->medico_responsavei->nome?></td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-prontuario">
                                                        <li><?= $this->Html->link('<i class="fa fa-medkit"></i> Digitar Laudo', ['action' => 'laudar', $ap->id], ['escape' => false, 'target' => '_blank']) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-print"></i> Abrir Laudo', [], ['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-file-image-o"></i> Imagens',[], ['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-file-image-o"></i> Imagens AR',[],['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-picture-o"></i> Imprimir Imagens',[],['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-pencil"></i> Dados Clínicos', ['action' => 'edit', $ap->id, '?'=>['first'=>1]], ['escape' => false]) ?></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>