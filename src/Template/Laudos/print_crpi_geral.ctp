<?php
echo $this->Html->css('pdf/estilo_padrao');
echo $this->Html->css('print/print_crpi');
?>
<div>
    <div class="page-header">
        <div class="row header">
            <div class="col-1 " style="margin-bottom: 15px;visibility: <?= $laudos->cabecalho!=1?'':'hidden'?>">
                <?= $this->Html->image('docs/logo_crpi.png', ['style' => 'margin-left:20pt;height:100px;width:100px;']); ?>
            </div>
            <div class="col-5" style="margin-bottom: 10px;visibility: <?= $laudos->cabecalho!=1?'':'hidden'?>">
                <h2 style="text-align: center;font-family:Times New Roman,Times,serif">CENTRO RADIOLÓGICO POR IMAGEM</h2>
                <p class="text-center">Rua Rui Barbosa, 3206 - Centro - Fone: 3383-3151<br>Campo Grande-MS - Fone/Fax:
                    3383-2926</p>
            </div>
            <div class="clearfix" style="border-bottom: 1px solid #dedede"></div>
            <div class="col-6" style="margin-top: 10px;">
                <p class="text-left header-info title-center data-patient" style="padding-left: 120pt">
                    <span>Paciente:</span> <?= $laudo->paciente; ?><br/>
                    <span>Exame:</span> <?= $laudo->exame; ?> <?=(!empty($laudo->complemento) && $this->Configuracao->mostraProcedimentoComplemento())?' '.$laudo->complemento:null ?><br>
                    <span>Dr(a):</span> <?= $laudo->solicitante; ?><br/>
                    <span>Data:</span> <?= $laudo->data; ?> <span style="float: right; margin-right:30%">Dt Nasc: <?= $laudo->data_nasc; ?></span><br/>
                </p>
            </div>
        </div>
    </div>
    <div class="page-footer">
        <div class="footer-info">
            <div class="col-3" style="text-align: left;visibility: <?= $laudos->cabecalho!=1?'':'hidden'?>">
                <ul>
                    <li style="margin-bottom: 2px;list-style-type:none"><strong><i>&emsp;Dr. Ozualdo A. Barros Dalavia</i> <br/><i>
                                <small>Médico Radiologista - CRM-MS 773 - RQE 616</small></li>
                    <li>Residência HSE-RJ <br/>Título de Especialista em Radiologia</li>
                    <li>Pontifícia Universidade Católica</li>
                    <li>Colégio Brasileiro de Radiologia</li>
                    <li>Conselho Federal de Medicina</li>
                </ul>
            </div>
            <div class="col-3" style="text-align: left;visibility: <?= $laudos->cabecalho!=1?'':'hidden'?>">
                <ul>
                    <li style="margin-bottom: 2px;list-style-type:none"><strong><i>&emsp;&emsp;Dr. Claudio Carvalho Dalavia</i> <br/><i><small>Médico Radiologista - CRM-MS 5089 - RQE 3316</small></i></strong></li>
                    <li>Doutor em Radiologia e Diagnóstico por <br> Imagem pela Universidade Federal de <br> São Paulo</li>
                    <li>Membro titular do CBR</li>
                    <li>Professor de Radiologia da Faculdade de <br> Medicina - UFMS</li>
                </ul>
            </div>
            <div class="clearfix"></div>

        </div>
    </div>

    <table>
        <thead>
        <tr>
            <td>
                <!--place holder for the fixed-position header-->
                <div class="page-header-space"></div>
            </td>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>
                <!--*** CONTENT GOES HERE ***-->
                <div class="page">
                    <div class="content" style="padding-bottom: 10px;">
                        <?php echo $laudo->texto; ?>
                        <?php if(!empty($laudos->imagens)){ ?>
                        <p style="font-size:8pt;" class="break-spacing">
                            Exame documentado com <?= $laudos->imagens ?> foto(s)
                        </p>
                        <?php } ?>

                        <?php if(!empty($laudos->filmes)){ ?>
                            <p style="font-size:8pt;" class="break-spacing">
                                Exame documentado com <?= $laudos->filmes ?> filme(s)
                            </p>
                        <?php } ?>

                        <?php if(!empty($laudos->papeis)){ ?>
                            <p style="font-size:8pt;" class="break-spacing">
                                Exame documentado com <?= $laudos->papeis ?> folha(s) 13 e CD
                            </p>
                        <?php } ?>
                        <span style="font-size:8pt;float: left;">Digitado por:  <?= $laudo->digitado_por ?></span>
                        <span style="float: right;font-size:8pt;padding-right: 130pt;"> Fechado/Revisado por:</span><br/>
                        <span style="font-size:8pt;float: left;">Alterado por: <?= $laudo->alterado_por; ?></span>

                    </div>
                </div>
            </td>
        </tr>
        </tbody>

        <tfoot>
        <tr>
            <td>
                <!--place holder for the fixed-position footer-->
                <div class="page-footer-space"></div>
            </td>
        </tr>
        </tfoot>
    </table>
</div>

<?php if (empty($_GET['preview'])) { ?>
    <script>
        window.print();
    </script>
<?php } ?>

