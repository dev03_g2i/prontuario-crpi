<div class="wrapper  white-bg page-heading">
    <div class="col-lg-9">
        <h2>Laudos</h2>
        <ol class="breadcrumb">
            <li>Laudos</li>
            <li class="active">
                <strong>Etiquetas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Laudos', ['url' => ['action' => 'etiquetas'], 'target' => '_blank']) ?>

                        <div class="col-md-6">
                            <?= $this->Form->input('ids', ['label' => 'Exames', 'empty' => 'Selecione', 'type' => 'select', 'data' => 'select', 'controller' => 'AtendimentoProcedimentos', 'action' => 'fill', 'multiple' => 'multiple']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?= $this->Form->button($this->Html->icon('print') . ' Gerar Etiquetas', ['type' => 'submit', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Etiquetas', 'escape' => false, 'listen' => 'f']) ?>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>