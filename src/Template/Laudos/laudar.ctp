<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Digitar Laudo</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    
    <div class="area area_laudo">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content ">
                        <div class="laudo form">
                            <?= $this->Form->create($laudos,['id'=>'form-laudo']) ?>
                            <div class="col-md-7">
                                <h4>
                                    <strong>Paciente: </strong><?= $atendProc->atendimento->cliente->nome?> <br>
                                    <small><strong>Idade: </strong><?= $this->Data->idade($atendProc->atendimento->cliente->nascimento) ?></small><br />
                                    <small><strong>Solicitante: </strong><?= $atendProc->atendimento->solicitante->nome ?></small><br />
                                    <small><strong>Modalidade: </strong><?= $atendProc->procedimento->grupo_procedimento->nome ?></small><br>
									<small><strong>Digitado por: </strong><?= ($laudos->digitado_por)?$this->Laudo->assinadoPor($laudos->digitado_por).' - ':'Não Digitado'?> <?= ($laudos->digitado_por)?$laudos->created:''?></small>
                                </h4>
                            </div>
                            <div class="col-md-5">
                                <h4>
                                    <strong>Atendimento: </strong><?= $atendProc->atendimento_id ?> / <strong>Arquivamento: </strong><?=$atendProc->id?> <br />
                                    <small><strong>Responsável: </strong><?= $atendProc->medico_responsavei->nome ?> </small> <br />
                                    <small><strong>Procedimento: </strong><?= $atendProc->procedimento->nome ?> <?=(!empty($atendProc->complemento)) ?' '.$atendProc->complemento : null?> </small> <br />
                                    <small><strong>Assinado por: </strong><?= ($laudos->assinado_por)?$this->Laudo->assinadoPor($laudos->assinado_por).' - ':'Não Assinado'?> <?= ($laudos)?$laudos->dt_assinatura:''?></small><br>
									<small><strong>Alterado por: </strong><?= ($laudos->alterado_por)?$this->Laudo->assinadoPor($laudos->alterado_por).' - ':'Não Alterado'?> <?=  ($laudos->alterado_por)?$laudos->modified:''?></small>
									</h4>
                            </div>
                            <div class="clearfix"></div>
                            <fieldset>
                                <legend>
                                    <div class="col-md-12 text-left">
                                        <?php
                                        // Se a configuração estiver ativa, mostra o botão de salvar.
                                        if ($this->GrupoUsuarios->mostraBotaoSalvarEmLaudar()): ?>
                                            <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'value' => 'salvar', 'class' => 'btn btn-'.($this->Laudo->verificaAssinatura($laudos->id)?'danger':'primary'),'escape'=>false,'id'=>'salvar-laudo']) ?>
                                            <?php // echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'value' => 'salvar', 'class' => 'btn btn-'.($this->Laudo->verificaAssinatura($laudos->id)?'danger':'primary'),'escape'=>false,'id'=>'salvar-laudo']) ?>
                                        <?php endif; ?>
                                        <!-- <?= $this->Html->link('<i class="fa fa-exclamation"></i> Fechar', ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-cinza']) ?> -->
                                        <?php
                                        // Se a configuração estiver ativa, mostra o botão de assinar.
                                        if ($this->GrupoUsuarios->mostraBotaoAssinarEmLaudar()): ?>
                                            <?php /*CRPI*/ echo $this->Form->button('<i class="fa fa-pencil"></i> Assinar', ['type'=>'button','id'=>'btn_assinar', 'class' => 'btn btn-'.($this->Laudo->verificaAssinatura($laudos->id)?'danger':'cinza'),'escape'=>false]); ?>
                                            <?php // echo $this->Form->button('<i class="fa fa-pencil"></i> Assinar', ['type'=>'button','id'=>'btn_assinar', 'class' => 'btn btn-'.($this->Laudo->verificaAssinatura($laudos->id)?'danger':'cinza'),'escape'=>false, 'data-laudo-id' => !empty($laudos->id) ? $laudos->id : '']); ?>
                                        <?php endif; ?>
                                        <?= $this->Html->link('<i class="fa fa-exclamation"></i> Fechar', ['action' => 'index'], ['escape' => false, 'class' => 'btn btn-cinza'])?>
                                        <?= $this->Html->link('<i class="fa fa-print"></i> Imprimir',($laudos->id)?['action' => $config_laudo->modelo_impressao, '?' => ['id' => $laudos->id]]:'javascript:void(0)',['verifica-laudo' => ($laudos->id)?($laudos->id):'', 'escape' => false, 'id' => 'print-laudo', 'class' => ($laudos->id)?'btn btn-primary':'btn btn-cinza', 'target' => '_blank'])?>
                                        <?php
                                        $imagem = $this->Laudo->verificaImagem($atendProc->id);
                                        echo $this->Form->dropdownButton('<i class="fa fa-picture-o"></i> Imagens', [
                                            $this->Html->link('<i class="fa fa-file-image-o"></i> Imagens', $this->Laudo->gerarUrlImage($imagem) , ['escape' => false, 'target']),
                                            $this->Html->link('<i class="fa fa-file-image-o"></i> Imagens AR',$this->Laudo->gerarUrlImageAr($imagem),['escape' => false]),
                                            $this->Html->link('<i class="fa fa-picture-o"></i> Imprimir Imagens',[],['escape' => false])

                                        ],['class' => 'btn btn-cinza']);
                                        ?>
                                        <?= $this->Html->link('<i class="fa fa-files-o"></i> Modelos',['controller' => 'Textos', 'action' => 'index'],['class' => 'btn btn-cinza ', 'target' => '_blank', 'escape'=>false]) ?>
                                        <?= $this->Html->link($this->Form->button('<i class="fa fa-pencil"></i> Dados Clínicos', ['type'=>'button','class' => 'btn btn-cinza','escape'=>false]),['action'=>'edit',$atendProc->id,'ajax'=>1, 'first' => 1],['escape'=>false,'data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                        <?= $this->Html->link($this->Form->button('<i class="fa fa-refresh"></i> Histórico de exames', ['type'=>'button','class' => 'btn btn-cinza','escape'=>false]),['action'=>'exames',$atendProc->atendimento->cliente_id],['escape'=>false,'data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                        <?= $this->Html->link($this->Form->button('<i class="fa fa-list"></i> Rastrear Laudo', ['type'=>'button','class' => 'btn btn-cinza','escape'=>false]), ['controller' => 'LogLaudos', 'action' => 'index', $laudos->id], ['escape' => false, 'target' => '_blank']) ?>
                                    </div>

                                    <div class="clearfix"></div>
                                </legend>

                                <div class='col-md-6'>
                                    <?php echo $this->Form->input('grupo',['type'=>'select','label'=>'Grupos','data' => 'select', 'controller' => 'TextoGrupos', 'action' => 'fill']); ?>
                                </div>

                                <div class='col-md-6'>
                                    <?php echo $this->Form->input('modelo', ['options' => $textos, 'data' => 'select2-basic', 'controller' => 'Textos', 'empty' => 'Selecione',
                                        'append' => [
                                            $this->Form->button('<span class="fa fa-paste"></span>', [
                                                'id' => 'btnAddModelo', 'escape' => false, 'toggle' => 'tooltip','type'=>'button', 'data-placement' => 'bottom', 'title' => 'Colar Modelo', 'class' => 'btn btn-success'
                                            ])
                                        ]]); ?>
                                </div>
                                <?php if($this->LaudoConfiguracao->laudarQtdImagens()):?>
                                    <div class='col-md-3'>
                                        <?php echo $this->Form->input('imagens',['label'=>'Quantidade de Imagens']); ?>
                                    </div>
                                <?php endif;?>
                                <?php if($this->LaudoConfiguracao->laudarQtdFilmes()):?>
                                    <div class='col-md-3'>
                                        <?php echo $this->Form->input('filmes',['label'=>'Quantidade de Filmes']); ?>
                                    </div>
                                <?php endif;?>
                                <?php if($this->LaudoConfiguracao->laudarQtdPapeis()):?>
                                    <div class='col-md-3'>
                                        <?php echo $this->Form->input('papeis',['label'=>'Quantidade de Papéis']); ?>
                                    </div>
                                <?php endif;?>
                                <?php if($this->LaudoConfiguracao->laudarCabecalho()):?>
                                <div class='col-md-3' style='margin-top:16px'>
                                    <?php echo $this->Form->input('cabecalho',['label'=>' Não Exibir cabeçalho?']); ?>
                                </div>
                                <?php endif;?>
                                <?php if($this->LaudoConfiguracao->laudarNumeracaoPaginas()):?>
                                <div class='clearfix'></div>
                                <div class='col-md-3' >
                                    <?php echo $this->Form->input('paginas',['label'=>' Mostrar numeração de paginas?','type'=>'select','options'=>[1=>'Sim',0=>'Não'],'default'=>0]); ?>
                                </div>
                                <?php endif;?>
                                
                                <div class='col-md-12'>
                                    <?php echo $this->Form->input('texto_html',['id'=>'texto','label'=>'Laudo','data'=>'ck','required'=>'required']); ?>
                                </div>

                                <?php
                                echo $this->Form->input('atendimento_procedimento_id', ['type' => 'hidden', 'value' => $atendProc->id]);
                                /*echo $this->Form->input('ficha',['type'=>'hidden','value'=>$ficha]);
                                echo $this->Form->input('cadexameficha',['type'=>'hidden','value'=>$cadexameficha->tblCadastroExameIndex]);*/
                                echo $this->Form->input('assinar',['type'=>'hidden','value'=>'']);
                                echo $this->Form->input('imprimir_laudo',['type'=>'hidden','value'=>'']);
                                echo $this->Form->input('assinado_por',['type'=>'hidden','value'=> $laudos->assinado_por]);
                                echo $this->Form->input('laudo_reedicao',['type'=>'hidden','value'=> $user_logado['laudo_reedicao']]);
                                ?>

                            </fieldset>

                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>