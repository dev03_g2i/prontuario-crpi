<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <div class="col-lg-8">
            <h2>Laudos</h2>
            <ol class="breadcrumb">
                <li class="active">Ficha: <?= $atendProc->atendimento_id ?></li>
                <li class="active">
                    <strong> Paciente: <?= $atendProc->atendimento->cliente->nome?>
                    </strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4 ">

            <h3 class="text-right">
                <?php
                    $iniciarExame = (empty($atendProc->inicio_exame) && empty($atendProc->fim_exame)) ? true : false;
                    if(empty($iniciarExame->inicio_exame) && empty($atendProc->fim_exame)): ?>
                        <button column="<?=($iniciarExame)?'inicio_exame':'fim_exame'?>" type="button" class="btn btn-xs inicarFinalizarExame <?=($iniciarExame)?'btn-info':'btn-warning'?>" data-id="<?= $atendProc->id ?>"><?=($iniciarExame)?'Iniciar Exame':'Finalizar Exame'?></button>
                    <?php endif; ?>
            </h3>
            <h3> Inicio: <?= $this->Time->format($atendProc->inicio_exame,'dd/MM/YYYY HH:mm') ?>
                <div class="clearfix"></div>
                Fim: <?= $this->Time->format($atendProc->fim_exame,'dd/MM/YYYY HH:mm') ?>
            </h3>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tblcadastroexameficha form">
                            <?= $this->Form->create($atendProc) ?>
                            <fieldset>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('procedimento_id', ['label' => 'Exame','options'=>$exames]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('medico_id', ['label' => 'Profissional', 'options' => $profissional, 'empty'=>'Selecione']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('revisor_id', ['label' => 'Revisor', 'options' => $profissional,'empty'=>'Selecione']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('dt_entrega', ['label' => 'Data Entrega', 'type' => 'text', 'value' => $this->Time->format($atendProc->dt_entrega, 'dd/MM/YYYY HH:mm'), 'class' => "datetimepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('enfermeiro_id', ['label' => 'Enfermeiro', 'options' => $enfermeiro, 'empty'=>'Selecione']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tecnico_id', ['label' => 'Técnico', 'options' => $tecnicos, 'empty'=>'Selecione']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('prioridade_id', ['label' => 'Prioridade','type'=>'select','options'=>$prioridades,'empty'=>'Selecione']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('textos',['label'=>'Textos','data' => 'select2-basic', 'controller' => 'AnotacaoModelos', 'options' => $anotacaoModelos, 'empty' => 'Selecione',
                                    'append' => [
                                        $this->Html->link('<i class="fa fa-clipboard"></i>', ['controller' => 'AnotacaoModelos', 'action' => 'add'], ['class' => 'btn btn-default', 'listen' => 'f', 'target' => '_blank', 'escape' => false])
                                    ]
                                ]);
                                //echo $this->Html->link('teste', ['controller' => 'AnotacaoModelos', 'action' => 'add'], ['class' => 'add-textos', 'id' => 'add-textos', 'listen' => 'f', 'target' => '_blank']);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('anotacao_laudo', ['type'=>'textarea','label' => 'Anotação', 'value'=>(!empty($atendProc->anotacao_laudo)?$atendProc->anotacao_laudo:'')]);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                    <?= $this->Form->button(__('Fechar'), ['type'=>'button','class' => 'btn btn-danger', 'data-dismiss' => 'modal', 'aria-label' => 'Close']) ?>
                                    <!--<?= $this->Form->button(__('Fechar'), ['type'=>'button','class' => 'btn btn-danger','onclick'=>'Navegar(\'\',\'back\')']) ?>-->
                                    <?= $this->Form->button(__('Salvar'), ['type'=>'submit', 'class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>