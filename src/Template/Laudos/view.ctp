
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tblcadastroexameficha</h2>
        <ol class="breadcrumb">
            <li>Tblcadastroexameficha</li>
            <li class="active">
                <strong>Litagem de Tblcadastroexameficha</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Tblcadastroexameficha</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('TblCadastroExameFichaNumLaminas') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameFichaNumLaminas) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaCores') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameFichaCores) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaVolume') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameFichaVolume) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameEntreguePor') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameEntreguePor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameEntreguePara') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameEntreguePara) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDeveDesmarcadoPor') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameDeveDesmarcadoPor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDeveConferidoPor') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameDeveConferidoPor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeDocumento') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeDocumento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDocumentoGuia') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameDocumentoGuia) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameObservacoesGerais') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameObservacoesGerais) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameLote') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameLote) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameCodAMB') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameCodAMB) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameAPC') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameAPC) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNumGuiaTISS') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNumGuiaTISS) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameAtendidoPor') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameAtendidoPor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDtDigitado') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameDtDigitado) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomePaciente') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomePaciente) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeExame') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeExame) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeGrupo') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeGrupo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeTecnico') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeTecnico) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeEnfermeira') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeEnfermeira) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeConvenio') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeConvenio) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNomeRadiologista') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameNomeRadiologista) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExamePrioridadeNome') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExamePrioridadeNome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameOrigemNome') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameOrigemNome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDestinoNome') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameDestinoNome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameSetor') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameSetor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameSolicitanteNome') ?></th>
                                <td><?= h($tblcadastroexameficha->tblCadastroExameSolicitanteNome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameIndex') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameIndex) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblExameId') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblExameId) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblFichaIdGeral') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblFichaIdGeral) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblGrupoExameId') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblGrupoExameId) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblPostoId') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblPostoId) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaValor') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaValor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaDesconto') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaDesconto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaDescontoPorct') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaDescontoPorct) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaTaxa') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaTaxa) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaMaterial') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaMaterial) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaImpressaQtdadePrints') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaImpressaQtdadePrints) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaFrascos') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaFrascos) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaLaminas') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaLaminas) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameEntregueNumDespacho') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameEntregueNumDespacho) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNumControleFaturamento') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameNumControleFaturamento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameRadiologista') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameRadiologista) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameMedicamento') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameMedicamento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaValorFilme') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaValorFilme) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaValorCHHH') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaValorCHHH) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaValorCHOP') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameFichaValorCHOP) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameAutorizPara') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameAutorizPara) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameAPCLAUDO') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameAPCLAUDO) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameTecnico') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameTecnico) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameEnfermeira') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameEnfermeira) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameNumeracao') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameNumeracao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameTecnicoDoc') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameTecnicoDoc) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameTecnicoAnexo') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameTecnicoAnexo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameTecnicoDiverso') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameTecnicoDiverso) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameTipoUnimed') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameTipoUnimed) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameStatus') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameStatus) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameGrupo') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameGrupo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExamePrioridade') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExamePrioridade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameVlBaseNF') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameVlBaseNF) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameCodPaciente') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameCodPaciente) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameRevisor') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameRevisor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameOrigemCod') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameOrigemCod) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDestinoCod') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameDestinoCod) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameSituacaoAtendimento') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameSituacaoAtendimento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameSincronismo') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameSincronismo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameSolicitanteCodigo') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameSolicitanteCodigo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameWebImgLink') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameWebImgLink) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameWebLaudo') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameWebLaudo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameWebDados') ?></th>
                                <td><?= $this->Number->format($tblcadastroexameficha->tblCadastroExameWebDados) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameEntregueData') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameEntregueData) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDeveDesmarcadoData') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDeveDesmarcadoData) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDeveConferidoData') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDeveConferidoData) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDtFaturamento') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDtFaturamento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExamePreConferidoData') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExamePreConferidoData) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExamePagoMedicoData') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExamePagoMedicoData) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameAPCData') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameAPCData) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameAPCInicioValidade') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameAPCInicioValidade) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameAPCFimValidade') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameAPCFimValidade) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDtAutorizacao') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDtAutorizacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDtEmissaoGuia') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDtEmissaoGuia) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExamePrevEntregaDt') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExamePrevEntregaDt) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExamePrevEntregaHora') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExamePrevEntregaHora) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameAtendido') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameAtendido) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDtFicha') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDtFicha) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameDtAgendamento') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameDtAgendamento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameCompareceu') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameCompareceu) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameAtendidoProfissional') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameAtendidoProfissional) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameInicioExame') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameInicioExame) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('TblCadastroExameFimExame') ?></th>
                                                                <td><?= h($tblcadastroexameficha->tblCadastroExameFimExame) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaDigitado') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameFichaDigitado ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFichaImpressoOriginal') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameFichaImpressoOriginal ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameEntregue') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameEntregue ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDeveGuia') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameDeveGuia ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameDeveConferido') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameDeveConferido ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameAtualizadoEstoqueSolicitante') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameAtualizadoEstoqueSolicitante ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameFaturado') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameFaturado ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExamePreConferidoFaturamento') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExamePreConferidoFaturamento ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameGlosado') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameGlosado ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameRecebido') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameRecebido ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExameAtualizadoEstoqueProcedencia') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExameAtualizadoEstoqueProcedencia ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('TblCadastroExamePagoMedico') ?>6</th>
                                <td><?= $tblcadastroexameficha->tblCadastroExamePagoMedico ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>