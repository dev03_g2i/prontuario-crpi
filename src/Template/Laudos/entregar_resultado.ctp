<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Laudos - Entregar Resultado</h2>
            <h4>Atendimento: <?=$dados->atendimento_id?> - <?=$dados->atendimento->cliente->nome?></h4>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tblcadastroexameficha form">
                            <?= $this->Form->create('entregarResultado') ?>
                            <div class="col-md-12">
                                <?php echo $this->Form->input('retirado_por', ['type' => 'text', 'required' => true]); ?>
                            </div>
                            <div class="col-md-12 ulEntregarLaudo">
                                <ul>
                                    <?php $html = '';
                                    foreach ($atendProc as $ap):
                                        if($ap->situacao_laudo_id == 11){// Entregue
                                            $html = '<strong>'.$ap->procedimento->nome.'</strong> - Retirado por: '.$ap->retirado_por.' - em '.$this->Time->format($ap->dt_entrega_resultado, 'dd/MM/YYYY').' às '.$this->Time->format($ap->dt_entrega_resultado, 'HH:mm');
                                        }else {
                                            $html = '<strong>'.$ap->procedimento->nome.'</strong> - <input type="checkbox" name="entregar[]" value="'.$ap->id.'">';
                                        }
                                        ?>
                                        <li><?php echo $html; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>