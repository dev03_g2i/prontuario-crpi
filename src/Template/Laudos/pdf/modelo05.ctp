<?php
    echo $this->Dompdf->css('pdf/modelo05');
?>
<div id="header">
    <div class="row header-info">
        <div class="col-print-9">
            <ul class="lista-info">
                <li>Paciente: <strong><?= $laudos->atendimento_procedimento->atendimento->cliente->nome ?></strong></li>
                <li>Médico: <strong><?= $laudos->atendimento_procedimento->medico_responsavei->nome ?></strong></li>
                <li>Exame: <strong><?= $laudos->atendimento_procedimento->id ?></strong></li>
            </ul>
        </div>
        <div class="col-print-3">
            <ul class="lista-info">
                <li>Idade: <strong><?= $laudos->atendimento_procedimento->atendimento->cliente->idade ?></strong></li>
                <li>RG: <strong><?= $laudos->atendimento_procedimento->atendimento->cliente->rg ?></strong></li>
                <li>Data: <strong><?= $laudos->atendimento_procedimento->atendimento->created ?></strong></li>
            </ul>
        </div>    
    </div>
</div>

<div class="content">
    <?php echo $laudos->texto_html; ?>
</div>

<div id="assinatura-print">
    <?php if(!empty($caminho_assinatura)):?>
        <img src="<?= $caminho_assinatura?>" class="img-responsive"/>
    <?php endif;?>
    <hr>
    <ul>
        <li><?= $laudos->atendimento_procedimento->medico_responsavei->nome ?></li>
        <li><?= $laudos->atendimento_procedimento->medico_responsavei->sigla . ' ' . $laudos->atendimento_procedimento->medico_responsavei->conselho ?></li>
    </ul>
</div>