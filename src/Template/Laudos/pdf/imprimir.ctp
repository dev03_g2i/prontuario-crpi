<div id="header">
    <div id="image-print" class="text-center">
        <?php echo $this->Dompdf->image('logo_cliente.png',['class' => 'img-responsive']); ?>
    </div>
    <div class="row">
        <div class="lista-info border" style="height:180px; padding: 5px;">
            <div class="col-print-6">
                <ul style="text-indent: -35px">
                    <li>Paciente: <?= $laudos->atendimento_procedimento->atendimento->cliente->nome ?></li>
                    <li>Médico Solicitante: <?= $laudos->atendimento_procedimento->atendimento->solicitante->nome ?></li>
                    <li>Convênio:<?= $laudos->atendimento_procedimento->atendimento->convenio->nome ?></li>
                </ul>
            </div>
            <div class="col-print-6 text-right">
                <ul>
                    <li>N° Exame: <?= $laudos->atendimento_procedimento->id ?></li>
                    <li>Data: <?= $laudos->atendimento_procedimento->atendimento->created ?></li>
                    <li>Idade: <?= $idade ?> </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script type="text/php">
    if (isset($pdf)) {
        $x = 270;
        $y = 10;
        $text = "Pagina {PAGE_NUM} de {PAGE_COUNT}";
        $font = null;
        $size = 10;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>

<?php echo $laudos->texto_html; ?>

<div id="assinatura-print">
    <?php if(!empty($caminho_assinatura)):?>
        <img src="<?= $caminho_assinatura?>" class="img-responsive"/>
    <?php endif;?>
    <hr>
    <ul>
        <li><?= $laudos->atendimento_procedimento->medico_responsavei->nome ?></li>
        <li><?= $laudos->atendimento_procedimento->medico_responsavei->sigla . ' ' . $laudos->atendimento_procedimento->medico_responsavei->conselho ?></li>
    </ul>
</div>
<div id="footer" class="text-center">
    <i>Rua Américo Marques, N.203 - Vila Sobrinho - CEP: 79.110-300</i><br/>
    <i>Campo Grande - MS Fones: (67)3362-4560</i>
</div>