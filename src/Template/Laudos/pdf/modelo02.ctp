<?php
    echo $this->Dompdf->css('pdf/modelo02');
    $top = "160pt";
?>

<div id="header" >
    <?php if($laudos->cabecalho!=1):
        $top = "110pt";
    ?>
    <div id="local">
        <div class="col-md-2">
            <?= $this->Dompdf->image('sim-radiologia.png', ['class' => 'img-responsive']) ?>
        </div>
        <div class="col-md-10" style="text-align: center">
            <h2>Sim – Serviços de Imagens Médicas</h2>
            <p style="text-align: center">Rua Antonio Maria Coelho, 1636 – Centro – Campo Grande – MS <br /> Tel. 67 3305-8741 / 9656-6085 / 99103-8487</p>
        </div>

        <div class="clearfix"></div>
    </div>
        <div class="clearfix"></div>


    <?php endif; ?>
</div>
<div id="footer">
    
</div>
<div id="content" style="top: <?= $top ?>;">
    <table cellpadding="0" cellspacing="0" border="0" id="tabela" >
        <tr>
            <td width="100" style="text-align: right; padding-right:50px ">Paciente:</td>
            <td width="190"><?= $laudo->paciente ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:70px ">Exame:</td>
            <td><?= $laudo->exame  ?> <?=(!empty($laudo->complemento) && $this->Configuracao->mostraProcedimentoComplemento())?' '.$laudo->complemento:null ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:90px ">Dr(a):</td>
            <td><?= $laudo->solicitante ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:96px ">Data:</td>
            <td><?= $laudo->data ?></td>
        </tr>

    </table>

    <?= $laudo->texto ?> <br />
    <div id="documentado">
        <div class="col-md-6">
            <?php if(!empty($laudos->imagens)){ ?>
                Exame documentado com <?= $laudos->imagens ?> foto(s) <br />
            <?php } ?>

            <?php if(!empty($laudos->filmes)){ ?>
                Exame documentado com <?= $laudos->filmes ?> filme(s) <br />
            <?php } ?>

            <?php if(!empty($laudos->papeis)){ ?>
                Exame documentado com <?= $laudos->papeis ?> folha(s) 13 e CD <br />
            <?php } ?>
            Digitado por:  <?= $laudo->digitado_por ?>
        </div>
    <div class="col-md-6">
        <?php if(!empty($laudos->imagens)){ ?>
            <br />
        <?php } ?>

        <?php if(!empty($laudos->filmes)){ ?>
            <br />
        <?php } ?>
        <?php if(!empty($laudos->papeis)){ ?>
            <br />
        <?php } ?>
        Fechado/Revisado por:
    </div>
    </div>

</div>