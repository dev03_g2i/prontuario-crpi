<?php
    echo $this->Dompdf->css('pdf/modelo03');
?>
<div id="header">
    <div class="row">
        <div class="col-print-6">
                <div class="logo">
                    <?= $this->Dompdf->image('laudos/logo.jpg', ['class' => 'img-responsive']) ?>
                </div>
            </div>
            <div class="col-print-6">
                <ul class="lista-info">
                    <li>Paciente: <?= $laudos->atendimento_procedimento->atendimento->cliente->nome ?></li>
                    <li>N° Exame: <?= $laudos->atendimento_procedimento->id ?></li>
                    <li>Dr(a): <?= $laudos->atendimento_procedimento->atendimento->solicitante->nome ?></li>
                    <li>Convênio:<?= $laudos->atendimento_procedimento->atendimento->convenio->nome ?></li>
                    <li>Data: <?= $laudos->atendimento_procedimento->atendimento->created ?></li>
                </ul>
        </div>
    </div>
</div>
<script type="text/php">
    if (isset($pdf)) {
        $x = 270;
        $y = 10;
        $text = "Pagina {PAGE_NUM} de {PAGE_COUNT}";
        $font = null;
        $size = 10;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>

<div class="content">
    <?php echo $laudos->texto_html; ?>
</div>

<div id="assinatura-print">
    <?php if(!empty($caminho_assinatura)):?>
        <img src="<?= $caminho_assinatura?>" class="img-responsive"/>
    <?php endif;?>
    <hr>
    <ul>
        <li><?= $laudos->atendimento_procedimento->medico_responsavei->nome ?></li>
        <li><?= $laudos->atendimento_procedimento->medico_responsavei->sigla . ' ' . $laudos->atendimento_procedimento->medico_responsavei->conselho ?></li>
    </ul>
</div>
<div id="footer" class="text-center">
    <i>Rua Américo Marques, N.203 - Vila Sobrinho - CEP: 79.110-300</i><br/>
    <i>Campo Grande - MS Fones: (67)3362-4560</i>
</div>