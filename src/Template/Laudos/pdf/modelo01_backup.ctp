<?php
    echo $this->Dompdf->css('pdf/header_footer');
    echo $this->Dompdf->css('pdf/modelo01');
    $top = "140pt";
?>

<div id="header" >
    <?php if($laudos->cabecalho!=1):
        $top = "90pt";
    ?>
    <div id="local">
        <div class="col-md-12">
            <div class="col-md-2 logo">
                <?= $this->Dompdf->image('logo_crpi.png', ['class' => 'img-responsive']) ?>
            </div>
            <div class="header-txt">
                <h2>CENTRO RADIOLÓGICO POR IMAGEM</h2>
                <p>Rua Rui Barbosa, 3206 - Centro - Fone: 3383-3151<br />Campo Grande-MS - Fone/Fax: 3383-2926</p>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
        <div class="clearfix"></div>
    <?php endif; ?>
</div>
<div id="footer">
    <?php if($laudos->cabecalho!=1):
    ?>
    <div class="col-md-5">
        <strong><i>Dr. Ozualdo A. Barros Dalavia</i> <br />
            <i><small>Médico Radiologista -CRM-MS 773</small></i>
        </strong>
    </div>
    <div class="col-md-5">
        <strong><i>Dr. Claudio Carvalho Dalavia</i> <br />
            <i><small>Médico Radiologista -CRM-MS 5089</small></i>
        </strong>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-6">
        <ul>
            <li>Residência HSE-RJ <br />Título de Especialista em Radiologia</li>
            <li>Pontifícia Universidade Católica</li>
            <li>Colégio Brasileiro de Radiologia</li>
            <li>Conselho Federal de Medicina</li>
        </ul>
    </div>
    <div class="col-md-6" style="padding-left: 0pt">
        <ul>
            <li>Doutor em Radiologia e Diagnóstico por <br> Imagem pela Universidade Federal de <br> São Paulo</li>
            <li>Membro titular do CBR</li>
            <li>Professor de Radiologia da Faculdade de <br> Medicina - UFMS</li>
        </ul>
    </div>
    <div class="clearfix"></div>
<?php endif; ?>
</div>
<div id="content" style="top: <?= $top ?>;">
    <table cellpadding="0" cellspacing="0" border="0" id="tabela" >
        <tr>
            <td width="100" style="text-align: right; padding-right:50px ">Paciente:</td>
            <td colspan="3"><?= $laudo->paciente ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:70px ">Exame:</td>
            <td colspan="3"><?= $laudo->exame  ?> <?=(!empty($laudo->complemento) && $this->Configuracao->mostraProcedimentoComplemento())?' '.$laudo->complemento:null ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:90px ">Dr(a):</td>
            <td colspan="3"><?= $laudo->solicitante ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:96px ">Data:</td>
            <td><?= $laudo->data ?></td>
			<td width="100" style="text-align: right; padding-right:96px ">Dt Nasc:</td>
			<td><?= $laudo->data_nasc ?></td>
        </tr>

    </table>

    <?= $laudo->texto ?> <br />
    <div id="documentado">
        <div class="col-md-6">
            <?php if(!empty($laudos->imagens)){ ?>
                Exame documentado com <?= $laudos->imagens ?> foto(s) <br />
            <?php } ?>

            <?php if(!empty($laudos->filmes)){ ?>
                Exame documentado com <?= $laudos->filmes ?> filme(s) <br />
            <?php } ?>

            <?php if(!empty($laudos->papeis)){ ?>
                Exame documentado com <?= $laudos->papeis ?> folha(s) 13 e CD <br />
            <?php } ?>
            Digitado por:  <?= $laudo->digitado_por ?><br>
            Alterado por: <?= $laudo->alterado_por; ?>
        </div>
    <div class="col-md-6">
        <?php if(!empty($laudos->imagens)){ ?>
            <br />
        <?php } ?>

        <?php if(!empty($laudos->filmes)){ ?>
            <br />
        <?php } ?>
        <?php if(!empty($laudos->papeis)){ ?>
            <br />
        <?php } ?>
        Fechado/Revisado por:
    </div>
    </div>

</div>