<?php
    echo $this->Dompdf->css('pdf/modelo04');
    $top = "160pt";
?>

<div id="header" >
    <?php if($laudos->cabecalho!=1):
        $top = "110pt";
    ?>
    <div id="local">
        <div class="col-md-2">
            <div class="logo">
                <?= $this->Dompdf->image('laudos/logo.jpg', ['class' => 'img-responsive']) ?>
            </div>
        </div>
        <div class="col-md-10" style="text-align: center">
            <h2><?= $laudo->titulo_clinica; ?></h2>
            <!--<p style="text-align: center">Rua Rui Barbosa, 3206 - Centro - Fone: 3383-3151<br />Campo Grande-MS - Fone/Fax: 3383-2926</p>-->
            <p style="text-align: center"><?= $laudo->endereco; ?></p>
        </div>

        <div class="clearfix"></div>
    </div>
        <div class="clearfix"></div>
    <?php endif; ?>
</div>

<div id="footer" class="text-center">
    <i><?= $laudo->endereco; ?></i>
</div>

<div id="content" style="top: <?= $top ?>;">
    <table cellpadding="0" cellspacing="0" border="0" id="tabela" >
        <tr>
            <td width="100" style="text-align: right; padding-right:50px ">Paciente:</td>
            <td ><?= $laudo->paciente ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:70px ">Exame:</td>
            <td><?= $laudo->exame  ?> <?=(!empty($laudo->complemento) && $this->Configuracao->mostraProcedimentoComplemento())?' '.$laudo->complemento:null ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:90px ">Dr(a):</td>
            <td><?= $laudo->solicitante ?></td>
        </tr>
        <tr>
            <td width="100" style="text-align: right; padding-right:96px ">Data:</td>
            <td><?= $laudo->data ?></td>
        </tr>

    </table>

    <?= $laudo->texto ?> <br />
    
    <div id="assinatura-print">
        <?php if(!empty($caminho_assinatura)): ?>
            <img src="<?= $caminho_assinatura?>" class="img-responsive"/>
        <?php endif;?>
        <hr>
        <ul>
            <li><?= $laudos->atendimento_procedimento->medico_responsavei->nome ?></li>
            <li><?= $laudos->atendimento_procedimento->medico_responsavei->sigla . ' ' . $laudos->atendimento_procedimento->medico_responsavei->conselho ?></li>
        </ul>
    </div>

</div>