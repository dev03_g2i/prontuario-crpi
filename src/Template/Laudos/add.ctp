<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tblcadastroexameficha</h2>
            <ol class="breadcrumb">
                <li>Tblcadastroexameficha</li>
                <li class="active">
                    <strong> Cadastrar Tblcadastroexameficha
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tblcadastroexameficha form">
                            <?= $this->Form->create($tblcadastroexameficha) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Tblcadastroexameficha') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblExameId');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblFichaIdGeral');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblGrupoExameId');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblPostoId');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaValor', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaNumLaminas');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaDesconto', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaDescontoPorct');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaTaxa', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaDigitado');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaMaterial');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaImpressoOriginal');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaImpressaQtdadePrints');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaCores');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaFrascos');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaLaminas');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaVolume');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameEntregue');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameEntreguePor');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameEntregueData');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameEntreguePara');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameEntregueNumDespacho');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDeveGuia');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDeveDesmarcadoPor');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDeveDesmarcadoData');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDeveConferido');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDeveConferidoPor');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDeveConferidoData');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeDocumento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAtualizadoEstoqueSolicitante');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDtFaturamento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFaturado');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNumControleFaturamento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePreConferidoFaturamento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePreConferidoData');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameGlosado');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameRecebido');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAtualizadoEstoqueProcedencia');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameRadiologista');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameMedicamento', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDocumentoGuia');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameObservacoesGerais');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameLote');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaValorFilme', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaValorCHHH', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFichaValorCHOP', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePagoMedico');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePagoMedicoData');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameCodAMB');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAutorizPara');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAPC');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAPCData');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAPCInicioValidade');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAPCFimValidade');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAPCLAUDO');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameTecnico');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameEnfermeira');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNumeracao');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameTecnicoDoc');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNumGuiaTISS');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDtAutorizacao');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDtEmissaoGuia');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameTecnicoAnexo');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameTecnicoDiverso');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameTipoUnimed');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePrevEntregaDt');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePrevEntregaHora');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameStatus');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAtendido');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAtendidoPor');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameGrupo');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDtDigitado');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDtFicha');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomePaciente');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeExame');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeGrupo');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeTecnico');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeEnfermeira');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeConvenio');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameNomeRadiologista');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePrioridade');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExamePrioridadeNome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameVlBaseNF', ['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameCodPaciente');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameRevisor');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameOrigemCod');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameOrigemNome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDestinoCod');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDestinoNome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameDtAgendamento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameSetor');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameCompareceu');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameAtendidoProfissional');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameSituacaoAtendimento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameSincronismo');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameSolicitanteCodigo');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameSolicitanteNome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameInicioExame');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameFimExame');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameWebImgLink');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameWebLaudo');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tblCadastroExameWebDados');
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>