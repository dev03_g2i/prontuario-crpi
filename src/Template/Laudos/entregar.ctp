<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Entregar Exame</h2>
            <ol class="breadcrumb">
                <li>Entrega</li>
                <li class="active">
                    <strong>Exame</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tblcadastroexameficha form">
                            <?= $this->Form->create($tblcadastroexameficha) ?>
                            <fieldset>
                                <?php
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('pacientenome', ['label' => 'Paciente', 'readonly' => 'readonly']);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('nome_exame', ['label' => 'Paciente', 'readonly' => 'readonly']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('dt_entrega', ['label' => 'Data Entrega','readonly' => 'readonly', 'type' => 'text', 'value' => date('d/m/Y H:i'), 'class' => "datetimepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('entregue_por', ['label' => 'Entregue por ', 'readonly' => 'readonly','value'=>!empty($tblcadastroexameficha->entregue_por)?$tblcadastroexameficha->entregue_por:$entregue_por]);
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('entrege_para', ['label' => 'Para', 'type' => 'text']);
                                echo "</div>";
                                ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>