<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Laudos</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Laudos', ['type' => 'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('filtro_atalho',['label'=>'Filtros (Atalho)','type'=>'select','options'=>$situacao_atalhos,'empty'=>'Selecione', 'default'=> (!empty($atendimento_id)) ? null : $filtro_atalho]); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('situacao',['label'=>'Situação','type'=>'select','options'=>$situacao_laudos,'empty'=>'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('tipo_data',['label'=>'Tipo Data','type'=>'select','options'=>['Atendimentos.data'=>'Data Ficha','AtendimentoProcedimentos.dt_entrega'=>'Data Entrega'], 'default'=>'Atendimentos.data']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('grupo_procedimento_id',['label'=>'Modalidade','type'=>'select','options'=>$grupo_procedimentos,'empty'=>'Selecione']); ?>
                            </div>
                            <!-- ID Atendimento -->
                            <div class="col-md-4">
                                <?= $this->Form->input('ficha', ['label' => 'Atendimento', 'type' => 'text', 'value' => (!empty($atendimento_id)?$atendimento_id:'')]); ?>
                            </div>
                            <div class="col-md-4">
                                <!-- ID AtendimentoProcedimento -->
                                <?= $this->Form->input('arquivo_integracao', ['label' => 'Exame/Integração', 'type' => 'text']); ?>
                            </div>
                            <?php if($user_diagnostico == 0):// Técnico?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('profissional',['class' => 'select2', 'type'=>'select','options'=>$medicos,'empty'=>'Selecione','default' => !empty($atendimento_id) ? null : @$user_logado['codigo_medico']]); ?>
                            </div>
                            <?php endif; ?>
                            <div class="col-md-4">
                                <?= $this->Form->input('paciente', ['label' => 'Paciente', 'type' => 'text']); ?>
                            </div>
                            <div id="periodos" style="display: none;">
                                <div class="col-md-4">
                                    <div class="col-md-6">
                                        <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $this->Form->input('fim', ['label' => 'Data Final', 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default btn-count-laudos', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                <?= $this->Form->button('0', ['type' => 'button', 'class' => 'btn btn-default clearBtn btn-laudo-aguardando', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Aguardando', 'escape' => false]) ?>
                                <?= $this->Form->button('0', ['type' => 'button', 'class' => 'btn btn-default clearBtn btn-laudo-exame', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Exame', 'escape' => false]) ?>
                                <?= $this->Form->button('0', ['type' => 'button', 'class' => 'btn btn-default clearBtn btn-laudo-liberado', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Liberado', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th>&nbsp;</th>
                                        <th></th>
                                        <th><?= $this->Paginator->sort('Atendimento') ?></th>
                                        <th><?= $this->Paginator->sort('Exame') ?></th>
                                        <th><?= $this->Paginator->sort('dtficha',['label'=>'Data Ficha']) ?></th>
                                        <!--<th><?= $this->Paginator->sort('preventrega',['label'=>'Data Entrega']) ?></th>-->
                                        <th class="<?=$this->Configuracao->showPrevEntrega()?>"><?= $this->Paginator->sort('prev_entrega_proc',['label'=>'Prev.Entrega']) ?></th>
                                        <th><?= $this->Paginator->sort('situacao_laudo_id',['label'=>'Situação']) ?></th>
                                        <th><?= $this->Paginator->sort('grupo_nome',['label'=>'Modalidade']) ?></th>
                                        <th><?= $this->Paginator->sort('pacientenome',['label'=>'Paciente']) ?></th>
                                        <th><?= $this->Paginator->sort('nome_exame',['label'=>'Exame']) ?></th>
                                        <th><?= $this->Paginator->sort('convenio_nome',['label'=>'Convênio']) ?></th>
                                        <th><?= $this->Paginator->sort('profissional_nome',['label'=>'Profissional']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $imagem = null;
                                    foreach ($atendProc as $ap):
                                        $imagem = $this->Laudo->verificaImagem($ap->id);?>
                                        <tr>
                                            <td>
                                                <?php if(empty($ap->fim_exame) && empty($ap->inicio_exame)): ?>
                                                    <i class="fa fa-circle color-laudo-aguardando" data-toggle="tooltip" title="Exame Não Iniciado"></i>
                                                <?php elseif(!empty($ap->inicio_exame) && empty($ap->fim_exame)): ?>
                                                    <i class="fa fa-circle color-laudo-exame" data-toggle="tooltip" title="Exame Iniciado"></i>
                                                <?php elseif(!empty($ap->fim_exame) && !empty($ap->inicio_exame)): ?>
                                                    <i class="fa fa-circle color-laudo-liberado" data-toggle="tooltip" title="Exame Finalizado"></i>
                                                <?php endif;?>
                                            </td>
                                            <td><?=($this->Laudo->existeLaudo($ap->id))?'<i class="fa fa-file-text" data-toggle="tooltip" title="Laudo Digitado"></i>':''?></td>
                                            <td><?=(!empty($ap->anotacao_laudo))?'<i class="fa fa-star" data-toggle="tooltip" title="Anotação"></i>':''?></td>
                                            <td><?=(!empty($imagem))? $this->Html->link('<i class="fa fa-camera"></i>', $this->Laudo->gerarUrlImage($imagem) , ['escape' => false, 'target', 'class' => 'text-success', 'toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Abrir Imagens']) : ""?></td>
                                            <td><?=$ap->atendimento_id?></td>
                                            <td><?=$ap->id?></td>
                                            <td><?=$ap->atendimento->data?></td>
                                            <!--<td><?=$ap->dt_entrega?></td>-->
                                            <td class="<?=$this->Configuracao->showPrevEntrega()?>"><?=$ap->dt_entrega ?></td>
                                            <td><?=$ap->has('situacao_laudo')? $ap->situacao_laudo->nome:'';?></td>
                                            <td><?=$ap->procedimento->grupo_procedimento->nome?></td>
                                            <td><?=$ap->atendimento->cliente->nome?></td>
                                            <td><?=$ap->procedimento->nome?></td>
                                            <td><?=$ap->atendimento->convenio->nome?></td>
                                            <td><?=$ap->medico_responsavei->nome?></td>
                                            <td>
                                                <div class="dropdown">
                                                    <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-prontuario">
                                                        <li><?= $this->Html->link('<i class="fa fa-pencil"></i> Dados Clínicos', ['action' => 'edit', $ap->id, '?'=>['first'=>1,'ajax'=>1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-medkit"></i> Digitar Laudo', ['action' => 'laudar', $ap->id], ['escape' => false, 'target' => '_blank']) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-print"></i> Abrir Laudo', ['action' => 'laudar', $ap->id], ['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-file-image-o"></i> Imagens', $this->Laudo->gerarUrlImage($imagem) , ['escape' => false, 'target']) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-file-image-o"></i> Imagens AR',$this->Laudo->gerarUrlImageAr($imagem),['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-picture-o"></i> Imprimir Imagens',[],['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link('<i class="fa fa-file-text-o"></i> Entregar Resultado', ['action' => 'entregarResultado', $ap->atendimento->cliente_id, '?'=>['first'=>1,'ajax'=>1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
echo $this->Html->scriptBlock(
    '
    window.onload = function () {
        $(".btn-count-laudos").click();
    };
    ',
    ['inline' => false, 'block' => 'scriptLast']
);
?>