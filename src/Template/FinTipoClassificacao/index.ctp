<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fin Tipo Classificacao</h2>
            <ol class="breadcrumb">
                <li>Fin Tipo Classificacao</li>
                <li class="active">
                    <strong>Listagem de Fin Tipo Classificacao</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinTipoClassificacao',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('descricao',['name'=>'FinTipoClassificacao__descricao']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('status',['name'=>'FinTipoClassificacao__status']); ?>
                                                                                    </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Fin Tipo Classificacao', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fin Tipo Classificacao','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fin Tipo Classificacao') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('descricao') ?></th>
                                                                                <th><?= $this->Paginator->sort('status') ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($finTipoClassificacao as $finTipoClassificacao): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($finTipoClassificacao->id) ?></td>
                                                                                <td><?= h($finTipoClassificacao->descricao) ?></td>
                                                                                <td><?= $this->Number->format($finTipoClassificacao->status) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'finTipoClassificacao','action' => 'view', $finTipoClassificacao->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'finTipoClassificacao','action' => 'edit', $finTipoClassificacao->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'finTipoClassificacao','action'=>'delete', $finTipoClassificacao->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
