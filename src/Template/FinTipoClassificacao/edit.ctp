<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fin Tipo Classificacao</h2>
            <ol class="breadcrumb">
                <li>Fin Tipo Classificacao</li>
                <li class="active">
                    <strong>
                                                Editar Fin Tipo Classificacao
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finTipoClassificacao form">
                            <?= $this->Form->create($finTipoClassificacao) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Fin Tipo Classificacao') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('status'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

