<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cliente Grupo Retornos</h2>
            <ol class="breadcrumb">
                <li>Cliente Grupo Retornos</li>
                <li class="active">
                    <strong>Litagem de Cliente Grupo Retornos</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Cliente Grupo Retornos', ['action' => 'add',$cliente], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Cliente Grupo Retornos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Cliente Grupo Retornos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('cliente_id') ?></th>
                                        <th><?= $this->Paginator->sort('grupo_id') ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified',['label'=>'Dt. modificação']) ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Quem cadastrou']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($clienteGrupoRetornos as $clienteGrupoRetorno): ?>
                                        <tr>
                                            <td><?= $clienteGrupoRetorno->has('cliente') ? $this->Html->link($clienteGrupoRetorno->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteGrupoRetorno->cliente->id]) : '' ?></td>
                                            <td><?= $clienteGrupoRetorno->has('grupo_retorno') ? $this->Html->link($clienteGrupoRetorno->grupo_retorno->nome, ['controller' => 'GrupoRetornos', 'action' => 'view', $clienteGrupoRetorno->grupo_retorno->id]) : '' ?></td>
                                            <td><?= h($clienteGrupoRetorno->created) ?></td>
                                            <td><?= h($clienteGrupoRetorno->modified) ?></td>
                                            <td><?= $clienteGrupoRetorno->has('user') ? $this->Html->link($clienteGrupoRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteGrupoRetorno->user->id]) : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteGrupoRetorno->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'ClienteGrupoRetornos\',' . $clienteGrupoRetorno->id . ',recarregar,' . $cliente . ')', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

