<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cliente Grupo Retornos</h2>
            <ol class="breadcrumb">
                <li>Cliente Grupo Retornos</li>
                <li class="active">
                    <strong> Cadastrar Cliente Grupo Retornos
                    </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clienteGrupoRetornos form">
                            <?= $this->Form->create($clienteGrupoRetorno) ?>
                            <fieldset>
                                <?php
                                if(!empty($cliente)){
                                    echo $this->Form->input('cliente_id', ['value' => $cliente,'type'=>'hidden']);
                                    echo "<div class='col-md-12'>";
                                    echo $this->Form->input('grupo_id', ['options' => $grupoRetornos,'empty'=>'Selecione']);
                                    echo "</div>";
                                }else{
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('cliente_id', ['options' => $clientes,'empty'=>'Selecione','label'=>'Paciente']);
                                    echo "</div>";
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('grupo_id', ['options' => $grupoRetornos,'empty'=>'Selecione']);
                                    echo "</div>";
                                }


                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

