

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Grupo Retornos</h2>
        <ol class="breadcrumb">
            <li>Cliente Grupo Retornos</li>
            <li class="active">
                <strong>Litagem de Cliente Grupo Retornos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="clienteGrupoRetornos">
    <h3><?= h($clienteGrupoRetorno->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Cliente') ?></th>
            <td><?= $clienteGrupoRetorno->has('cliente') ? $this->Html->link($clienteGrupoRetorno->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteGrupoRetorno->cliente->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Grupo Retorno') ?></th>
            <td><?= $clienteGrupoRetorno->has('grupo_retorno') ? $this->Html->link($clienteGrupoRetorno->grupo_retorno->nome, ['controller' => 'GrupoRetornos', 'action' => 'view', $clienteGrupoRetorno->grupo_retorno->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $clienteGrupoRetorno->has('situacao_cadastro') ? $this->Html->link($clienteGrupoRetorno->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $clienteGrupoRetorno->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $clienteGrupoRetorno->has('user') ? $this->Html->link($clienteGrupoRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteGrupoRetorno->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($clienteGrupoRetorno->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($clienteGrupoRetorno->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($clienteGrupoRetorno->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

