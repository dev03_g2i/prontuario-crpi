
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Recalcular</h2>
        <ol class="breadcrumb">
            <li>Fatura Recalcular</li>
            <li class="active">
                <strong>Litagem de Fatura Recalcular</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Recalcular</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Convenio') ?></th>
                                                                <td><?= $faturaRecalcular->has('convenio') ? $this->Html->link($faturaRecalcular->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $faturaRecalcular->convenio->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Periodo') ?></th>
                                <td><?= h($faturaRecalcular->periodo) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturaRecalcular->has('situacao_cadastro') ? $this->Html->link($faturaRecalcular->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaRecalcular->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaRecalcular->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Created') ?></th>
                                <td><?= $this->Number->format($faturaRecalcular->user_created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Update') ?></th>
                                <td><?= $this->Number->format($faturaRecalcular->user_update) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaRecalcular->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaRecalcular->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


