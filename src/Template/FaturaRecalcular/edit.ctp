<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Recalcular</h2>
            <ol class="breadcrumb">
                <li>Fatura Recalcular</li>
                <li class="active">
                    <strong>
                                                Editar Fatura Recalcular
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaRecalcular form">
                            <?= $this->Form->create($faturaRecalcular) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Fatura Recalcular') ?></legend>
                                                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('convenio_id', ['data'=>'select','controller'=>'convenios','action'=>'fill','data-value'=>$faturaRecalcular->convenio_id]); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('periodo'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_created'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_update'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

