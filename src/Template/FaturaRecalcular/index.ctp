<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Recalcular</h2>
            <ol class="breadcrumb">
                <li>Fatura Recalcular</li>
                <li class="active">
                    <strong>Listagem de Fatura Recalcular</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaRecalcular',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('convenio_id', ['name'=>'FaturaRecalcular__convenio_id','data'=>'select','controller'=>'convenios','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('grupo_procedimento_id', ['name'=>'FaturaRecalcular__grupo_procedimento_id','data'=>'select','controller'=>'convenios','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('periodo',['name'=>'FaturaRecalcular__periodo']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('user_created',['name'=>'FaturaRecalcular__user_created']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('user_update',['name'=>'FaturaRecalcular__user_update']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>

                <!--<div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Fatura Recalcular', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fatura Recalcular','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>-->

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fatura Recalcular') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                        <th><?= $this->Paginator->sort('grupo_procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('periodo') ?></th>
                                        <th><?= $this->Paginator->sort('user_created', 'Cadastrado Por') ?></th>
                                        <th><?= $this->Paginator->sort('user_update', 'Alterado Por') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaRecalcular as $faturaRecalcular): ?>
                                        <tr>
                                            <td><?= $this->Number->format($faturaRecalcular->id) ?></td>
                                            <td><?= $faturaRecalcular->has('convenio') ? $this->Html->link($faturaRecalcular->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $faturaRecalcular->convenio->id]) : '' ?></td>
                                            <td><?= $faturaRecalcular->has('grupo_procedimento') ? $this->Html->link($faturaRecalcular->grupo_procedimento->nome, ['controller' => 'GrupoProcedimentos', 'action' => 'view', $faturaRecalcular->grupo_procedimento->id]) : '' ?></td>
                                            <td><?= h($faturaRecalcular->periodo) ?></td>
                                            <td><?= $faturaRecalcular->has('user_reg') ? $faturaRecalcular->user_reg->nome : '' ?></td>
                                            <td><?= $faturaRecalcular->has('user_alt') ? $faturaRecalcular->user_alt->nome : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaRecalcular','action' => 'view', $faturaRecalcular->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <!--<?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaRecalcular','action' => 'edit', $faturaRecalcular->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturaRecalcular','action'=>'delete', $faturaRecalcular->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>-->
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
