

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Estado Civil</h2>
        <ol class="breadcrumb">
            <li>Estado Civil</li>
            <li class="active">
                <strong>Listagem de Estado Civil</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="estadoCivis">
    <h3><?= h($estadoCivi->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($estadoCivi->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situação Cadastro') ?></th>
            <td><?= $estadoCivi->has('situacao_cadastro') ? $this->Html->link($estadoCivi->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $estadoCivi->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Usuário') ?></th>
            <td><?= $estadoCivi->has('user') ? $this->Html->link($estadoCivi->user->nome, ['controller' => 'Users', 'action' => 'view', $estadoCivi->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Codigo') ?></th>
            <td><?= $this->Number->format($estadoCivi->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Criado') ?></th>
            <td><?= h($estadoCivi->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado') ?></th>
            <td><?= h($estadoCivi->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

