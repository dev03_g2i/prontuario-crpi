<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estq Saida</h2>
            <ol class="breadcrumb">
                <li>Estq Saida</li>
                <li class="active">
                    <strong>
                                                Editar Estq Saida
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="estqSaida form">
                            <?= $this->Form->create($estqSaida) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Estq Saida') ?></legend>
                                                                                                    <div class='col-md-6'>
                                                                                    <?=$this->Form->input('data', ['type' => 'text', 'class' => 'datepicker','value'=>$this->Time->format($estqSaida->data,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                                                                        </div>
                                                                                 <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('tipo_movimento_id', ['data'=>'select','controller'=>'estqTipoMovimento','action'=>'fill','data-value'=>$estqSaida->tipo_movimento_id]); ?>
                                                                                            </div>
                                                                                        <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('atendimento_procedimento_id', ['data'=>'select','controller'=>'atendimentoProcedimentos','action'=>'fill','data-value'=>$estqSaida->atendimento_procedimento_id]); ?>
                                                                                            </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('exportado_fatura'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_insert'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_update'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

