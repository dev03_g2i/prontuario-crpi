
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Estq Saida</h2>
        <ol class="breadcrumb">
            <li>Estq Saida</li>
            <li class="active">
                <strong>Litagem de Estq Saida</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Estq Saida</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Estq Tipo Movimento') ?></th>
                                                                <td><?= $estqSaida->has('estq_tipo_movimento') ? $this->Html->link($estqSaida->estq_tipo_movimento->id, ['controller' => 'EstqTipoMovimento', 'action' => 'view', $estqSaida->estq_tipo_movimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Atendimento Procedimento') ?></th>
                                                                <td><?= $estqSaida->has('atendimento_procedimento') ? $this->Html->link($estqSaida->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $estqSaida->atendimento_procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $estqSaida->has('situacao_cadastro') ? $this->Html->link($estqSaida->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $estqSaida->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($estqSaida->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Exportado Fatura') ?></th>
                                <td><?= $this->Number->format($estqSaida->exportado_fatura) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Insert') ?></th>
                                <td><?= $this->Number->format($estqSaida->user_insert) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Update') ?></th>
                                <td><?= $this->Number->format($estqSaida->user_update) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($estqSaida->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($estqSaida->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($estqSaida->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


