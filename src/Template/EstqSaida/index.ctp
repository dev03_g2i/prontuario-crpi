<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h5><strong>Paciente: </strong><?=$dadosPaciente->atendimento->cliente->nome?></h5>
            <h5><strong>Convênio: </strong><?=$dadosPaciente->atendimento->convenio->nome?></h5>
        </div>
        <div class="col-lg-3 text-right">
            <h5><strong>Atendimento: </strong><?=$dadosPaciente->atendimento_id?></h5>
            <h5><strong>Data: </strong><?=$dadosPaciente->atendimento->data?></h5>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaMatmed',['type'=>'get', 'id' => 'frm-fatura-matmed']) ?>
                            <div class="col-md-4">
                                <?php echo $this->Form->input('atendimento_procedimento_id', ['id' => 'atend-proc-id', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'options' => $atendProcedimentos, 'class' => 'select2', 'required' => true]);?>
                            </div>
                            <div class="col-md-4">
                                <?php echo $this->Form->input('fatura_kit_id', ['label' => 'Kit/Pacote', 'empty' => 'Selecione', 'options' => $estq_kit, 'class' => 'select2', 'required' => true, 'append' => [$this->Form->button($this->Html->icon("plus"), ['type' => 'button', 'id' => 'btnAddKit', 'toggle'=>'tooltip','data-placement'=>'top','title'=>'Adicionar Kit'])]]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php echo $this->Form->input('data', ['label' => 'Dt.Base', 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($dadosPaciente->atendimento->data, 'dd/MM/YYYY'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                            </div>
                            <?php echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $dadosPaciente->atendimento_id])?>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default btnFilterTotais','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                <?= $this->Html->link('<i class="fa fa-plus"></i>',['controller' => 'EstqSaida', 'action' => 'addArtigo', '?' =>  ['atendimento_id' => $dadosPaciente->atendimento_id]], ['class' => 'btn btn-primary','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Artigos', 'id' => 'openModalArtigo', 'data-target' => '#modal_lg', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label>Total </label><br>
                                        <span class="total">R$ 0,00</span>
                                    </div>
                                    <div class="col-md-3">
                                        <label>Vl.Recebido </label><br>
                                        <span class="valor_fatura">R$ 0,00</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-6">
                            <h5><?= __('Estoque Saida') ?></h5>
                        </div>
                        <!--<div class="col-md-6 text-right">
                            <?=$this->Html->link('Receber Todos', ['action' => 'receberTodos', '?' => ['atendimento_id' => $dadosPaciente->atendimento->id, 'first' => 1]], ['class' => 'btn btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal_lg'])?>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('data') ?></th>
                                        <th><?= $this->Paginator->sort('artigo_id', 'Código') ?></th>
                                        <th><?= $this->Paginator->sort('EstqArtigo.nome', 'Nome artigo') ?></th>
                                        <th><?= $this->Paginator->sort('codigo_convenio', 'Convênio') ?></th>
                                        <th><?= $this->Paginator->sort('codigo_tiss', 'Código Tiss') ?></th>
                                        <th><?= $this->Paginator->sort('codigo_tuss', 'Código Tuss') ?></th>
                                        <th><?= $this->Paginator->sort('quantidade') ?></th>
                                        <th><?= $this->Paginator->sort('vl_venda', 'Vl.Venda') ?></th>
                                        <th><?= $this->Paginator->sort('total') ?></th>
                                        <th class="valor-prod-caixa"><?= $this->Paginator->sort('qt_recebida') ?></th>
                                        <th class="valor-prod-caixa"><?= $this->Paginator->sort('vl_recebido') ?></th>
                                        <th><?= $this->Paginator->sort('dt_recebimento') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $options = ['places' => 2];
                                    foreach ($estqSaida as $es): ?>
                                        <tr class="delete<?=$es->id?>">
                                            <td><?=$es->data?></td>
                                            <td><?=$es->has('estq_artigo') ? $es->estq_artigo->codigo_livre : '' ?></td>
                                            <td><?=$es->has('estq_artigo') ? $es->estq_artigo->nome : '' ?></td>
                                            <td><?=$es->codigo_convenio ?></td>
                                            <td><?=$es->codigo_tiss ?></td>
                                            <td><?=$es->codigo_tuss ?></td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="FaturaMatmed" data-param="text" data-title="quantidade"
                                                   data-pk="<?= $es->id ?>"
                                                   data-value="<?= $es->quantidade ?>"
                                                   listen="f"><?= $es->quantidade ?>
                                                </a>
                                            </td>
                                            <td><?=$this->Number->format($es->vl_venda, $options)?></td>
                                            <td><?=$this->Number->format($es->total, $options) ?></td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="FaturaMatmed" data-param="text" data-title="qtd_recebida"
                                                   data-pk="<?= $es->id ?>"
                                                   data-value="<?= $es->qtd_recebida ?>"
                                                   listen="f"><?= $es->qtd_recebida ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="FaturaMatmed" data-param="decimal"
                                                   data-title="vl_recebido" data-pk="<?= $es->id ?>"
                                                   data-value="<?= $this->Number->format($es->vl_recebido, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($es->vl_recebido); ?>
                                                </a>
                                            </td>
                                            <td><?=$this->Time->format($es->dt_recebimento, 'dd/MM/YYYY')?></td>
                                            <td class="actions">
                                                <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaMatmed','action' => 'view', $es->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaMatmed','action' => 'edit', $es->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>-->
                                                <?= $this->Html->link($this->Html->icon('remove'),  'javascript:void(0)',['onclick'=>'DeletarModal(\'EstqSaida\','.$es->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>