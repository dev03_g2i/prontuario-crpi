<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tipo Pagamento</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finTipoPagamento form">
                            <?= $this->Form->create($finTipoPagamento) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Tipo Pagamento') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('descricao'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('usado', ['type' => 'select', 'options' => ['Contas pagar' => 'Contas à pagar', 'Contas receber' => 'Contas à receber', 'Ambas' => 'Ambas']]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('taxa', ['mask' => 'money', 'value' => number_format($finTipoPagamento->taxa, 2)]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('num_parcelas', ['label' => 'Número de parcelas', 'type' => 'number']); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>