<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tipo Pagamento</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinTipoPagamento',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('descricao',['label' => 'Nome']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Novo Tipo de Pagamento', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fin Tipo Pagamento','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Tipo Pagamentos') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('descricao') ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($finTipoPagamento as $finTipoPagamento): ?>
                                        <tr>
                                            <td>
                                                <?= $this->Number->format($finTipoPagamento->id) ?>
                                            </td>
                                            <td>
                                                <?= h($finTipoPagamento->descricao) ?>
                                            </td>
                                            <td class="actions" style="white-space:nowrap">
                                                <div class="no-padding dropdown">
                                                    <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                    <ul class="dropdown-menu dropdown-menu-left">
                                                        <li>
                                                            <?= $this->Html->link('Visualizar/Editar', ['action' => 'edit', $finTipoPagamento->id], ['target' => '_blank']) ?>
                                                        </li>
                                                        <li>
                                                            <?= $this->Html->link('Excluir', '#Javascript:void(0)', ['onclick' => 'Deletar(\'FinTipoPagamento\',' . $finTipoPagamento->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false,  'listen' => 'f']) ?>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

