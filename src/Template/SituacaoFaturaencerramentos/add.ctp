<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Faturaencerramentos</h2>
        <ol class="breadcrumb">
            <li>Situacao Faturaencerramentos</li>
            <li class="active">
                <strong>
                    Cadastrar Situacao Faturaencerramentos
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="situacaoFaturaencerramentos form">
                        <?= $this->Form->create($situacaoFaturaencerramento) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Situacao Faturaencerramento') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('nome');
                                echo "</div>";
                            ?>
                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

