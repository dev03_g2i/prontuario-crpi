<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Faturaencerramentos</h2>
        <ol class="breadcrumb">
            <li>Situacao Faturaencerramentos</li>
            <li class="active">
                <strong>Litagem de Situacao Faturaencerramentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('SituacaoFaturaencerramento',['type'=>'get']) ?>
                        <?php
                                                    echo "<div class='col-md-4'>";
                                echo $this->Form->input('nome',['name'=>'SituacaoFaturaencerramentos__nome']);
                                echo "</div>";
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Situacao Faturaencerramentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Situacao Faturaencerramentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Situacao Faturaencerramentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($situacaoFaturaencerramentos as $situacaoFaturaencerramento): ?>
            <tr>
                <td><?= $this->Number->format($situacaoFaturaencerramento->id) ?></td>
                <td><?= h($situacaoFaturaencerramento->nome) ?></td>
                <td><?= $situacaoFaturaencerramento->has('situacao_cadastro') ? $this->Html->link($situacaoFaturaencerramento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $situacaoFaturaencerramento->situacao_cadastro->id]) : '' ?></td>
                <td><?= $situacaoFaturaencerramento->has('user') ? $this->Html->link($situacaoFaturaencerramento->user->nome, ['controller' => 'Users', 'action' => 'view', $situacaoFaturaencerramento->user->id]) : '' ?></td>
                <td><?= h($situacaoFaturaencerramento->created) ?></td>
                <td><?= h($situacaoFaturaencerramento->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'situacaoFaturaencerramentos','action' => 'view', $situacaoFaturaencerramento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'situacaoFaturaencerramentos','action' => 'edit', $situacaoFaturaencerramento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'situacaoFaturaencerramentos','action'=>'delete', $situacaoFaturaencerramento->id],['onclick'=>'Deletar(\'situacaoFaturaencerramentos\','.$situacaoFaturaencerramento->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
