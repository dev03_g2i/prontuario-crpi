<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Agenda Procedimentos</li>
            <li class="active">
                <strong>                    Editar Agenda Procedimentos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendaProcedimentos form">
                        <?= $this->Form->create($agendaProcedimento) ?>
                        <fieldset>
                            <legend><?= __('Editar Agenda Procedimento') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('agenda_id', ['data'=>'select','controller'=>'agendas','action'=>'fill','data-value'=>$agendaProcedimento->agenda_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('agenda_procedimento_id');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

