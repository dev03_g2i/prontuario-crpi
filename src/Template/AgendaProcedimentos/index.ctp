<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Agenda Procedimentos</li>
            <li class="active">
                <strong>Litagem de Agenda Procedimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('AgendaProcedimento',['type'=>'get']) ?>
                        <?php
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('agenda_id', ['name'=>'AgendaProcedimentos__agenda_id','data'=>'select','controller'=>'agendas','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('agenda_procedimento_id',['name'=>'AgendaProcedimentos__agenda_procedimento_id']);
                                echo "</div>";
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Agenda Procedimentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Agenda Procedimentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Agenda Procedimentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('agenda_id') ?></th>
                <th><?= $this->Paginator->sort('agenda_procedimento_id') ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($agendaProcedimentos as $agendaProcedimento): ?>
            <tr>
                <td><?= $this->Number->format($agendaProcedimento->id) ?></td>
                <td><?= $agendaProcedimento->has('agenda') ? $this->Html->link($agendaProcedimento->agenda->id, ['controller' => 'Agendas', 'action' => 'view', $agendaProcedimento->agenda->id]) : '' ?></td>
                <td><?= $this->Number->format($agendaProcedimento->agenda_procedimento_id) ?></td>
                <td><?= $agendaProcedimento->has('situacao_cadastro') ? $this->Html->link($agendaProcedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $agendaProcedimento->situacao_cadastro->id]) : '' ?></td>
                <td><?= $agendaProcedimento->has('user') ? $this->Html->link($agendaProcedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $agendaProcedimento->user->id]) : '' ?></td>
                <td><?= h($agendaProcedimento->created) ?></td>
                <td><?= h($agendaProcedimento->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'agendaProcedimentos','action' => 'view', $agendaProcedimento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'agendaProcedimentos','action' => 'edit', $agendaProcedimento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'agendaProcedimentos','action'=>'delete', $agendaProcedimento->id],['onclick'=>'Deletar(\'agendaProcedimentos\','.$agendaProcedimento->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
