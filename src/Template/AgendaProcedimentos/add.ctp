<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Agenda Procedimentos</li>
            <li class="active">
                <strong>                    Cadastrar Agenda Procedimentos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendaProcedimentos form">
                        <?= $this->Form->create($agendaProcedimento) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Agenda Procedimento') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('agenda_id', ['type' => 'text', 'value' => 11139]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('agenda_cadprocedimento_id', ['type' => 'text','value' => 1]);
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

