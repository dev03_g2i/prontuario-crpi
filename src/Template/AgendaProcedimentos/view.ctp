
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Agenda Procedimentos</li>
            <li class="active">
                <strong>Litagem de Agenda Procedimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Agenda Procedimentos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Agenda') ?></th>
                                                                <td><?= $agendaProcedimento->has('agenda') ? $this->Html->link($agendaProcedimento->agenda->id, ['controller' => 'Agendas', 'action' => 'view', $agendaProcedimento->agenda->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $agendaProcedimento->has('situacao_cadastro') ? $this->Html->link($agendaProcedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $agendaProcedimento->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $agendaProcedimento->has('user') ? $this->Html->link($agendaProcedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $agendaProcedimento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($agendaProcedimento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Agenda Procedimento Id') ?></th>
                                <td><?= $this->Number->format($agendaProcedimento->agenda_procedimento_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($agendaProcedimento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($agendaProcedimento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


