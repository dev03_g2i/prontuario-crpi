<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Procedimentos</h2>
            <ol class="breadcrumb">
                <li>Fichas</li>
                <li class="active">
                    <strong>Litagem de Procedimento/fichas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Vincular Ficha', ['action' => 'add','?'=>['procedimento_id'=>$procedimento_id]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Vincular Ficha', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fichas') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('gasto_id') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Ult. Atualização']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($procedimentoGastos as $procedimentoGasto): ?>
                                        <tr>
                                            <td><?= $this->Number->format($procedimentoGasto->id) ?></td>
                                            <td><?= $procedimentoGasto->has('procedimento') ? $procedimentoGasto->procedimento->nome : '' ?></td>
                                            <td><?= $procedimentoGasto->has('gasto') ? $this->Html->link($procedimentoGasto->gasto->nome, ['controller' => 'Gastos', 'action' => 'view', $procedimentoGasto->gasto->id]) : '' ?></td>
                                            <td><?= h($procedimentoGasto->created) ?></td>
                                            <td><?= h($procedimentoGasto->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $procedimentoGasto->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'procedimentoGastos\',' . $procedimentoGasto->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
