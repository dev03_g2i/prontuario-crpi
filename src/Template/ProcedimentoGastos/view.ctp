
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimento Gastos</h2>
        <ol class="breadcrumb">
            <li>Procedimento Gastos</li>
            <li class="active">
                <strong>Litagem de Procedimento Gastos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Procedimento Gastos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Procedimento') ?></th>
                                                                <td><?= $procedimentoGasto->has('procedimento') ? $this->Html->link($procedimentoGasto->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $procedimentoGasto->procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Gasto') ?></th>
                                                                <td><?= $procedimentoGasto->has('gasto') ? $this->Html->link($procedimentoGasto->gasto->nome, ['controller' => 'Gastos', 'action' => 'view', $procedimentoGasto->gasto->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $procedimentoGasto->has('user') ? $this->Html->link($procedimentoGasto->user->nome, ['controller' => 'Users', 'action' => 'view', $procedimentoGasto->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $procedimentoGasto->has('situacao_cadastro') ? $this->Html->link($procedimentoGasto->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $procedimentoGasto->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($procedimentoGasto->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($procedimentoGasto->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($procedimentoGasto->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


