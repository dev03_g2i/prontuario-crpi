<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimento Gastos</h2>
        <ol class="breadcrumb">
            <li>Procedimento Gastos</li>
            <li class="active">
                <strong>                    Editar Procedimento Gastos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="procedimentoGastos form">
                        <?= $this->Form->create($procedimentoGasto) ?>
                        <fieldset>
                            <legend><?= __('Editar Procedimento Gasto') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('procedimento_id', ['data'=>'select','controller'=>'procedimentos','action'=>'fill','data-value'=>$procedimentoGasto->procedimento_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('gasto_id', ['data'=>'select','controller'=>'gastos','action'=>'fill','data-value'=>$procedimentoGasto->gasto_id]);
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

