<div class="this-place">
    <?= $this->Form->create($mastologia, ['id' => 'frm-modelo-formulario']) ?>
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Mastologias</h2>
            <ol class="breadcrumb">
                <li>Mastologias</li>
                <li class="active">
                    <strong> Cadastrar Mastologias
                    </strong>
                </li>
                <li>Paciente:<strong> <?= 'Nome do Paciente' ?></strong></li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="mastologias form">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('HDA') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php
                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Nódulo em Mama:</b>
                                            <input type='radio' name='nodulo_mama' value=1> Sim
                                            <input type='radio' name='nodulo_mama' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Nódulo em Mama Dir.</label>";
                                        echo $this->Form->input('nodulo_mama_direita', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Nódulo em Mama Esq.</label>";
                                        echo $this->Form->input('nodulo_mama_esquerda', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-2'>";
                                        echo $this->Form->input('tempo_nodulo', ['label' => 'Tempo']);
                                        echo "</div>";
                                        echo "<div class='col-md-2'>";
                                        echo $this->Form->input('crescimento', ['label' => 'Crescimento']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Alt. Exame imagem:</b>
                                            <input type='radio' name='alteracao_exame_imagem' value=1> Sim
                                            <input type='radio' name='alteracao_exame_imagem' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('exames');
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Mastalgia:</b>
                                            <input type='radio' name='mastalgia' value=1> Sim
                                            <input type='radio' name='mastalgia' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('tempo_mastalgia', ['label' => 'Tempo']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Secreção Mamilar:</b>
                                            <input type='radio' name='secrecao_mamilar' value=1> Sim
                                            <input type='radio' name='secrecao_mamilar' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('carc_mamilar', ['label' => 'Característica']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Exame de Rotina:</b>
                                            <input type='radio' name='exame_rotina' value=1> Sim
                                            <input type='radio' name='exame_rotina' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('outras_queixas');
                                        echo "</div>";
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Antecedentes Pessoais') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php
                                        echo "<div class='col-md-2'>";
                                        echo $this->Form->input('idade');
                                        echo "</div>";
                                        echo "<div class='col-md-1'>";
                                        echo $this->Form->input('g');
                                        echo "</div>";
                                        echo "<div class='col-md-1'>";
                                        echo $this->Form->input('p');
                                        echo "</div>";
                                        echo "<div class='col-md-1'>";
                                        echo $this->Form->input('a');
                                        echo "</div>";
                                        echo "<div class='col-md-1'>";
                                        echo $this->Form->input('c');
                                        echo "</div>";
                                        echo "<div class='col-md-3'>";
                                        echo $this->Form->input('dum', ['type' => 'text', ['label' => 'DUM'], 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                        echo "</div>";
                                        echo "<div class='col-md-3'>";
                                        echo $this->Form->input('menarca');
                                        echo "</div>";
                                        echo "<div class='col-md-8'>";
                                        echo $this->Form->input('patologias');
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('mac', ['label' => 'MAC']);
                                        echo "</div>";
                                        echo "<div class='col-md-8'>";
                                        echo $this->Form->input('medicacao', ['label' => 'Medicação']);
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('alergias');
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('idade_parto', ['label' => 'Idade 1º Parto']);
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo "<label class='no-padding radio-inline'><b>Amamentação:</b>
                                            <input type='radio' name='amamentacao' value=1> Sim
                                            <input type='radio' name='amamentacao' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('tempo_amamentacao', ['label' => 'Tempo']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Mastite:</b>
                                            <input type='radio' name='mastite' value=1> Sim
                                            <input type='radio' name='mastite' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('localizacao', ['label' => 'Localização']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Biópsia em Mama Prévia:</b>
                                            <input type='radio' name='biopsia_mama' value=1> Sim
                                            <input type='radio' name='biopsia_mama' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('ap', ['label' => 'AP']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Punção em Mama:</b>
                                            <input type='radio' name='puncao_mama' value=1> Sim
                                            <input type='radio' name='puncao_mama' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Punção em Mama Dir.</label>";
                                        echo $this->Form->input('puncao_mama_direita', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Punção em Mama Esq.</label>";
                                        echo $this->Form->input('puncao_mama_esquerda', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-2'>";
                                        echo $this->Form->input('punca_mama_obs', ['label' => 'OBS']);
                                        echo "</div>";
                                        echo "<div class='col-md-3'>";
                                        echo $this->Form->input('cirurgias');
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Tabagismo:</b>
                                            <input type='radio' name='tabagismo' value=1> Sim
                                            <input type='radio' name='tabagismo' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('tempo_tabagismo', ['label' => 'Tempo de Uso']);
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('cigarros_dia', ['label' => 'Nº Cigarros/Dia']);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('ultimo_preventivo', ['type' => 'text', 'label' => 'Último Preventivo', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('ultima_mamografia', ['type' => 'text','label' => 'Última Mamografia', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                        echo "</div>";
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Antecedentes Familiares') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('cancer_mama_ovario', ['label' => 'Câncer de Mama ou Ovário']);
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('outros');
                                        echo "</div>";
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Exame Físico') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('peso', ['label' => 'Peso(Kg)']);
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('altura', ['label' => 'Altura(cm)']);
                                        echo "</div>";
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('pa', ['label' => 'PA(mmHg)']);
                                        echo "</div>";
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Mamas Inspeção') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php
                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Abulamento:</b>
                                            <input type='radio' name='abulamento' value=1> Sim
                                            <input type='radio' name='abulamento' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Abulamento Direita</label>";
                                        echo $this->Form->input('abulamento_direita', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Abulamento Esquerda</label>";
                                        echo $this->Form->input('abulamento_esquerda', ['label' => false]);
                                        echo "</div>";

                                        echo "<div class='clearfix'></div>";

                                        echo "<div class='col-md-3'>";
                                        echo "<label class='no-padding radio-inline'><b>Retração:</b>
                                            <input type='radio' name='retracao' value=1> Sim
                                            <input type='radio' name='retracao' value=0> Não
                                        </label>";
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Retração Direita</label>";
                                        echo $this->Form->input('retracao_direita', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-2 alinhaCheckbox'>";
                                        echo "<label class='control-label'>Retração Esquerda</label>";
                                        echo $this->Form->input('retracao_esquerda', ['label' => false]);
                                        echo "</div>";
                                        echo "<div class='col-md-5'>";
                                        echo $this->Form->input('cam', ['label' => 'CAM']);
                                        echo "</div>";
                                        echo "<div class='col-md-12'>";
                                        echo $this->Form->input('papacao', ['label' => 'Palpação']);
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('expressao_papilar', ['label' => 'Expressão Papilar']);
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('ganglios', ['label' => 'Gânglios']);
                                        echo "</div>";
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Exames Complementares') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <?php
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('mamografia');
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('ultrassonagrafia');
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('citologia');
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('histologia');
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('hd', ['label' => 'HD']);
                                        echo "</div>";
                                        echo "<div class='col-md-6'>";
                                        echo $this->Form->input('conduta');
                                        echo "</div>";

                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>

