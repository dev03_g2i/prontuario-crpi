
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Mastologias</h2>
        <ol class="breadcrumb">
            <li>Mastologias</li>
            <li class="active">
                <strong>Litagem de Mastologias</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Mastologias</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Tempo Nodulo') ?></th>
                                <td><?= h($mastologia->tempo_nodulo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Crescimento') ?></th>
                                <td><?= h($mastologia->crescimento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Exames') ?></th>
                                <td><?= h($mastologia->exames) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tempo Mastalgia') ?></th>
                                <td><?= h($mastologia->tempo_mastalgia) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Carc Mamilar') ?></th>
                                <td><?= h($mastologia->carc_mamilar) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Outras Queixas') ?></th>
                                <td><?= h($mastologia->outras_queixas) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('G') ?></th>
                                <td><?= h($mastologia->g) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('P') ?></th>
                                <td><?= h($mastologia->p) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('A') ?></th>
                                <td><?= h($mastologia->a) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('C') ?></th>
                                <td><?= h($mastologia->c) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Menarca') ?></th>
                                <td><?= h($mastologia->menarca) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Patologias') ?></th>
                                <td><?= h($mastologia->patologias) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mac') ?></th>
                                <td><?= h($mastologia->mac) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Medicacao') ?></th>
                                <td><?= h($mastologia->medicacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Alergias') ?></th>
                                <td><?= h($mastologia->alergias) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tempo Amamentacao') ?></th>
                                <td><?= h($mastologia->tempo_amamentacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Localizacao') ?></th>
                                <td><?= h($mastologia->localizacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Ap') ?></th>
                                <td><?= h($mastologia->ap) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Punca Mama Obs') ?></th>
                                <td><?= h($mastologia->punca_mama_obs) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cirurgias') ?></th>
                                <td><?= h($mastologia->cirurgias) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tempo Tabagismo') ?></th>
                                <td><?= h($mastologia->tempo_tabagismo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cigarros Dia') ?></th>
                                <td><?= h($mastologia->cigarros_dia) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cancer Mama Ovario') ?></th>
                                <td><?= h($mastologia->cancer_mama_ovario) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Outros') ?></th>
                                <td><?= h($mastologia->outros) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Peso') ?></th>
                                <td><?= h($mastologia->peso) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Altura') ?></th>
                                <td><?= h($mastologia->altura) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Pa') ?></th>
                                <td><?= h($mastologia->pa) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cam') ?></th>
                                <td><?= h($mastologia->cam) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Papacao') ?></th>
                                <td><?= h($mastologia->papacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Expressao Papilar') ?></th>
                                <td><?= h($mastologia->expressao_papilar) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Ganglios') ?></th>
                                <td><?= h($mastologia->ganglios) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $mastologia->has('situacao_cadastro') ? $this->Html->link($mastologia->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $mastologia->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $mastologia->has('user') ? $this->Html->link($mastologia->user->nome, ['controller' => 'Users', 'action' => 'view', $mastologia->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cliente Historico') ?></th>
                                <td><?= $mastologia->has('cliente_historico') ? $this->Html->link($mastologia->cliente_historico->id, ['controller' => 'ClienteHistoricos', 'action' => 'view', $mastologia->cliente_historico->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($mastologia->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cliente Historico Id') ?></th>
                                <td><?= $this->Number->format($mastologia->cliente_historico_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Idade') ?></th>
                                <td><?= $this->Number->format($mastologia->idade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Idade Parto') ?></th>
                                <td><?= $this->Number->format($mastologia->idade_parto) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dum') ?></th>
                                                                <td><?= h($mastologia->dum) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Ultimo Preventivo') ?></th>
                                                                <td><?= h($mastologia->ultimo_preventivo) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Ultima Mamografia') ?></th>
                                                                <td><?= h($mastologia->ultima_mamografia) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($mastologia->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($mastologia->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nodulo Mama') ?>6</th>
                                <td><?= $mastologia->nodulo_mama ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nodulo Mama Direita') ?>6</th>
                                <td><?= $mastologia->nodulo_mama_direita ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nodulo Mama Esquerda') ?>6</th>
                                <td><?= $mastologia->nodulo_mama_esquerda ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Alteracao Exame Imagem') ?>6</th>
                                <td><?= $mastologia->alteracao_exame_imagem ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mastalgia') ?>6</th>
                                <td><?= $mastologia->mastalgia ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Secrecao Mamilar') ?>6</th>
                                <td><?= $mastologia->secrecao_mamilar ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Exame Rotina') ?>6</th>
                                <td><?= $mastologia->exame_rotina ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Amamentacao') ?>6</th>
                                <td><?= $mastologia->amamentacao ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mastite') ?>6</th>
                                <td><?= $mastologia->mastite ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Biopsia Mama') ?>6</th>
                                <td><?= $mastologia->biopsia_mama ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Puncao Mama') ?>6</th>
                                <td><?= $mastologia->puncao_mama ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Puncao Mama Direita') ?>6</th>
                                <td><?= $mastologia->puncao_mama_direita ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Puncao Mama Esquerda') ?>6</th>
                                <td><?= $mastologia->puncao_mama_esquerda ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tabagismo') ?>6</th>
                                <td><?= $mastologia->tabagismo ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Abulamento') ?>6</th>
                                <td><?= $mastologia->abulamento ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Abulamento Direita') ?>6</th>
                                <td><?= $mastologia->abulamento_direita ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Abulamento Esquerda') ?>6</th>
                                <td><?= $mastologia->abulamento_esquerda ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Retracao') ?>6</th>
                                <td><?= $mastologia->retracao ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Retracao Direita') ?>6</th>
                                <td><?= $mastologia->retracao_direita ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Retracao Esquerda') ?>6</th>
                                <td><?= $mastologia->retracao_esquerda ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Mamografia') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($mastologia->mamografia)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Ultrassonagrafia') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($mastologia->ultrassonagrafia)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Citologia') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($mastologia->citologia)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Histologia') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($mastologia->histologia)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Hd') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($mastologia->hd)); ?>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Conduta') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($mastologia->conduta)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


