<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Mastologias</h2>
        <ol class="breadcrumb">
            <li>Mastologias</li>
            <li class="active">
                <strong>Litagem de Mastologias</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('Mastologia',['type'=>'get']) ?>
                        <?php
                                                    echo "<div class='col-md-4'>";
                                echo $this->Form->input('cliente_historico_id',['name'=>'Mastologias__cliente_historico_id']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('nodulo_mama',['name'=>'Mastologias__nodulo_mama']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('nodulo_mama_direita',['name'=>'Mastologias__nodulo_mama_direita']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('nodulo_mama_esquerda',['name'=>'Mastologias__nodulo_mama_esquerda']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('tempo_nodulo',['name'=>'Mastologias__tempo_nodulo']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('crescimento',['name'=>'Mastologias__crescimento']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('alteracao_exame_imagem',['name'=>'Mastologias__alteracao_exame_imagem']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('exames',['name'=>'Mastologias__exames']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('mastalgia',['name'=>'Mastologias__mastalgia']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('tempo_mastalgia',['name'=>'Mastologias__tempo_mastalgia']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('secrecao_mamilar',['name'=>'Mastologias__secrecao_mamilar']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('carc_mamilar',['name'=>'Mastologias__carc_mamilar']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('exame_rotina',['name'=>'Mastologias__exame_rotina']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('outras_queixas',['name'=>'Mastologias__outras_queixas']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('idade',['name'=>'Mastologias__idade']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('g',['name'=>'Mastologias__g']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('p',['name'=>'Mastologias__p']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('a',['name'=>'Mastologias__a']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('c',['name'=>'Mastologias__c']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                    echo $this->Form->input('dum', ['name'=>'Mastologias__dum','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('menarca',['name'=>'Mastologias__menarca']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('patologias',['name'=>'Mastologias__patologias']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('mac',['name'=>'Mastologias__mac']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('medicacao',['name'=>'Mastologias__medicacao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('alergias',['name'=>'Mastologias__alergias']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('idade_parto',['name'=>'Mastologias__idade_parto']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('amamentacao',['name'=>'Mastologias__amamentacao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('tempo_amamentacao',['name'=>'Mastologias__tempo_amamentacao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('mastite',['name'=>'Mastologias__mastite']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('localizacao',['name'=>'Mastologias__localizacao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('biopsia_mama',['name'=>'Mastologias__biopsia_mama']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('ap',['name'=>'Mastologias__ap']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('puncao_mama',['name'=>'Mastologias__puncao_mama']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('puncao_mama_direita',['name'=>'Mastologias__puncao_mama_direita']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('puncao_mama_esquerda',['name'=>'Mastologias__puncao_mama_esquerda']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('punca_mama_obs',['name'=>'Mastologias__punca_mama_obs']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('cirurgias',['name'=>'Mastologias__cirurgias']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('tabagismo',['name'=>'Mastologias__tabagismo']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('tempo_tabagismo',['name'=>'Mastologias__tempo_tabagismo']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('cigarros_dia',['name'=>'Mastologias__cigarros_dia']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                    echo $this->Form->input('ultimo_preventivo', ['name'=>'Mastologias__ultimo_preventivo','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                    echo $this->Form->input('ultima_mamografia', ['name'=>'Mastologias__ultima_mamografia','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('cancer_mama_ovario',['name'=>'Mastologias__cancer_mama_ovario']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('outros',['name'=>'Mastologias__outros']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('peso',['name'=>'Mastologias__peso']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('altura',['name'=>'Mastologias__altura']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('pa',['name'=>'Mastologias__pa']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('abulamento',['name'=>'Mastologias__abulamento']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('abulamento_direita',['name'=>'Mastologias__abulamento_direita']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('abulamento_esquerda',['name'=>'Mastologias__abulamento_esquerda']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('retracao',['name'=>'Mastologias__retracao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('retracao_direita',['name'=>'Mastologias__retracao_direita']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('retracao_esquerda',['name'=>'Mastologias__retracao_esquerda']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('cam',['name'=>'Mastologias__cam']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('papacao',['name'=>'Mastologias__papacao']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('expressao_papilar',['name'=>'Mastologias__expressao_papilar']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('ganglios',['name'=>'Mastologias__ganglios']);
                                echo "</div>";
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Mastologias', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Mastologias','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Mastologias') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cliente_historico_id') ?></th>
                <th><?= $this->Paginator->sort('nodulo_mama') ?></th>
                <th><?= $this->Paginator->sort('nodulo_mama_direita') ?></th>
                <th><?= $this->Paginator->sort('nodulo_mama_esquerda') ?></th>
                <th><?= $this->Paginator->sort('tempo_nodulo') ?></th>
                <th><?= $this->Paginator->sort('crescimento') ?></th>
                <th><?= $this->Paginator->sort('alteracao_exame_imagem') ?></th>
                <th><?= $this->Paginator->sort('exames') ?></th>
                <th><?= $this->Paginator->sort('mastalgia') ?></th>
                <th><?= $this->Paginator->sort('tempo_mastalgia') ?></th>
                <th><?= $this->Paginator->sort('secrecao_mamilar') ?></th>
                <th><?= $this->Paginator->sort('carc_mamilar') ?></th>
                <th><?= $this->Paginator->sort('exame_rotina') ?></th>
                <th><?= $this->Paginator->sort('outras_queixas') ?></th>
                <th><?= $this->Paginator->sort('idade') ?></th>
                <th><?= $this->Paginator->sort('g') ?></th>
                <th><?= $this->Paginator->sort('p') ?></th>
                <th><?= $this->Paginator->sort('a') ?></th>
                <th><?= $this->Paginator->sort('c') ?></th>
                <th><?= $this->Paginator->sort('dum') ?></th>
                <th><?= $this->Paginator->sort('menarca') ?></th>
                <th><?= $this->Paginator->sort('patologias') ?></th>
                <th><?= $this->Paginator->sort('mac') ?></th>
                <th><?= $this->Paginator->sort('medicacao') ?></th>
                <th><?= $this->Paginator->sort('alergias') ?></th>
                <th><?= $this->Paginator->sort('idade_parto') ?></th>
                <th><?= $this->Paginator->sort('amamentacao') ?></th>
                <th><?= $this->Paginator->sort('tempo_amamentacao') ?></th>
                <th><?= $this->Paginator->sort('mastite') ?></th>
                <th><?= $this->Paginator->sort('localizacao') ?></th>
                <th><?= $this->Paginator->sort('biopsia_mama') ?></th>
                <th><?= $this->Paginator->sort('ap') ?></th>
                <th><?= $this->Paginator->sort('puncao_mama') ?></th>
                <th><?= $this->Paginator->sort('puncao_mama_direita') ?></th>
                <th><?= $this->Paginator->sort('puncao_mama_esquerda') ?></th>
                <th><?= $this->Paginator->sort('punca_mama_obs') ?></th>
                <th><?= $this->Paginator->sort('cirurgias') ?></th>
                <th><?= $this->Paginator->sort('tabagismo') ?></th>
                <th><?= $this->Paginator->sort('tempo_tabagismo') ?></th>
                <th><?= $this->Paginator->sort('cigarros_dia') ?></th>
                <th><?= $this->Paginator->sort('ultimo_preventivo') ?></th>
                <th><?= $this->Paginator->sort('ultima_mamografia') ?></th>
                <th><?= $this->Paginator->sort('cancer_mama_ovario') ?></th>
                <th><?= $this->Paginator->sort('outros') ?></th>
                <th><?= $this->Paginator->sort('peso') ?></th>
                <th><?= $this->Paginator->sort('altura') ?></th>
                <th><?= $this->Paginator->sort('pa') ?></th>
                <th><?= $this->Paginator->sort('abulamento') ?></th>
                <th><?= $this->Paginator->sort('abulamento_direita') ?></th>
                <th><?= $this->Paginator->sort('abulamento_esquerda') ?></th>
                <th><?= $this->Paginator->sort('retracao') ?></th>
                <th><?= $this->Paginator->sort('retracao_direita') ?></th>
                <th><?= $this->Paginator->sort('retracao_esquerda') ?></th>
                <th><?= $this->Paginator->sort('cam') ?></th>
                <th><?= $this->Paginator->sort('papacao') ?></th>
                <th><?= $this->Paginator->sort('expressao_papilar') ?></th>
                <th><?= $this->Paginator->sort('ganglios') ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mastologias as $mastologia): ?>
            <tr>
                <td><?= $this->Number->format($mastologia->id) ?></td>
                <td><?= $this->Number->format($mastologia->cliente_historico_id) ?></td>
                <td><?= h($mastologia->nodulo_mama) ?></td>
                <td><?= h($mastologia->nodulo_mama_direita) ?></td>
                <td><?= h($mastologia->nodulo_mama_esquerda) ?></td>
                <td><?= h($mastologia->tempo_nodulo) ?></td>
                <td><?= h($mastologia->crescimento) ?></td>
                <td><?= h($mastologia->alteracao_exame_imagem) ?></td>
                <td><?= h($mastologia->exames) ?></td>
                <td><?= h($mastologia->mastalgia) ?></td>
                <td><?= h($mastologia->tempo_mastalgia) ?></td>
                <td><?= h($mastologia->secrecao_mamilar) ?></td>
                <td><?= h($mastologia->carc_mamilar) ?></td>
                <td><?= h($mastologia->exame_rotina) ?></td>
                <td><?= h($mastologia->outras_queixas) ?></td>
                <td><?= $this->Number->format($mastologia->idade) ?></td>
                <td><?= h($mastologia->g) ?></td>
                <td><?= h($mastologia->p) ?></td>
                <td><?= h($mastologia->a) ?></td>
                <td><?= h($mastologia->c) ?></td>
                <td><?= h($mastologia->dum) ?></td>
                <td><?= h($mastologia->menarca) ?></td>
                <td><?= h($mastologia->patologias) ?></td>
                <td><?= h($mastologia->mac) ?></td>
                <td><?= h($mastologia->medicacao) ?></td>
                <td><?= h($mastologia->alergias) ?></td>
                <td><?= $this->Number->format($mastologia->idade_parto) ?></td>
                <td><?= h($mastologia->amamentacao) ?></td>
                <td><?= h($mastologia->tempo_amamentacao) ?></td>
                <td><?= h($mastologia->mastite) ?></td>
                <td><?= h($mastologia->localizacao) ?></td>
                <td><?= h($mastologia->biopsia_mama) ?></td>
                <td><?= h($mastologia->ap) ?></td>
                <td><?= h($mastologia->puncao_mama) ?></td>
                <td><?= h($mastologia->puncao_mama_direita) ?></td>
                <td><?= h($mastologia->puncao_mama_esquerda) ?></td>
                <td><?= h($mastologia->punca_mama_obs) ?></td>
                <td><?= h($mastologia->cirurgias) ?></td>
                <td><?= h($mastologia->tabagismo) ?></td>
                <td><?= h($mastologia->tempo_tabagismo) ?></td>
                <td><?= h($mastologia->cigarros_dia) ?></td>
                <td><?= h($mastologia->ultimo_preventivo) ?></td>
                <td><?= h($mastologia->ultima_mamografia) ?></td>
                <td><?= h($mastologia->cancer_mama_ovario) ?></td>
                <td><?= h($mastologia->outros) ?></td>
                <td><?= h($mastologia->peso) ?></td>
                <td><?= h($mastologia->altura) ?></td>
                <td><?= h($mastologia->pa) ?></td>
                <td><?= h($mastologia->abulamento) ?></td>
                <td><?= h($mastologia->abulamento_direita) ?></td>
                <td><?= h($mastologia->abulamento_esquerda) ?></td>
                <td><?= h($mastologia->retracao) ?></td>
                <td><?= h($mastologia->retracao_direita) ?></td>
                <td><?= h($mastologia->retracao_esquerda) ?></td>
                <td><?= h($mastologia->cam) ?></td>
                <td><?= h($mastologia->papacao) ?></td>
                <td><?= h($mastologia->expressao_papilar) ?></td>
                <td><?= h($mastologia->ganglios) ?></td>
                <td><?= $mastologia->has('situacao_cadastro') ? $this->Html->link($mastologia->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $mastologia->situacao_cadastro->id]) : '' ?></td>
                <td><?= $mastologia->has('user') ? $this->Html->link($mastologia->user->nome, ['controller' => 'Users', 'action' => 'view', $mastologia->user->id]) : '' ?></td>
                <td><?= h($mastologia->created) ?></td>
                <td><?= h($mastologia->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'mastologias','action' => 'view', $mastologia->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'mastologias','action' => 'edit', $mastologia->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'mastologias','action'=>'delete', $mastologia->id],['onclick'=>'Deletar(\'mastologias\','.$mastologia->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
