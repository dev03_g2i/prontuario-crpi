<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Plano Contas</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                    <div class="finPlanoContas form">
                            <?= $this->Form->create($finPlanoConta) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Plano de Contas') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fin_classificacao_conta_id', ['label' => 'Classificação', 'type' => 'select', 'empty' => 'Selecione', 'options' => $classificacoes]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('nome'); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

