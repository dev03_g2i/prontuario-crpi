
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Plano Contas</h2>
        <ol class="breadcrumb">
            <li>Fin Plano Contas</li>
            <li class="active">
                <strong>Litagem de Fin Plano Contas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Plano Contas</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($finPlanoConta->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finPlanoConta->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Classificacao') ?></th>
                                <td><?= $this->Number->format($finPlanoConta->classificacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $this->Number->format($finPlanoConta->status) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Rateio') ?></th>
                                <td><?= $this->Number->format($finPlanoConta->rateio) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Pagar') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finPlanoConta->fin_contas_pagar)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Vencimento') ?></th>
                        <th><?= __('Valor') ?></th>
                        <th><?= __('Juros') ?></th>
                        <th><?= __('Multa') ?></th>
                        <th><?= __('Data Pagamento') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Valor Bruto') ?></th>
                        <th><?= __('Desconto') ?></th>
                        <th><?= __('Fin Tipo Pagamento Id') ?></th>
                        <th><?= __('Fin Tipo Documento Id') ?></th>
                        <th><?= __('Numero Documento') ?></th>
                        <th><?= __('Data Cadastro') ?></th>
                        <th><?= __('Parcela') ?></th>
                        <th><?= __('Fin Contas Pagar Planejamento Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finPlanoConta->fin_contas_pagar as $finContasPagar): ?>
                <tr>
                    <td><?= h($finContasPagar->id) ?></td>
                    <td><?= h($finContasPagar->data) ?></td>
                    <td><?= h($finContasPagar->vencimento) ?></td>
                    <td><?= h($finContasPagar->valor) ?></td>
                    <td><?= h($finContasPagar->juros) ?></td>
                    <td><?= h($finContasPagar->multa) ?></td>
                    <td><?= h($finContasPagar->data_pagamento) ?></td>
                    <td><?= h($finContasPagar->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasPagar->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasPagar->situacao) ?></td>
                    <td><?= h($finContasPagar->complemento) ?></td>
                    <td><?= h($finContasPagar->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasPagar->valor_bruto) ?></td>
                    <td><?= h($finContasPagar->desconto) ?></td>
                    <td><?= h($finContasPagar->fin_tipo_pagamento_id) ?></td>
                    <td><?= h($finContasPagar->fin_tipo_documento_id) ?></td>
                    <td><?= h($finContasPagar->numero_documento) ?></td>
                    <td><?= h($finContasPagar->data_cadastro) ?></td>
                    <td><?= h($finContasPagar->parcela) ?></td>
                    <td><?= h($finContasPagar->fin_contas_pagar_planejamento_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasPagar','action' => 'view', $finContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasPagar','action' => 'edit', $finContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasPagar','action' => 'delete', $finContasPagar->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finPlanoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Pagar Planejamentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finPlanoConta->fin_contas_pagar_planejamentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Periodicidade') ?></th>
                        <th><?= __('Plazo Determinado') ?></th>
                        <th><?= __('Valor Parcela') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Dia Vencimento') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('A Vista') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Proxima Geracao') ?></th>
                        <th><?= __('Ultima Geracao') ?></th>
                        <th><?= __('Geradas') ?></th>
                        <th><?= __('Tipo Pgmt') ?></th>
                        <th><?= __('Tipo Dcmt') ?></th>
                        <th><?= __('Ultimo Gerado') ?></th>
                        <th><?= __('Mes Vencimento') ?></th>
                        <th><?= __('Ultima Atualizacao') ?></th>
                        <th><?= __('Data Documento') ?></th>
                        <th><?= __('Primeiro Vencimento') ?></th>
                        <th><?= __('Mumero Documento') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finPlanoConta->fin_contas_pagar_planejamentos as $finContasPagarPlanejamentos): ?>
                <tr>
                    <td><?= h($finContasPagarPlanejamentos->id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->periodicidade) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->plazo_determinado) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->valor_parcela) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->cliente_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->complemento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->dia_vencimento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->situacao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->a_vista) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->correcao_monetaria) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->proxima_geracao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->ultima_geracao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->geradas) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->tipo_pgmt) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->tipo_dcmt) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->ultimo_gerado) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->mes_vencimento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->ultima_atualizacao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->data_documento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->primeiro_vencimento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->mumero_documento) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasPagarPlanejamentos','action' => 'view', $finContasPagarPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasPagarPlanejamentos','action' => 'edit', $finContasPagarPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasPagarPlanejamentos','action' => 'delete', $finContasPagarPlanejamentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finPlanoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Receber') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finPlanoConta->fin_contas_receber)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Valor') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Status') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Data Pagamento') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Data Vencimento') ?></th>
                        <th><?= __('Documento') ?></th>
                        <th><?= __('Numdoc') ?></th>
                        <th><?= __('Parcela') ?></th>
                        <th><?= __('Forma Pagamento') ?></th>
                        <th><?= __('Boleto Mensagem') ?></th>
                        <th><?= __('Boleto Mensagem Livre') ?></th>
                        <th><?= __('Boleto Barras') ?></th>
                        <th><?= __('Boletolinhadigitavel') ?></th>
                        <th><?= __('Boleto Nosso Numero') ?></th>
                        <th><?= __('Juros') ?></th>
                        <th><?= __('Multa') ?></th>
                        <th><?= __('Boleto Cedente') ?></th>
                        <th><?= __('Numero Fiscal') ?></th>
                        <th><?= __('Data Fiscal') ?></th>
                        <th><?= __('Valor Bruto') ?></th>
                        <th><?= __('Desconto') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Fin Tipo Pagamento Id') ?></th>
                        <th><?= __('Fin Tipo Documento If') ?></th>
                        <th><?= __('Numero Documento') ?></th>
                        <th><?= __('Fin Planejamento Id') ?></th>
                        <th><?= __('Impresso') ?></th>
                        <th><?= __('ValorPagar') ?></th>
                        <th><?= __('Vencimento Boleto') ?></th>
                        <th><?= __('Perjuros') ?></th>
                        <th><?= __('Permulta') ?></th>
                        <th><?= __('Data Cadastro') ?></th>
                        <th><?= __('Sis Antigo Cx Boleto') ?></th>
                        <th><?= __('Sis Antigo Cx') ?></th>
                        <th><?= __('Extern Id') ?></th>
                        <th><?= __('User Created') ?></th>
                        <th><?= __('User Modified') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th><?= __('Controle Financeiro Id') ?></th>
                        <th><?= __('Origem Registro') ?></th>
                        <th><?= __('Fatura Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finPlanoConta->fin_contas_receber as $finContasReceber): ?>
                <tr>
                    <td><?= h($finContasReceber->id) ?></td>
                    <td><?= h($finContasReceber->data) ?></td>
                    <td><?= h($finContasReceber->valor) ?></td>
                    <td><?= h($finContasReceber->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasReceber->cliente_id) ?></td>
                    <td><?= h($finContasReceber->status) ?></td>
                    <td><?= h($finContasReceber->complemento) ?></td>
                    <td><?= h($finContasReceber->data_pagamento) ?></td>
                    <td><?= h($finContasReceber->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasReceber->data_vencimento) ?></td>
                    <td><?= h($finContasReceber->documento) ?></td>
                    <td><?= h($finContasReceber->numdoc) ?></td>
                    <td><?= h($finContasReceber->parcela) ?></td>
                    <td><?= h($finContasReceber->forma_pagamento) ?></td>
                    <td><?= h($finContasReceber->boleto_mensagem) ?></td>
                    <td><?= h($finContasReceber->boleto_mensagem_livre) ?></td>
                    <td><?= h($finContasReceber->boleto_barras) ?></td>
                    <td><?= h($finContasReceber->boletolinhadigitavel) ?></td>
                    <td><?= h($finContasReceber->boleto_nosso_numero) ?></td>
                    <td><?= h($finContasReceber->juros) ?></td>
                    <td><?= h($finContasReceber->multa) ?></td>
                    <td><?= h($finContasReceber->boleto_cedente) ?></td>
                    <td><?= h($finContasReceber->numero_fiscal) ?></td>
                    <td><?= h($finContasReceber->data_fiscal) ?></td>
                    <td><?= h($finContasReceber->valor_bruto) ?></td>
                    <td><?= h($finContasReceber->desconto) ?></td>
                    <td><?= h($finContasReceber->correcao_monetaria) ?></td>
                    <td><?= h($finContasReceber->fin_tipo_pagamento_id) ?></td>
                    <td><?= h($finContasReceber->fin_tipo_documento_if) ?></td>
                    <td><?= h($finContasReceber->numero_documento) ?></td>
                    <td><?= h($finContasReceber->fin_planejamento_id) ?></td>
                    <td><?= h($finContasReceber->impresso) ?></td>
                    <td><?= h($finContasReceber->valorPagar) ?></td>
                    <td><?= h($finContasReceber->vencimento_boleto) ?></td>
                    <td><?= h($finContasReceber->perjuros) ?></td>
                    <td><?= h($finContasReceber->permulta) ?></td>
                    <td><?= h($finContasReceber->data_cadastro) ?></td>
                    <td><?= h($finContasReceber->sis_antigo_cx_boleto) ?></td>
                    <td><?= h($finContasReceber->sis_antigo_cx) ?></td>
                    <td><?= h($finContasReceber->extern_id) ?></td>
                    <td><?= h($finContasReceber->user_created) ?></td>
                    <td><?= h($finContasReceber->user_modified) ?></td>
                    <td><?= h($finContasReceber->created) ?></td>
                    <td><?= h($finContasReceber->modified) ?></td>
                    <td><?= h($finContasReceber->controle_financeiro_id) ?></td>
                    <td><?= h($finContasReceber->origem_registro) ?></td>
                    <td><?= h($finContasReceber->fatura_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasReceber','action' => 'view', $finContasReceber->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasReceber','action' => 'edit', $finContasReceber->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasReceber','action' => 'delete', $finContasReceber->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finPlanoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Receber Planejamentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finPlanoConta->fin_contas_receber_planejamentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Periodicidade') ?></th>
                        <th><?= __('Plazo Determinado') ?></th>
                        <th><?= __('Valor Parcela') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Dia Vencimento') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('A Vista') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Proxima Geracao') ?></th>
                        <th><?= __('Ultima Geracao') ?></th>
                        <th><?= __('Geradas') ?></th>
                        <th><?= __('Tipo Pgmt') ?></th>
                        <th><?= __('Tipo Dcmt') ?></th>
                        <th><?= __('Ultimo Gerado') ?></th>
                        <th><?= __('Mes Vencimento') ?></th>
                        <th><?= __('Ultima Atualizacao') ?></th>
                        <th><?= __('Data Documento') ?></th>
                        <th><?= __('Primeiro Vencimento') ?></th>
                        <th><?= __('Mumero Documento') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finPlanoConta->fin_contas_receber_planejamentos as $finContasReceberPlanejamentos): ?>
                <tr>
                    <td><?= h($finContasReceberPlanejamentos->id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->periodicidade) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->plazo_determinado) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->valor_parcela) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->cliente_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->complemento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->dia_vencimento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->situacao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->a_vista) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->correcao_monetaria) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->proxima_geracao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->ultima_geracao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->geradas) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->tipo_pgmt) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->tipo_dcmt) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->ultimo_gerado) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->mes_vencimento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->ultima_atualizacao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->data_documento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->primeiro_vencimento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->mumero_documento) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasReceberPlanejamentos','action' => 'view', $finContasReceberPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasReceberPlanejamentos','action' => 'edit', $finContasReceberPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasReceberPlanejamentos','action' => 'delete', $finContasReceberPlanejamentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finPlanoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finPlanoConta->fin_movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Documento') ?></th>
                        <th><?= __('Credito') ?></th>
                        <th><?= __('Debito') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th><?= __('Status Lancamento') ?></th>
                        <th><?= __('Categoria') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Contas Pagar Id') ?></th>
                        <th><?= __('Fin Banco Movimento Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Data Criacao') ?></th>
                        <th><?= __('Data Alteracao') ?></th>
                        <th><?= __('Criado Por') ?></th>
                        <th><?= __('Atualizado Por') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finPlanoConta->fin_movimentos as $finMovimentos): ?>
                <tr>
                    <td><?= h($finMovimentos->id) ?></td>
                    <td><?= h($finMovimentos->situacao) ?></td>
                    <td><?= h($finMovimentos->data) ?></td>
                    <td><?= h($finMovimentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finMovimentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finMovimentos->complemento) ?></td>
                    <td><?= h($finMovimentos->documento) ?></td>
                    <td><?= h($finMovimentos->credito) ?></td>
                    <td><?= h($finMovimentos->debito) ?></td>
                    <td><?= h($finMovimentos->fin_banco_id) ?></td>
                    <td><?= h($finMovimentos->status_lancamento) ?></td>
                    <td><?= h($finMovimentos->categoria) ?></td>
                    <td><?= h($finMovimentos->cliente_id) ?></td>
                    <td><?= h($finMovimentos->fin_contas_pagar_id) ?></td>
                    <td><?= h($finMovimentos->fin_banco_movimento_id) ?></td>
                    <td><?= h($finMovimentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finMovimentos->data_criacao) ?></td>
                    <td><?= h($finMovimentos->data_alteracao) ?></td>
                    <td><?= h($finMovimentos->criado_por) ?></td>
                    <td><?= h($finMovimentos->atualizado_por) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinMovimentos','action' => 'view', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinMovimentos','action' => 'edit', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinMovimentos','action' => 'delete', $finMovimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finPlanoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Planejamentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finPlanoConta->fin_planejamentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Periodicidade') ?></th>
                        <th><?= __('Plazo Determinado') ?></th>
                        <th><?= __('Valor Parcela') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Dia Vencimento') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('A Vista') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Proxima Geracao') ?></th>
                        <th><?= __('Ultima Geracao') ?></th>
                        <th><?= __('Geradas') ?></th>
                        <th><?= __('Tipo Pgmt') ?></th>
                        <th><?= __('Tipo Dcmt') ?></th>
                        <th><?= __('Ultimo Gerado') ?></th>
                        <th><?= __('Mes Vencimento') ?></th>
                        <th><?= __('Ultima Atualizacao') ?></th>
                        <th><?= __('Data Documento') ?></th>
                        <th><?= __('Primeiro Vencimento') ?></th>
                        <th><?= __('Mumero Documento') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finPlanoConta->fin_planejamentos as $finPlanejamentos): ?>
                <tr>
                    <td><?= h($finPlanejamentos->id) ?></td>
                    <td><?= h($finPlanejamentos->periodicidade) ?></td>
                    <td><?= h($finPlanejamentos->plazo_determinado) ?></td>
                    <td><?= h($finPlanejamentos->valor_parcela) ?></td>
                    <td><?= h($finPlanejamentos->cliente_id) ?></td>
                    <td><?= h($finPlanejamentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finPlanejamentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finPlanejamentos->complemento) ?></td>
                    <td><?= h($finPlanejamentos->dia_vencimento) ?></td>
                    <td><?= h($finPlanejamentos->situacao) ?></td>
                    <td><?= h($finPlanejamentos->a_vista) ?></td>
                    <td><?= h($finPlanejamentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finPlanejamentos->correcao_monetaria) ?></td>
                    <td><?= h($finPlanejamentos->proxima_geracao) ?></td>
                    <td><?= h($finPlanejamentos->ultima_geracao) ?></td>
                    <td><?= h($finPlanejamentos->geradas) ?></td>
                    <td><?= h($finPlanejamentos->tipo_pgmt) ?></td>
                    <td><?= h($finPlanejamentos->tipo_dcmt) ?></td>
                    <td><?= h($finPlanejamentos->ultimo_gerado) ?></td>
                    <td><?= h($finPlanejamentos->mes_vencimento) ?></td>
                    <td><?= h($finPlanejamentos->ultima_atualizacao) ?></td>
                    <td><?= h($finPlanejamentos->data_documento) ?></td>
                    <td><?= h($finPlanejamentos->primeiro_vencimento) ?></td>
                    <td><?= h($finPlanejamentos->mumero_documento) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinPlanejamentos','action' => 'view', $finPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinPlanejamentos','action' => 'edit', $finPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinPlanejamentos','action' => 'delete', $finPlanejamentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finPlanoConta->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


