<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Plano Contas</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinPlanoConta',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                <?= $this->Form->input('planocontas', ['label' => 'Plano de contas', 'type' => 'select', 'data-planocontas' => "planocontas", 'empty' => 'Selecione', 'options' => [@$fin_plano_contas->id => @$fin_plano_contas->nome], 'default' => @$fin_plano_contas->id]); ?>
                                </div>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('classificacao',['label'=>'Classificação', 'type' => 'select', 'empty' => 'Selecione', 'options' => $classificacoes]); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Plano de Contas', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Plano de Contas','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Lista de Plano de Contas') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('nome') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('fin_classificacao_contas', 'Classificação') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('rateio', 'Automatico/Manual') ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($finPlanoContas as $finPlanoConta): ?>
                                        <tr>
                                            <td>
                                                <?= $this->Number->format($finPlanoConta->id) ?>
                                            </td>
                                            <td>
                                                <?= h($finPlanoConta->nome) ?>
                                            </td>
                                            <td>
                                                <?= h($finPlanoConta->fin_classificacao_conta->descricao) ?>
                                            </td>
                                            <?php if(isset($finPlanoConta->rateio) &&  $finPlanoConta->rateio === 1):?>
                                                <td>
                                                    <?= 'Automático' ?>
                                                </td>
                                            <?php elseif(isset($finPlanoConta->rateio) &&  $finPlanoConta->rateio === 2):?>
                                                <td>
                                                    <?= 'Manual' ?>
                                                </td>
                                            <?php else:?>
                                                <td>
                                                    <?= 'Não tem' ?>
                                                </td>
                                            <?php endif;?>
                                            <td class="actions" style="white-space:nowrap">
                                                        <div class="no-padding dropdown">
                                                            <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                            <ul class="dropdown-menu dropdown-menu-left">
                                                                <li>
                                                                    <?= $this->Html->link('Visualizar/Editar', ['action' => 'edit', $finPlanoConta->id], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Editar Rateio', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Anexos', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                                <li>
                                                                <?= $this->Html->link('Excluir', '#Javascript:void(0)', ['onclick' => 'Deletar(\'FinPlanoContas\',' . $finPlanoConta->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false,  'listen' => 'f']) ?>
                                                                </li>
                                                                <li>
                                                                    <?= $this->Html->link('Vinculos', ['action' => ''], ['target' => '_blank']) ?>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>