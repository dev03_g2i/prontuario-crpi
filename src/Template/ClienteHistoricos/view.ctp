<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Procedimento</h2>
        <ol class="breadcrumb">
            <li>Paciente/Procedimento</li>
            <li class="active">
                <strong>Litagem de Paciente/Procedimento</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Código') ?></th>
                                <th><?= __('Cliente') ?></th>
                                <th><?= __('Tipo Historia') ?></th>
                                <th><?= __('Profissional') ?></th>
                                <th><?= __('Quem cadastrou') ?></th>
                                <th><?= __('Data Cadastro') ?></th>
                                <th><?= __('Data modificação') ?></th>

                            </tr>
                            <tr>
                                <td><?= $this->Number->format($clienteHistorico->id) ?></td>
                                <td><?= $clienteHistorico->has('cliente') ? $this->Html->link($clienteHistorico->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteHistorico->cliente->id]) : '' ?></td>
                                <td><?= $clienteHistorico->has('tipo_historia') ? $this->Html->link($clienteHistorico->tipo_historia->nome, ['controller' => 'TipoHistorias', 'action' => 'view', $clienteHistorico->tipo_historia->id]) : '' ?></td>
                                <td><?= $clienteHistorico->has('medico_responsavei') ? $this->Html->link($clienteHistorico->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $clienteHistorico->medico_responsavei->id]) : '' ?></td>
                                <td><?= $clienteHistorico->has('user') ? $this->Html->link($clienteHistorico->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteHistorico->user->id]) : '' ?></td>
                                <td><?= h($clienteHistorico->created) ?></td>
                                <td><?= h($clienteHistorico->modified) ?></td>
                            </tr>

                            <tr>
                                <th ><?= __('Descrição') ?></th>
                                <td colspan="6"><?= h($clienteHistorico->descricao) ?></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <?php if(!empty($clienteHistorico->historico_tratamentos)):
                    $historico_tratramento = $clienteHistorico->historico_tratamentos;
                ?>
                <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Tratamentos - Financeiro</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Cod. Tratamento') ?></th>
                                <th><?= __('Procedimento') ?></th>
                                <th><?= __('Tipo') ?></th>
                                <th><?= __('Descrição') ?></th>
                                <th><?= __('Face') ?></th>
                                <th><?= __('Financeiro') ?></th>
                                <th><?= __('Data cadastro') ?></th>
                                <th><?= __('Quem cadastrou') ?></th>

                            </tr>
                            <?php foreach ($historico_tratramento as $h):
                                if($h->origen=='f'): ?>
                            <tr>
                                <td><?= $h->has('atendimento_procedimento') ? $h->atendimento_procedimento->atendimento_id : '' ?></td>
                                <td><?= $h->has('atendimento_procedimento') ? $h->atendimento_procedimento->procedimento->nome: '' ?></td>
                                <td><?= $h->has('atendimento_iten') ? ($h->atendimento_iten->tipo=='d' ?'Dente' :'Região') : '' ?></td>
                                <td><?= $h->has('atendimento_iten') ? ($h->atendimento_iten->tipo=='d' ? $h->atendimento_iten->dente->descricao :$h->atendimento_iten->regio->nome) : '' ?></td>
                                <td><?= $h->has('face_iten') ? $h->face_iten->face->nome : 'Sem face' ?></td>
                                <td><?= !empty($h->atendimento_iten->financeiro) ? $h->atendimento_iten->financeiro->nome : $h->face_iten->financeiro->nome ?></td>
                                <td><?= !empty($h->atendimento_iten->data_financeiro) ? $h->atendimento_iten->data_financeiro : $h->face_iten->data_financeiro ?></td>
                                <td><?= $h->atendimento_iten->has('user__financeiro') ? $h->atendimento_iten->user__financeiro->nome: $h->face_iten->user__financeiro->nome ?></td>
                            </tr>
                            <?php endif;
                            endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>

                <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Tratamentos - Execução</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Cod. Tratamento') ?></th>
                                <th><?= __('Procedimento') ?></th>
                                <th><?= __('Tipo') ?></th>
                                <th><?= __('Descrição') ?></th>
                                <th><?= __('Face') ?></th>
                                <th><?= __('Executor') ?></th>
                                <th><?= __('Data cadastro') ?></th>
                                <th><?= __('Quem cadastrou') ?></th>

                            </tr>
                            <?php foreach ($historico_tratramento as $h):
                                if($h->origen=='e'): ?>
                            <tr>
                                <td><?= $h->has('atendimento_procedimento') ? $h->atendimento_procedimento->atendimento_id : '' ?></td>
                                <td><?= $h->has('atendimento_procedimento') ? $h->atendimento_procedimento->procedimento->nome : '' ?></td>
                                <td><?= $h->has('atendimento_iten') ? ($h->atendimento_iten->tipo=='d' ?'Dente' :'Região') : '' ?></td>
                                <td><?= $h->has('atendimento_iten') ? ($h->atendimento_iten->tipo=='d' ? $h->atendimento_iten->dente->descricao :$h->atendimento_iten->regio->nome) : '' ?></td>
                                <td><?= $h->has('face_iten') ? $h->face->face->nome : 'Sem face' ?></td>
                                <td><?= $h->atendimento_iten->has('executor') ? $h->atendimento_iten->executor->nome: $h->face_iten->executor->nome ?></td>
                                <td><?= !empty($h->atendimento_iten->data_executor) ? $h->atendimento_iten->data_executor: $h->face_iten->data_executor ?></td>
                                <td><?= $h->atendimento_iten->has('user__executor') ? $h->atendimento_iten->user__executor->nome: $h->face_iten->user__executor->nome ?></td>
                            </tr>
                            <?php endif;
                            endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php if(!empty($clienteHistorico->historico_face_artigos) || !empty($clienteHistorico->historico_iten_artigos)):
                    $face_artigos = $clienteHistorico->historico_face_artigos;
                    $iten_artigos = $clienteHistorico->historico_iten_artigos;
                ?>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Estoque/Dispensação</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Cod. Tratamento') ?></th>
                                <th><?= __('Procedimento') ?></th>
                                <th><?= __('Tipo') ?></th>
                                <th><?= __('Descrição') ?></th>
                                <th><?= __('Face') ?></th>
                                <th><?= __('Codigo Artigo') ?></th>
                                <th><?= __('Artigo') ?></th>
                                <th><?= __('Quantidade') ?></th>
                                <th><?= __('Data cadastro') ?></th>
                            </tr>
                            <?php
                                if(!empty($face_artigos)):
                            foreach ($face_artigos as $face_artigo):
                                    $atendimento_procedimento = $face_artigo->face_artigo->face_iten->atendimento_iten->atendimento_procedimento;
                                    $atendimento_iten = $face_artigo->face_artigo->face_iten->atendimento_iten;
                                    $face_iten = $face_artigo->face_artigo->face_iten;
                                    $artigo = $face_artigo->face_artigo->artigo;
                            ?>
                                    <tr>
                                        <td><?= !empty($atendimento_procedimento) ?  $atendimento_procedimento->atendimento_id: '' ?></td>
                                        <td><?= !empty($atendimento_procedimento) ?  $atendimento_procedimento->procedimento->nome: '' ?></td>
                                        <td><?= !empty($atendimento_iten) ?  ($atendimento_iten->tipo=='d' ? 'Dente':'Região') : '' ?></td>
                                        <td><?= !empty($atendimento_iten->dente) ?  $atendimento_iten->dente->descricao : $atendimento_iten->regio->descricao ?></td>
                                        <td><?= !empty($face_iten->face) ?  $face_iten->face->nome : '' ?></td>
                                        <td><?= !empty($artigo) ?  $artigo->codigo_livre : '' ?></td>
                                        <td><?= !empty($artigo) ?  $artigo->nome : '' ?></td>
                                        <td><?= $face_artigo->face_artigo->quantidade ?></td>
                                        <td><?= $face_artigo->created ?></td>
                                    </tr>
                                <?php
                            endforeach;
                            endif;
                            if(!empty($iten_artigos)):
                            foreach ($iten_artigos as $iten_artigo):
                                    $atendimento_procedimento = $iten_artigo->atendimentoiten_artigo->atendimento_iten->atendimento_procedimento;
                                    $atendimento_iten = $iten_artigo->atendimentoiten_artigo->atendimento_iten;
                                    $artigo = $iten_artigo->atendimentoiten_artigo->artigo;
                            ?>
                                    <tr>
                                        <td><?= !empty($atendimento_procedimento) ?  $atendimento_procedimento->atendimento_id: '' ?></td>
                                        <td><?= !empty($atendimento_procedimento) ?  $atendimento_procedimento->procedimento->nome: '' ?></td>
                                        <td><?= !empty($atendimento_iten) ?  ($atendimento_iten->tipo=='d' ? 'Dente':'Região') : '' ?></td>
                                        <td><?= !empty($atendimento_iten->dente) ?  $atendimento_iten->dente->descricao : $atendimento_iten->regio->descricao ?></td>
                                        <td>Sem faces</td>
                                        <td><?= !empty($artigo) ?  $artigo->codigo_livre : '' ?></td>
                                        <td><?= !empty($artigo) ?  $artigo->nome : '' ?></td>
                                        <td><?= $iten_artigo->atendimentoiten_artigo->quantidade ?></td>
                                        <td><?= $iten_artigo->created ?></td>
                                    </tr>
                                <?php
                            endforeach;
                            endif;
                            ?>

                        </table>
                    </div>
                </div>
            </div>

            <?php endif; ?>

        </div>
    </div>
</div>

