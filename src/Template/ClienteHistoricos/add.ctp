<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Procedimentos</li>
            <li class="active">
                <strong> Cadastrar Paciente/Procedimentos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="">
                    <div class="clienteHistoricos form">
                        <?= $this->Form->create($clienteHistorico,['id'=>'form-resp']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Procedimento') ?></legend>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'label' => 'Profissional','empty'=>'Selecione','default'=>@$user_logado['codigo_medico']]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('tipohistoria_id', ['options' => $tipoHistorias, 'label' => 'Tipo de procedimento','empty'=>'Selecione', 'default' => 1]);
                            echo "</div>";
                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('modelo', ['type' => 'select', 'empty'=>'Selecione',
                                'append' => [
                                    $this->Form->button('<span class="fa fa-paste"></span>', [
                                        'id' => 'add-historia-modelo', 'escape' => false, 'toggle' => 'tooltip','type'=>'button', 'data-placement' => 'bottom', 'title' => 'Colar Modelo', 'class' => 'btn btn-success'
                                    ])
                                ]]);
                            echo "</div>";
                            echo "<div class='col-md-1 text-right' style='margin-top: 23px;'>";
                            echo $this->Html->link('<i class="fa fa-plus-square"></i>', ['controller' => 'TipoHistoriaModelos', 'action' => 'index'], ['class' => 'btn btn-warning dim', 'escape' => false, 'toggle' => 'tooltip','type'=>'button', 'data-placement' => 'bottom', 'title' => 'Cadastrar Modelos', 'target' => '_blank', 'listen' => 'f']);
                            echo "</div>";

                            echo '<div class="col-md-12 text-right">';
                            echo $this->Form->button(__('Cancelar'), ['class' => 'btn btn-info', 'type'=>'button' ,'id'=>'cancel-historico']) ;
                            echo $this->Form->button(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'EnviarForm(\'form-resp\')' ,'id'=>'salve-tratamento']);
                            echo '</div>';

                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('descricao', ['label' => 'Descrição', 'data' => 'ck']);
                            echo "</div>";

                            echo $this->Form->input('cliente_id', ['type' => 'hidden', 'value' => $cliente]);
                            ?>

                        </fieldset>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

