
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Procedimentos</li>
            <li class="active">
                <strong>Listagem de Paciente/Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right" style="padding-top: 20px">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Novo', ['action' => 'add',$cliente_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Procedimento','id'=>'add-historico','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>
            <div class="row add-historico"></div>
            <div class="clearfix"></div>

            <div class="ibox float-e-margins">
                <div class="">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
                                    <th><?= $this->Paginator->sort('tipohistoria_id', ['label' => 'Tipo de Procedimento']) ?></th>
                                    <th><?= $this->Paginator->sort('medico_id', ['label' => 'Profissional']) ?></th>
                                    <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($clienteHistoricos as $clienteHistorico): ?>
                                    <tr>
                                        <!--<td><?= $this->Html->link($clienteHistorico->created,'#Javascript:void(0)',['toggle'=>'popover','data-placement'=>'bottom','data-content'=>$clienteHistorico->descricao,'data-trigger'=>'focus','listen'=>'f']) ?></td>-->
                                        <td><?=$clienteHistorico->created ?></td>
                                        <!--<td><?= $clienteHistorico->has('tipo_historia') ? $this->Html->link($clienteHistorico->tipo_historia->nome,  '#Javascript:void(0)',['toggle'=>'popover','data-placement'=>'bottom','data-content'=>$clienteHistorico->descricao,'data-trigger'=>'focus','listen'=>'f']) : '' ?></td>-->
                                        <td><?=$clienteHistorico->tipo_historia->nome ?></td>
                                        <!--<td><?= $clienteHistorico->has('medico_responsavei') ? $this->Html->link($clienteHistorico->medico_responsavei->nome,  '#Javascript:void(0)',['toggle'=>'popover','data-placement'=>'bottom','data-content'=>$clienteHistorico->descricao,'data-trigger'=>'focus','listen'=>'f']) : '' ?></td>-->
                                        <td><?=$clienteHistorico->medico_responsavei->nome;?></td>
                                        <!--<td><?= $this->Html->link(nl2br($clienteHistorico->descricao),'#Javascript:void(0)',['toggle'=>'popover','data-placement'=>'bottom','data-content'=>$clienteHistorico->descricao,'data-trigger'=>'focus','listen'=>'f','escape' => false]) ?></td>-->
                                        <?php 
                                            $descricao = ($clienteHistorico->descricao != strip_tags($clienteHistorico->descricao)) ? $clienteHistorico->descricao : nl2br($clienteHistorico->descricao);
                                        ?>
                                        <td class="text-wrap-td"><?= $descricao ?></td>
                                        <td class="actions">
                                            <!--<?= $this->Html->link($this->Html->icon('list'), ['action' => 'view', $clienteHistorico->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>-->
                                            <?php if($clienteHistorico->mastologia_id != null):
                                                echo $this->Html->link('<i class="fa fa-file-text-o"></i>', ['controller' => 'Mastologias', 'action' => 'edit', $clienteHistorico->mastologia_id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Formulario Mastologia','escape' => false,'class'=>'btn btn-xs btn-warning', 'listen' => 'f', 'target' => '_blank']);
                                            else:
                                                echo $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteHistorico->id,$cliente_id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']);
                                            endif;?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

