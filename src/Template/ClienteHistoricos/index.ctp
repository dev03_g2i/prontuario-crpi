
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Historicos</h2>
        <ol class="breadcrumb">
            <li>Cliente Historicos</li>
            <li class="active">
                <strong>Listagem de Cliente Historicos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content ">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Cliente Historicos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Cliente Historicos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Cliente Historicos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
            <th><?= $this->Paginator->sort('tipohistoria_id', ['label' => 'Tipo História']) ?></th>
            <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
            <th><?= $this->Paginator->sort('medico_id', ['label' => 'Profissional']) ?></th>
            <th><?= $this->Paginator->sort('anotacao', ['label' => 'Elemento']) ?></th>
            <th class="actions"><?= __('Ações') ?></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($clienteHistoricos as $clienteHistorico): ?>
            <tr>
                <td><?= h($clienteHistorico->created) ?></td>
                <td><?= $clienteHistorico->has('tipo_historia') ? $this->Html->link($clienteHistorico->tipo_historia->nome, ['controller' => 'TipoHistorias', 'action' => 'view', $clienteHistorico->tipo_historia->id]) : '' ?></td>
                <td><?= h($clienteHistorico->descricao) ?></td>
                <td><?= $clienteHistorico->has('medico_responsavei') ? $this->Html->link($clienteHistorico->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $clienteHistorico->medico_responsavei->id]) : '' ?></td>
                <td><?= h($clienteHistorico->anotacao) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list'), ['action' => 'view', $clienteHistorico->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteHistorico->id,$cliente_id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $clienteHistorico->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $clienteHistorico->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

