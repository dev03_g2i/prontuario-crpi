<div id="header">
    <div id="image-print">
        <img src="https://dummyimage.com/2480x150/f0f0f0/292729.png">
    </div>
    <div class="lista-info">
        <div class="col-print-6">
            <ul style="text-indent: -35px">
                <li>Paciente: SANTOS, MARIZA AMORIM DOS</li>
                <li>Médico: LUCIA RODRIGUES BEZERRA</li>
                <li>Convênio: U.F.M.S</li>
            </ul>
        </div>
        <div class="col-print-6 text-right">
            <ul>
                <li>N° Exame: 211554</li>
                <li>Data: 04/08/2017</li>
                <li>CRM/DR/DR</li>
            </ul>
        </div>
    </div>
</div>
<div id="footer">
    <img src="https://dummyimage.com/2480x150/f0f0f0/292729.png">
</div>

<script type="text/php">
    if (isset($pdf)) {
        $x = 270;
        $y = 10;
        $text = "Pagina {PAGE_NUM} de {PAGE_COUNT}";
        $font = null;
        $size = 10;
        $color = array(0,0,0);
        $word_space = 0.0;  //  default
        $char_space = 0.0;  //  default
        $angle = 0.0;   //  default
        $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
    }
</script>

<?php echo $clienteHistoricos->descricao; ?>

<div id="assinatura-print">
    <hr>
    <ul>
        <li>Fabiano Carloto</li>
        <li>CRM-MS 4493</li>
    </ul>
</div>
