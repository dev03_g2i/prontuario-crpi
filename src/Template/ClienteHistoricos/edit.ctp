
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Procedimentos</li>
            <li class="active">
                <strong>
                    Editar Paciente/Procedimentos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="clienteHistoricos form">
                        <?= $this->Form->create($clienteHistorico,['id'=>'form-resp']) ?>
                        <fieldset>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'label' => 'Profissional','empty'=>'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('tipohistoria_id', ['options' => $tipoHistorias, 'label' => 'Tipo de procedimento']);
                            echo "</div>";
                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('modelo', ['type' => 'select', 'empty'=>'Selecione',
                                'append' => [
                                    $this->Form->button('<span class="fa fa-paste"></span>', [
                                        'id' => 'add-historia-modelo', 'escape' => false, 'toggle' => 'tooltip','type'=>'button', 'data-placement' => 'bottom', 'title' => 'Colar Modelo', 'class' => 'btn btn-success'
                                    ])
                                ]]);
                            echo "</div>";
                            /*echo "<div class='col-md-1' style='margin-top: 23px;'>";
                            echo $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'ClienteHistoricos', 'action' => 'imprimir.pdf', '?'=>['id'=>$clienteHistorico->id ]], ['class' => 'btn btn-default dim', 'listen' => 'f', 'target' => '_blank', 'escape' => false]);
                            echo "</div>";*/

                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('descricao', ['label' => 'Descrição','type'=>'textarea', 'data' => 'ck']);
                            echo "</div>";

                            //echo $this->Html->link('print', ['controller' => 'ClienteHistoricos', 'action' => 'imprimir.pdf', '?'=>['id'=>$clienteHistorico->id ]], ['listen' => 'f', 'class' => 'btn btn-info', 'target' => '_blank']);

                            echo $this->Form->input('cliente_id', ['type' => 'hidden']);

                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary', 'onclick' => 'EnviarForm("form-resp")']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

