<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Relatório mensal</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinMovimentos', ['type' => 'get']) ?>
                                <div class="col-sm-12 no-padding">
                                    <div class="col-sm-3">
                                        <?= $this->Form->input('inicio', ['label' => 'Inicio', 'type' => 'text', 'class' => "datepicker", 'default' => $initialDate, 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?= $this->Form->input('fim', ['label' => 'Fim', 'type' => 'text', 'class' => "datepicker", 'default' => $finalDate, 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?= $this->Form->input('contabilidades', ['label' => 'Empresas', 'type' => 'select', 'multiple' => 'multiple', 'data-contabilidades' => "contabilidades", 'empty' => 'Selecione', 'options' => [@$fin_contabilidades->id => @$fin_contabilidades->nome], 'default' => @$fin_contabilidades->id]); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?= $this->Form->input('planocontas', ['label' => 'Plano de contas', 'multiple' => 'multiple', 'type' => 'select', 'data-planocontas' => "planocontas", 'empty' => 'Selecione', 'options' => [@$fin_plano_contas->id => @$fin_plano_contas->nome], 'default' => @$fin_plano_contas->id]); ?>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center p-t-25">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                    <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php if(!empty($relatorioMensal->entradas)): ?>
        <div class="row">
            <section class="col-sm-12 no-padding connectedSortable">
                <div class="col-sm-12 no-padding p-t-10">
                    <div class="col-sm-12 no-padding p-r-10">
                        <div class="ibox float-e-margins">
                            <div class="col-sm-12 ibox-content">
                                <h3>Entradas</h3>
                            </div>
                            <div class="ibox-content">
                                <div class="clearfix">
                                    <div class="p-t-10 table-responsive no-padding background-white">
                                        <div class="col-md-12">
                                            <table class="table" style="table-layout : auto;">
                                                <thead>
                                                    <tr>
                                                        <th>Faturamentos</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($relatorioMensal->entradas->data as $key => $entrada):?>
                                                    <tr>
                                                        <td colspan="5">
                                                            <table class="table table-hover" style="table-layout : fixed;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <?= $this->Paginator->sort('id', $entrada->name) ?>
                                                                        </th>
                                                                        <th>
                                                                            <?= $this->Paginator->sort('credito', 'Crédito') ?>
                                                                        </th>
                                                                        <th>
                                                                            <?= $this->Paginator->sort('debito', 'Debito') ?>
                                                                        </th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach ($entrada->data as $key => $receita): ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?= $receita->fin_plano_conta->nome ?>
                                                                        </td>
                                                                        <td class="d-credit">
                                                                            <?= number_format($receita->credito, 2, ',', '.'); ?>
                                                                        </td>
                                                                        <td class="d-debit">
                                                                            <?= number_format($receita->debito, 2, ',', '.'); ?>
                                                                        </td>
                                                                        <?php if (($receita->credito - $receita->debito) >= 0):?>
                                                                            <td class="d-credit">
                                                                                <?= number_format(($receita->credito - $receita->debito), 2, ',', '.'); ?>
                                                                            </td>
                                                                        <?php else: ?>
                                                                            <td class="d-debit">
                                                                                <?= number_format(($receita->credito - $receita->debito), 2, ',', '.'); ?>
                                                                            </td>
                                                                        <?php endif;?>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                                <?php if(count($relatorioMensal->entradas->data) > 1): ?>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td>Total:</td>
                                                                        <td class="d-credit"><?= number_format($entrada->credito, 2, ',', '.'); ?></td>
                                                                        <td class="d-credit"><?= number_format($entrada->debito, 2, ',', '.'); ?></td>
                                                                        <td><?= number_format($entrada->credito - $entrada->debito, 2, ',', '.'); ?></td>
                                                                    </tr>
                                                                </tfoot>
                                                                <?php endif;?>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td width="25%">Total Entradas:</td>
                                                        <td width="25%" class="d-credit">
                                                            <strong>
                                                                <?= number_format($relatorioMensal->entradas->totalEntradas->credito, 2, ',', '.'); ?>
                                                            </strong>
                                                        </td>
                                                        <td width="25%" class="d-debit">
                                                            <strong>
                                                                <?= number_format($relatorioMensal->entradas->totalEntradas->debito, 2, ',', '.'); ?>
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <strong>
                                                                <?= number_format($relatorioMensal->entradas->totalEntradas->credito - $relatorioMensal->entradas->totalEntradas->debito, 2, ',', '.'); ?>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <?php endif;?>
    <?php if(!empty($relatorioMensal->saidas)): ?>
        <div class="row">
            <section class="col-sm-12 no-padding connectedSortable">
                <div class="col-sm-12 no-padding p-t-10">
                    <div class="col-sm-12 no-padding p-r-10">
                        <div class="ibox float-e-margins">
                            <div class="col-sm-12 ibox-content">
                                <h3>Saidas</h3>
                            </div>
                            <div class="ibox-content">
                                <div class="clearfix">
                                    <div class="p-t-10 table-responsive no-padding background-white">
                                        <div class="col-md-12">
                                            <table class="table" style="table-layout : auto;">
                                                <thead>
                                                    <tr>
                                                        <th>Débitos</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($relatorioMensal->saidas->data as $key => $saida):?>
                                                    <tr>
                                                        <td colspan="5">
                                                            <table class="table table-hover" style="table-layout : fixed;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <?= $this->Paginator->sort('id', $saida->name) ?>
                                                                        </th>
                                                                        <th>
                                                                            <?= $this->Paginator->sort('credito', 'Crédito') ?>
                                                                        </th>
                                                                        <th>
                                                                            <?= $this->Paginator->sort('debito', 'Debito') ?>
                                                                        </th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach ($saida->data as $key => $despesa): ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?= $despesa->fin_plano_conta->nome ?>
                                                                        </td>
                                                                        <td class="d-credit">
                                                                            <?= number_format($despesa->credito, 2, ',', '.'); ?>
                                                                        </td>
                                                                        <td class="d-debit">
                                                                            <?= number_format($despesa->debito, 2, ',', '.'); ?>
                                                                        </td>
                                                                        <?php if (($despesa->credito - $despesa->debito) >= 0):?>
                                                                            <td class="d-credit">
                                                                                <?= number_format(($despesa->credito - $despesa->debito), 2, ',', '.'); ?>
                                                                            </td>
                                                                        <?php else: ?>
                                                                            <td class="d-debit">
                                                                                <?= number_format(($despesa->credito - $despesa->debito), 2, ',', '.'); ?>
                                                                            </td>
                                                                        <?php endif;?>
                                                                    </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                                <?php if(count($relatorioMensal->saidas->data) > 1): ?>
                                                                <tfoot>
                                                                    <tr>
                                                                        <td>Total:</td>
                                                                        <td class="d-credit"><?= number_format($saida->credito, 2, ',', '.'); ?></td>
                                                                        <td class="d-debit"><?= number_format($saida->debito, 2, ',', '.'); ?></td>
                                                                        <td><?= number_format($saida->credito - $saida->debito, 2, ',', '.'); ?></td>
                                                                    </tr>
                                                                </tfoot>
                                                                <?php endif;?>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td width="25%">Total Saidas:</td>
                                                        <td width="25%" class="d-credit">
                                                            <strong>
                                                                <?= number_format($relatorioMensal->saidas->totalSaidas->credito, 2, ',', '.'); ?>
                                                            </strong>
                                                        </td>
                                                        <td width="25%" class="d-debit">
                                                            <strong>
                                                                <?= number_format($relatorioMensal->saidas->totalSaidas->debito, 2, ',', '.'); ?>
                                                            </strong>
                                                        </td>
                                                        <td>
                                                            <strong>
                                                                <?= number_format($relatorioMensal->saidas->totalSaidas->credito - $relatorioMensal->saidas->totalSaidas->debito, 2, ',', '.'); ?>
                                                            </strong>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    <?php endif;?>
</div>
<?php echo $this->Html->script('controllers/DashboardFinanceiro',['block' => 'scriptBottom']); ?>