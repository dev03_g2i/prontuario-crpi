<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Bancos</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finBancos form">
                            <?= $this->Form->create($finBanco) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Banco') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                            <?=$this->Form->input('nome', ['required' => 'required']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('agencia'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('conta'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('limite', ['mask' => 'money']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('saldo', ['mask' => 'money']); ?>
                                        </div>
                                        <div class="col-md-6 no-padding p-t-20">
                                            <div class='col-md-3'>
                                                <?=$this->Form->input('mostra_saldo_geral'); ?>
                                            </div>
                                            <div class='col-md-3'>
                                                <?=$this->Form->input('mostra_fluxo'); ?>
                                            </div>
                                        </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>