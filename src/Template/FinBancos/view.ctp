
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Bancos</h2>
        <ol class="breadcrumb">
            <li>Fin Bancos</li>
            <li class="active">
                <strong>Litagem de Fin Bancos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Bancos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($finBanco->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Agencia') ?></th>
                                <td><?= h($finBanco->agencia) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Conta') ?></th>
                                <td><?= h($finBanco->conta) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finBanco->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Limite') ?></th>
                                <td><?= $this->Number->format($finBanco->limite) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Saldo') ?></th>
                                <td><?= $this->Number->format($finBanco->saldo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $this->Number->format($finBanco->status) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mostra Fluxo') ?>6</th>
                                <td><?= $finBanco->mostra_fluxo ? __('Sim') : __('Não'); ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mostra Saldo Geral') ?>6</th>
                                <td><?= $finBanco->mostra_saldo_geral ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Banco Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finBanco->fin_banco_movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th><?= __('Mes') ?></th>
                        <th><?= __('Ano') ?></th>
                        <th><?= __('Status') ?></th>
                        <th><?= __('SaldoInicial') ?></th>
                        <th><?= __('SaldoFinal') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Por') ?></th>
                        <th><?= __('EncerradoDt') ?></th>
                        <th><?= __('EncerradoPor') ?></th>
                        <th><?= __('Data Movimento') ?></th>
                        <th><?= __('Fin Movimento Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finBanco->fin_banco_movimentos as $finBancoMovimentos): ?>
                <tr>
                    <td><?= h($finBancoMovimentos->id) ?></td>
                    <td><?= h($finBancoMovimentos->fin_banco_id) ?></td>
                    <td><?= h($finBancoMovimentos->mes) ?></td>
                    <td><?= h($finBancoMovimentos->ano) ?></td>
                    <td><?= h($finBancoMovimentos->status) ?></td>
                    <td><?= h($finBancoMovimentos->saldoInicial) ?></td>
                    <td><?= h($finBancoMovimentos->saldoFinal) ?></td>
                    <td><?= h($finBancoMovimentos->data) ?></td>
                    <td><?= h($finBancoMovimentos->por) ?></td>
                    <td><?= h($finBancoMovimentos->encerradoDt) ?></td>
                    <td><?= h($finBancoMovimentos->encerradoPor) ?></td>
                    <td><?= h($finBancoMovimentos->data_movimento) ?></td>
                    <td><?= h($finBancoMovimentos->fin_movimento_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinBancoMovimentos','action' => 'view', $finBancoMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinBancoMovimentos','action' => 'edit', $finBancoMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinBancoMovimentos','action' => 'delete', $finBancoMovimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finBanco->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contabilidade Bancos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finBanco->fin_contabilidade_bancos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finBanco->fin_contabilidade_bancos as $finContabilidadeBancos): ?>
                <tr>
                    <td><?= h($finContabilidadeBancos->id) ?></td>
                    <td><?= h($finContabilidadeBancos->fin_contabilidade_id) ?></td>
                    <td><?= h($finContabilidadeBancos->fin_banco_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContabilidadeBancos','action' => 'view', $finContabilidadeBancos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContabilidadeBancos','action' => 'edit', $finContabilidadeBancos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContabilidadeBancos','action' => 'delete', $finContabilidadeBancos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finBanco->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finBanco->fin_movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Documento') ?></th>
                        <th><?= __('Credito') ?></th>
                        <th><?= __('Debito') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th><?= __('Status Lancamento') ?></th>
                        <th><?= __('Categoria') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Contas Pagar Id') ?></th>
                        <th><?= __('Fin Banco Movimento Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Data Criacao') ?></th>
                        <th><?= __('Data Alteracao') ?></th>
                        <th><?= __('Criado Por') ?></th>
                        <th><?= __('Atualizado Por') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finBanco->fin_movimentos as $finMovimentos): ?>
                <tr>
                    <td><?= h($finMovimentos->id) ?></td>
                    <td><?= h($finMovimentos->situacao) ?></td>
                    <td><?= h($finMovimentos->data) ?></td>
                    <td><?= h($finMovimentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finMovimentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finMovimentos->complemento) ?></td>
                    <td><?= h($finMovimentos->documento) ?></td>
                    <td><?= h($finMovimentos->credito) ?></td>
                    <td><?= h($finMovimentos->debito) ?></td>
                    <td><?= h($finMovimentos->fin_banco_id) ?></td>
                    <td><?= h($finMovimentos->status_lancamento) ?></td>
                    <td><?= h($finMovimentos->categoria) ?></td>
                    <td><?= h($finMovimentos->cliente_id) ?></td>
                    <td><?= h($finMovimentos->fin_contas_pagar_id) ?></td>
                    <td><?= h($finMovimentos->fin_banco_movimento_id) ?></td>
                    <td><?= h($finMovimentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finMovimentos->data_criacao) ?></td>
                    <td><?= h($finMovimentos->data_alteracao) ?></td>
                    <td><?= h($finMovimentos->criado_por) ?></td>
                    <td><?= h($finMovimentos->atualizado_por) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinMovimentos','action' => 'view', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinMovimentos','action' => 'edit', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinMovimentos','action' => 'delete', $finMovimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finBanco->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


