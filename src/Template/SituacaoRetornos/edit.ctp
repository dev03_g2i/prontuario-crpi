
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Retornos</h2>
        <ol class="breadcrumb">
            <li>Situacao Retornos</li>
            <li class="active">
                <strong>                    Editar Situacao Retornos
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="situacaoRetornos form">
                        <?= $this->Form->create($situacaoRetorno) ?>
                        <fieldset>
                                                        <legend><?= __('Editar Situacao Retorno') ?></legend>
                                                        <?php
                                                                        echo "<div class='col-md-6'>";
                                            echo $this->Form->input('nome');
                                            echo "</div>";
                                                                        ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

