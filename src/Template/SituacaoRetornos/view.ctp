

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Retornos</h2>
        <ol class="breadcrumb">
            <li>Situacao Retornos</li>
            <li class="active">
                <strong>Litagem de Situacao Retornos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="situacaoRetornos">
    <h3><?= h($situacaoRetorno->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($situacaoRetorno->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $situacaoRetorno->has('situacao_cadastro') ? $this->Html->link($situacaoRetorno->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $situacaoRetorno->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $situacaoRetorno->has('user') ? $this->Html->link($situacaoRetorno->user->nome, ['controller' => 'Users', 'action' => 'view', $situacaoRetorno->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($situacaoRetorno->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($situacaoRetorno->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($situacaoRetorno->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Retornos') ?></h4>
        <?php if (!empty($situacaoRetorno->retornos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Grupo Id') ?></th>
                <th><?= __('Previsao') ?></th>
                <th><?= __('Dias') ?></th>
                <th><?= __('Situacao Retorno Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Observacao') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($situacaoRetorno->retornos as $retornos): ?>
            <tr>
                <td><?= h($retornos->id) ?></td>
                <td><?= h($retornos->cliente_id) ?></td>
                <td><?= h($retornos->grupo_id) ?></td>
                <td><?= h($retornos->previsao) ?></td>
                <td><?= h($retornos->dias) ?></td>
                <td><?= h($retornos->situacao_retorno_id) ?></td>
                <td><?= h($retornos->created) ?></td>
                <td><?= h($retornos->modified) ?></td>
                <td><?= h($retornos->user_id) ?></td>
                <td><?= h($retornos->situacao_id) ?></td>
                <td><?= h($retornos->observacao) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Retornos','action' => 'view', $retornos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Retornos','action' => 'edit', $retornos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Retornos','action' => 'delete', $retornos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $situacaoRetorno->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

