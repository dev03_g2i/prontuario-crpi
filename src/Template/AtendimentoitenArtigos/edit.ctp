<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estoque</h2>
            <ol class="breadcrumb">
                <li>Dispensação</li>
                <li class="active">
                    <strong>Editar</strong>
                </li>
            </ol>
        </div>
        <div class="text-right">
            <a href="Javascript:void(0)" onclick="Navegar('','back')" class="btn btn-default btn-xs"><?= $this->Html->icon('arrow-left') ?></a>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="atendimentoitenArtigos form">
                            <?= $this->Form->create($atendimentoitenArtigo) ?>
                            <fieldset>
                                <legend><?= __('Editar Dispensação') ?></legend>
                                <?php
                                echo $this->Form->input('aitens_id', ['type'=>'hidden']);
                                echo $this->Form->input('procedimento_gasto_id', ['type'=>'hidden']);

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('artigo_id',['data' => 'select', 'controller' => 'EstqArtigo', 'action' => 'fill', 'data-value' => $atendimentoitenArtigo->artigo_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('quantidade');
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

