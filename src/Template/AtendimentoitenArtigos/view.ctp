
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimentoiten Artigos</h2>
        <ol class="breadcrumb">
            <li>Atendimentoiten Artigos</li>
            <li class="active">
                <strong>Litagem de Atendimentoiten Artigos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Atendimentoiten Artigos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Atendimento Iten') ?></th>
                                                                <td><?= $atendimentoitenArtigo->has('atendimento_iten') ? $this->Html->link($atendimentoitenArtigo->atendimento_iten->id, ['controller' => 'AtendimentoItens', 'action' => 'view', $atendimentoitenArtigo->atendimento_iten->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Procedimento Gasto') ?></th>
                                                                <td><?= $atendimentoitenArtigo->has('procedimento_gasto') ? $this->Html->link($atendimentoitenArtigo->procedimento_gasto->id, ['controller' => 'ProcedimentoGastos', 'action' => 'view', $atendimentoitenArtigo->procedimento_gasto->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $atendimentoitenArtigo->has('user') ? $this->Html->link($atendimentoitenArtigo->user->nome, ['controller' => 'Users', 'action' => 'view', $atendimentoitenArtigo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $atendimentoitenArtigo->has('situacao_cadastro') ? $this->Html->link($atendimentoitenArtigo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $atendimentoitenArtigo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($atendimentoitenArtigo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Artigo Id') ?></th>
                                <td><?= $this->Number->format($atendimentoitenArtigo->artigo_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($atendimentoitenArtigo->quantidade) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($atendimentoitenArtigo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($atendimentoitenArtigo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


