
<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Produtividade Profissionais</h2>
            <ol class="breadcrumb">
                <li>Produtividade Profissionais</li>
                <li class="active">
                    <strong>Litagem de Produtividade Profissionais</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Produtividade Profissionais</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Convenio') ?></th>
                                    <td><?= $produtividadeProfissionai->has('convenio') ? $this->Html->link($produtividadeProfissionai->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $produtividadeProfissionai->convenio->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Medico Responsavei') ?></th>
                                    <td><?= $produtividadeProfissionai->has('medico_responsavei') ? $this->Html->link($produtividadeProfissionai->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $produtividadeProfissionai->medico_responsavei->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Procedimento') ?></th>
                                    <td><?= $produtividadeProfissionai->has('procedimento') ? $this->Html->link($produtividadeProfissionai->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $produtividadeProfissionai->procedimento->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Quem Cadastrou') ?></th>
                                    <td><?= $produtividadeProfissionai->has('user') ? $this->Html->link($produtividadeProfissionai->user->nome, ['controller' => 'Users', 'action' => 'view', $produtividadeProfissionai->user->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Situação Cadastro') ?></th>
                                    <td><?= $produtividadeProfissionai->has('situacao_cadastro') ? $this->Html->link($produtividadeProfissionai->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $produtividadeProfissionai->situacao_cadastro->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Id') ?></th>
                                    <td><?= $this->Number->format($produtividadeProfissionai->id) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Perc Repasse') ?></th>
                                    <td><?= $this->Number->format($produtividadeProfissionai->perc_repasse) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Perc Imposto') ?></th>
                                    <td><?= $this->Number->format($produtividadeProfissionai->perc_imposto) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Criação') ?></th>
                                    <td><?= h($produtividadeProfissionai->created) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Modificação') ?></th>
                                    <td><?= h($produtividadeProfissionai->modified) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


