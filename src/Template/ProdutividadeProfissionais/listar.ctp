<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Produtividade profissionais</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('ProdutividadeProfissionais',['type'=>'get', 'id' => 'frm-produtividade']) ?>

                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_inicio', ['required' => 'required', 'label' => 'Periodo Inicio', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('unidade_id',['label' => 'Unidade', 'options' => $unidades, 'default' => 1, 'empty' => 'Selecione', 'id' => 'profterolens']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('origen_id', ['label' => 'Origem', 'options' => $origens, 'empty' => 'Selecione']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('convenio_id',['name' => 'convenio', 'id' => 'id-convenio', 'label' => 'Convênio', 'empty' => 'Selecione', 'options' => $convenios]);?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('data_fim', ['required' => 'required', 'label' => 'Periodo Fim', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('grupo_procedimento_id', ['name' => 'grupos[]', 'label' => 'Grupo Procedimento', 'empty' => 'Selecione', 'multiple', 'options' => $grupo_procedimentos, 'class' => 'select2']);?>
                    </div>
                    <div class="col-md-3">
                        <?php echo $this->Form->input('procedimentos', ['name' => 'procedimentos[]', 'label' => 'Procedimentos', 'empty' => 'Selecione', 'multiple', 'options' => $procedimentos, 'class' => 'select2']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('medico_id', ['name' => 'medicos[]', 'multiple', 'label' => 'Médico Executor', 'empty' => 'Selecione', 'options' => $medicos, 'class' => 'select2']);?>
                    </div>
                    <div class="clearfix"></div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('controle', ['type' => 'text']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_fatura_id', ['label' => 'Conferência', 'options' => $situacao_faturas, 'empty' => 'Todos']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('status_faturamento', ['label' => 'Faturamento', 'options' => [0=>'Não Faturado', 1=>'Faturado'], 'default' => 'Todos', 'empty' => 'Selecione']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('situacao_datas', ['label' => 'situação', 'options' => ['data'=>'Data Atendimento', 'dt_recebimento'=>'Data Recebimento'], 'default' => 'data', 'empty' => 'Selecione']);?>
                    </div>
                    <div class='col-md-3'>
                        <?php echo $this->Form->input('solicitante_id', ['name' => 'solicitantes[]', 'multiple', 'label' => 'Solicitante', 'empty' => 'Selecione', 'options' => $solicitantes, 'class' => 'select2']);?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('relatorio',['label'=>'Relatório', 'type'=>'select',
                            'options'=> $tiposRelatorio,
                            'append' => $this->Form->button($this->Html->icon('print'), ['type'=>'button',
                                'class' => 'btn btn-default gerarReport','toggle'=>'tooltip','
                                data-placement'=>'bottom','title'=>'Impressão','escape' => false,
                                'controller'=>'ProdutividadeProfissionais','action'=>'salvarTemp',
                                'controller2'=>'Relatorios','action2'=>'reportProdutividade'
                               ])
                        ])?>
                    </div>
                    <div class="col-md-12 text-center" style="margin-top: 22px;">
                        <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default btnSalvarTemp','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false, 'controller' => 'ProdutividadeProfissionais', 'action' => 'salvarTemp']) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default btnRefresh','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        <button type="button" class="btn btn-primary m-r-sm contagem" toggle="tooltip", data-placement="bottom", title="Qtd. Atendimentos">0</button>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins resetTotais">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tl.Caixa </label><br>
                                <span class="valor_caixa">R$ 0</span>
                            </div>
                            <div class="col-md-3">
                                <label>Tl.Fatura </label><br>
                                <span class="valor_fatura">R$ 0</span>
                            </div>
                            <div class="col-md-3">
                                <label>Vl.Recebido </label><br>
                                <span class="valor_prod_convenio valor_recebido">R$ 0</span>
                            </div>
                            <div class="col-md-3">
                                <label>Tl.Prod.Fatura</label><br>
                                <span class="valor_prod_fatura">R$ 0</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-3">
                                <label>Tl.Prod.Caixa</label><br>
                                <span class="valor_prod_caixa">R$ 0</span>
                            </div>
                            <div class="col-md-3">
                                <label>Tl.Prod.Rec.Convênio</label><br>
                                <span class="valor_prod_convenio">R$ 0</span>
                            </div>
                            <div class="col-md-3">
                                <label>Tl.Prod.Clin.Rec</label><br>
                                <span class="valor_prodclin_recebimento">R$ 0</span>
                            </div>
                            <div class="col-md-3">
                                <label>Tl.ProdCaixa.Clinica</label><br>
                                <span class="valor_prodcaixa_clinica">R$ 0</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-md-12 text-right">
                        <?php echo $this->Html->link('Receber Todos', ['action' => 'receber_todos', 'first' => 1], ['disabled' => true, 'onclick' => 'return false', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Receber Todos','class'=>'btn btn-primary', 'id' => 'btnReceberTodos', 'escape' => false]) ?>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('atendimento') ?></th>
                                <th><?= $this->Paginator->sort('data') ?></th>
                                <th><?= $this->Paginator->sort('paciente') ?></th>
                                <th><?= $this->Paginator->sort('convenio') ?></th>
                                <th><?= $this->Paginator->sort('profissional') ?></th>
                                <th><?= $this->Paginator->sort('codigo') ?></th>
                                <th><?= $this->Paginator->sort('procedimento') ?></th>
                                <th><?= $this->Paginator->sort('quantidade', 'Qtd') ?></th>
                                <th><?= $this->Paginator->sort('valor_fatura', 'Vl.Fatura') ?></th>
                                <th><?= $this->Paginator->sort('valor_caixa', 'Vl.Caixa') ?></th>
                                <th><?= $this->Paginator->sort('material', 'Vl.Material') ?></th>
                                <th><?= $this->Paginator->sort('total') ?></th>
                                <th><?= $this->Paginator->sort('valor_recebido', 'Vl.Rec.Convênio') ?></th>
                                <th><?= $this->Paginator->sort('perc_imposto', '%Imposto') ?></th>
                                <th><?= $this->Paginator->sort('perc_recebimento', '%Produ.') ?></th>
                                <th class="valor-prod-fatura"><?= $this->Paginator->sort('valor_prod_fatura', 'Vl.Prod.Fatura') ?></th>
                                <th class="valor-prod-caixa"><?= $this->Paginator->sort('valor_prod_caixa', 'Vl.Prod.Caixa') ?></th>
                                <th class="valor-prod-convenio"><?= $this->Paginator->sort('valor_prodrec_convenio', 'Vl.ProdRec.Convênio') ?></th>
                                <th class="valor-prodclin-recebimento"><?= $this->Paginator->sort('valor_prodclin_recebimento', 'Vl.ProdClin.Rec') ?></th>
                                <th><?= $this->Paginator->sort('producao_pg_dt', 'Dt.Pgto Produção') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $options = ['places' => 2];
                            if($escondeDados):
                                $i=0;
                                foreach ($temp as $t):?>
                                    <tr>
                                        <td><?=$t->atendimento_id?></td>
                                        <td><?=$this->Time->format($t->data_atendimento, 'dd/MM/YYYY')?></td>
                                        <td><?=$t->paciente?></td>
                                        <td><?=$t->convenio?></td>
                                        <td><?=$t->profissional?></td>
                                        <td><?=$t->codigo?></td>
                                        <td><?=$t->procedimento?></td>
                                        <td><?=$t->quantidade?></td>
                                        <td><?=$this->Number->format($t->valor_fatura, $options)?></td>
                                        <td><?=$this->Number->format($t->valor_caixa, $options)?></td>
                                        <td><?=$this->Number->format($t->material, $options)?></td>
                                        <td><?=$this->Number->format($t->total, $options)?></td>
                                        <td><?=$this->Number->format($t->valor_recebido, $options)?></td>
                                        <td><?=$this->Number->toPercentage($t->perc_imposto)?></td>
                                        <td><?=$this->Number->toPercentage($t->perc_recebimento)?></td>
                                        <td class="valor_prod_fatura"><?=$this->Number->format($t->valor_prod_fatura, $options)?></td>
                                        <td class="valor_prod_caixa"><?=$this->Number->format($t->valor_prod_caixa, $options)?></td>
                                        <td class="valor_prod_convenio"><?=$this->Number->format($t->valor_prodrec_convenio, $options)?></td>
                                        <td class="valor_prodclin_recebimento"><?=$this->Number->format($t->valor_prodclin_recebimento, $options)?></td>
                                        <td><?=$this->Time->format($t->producao_pg_dt, 'dd/MM/YYYY')?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Abrir atendimento', ['controller' => 'Atendimentos', 'action' => 'edit', $t->atendimento_id],['escape' => false, 'target' => '_blank']) ?></li>
                                                    <li><?= $this->Html->link('<i class="fa fa-file-text-o"></i> Faturamento', ['controller' => 'AtendimentoProcedimentos', 'action' => 'situacao_recebimento', $t->id, 'first' => 1],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                    <li><?= $this->Html->link('<i class="fa fa-dollar"></i> Pagamento Produção', ['controller' => 'AtendimentoProcedimentos', 'action' => 'pagamento_producao', $t->id, 'first' => 1],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php  $i++; endforeach; endif;?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?php if($escondeDados):?>
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>