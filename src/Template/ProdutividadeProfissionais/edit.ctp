<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Produtividade Profissionais</h2>
            <ol class="breadcrumb">
                <li>Produtividade Profissionais</li>
                <li class="active">
                    <strong> Editar Produtividade Profissionais
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="produtividadeProfissionais form">
                            <?= $this->Form->create($produtividadeProfissionai) ?>
                            <fieldset>
                                <legend><?= __('Editar Produtividade Profissionai') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('convenio_id', ['data' => 'select', 'controller' => 'convenios', 'action' => 'fill', 'data-value' => $produtividadeProfissionai->convenio_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('profissional_id', ['data' => 'select', 'controller' => 'medicoResponsaveis', 'action' => 'fill', 'data-value' => $produtividadeProfissionai->profissional_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('procedimento_id', ['data' => 'select', 'controller' => 'procedimentos', 'action' => 'fill', 'data-value' => $produtividadeProfissionai->procedimento_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('perc_recebimento', ['type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('perc_imposto', ['type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

