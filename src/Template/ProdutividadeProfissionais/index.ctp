<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Produtividade Profissionais</h2>
            <ol class="breadcrumb">
                <li>Produtividade Profissionais</li>
                <li class="active">
                    <strong>Litagem de Produtividade Profissionais</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('ProdutividadeProfissionai',['type'=>'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('convenio_id', ['name'=>'ProdutividadeProfissionais__convenio_id', 'options' => $convenios]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('procedimento_id', ['name'=>'procedimento_id','data'=>'select','controller'=>'procedimentos','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            ?>
                            <div class="col-md-4" style="margin-top: 23px;">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'fill-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'refresh-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Produtividade', ['action' => 'add', $profissional_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Produtividade Profissionais','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Produtividade Profissionais') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <!--<th><?= $this->Paginator->sort('id') ?></th>-->
                                        <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                        <th><?= $this->Paginator->sort('profissional_id') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('perc_recebimento') ?></th>
                                        <th><?= $this->Paginator->sort('perc_imposto') ?></th>
                                        <!--<th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>-->
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($produtividadeProfissionais as $produtividadeProfissionai): ?>
                                        <tr class="delete<?=$produtividadeProfissionai->id?>">
                                            <!--<td><?= $this->Number->format($produtividadeProfissionai->id) ?></td>-->
                                            <td><?= $produtividadeProfissionai->has('convenio') ? $this->Html->link($produtividadeProfissionai->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $produtividadeProfissionai->convenio->id]) : '' ?></td>
                                            <td><?= $produtividadeProfissionai->has('medico_responsavei') ? $this->Html->link($produtividadeProfissionai->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $produtividadeProfissionai->medico_responsavei->id]) : '' ?></td>
                                            <td><?= $produtividadeProfissionai->has('procedimento') ? $this->Html->link($produtividadeProfissionai->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $produtividadeProfissionai->procedimento->id]) : '' ?></td>
                                            <td><?= $this->Number->format($produtividadeProfissionai->perc_recebimento) ?></td>
                                            <td><?= $this->Number->format($produtividadeProfissionai->perc_imposto) ?></td>
                                            <!--<td><?= $produtividadeProfissionai->has('user') ? $this->Html->link($produtividadeProfissionai->user->nome, ['controller' => 'Users', 'action' => 'view', $produtividadeProfissionai->user->id]) : '' ?></td>
                <td><?= $produtividadeProfissionai->has('situacao_cadastro') ? $this->Html->link($produtividadeProfissionai->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $produtividadeProfissionai->situacao_cadastro->id]) : '' ?></td>
                <td><?= h($produtividadeProfissionai->created) ?></td>
                <td><?= h($produtividadeProfissionai->modified) ?></td>-->
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'produtividadeProfissionais','action' => 'view', $produtividadeProfissionai->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'produtividadeProfissionais','action' => 'edit', $produtividadeProfissionai->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Form->button($this->Html->icon('remove'),['onclick'=>'DeletarModal(\'produtividadeProfissionais\','.$produtividadeProfissionai->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
