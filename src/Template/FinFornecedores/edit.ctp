<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fornecedores</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finFornecedores form">
                                <?= $this->Form->create($finFornecedore) ?>
                                    <fieldset>
                                        <legend>
                                            <?= __('Editar Fornecedor') ?>
                                        </legend>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('nome', ['required' => 'required']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('telefone', ['mask' => 'fone']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('celular', ['mask' => 'phone']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('email'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('fin_plano_conta_id', ['label' => 'Plano de contas', 'class' => 'select2', 'data-value' => $finFornecedore->fin_plano_conta_id, 'options' => $planoContas, 'empty' => 'selecione']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('cep', ['mask' => 'cep']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('cidade'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('uf'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('numero'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('rua'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('bairro'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('cnpj', ['mask' => 'cnpj']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('cpf', ['mask' => 'cpf']); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('banco'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('agencia'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('conta'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('operacao'); ?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?=$this->Form->input('observacao'); ?>
                                        </div>
                                    </fieldset>
                                    <div class="col-md-12 text-right">
                                        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?= $this->Form->end() ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

