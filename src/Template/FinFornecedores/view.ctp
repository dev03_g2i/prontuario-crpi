
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Fornecedores</h2>
        <ol class="breadcrumb">
            <li>Fin Fornecedores</li>
            <li class="active">
                <strong>Litagem de Fin Fornecedores</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Fornecedores</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($finFornecedore->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Telefone') ?></th>
                                <td><?= h($finFornecedore->telefone) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Celular') ?></th>
                                <td><?= h($finFornecedore->celular) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($finFornecedore->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cidade') ?></th>
                                <td><?= h($finFornecedore->cidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Uf') ?></th>
                                <td><?= h($finFornecedore->uf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Rua') ?></th>
                                <td><?= h($finFornecedore->rua) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td><?= h($finFornecedore->bairro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Email') ?></th>
                                <td><?= h($finFornecedore->email) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cnpj') ?></th>
                                <td><?= h($finFornecedore->cnpj) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cpf') ?></th>
                                <td><?= h($finFornecedore->cpf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Banco') ?></th>
                                <td><?= h($finFornecedore->banco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Agencia') ?></th>
                                <td><?= h($finFornecedore->agencia) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Conta') ?></th>
                                <td><?= h($finFornecedore->conta) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Operacao') ?></th>
                                <td><?= h($finFornecedore->operacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finFornecedore->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= $this->Number->format($finFornecedore->numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao') ?></th>
                                <td><?= $this->Number->format($finFornecedore->situacao) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($finFornecedore->observacao)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


