
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Tipo Documento</h2>
        <ol class="breadcrumb">
            <li>Fin Tipo Documento</li>
            <li class="active">
                <strong>Litagem de Fin Tipo Documento</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Tipo Documento</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($finTipoDocumento->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Usado') ?></th>
                                <td><?= h($finTipoDocumento->usado) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finTipoDocumento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status Id') ?></th>
                                <td><?= $this->Number->format($finTipoDocumento->status_id) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Pagar') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finTipoDocumento->fin_contas_pagar)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Vencimento') ?></th>
                        <th><?= __('Valor') ?></th>
                        <th><?= __('Juros') ?></th>
                        <th><?= __('Multa') ?></th>
                        <th><?= __('Data Pagamento') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Valor Bruto') ?></th>
                        <th><?= __('Desconto') ?></th>
                        <th><?= __('Fin Tipo Pagamento Id') ?></th>
                        <th><?= __('Fin Tipo Documento Id') ?></th>
                        <th><?= __('Numero Documento') ?></th>
                        <th><?= __('Data Cadastro') ?></th>
                        <th><?= __('Parcela') ?></th>
                        <th><?= __('Fin Contas Pagar Planejamento Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finTipoDocumento->fin_contas_pagar as $finContasPagar): ?>
                <tr>
                    <td><?= h($finContasPagar->id) ?></td>
                    <td><?= h($finContasPagar->data) ?></td>
                    <td><?= h($finContasPagar->vencimento) ?></td>
                    <td><?= h($finContasPagar->valor) ?></td>
                    <td><?= h($finContasPagar->juros) ?></td>
                    <td><?= h($finContasPagar->multa) ?></td>
                    <td><?= h($finContasPagar->data_pagamento) ?></td>
                    <td><?= h($finContasPagar->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasPagar->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasPagar->situacao) ?></td>
                    <td><?= h($finContasPagar->complemento) ?></td>
                    <td><?= h($finContasPagar->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasPagar->valor_bruto) ?></td>
                    <td><?= h($finContasPagar->desconto) ?></td>
                    <td><?= h($finContasPagar->fin_tipo_pagamento_id) ?></td>
                    <td><?= h($finContasPagar->fin_tipo_documento_id) ?></td>
                    <td><?= h($finContasPagar->numero_documento) ?></td>
                    <td><?= h($finContasPagar->data_cadastro) ?></td>
                    <td><?= h($finContasPagar->parcela) ?></td>
                    <td><?= h($finContasPagar->fin_contas_pagar_planejamento_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasPagar','action' => 'view', $finContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasPagar','action' => 'edit', $finContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasPagar','action' => 'delete', $finContasPagar->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finTipoDocumento->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


