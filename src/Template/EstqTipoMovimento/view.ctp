
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Estq Tipo Movimento</h2>
        <ol class="breadcrumb">
            <li>Estq Tipo Movimento</li>
            <li class="active">
                <strong>Litagem de Estq Tipo Movimento</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Estq Tipo Movimento</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($estqTipoMovimento->descricao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $estqTipoMovimento->has('user') ? $this->Html->link($estqTipoMovimento->user->nome, ['controller' => 'Users', 'action' => 'view', $estqTipoMovimento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($estqTipoMovimento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Id') ?></th>
                                <td><?= $this->Number->format($estqTipoMovimento->situacao_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($estqTipoMovimento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($estqTipoMovimento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


