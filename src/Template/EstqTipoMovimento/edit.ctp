<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estq Tipo Movimento</h2>
            <ol class="breadcrumb">
                <li>Estq Tipo Movimento</li>
                <li class="active">
                    <strong>
                                                Editar Estq Tipo Movimento
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="estqTipoMovimento form">
                            <?= $this->Form->create($estqTipoMovimento) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Estq Tipo Movimento') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('situacao_id'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

