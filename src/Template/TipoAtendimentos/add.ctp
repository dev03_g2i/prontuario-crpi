
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Atendimentos</h2>
        <ol class="breadcrumb">
            <li>Tipo Atendimentos</li>
            <li class="active">
                <strong>                        Cadastrar Tipo Atendimentos
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="tipoAtendimentos form">
    <?= $this->Form->create($tipoAtendimento) ?>
    <fieldset>
                <legend><?= __('Cadastrar Tipo Atendimento') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                echo $this->Form->input('nome');
                echo "</div>";
               
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

