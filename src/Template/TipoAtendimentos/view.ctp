

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Atendimentos</h2>
        <ol class="breadcrumb">
            <li>Tipo Atendimentos</li>
            <li class="active">
                <strong>Litagem de Tipo Atendimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="tipoAtendimentos">
    <h3><?= h($tipoAtendimento->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($tipoAtendimento->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $tipoAtendimento->has('situacao_cadastro') ? $this->Html->link($tipoAtendimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoAtendimento->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $tipoAtendimento->has('user') ? $this->Html->link($tipoAtendimento->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoAtendimento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($tipoAtendimento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($tipoAtendimento->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($tipoAtendimento->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

