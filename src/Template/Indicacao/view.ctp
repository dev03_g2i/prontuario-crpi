<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Indicacao</h2>
        <ol class="breadcrumb">
            <li>Indicacao</li>
            <li class="active">
                <strong>Litagem de Indicacao</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Indicacao</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($indicacao->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situação Cadastro') ?></th>
                                <td><?= $indicacao->has('situacao_cadastro') ? $this->Html->link($indicacao->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $indicacao->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quem Cadastrou') ?></th>
                                <td><?= $indicacao->has('user') ? $this->Html->link($indicacao->user->nome, ['controller' => 'Users', 'action' => 'view', $indicacao->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($indicacao->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dt. Criação') ?></th>
                                <td><?= h($indicacao->created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dt. Modificação') ?></th>
                                <td><?= h($indicacao->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


