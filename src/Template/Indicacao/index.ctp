<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Indicação</h2>
        <ol class="breadcrumb">
            <li>Indicação</li>
            <li class="active">
                <strong>Litagem de Indicação</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('Indicacao', ['type' => 'get']) ?>
                        <?php
                        echo "<div class='col-md-4'>";
                        echo $this->Form->input('nome');
                        echo "</div>";
                        ?>
                        <div class="col-md-4" style="margin-top: 25px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Indicação', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Indicação', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Indicação') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                    <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                    <th><?= $this->Paginator->sort('user_id',['label'=>'Quem cadastrou']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($indicacao as $indicacao): ?>
                                    <tr>
                                        <td><?= $this->Number->format($indicacao->id) ?></td>
                                        <td><?= h($indicacao->nome) ?></td>
                                        <td><?= h($indicacao->created) ?></td>
                                        <td><?= h($indicacao->modified) ?></td>
                                        <td><?= $indicacao->has('situacao_cadastro') ? $this->Html->link($indicacao->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $indicacao->situacao_cadastro->id]) : '' ?></td>
                                        <td><?= $indicacao->has('user') ? $this->Html->link($indicacao->user->nome, ['controller' => 'Users', 'action' => 'view', $indicacao->user->id]) : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'Indicacao-itens','action' => 'all','?'=>['indicacao_id'=>$indicacao->id,'first'=>1,'ajax'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Itens', 'escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $indicacao->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $indicacao->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $indicacao->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

