
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Configuracoes</h2>
        <ol class="breadcrumb">
            <li>Configuracoes</li>
            <li class="active">
                <strong>Litagem de Configuracoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Configuracoes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Logo') ?></th>
                                <td><?= h($configuraco->logo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Logo Dir') ?></th>
                                <td><?= h($configuraco->logo_dir) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nome Login') ?></th>
                                <td><?= h($configuraco->nome_login) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Icone') ?></th>
                                <td><?= h($configuraco->icone) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Icone Dir') ?></th>
                                <td><?= h($configuraco->icone_dir) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Url Imagens') ?></th>
                                <td><?= h($configuraco->url_imagens) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Duplica Cpf') ?></th>
                                <td><?= h($configuraco->duplica_cpf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Url Image Laudo') ?></th>
                                <td><?= h($configuraco->url_image_laudo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Endereco') ?></th>
                                <td><?= h($configuraco->endereco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cnpj') ?></th>
                                <td><?= h($configuraco->cnpj) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Telefone') ?></th>
                                <td><?= h($configuraco->telefone) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Celular') ?></th>
                                <td><?= h($configuraco->celular) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Fax') ?></th>
                                <td><?= h($configuraco->fax) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($configuraco->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tiss Cnes') ?></th>
                                <td><?= h($configuraco->tiss_cnes) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($configuraco->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Usa Arquivo') ?></th>
                                <td><?= $this->Number->format($configuraco->usa_arquivo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Arquivo') ?></th>
                                <td><?= $this->Number->format($configuraco->arquivo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Restringe Cpf') ?></th>
                                <td><?= $this->Number->format($configuraco->restringe_cpf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Idade Cpf') ?></th>
                                <td><?= $this->Number->format($configuraco->idade_cpf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Paciente Matrvalidade') ?></th>
                                <td><?= $this->Number->format($configuraco->paciente_matrvalidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Matmed Fatura') ?></th>
                                <td><?= $this->Number->format($configuraco->matmed_fatura) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Edit Desconto') ?></th>
                                <td><?= $this->Number->format($configuraco->edit_desconto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Edit Valor Caixa') ?></th>
                                <td><?= $this->Number->format($configuraco->edit_valor_caixa) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Orientacao Proc Agenda') ?></th>
                                <td><?= $this->Number->format($configuraco->orientacao_proc_agenda) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Atendimento Dt Entrega') ?></th>
                                <td><?= $this->Number->format($configuraco->atendimento_dt_entrega) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Utiliza Laudos') ?></th>
                                <td><?= $this->Number->format($configuraco->utiliza_laudos) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Usa Filme') ?></th>
                                <td><?= $this->Number->format($configuraco->usa_filme) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Usa Porte') ?></th>
                                <td><?= $this->Number->format($configuraco->usa_porte) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Usa Uco') ?></th>
                                <td><?= $this->Number->format($configuraco->usa_uco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Indicacao Obrigatoria') ?></th>
                                <td><?= $this->Number->format($configuraco->indicacao_obrigatoria) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Caixa Idplano Contas') ?></th>
                                <td><?= $this->Number->format($configuraco->caixa_idplano_contas) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mostra Caixa Plano') ?></th>
                                <td><?= $this->Number->format($configuraco->mostra_caixa_plano) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Caixa Editdt Entrega') ?></th>
                                <td><?= $this->Number->format($configuraco->caixa_editdt_entrega) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Emissao Doc Ocultatitulo') ?></th>
                                <td><?= $this->Number->format($configuraco->emissao_doc_ocultatitulo) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Agenda Interface') ?></th>
                                <td><?= $this->Number->format($configuraco->agenda_interface) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Agenda Interface Lista') ?></th>
                                <td><?= $this->Number->format($configuraco->agenda_interface_lista) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Mostra Solicitante Procedimentos') ?></th>
                                <td><?= $this->Number->format($configuraco->mostra_solicitante_procedimentos) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Imprimir Ficha Semreceber') ?></th>
                                <td><?= $this->Number->format($configuraco->imprimir_ficha_semreceber) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Usa Filme Reais') ?></th>
                                <td><?= $this->Number->format($configuraco->usa_filme_reais) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Controle Finsetor Atendproc') ?></th>
                                <td><?= $this->Number->format($configuraco->controle_finsetor_atendproc) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Controle Finsetor Atendproc Padrao') ?></th>
                                <td><?= $this->Number->format($configuraco->controle_finsetor_atendproc_padrao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Controle Financsetor Receita') ?></th>
                                <td><?= $this->Number->format($configuraco->controle_financsetor_receita) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Controle Financsetor Receita Padrao') ?></th>
                                <td><?= $this->Number->format($configuraco->controle_financsetor_receita_padrao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($configuraco->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($configuraco->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Imagem') ?>6</th>
                                <td><?= $configuraco->imagem ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


