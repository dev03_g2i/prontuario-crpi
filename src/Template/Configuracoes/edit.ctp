<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Configuracoes</h2>
            <ol class="breadcrumb">
                <li>Configuracoes</li>
                <li class="active">
                    <strong>
                        Editar Configuracoes
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="configuracoes form">
                            <?= $this->Form->create($configuraco) ?>
                            <fieldset>
                                <legend><?= __('Editar Configuraco') ?></legend>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('logo'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('logo_dir'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('nome_login'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('icone'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('icone_dir'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('imagem'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('url_imagens'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('usa_arquivo'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('arquivo'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('restringe_cpf'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('duplica_cpf'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('idade_cpf'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('paciente_matrvalidade'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('url_image_laudo'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('matmed_fatura'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('edit_desconto'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('edit_valor_caixa'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('orientacao_proc_agenda'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('atendimento_dt_entrega'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('utiliza_laudos'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('usa_filme'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('usa_porte'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('usa_uco'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('indicacao_obrigatoria'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('caixa_idplano_contas'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('mostra_caixa_plano'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('caixa_editdt_entrega'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('emissao_doc_ocultatitulo'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('endereco'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('cnpj'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('telefone'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('celular'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('fax'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('cep'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('agenda_interface'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('agenda_interface_lista'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('mostra_solicitante_procedimentos'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('imprimir_ficha_semreceber'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('usa_filme_reais'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('tiss_cnes'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('controle_finsetor_atendproc'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('controle_finsetor_atendproc_padrao'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('controle_financsetor_receita'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('controle_financsetor_receita_padrao'); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

