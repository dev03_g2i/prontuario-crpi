<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Configuracoes</h2>
            <ol class="breadcrumb">
                <li>Configuracoes</li>
                <li class="active">
                    <strong>Listagem de Configuracoes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Configuraco',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('logo',['name'=>'Configuracoes__logo']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('logo_dir',['name'=>'Configuracoes__logo_dir']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('nome_login',['name'=>'Configuracoes__nome_login']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('icone',['name'=>'Configuracoes__icone']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('icone_dir',['name'=>'Configuracoes__icone_dir']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('imagem',['name'=>'Configuracoes__imagem']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('url_imagens',['name'=>'Configuracoes__url_imagens']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('usa_arquivo',['name'=>'Configuracoes__usa_arquivo']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('arquivo',['name'=>'Configuracoes__arquivo']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('restringe_cpf',['name'=>'Configuracoes__restringe_cpf']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('duplica_cpf',['name'=>'Configuracoes__duplica_cpf']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('idade_cpf',['name'=>'Configuracoes__idade_cpf']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('paciente_matrvalidade',['name'=>'Configuracoes__paciente_matrvalidade']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('url_image_laudo',['name'=>'Configuracoes__url_image_laudo']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('matmed_fatura',['name'=>'Configuracoes__matmed_fatura']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('edit_desconto',['name'=>'Configuracoes__edit_desconto']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('edit_valor_caixa',['name'=>'Configuracoes__edit_valor_caixa']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('orientacao_proc_agenda',['name'=>'Configuracoes__orientacao_proc_agenda']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('atendimento_dt_entrega',['name'=>'Configuracoes__atendimento_dt_entrega']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('utiliza_laudos',['name'=>'Configuracoes__utiliza_laudos']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('usa_filme',['name'=>'Configuracoes__usa_filme']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('usa_porte',['name'=>'Configuracoes__usa_porte']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('usa_uco',['name'=>'Configuracoes__usa_uco']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('indicacao_obrigatoria',['name'=>'Configuracoes__indicacao_obrigatoria']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('caixa_idplano_contas',['name'=>'Configuracoes__caixa_idplano_contas']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('mostra_caixa_plano',['name'=>'Configuracoes__mostra_caixa_plano']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('caixa_editdt_entrega',['name'=>'Configuracoes__caixa_editdt_entrega']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('emissao_doc_ocultatitulo',['name'=>'Configuracoes__emissao_doc_ocultatitulo']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('endereco',['name'=>'Configuracoes__endereco']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cnpj',['name'=>'Configuracoes__cnpj']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('telefone',['name'=>'Configuracoes__telefone']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('celular',['name'=>'Configuracoes__celular']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('fax',['name'=>'Configuracoes__fax']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cep',['name'=>'Configuracoes__cep']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('agenda_interface',['name'=>'Configuracoes__agenda_interface']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('agenda_interface_lista',['name'=>'Configuracoes__agenda_interface_lista']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('mostra_solicitante_procedimentos',['name'=>'Configuracoes__mostra_solicitante_procedimentos']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('imprimir_ficha_semreceber',['name'=>'Configuracoes__imprimir_ficha_semreceber']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('usa_filme_reais',['name'=>'Configuracoes__usa_filme_reais']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tiss_cnes',['name'=>'Configuracoes__tiss_cnes']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('controle_finsetor_atendproc',['name'=>'Configuracoes__controle_finsetor_atendproc']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('controle_finsetor_atendproc_padrao',['name'=>'Configuracoes__controle_finsetor_atendproc_padrao']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('controle_financsetor_receita',['name'=>'Configuracoes__controle_financsetor_receita']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('controle_financsetor_receita_padrao',['name'=>'Configuracoes__controle_financsetor_receita_padrao']); ?>
                                                                                    </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Configuracoes', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Configuracoes','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Configuracoes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('logo') ?></th>
                                                                                <th><?= $this->Paginator->sort('logo_dir') ?></th>
                                                                                <th><?= $this->Paginator->sort('nome_login') ?></th>
                                                                                <th><?= $this->Paginator->sort('icone') ?></th>
                                                                                <th><?= $this->Paginator->sort('icone_dir') ?></th>
                                                                                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                                                                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                                                                <th><?= $this->Paginator->sort('imagem') ?></th>
                                                                                <th><?= $this->Paginator->sort('url_imagens') ?></th>
                                                                                <th><?= $this->Paginator->sort('usa_arquivo') ?></th>
                                                                                <th><?= $this->Paginator->sort('arquivo') ?></th>
                                                                                <th><?= $this->Paginator->sort('restringe_cpf') ?></th>
                                                                                <th><?= $this->Paginator->sort('duplica_cpf') ?></th>
                                                                                <th><?= $this->Paginator->sort('idade_cpf') ?></th>
                                                                                <th><?= $this->Paginator->sort('paciente_matrvalidade') ?></th>
                                                                                <th><?= $this->Paginator->sort('url_image_laudo') ?></th>
                                                                                <th><?= $this->Paginator->sort('matmed_fatura') ?></th>
                                                                                <th><?= $this->Paginator->sort('edit_desconto') ?></th>
                                                                                <th><?= $this->Paginator->sort('edit_valor_caixa') ?></th>
                                                                                <th><?= $this->Paginator->sort('orientacao_proc_agenda') ?></th>
                                                                                <th><?= $this->Paginator->sort('atendimento_dt_entrega') ?></th>
                                                                                <th><?= $this->Paginator->sort('utiliza_laudos') ?></th>
                                                                                <th><?= $this->Paginator->sort('usa_filme') ?></th>
                                                                                <th><?= $this->Paginator->sort('usa_porte') ?></th>
                                                                                <th><?= $this->Paginator->sort('usa_uco') ?></th>
                                                                                <th><?= $this->Paginator->sort('indicacao_obrigatoria') ?></th>
                                                                                <th><?= $this->Paginator->sort('caixa_idplano_contas') ?></th>
                                                                                <th><?= $this->Paginator->sort('mostra_caixa_plano') ?></th>
                                                                                <th><?= $this->Paginator->sort('caixa_editdt_entrega') ?></th>
                                                                                <th><?= $this->Paginator->sort('emissao_doc_ocultatitulo') ?></th>
                                                                                <th><?= $this->Paginator->sort('endereco') ?></th>
                                                                                <th><?= $this->Paginator->sort('cnpj') ?></th>
                                                                                <th><?= $this->Paginator->sort('telefone') ?></th>
                                                                                <th><?= $this->Paginator->sort('celular') ?></th>
                                                                                <th><?= $this->Paginator->sort('fax') ?></th>
                                                                                <th><?= $this->Paginator->sort('cep') ?></th>
                                                                                <th><?= $this->Paginator->sort('agenda_interface') ?></th>
                                                                                <th><?= $this->Paginator->sort('agenda_interface_lista') ?></th>
                                                                                <th><?= $this->Paginator->sort('mostra_solicitante_procedimentos') ?></th>
                                                                                <th><?= $this->Paginator->sort('imprimir_ficha_semreceber') ?></th>
                                                                                <th><?= $this->Paginator->sort('usa_filme_reais') ?></th>
                                                                                <th><?= $this->Paginator->sort('tiss_cnes') ?></th>
                                                                                <th><?= $this->Paginator->sort('controle_finsetor_atendproc') ?></th>
                                                                                <th><?= $this->Paginator->sort('controle_finsetor_atendproc_padrao') ?></th>
                                                                                <th><?= $this->Paginator->sort('controle_financsetor_receita') ?></th>
                                                                                <th><?= $this->Paginator->sort('controle_financsetor_receita_padrao') ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($configuracoes as $configuraco): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($configuraco->id) ?></td>
                                                                                <td><?= h($configuraco->logo) ?></td>
                                                                                <td><?= h($configuraco->logo_dir) ?></td>
                                                                                <td><?= h($configuraco->nome_login) ?></td>
                                                                                <td><?= h($configuraco->icone) ?></td>
                                                                                <td><?= h($configuraco->icone_dir) ?></td>
                                                                                <td><?= h($configuraco->created) ?></td>
                                                                                <td><?= h($configuraco->modified) ?></td>
                                                                                <td><?= h($configuraco->imagem) ?></td>
                                                                                <td><?= h($configuraco->url_imagens) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->usa_arquivo) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->arquivo) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->restringe_cpf) ?></td>
                                                                                <td><?= h($configuraco->duplica_cpf) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->idade_cpf) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->paciente_matrvalidade) ?></td>
                                                                                <td><?= h($configuraco->url_image_laudo) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->matmed_fatura) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->edit_desconto) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->edit_valor_caixa) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->orientacao_proc_agenda) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->atendimento_dt_entrega) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->utiliza_laudos) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->usa_filme) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->usa_porte) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->usa_uco) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->indicacao_obrigatoria) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->caixa_idplano_contas) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->mostra_caixa_plano) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->caixa_editdt_entrega) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->emissao_doc_ocultatitulo) ?></td>
                                                                                <td><?= h($configuraco->endereco) ?></td>
                                                                                <td><?= h($configuraco->cnpj) ?></td>
                                                                                <td><?= h($configuraco->telefone) ?></td>
                                                                                <td><?= h($configuraco->celular) ?></td>
                                                                                <td><?= h($configuraco->fax) ?></td>
                                                                                <td><?= h($configuraco->cep) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->agenda_interface) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->agenda_interface_lista) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->mostra_solicitante_procedimentos) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->imprimir_ficha_semreceber) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->usa_filme_reais) ?></td>
                                                                                <td><?= h($configuraco->tiss_cnes) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->controle_finsetor_atendproc) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->controle_finsetor_atendproc_padrao) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->controle_financsetor_receita) ?></td>
                                                                                <td><?= $this->Number->format($configuraco->controle_financsetor_receita_padrao) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'configuracoes','action' => 'view', $configuraco->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'configuracoes','action' => 'edit', $configuraco->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'configuracoes','action'=>'delete', $configuraco->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
