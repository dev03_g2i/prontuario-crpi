<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Convenio Indicetabelas</h2>
            <ol class="breadcrumb">
                <li>Convenio Indicetabelas</li>
                <li class="active">
                    <strong>Listagem de Convenio Indicetabelas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('ConvenioIndicetabela',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('convenio_id', ['name'=>'ConvenioIndicetabelas__convenio_id','data'=>'select','controller'=>'convenios','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('grupo_procedimento_id', ['name'=>'ConvenioIndicetabelas__grupo_procedimento_id','data'=>'select','controller'=>'grupoProcedimentos','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('honorario_porte',['name'=>'ConvenioIndicetabelas__honorario_porte','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('operacional_uco',['name'=>'ConvenioIndicetabelas__operacional_uco','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('filme',['name'=>'ConvenioIndicetabelas__filme','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('user_insert',['name'=>'ConvenioIndicetabelas__user_insert']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('user_insert_dt', ['name'=>'ConvenioIndicetabelas__user_insert_dt','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('user_update',['name'=>'ConvenioIndicetabelas__user_update']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('user_update_dt', ['name'=>'ConvenioIndicetabelas__user_update_dt','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Convenio Indicetabelas', ['action' => 'add', $convenio_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Convenio Indicetabelas','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Convenio Indicetabelas') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                        <th><?= $this->Paginator->sort('grupo_procedimento_id') ?></th>
                                        <th><?= $this->Paginator->sort('operacional_uco') ?></th>
                                        <th><?= $this->Paginator->sort('filme') ?></th>
                                        <th><?= $this->Paginator->sort('honorario_porte') ?></th>
                                        <th><?= $this->Paginator->sort('user_insert', 'Cadastrado Por') ?></th>
                                        <th><?= $this->Paginator->sort('user_update', 'Alterado Por') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($convenioIndicetabelas as $convenioIndicetabela):
                                        $options = ['places' => 2];
                                        ?>
                                        <tr class="delete<?=$convenioIndicetabela->id?>">
                                            <td><?= $this->Number->format($convenioIndicetabela->id) ?></td>
                                            <td><?= $convenioIndicetabela->has('convenio') ? $this->Html->link($convenioIndicetabela->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $convenioIndicetabela->convenio->id]) : '' ?></td>
                                            <td><?= $convenioIndicetabela->has('grupo_procedimento') ? $this->Html->link($convenioIndicetabela->grupo_procedimento->nome, ['controller' => 'GrupoProcedimentos', 'action' => 'view', $convenioIndicetabela->grupo_procedimento->id]) : '' ?></td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="ConvenioIndicetabelas" data-param="decimal"
                                                   data-title="operacional_uco" data-pk="<?= $convenioIndicetabela->id ?>"
                                                   data-value="<?= $this->Number->format($convenioIndicetabela->operacional_uco, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($convenioIndicetabela->operacional_uco); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="ConvenioIndicetabelas" data-param="decimal"
                                                   data-title="filme" data-pk="<?= $convenioIndicetabela->id ?>"
                                                   data-value="<?= $this->Number->format($convenioIndicetabela->filme, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($convenioIndicetabela->filme); ?>
                                                </a>
                                            </td>
                                            <td>
                                                <a class="edit_local" href="#" data-type="text"
                                                   data-controller="ConvenioIndicetabelas" data-param="decimal"
                                                   data-title="honorario_porte" data-pk="<?= $convenioIndicetabela->id ?>"
                                                   data-value="<?= $this->Number->format($convenioIndicetabela->honorario_porte, $options) ?>"
                                                   listen="f"><?= $this->Number->currency($convenioIndicetabela->honorario_porte); ?>
                                                </a>
                                            </td>
                                            <td><?php echo ($convenioIndicetabela->has('user_reg'))?$convenioIndicetabela->user_reg->nome.' - '.$convenioIndicetabela->user_insert_dt:''; ?></td>
                                            <td><?= ($convenioIndicetabela->has('user_alt'))?$convenioIndicetabela->user_alt->nome.' - '.$convenioIndicetabela->user_update_dt:''; ?></td>
                                            <td class="actions">
                                                <div class="dropdown">
                                                    <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-prontuario">
                                                        <!--<li><?= $this->Html->link('<i class="fa fa-calculator"></i> Calcular',  ['controller'=>'convenioIndicetabelas','action' => 'calcular', $convenioIndicetabela->id],['onclick'=>'excluir(event, this)', 'escape' => false, 'listen' => 'f']) ?></li>-->
                                                        <li><?= $this->Html->link($this->Html->icon('list-alt').' Visualizar', ['controller'=>'convenioIndicetabelas','action' => 'view', $convenioIndicetabela->id],['escape' => false]) ?></li>
                                                        <li><?= $this->Html->link($this->Html->icon('pencil').' Editar', ['controller'=>'convenioIndicetabelas','action' => 'edit', $convenioIndicetabela->id],['escape' => false]) ?></li>
                                                        <li><?= $this->html->link($this->Html->icon('remove'). 'Deletar', 'javascript:void(0)', ['onclick'=>'DeletarModal(\'convenioIndicetabelas\','.$convenioIndicetabela->id.')', 'escape' => false, 'listen' => 'f']) ?></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
