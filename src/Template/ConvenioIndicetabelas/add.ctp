<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Convenio Indicetabelas</h2>
            <ol class="breadcrumb">
                <li>Convenio Indicetabelas</li>
                <li class="active">
                    <strong>
                        Cadastrar Convenio Indicetabelas
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <?php require_once ('form.ctp'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

