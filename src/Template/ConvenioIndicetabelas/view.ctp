
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Convenio Indicetabelas</h2>
        <ol class="breadcrumb">
            <li>Convenio Indicetabelas</li>
            <li class="active">
                <strong>Litagem de Convenio Indicetabelas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Convenio Indicetabelas</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Convenio') ?></th>
                                                                <td><?= $convenioIndicetabela->has('convenio') ? $this->Html->link($convenioIndicetabela->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $convenioIndicetabela->convenio->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Grupo Procedimento') ?></th>
                                                                <td><?= $convenioIndicetabela->has('grupo_procedimento') ? $this->Html->link($convenioIndicetabela->grupo_procedimento->nome, ['controller' => 'GrupoProcedimentos', 'action' => 'view', $convenioIndicetabela->grupo_procedimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $convenioIndicetabela->has('situacao_cadastro') ? $this->Html->link($convenioIndicetabela->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $convenioIndicetabela->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($convenioIndicetabela->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Honorario Porte') ?></th>
                                <td><?= $this->Number->format($convenioIndicetabela->honorario_porte) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Operacional Uco') ?></th>
                                <td><?= $this->Number->format($convenioIndicetabela->operacional_uco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Filme') ?></th>
                                <td><?= $this->Number->format($convenioIndicetabela->filme) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Insert') ?></th>
                                <td><?= $this->Number->format($convenioIndicetabela->user_insert) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Update') ?></th>
                                <td><?= $this->Number->format($convenioIndicetabela->user_update) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('User Insert Dt') ?></th>
                                                                <td><?= h($convenioIndicetabela->user_insert_dt) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('User Update Dt') ?></th>
                                                                <td><?= h($convenioIndicetabela->user_update_dt) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($convenioIndicetabela->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($convenioIndicetabela->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


