<div class="convenioIndicetabelas form">
    <?= $this->Form->create($convenioIndicetabela) ?>
    <div class='col-md-6'>
        <?=$this->Form->input('convenio_id', ['data'=>'select','controller'=>'convenios','action'=>'fill', 'data-value' => $convenio_id]); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('grupo_procedimento_id', ['options' => $grupo_procedimentos]); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('operacional_uco',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('filme',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('honorario_porte',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
    </div>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>