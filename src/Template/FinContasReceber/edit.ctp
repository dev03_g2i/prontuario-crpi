<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fin Contas Receber</h2>
            <ol class="breadcrumb">
                <li>Fin Contas Receber</li>
                <li class="active">
                    <strong>
                        Editar Fin Contas Receber
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finContasReceber form">
                            <?= $this->Form->create($finContasReceber) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Fin Contas Receber') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('data', ['type' => 'text', 'class' => 'datepicker','value'=>$this->Time->format($finContasReceber->data,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('valor',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fin_plano_conta_id', ['data'=>'select','controller'=>'finPlanoContas','action'=>'fill','data-value'=>$finContasReceber->fin_plano_conta_id, 'empty' => 'selecione']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('cliente_id', ['data'=>'select','controller'=>'clientes','action'=>'fill','data-value'=>$finContasReceber->cliente_id, 'empty' => 'selecione']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('status'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('complemento'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('data_pagamento', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($finContasReceber->data_pagamento,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fin_contabilidade_id', ['data'=>'select','controller'=>'finContabilidades','action'=>'fill','data-value'=>$finContasReceber->fin_contabilidade_id]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('data_vencimento', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($finContasReceber->data_vencimento,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('documento'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('numdoc'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('parcela'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('forma_pagamento'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('boleto_mensagem'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('boleto_mensagem_livre'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('boleto_barras'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('boletolinhadigitavel'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('boleto_nosso_numero'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('juros', ['value' => number_format($finContasReceber->juros, 2)]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('multa', ['value' => number_format($finContasReceber->multa, 2)]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('boleto_cedente'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('numero_fiscal'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('data_fiscal', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($finContasReceber->data_fiscal,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('valor_bruto',['prepend' => 'R$', 'type' => 'text', 'value' => number_format($finContasReceber->valor_bruto, 2), 'mask' => 'money']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('desconto',['prepend' => 'R$', 'type' => 'text', 'value' => number_format($finContasReceber->desconto, 2), 'mask' => 'money']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('correcao_monetaria'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fin_tipo_pagamento_id', ['data'=>'select','controller'=>'finTipoPagamento','action'=>'fill','data-value'=>$finContasReceber->fin_tipo_pagamento_id, 'empty' => 'selecione']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fin_tipo_documento_id', ['data'=>'select','controller'=>'finTipoDocumento','action'=>'fill','data-value'=>$finContasReceber->fin_tipo_documento_id, 'empty' => 'selecione']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('numero_documento'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fin_planejamento_id', ['data'=>'select','controller'=>'finContasReceberPlanejamentos','action'=>'fill','data-value'=>$finContasReceber->fin_planejamento_id, 'empty' => 'selecione']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('impresso'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('valorPagar',['prepend' => 'R$', 'value' => number_format($finContasReceber->valorPagar, 2), 'type' => 'text', 'mask' => 'money']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('vencimento_boleto', ['empty' => true,'type'=>'text','class'=>'datepicker','value'=>$this->Time->format($finContasReceber->vencimento_boleto,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('perjuros'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('permulta'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?php echo $this->Form->input('data_cadastro', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($finContasReceber->data_cadastro,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('sis_antigo_cx_boleto'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('sis_antigo_cx'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('extern_id'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('user_created'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('user_modified'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('controle_financeiro_id'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('origem_registro'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fatura_id'); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>