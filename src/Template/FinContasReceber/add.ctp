<div class="this-place">
  <div class="white-bg page-heading">
    <div class="col-lg-9">
      <h2>Contas à receber</h2>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="area">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox float-e-margins">
          <div class="ibox-content">
            <div class="contasreceber form">
              <?= $this->Form->create($finContasReceber) ?>
                <fieldset>
                  <legend>
                    <?= __('Cadastrar Conta') ?>
                  </legend>
                  <div class='col-md-6'>
                    <?=$this->Form->input('data', ['label' => 'Data do documento', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('data_vencimento', ['label' => 'Data de vencimento', 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('valor_bruto', ['type' => 'text', 'label' => 'Valor bruto', 'value' => number_format($finContasReceber->valor_bruto, 2), 'mask' => 'money', 'prepend' => [$this->Form->button($this->Html->icon("usd"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('juros', ['type' => 'text', 'label' => 'Juros', 'value' => number_format($finContasReceber->juros, 2), 'mask' => 'money', 'prepend' => [$this->Form->button($this->Html->icon("usd"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('multa', ['type' => 'text', 'label' => 'Multa', 'value' => number_format($finContasReceber->multa, 2), 'mask' => 'money', 'prepend' => [$this->Form->button($this->Html->icon("usd"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('desconto', ['type' => 'text', 'label' => 'Desconto', 'value' => number_format($finContasReceber->desconto, 2), 'mask' => 'money', 'prepend' => [$this->Form->button($this->Html->icon("usd"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('valor', ['type' => 'text', 'redonly', 'value' => number_format($finContasReceber->valor, 2), 'label' => 'Valor líquido', 'mask' => 'money', 'prepend' => [$this->Form->button($this->Html->icon("usd"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('data_pagamento', ['empty' => true,'type'=>'text','class'=>'', 'redonly', 'disabled', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('fin_plano_conta_id', ['label' => 'Plano de contas', 'required' => 'required', 'data'=>'select','controller'=>'finPlanoContas','action'=>'fill',  'empty' => 'selecione']); ?>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group select required">
                      <label class="control-label" for="cliente-id">Cliente</label><button class="btn btn-xs btn-primary m-l-5">Novo</button>
                      <select name="cliente_id" class="form-control select2-hidden-accessible" required="required"
                        data="select" controller="Clientes" action="fill" id="cliente-id" tabindex="-1" aria-hidden="true">
                        <option value="">selecione</option>
                      </select>
                      <span class="select2 select2-container select2-container--bootstrap" dir="ltr" style="width: 100%;">
                        <span class="selection">
                        </span>
                        <span class="dropdown-wrapper" aria-hidden="true"></span>
                      </span>
                    </div>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('fin_tipo_documento_id', ['label' => 'Tipo documento', 'data'=>'select', 'required' => 'required', 'controller'=>'finTipoDocumento','action'=>'fill',  'empty' => 'selecione']); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('numero_documento', ['required' => 'required']); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('fin_tipo_pagamento_id', ['label' => 'Forma de pagamento', 'required' => 'required', 'data'=>'select','controller'=>'finTipoPagamento','action'=>'fill',  'empty' => 'selecione']); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('status', ['label' => 'Status', 'options' => [1 => 'Ativo', 2 => 'Inativo'], 'empty' => 'selecione']); ?>
                  </div>
                  <div class='col-md-6'>
                    <?=$this->Form->input('fin_contabilidade_id', ['label' => 'Empresas', 'required' => 'required', 'data'=>'select','controller'=>'finContabilidades','action'=>'fill', 'empty' => 'selecione']); ?>
                  </div>
                  <div class='col-md-12'>
                    <?=$this->Form->input('complemento'); ?>
                  </div>
                </fieldset>
                <div class="col-md-12 text-right">
                  <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                </div>
                <div class="clearfix"></div>
                <?= $this->Form->end() ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
echo $this->Html->script('controllers/Contaspagar',['block' => 'scriptBottom']);
?>