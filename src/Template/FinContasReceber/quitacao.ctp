<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Contas à receber</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="filtros">
                            <div class="col-sm-6 no-padding" style="border-right: thin solid lightgray;">
                                <?= $this->Form->create('ContasReceber', ['type' => 'get', 'id' => 'contas-receber-form']) ?>
                                <div class="col-sm-12 no-padding">
                                    <div class="col-sm-12 m-b-20"><h3><strong>Filtros</strong></h3></div>
                                    <div class="col-sm-12 no-padding">
                                        <div class="col-sm-6">
                                            <?= $this->Form->input('inicio', ['label' => 'Inicio', 'type' => 'text', 'class' => "datepicker", 'value' => date('01/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $this->Form->input('fim', ['label' => 'Fim', 'type' => 'text', 'class' => "datepicker", 'value' => date('t/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 no-padding">
                                        <div class="col-sm-6">
                                        <?= $this->Form->input('cliente', ['label' => 'Cliente', 'type' => 'select', 'data-cliente' => "cliente", 'empty' => 'Selecione', 'options' => [@$cliente->id => @$cliente->nome], 'default' => @$cliente->id]); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $this->Form->input('planoconta', ['label' => 'Plano de contas', 'type' => 'select', 'data-planocontas' => "planocontas", 'empty' => 'Selecione', 'options' => [@$fin_plano_contas->id => @$fin_plano_contas->nome]]); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 text-center p-t-20">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default btnFiltrarContasReceber', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                    <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default btnRefreshContasReceber', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                    <!-- <= $this->Html->link('<i class="fa fa-print"></i>', '', ['escape' => false, 'class'=>'btn btn-default', 'title' => 'Relatório por vencimento']) ?>-->
                                    <!-- <= $this->Html->link('<i class="fa fa-print"></i>', '', ['escape' => false, 'class'=>'btn btn-default', 'title' => 'Relatório geral']) ?>-->
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                            <div class="col-sm-6 no-padding">
                                <div class="col-sm-12 no-padding">
                                    <div class="col-sm-12 m-b-20"><h3><strong>Quitação</strong></h3></div>
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('banco_movimento', ['label' => 'Banco/Movimento', 'type' => 'select', 'empty' => 'Selecione', 'options' => $banco_movimentos]); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('documento', ['label' => 'Documento', 'type' => 'text']) ?>
                                    </div>
                                </div>
                                <div class="col-sm-12 no-padding m-t-5">
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('forma_pagamento', ['label' => 'Forma de pagamento', 'type' => 'select', 'empty' => 'Selecione', 'options' => $formas_pagamento, 'class' => 'form-control select2']) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $this->Form->button('processar', ['type' => 'button', 'id' => 'processarContas', 'class' => 'btn btn-xs btn-primary m-t-28', 'title' => 'Processar', 'escape' => false]) ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label>Total selecionado: </label>
                                    <strong id="totalSelecionado">0,00</strong>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row table-responsive">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <div class="col-md-3">
                            <label>Total do período</label>
                            </br>
                            <strong><?= isset($totalPeriodo) ? number_format($totalPeriodo, 2, ',', '.') : '0,00' ?></strong>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <section class="col-sm-12 no-padding connectedSortable">
            <div class="col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding">
                    <div class="ibox float-e-margins">
                        <div class="col-sm-12 ibox-content btn-group p-b-0 p-l-20">
                            <div class="col-sm-2 no-padding">
                                <h3>Contas à receber</h3>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="p-t-10 no-padding background-white">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>
                                                Vencimento
                                            </th>
                                            <th>
                                                Fornecedor
                                            </th>
                                            <th>
                                                Plano de contas
                                            </th>
                                            <th>
                                                Empresa
                                            </th>
                                            <th>
                                                Tipo de documento
                                            </th>
                                            <th>
                                                Parcela
                                            </th>
                                            <th>
                                                Complemento
                                            </th>
                                            <th>
                                                Valor Bruto
                                            </th>
                                            <th>
                                                Valor c/ ded.
                                            </th>
                                            <th>
                                                Marcar pgmt
                                            </th>
                                            <th>
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($contasreceber as $contasreceber): ?>
                                            <tr>
                                                <td name="vencimentoCol">
                                                    <?= date_format($contasreceber->vencimento, 'd/m/Y') ?>
                                                </td>
                                                <td name="fornecedorCol">
                                                    <?= h($contasreceber->cliente->nome) ?>
                                                </td>
                                                <td name="planoCol">
                                                    <?= h($contasreceber->fin_plano_conta->nome) ?>
                                                </td>
                                                <td name="contabilidadeCol">
                                                    <?= h($contasreceber->fin_contabilidade->nome) ?>
                                                </td>
                                                <td name="documentoCol">
                                                    <?= h($contasreceber->fin_tipo_documento->descricao) ?>
                                                </td>
                                                <td name="parcelaCol">
                                                    <?= h($contasreceber->parcela) ?>
                                                </td>
                                                <td name="complementoCol">
                                                    <?= $contasreceber->complemento ? h($contasreceber->complemento) : '' ?>
                                                </td>
                                                <td name="valorCol">
                                                    <?= $contasreceber->valor_bruto ? number_format($contasreceber->valor_bruto, 2, ',', '.') : '0,00' ?>
                                                </td>
                                                <td name="valorDedCol">
                                                    <?= $contasreceber->valor ? number_format($contasreceber->valor, 2, ',', '.') : '0,00' ?>
                                                </td>
                                                <td align="center" name="checkbox">
                                                    <?= $this->Form->input('', ['type' => 'checkbox', 'id' => $contasreceber->id, 'valor' => $contasreceber->valor])?>
                                                </td>
                                                <td class="actions" style="white-space:nowrap">
                                                    <div class="no-padding dropdown">
                                                        <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                        <ul class="dropdown-menu dropdown-menu-left">
                                                            <li>
                                                                <?= $this->Html->link('Visualizar', ['action' => 'view', $contasreceber->id], ['target' => '_blank']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Editar', ['action' => 'edit', $contasreceber->id], ['target' => '_blank']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Editar Rateio', ['']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Anexos', ['']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Excluir', '#Javascript:void(0)', ['onclick' => 'Deletar(\'FinContasReceber\',' . $contasreceber->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'listen' => 'f']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Vinculos', ['']) ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <!-- <div style="text-align:center">
                                        <div class="paginator">
                                            <ul class="pagination">
                                                <?php if ($this->Paginator->numbers()): ?>
                                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                                <?php endif; ?>
                                                <?= $this->Paginator->numbers() ?>
                                                <?php if ($this->Paginator->numbers()): ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                                <?php endif; ?>
                                                <p>
                                                    <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                                </p>
                                            </ul>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('controllers/Contasreceber', ['block' => 'scriptBottom']);
?>