
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Contas Receber</h2>
        <ol class="breadcrumb">
            <li>Fin Contas Receber</li>
            <li class="active">
                <strong>Litagem de Fin Contas Receber</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Contas Receber</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Fin Plano Conta') ?></th>
                                                                <td><?= $finContasReceber->has('fin_plano_conta') ? $this->Html->link($finContasReceber->fin_plano_conta->nome, ['controller' => 'FinPlanoContas', 'action' => 'view', $finContasReceber->fin_plano_conta->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Cliente') ?></th>
                                                                <td><?= $finContasReceber->has('cliente') ? $this->Html->link($finContasReceber->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $finContasReceber->cliente->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Contabilidade') ?></th>
                                                                <td><?= $finContasReceber->has('fin_contabilidade') ? $this->Html->link($finContasReceber->fin_contabilidade->nome, ['controller' => 'FinContabilidades', 'action' => 'view', $finContasReceber->fin_contabilidade->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Documento') ?></th>
                                <td><?= h($finContasReceber->documento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numdoc') ?></th>
                                <td><?= h($finContasReceber->numdoc) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Parcela') ?></th>
                                <td><?= h($finContasReceber->parcela) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Boleto Mensagem Livre') ?></th>
                                <td><?= h($finContasReceber->boleto_mensagem_livre) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Boleto Barras') ?></th>
                                <td><?= h($finContasReceber->boleto_barras) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Boletolinhadigitavel') ?></th>
                                <td><?= h($finContasReceber->boletolinhadigitavel) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Boleto Nosso Numero') ?></th>
                                <td><?= h($finContasReceber->boleto_nosso_numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero Fiscal') ?></th>
                                <td><?= h($finContasReceber->numero_fiscal) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Tipo Pagamento') ?></th>
                                                                <td><?= $finContasReceber->has('fin_tipo_pagamento') ? $this->Html->link($finContasReceber->fin_tipo_pagamento->id, ['controller' => 'FinTipoPagamento', 'action' => 'view', $finContasReceber->fin_tipo_pagamento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Tipo Documento') ?></th>
                                                                <td><?= $finContasReceber->has('fin_tipo_documento') ? $this->Html->link($finContasReceber->fin_tipo_documento->id, ['controller' => 'FinTipoDocumento', 'action' => 'view', $finContasReceber->fin_tipo_documento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero Documento') ?></th>
                                <td><?= h($finContasReceber->numero_documento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Contas Receber Planejamento') ?></th>
                                                                <td><?= $finContasReceber->has('fin_contas_receber_planejamento') ? $this->Html->link($finContasReceber->fin_contas_receber_planejamento->id, ['controller' => 'FinContasReceberPlanejamentos', 'action' => 'view', $finContasReceber->fin_contas_receber_planejamento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Impresso') ?></th>
                                <td><?= h($finContasReceber->impresso) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finContasReceber->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor') ?></th>
                                <td><?= $this->Number->format($finContasReceber->valor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $this->Number->format($finContasReceber->status) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Forma Pagamento') ?></th>
                                <td><?= $this->Number->format($finContasReceber->forma_pagamento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Boleto Mensagem') ?></th>
                                <td><?= $this->Number->format($finContasReceber->boleto_mensagem) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Juros') ?></th>
                                <td><?= $this->Number->format($finContasReceber->juros) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Multa') ?></th>
                                <td><?= $this->Number->format($finContasReceber->multa) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Boleto Cedente') ?></th>
                                <td><?= $this->Number->format($finContasReceber->boleto_cedente) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Bruto') ?></th>
                                <td><?= $this->Number->format($finContasReceber->valor_bruto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Desconto') ?></th>
                                <td><?= $this->Number->format($finContasReceber->desconto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Correcao Monetaria') ?></th>
                                <td><?= $this->Number->format($finContasReceber->correcao_monetaria) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('ValorPagar') ?></th>
                                <td><?= $this->Number->format($finContasReceber->valorPagar) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Perjuros') ?></th>
                                <td><?= $this->Number->format($finContasReceber->perjuros) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Permulta') ?></th>
                                <td><?= $this->Number->format($finContasReceber->permulta) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Sis Antigo Cx Boleto') ?></th>
                                <td><?= $this->Number->format($finContasReceber->sis_antigo_cx_boleto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Sis Antigo Cx') ?></th>
                                <td><?= $this->Number->format($finContasReceber->sis_antigo_cx) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Extern Id') ?></th>
                                <td><?= $this->Number->format($finContasReceber->extern_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Created') ?></th>
                                <td><?= $this->Number->format($finContasReceber->user_created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Modified') ?></th>
                                <td><?= $this->Number->format($finContasReceber->user_modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Controle Financeiro Id') ?></th>
                                <td><?= $this->Number->format($finContasReceber->controle_financeiro_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Origem Registro') ?></th>
                                <td><?= $this->Number->format($finContasReceber->origem_registro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Fatura Id') ?></th>
                                <td><?= $this->Number->format($finContasReceber->fatura_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($finContasReceber->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Pagamento') ?></th>
                                                                <td><?= h($finContasReceber->data_pagamento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Vencimento') ?></th>
                                                                <td><?= h($finContasReceber->data_vencimento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Fiscal') ?></th>
                                                                <td><?= h($finContasReceber->data_fiscal) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Vencimento Boleto') ?></th>
                                                                <td><?= h($finContasReceber->vencimento_boleto) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Cadastro') ?></th>
                                                                <td><?= h($finContasReceber->data_cadastro) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($finContasReceber->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($finContasReceber->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Complemento') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($finContasReceber->complemento)); ?>
                </div>
            </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Abatimentos Contas Receber') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContasReceber->fin_abatimentos_contas_receber)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Abatimento Id') ?></th>
                        <th><?= __('Fin Contas Receber Id') ?></th>
                        <th><?= __('Percentual') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContasReceber->fin_abatimentos_contas_receber as $finAbatimentosContasReceber): ?>
                <tr>
                    <td><?= h($finAbatimentosContasReceber->id) ?></td>
                    <td><?= h($finAbatimentosContasReceber->fin_abatimento_id) ?></td>
                    <td><?= h($finAbatimentosContasReceber->fin_contas_receber_id) ?></td>
                    <td><?= h($finAbatimentosContasReceber->percentual) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinAbatimentosContasReceber','action' => 'view', $finAbatimentosContasReceber->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinAbatimentosContasReceber','action' => 'edit', $finAbatimentosContasReceber->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinAbatimentosContasReceber','action' => 'delete', $finAbatimentosContasReceber->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContasReceber->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


