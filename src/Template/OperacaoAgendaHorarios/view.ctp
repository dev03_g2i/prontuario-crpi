
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Operacao Agenda Horarios</h2>
        <ol class="breadcrumb">
            <li>Operacao Agenda Horarios</li>
            <li class="active">
                <strong>Litagem de Operacao Agenda Horarios</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Operacao Agenda Horarios</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($operacaoAgendaHorario->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $operacaoAgendaHorario->has('situacao_cadastro') ? $this->Html->link($operacaoAgendaHorario->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $operacaoAgendaHorario->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $operacaoAgendaHorario->has('user') ? $this->Html->link($operacaoAgendaHorario->user->nome, ['controller' => 'Users', 'action' => 'view', $operacaoAgendaHorario->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($operacaoAgendaHorario->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($operacaoAgendaHorario->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($operacaoAgendaHorario->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Grupo Agenda Horarios') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($operacaoAgendaHorario->grupo_agenda_horarios)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Dia Semana') ?></th>
                        <th><?= __('Data Inicio') ?></th>
                        <th><?= __('Data Fim') ?></th>
                        <th><?= __('Hora Inicio') ?></th>
                        <th><?= __('Hora Fim') ?></th>
                        <th><?= __('Intervalo') ?></th>
                        <th><?= __('Grupo Id') ?></th>
                        <th><?= __('Tipo Agenda Id') ?></th>
                        <th><?= __('Situacao Id') ?></th>
                        <th><?= __('User Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th><?= __('Operacao Agenda Horario Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($operacaoAgendaHorario->grupo_agenda_horarios as $grupoAgendaHorarios): ?>
                <tr>
                    <td><?= h($grupoAgendaHorarios->id) ?></td>
                    <td><?= h($grupoAgendaHorarios->dia_semana) ?></td>
                    <td><?= h($grupoAgendaHorarios->data_inicio) ?></td>
                    <td><?= h($grupoAgendaHorarios->data_fim) ?></td>
                    <td><?= h($grupoAgendaHorarios->hora_inicio) ?></td>
                    <td><?= h($grupoAgendaHorarios->hora_fim) ?></td>
                    <td><?= h($grupoAgendaHorarios->intervalo) ?></td>
                    <td><?= h($grupoAgendaHorarios->grupo_id) ?></td>
                    <td><?= h($grupoAgendaHorarios->tipo_agenda_id) ?></td>
                    <td><?= h($grupoAgendaHorarios->situacao_id) ?></td>
                    <td><?= h($grupoAgendaHorarios->user_id) ?></td>
                    <td><?= h($grupoAgendaHorarios->created) ?></td>
                    <td><?= h($grupoAgendaHorarios->modified) ?></td>
                    <td><?= h($grupoAgendaHorarios->operacao_agenda_horario_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'GrupoAgendaHorarios','action' => 'view', $grupoAgendaHorarios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'GrupoAgendaHorarios','action' => 'edit', $grupoAgendaHorarios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'GrupoAgendaHorarios','action' => 'delete', $grupoAgendaHorarios->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $operacaoAgendaHorario->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


