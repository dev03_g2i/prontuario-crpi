
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Atendimento Procedimentos</li>
            <li class="active">
                <strong>Listagem de Atendimento Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Atendimento Procedimentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Atendimento Procedimentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Atendimento Procedimentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                <th><?= $this->Paginator->sort('atendimento_id') ?></th>
                <th><?= $this->Paginator->sort('valor_fatura') ?></th>
                <th><?= $this->Paginator->sort('quantidade') ?></th>
                <th><?= $this->Paginator->sort('desconto') ?></th>
                <th><?= $this->Paginator->sort('porc_desconto', ['label' => 'Porcentagem Desconto']) ?></th>
                <th><?= $this->Paginator->sort('valor_caixa') ?></th>
                <th><?= $this->Paginator->sort('material') ?></th>
                <th><?= $this->Paginator->sort('num_controle', ['label' => 'Número Controle']) ?></th>
                <th><?= $this->Paginator->sort('medico_id', ['label' => 'Médico']) ?></th>
                <th><?= $this->Paginator->sort('valor_matmed') ?></th>
                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($atendimentoProcedimentos as $atendimentoProcedimento): ?>
            <tr>
                <td><?= $this->Number->format($atendimentoProcedimento->id) ?></td>
                <td><?= $atendimentoProcedimento->has('procedimento') ? $this->Html->link($atendimentoProcedimento->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $atendimentoProcedimento->procedimento->id]) : '' ?></td>
                <td><?= $atendimentoProcedimento->has('atendimento') ? $this->Html->link($atendimentoProcedimento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $atendimentoProcedimento->atendimento->id]) : '' ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->valor_fatura) ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->quantidade) ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->desconto) ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->porc_desconto) ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->valor_caixa) ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->material) ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->num_controle) ?></td>
                <td><?= $atendimentoProcedimento->has('medico_responsavei') ? $this->Html->link($atendimentoProcedimento->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $atendimentoProcedimento->medico_responsavei->id]) : '' ?></td>
                <td><?= $this->Number->format($atendimentoProcedimento->valor_matmed) ?></td>
                <td><?= $atendimentoProcedimento->has('situacao_cadastro') ? $this->Html->link($atendimentoProcedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $atendimentoProcedimento->situacao_cadastro->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $atendimentoProcedimento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $atendimentoProcedimento->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $atendimentoProcedimento->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

