<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Procedimentos</h2>
        <!--<ol class="breadcrumb">
            <li>Atendimento Procedimentos</li>
            <li class="active">
                <strong> Cadastrar Atendimento Procedimentos
                </strong>
            </li>
        </ol>-->
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="atendimentoProcedimentos form">
                        <?= $this->Form->create($atendimentoProcedimento, ["id" => "form_atendimento"]) ?>
                        <fieldset>

                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'empty' => 'Selecione']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('convenio_id', ['options' => $Convenios, 'empty' => 'Selecione', 'id' => 'convenio', 'default' => $Atendimentos->convenio_id]);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('procedimento_id', ['verifica-preco' => $Atendimentos->convenio_id, 'data' => 'select', 'controller' => 'Procedimentos', 'action' => 'fill']);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('complemento', ['type' => 'text']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('controle', ['type' => 'text']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('quantidade', ['type' => 'number','value'=>1]);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo "<label>Código</label>";
                            echo "<div id='show-codigo'></div>";
                            echo "</div>";

                            echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $Atendimentos->id]);
                            echo $this->Form->input('digitado', ['type' => 'hidden', 'value' => 'Sim']);
                            echo $this->Form->input('documento_guia', ['type' => 'hidden']);
                            echo $this->Form->input('porc_desconto', ['type' => 'hidden']);
                            echo $this->Form->input('material', ['type' => 'hidden']);
                            echo $this->Form->input('codigo', ['label' => 'Código', 'id' => 'input-codigo', 'type' => 'hidden']);

                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'SalvarPrev()', 'type' => 'button', 'id' => 'btn-editar']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    /*  $(function(){
     $("#procedimento-id").change(function () {
     if ($("#convenio").val() == "") {
     BootstrapDialog.alert('Selecione um convênio!');
     $(this).val('');
     return false;
     }
     getValoresProcedimentos($("#convenio").val(),$(this).val());
     })
     });*/
</script>


