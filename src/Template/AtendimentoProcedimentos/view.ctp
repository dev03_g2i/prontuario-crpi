

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Atendimento Procedimentos</li>
            <li class="active">
                <strong>Listagem de Atendimento Procedimentos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="atendimentoProcedimentos">
    <h3><?= h($atendimentoProcedimento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Procedimento') ?></th>
            <td><?= $atendimentoProcedimento->has('procedimento') ? $this->Html->link($atendimentoProcedimento->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $atendimentoProcedimento->procedimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Atendimento') ?></th>
            <td><?= $atendimentoProcedimento->has('atendimento') ? $this->Html->link($atendimentoProcedimento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $atendimentoProcedimento->atendimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Medico Responsavel') ?></th>
            <td><?= $atendimentoProcedimento->has('medico_responsavei') ? $this->Html->link($atendimentoProcedimento->medico_responsavei->id, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $atendimentoProcedimento->medico_responsavei->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situação Cadastro') ?></th>
            <td><?= $atendimentoProcedimento->has('situacao_cadastro') ? $this->Html->link($atendimentoProcedimento->situacao_cadastro->id, ['controller' => 'SituacaoCadastros', 'action' => 'view', $atendimentoProcedimento->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Usuário') ?></th>
            <td><?= $atendimentoProcedimento->has('user') ? $this->Html->link($atendimentoProcedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $atendimentoProcedimento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Codigo') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Valor Fatura') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->valor_fatura) ?></td>
        </tr>
        <tr>
            <th><?= __('Quantidade') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->quantidade) ?></td>
        </tr>
        <tr>
            <th><?= __('Desconto') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->desconto) ?></td>
        </tr>
        <tr>
            <th><?= __('Porcentagem Desconto') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->porc_desconto) ?></td>
        </tr>
        <tr>
            <th><?= __('Valor Caixa') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->valor_caixa) ?></td>
        </tr>
        <tr>
            <th><?= __('Material') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->material) ?></td>
        </tr>
        <tr>
            <th><?= __('Num Controle') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->num_controle) ?></td>
        </tr>
        <tr>
            <th><?= __('Valor Matmed') ?></th>
            <td><?= $this->Number->format($atendimentoProcedimento->valor_matmed) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($atendimentoProcedimento->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($atendimentoProcedimento->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Digitado') ?></h4>
        <?= $this->Text->autoParagraph(h($atendimentoProcedimento->digitado)); ?>
    </div>
    <div class="row">
        <h4><?= __('Documento Guia') ?></h4>
        <?= $this->Text->autoParagraph(h($atendimentoProcedimento->documento_guia)); ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

