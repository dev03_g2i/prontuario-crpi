<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Pagamento Produção</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form">
                            <?=$this->Form->create($atendProc)?>

                            <div class="col-md-4">
                                <?=$this->Form->input('producao_pg_dt', ['label' => 'Data', 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($atendProc->producao_pg_dt, 'dd/MM/YYYY'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]])?>
                            </div>
                            <div class="col-md-4">
                                <?=$this->Form->input('producao_pg_user', ['label' => 'Usuário', 'options' => $users, 'default' => $atendProc->producao_pg_user])?>
                            </div>
                            <div class="col-md-4">
                                <?=$this->Form->input('producao_pg_dtuser', ['label' => 'Dt.Usuário', 'type' => 'text', 'class' => 'datetimepicker', 'value' => $this->Time->format($atendProc->producao_pg_dtuser, 'dd/MM/YYYY HH:mm'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]])?>
                            </div>
                            <div class="col-md-12 text-right">
                                <?=$this->Form->button("<i class='fa fa-save'></i> Salvar", ['type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false])?>
                            </div>

                            <div class="clearfix"></div>
                            <?=$this->Form->create()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>