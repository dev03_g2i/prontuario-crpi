<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Faturamento</li>
            <li class="active">
                <strong> Editar</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="atendimentoProcedimentos form">
                        <?= $this->Form->create($atendimentoProcedimento) ?>

                        <fieldset>
                            <legend><?= __('Faturamento') ?></legend>

                            <div class='col-md-6'>
                                <?=  $this->Form->input('nr_guia', ['label'=>'Número guia']); ?>
                            </div>


                            <div class='col-md-6'>
                                <?=  $this->Form->input('dt_emissao_guia', ['type'=>'text','class'=>'datetimepicker','label'=>'Data emissão Guia','value'=>$this->Time->format($atendimentoProcedimento->dt_emissao_guia,'dd/MM/Y HH:mm')]); ?>
                            </div>

                            <div class='col-md-6'>
                                <?=  $this->Form->input('autorizacao_senha', ['label'=>'Autorização/Senha ']); ?>
                            </div>

                            <div class='col-md-6'>
                                <?=  $this->Form->input('dt_autorizacao', ['type'=>'text','class'=>'datetimepicker','label'=>'Data Autorização','value'=>$this->Time->format($atendimentoProcedimento->dt_autorizacao,'dd/MM/Y HH:mm')]); ?>
                            </div>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

