<div class="arts">
    <div class="table-responsive">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Procedimento</th>
                <th>Situação Fatura</th>
                <th>Código</th>
                <th>Caixa</th>
                <th>Desconto R$</th>
                <th>Profissional</th>
                <th>Complemento</th>
                <th class="<?= $this->Configuracao->showControleFinanceiroOnAtendimentos() ?>">Controle</th>
                <th>Qtd</th>
                <th class="<?= $this->Configuracao->showPrevEntrega() ?>">Prev.Entrega</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $valor = 0;
            $val_base = 0;
            $options = ['places' => 2, 'before' => 'R$ '];
            foreach ($AtendimentoProcedimentos as $atendproc):
                $valor += $atendproc->valor_caixa;
                $val_base += $atendproc->valor_fatura;
                ?>
                <tr class="<?= $atendproc->regra ?>">
                    <td><?= $atendproc->procedimento->nome ?></td>
                    <td class="situacao-fatura"><?= ($atendproc->status_faturamento == 1) ? 'Faturado' : '' ?></td>
                    <td><?= !empty($atendproc->codigo) ? $atendproc->codigo : 'Não encontrado' ?></td>
                    <td><?php echo $this->Number->format($atendproc->valor_caixa, $options); ?></td>
                    <td><?php echo $this->Number->format($atendproc->desconto, $options); ?></td>
                    <td><?= $atendproc->has('medico_responsavei') ? $atendproc->medico_responsavei->nome : '' ?></td>
                    <td><?= $atendproc->complemento ?></td>
                    <td class="<?= $this->Configuracao->showControleFinanceiroOnAtendimentos() ?>">
                        <?= $atendproc->controle_financeiro->descricao ?></td>
                    <td><?php echo $atendproc->quantidade; ?></td>
                    <td class="<?= $this->Configuracao->showPrevEntrega() ?>"><?= $this->Time->format($atendproc->prev_entrega_proc, 'dd/MM/YYYY HH:mm'); ?></td>
                    <td class="actions">
                        <div class="col-md-8 pull-left">
                            <div class="dropdown">
                                <button class="btn btn-info btn-xs dropdown-toggle" type="button"
                                        data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span
                                            class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-prontuario">
                                    <li>
                                        <?php
                                        if ($atendproc->status_faturamento != 1): // Mostrar botão de editar somente se não foi faturado
                                            echo $this->Html->link('<i class="fa fa-edit"></i> Editar', 'javascript:void(0)', ['onclick' => 'editProcedimento(' . $atendproc->id . ',\'edit\')', 'escape' => false, 'listen' => 'f']);
                                        endif;
                                        ?>
                                    </li>
                                    <li class="<?=$this->Configuracao->usaEquipe(); ?>">
                                        <?= $this->Html->link('<i class="fa fa-users"></i> Equipe', ['controller' => 'AtendimentoProcedimentoEquipes', 'action' => 'index', $atendproc->id], ['escape' => false, 'target' => '_blank']); ?>
                                    </li>
                                    <li>
                                        <?php
                                        if (!empty($atendproc->situacao_recebimento_id)) {
                                            echo $this->Html->link('<i class="fa fa-file-text-o"></i> Faturamento', ['controller' => 'AtendimentoProcedimentos', 'action' => 'situacaoRecebimento', $atendproc->id], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg', 'listen' => 'f']);
                                        }
                                        ?>
                                    </li>
                                    <li><?= $this->Html->link('<i class="fa fa-user-md"></i> Mat/Med - Fatura', ['controller' => 'FaturaMatmed', 'action' => 'index', $atendproc->atendimento_id], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 pull-right">
                            <?php
                            if ($atendproc->status_faturamento != 1): // Mostrar botão de remover somente se não foi faturado
                                echo $this->Html->link($this->Html->icon('remove'), 'javascript:void(0)', ['onclick' => 'Deletar(\'AtendimentoProcedimentos\',' . $atendproc->id . ',\'atualizar\',' . $atendproc->atendimento_id . ')', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Remover', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => "f"]);
                            endif;
                            ?>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <!--<div class="col-md-12">
            <p class="text-left"><strong><span class="total-caixa"></span></strong></p>
        </div>-->
    </div>
</div>
