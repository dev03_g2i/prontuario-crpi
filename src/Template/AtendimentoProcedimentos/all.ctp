<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Atendimentos</h2>
            <ol class="breadcrumb">
                <li>Atendimentos</li>
                <li class="active">
                    <strong>Litagem de Atendimentos</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area_pront">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <?= $this->Form->create('Atendimentos',['type'=>'get']) ?>
                            <div class="col-md-4">
                                <?= $this->Form->input('convenio_id',['type'=>'select','empty'=>'Selecione','options'=>$convenio]);?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->input('tipoatendimento_id',['label' => 'Tipo', 'type'=>'select','empty'=>'Selecione','options'=>$tipoatendimento]);?>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->input('data',['type'=>'text','class'=>"datepicker",'append' => [$this->Form->button($this->Html->icon("calendar"),['type'=>'button'])]]);?>
                            </div>

                            <div class="col-md-4" style="margin-top: 26px;">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'fill-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'refresh-modal','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                        <tr>
                                            <th><?= __('Data') ?></th>
                                            <th><?= __('Procedimento') ?></th>
                                            <th><?= __('Profissional') ?></th>
                                            <th><?= __('Convênio') ?></th>
                                            <th><?= __('Tipo') ?></th>
                                            <th><?= __('Solicitante') ?></th>
                                            <th><?= __('Unidade') ?></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                        <?php
                                        $valor=0;
                                        $valor_fatura=0;
                                        foreach ($atendimentos as $atendimento):
                                        if (!empty($atendimento->atendimento_procedimentos)):
                                            foreach ($atendimento->atendimento_procedimentos as $anted):
                                                $valor+=$anted->valor_caixa;
                                                $valor_fatura+=$anted->valor_fatura;
                                                ?>
                                                <tr>
                                                    <td><?= $atendimento->data?></td>
                                                    <td><?= $anted->has('procedimento') ? $anted->procedimento->nome : '' ?></td>
                                                    <td><?= $anted->has('medico_responsavei') ? $anted->medico_responsavei->nome : '' ?></td>
                                                    <td><?= $atendimento->has('convenio') ? $atendimento->convenio->nome : '' ?></td>
                                                    <td><?= $atendimento->has('tipo_atendimento') ? $atendimento->tipo_atendimento->nome : '' ?></td>
                                                    <td><?= $atendimento->has('solicitante') ? $atendimento->solicitante->nome : '' ?></td>
                                                    <td><?= $atendimento->has('unidade') ? $atendimento->unidade->nome : '' ?></td>
                                                    <td class="actions">
                                                        <?= $this->Html->link('<i class="fa fa-user-md"></i>', ['controller'=>'Atendimentos','action' => 'view',$atendimento->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Atendimentos', 'escape' => false, 'class' => 'btn btn-xs btn-default','target'=>'_blanck','listen'=>'f']) ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; endif; endforeach; ?>

                                    </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

