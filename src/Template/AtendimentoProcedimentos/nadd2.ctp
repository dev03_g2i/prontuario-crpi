<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Atendimento Procedimentos</li>
            <li class="active">
                <strong> Cadastrar Atendimento Procedimentos
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="atendimentoProcedimentos form">
                <?= $this->Form->create($atendimentoProcedimento, ["id" => "form_atendimento"]) ?>

                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"> Procedimentos</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2">Tiss</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3">Cobrança</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">

                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'empty' => 'Selecione']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'Selecione', 'id' => 'convenio-id']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('procedimentos', ['type'=>'select','data' => 'select','controller'=>'Procedimentos', 'action'=>'fill', 'empty'=>'Selecione','label'=>'Procedimentos/Código']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('complemento', ['type' => 'text']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('controle', ['type' => 'text']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('quantidade', ['type' => 'number','value'=>1]);
                                echo "</div>";

                                /*echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $atendimentos->id]);
                                echo $this->Form->input('digitado', ['type' => 'hidden', 'value' => 'Sim']);
                                echo $this->Form->input('documento_guia', ['type' => 'hidden']);
                                echo $this->Form->input('porc_desconto', ['type' => 'hidden']);
                                echo $this->Form->input('material', ['type' => 'hidden']);*/
                                ?>

                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <?php //echo $this->Form->input('senha')?>
                                </div>
                                <div class="col-md-4">
                                    <?php //echo $this->Form->input('dt_autorizacao', ['type' => 'text', 'class' => 'datepicker'])?>
                                </div>
                                <div class="col-md-4">
                                    <?php //echo $this->Form->input('guia', ['rows' => 2])?>
                                </div>
                            </div>
                        </div>

                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">

                            </div>
                        </div>

                    </div>


                </div>

                <div class="col-md-12 text-right" style="margin-top: 15px">
                    <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'SalvarPrev()', 'type' => 'button', 'id' => 'btn-editar']) ?>
                </div>
                <div class="clearfix"></div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<script>

    /*  $(function(){
     $("#procedimento-id").change(function () {
     if ($("#convenio").val() == "") {
     BootstrapDialog.alert('Selecione um convênio!');
     $(this).val('');
     return false;
     }
     getValoresProcedimentos($("#convenio").val(),$(this).val());
     })
     });*/
</script>

