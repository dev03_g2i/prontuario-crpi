<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Atendimento Procedimentos</li>
            <li class="active">
                <strong> Editar Atendimento Procedimentos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="atendimentoProcedimentos form">
                        <?= $this->Form->create($atendimentoProcedimento,["id"=>"form_atendimento"]) ?>
                        <fieldset>

                            <?php
                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'empty' => 'Selecione']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('convenio_id', ['options' => $Convenios, 'empty' => 'Selecione','id'=>'convenio']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('procedimento_id', ['options' => $procedimentos, 'empty' => 'Selecione']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_fatura');
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('desconto',["id"=>"desconto-id"]);
                            echo "</div>";


                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_caixa');
                            echo "</div>";


                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('num_controle');
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_matmed',["label"=>"Valor Material Médico"]);
                            echo "</div>";

                            echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $Atendimentos->id]);
                            echo $this->Form->input('quantidade', ['type' => 'hidden', 'value' => 1]);
                            echo $this->Form->input('digitado', ['type' => 'hidden', 'value' => 'Sim']);
                            echo $this->Form->input('documento_guia', ['type' => 'hidden']);
                            echo $this->Form->input('porc_desconto', ['type' => 'hidden']);
                            echo $this->Form->input('material', ['type' => 'hidden']);


                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary','onclick'=>'EnviarFormulario(\'form_atendimento\',\'atualizar\')']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $("#procedimento-id").change(function () {

            if ($("#convenio").val() == "") {
                BootstrapDialog.alert('Selecione um convênio!');
                $(this).val('');
                return false;
            }
            getValoresProcedimentos($("#convenio").val(),$(this).val());
        })
    });
</script>


