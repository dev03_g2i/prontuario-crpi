<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Procedimentos</h2>
        <ol class="breadcrumb">
            <li>Cobrança</li>
            <li class="active">
                <strong>Editar</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="atendimentoProcedimentos form">
                        <?= $this->Form->create($atendimentoProcedimento) ?>
                        <fieldset>
                            <legend><?= __('Editar Cobrança') ?></legend>

                            <div class='col-md-6'>
                                <?=  $this->Form->input('percentual_cobranca', ['label'=>'Percentual Cobrança']); ?>
                            </div>


                            <div class='col-md-6'>
                                <?=  $this->Form->input('apurar_id', ['options'=>$apurar,'empty'=>'selecione']); ?>
                            </div>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

