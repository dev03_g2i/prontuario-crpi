<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Detalhes Fatura</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="situacaoRecebimentos form">

                            <div class="col-md-3">
                                <?php echo $this->Form->input('fatura_encerramento_id', ['label' => 'N° Fatura', 'type' => 'text', 'readonly' => true, 'value' => $atendProc->fatura_encerramento_id])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('situacao_recebimento_id', ['type' => 'text', 'label' => 'Situação', 'readonly' => true, 'value' => ($atendProc->has('situacao_recebimento'))?$atendProc->situacao_recebimento->nome:''])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('dt_recebimento', ['label' => 'Data Recebimento','type' => 'text', 'readonly' => true, 'value' => $this->Time->format($atendProc->dt_recebimento, 'dd/MM/YYYY')])?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->input('valor_recebido', ['label' => 'Valor Recebido','prepend' => 'R$', 'type' => 'text', 'readonly' => true, 'value' => number_format($atendProc->valor_recebido, 2, '.', '')])?>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>