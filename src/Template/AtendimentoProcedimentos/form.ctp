<?php $options = ['places' => 2];
echo $this->Form->create(null, ['id' => 'form_procedimento']) ?>
    <div>
        <h3>Procedimento</h3>
        <section>
            <div class='col-md-6'>
                <?= $this->Form->input('procedimento_id', ['required' => true, 'verifica-preco' => $convenio, 'type' => 'select', 'data' => 'select', 'controller' => 'Procedimentos', 'action' => 'fill', 'empty' => 'Selecione', 'label' => 'Procedimentos/Código', 'data-value' => @$cadastro_procedimento['procedimento_id']]); ?>
            </div>
            <div class='col-md-3'>
                <?php echo $this->Form->input('codigo', ['label' => 'Código', 'readonly' => true, 'value' => @$cadastro_procedimento['codigo']]); ?>
            </div>
            <div class='col-md-3'>
                <?php echo $this->Form->input('chave_sus', ['label' => 'Chave SUS', 'type' => 'text', 'maxlength' => 20, 'value' => @$cadastro_procedimento['chave_sus']]); ?>
            </div>

            <?php if ($this->Configuracao->mostraCampoControleFinanceiroOnProcedimento()): ?>
                <div class='col-md-3'>
                    <?= $this->Form->input('controle_financeiro_id', ['type' => 'select', 'options' => @$controles, 'value' => @$cadastro_procedimento['controle_financeiro_id']]); ?>
                </div>
            <?php else: ?>
                <?= $this->Form->input('controle_financeiro_id', ['type' => 'hidden', 'value' => !empty($cadastro_procedimento['controle_financeiro_id']) ? $cadastro_procedimento['controle_financeiro_id'] : @$controles]); ?>
            <?php endif; ?>

            <div class='col-md-6'>
                <?= $this->Form->input('medico_id', ['required' => true, 'type' => 'select', 'data' => 'select', 'controller' => 'MedicoResponsaveis', 'action' => 'fill', 'empty' => 'Selecione', 'data-value' => @$cadastro_procedimento['medico_id']]); ?>
            </div>

            <?php if ($this->Configuracao->showSolicitante() == false && $this->Configuracao->mostraProcedimentoComplemento()): ?>
                <div class='col-md-6'>
                    <?= $this->Form->input('complemento', ['label' => 'Detalhe/Complemento', 'type' => 'select', 'class' => 'select2', 'options' => $complementos, 'empty' => 'Selecione', 'default' => @$cadastro_procedimento['complemento']]); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-6 <?= ($this->Configuracao->showSolicitante() == false) ? 'hidden' : '' ?>">
                <?php echo $this->Form->input('solicitante_id', ['type' => 'select', 'data' => 'select', 'controller' => 'Solicitantes', 'action' => 'fill', 'empty' => 'Selecione', 'data-value' => @$cadastro_procedimento['solicitante_id'],
                    'append' => [
                        $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f']),
                        $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Solicitantes', 'action' => 'view', 'target' => '_blank', 'listen' => 'f', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Dados Solicitante'])
                    ]
                ]); ?>
            </div>
            <?= ($this->Configuracao->showSolicitante() == false) ? '<div class="clearfix"></div>' : '' ?>
            <div class='col-md-3'>
                <?= $this->Form->input('valor_unitario', ['label' => 'Valor Unitário', 'readonly' => $this->Configuracao->editValorCaixa(), 'type' => 'text', 'mask' => 'money', 'value' => $this->Number->format(@$cadastro_procedimento['vl_caixa_original'], $options)]); ?>
            </div>
            <div class='col-md-2'>
                <?= $this->Form->input('quantidade', ['type' => 'number', 'value' => !empty($cadastro_procedimento['quantidade']) ? $cadastro_procedimento['quantidade'] : 1]); ?>
            </div>
            <div class='col-md-3'>
                <?= $this->Form->input('valor_total', ['readonly' => true, 'label' => 'Valor Total', 'type' => 'text', 'mask' => 'money', 'value' => $this->Number->format(@$cadastro_procedimento['valor_total'], $options)]); ?>
            </div>
            <div class='col-md-2'>
                <?= $this->Form->input('desconto', ['label' => 'Desconto R$', 'readonly' => $this->Configuracao->editDesconto(), 'type' => 'text', 'mask' => 'money', 'value' => !empty(@$cadastro_procedimento['desconto']) ? $this->Number->format(@$cadastro_procedimento['desconto'], $options) : '0.00',
                    'append' => [
                        $this->Form->button('<i class="fa fa-check-circle"></i>', ['id' => 'aplicar-desconto', 'toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Aplicar Desconto', 'type' => 'button'])
                    ]]); ?>
            </div>
            <div class='col-md-2'>
                <?= $this->Form->input('total_liquido', ['label' => 'Total liquido', 'readonly' => true, 'type' => 'text', 'mask' => 'money', 'value' => $this->Number->format(@$cadastro_procedimento['valor_caixa'], $options)]); ?>
            </div>
            <?php if ($this->Configuracao->showSolicitante() == true && $this->Configuracao->mostraProcedimentoComplemento()): ?>
                <div class='col-md-12'>
                <?= $this->Form->input('complemento', ['label' => 'Detalhe/Complemento', 'type' => 'select', 'class' => 'select2', 'options' => $complementos, 'default' => @$cadastro_procedimento['complemento']]); ?>
                </div>
            <?php endif; ?>
        </section>

        <?php if ($this->Convenio->usaTiss($convenio)): ?>
            <h3>Tiss</h3>
            <section>
                <div class='col-md-6'>
                    <?= $this->Form->input('nr_guia', ['required' => $this->Convenio->requiredNumeroGuia($convenio), 'value' => @$cadastro_procedimento['nr_guia']]); ?>
                </div>
                <div class='col-md-6'>
                    <?= $this->Form->input('dt_emissao_guia', ['class' => 'datetimepicker', 'value' => $this->Time->format(@$cadastro_procedimento['dt_emissao_guia'], 'dd/MM/YYYY HH:mm')]); ?>
                </div>
                <div class='col-md-6'>
                    <?= $this->Form->input('autorizacao_senha', ['required' => $this->Convenio->requiredAutorizacaoSenha($convenio), 'value' => @$cadastro_procedimento['autorizacao_senha']]); ?>
                </div>
                <div class='col-md-6'>
                    <?= $this->Form->input('dt_autorizacao', ['class' => 'datetimepicker', 'value' => $this->Time->format(@$cadastro_procedimento['dt_autorizacao'], 'dd/MM/YYYY HH:mm')]); ?>
                </div>
            </section>
        <?php endif; ?>

        <?php if ($this->Convenio->usaCobranca($convenio)): ?>
            <h3>Cobrança</h3>
            <section>
                <div class='col-md-6'>
                    <?= $this->Form->input('percentual_cobranca', ['value' => @$cadastro_procedimento['percentual_cobranca']]); ?>
                </div>
                <div class='col-md-6'>
                    <?= $this->Form->input('apurar_id', ['options' => $apurar, 'default' => @$cadastro_procedimento['apurar_id'], 'empty' => 'selecione']); ?>
                </div>
            </section>
        <?php endif; ?>

        <?php if ($this->Configuracao->usaDiagnostico()): ?>
            <h3>Diagnóstico</h3>
            <section>
                <div class='col-md-6'>
                    <?= $this->Form->input('prev_entrega_proc', ['class' => 'datetimepicker', 'value' => $this->Time->format(@$cadastro_procedimento['prev_entrega_proc'], 'dd/MM/YYYY HH:mm')]); ?>
                </div>
            </section>
        <?php endif; ?>

        <?php
        /* Esses campos são essencias para o calculo dos valores dos procedimentos */
        echo $this->Form->input('action', ['type' => 'hidden', 'value' => @$action]);
        echo $this->Form->input('regra', ['type' => 'hidden', 'id' => 'precoprocedimento-regra', 'value' => @$cadastro_procedimento['regra']]);
        echo $this->Form->input('valor_caixa', ['type' => 'hidden', 'value' => @$cadastro_procedimento['valor_caixa']]);
        echo $this->Form->input('valor_base', ['type' => 'hidden', 'value' => @$cadastro_procedimento['valor_base']]);
        echo $this->Form->input('valor_fatura', ['type' => 'hidden', 'value' => @$cadastro_procedimento['valor_fatura']]);
        echo $this->Form->input('vl_caixa_original', ['type' => 'hidden', 'value' => @$cadastro_procedimento['vl_caixa_original']]);
        echo $this->Form->input('atendimento_id', ['type' => 'hidden', 'value' => $atendimento_id]);
        echo $this->Form->input('convenio_id', ['type' => 'hidden', 'value' => !empty($cadastro_procedimento['convenio_id']) ? $cadastro_procedimento['convenio_id'] : $convenio]);
        echo $this->Form->input('ordenacao', ['type' => 'hidden', 'value' => @$cadastro_procedimento['ordenacao']]);
        echo $this->Form->input('filme_reais', ['type' => 'hidden', 'value' => @$cadastro_procedimento['filme_reais']]);
        echo $this->Form->input('uco', ['type' => 'hidden', 'value' => @$cadastro_procedimento['uco']]);
        echo $this->Form->input('porte', ['type' => 'hidden', 'value' => @$cadastro_procedimento['porte']]);
        echo $this->Form->input('filme', ['type' => 'hidden', 'value' => @$cadastro_procedimento['filme']]);
        if (isset($pos)) {
            echo $this->Form->input('pos', ['type' => 'hidden', 'value' => isset($pos) ? $pos : '']);
        }
        ?>

    </div>
<?= $this->Form->end() ?>