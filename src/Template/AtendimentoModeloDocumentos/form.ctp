<div class='col-md-12'>
    <?= $this->Form->input('nome'); ?>
</div>
<div class="col-md-12">
    <label>
        Para que seja substituído as informações corretamente nos contratos, utilize estas variáveis:
    </label>
    <div class="panel panel-default">
        <div class="panel-heading">Dados Paciente, Dados Responsavel - Financeiro (tabela de responsavel) e Dados Responsavel - Acompanhante (tabela
            de responsavel)</div>
        <div class="panel-body">
            {nome_paciente}, {cpf}, {rua}, {numero}, {cidade}, {estado}, {bairro}, {complemento}, {num_prontuario}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">Dados Atendimento</div>
        <div class="panel-body">
            {num_atendimento}, {dt_atendimento}, {hora_atendimento}, {convenio_nome}, {tipo_atendimento}, {nome_solicitante}, {usuario_responsavel},
            {total_pagar}, {dia_atendimento}, {nome_mes_atendimento}, {num_mes_atendimento}, {ano_atendimento}, {data_doc}, {nome_mes_doc}, {num_mes_doc}, {ano_doc}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">Dados Internação</div>
        <div class="panel-body">
            {internacao_acomodacao}, {internacao_previa}, {internacao_data}, {internacao_dthora}, {internacao_alta}, {internacao_alta_dthora}
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">Procedimentos</div>
        <div class="panel-body">
            <!--{procdimento_id}, {procedimento_descricao}, {procedimento_codigo}, {procedimento_valorfat}, {procedimento_valorcaixa}, {procedimento_valmatmed}-->
            {tabela_cod_desc}, {tabela_cod_desc_valor_fat}, {tabela_todos_valor_caixa}, {tabela_todos_valor_caixa_matmed}
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading">Dados Financeiro</div>
        <div class="panel-body">
            <!--{valor}, {parcela}, {numero_parcela}, {vencimento}, {tipo_documento}, {forma_pagamento}-->
                {tabela_financeiro}<br><br>
        </div>
    </div>
</div>
<div class='col-md-12'>
    <?=$this->Form->input('descricao', ['data' => 'ck']); ?>
</div>
<div class="col-md-12 text-right">
    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
</div>