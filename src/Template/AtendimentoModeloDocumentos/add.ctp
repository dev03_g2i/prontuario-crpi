<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Atendimento Modelo Documentos</h2>
            <ol class="breadcrumb">
                <li>Atendimento Modelo Documentos</li>
                <li class="active">
                    <strong>
                        Cadastrar Atendimento Modelo Documentos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="atendimentoModeloDocumentos form">
                            <?= $this->Form->create($atendimentoModeloDocumento) ?>
                            <?php require_once('form.ctp'); ?>  
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>