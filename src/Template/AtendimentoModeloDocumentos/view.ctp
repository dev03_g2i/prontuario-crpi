<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Atendimento Modelo Documentos</h2>
            <ol class="breadcrumb">
                <li>Atendimento Modelo Documentos</li>
                <li class="active">
                    <strong>Litagem de Atendimento Modelo Documentos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Atendimento Modelo Documentos</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($atendimentoModeloDocumento->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Nome') ?>
                                    </th>
                                    <td>
                                        <?= h($atendimentoModeloDocumento->nome) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoModeloDocumento->has('user') ? $this->Html->link($atendimentoModeloDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $atendimentoModeloDocumento->user->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situacao Cadastro') ?>
                                    </th>
                                    <td>
                                        <?= h($atendimentoModeloDocumento->situacao_cadastro->nome) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($atendimentoModeloDocumento->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($atendimentoModeloDocumento->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Descricao') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= nl2br($atendimentoModeloDocumento->descricao); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>