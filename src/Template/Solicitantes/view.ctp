
<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitantes</h2>
            <ol class="breadcrumb">
                <li>Solicitantes</li>
                <li class="active">
                    <strong>Litagem de Solicitantes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Solicitantes</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __( 'Id') ?></th>
                                    <td><?= $this->Number->format($solicitante->id) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Nome') ?></th>
                                    <td><?= h($solicitante->nome) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Conselho') ?></th>
                                    <td><?= h($solicitante->conselho) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Sigla') ?></th>
                                    <td><?= h($solicitante->sigla) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Cpf') ?></th>
                                    <td><?= h($solicitante->cpf) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Cep') ?></th>
                                    <td><?= h($solicitante->cep) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Endereço') ?></th>
                                    <td><?= h($solicitante->endereco) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('numero') ?></th>
                                    <td><?= h($solicitante->numero) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Bairro') ?></th>
                                    <td><?= h($solicitante->bairro) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Estado') ?></th>
                                    <td><?= h($solicitante->estado) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Cidade') ?></th>
                                    <td><?= h($solicitante->cidade) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Fone 1') ?></th>
                                    <td><?= h($solicitante->fone1) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Fone 2') ?></th>
                                    <td><?= h($solicitante->fone2) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Cbo') ?></th>
                                    <td><?= h($solicitante->solicitante_cbo->descricao) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Situação Cadastro') ?></th>
                                    <td><?= $solicitante->has('situacao_cadastro') ? $this->Html->link($solicitante->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $solicitante->situacao_cadastro->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Quem Cadastrou') ?></th>
                                    <td><?= $solicitante->has('user') ? $this->Html->link($solicitante->user->nome, ['controller' => 'Users', 'action' => 'view', $solicitante->user->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Criação') ?></th>
                                    <td><?= h($solicitante->created) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Modificação') ?></th>
                                    <td><?= h($solicitante->modified) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!--<div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3><?= __('Related Atendimentos') ?></h3>
                    </div>
                    <div class="ibox-content">
                        <?php if (!empty($solicitante->atendimentos)): ?>
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th><?= __('Id') ?></th>
                                        <th><?= __('Hora') ?></th>
                                        <th><?= __('Data') ?></th>
                                        <th><?= __('Observacao') ?></th>
                                        <th><?= __('Cliente Id') ?></th>
                                        <th><?= __('Unidade Id') ?></th>
                                        <th><?= __('Total Liquido') ?></th>
                                        <th><?= __('Desconto') ?></th>
                                        <th><?= __('Percentual Desconto') ?></th>
                                        <th><?= __('Total Geral') ?></th>
                                        <th><?= __('Convenio Id') ?></th>
                                        <th><?= __('Total Pagoato') ?></th>
                                        <th><?= __('Tipoatendimento Id') ?></th>
                                        <th><?= __('Num Ficha Externa') ?></th>
                                        <th><?= __('Situacao Id') ?></th>
                                        <th><?= __('User Id') ?></th>
                                        <th><?= __('Created') ?></th>
                                        <th><?= __('Modified') ?></th>
                                        <th><?= __('Tipo') ?></th>
                                        <th><?= __('Id Old') ?></th>
                                        <th><?= __('Acertado') ?></th>
                                        <th><?= __('Status') ?></th>
                                        <th><?= __('Finalizado') ?></th>
                                        <th><?= __('Solicitante Id') ?></th>
                                        <th class="actions"><?= __('Actions') ?></th>
                                    </tr>
                                    <?php foreach ($solicitante->atendimentos as $atendimentos): ?>
                                        <tr>
                                            <td><?= h($atendimentos->id) ?></td>
                                            <td><?= h($atendimentos->hora) ?></td>
                                            <td><?= h($atendimentos->data) ?></td>
                                            <td><?= h($atendimentos->observacao) ?></td>
                                            <td><?= h($atendimentos->cliente_id) ?></td>
                                            <td><?= h($atendimentos->unidade_id) ?></td>
                                            <td><?= h($atendimentos->total_liquido) ?></td>
                                            <td><?= h($atendimentos->desconto) ?></td>
                                            <td><?= h($atendimentos->percentual_desconto) ?></td>
                                            <td><?= h($atendimentos->total_geral) ?></td>
                                            <td><?= h($atendimentos->convenio_id) ?></td>
                                            <td><?= h($atendimentos->total_pagoato) ?></td>
                                            <td><?= h($atendimentos->tipoatendimento_id) ?></td>
                                            <td><?= h($atendimentos->num_ficha_externa) ?></td>
                                            <td><?= h($atendimentos->situacao_id) ?></td>
                                            <td><?= h($atendimentos->user_id) ?></td>
                                            <td><?= h($atendimentos->created) ?></td>
                                            <td><?= h($atendimentos->modified) ?></td>
                                            <td><?= h($atendimentos->tipo) ?></td>
                                            <td><?= h($atendimentos->id_old) ?></td>
                                            <td><?= h($atendimentos->acertado) ?></td>
                                            <td><?= h($atendimentos->status) ?></td>
                                            <td><?= h($atendimentos->finalizado) ?></td>
                                            <td><?= h($atendimentos->solicitante_id) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Atendimentos','action' => 'view', $atendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Atendimentos','action' => 'edit', $atendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Atendimentos','action' => 'delete', $atendimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $solicitante->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>-->
            </div>
        </div>
    </div>
</div>


