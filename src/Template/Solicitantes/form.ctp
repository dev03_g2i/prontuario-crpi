<div class="solicitantes form">
    <?= $this->Form->create($solicitante) ?>
    <div class='col-md-6'>
        <?php echo $this->Form->input('nome'); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('conselho'); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('sigla'); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('cpf', ['mask' => 'cpf']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('cep', ['mask' => 'cep', 'role' => 'cep']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('endereco', ['label' => 'Endereço', 'data-cep' => 'endereco']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('numero', ['label' => 'Número', 'type' => 'number']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('bairro', ['data-cep' => 'bairro']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('estado', ['data-cep' => 'uf']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('cidade', ['data-cep' => 'cidade']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('fone1', ['mask' => 'fone']); ?>
    </div>
    <div class='col-md-6'>
        <?php echo $this->Form->input('fone2', ['mask' => 'fone']); ?>
    </div>
    <div class="col-sm-6">
        <?= $this->Form->input('cbo_id', ['label' => 'Cbo','type' => 'select', 'data' => 'select', 'controller' => 'SolicitanteCbo', 'action' => 'fill', 'required', 'data-value' => !empty($solicitante->cbo_id) ? $solicitante->cbo_id : -1]);?>
    </div>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>