<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitantes</h2>
            <ol class="breadcrumb">
                <li>Solicitantes</li>
                <li class="active">
                    <strong>
                        Cadastrar Solicitantes
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <?php include_once ('form.ctp'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

