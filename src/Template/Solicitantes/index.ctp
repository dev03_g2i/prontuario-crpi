<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Solicitantes</h2>
            <ol class="breadcrumb">
                <li>Solicitantes</li>
                <li class="active">
                    <strong>Litagem de Solicitantes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Solicitante', ['type' => 'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('nome', ['name' => 'Solicitantes__nome']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('conselho', ['name' => 'Solicitantes__conselho']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('sigla', ['name' => 'Solicitantes__sigla']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('cpf', ['name' => 'Solicitantes__cpf', 'mask' => 'cpf']);
                            echo "</div>";
                            echo "<div class='col-md-4' id=''>";
                            echo $this->Form->input('cbo_id', ['name' => 'Solicitantes__cbo_id', 'label' => 'Cbo', 'type' => 'select', 'data' => 'select', 'controller' => 'SolicitanteCbo', 'action' => 'fill']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Solicitantes', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Solicitantes', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Solicitantes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('conselho') ?></th>
                                        <th><?= $this->Paginator->sort('sigla') ?></th>
                                        <th><?= $this->Paginator->sort('cpf') ?></th>
                                        <th><?= $this->Paginator->sort('cbo_id', ['label' => 'Cbo']) ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($solicitantes as $solicitante): ?>
                                        <tr>
                                            <td><?= $this->Number->format($solicitante->id) ?></td>
                                            <td><?= h($solicitante->nome) ?></td>
                                            <td><?= h($solicitante->conselho) ?></td>
                                            <td><?= h($solicitante->sigla) ?></td>
                                            <td><?= h($solicitante->cpf) ?></td>
                                            <td><?= h($solicitante->solicitante_cbo->descricao) ?></td>
                                            <td><?= h($solicitante->created) ?></td>
                                            <td><?= h($solicitante->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link('<i class="fa fa-line-chart"></i>', ['controller'=>'ProdutividadeSolicitantes','action' => 'index','?'=> ['solicitante_id'=>$solicitante->id,'first'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Repasse', 'escape' => false, 'class' => 'btn btn-xs btn-warning','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $solicitante->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $solicitante->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'solicitantes\',' . $solicitante->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
