<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3>Agenda de contatos</h3>
                <ol class="breadcrumb">
                    <li>Agenda</li>
                    <li class="active">
                        <strong>Contatos</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros ">
                    <?= $this->Form->create('AgendaTel', ['type' => 'get','id'=>'form-agendaTel']) ?>

                    <div class="col-md-3">
                        <?= $this->Form->input('id', ['label' => 'Nome, categoria ou observação', 'type' => 'select', 'data' => 'select', 'controller' => 'AgendaTel', 'action' => 'fill']);?>
                    </div>

                    <div class="col-md-3" style="margin-top: 28px">
                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'escape' => false]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="text-right">
            <p>
                <?= $this->Html->link($this->Html->icon('plus') . ' Novo', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Contato', 'class' => 'btn btn-primary', 'escape' => false]) ?>
            </p>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Contatos') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('Nome') ?></th>
                                <th><?= $this->Paginator->sort('Endereço') ?></th>
                                <th><?= $this->Paginator->sort('Bairro') ?></th>
                                <th><?= $this->Paginator->sort('CEP') ?></th>
                                <th><?= $this->Paginator->sort('Cidade') ?></th>
                                <th><?= $this->Paginator->sort('Estado') ?></th>
                                <th><?= $this->Paginator->sort('Telefones') ?></th>
                                <th><?= $this->Paginator->sort('E-mail') ?></th>
                                <th><?= $this->Paginator->sort('Categoria') ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($agendaTel as $agendaTel): ?>
                                <tr>
                                    <td><?= h($agendaTel->name) ?></td>
                                    <td><?= h($agendaTel->address) ?></td>
                                    <td><?= h($agendaTel->district) ?></td>
                                    <td><?= h($agendaTel->zipCode) ?></td>
                                    <td><?= h($agendaTel->city) ?></td>
                                    <td><?= h($agendaTel->state) ?></td>
                                    <td><?= h($agendaTel->phones) ?></td>
                                    <td><?= h($agendaTel->email) ?></td>
                                    <td><?= h($agendaTel->category) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'agendaTel','action' => 'view', $agendaTel->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'agendaTel','action' => 'edit', $agendaTel->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                        <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'agendaTel','action'=>'delete', $agendaTel->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>