<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agenda de contatos</h2>
            <ol class="breadcrumb">
                <li>Agenda</li>
                <li class="active">
                    <strong>Cadastrar Contato</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="agendaTel form">
                            <?= $this->Form->create($agendaTel) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Contato') ?></legend>
                                <div class="col-md-6">
                                    <?= $this->Form->input('name', ['placeholder' => 'G2i', 'label' => 'Nome']);?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('address', ['placeholder' => 'Av. Afonso Pena 3504','label'=>'Endereço']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('district', ['placeholder' => 'Centro', 'label'=>'Bairro']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('zipCode', ['placeholder' => '79002-075',"mask" => "cep", 'label' => 'CEP']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('city', ['placeholder' => 'Campo Grande', 'label' => 'Cidade']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('state', ['placeholder' => 'MS', 'label' => 'Estado']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('phones', ['placeholder' => '(67) 3029-7847 Fixo', 'label' => 'Telefones']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('email', ['placeholder' => 'g2i@gmail.com', 'label' => 'E-mail', "mask" => "email"]); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('category', ['placeholder' => 'Desenvolvimento de software', 'label' => 'Categoria']); ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $this->Form->input('observation', ['placeholder' => 'grupog2i.com.br', 'label' => 'Observação']); ?>
                                </div>                                                            
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

