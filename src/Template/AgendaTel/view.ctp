
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Tel</h2>
        <ol class="breadcrumb">
            <li>Agenda Tel</li>
            <li class="active">
                <strong>Litagem de Agenda Tel</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Agenda Tel</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Name') ?></th>
                                <td><?= h($agendaTel->name) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Address') ?></th>
                                <td><?= h($agendaTel->address) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('District') ?></th>
                                <td><?= h($agendaTel->district) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('ZipCode') ?></th>
                                <td><?= h($agendaTel->zipCode) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('City') ?></th>
                                <td><?= h($agendaTel->city) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('State') ?></th>
                                <td><?= h($agendaTel->state) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Phones') ?></th>
                                <td><?= h($agendaTel->phones) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Email') ?></th>
                                <td><?= h($agendaTel->email) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Category') ?></th>
                                <td><?= h($agendaTel->category) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($agendaTel->id) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observation') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($agendaTel->observation)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


