<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Controlador Acoes</h2>
        <ol class="breadcrumb">
            <li>Grupo Controlador Acoes</li>
            <li class="active">
                <strong>Litagem de Grupo Controlador Acoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('GrupoControladorAco',['type'=>'get']) ?>
                        <?php
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('grupo_controlador_id', ['name'=>'GrupoControladorAcoes__grupo_controlador_id','data'=>'select','controller'=>'grupoControladores','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
    echo "<div class='col-md-4'>";
                                echo $this->Form->input('acao_id', ['name'=>'GrupoControladorAcoes__acao_id','data'=>'select','controller'=>'acoes','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('acesso',['name'=>'GrupoControladorAcoes__acesso']);
                                echo "</div>";
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Grupo Controlador Acoes', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Grupo Controlador Acoes','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Grupo Controlador Acoes') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('grupo_controlador_id') ?></th>
                <th><?= $this->Paginator->sort('acao_id') ?></th>
                <th><?= $this->Paginator->sort('acesso') ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                <th><?= $this->Paginator->sort('situacao_id') ?></th>
                <th><?= $this->Paginator->sort('user_id') ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($grupoControladorAcoes as $grupoControladorAco): ?>
            <tr>
                <td><?= $this->Number->format($grupoControladorAco->id) ?></td>
                <td><?= $grupoControladorAco->has('grupo_controladore') ? $this->Html->link($grupoControladorAco->grupo_controladore->id, ['controller' => 'GrupoControladores', 'action' => 'view', $grupoControladorAco->grupo_controladore->id]) : '' ?></td>
                <td><?= $grupoControladorAco->has('aco') ? $this->Html->link($grupoControladorAco->aco->nome, ['controller' => 'Acoes', 'action' => 'view', $grupoControladorAco->aco->id]) : '' ?></td>
                <td><?= h($grupoControladorAco->acesso) ?></td>
                <td><?= h($grupoControladorAco->created) ?></td>
                <td><?= h($grupoControladorAco->modified) ?></td>
                <td><?= $grupoControladorAco->has('situacao_cadastro') ? $this->Html->link($grupoControladorAco->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoControladorAco->situacao_cadastro->id]) : '' ?></td>
                <td><?= $grupoControladorAco->has('user') ? $this->Html->link($grupoControladorAco->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoControladorAco->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $grupoControladorAco->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $grupoControladorAco->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  '#Javascript:void(0)', ['onclick'=>'Deletar(\'grupoControladorAcoes\','.$grupoControladorAco->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
