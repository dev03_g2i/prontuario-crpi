
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Controlador Acoes</h2>
        <ol class="breadcrumb">
            <li>Grupo Controlador Acoes</li>
            <li class="active">
                <strong>Litagem de Grupo Controlador Acoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Grupo Controlador Acoes</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Grupo Controladore') ?></th>
                                                                <td><?= $grupoControladorAco->has('grupo_controladore') ? $this->Html->link($grupoControladorAco->grupo_controladore->id, ['controller' => 'GrupoControladores', 'action' => 'view', $grupoControladorAco->grupo_controladore->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Aco') ?></th>
                                                                <td><?= $grupoControladorAco->has('aco') ? $this->Html->link($grupoControladorAco->aco->nome, ['controller' => 'Acoes', 'action' => 'view', $grupoControladorAco->aco->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $grupoControladorAco->has('situacao_cadastro') ? $this->Html->link($grupoControladorAco->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoControladorAco->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $grupoControladorAco->has('user') ? $this->Html->link($grupoControladorAco->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoControladorAco->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($grupoControladorAco->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($grupoControladorAco->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($grupoControladorAco->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Acesso') ?>6</th>
                                <td><?= $grupoControladorAco->acesso ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


