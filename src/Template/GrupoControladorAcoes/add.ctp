<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Controlador Acoes</h2>
        <ol class="breadcrumb">
            <li>Grupo Controlador Acoes</li>
            <li class="active">
                <strong>                    Cadastrar Grupo Controlador Acoes
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="grupoControladorAcoes form">
                        <?= $this->Form->create($grupoControladorAco) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Grupo Controlador Aco') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('grupo_controlador_id', ['data'=>'select','controller'=>'grupoControladores','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('acao_id', ['data'=>'select','controller'=>'acoes','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('acesso');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

