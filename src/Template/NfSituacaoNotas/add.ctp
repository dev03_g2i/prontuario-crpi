<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Situacao Notas</h2>
            <ol class="breadcrumb">
                <li>Nf Situacao Notas</li>
                <li class="active">
                    <strong>
                                                Cadastrar Nf Situacao Notas
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="nfSituacaoNotas form">
                            <?= $this->Form->create($nfSituacaoNota) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Nf Situacao Nota') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

