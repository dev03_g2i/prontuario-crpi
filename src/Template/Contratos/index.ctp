<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contratos</h2>
            <ol class="breadcrumb">
                <li>Contratos</li>
                <li class="active">
                    <strong>Litagem de Contratos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Contrato', ['type' => 'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('nome', ['name' => 'Contratos__nome']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Contratos', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Contratos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Contratos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($contratos as $contrato): ?>
                                        <tr>
                                            <td><?= $this->Number->format($contrato->id) ?></td>
                                            <td><?= h($contrato->nome) ?></td>
                                            <td><?= $contrato->has('situacao_cadastro') ? $this->Html->link($contrato->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $contrato->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= $contrato->has('user') ? $this->Html->link($contrato->user->nome, ['controller' => 'Users', 'action' => 'view', $contrato->user->id]) : '' ?></td>
                                            <td><?= h($contrato->created) ?></td>
                                            <td><?= h($contrato->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $contrato->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $contrato->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'contratos\',' . $contrato->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

