<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contratos</h2>
        <ol class="breadcrumb">
            <li>Contratos</li>
            <li class="active">
                <strong>                    Editar Contratos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="contratos form">
                        <?= $this->Form->create($contrato) ?>
                        <fieldset>
                            <legend><?= __('Editar Contrato') ?></legend>
                            <?php
                                echo "<div class='col-md-12'>";
                                        echo $this->Form->input('nome');
                                echo "</div>";
                            echo "<div class='col-md-12'>";
                            echo '<div class="panel panel-default">';
                            echo "<div class='panel-body'><strong>Para que seja substituído as informações corretamente nos contratos, utilize estas variáveis:</strong> <br />
                                        {nome_responsavel},{nome_dependente},{cpf},{rua},{numero},{cidade},{estado},{bairro},{complemento},{valor},{parcela},{numero_parcela},{vencimento},{primeiro_pagamento},{dia},{mes},{ano}
                                </div>
                                </div>
                                </div>";
                                echo "<div class='col-md-12'>";
                                        echo $this->Form->input('descricao',['label'=>'Descrição','data'=>'sumer']);
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

