<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contratos</h2>
            <ol class="breadcrumb">
                <li>Contratos</li>
                <li class="active">
                    <strong>Litagem de Contratos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Contratos</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Nome') ?></th>
                                    <td><?= h($contrato->nome) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Situação Cadastro') ?></th>
                                    <td><?= $contrato->has('situacao_cadastro') ? $this->Html->link($contrato->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $contrato->situacao_cadastro->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Quem Cadastrou') ?></th>
                                    <td><?= $contrato->has('user') ? $this->Html->link($contrato->user->nome, ['controller' => 'Users', 'action' => 'view', $contrato->user->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Id') ?></th>
                                    <td><?= $this->Number->format($contrato->id) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Criação') ?></th>
                                    <td><?= h($contrato->created) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Modificação') ?></th>
                                    <td><?= h($contrato->modified) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3><?= __('Descricao') ?></h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(html_entity_decode($contrato->descricao)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


