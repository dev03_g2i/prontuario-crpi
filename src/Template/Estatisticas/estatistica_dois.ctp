<div class="ibox-title">
    <h2><?= __($nomeTabela); ?></h2>
</div>

<?php if (!empty($tabelas)):
    $c = 0;
    foreach ($tabelas as $tab): ?>
        <div class="ibox-content">
            <div class="clearfix">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><?= $nomesGrupos[$c]; ?></th>
                        </tr>
                        <tr>
                            <th><?= __($nomeCabecalho) ?></th>
                            <th><?= __('Jan') ?></th>
                            <th><?= __('Fev') ?></th>
                            <th><?= __('Mar') ?></th>
                            <th><?= __('Abr') ?></th>
                            <th><?= __('Mai') ?></th>
                            <th><?= __('Jun') ?></th>
                            <th><?= __('Jul') ?></th>
                            <th><?= __('Ago') ?></th>
                            <th><?= __('Set') ?></th>
                            <th><?= __('Out') ?></th>
                            <th><?= __('Nov') ?></th>
                            <th><?= __('Dez') ?></th>
                            <th><?= __('Total') ?></th>
                            <th><?= __('Média') ?></th>
                        </tr>
                        </thead>
                        <tbody class="text-right">
                        <?php
                        $aux = 0;
                        foreach ($tab as $q): ?>
                            <tr>
                                <td class="text-left"><?= h($q->nome) ?></td>
                                <td><?= ($q->qtdJan) ? $this->Number->format($q->qtdJan, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdFev) ? $this->Number->format($q->qtdFev, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdMar) ? $this->Number->format($q->qtdMar, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdAbr) ? $this->Number->format($q->qtdAbr, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdMai) ? $this->Number->format($q->qtdMai, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdJun) ? $this->Number->format($q->qtdJun, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdJul) ? $this->Number->format($q->qtdJul, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdAgo) ? $this->Number->format($q->qtdAgo, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdSet) ? $this->Number->format($q->qtdSet, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdOut) ? $this->Number->format($q->qtdOut, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdNov) ? $this->Number->format($q->qtdNov, ['places' => 2]) : 0 ?></td>
                                <td><?= ($q->qtdDez) ? $this->Number->format($q->qtdDez, ['places' => 2]) : 0 ?></td>
                                <td class="warning">
                                    <b><?= $q->qtd_agendamentos ? $q->qtd_agendamentos : 0 ?></b> 
                                </td>
                                <td class="warning">
                                    <b><?= $this->Number->format($medias[$c][$aux], ['places' => 2]); ?></b>
                                </td>
                            </tr>
                            <?php
                            $aux++;
                        endforeach; ?>
                        <tr class="warning">
                        <th class="text-left"><?= 'Total' ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdJan'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdFev'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdMar'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdAbr'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdMai'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdJun'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdJul'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdAgo'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdSet'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdOut'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdNov'], ['places' => 2]) ?></th>
                            <th class="text-right"><?= $this->Number->format($somas[$c]['qtdDez'], ['places' => 2]) ?></th>
                            <th class="text-right primary"><?= $this->Number->format($totalSomas[$c], ['places' => 2]) ?></th>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php
        $c++;
    endforeach;
endif; ?>