<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Gráficos diários</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area no-padding m-t-25">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Filtros</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="filtros ">
                                <?= $this->Form->create('Estatisticas', ['type' => 'get','id'=>'form-estatisticas']) ?>
                                <div class="col-md-3">
                                    <?= $this->Form->input('mes',['label' => 'Mês', 'type'=>'select', 'options' => $meses, 'required' => 'required', 'value' => $formatedDateMes]);?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Form->input('ano', ['label' => 'Ano', 'type' => 'select', 'options' => $anos, 'required' => 'required', 'value' => $formatedDateAno]); ?>
                                </div>
                                <div class="col-md-12" style="margin-top: 28px">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill-chart', 'class' => 'btn btn-default', 'escape' => false]) ?>
                                    <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="bodyCharts" style="visibility: hidden; display: none;">
                        <div class="table-responsive">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5 id="mesSelecionadoDiaryLineChart">
                                                Atendimentos do mês:
                                            </h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div id="diaryLineChartDiv">
                                                <canvas id="diaryLineChart"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Atendimentos dos últimos 3 meses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div id="barChartDiv">
                                                <canvas id="barChart"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <span class="label label-info pull-right">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <h5 id="mesSelecionadoGrafico">
                                                Valores Caixas (Dia/Dia) do mês:
                                            </h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div id="graficoDiv">
                                                <canvas id="grafico"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="ibox float-e-margins">
                                        <div class="ibox-title">
                                            <h5>Caixas dos últimos 3 meses</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div id="barChartCaixaDiv">
                                                <canvas id="barChartCaixa"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>