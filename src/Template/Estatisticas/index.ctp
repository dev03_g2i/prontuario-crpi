<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estatística</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Estatisticas', ['type' => 'post', 'url' => ['action' => 'index']]) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('tipo_estatistica', ['type' => 'select', 'options' => $tiposEstatisticas, 'id' => 'tipoEstatisticaId']);
                            echo "</div>";

                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('ano', ['name' => 'ano[]', 'type' => 'select', 'options' => $anos, 'id' => 'ano', 'empty' => 'Selecione', 'multiple', 'class' => 'select2', 'required' => 'true']);
                            echo "</div>";

                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('convenio', ['type' => 'select', 'empty' => 'Selecione', 'name' => 'convenio[]', 'label' => 'Convênio', 'multiple', 'class' => 'select2', 'options' => $convenios]);
                            echo "</div>";

                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('unidade', ['type' => 'select', 'options' => $unidades]);
                            echo "</div>";

                            echo "<div class='col-md-4' id='grupo_procedimento_estatistica'>";
                            echo $this->Form->input('grupo_procedimento', ['type' => 'select', 'empty' => 'Selecione', 'options' => $gruposFiltro, 'name' => 'grupo_procedimento[]', 'multiple', 'class' => 'select2']);
                            echo "</div>";

                            echo "<div class='col-md-4' id='tipo_resultado_estatistica'>";
                            echo $this->Form->input('tipo_resultado', ['type' => 'select', 'options' => [1 => 'Quantidade', 2 => 'Valor']]);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'submit', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'formtarget' => '_blank', 'title' => 'Gerar']) ?>

                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

