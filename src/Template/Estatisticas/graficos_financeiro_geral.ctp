<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estatística</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area no-padding m-t-25">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Gráficos</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Atendimentos do mês: <?= $this->Time->format(date('Y/m/d'), 'MMMM') ?> </h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div>
                                            <canvas id="doughnutChart" height="140"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Atendimentos dos últimos 3 meses</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div>
                                            <canvas id="barChart" height="140"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <span class="label label-info pull-right"><i class="fa fa-calendar"></i></span>
                                        <h5>Valores Caixas (Dia/Dia) do
                                            mês: <?= $this->Time->format(date('Y/m/d'), 'MMMM') ?></h5>
                                    </div>
                                    <div class="ibox-content">
                                        <canvas id="grafico" width="600" height="275"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Caixas dos últimos 3 meses</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <div>
                                            <canvas id="barChartCaixa" height="140"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
function number_format(number, decimals, dec_point, thousands_point) {

if (number == null || !isFinite(number)) {
    throw new TypeError('number is not valid');
}

if (!decimals) {
    var len = number.toString().split('.').length;
    decimals = len > 1 ? len : 0;
}

if (!dec_point) {
    dec_point = ',';
}

if (!thousands_point) {
    thousands_point = '.';
}

number = parseFloat(number).toFixed(decimals);

number = number.replace(",", dec_point);

var splitNum = number.split(dec_point);
splitNum[0] = splitNum[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_point);
number = splitNum.join(dec_point);

return number;
}
</script>
<?php
echo $this->Html->scriptBlock(
    "        $(window).load(function(){
                 $.ajax({
                    url:site_path+\"/Estatisticas/graficoCaixaConvenio\",
                    dataType:\"JSON\",
                    success:function(txt){
                    var data = {
                        datasets:[
                            {
                                label: txt.labels[0][0],
                                data: txt.valoresCaixa,
                                backgroundColor:\"rgba(0, 0, 0, 0.5)\"
                            },
                            {
                                label: txt.labels[0][1],
                                data: txt.valoresTotal,
                                backgroundColor:\"rgba(26,179,148,0.5)\"
                            },
                        ],
                        labels: txt.dias,
                    };
                   var ctx = document.getElementById(\"grafico\");        
                   var myChart = new Chart(ctx, {
                        type: \"line\",
                        data:data,
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {return number_format(value) + ',00';}
                                    }
                                }]
                            }
                        }
                    });
                    }
                 })
                  $.ajax({
                    url:site_path+\"/Estatisticas/graficoCaixaConveniosUltimos\",
                    dataType:\"JSON\",
                    success:function(txt){
                    var data = {
                        datasets:[
                            {
                                label: txt.labels[0][0],
                                data: txt.valoresCaixa,
                                backgroundColor:\"rgba(0, 0, 0, 0.5)\"
                            },
                            {
                                label: txt.labels[0][1],
                                data: txt.valoresTotal,
                                backgroundColor:\"rgba(26,179,148,0.5)\"
                            },
                        ],
                        labels: txt.meses,
                    };
                    var ctx = document.getElementById(\"barChartCaixa\");        
                    var myChart = new Chart(ctx, {
                        type: \"line\",
                        data:data,
                        options: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {return number_format(value) + ',00';}
                                    }
                                }]
                            }
                        }
                        });
                    }
                 })          
                $.ajax({
                    url:site_path+\"/Estatisticas/grafico-atendimento\",
                    dataType:\"JSON\",
                    success:function(txt){
                        var doughnutData = {
                            labels: txt.grupo,
                            datasets: [{
                                data: txt.mesAtual,
                                backgroundColor: [\"#a3e1d4\",\"#dedede\",\"#B1B3C6\", \"#B9B9BD\", \"#C2C4DF\", \"#9A9FCB\", \"#ADB1D7\", \"#DEDFEC\"]
                                }]
                            };
                            var doughnutOptions = {
                                responsive: true
                        };
                        var ctx4 = document.getElementById(\"doughnutChart\").getContext(\"2d\");
                        new Chart(ctx4, {type: \"doughnut\", data: doughnutData, options:doughnutOptions});                     
                        var barData = {
                            labels: txt.grupo,
                            datasets: [
                                {
                                    label: txt.meses[0],
                                    backgroundColor: \"rgba(0, 0, 0, 0.5)\",
                                    data: txt.mes3
                                },
                                {
                                    label: txt.meses[1],
                                    backgroundColor: \"rgba(26,179,148,0.5)\",
                                    data: txt.mes2
                                },
                                {
                                    label: txt.meses[2],
                                    backgroundColor: \"#dedede\",
                                    data: txt.mes1
                                } 
                            ]
                        };
                        var barOptions = {
                            responsive: true,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                        callback: function(value) {return number_format(value) + ',00';}
                                    }
                                }]
                            }
                        };
                        var ctx2 = document.getElementById(\"barChart\").getContext(\"2d\");
                        new Chart(ctx2, {type: \"bar\", data: barData, options:barOptions});
                    }    
                })
        })
    ",
    ['inline' => false, 'block' => 'scriptLast']
);
?>