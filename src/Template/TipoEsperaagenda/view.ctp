<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tipo Esperaagenda</h2>
            <ol class="breadcrumb">
                <li>Tipo Esperaagenda</li>
                <li class="active">
                    <strong>Litagem de Tipo Esperaagenda</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Tipo Esperaagenda</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Descricao') ?>
                                    </th>
                                    <td>
                                        <?= h($tipoEsperaagenda->descricao) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $tipoEsperaagenda->has('user') ? $this->Html->link($tipoEsperaagenda->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoEsperaagenda->user->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situacao') ?>
                                    </th>
                                    <td>
                                        <?= $tipoEsperaagenda->has('situacao_cadastro') ? $this->Html->link($tipoEsperaagenda->situacao_cadastro->nome, ['controller' => 'Users', 'action' => 'view', $tipoEsperaagenda->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Status Tipoespera') ?>
                                    </th>
                                    <td>
                                        <?= $tipoEsperaagenda['StatusTipoEspera']['descricao']; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($tipoEsperaagenda->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($tipoEsperaagenda->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>