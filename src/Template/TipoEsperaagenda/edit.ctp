<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Tipo Esperaagenda</h2>
            <ol class="breadcrumb">
                <li>Tipo Esperaagenda</li>
                <li class="active">
                    <strong>
                        Editar Tipo Esperaagenda
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="tipoEsperaagenda form">
                            <?= $this->Form->create($tipoEsperaagenda) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Tipo Esperaagenda') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('descricao'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('situacao_id'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('status_tipoespera_id'); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>