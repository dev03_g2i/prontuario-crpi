<div class="this-place">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Retornos</h2>
            <ol class="breadcrumb">
                <li>Retornos</li>
                <li class="active">
                    <strong>Litagem de Retornos</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Dados Cliente</h3>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Nome') ?></th>
                                    <th><?= __('Telefone') ?></th>
                                    <th><?= __('Celular') ?></th>
                                    <th><?= __('Comercial') ?></th>
                                    <th><?= __('Nascimento') ?></th>
                                </tr>
                                <tr>
                                    <td><?= $retorno->has('cliente') ? $this->Html->link($retorno->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $retorno->cliente->id]) : '' ?></td>
                                    <td><?= $retorno->has('cliente') ? $this->Html->link($retorno->cliente->telefone, ['controller' => 'Clientes', 'action' => 'view', $retorno->cliente->id]) : '' ?></td>
                                    <td><?= $retorno->has('cliente') ? $this->Html->link($retorno->cliente->celular, ['controller' => 'Clientes', 'action' => 'view', $retorno->cliente->id]) : '' ?></td>
                                    <td><?= $retorno->has('cliente') ? $this->Html->link($retorno->cliente->comercial, ['controller' => 'Clientes', 'action' => 'view', $retorno->cliente->id]) : '' ?></td>
                                    <td><?= $retorno->has('cliente') ? $this->Html->link($retorno->cliente->nascimento, ['controller' => 'Clientes', 'action' => 'view', $retorno->cliente->id]) : '' ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">Dados Retorno</div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Código') ?></th>
                                    <th><?= __('Data') ?></th>
                                    <th><?= __('Grupo Retorno') ?></th>
                                    <th><?= __('Situacao Retorno') ?></th>
                                    <th><?= __('Quem cadastrou') ?></th>
                                    <th><?= __('Dias') ?></th>
                                    <th><?= __('Dt. cadastro') ?></th>
                                    <th><?= __('Dt. atualização') ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>

                                </tr>
                                <tr>
                                    <td><?= $this->Number->format($retorno->id) ?></td>
                                    <td><?= h($retorno->previsao) ?></td>
                                    <td><?= $retorno->has('grupo_retorno') ? $this->Html->link($retorno->grupo_retorno->nome, ['controller' => 'GrupoRetornos', 'action' => 'view', $retorno->grupo_retorno->id]) : '' ?></td>
                                    <td><?php
                                        if($retorno->has('situacao_retorno')){
                                            if($retorno->situacao_retorno_id==1){
                                                echo $this->Html->link($retorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $retorno->situacao_retorno->id],['class'=>'btn btn-xs btn-info']);
                                            }else if ($retorno->situacao_retorno_id==2){
                                                echo $this->Html->link($retorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $retorno->situacao_retorno->id],['class'=>'btn btn-xs btn-primary']);
                                            }else{
                                                echo $this->Html->link($retorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $retorno->situacao_retorno->id],['class'=>'btn btn-xs btn-danger']);
                                            }
                                        }else{
                                            echo '';
                                        }  ?>
                                    </td>
                                    <td><?= $retorno->has('user') ? $this->Html->link($retorno->user->nome, ['controller' => 'Users', 'action' => 'view', $retorno->user->id]) : '' ?></td>
                                    <td><?= $this->Number->format($retorno->dias) ?></td>
                                    <td><?= h($retorno->created) ?></td>
                                    <td><?= h($retorno->modified) ?></td>
                                    <td class="actions">
                                        <?= $this->Html->link($this->Html->icon('check'), ['action' => 'aprovar', $retorno->id,'?'=>['ajax'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Alterar Situação', 'escape' => false, 'class' => 'btn btn-xs btn-success','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?= __('<strong>Observação</strong>') ?></td>
                                    <td colspan="8"><?= $this->Text->autoParagraph(h($retorno->observacao)); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
