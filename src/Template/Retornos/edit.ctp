<div class="this-place">
    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Retornos</h2>
            <ol class="breadcrumb">
                <li>Retornos</li>
                <li class="active">
                    <strong> Editar Retornos
                    </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="retornos form">
                            <?= $this->Form->create($retorno,['id'=>'frmretornos']) ?>
                            <fieldset>
                                <legend><?= __('Editar Retorno') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('cliente_id', ['type' => 'select', 'data-cliente' => "cliente", 'empty' => 'Selecione', 'options' => [@$clientes->id => @$clientes->nome], 'default' => @$clientes->id,'label'=>'Paciente']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('grupo_id', ['options' => $grupoRetornos]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('previsao', ['label'=>'Data','empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($retorno->previsao, 'dd-MM-Y'),'append' => [$this->Form->button($this->Html->icon("calendar"),['type'=>'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('dias',['data-toggle'=>'popover','title'=>'Alerta','data-content'=>'Ao adicionar valor a este campo, o campo data não será considerado.','data-placement'=>'bottom']);
                                echo "</div>";
                                echo "<div class='clearfix'></div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('situacao_retorno_id',['label'=>'Situação Retorno','options' => $situacaoRetornos, 'empty' => true]);
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('observacao',['label'=>'Observação']);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

