<div class="this-place">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Retornos</h2>
            <ol class="breadcrumb">
                <li>Retornos</li>
                <li class="active">
                    <strong>Cadastrar Retornos</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="retornos form">
                            <?= $this->Form->create($retorno,['id'=>'frmretornos']) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Retorno') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('cliente_id', ['type' => 'select', 'data-cliente' => "cliente", 'empty' => 'Selecione', 'options' => [@$clientes->id => @$clientes->nome], 'default' => @$clientes->id,'label'=>'Paciente']);
                                echo "</div>";
                                echo "<div class='col-md-6' data-toggle='popover' title='Alerta' data-content='Lembre-se de verificar se existe grupos vinculados ao cliente selecionado!' data-placement='bottom' data-trigger='hover' >";
                                echo $this->Form->input('grupo_id', ['options' => $grupoRetornos, 'empty' => 'selecione',
                                    'append' => [
                                    $this->Form->button('<span class="glyphicon glyphicon-plus-sign"></span>', [
                                        'data'=>'modal','controller'=>'ClienteGrupoRetornos','action'=>'nadd','vinculo'=>'cliente-id', 'escape' => false, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo Grupo','type'=>'button'
                                    ])
                                ]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('previsao', ['label' => 'Data', 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('dias');
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('observacao', ['label' => 'Observação']);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


