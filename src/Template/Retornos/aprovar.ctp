<div class="this-place">
    <div class="wrapper white-bg ">
        <div class="col-lg-4">
            <h2>Retornos</h2>
        </div>
        <div class="col-lg-8 text-right">
            <h2>
                <?= $this->Html->link('<i class="fa fa-group"></i> Responsáveis', ['controller' => 'Clientes', 'action' => 'openpront', '?' => ['resp' => $retorno->cliente_id, 'nome' => $retorno->cliente->nome]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Responsaveis', 'escape' => false, 'class' => 'btn btn-xs btn-success', 'listen' => 'f', 'target' => '_blanck']) ?>
                <?= $this->Html->link('<i class="fa fa-user"></i> Cadastro', ['controller' => 'Clientes', 'action' => 'edit', $retorno->cliente_id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Abrir Cadastro', 'escape' => false, 'class' => 'btn btn-xs btn-warning', 'listen' => 'f', 'target' => '_blanck']) ?>
                <?php if ($retorno->situacao_retorno_id == 2): ?>
                    <?= $this->Html->link($this->Html->icon('calendar') . ' Agendamento', ['controller' => 'Agendas', 'action' => 'view', '?' => ['retorno' => $retorno->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Ir para o agendamento', 'escape' => false, 'class' => 'btn btn-xs btn-info', 'listen' => 'f', 'target' => '_blanck']) ?>
                <?php endif; ?>
                <?= $this->Html->link($this->Html->icon('calendar') . ' Agenda', ['controller' => 'Agendas', 'action' => 'lista', '?' => ['retorno' => $retorno->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Ir para agenda', 'escape' => false, 'class' => 'btn btn-xs btn-info', 'listen' => 'f', 'target' => '_blanck','id'=>'btn-agenda','style'=>'display:none']) ?>
                <?= $this->Html->link($this->Html->icon('list-alt') . ' Prontuário', ['controller' => 'Clientes', 'action' => 'openpront', '?' => ['pront' => $retorno->cliente_id, 'nome' => $retorno->cliente->nome]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Abrir Prontuário', 'escape' => false, 'class' => 'btn btn-xs btn-primary', 'listen' => 'f', 'target' => '_blank']) ?>
            </h2>
        </div>
        <div class="clearfix">
            <div class="col-md-6">
                    <div class="col-md-12">
                        <h3>Paciente: <?= $retorno->cliente->nome ." (" . $retorno->cliente->id . ")" ?></h3>
                    </div>
                    <div class="col-md-12">
                        Telefone: <strong><?= $retorno->cliente->telefone ?></strong>
                    </div>

                    <div class="col-md-12">
                        Celular: <strong><?= $retorno->cliente->celular?></strong>
                    </div>
                    <div class="col-md-12">
                        Profissão: <strong><?= $retorno->cliente->profissao?></strong>
                    </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <h3>Idade: <?= $this->Data->idade($retorno->cliente->nascimento) ?></h3>
                </div>
                <?php
                if (!empty($retorno->cliente->cliente_responsaveis)) {
                    foreach ($retorno->cliente->cliente_responsaveis as $c) {
                        echo '<div class="col-md-12">';
                        echo $c->grau_parentesco->nome . ": <strong>" . $c->nome . "</strong>";
                        echo '</div>';
                    }
                }
                ?>

                <div class="col-md-12">
                    Particularidades: <strong><?= $retorno->cliente->particularidades ?></strong>
                </div>
                <div class="col-md-12">
                    Observação da Recepção: <strong><?= $retorno->cliente->recepcao ?></strong>
                </div>
            </div>
                    <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="col-md-6">
                            <h3>Confirmação de retorno</h3>
                        </div>
                        <div class="col-md-6">
                            <h3 class="text-right">
                                <?= $this->Html->link($this->Html->icon('list-alt') . ' Retornos Anteriores', ['controller' => 'LogRetornos', 'action' => 'index', '?' => ['retorno' => $retorno->id, 'ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Retornos Ateriores', 'escape' => false, 'class' => 'btn btn-xs btn-info', 'listen' => 'f', 'id' => 'list-anteriores']) ?>
                            </h3>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="retornos form">
                            <?= $this->Form->create($retorno, ['id' => 'form-retorno']) ?>
                            <fieldset>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('previsao', ['label' => 'Data', 'empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($retorno->previsao, 'dd-MM-Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('situacao_retorno_id', ['label' => 'Situação Retorno', 'empty' => 'Selecione', 'options' => $situacaoRetornos]);
                                echo "</div>";
                                echo "<div class='clearfix'></div>";

                                echo "<div id='data-futura' style='display: none'>";
                                echo "</div>";
                                echo "<div id='area-agenda' style='display: none'>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('inicio', ['label' => 'Data Inicial Agenda', 'empty' => true, 'type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);timepicker
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('fim', ['label' => 'Data Final Agenda', 'empty' => true, 'type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";

                                /* echo "<div class='col-md-6'>";
                                 echo $this->Form->input('medico_id', ['label'=>'Profissional','empty' => 'Selecione','options'=>$medicos]);
                                 echo "</div>";*/

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('grupoagenda_id', ['label' => 'Grupo Agenda', 'empty' => 'Selecione', 'options' => $grupos]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipoagenda_id', ['label' => 'Tipo Agenda', 'empty' => 'Selecione', 'options' => $tipo]);
                                echo "</div>";

                                echo "</div>";

                                echo "<div class='clearfix'></div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('observacao', ['label' => 'Observação']);
                                echo "</div>";

                                ?>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'AprovarRetorno(\'form-retorno\')']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
