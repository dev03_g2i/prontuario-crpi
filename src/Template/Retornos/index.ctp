<div class="this-place">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Retornos</h2>
            <ol class="breadcrumb">
                <li>Retornos</li>
                <li class="active">
                    <strong>Litagem de Retornos</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros ">
                            <?= $this->Form->create('Retornos', ['type' => 'get']) ?>
                            <div class="col-md-3">
                                <?= $this->Form->input('grupo_id', ['type' => 'select', 'empty' => 'Selecione', 'options' => $grupoRetornos, 'default' => @$grupo]); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $this->Form->input('cliente_id', ['id'=>'cliente_id','type' => 'select', 'data-cliente' => "cliente", 'empty' => 'Selecione', 'options' => [@$clientes->id => @$clientes->nome], 'default' => @$clientes->id,'label'=>'Paciente']); ?>
                            </div>

                            <div class="col-md-3">
                                <?= $this->Form->input('inicio', ['label' => 'Data inicial', 'type' => 'text', 'class' => "datepicker","value"=>$inicio, 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $this->Form->input('fim', ['label' => 'Data final', 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $this->Form->input('situacao_retorno_id', ['label'=>'Situação Retorno','name'=>'Retornos__situacao_retorno_id','type' => 'select', 'empty' => 'Selecione', 'options' => $situacaoRetornos]); ?>
                            </div>
                            <div class="col-md-3" style="margin-top: 25px">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Retornos', ['action' => 'add', '?' => ['grupo_id' => $grupo, 'cliente_id' => @$clientes->id]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Retornos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Retornos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('cliente_id') ?></th>
                                        <th><?= $this->Paginator->sort('grupo_id') ?></th>
                                        <th><?= $this->Paginator->sort('previsao') ?></th>
                                        <th><?= $this->Paginator->sort('dias') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_retorno_id', ['label' => 'Situação Retorno']) ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. Cadastro']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($retornos as $retorno): ?>
                                        <tr>
                                            <td><?= $this->Number->format($retorno->id) ?></td>
                                            <td><?= $retorno->has('cliente') ? $retorno->cliente->nome : '' ?></td>
                                            <td><?= $retorno->has('grupo_retorno') ? $retorno->grupo_retorno->nome : '' ?></td>
                                            <td><?= h($retorno->previsao) ?></td>
                                            <td><?= $this->Number->format($retorno->dias) ?></td>
                                            <td><?php
                                                if ($retorno->has('situacao_retorno')) {
                                                    if ($retorno->situacao_retorno_id == 1) {
                                                        echo $this->Html->link($retorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $retorno->situacao_retorno->id], ['class' => 'btn btn-xs btn-info disabled']);
                                                    } else if ($retorno->situacao_retorno_id == 2) {
                                                        echo $this->Html->link($retorno->situacao_retorno->nome, ['controller' => 'Agendas', 'action' => 'view', '?' => ['retorno' => $retorno->id, 'ajax' => 1]], ['class' => 'btn btn-xs btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal_lg', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Ver Agendamento']);
                                                    } else {
                                                        echo $this->Html->link($retorno->situacao_retorno->nome, ['controller' => 'SituacaoRetornos', 'action' => 'view', $retorno->situacao_retorno->id], ['class' => 'btn btn-xs btn-danger disabled']);
                                                    }
                                                } else {
                                                    echo '';
                                                } ?></td>
                                            <td><?= h($retorno->created) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('check'), ['action' => 'aprovar', $retorno->id, '?' => ['ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Alterar Situação', 'escape' => false, 'class' => 'btn btn-xs btn-success', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

