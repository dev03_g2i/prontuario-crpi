

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Dentes</h2>
        <ol class="breadcrumb">
            <li>Dentes</li>
            <li class="active">
                <strong>Litagem de Dentes</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="dentes">
    <h3><?= h($dente->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($dente->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $dente->has('user') ? $this->Html->link($dente->user->nome, ['controller' => 'Users', 'action' => 'view', $dente->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $dente->has('situacao_cadastro') ? $this->Html->link($dente->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $dente->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($dente->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($dente->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($dente->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

