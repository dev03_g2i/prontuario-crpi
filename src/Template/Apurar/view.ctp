
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Apurar</h2>
        <ol class="breadcrumb">
            <li>Apurar</li>
            <li class="active">
                <strong>Litagem de Apurar</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Apurar</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($apurar->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $apurar->has('user') ? $this->Html->link($apurar->user->nome, ['controller' => 'Users', 'action' => 'view', $apurar->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem modificou') ?></th>
                                                                <td><?= $apurar->has('users_up') ? $this->Html->link($apurar->users_up->nome, ['controller' => 'Users', 'action' => 'view', $apurar->users_up->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($apurar->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($apurar->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($apurar->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


