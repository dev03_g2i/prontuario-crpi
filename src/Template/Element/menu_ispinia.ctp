<li class="nav-header">

    <?php echo $this->Html->link('<h2>G2i</h2>', ['controller' => 'agendas', 'action' => 'index'], ['escape' => false])?>
</li>

<li>
    <?= $this->Html->link('<i class="fa fa-thumb-tack"></i><span class="nav-label">Sala de Espera</span>', ['controller' => 'Agendas', 'action' => 'index'], ['escape' => false]) ?>
</li>

<li <?= $this->fetch('title') == 'Index' ? 'active' : ''; ?>>
    <a href="#"><i class="fa fa-calendar"></i><span class="nav-label">Agenda</span><span
                class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li>
            <?= $this->Html->link('<i class="fa fa-th-large"></i> Lista', ['controller' => 'Agendas', 'action' => 'agendas'], ['escape' => false]) ?>
            <?= $this->Html->link('<i class="fa fa-th-large"></i> Calendário', ['controller' => 'Agendas', 'action' => 'lista'], ['escape' => false]) ?>
        </li>
    </ul>
</li>

<li <?= $this->fetch('title') == 'Clientes' ? 'class="active"' : ''; ?>>
    <a href="#"><i class="fa fa-user-md"></i><span class="nav-label">Atendimentos</span><span
                class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li>
            <?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Atendimentos', 'action' => 'index'], ['escape' => false]) ?>
            <?= $this->Html->link('<i class="fa fa-th-large"></i> Novo', ['controller' => 'Atendimentos', 'action' => 'add'], ['escape' => false]) ?>
        </li>
    </ul>
</li>

<li <?= $this->fetch('title') == 'Clientes' ? 'class="active"' : ''; ?>>
    <a href="#"><i class="fa fa-users"></i><span class="nav-label">Pacientes</span><span
            class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Clientes', 'action' => 'index'], ['escape' => false]) ?></li>
        <li><?= $this->Html->link('<i class="fa fa-plus"></i> Cadastrar', ['controller' => 'Clientes', 'action' => 'add'], ['escape' => false]) ?></li>
    </ul>
</li>

<li   <?= $this->fetch('title') == 'Index' ? 'active' : ''; ?>>
<?= $this->Html->link('<i class="fa fa-dashboard"></i> <span class="nav-label">Indicadores</span>', ['controller' => 'Indicadores', 'action' => 'index'], ['escape' => false]) ?>
</li>


<li <?= $this->fetch('title') == 'Relatorios' ? 'class="active"' : ''; ?>>
    <a href="#"><i class="fa fa-file-text"></i><span class="nav-label">Relatórios</span><span class="fa arrow"></span></a>
    <ul class="nav nav-second-level">
        <li><?= $this->Html->link('<i class="fa fa-calculator"></i> Caixa Executante', ['controller' => 'Relatorios', 'action' => 'executante','?'=>['ajax'=>1,'fisrt'=>1]], ['escape' => false,'data-toggle'=>'modal']) ?></li>
    </ul>
</li>








