<?= $this->Html->css('cake') ?>
<?= $this->Html->css('bootstrap') ?>
<?= $this->Html->css('ispinia/font-awesome/css/font-awesome') ?>
<?= $this->Html->css('ispinia/animate') ?>
<?= $this->Html->css('ispinia/style') ?>
<?= $this->fetch('meta') ?>
<?= $this->fetch('css') ?>

<?= $this->Html->css('bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css') ?>
<?= $this->Html->css('ispinia/plugins/css3_animations/animations') ?>
<?= $this->Html->css('ispinia/plugins/toastr/toastr.min') ?>
<?= $this->Html->css('ispinia/bootstrap-datetimepicker.min') ?>
<?= $this->Html->css('ispinia/plugins/bootstrap_message/bootstrap-dialog') ?>
<?= $this->Html->css('ispinia/plugins/bootstrap3-editable/css/bootstrap-editable') ?>
<?= $this->Html->css('ispinia/plugins/select2/select2') ?>
<?= $this->Html->css('ispinia/plugins/select2/select2-bootstrap') ?>
<?= $this->Html->css('ispinia/plugins/iCheck/skins/square/green') ?>
<?= $this->Html->css('ispinia/plugins/steps/jquery.steps') ?>
<?= $this->Html->css('ispinia/plugins/input_file/fileinput') ?>
<?= $this->Html->css('ispinia/plugins/summernote/summernote') ?>
<?= $this->Html->css('ispinia/plugins/lightboxview/css/lightview/lightview') ?>
<?= $this->Html->css('ispinia/plugins/fullcalendar/fullcalendar') ?>
<?= $this->Html->css('ispinia/plugins/fullcalendar/fullcalendar.print',['media'=>'print']) ?>
<?= $this->Html->css('ispinia/plugins/qtip/jquery.qtip') ?>
<?= $this->Html->css('ispinia/plugins/lightgallery/src/css/lightgallery') ?>
<?php //echo $this->Html->css('home/default'); ?>
<?= $this->Html->css('home/responsive') ?>
<?= $this->Html->css('g2iFinanceiro') ?>



