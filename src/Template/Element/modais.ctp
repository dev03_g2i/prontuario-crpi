<?php
use Cake\Filesystem\File;

$content = "<p class='text-center'>".$this->Html->image('loading.gif', ['alt' => 'carregando'])."</p> ";
echo $this->Modal->create(['id' => 'modal','data-modal-index'=>"1"]) ;
echo $this->Modal->header($this->fetch('title'), ['close' => false]) ;
echo $this->Modal->body($content, ['class' => 'my-body-class']) ;
echo $this->Modal->footer([
    $this->Form->button('Close', ['data-dismiss' => 'modal'])
]) ;
echo $this->Modal->end() ;
?>
<div class="modal fade bs-example-modal-lg" data-backdrop="false" id="modal_lg" data-modal-index="1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
           <?=$content?>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal-lg" data-backdrop="false" id="modal_2" data-modal-index="1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
           <?=$content?>
        </div>
    </div>
</div>

