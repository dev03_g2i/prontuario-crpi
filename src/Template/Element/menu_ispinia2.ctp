<nav class="navbar navbar-fixed-top navbar-pronturario" role="navigation" style="margin-bottom: 52px !important;">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                class="navbar-toggle collapsed"
                type="button">
            <i class="fa fa-reorder"></i>
        </button>
        <?php if ($user_logado['codigo_medico'] != null) {
            echo $this->Html->link('G2i', ['controller' => '/'], ['class' => 'navbar-brand', 'escape' => false]);
        } else {
            echo $this->Html->link('G2i', ['controller' => 'Agendas', 'action' => 'index'], ['class' => 'navbar-brand', 'escape' => false]);
        }
        ?>
    </div>
    <div class="navbar-collapse collapse" id="navbar">
        <ul class="nav navbar-nav">
            <li class="active">
                <?= $this->Html->link('<!--<i class="fa fa-thumb-tack"></i>--><span class="nav-label">Sala de Espera</span>', ['controller' => 'Agendas', 'action' => 'index'], ['escape' => false]) ?>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <!--<i class="fa fa-calendar"></i>-->Agenda
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li class="<?= $this->Configuracao->showAgendaLista() ?>">
                        <?= $this->Html->link('<i class="fa fa-list"></i> Agendas', ['controller' => 'Agendas', 'action' => 'agendas'], ['escape' => false]) ?>
                    </li>
                    <li class="<?= $this->Configuracao->showAgendaCalendario() ?>">
                        <?= $this->Html->link('<i class="fa fa-calendar"></i> Calendário', ['controller' => 'Agendas', 'action' => 'lista'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-money"></i> Orçamento/Valores de procedimentos', ['controller' => 'PrecoProcedimentos', 'action' => 'vl-procedimentos'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-print"></i> Imprimir', ['controller' => 'Agendas', 'action' => 'imprimir', '?' => ['ajax' => 1, 'fisrt' => 1]], ['escape' => false, 'data-toggle' => 'modal']) ?>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Pacientes
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?= $this->Html->link('<i class="fa fa-users"></i> Pacientes', ['controller' => 'Clientes', 'action' => 'index'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-user-plus"></i> Cadastrar Paciente', ['controller' => 'Clientes', 'action' => 'add'], ['escape' => false]) ?>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Atendimentos
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?= $this->Html->link('<i class="fa fa-list"></i> Atendimentos', ['controller' => 'Atendimentos', 'action' => 'index'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Relatórios', ['controller' => 'Atendimentos', 'action' => 'relatorios'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-plus"></i> Novo Atendimento', ['controller' => 'Atendimentos', 'action' => 'add'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-money"></i> Orçamento/Valores de procedimentos', ['controller' => 'PrecoProcedimentos', 'action' => 'vl-procedimentos'], ['escape' => false]) ?>
                    </li>
                </ul>
            </li>
            <li class="dropdown <?= $this->Configuracao->showLaudos() ?>">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Diagnóstico
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?= $this->Html->link('<i class="fa fa-list"></i> Laudos', ['controller' => 'Laudos', 'action' => 'index'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-print"></i> Gerar Etiquetas', ['controller' => 'Laudos', 'action' => 'etiquetas', '?' => ['ajax' => 1, 'fisrt' => 1]], ['escape' => false, 'data-toggle' => 'modal']) ?>
                    </li>
                </ul>
            </li>

            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Gerenciamento
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                <?php if($this->Configuracao->usaFinanceiro()):?>
                    <li class="dropdown dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Financeiro</a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= $this->Html->link('<i class="fa fa-archive"></i> Fluxo de Caixa', ['controller' => 'dashboardFinanceiro', 'action' => 'index'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-money"></i> Recebimentos', ['controller' => 'finContasReceber', 'action' => 'index'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-truck"></i> Fornecedores', ['controller' => 'finFornecedores', 'action' => 'index'], ['escape' => false]) ?>
                            </li>
                            <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Resultados</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-list-alt"></i> Resultados', ['controller' => 'FinResultados', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-line-chart"></i> Indicadores', ['controller' => 'FinIndicadores', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Contas à pagar</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-money"></i> Gerenciamento', ['controller' => 'finContasPagar', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-money"></i> Quitação', ['controller' => 'finContasPagar', 'action' => 'quitacao'], ['escape' => false]) ?>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Movimentos</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-exchange"></i> Conciliar Movimentação', ['controller' => 'finMovimentos', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-exchange"></i> Movimentação Geral', ['controller' => 'finMovimentos', 'action' => 'geral'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-exchange"></i> Abertura/Encerramento', ['controller' => 'finMovimentos', 'action' => 'controle'], ['escape' => false]) ?>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Dados base</a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-bank"></i> Bancos', ['controller' => 'finBancos', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-calculator"></i> Plano de contas', ['controller' => 'finPlanoContas', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-random"></i> Classificações', ['controller' => 'finClassificacaoContas', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-building"></i> Empresas', ['controller' => 'finContabilidades', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-users"></i> Grupo de empresas', ['controller' => 'finGrupos', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <!-- <li>
                                        <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Relatórios', ['controller' => 'finRelatorios', 'action' => 'mensal'], ['escape' => false]) ?>
                                    </li> -->
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-tree"></i> Tipo documento', ['controller' => 'finTipoDocumento', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                    <li>
                                        <?= $this->Html->link('<i class="fa fa-usd"></i> Forma pagamento', ['controller' => 'finTipoPagamento', 'action' => 'index'], ['escape' => false]) ?>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <li class="dropdown dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Faturamento</a>
                        <ul class="dropdown-menu">
                            <li>
                                <?= $this->Html->link('<i class="fa fa-calculator"></i> Pré-Faturamento e Relatórios(Procedimentos)', ['controller' => 'Faturamentos', 'action' => 'home'], ['escape' => false]) ?>
                            </li>
                            <?php if ($this->Faturamento->mostraFaturaEquipe()): ?>
                                <li>
                                    <?= $this->Html->link('<i class="fa fa-calculator"></i> Pré-Faturamento e Relatórios(Profissionais)', ['controller' => 'Faturamentos', 'action' => 'equipes'], ['escape' => false]) ?>
                                </li>
                            <?php endif; ?>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-close"></i> Encerrar Faturas', ['controller' => 'Faturamentos', 'action' => 'listar'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-check"></i> Faturas Encerradas', ['controller' => 'Faturamentos', 'action' => 'all'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Gestão de Faturas', ['controller' => 'Faturamentos', 'action' => 'gestao_faturas'], ['escape' => false]) ?>
                            </li>
                        </ul>
                    </li>
                    <?php if ($this->Configuracao->mostraMenuProdutividade()): ?>
                        <li class="dropdown dropdown-submenu"><a href="#" class="dropdown-toggle"
                                                                 data-toggle="dropdown">Produtividade</a>
                            <ul class="dropdown-menu">
                                <?php if ($this->Configuracao->mostraMenuProdutividadeExecutante()): ?>
                                    <li><?= $this->Html->link('<i class="fa fa-file-text-o"></i> Executantes', ['controller' => 'ProdutividadeProfissionais', 'action' => 'listarExecutantes'], ['escape' => false]) ?></li>
                                <?php endif; ?>

                                <?php if ($this->Configuracao->mostraMenuProdutividadeSolicitante()): ?>

                                    <li><?= $this->Html->link('<i class="fa fa-file-text-o"></i> Solicitantes', ['controller' => 'ProdutividadeProfissionais', 'action' => 'listarSolicitantes'], ['escape' => false]) ?></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-archive"></i> Fechamento de Caixa', ['controller' => 'Relatorios', 'action' => 'executante'], ['escape' => false]) ?>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Indicadores
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li class="dropdown dropdown-submenu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Gráficos</a>
                        <ul class="dropdown-menu">
                            <?php if ($this->Configuracao->mostraFluxo()): ?>
                                <li>
                                    <?= $this->Html->link('<i class="fa fa-archive"></i> Fluxo de Caixa', ['controller' => 'dashboardFinanceiro', 'action' => 'index'], ['escape' => false]) ?>
                                </li>
                            <?php endif; ?>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-bank"></i>  Faturamento', ['controller' => 'Faturamentos', 'action' => 'annualBilling'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-money"></i>  Financeiro', ['controller' => 'Estatisticas', 'action' => 'graficosFinanceiro'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-bar-chart"></i>  Evolução diária', ['controller' => 'Estatisticas', 'action' => 'graficosFinanceiroDiario'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-pie-chart"></i>  Indicadores', ['controller' => 'Indicadores', 'action' => 'index'], ['escape' => false]) ?>
                            </li>
                            <li>
                                <?= $this->Html->link('<i class="fa fa-calendar"></i>  Agendamentos', ['controller' => 'Indicadores', 'action' => 'all'], ['escape' => false]) ?>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-users"></i>  Pacientes', ['controller' => 'Indicadores', 'action' => 'pacientes'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-hospital-o"></i>  Procedimentos', ['controller' => 'Estatisticas', 'action' => 'index'], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?= $this->Html->link('<i class="fa fa-cubes"></i>  Colaboradores', ['controller' => 'Estatisticas', 'action' => 'rh'], ['escape' => false]) ?>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Utilitários
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?= $this->Html->link('<i class="fa fa-list-alt"></i> Meus Contatos', ['controller' => 'agendaTel', 'action' => 'index'], ['escape' => false]) ?>
                    </li>
                    <?php if($this->ConfiguracaoNf->hibilitacao()): ?>
                        <li>
                            <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Notas fiscais', ['controller' => 'Nfs', 'action' => 'index'], ['escape' => false]) ?>
                        </li>
                    <?php endif; ?>

                </ul>
            </li>
            <?php if($this->Configuracao->mostraMenuSuperiorSUS()): ?>
                <li class="dropdown">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">SUS
                        <span class="caret"></span>
                    </a>
                    <ul role="menu" class="dropdown-menu">
                        <li>
                            <?= $this->Html->link('<i class="fa fa-list-alt"></i> Relatório', ['controller' => 'atendimentos', 'action' => 'relatorio_sus', '?' => ['ajax' => 1, 'fisrt' => 1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg', 'load-script' => 'Atendimentos']) ?>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>

        </ul>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a href="http://g2isolucoes.com.br/tickets" target="_blank" data-toggle="tooltip"
                   data-placement="bottom" title="Suporte">
                    <i class="fa fa-gears"></i>
                </a>
            </li>
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>
                    <span class="label label-warning">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <!--<a href="profile.html" class="pull-left">
                                <img alt="image" class="img-circle" src="img/a7.jpg">
                            </a>
                            <div>
                                <small class="pull-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>-->
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>
                    <span class="label label-primary">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <!--<a href="profile.html" class="pull-left">
                                <img alt="image" class="img-circle" src="img/a7.jpg">
                            </a>
                            <div>
                                <small class="pull-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>-->
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <div class="client-avatar">
                    <?php if (!empty($user_image[0]->caminho) && !empty($user_image[0]->caminho_dir)) {
                        $url = 'users/caminho/' . $user_image[0]->caminho_dir . '/' . $user_image[0]->caminho;
                        echo $this->Html->image($url, ['alt' => @$user_logado['caminho']]);
                    } else {
                        echo $this->Html->image('content/img/empty-avatar.png', ['alt' => @$user_logado['nome'], 'class' => 'img-circle']);
                    }
                    ?>
                </div>
            </li>
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <?php echo $user_logado['nome']; ?>
                    <span class="caret"></span>
                </a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?php echo $this->Html->link('<i class="fa  fa-pencil-square-o"></i> Meu cadastro', ['controller' => 'Users', 'action' => 'edit', $user_logado['id']], ['escape' => false]) ?>
                    </li>
                    <li>
                        <?php echo $this->Html->link('<i class="fa fa-sign-out"></i> Sair', ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]); ?>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
<div id="right-sidebar" style="display: none" role="adm"
     class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
    <div class="sidebar-container" full-scroll>
        <ul class="nav nav-tabs navs-2" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                    Administração
                </a>
            </li>
            <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="background: #2f4050">
            <div role="tabpanel" class="tab-pane active" id="home">
                <div class="sidebar-title">
                    <h3>
                        <div class="text-right">
                            <span role="open-adm" class="animatedClick" data-target='clickExample'>
                                <i class="fa fa-close"></i>
                            </span>
                        </div>
                        <i class="fa fa-gears"></i> Ajustes
                    </h3>
                    <small><i class="fa fa-tim"></i> Área do administrador.</small>
                </div>
                <nav class="navbar-default navbar-static" role="tablist">
                    <div class="sidebar-collapse">
                        <ul class="nav">
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Agenda' aria-expanded="false" aria-controls='Agenda'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Agenda</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Agenda" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Grupo Agendas', ['controller' => 'GrupoAgendas', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Procedimentos', ['controller' => 'AgendaCadprocedimentos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipos', ['controller' => 'TipoAgendas', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Situações', ['controller' => 'SituacaoAgendas', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Medicos' aria-expanded="false" aria-controls='Medicos'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Profissionais</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Medicos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Listar', ['controller' => 'MedicoResponsaveis', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'MedicoResponsaveis', 'action' => 'add'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Solicitantes' aria-expanded="false"
                                   aria-controls='Solicitantes'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Solicitantes</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Solicitantes" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Listar', ['controller' => 'Solicitantes', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Faturamento' aria-expanded="false"
                                   aria-controls='Faturamento'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Faturamento</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Faturamento" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li class="nav nav-second-level" style="background-color: white;">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-list="collapse" href='#conveniosImpostos' aria-expanded="false"
                                               aria-controls='Faturamento'>
                                                <span class="nav-label">Convenios/Impostos</span>
                                                <span class="fa fa-caret-right"></span>
                                            </a>
                                            <div id="conveniosImpostos" class="panel-collapse collapse" role="tabpanel"
                                                 aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                                 style="height: 0px; background-color: white;">
                                                <ul class="nav nav-second-level">
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Convênios', ['controller' => 'Convenios', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo Convênios', ['controller' => 'TipoConvenios', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Criar Imposto', ['controller' => 'convenio_tipoimposto', 'action' => 'add'], ['escape' => false]); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="nav nav-second-level" style="background-color: white;">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-list="collapse" href='#situacaoOrigens' aria-expanded="false"
                                               aria-controls='Faturamento'>
                                                <span class="nav-label">Situação/Origens</span>
                                                <span class="fa fa-caret-right"></span>
                                            </a>
                                            <div id="situacaoOrigens" class="panel-collapse collapse" role="tabpanel"
                                                 aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                                 style="height: 0px; background-color: white;">
                                                <ul class="nav nav-second-level">
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Faturas', ['controller' => 'SituacaoFaturas', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Faturas Encerradas', ['controller' => 'SituacaoFaturaencerramentos', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Recebimentos', ['controller' => 'SituacaoRecebimentos', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Origens', ['controller' => 'Origens', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="nav nav-second-level" style="background-color: white;">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-list="collapse" href='#procedimentos' aria-expanded="false"
                                               aria-controls='Faturamento'>
                                                <span class="nav-label">Procedimentos</span>
                                                <span class="fa fa-caret-right"></span>
                                            </a>
                                            <div id="procedimentos" class="panel-collapse collapse" role="tabpanel"
                                                 aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                                 style="height: 0px; background-color: white;">
                                                <ul class="nav nav-second-level">
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Preço Procedimentos', ['controller' => 'PrecoProcedimentos', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Grupos de Procedimentos', ['controller' => 'GrupoProcedimentos', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Procedimentos', ['controller' => 'Procedimentos', 'action' => 'index'], ['escape' => false]); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="nav nav-second-level" style="background-color: white;">
                                            <a class="collapsed" role="button" data-toggle="collapse"
                                               data-list="collapse" href='#matMed' aria-expanded="false"
                                               aria-controls='Faturamento'>
                                                <span class="nav-label">Mat/Med</span>
                                                <span class="fa fa-caret-right"></span>
                                            </a>
                                            <div id="matMed" class="panel-collapse collapse" role="tabpanel"
                                                 aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                                 style="height: 0px; background-color: white;">
                                                <ul class="nav nav-second-level">
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> MAT/MED', ['controller' => 'EstqArtigos', 'action' => 'index'], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-caret-right"></i> Kit/Pacote', ['controller' => 'FaturaKit', 'action' => 'index'], ['class' => $this->Configuracao->MatMedFatura(), 'escape' => false, 'target' => '_blank']); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Indicacao' aria-expanded="false" aria-controls='Indicacao'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Indicações</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Indicacao" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Listar', ['controller' => 'Indicacao', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Indicacao', 'action' => 'add'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Tipos' aria-expanded="false" aria-controls='Tipos'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Tipos</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Tipos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <!--<li><?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo de Agendas', ['controller' => 'TipoAgendas', 'action' => 'index'], ['escape' => false]); ?></li>-->
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo de anexos', ['controller' => 'TipoAnexos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo de Atendimentos', ['controller' => 'TipoAtendimentos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <!--<li><?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo de Convênios', ['controller' => 'TipoConvenios', 'action' => 'index'], ['escape' => false]); ?></li>-->
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo de Procedimento', ['controller' => 'TipoHistorias', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Estado civil', ['controller' => 'EstadoCivis', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Grau Parentescos', ['controller' => 'GrauParentescos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Atendimentos - Modelos de Documentos', ['controller' => 'AtendimentoModeloDocumentos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Prontuario' aria-expanded="false" aria-controls='Prontuario'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Prontuários</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Prontuario" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo - Histórico de Consultas', ['controller' => 'TipoHistoriaModelos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Tipo de documentos', ['controller' => 'TipoDocumentos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Unidades' aria-expanded="false" aria-controls='Unidades'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Unidades</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Unidades" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Listar', ['controller' => 'Unidades', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Unidades', 'action' => 'add'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Laudos' aria-expanded="false" aria-controls='Laudos'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Laudo</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Laudos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Grupos', ['controller' => 'TextoGrupos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Textos', ['controller' => 'Textos', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Procedimento detalhes', ['controller' => 'ProcedimentoCaddetalhes', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="sidebar-title">
                    <h3>
                        <div class="text-right">
                            <span role="open-adm" class="animatedClick" data-target='clickExample'>
                                <i class="fa fa-close"></i>
                            </span>
                        </div>
                        <i class="fa fa-users"></i> Usuário
                    </h3>
                    <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                </div>
                <nav class="navbar-default navbar-static" role="tablist">
                    <div class="sidebar-collapse">
                        <ul class="nav">
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Users' aria-expanded="false" aria-controls='Users'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Usuários</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="Users" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Listar', ['controller' => 'Users', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Users', 'action' => 'add'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#GrupoUsuarios' aria-expanded="false"
                                   aria-controls='Users'>
                                    <i class="fa fa-th-large"></i>
                                    <span class="nav-label">Grupos de Usuários</span>
                                    <span class="fa arrow"></span>
                                </a>
                                <div id="GrupoUsuarios" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-caret-right"></i> Listar', ['controller' => 'GrupoUsuarios', 'action' => 'index'], ['escape' => false]); ?>
                                        </li>
                                        <li>
                                            <?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'GrupoUsuarios', 'action' => 'add'], ['escape' => false]); ?>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>