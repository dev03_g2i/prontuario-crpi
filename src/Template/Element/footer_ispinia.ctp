<?php
$root = strstr(ROOT,'public_html');
$root = strstr($root,'/');


if($root=="") {
    $site_path = dirname($_SERVER["SCRIPT_NAME"]);
    if ($site_path == '/')
        $site_path = '';
    $pos = strripos($site_path, "webroot");
    $ur = substr($site_path,0,$pos);
    if(substr($ur,-1)=="/") {
        $root = substr($ur,0,-1);
    }else{
        $root = $ur;
    }
}

echo "<script>var site_path =\"".$root."\";</script>";
?>

<?= $this->Html->script('ispinia/jquery-2.1.1',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('bootstrap',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/metisMenu/jquery.metisMenu',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/slimscroll/jquery.slimscroll.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/slimscroll/jquery.slimscroll.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/inspinia',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/pace/pace.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/mask/jquery.mask.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/maskmoney/jquery.maskMoney',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/datetimepicker/moment-with-locales',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/datetimepicker/bootstrap-datetimepicker.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/bootstrap_message/bootstrap-dialog',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/bootstrap3-editable/js/bootstrap-editable',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/select2/select2',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/select2/pt-BR',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/jquery_form/jquery.form.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/css3_animations/css3-animate-it',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/iCheck/icheck',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/steps/jquery.steps.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/validate/jquery.validate.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('jquery.cep.js',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('extensions',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/input_file/fileinput',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/input_file/fileinput_locale_pt-BR',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/summernote/dist/summernote',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/summernote/dist/lang/summernote-pt-BR',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/blueimp/jquery.blueimp-gallery.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/fullcalendar/moment.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/fullcalendar/jquery-ui.custom.min',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/fullcalendar/fullcalendar',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/fullcalendar/pt-br',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/qtip/jquery.qtip',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ispinia/plugins/chart/Chart',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('ckeditor/ckeditor',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('misc',['block' => 'scriptBottom']) ?>
<?= $this->Html->script('function',['block' => 'scriptBottom']) ?>

<?= $this->fetch('scriptBottom') ?>
<?= $this->fetch('scriptLast') ?>

