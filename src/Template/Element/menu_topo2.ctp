<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>  <span class="label label-warning">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <!--<a href="profile.html" class="pull-left">
                                <img alt="image" class="img-circle" src="img/a7.jpg">
                            </a>
                            <div>
                                <small class="pull-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>-->
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  <span class="label label-primary">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <!--<a href="profile.html" class="pull-left">
                                <img alt="image" class="img-circle" src="img/a7.jpg">
                            </a>
                            <div>
                                <small class="pull-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>-->
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>

            <li>
                <div class="client-avatar">
                    <?php if (!empty($user_image[0]->caminho) && !empty($user_image[0]->caminho_dir)) {
                        $url = 'users/caminho/'.$user_image[0]->caminho_dir.'/'.$user_image[0]->caminho;
                        echo $this->Html->image($url, ['alt' => @$user_logado['caminho']]);
                    } else {
                        echo $this->Html->image('content/img/empty-avatar.png', ['alt' => @$user_logado['nome'], 'class' => 'img-circle']);
                    }
                    ?>
                </div>
            </li>

            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo $user_logado['nome'];?> <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?php echo $this->Html->link('<i class="fa  fa-pencil-square-o"></i> Meu cadastro', ['controller' => 'Users', 'action' => 'edit', $user_logado['id']], ['escape' => false])?>
                    </li>
                    <li><?php echo $this->Html->link('<i class="fa fa-sign-out"></i> Sair', ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]); ?></li>
                </ul>
            </li>

        </ul>
    </nav>
</div>