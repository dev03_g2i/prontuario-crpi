<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="container-fluid">

            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            </div>

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <!--<a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div>
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>-->
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="#">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">0</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <!--<a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div>
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>-->
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="#">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>

                <li>
                    <a role="open-adm" class="animatedClick" data-target='clickExample'>
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>

                <li>
                    <div class="client-avatar">
                        <?php if (!empty($user_logado['caminho'])) {
                            echo $this->Html->image('/files/users/caminho/' . $user_logado['caminho_dir'] . '/' . $user_logado['caminho'], ['class' => 'img-circle', 'title' => $user_logado['nome'], 'alt' => $user_logado['nome'], 'toggle' => 'tooltip', 'data-placement' => 'bottom']);
                        } else {
                            echo $this->Html->image('content/img/empty-avatar.png', ['alt' => @$user_logado['nome'], 'class' => 'img-circle']);
                        }
                        ?>
                    </div>
                </li>

                <li class="dropdown">
                    <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo $user_logado['nome'];?> <span class="caret"></span></a>
                    <ul role="menu" class="dropdown-menu">
                        <li>
                            <?php echo $this->Html->link('<i class=""></i> Meu cadastro', ['controller' => 'Users', 'action' => 'cadastro', $user_logado['id']], ['escape' => false])?>
                        </li>
                        <li><?php echo $this->Html->link('<i class="fa fa-sign-out"></i> Sair', ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]); ?></li>
                    </ul>
                </li>

            </ul>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a role="open-adm" class="animatedClick" data-target='clickExample'>
                    <i class="fa fa-tasks"></i>
                </a>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>  <span class="label label-warning">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <!--<a href="profile.html" class="pull-left">
                                <img alt="image" class="img-circle" src="img/a7.jpg">
                            </a>
                            <div>
                                <small class="pull-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>-->
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  <span class="label label-primary">0</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <!--<a href="profile.html" class="pull-left">
                                <img alt="image" class="img-circle" src="img/a7.jpg">
                            </a>
                            <div>
                                <small class="pull-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>-->
                        </div>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="#">
                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>

            <li>
                <div class="client-avatar">
                    <?php if (!empty($user_image[0]->caminho) && !empty($user_image[0]->caminho_dir)) {
                        $url = 'users/caminho/'.$user_image[0]->caminho_dir.'/'.$user_image[0]->caminho;
                        echo $this->Html->image($url, ['alt' => @$user_logado['caminho']]);
                    } else {
                        echo $this->Html->image('content/img/empty-avatar.png', ['alt' => @$user_logado['nome'], 'class' => 'img-circle']);
                    }
                    ?>
                </div>
            </li>

            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"> <?php echo $user_logado['nome'];?> <span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <li>
                        <?php echo $this->Html->link('<i class="fa  fa-pencil-square-o"></i> Meu cadastro', ['controller' => 'Users', 'action' => 'edit', $user_logado['id']], ['escape' => false])?>
                    </li>
                    <li><?php echo $this->Html->link('<i class="fa fa-sign-out"></i> Sair', ['controller' => 'Users', 'action' => 'logout'], ['escape' => false]); ?></li>
                </ul>
            </li>

        </ul>
    </nav>
</div>

<div id="right-sidebar" style="display: none" role="adm"
     class="sidebar-open animated fadeOutRight clickExample bounceInRight goAway">
    <div class="sidebar-container" full-scroll>

        <ul class="nav nav-tabs navs-2" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                    Administração
                </a>
            </li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Usuários</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content" style="background: #2f4050">
            <div role="tabpanel" class="tab-pane active" id="home">
                <div class="sidebar-title">
                    <h3>
                        <div class="text-right">
                        <span role="open-adm" class="animatedClick" data-target='clickExample'>
                            <i class="fa fa-close"></i>
                        </span>
                        </div>
                        <i class="fa fa-gears"></i> Ajustes
                    </h3>
                    <small><i class="fa fa-tim"></i> Área do administrador.</small>

                </div>
                <nav class="navbar-default navbar-static" role="tablist">
                    <div class="sidebar-collapse">
                        <ul class="nav">
                            <!--<li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Configuracoes' aria-expanded="false" aria-controls='Configuracoes'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Configurações</span><span
                                        class="fa arrow"></span></a>

                                <div id="Configuracoes" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Configuracoes', 'action' => 'index'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>-->

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Convenios' aria-expanded="false" aria-controls='Convenios'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Convênios</span><span
                                        class="fa arrow"></span></a>

                                <div id="Convenios" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Convenios', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Convenios', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>


                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Medicos' aria-expanded="false" aria-controls='Medicos'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Profissionais</span><span
                                        class="fa arrow"></span></a>

                                <div id="Medicos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'MedicoResponsaveis', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'MedicoResponsaveis', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>


                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Solicitantes' aria-expanded="false" aria-controls='Solicitantes'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Solicitantes</span><span
                                        class="fa arrow"></span></a>

                                <div id="Solicitantes" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Solicitantes', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Solicitantes', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Procedimentos' aria-expanded="false" aria-controls='Procedimentos'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Procedimentos</span><span
                                        class="fa arrow"></span></a>

                                <div id="Procedimentos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Procedimentos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Procedimentos', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Origens' aria-expanded="false" aria-controls='Origens'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Origens</span><span
                                        class="fa arrow"></span></a>

                                <div id="Origens" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Origens', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Origens', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Indicacao' aria-expanded="false" aria-controls='Indicacao'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Indicações</span><span
                                        class="fa arrow"></span></a>

                                <div id="Indicacao" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Indicacao', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Indicacao', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#EstadoCivis' aria-expanded="false" aria-controls='EstadoCivis'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Estado Civil</span><span
                                        class="fa arrow"></span></a>

                                <div id="EstadoCivis" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'EstadoCivis', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'EstadoCivis', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#GrauParentescos' aria-expanded="false" aria-controls='GrauParentescos'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Grau Parentesco</span><span
                                        class="fa arrow"></span></a>

                                <div id="GrauParentescos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'GrauParentescos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Link Vinculo', ['controller' => 'LinkLigacoes', 'action' => 'index'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Grupos' aria-expanded="false" aria-controls='Grupos'>
                                    <i class="fa fa-th-large"></i><span
                                        class="nav-label">Grupos</span><span class="fa arrow"></span></a>

                                <div id="Grupos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Grupos de Procedimentos', ['controller' => 'GrupoProcedimentos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Grupos de Agendas', ['controller' => 'GrupoAgendas', 'action' => 'index'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>


                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#PrecoProcedimentos' aria-expanded="false" aria-controls='PrecoProcedimentos'>
                                    <i class="fa fa-th-large"></i><span
                                        class="nav-label">Preco Procedimentos</span><span class="fa arrow"></span></a>

                                <div id="PrecoProcedimentos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'PrecoProcedimentos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'PrecoProcedimentos', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Situacoes' aria-expanded="false" aria-controls='Situacoes'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Situações</span><span
                                        class="fa arrow"></span></a>

                                <div id="Situacoes" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Situação Agenda', ['controller' => 'SituacaoAgendas', 'action' => 'index'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Tipos' aria-expanded="false" aria-controls='Tipos'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Tipos</span><span
                                        class="fa arrow"></span></a>

                                <div id="Tipos" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Tipo de Agendas', ['controller' => 'TipoAgendas', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Tipo de anexos', ['controller' => 'TipoAnexos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Tipo de Atendimentos', ['controller' => 'TipoAtendimentos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Tipo de Convênios', ['controller' => 'TipoConvenios', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Tipo de documentos', ['controller' => 'TipoDocumentos', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Tipo de Procedimento', ['controller' => 'TipoHistorias', 'action' => 'index'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Unidades' aria-expanded="false" aria-controls='Unidades'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Unidades</span><span
                                        class="fa arrow"></span></a>

                                <div id="Unidades" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Unidades', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Unidades', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                        </ul>
                    </div>
                </nav>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                <div class="sidebar-title">
                    <h3>
                        <div class="text-right">
                        <span role="open-adm" class="animatedClick" data-target='clickExample'>
                            <i class="fa fa-close"></i>
                        </span>
                        </div>
                        <i class="fa fa-users"></i> Usuário
                    </h3>
                    <small><i class="fa fa-tim"></i>Área do Usuário.</small>
                </div>
                <nav class="navbar-default navbar-static" role="tablist">
                    <div class="sidebar-collapse">
                        <ul class="nav">
                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#Users' aria-expanded="false" aria-controls='Users'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Usuários</span><span
                                        class="fa arrow"></span></a>

                                <div id="Users" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'Users', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'Users', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>

                            <li>
                                <a class="collapsed" role="button" data-toggle="collapse" data-list="collapse"
                                   href='#GrupoUsuarios' aria-expanded="false" aria-controls='Users'>
                                    <i class="fa fa-th-large"></i><span class="nav-label">Grupos de Usuários</span><span
                                        class="fa arrow"></span></a>

                                <div id="GrupoUsuarios" class="panel-collapse collapse" role="tabpanel"
                                     aria-labelledby="collapseListGroupHeading1" aria-expanded="false"
                                     style="height: 0px;">
                                    <ul class="nav nav-second-level">
                                        <li><?= $this->Html->link('<i class="fa fa-th-large"></i> Listar', ['controller' => 'GrupoUsuarios', 'action' => 'index'], ['escape' => false]); ?></li>
                                        <li><?= $this->Html->link('<i class="fa fa-plus-square"></i> Cadastrar', ['controller' => 'GrupoUsuarios', 'action' => 'add'], ['escape' => false]); ?></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>