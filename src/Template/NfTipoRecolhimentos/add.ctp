<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Tipo Recolhimentos</h2>
            <ol class="breadcrumb">
                <li>Nf Tipo Recolhimentos</li>
                <li class="active">
                    <strong>
                                                Cadastrar Nf Tipo Recolhimentos
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="nfTipoRecolhimentos form">
                            <?= $this->Form->create($nfTipoRecolhimento) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Nf Tipo Recolhimento') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

