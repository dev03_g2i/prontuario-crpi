<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Bem vindo Gestor</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Dashboard', ['type' => 'get']) ?>
                                <div class="col-sm-6 no-padding">
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('Grupos', ['id' => 'grupos', 'label'=> 'Grupos', 'type' => 'select', 'options' => $groups, 'empty' => 'Selecione', 'default' => 0]);?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('Grupos', ['id' => 'empresas', 'label'=> 'Empresas', 'type' => 'select', 'empty' => 'Selecione um grupo', 'disabled'] )?>
                                    </div>
                                </div>
                                <div class="col-sm-6 no-padding" style="border-left: solid thin #e7eaec;">
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('data', ['id' => 'date-for-cockpit', 'default' => $dateField, 'label' => 'Data', 'type' => 'text', 'class' => "datepicker-month", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'cockpit-search', 'class' => 'btn btn-default m-t-23', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row" id="first-row">
        <section class="col-sm-6 no-padding connectedSortable" id="dashboardSection1">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    Movimentos por banco/mês
                </div>
                <div class="ibox-content m-r-5">
                    <div class="col-sm-12 background-lightgrey p-tb-10">
                        <div class="col-sm-12 background-lightgrey" id="changeTable" style="display: inline-block;">
                            <i class="m-r-5 fa fa-caret-down" id="arrowDashboardSection1"></i>
                            <span class="font-14"> Saldo - Conta Principal</span>
                            <small id="actualCompany" class="m-l-5 small-617AB5">(Total)</small>
                            <?php if($resultBankMoves[0] >= 0): ?>
                            <h3 class="d-credit no-margin no-padding m-t-10">R$
                                <?= number_format($resultBankMoves[0], 2, ',', '.') ?>
                            </h3>
                            <?php else:?>
                            <h3 class="d-debit no-margin no-padding m-t-10">R$
                                <?= number_format($resultBankMoves[0], 2, ',', '.') ?>
                            </h3>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-12 background-lightgrey table-responsive d-block" id="divTable">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Banco</th>
                                        <th>Sal. Anterior</th>
                                        <th>Saldo</th>
                                        <th>Movimento</th>
                                        <th>Últ. Lanç.</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($bankMoves as $move): ?>
                                    <tr>
                                        <td>
                                            <?= $move->fin_banco->nome ?>
                                        </td>
                                        <?php if($move->saldoInicial >= 0): ?>
                                        <td class="d-credit">
                                            <?= number_format($move->saldoInicial, 2, ',', '.') ?>
                                        </td>
                                        <?php else:?>
                                        <td class="d-debit">
                                            <?= number_format($move->saldoInicial, 2, ',', '.') ?>
                                        </td>
                                        <?php endif;?>
                                        <?php if(($move->soma_movimentos_saldos) >= 0): ?>
                                        <td class="d-credit">
                                            <?= number_format(($move->soma_movimentos_saldos), 2, ',', '.') ?>
                                        </td>
                                        <?php else:?>
                                        <td class="d-debit">
                                            <?= number_format(($move->soma_movimentos_saldos), 2, ',', '.') ?>
                                        </td>
                                        <?php endif;?>
                                        <td>
                                            <?= $move->data_movimento ?>
                                        </td>
                                        <td>
                                            <?= !empty($move->fin_movimentos) && (!empty(end($move->fin_movimentos)->data) || isset(end($move->fin_movimentos)->data)) ? end($move->fin_movimentos)->data : 'Não teve movimento'?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    <tfoot>
                                        <tr>
                                            <td>
                                                <strong class="text-grey">Todas as contas</strong>
                                            </td>
                                            <?php if($resultBankMoves[1] >= 0): ?>
                                            <td>
                                                <strong class="d-credit">
                                                    <?= number_format($resultBankMoves[1], 2, ',', '.') ?>
                                                </strong>
                                            </td>
                                            <?php else: ?>
                                            <td>
                                                <strong class="d-debit">
                                                    <?= number_format($resultBankMoves[1], 2, ',', '.') ?>
                                                </strong>
                                            </td>
                                            <?php endif; ?>
                                            <?php if($resultBankMoves[0] >= 0): ?>
                                            <td>
                                                <strong class="d-credit">
                                                    <?= number_format($resultBankMoves[0], 2, ',', '.') ?>
                                                </strong>
                                            </td>
                                            <?php else: ?>
                                            <td>
                                                <strong class="d-debit">
                                                    <?= number_format($resultBankMoves[0], 2, ',', '.') ?>
                                                </strong>
                                            </td>
                                            <?php endif; ?>
                                        </tr>
                                    </tfoot>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="col-sm-6 no-padding connectedSortable">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    Gráfico
                </div>
                <div class='ibox-content m-l-5'>
                    <div class="chart-container" style="position: relative;">
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="row" id="second-row">
        <div class="ibox-title">
            <strong>Saldo anterior: </strong>
            <?php if($lastBankFinalBalance < 0): ?>
            <strong class="d-debit">
                <?= number_format($lastBankFinalBalance, 2, ',', '.')?>
            </strong>
            <?php else:?>
            <strong class="d-credit">
                <?= number_format($lastBankFinalBalance, 2, ',', '.')?>
            </strong>
            <?php endif;?>
        </div>
        <section class="col-sm-7 no-padding connectedSortable">
            <div class=" col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding p-r-10">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="p-t-10 table-responsive no-padding background-white">
                                    <table class="table table-hover">
                                        <thead>
                                            <div class="col-sm-12 text-center">
                                                <strong>Realizado</strong>
                                            </div>
                                            <tr>
                                                <th>
                                                    <div class="text-center">Dia
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Entradas
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Saídas
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Transf
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Saldo do dia
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Saldo Acumulado
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($resultMoves[0] as $move): ?>
                                            <tr id="line">
                                                <td align="center" id="diaModal" value="<?= $move->dia ?>">
                                                    <?= $move->dia ?>
                                                </td>
                                                <?php if($move->entradas >= 0): ?>
                                                <td class="d-credit" align="right">
                                                    <?= number_format($move->entradas, 2, ',', '.') ?>
                                                </td>
                                                <?php else:?>
                                                <td class="d-debit" align="right">
                                                    <?= number_format($move->entradas, 2, ',', '.') ?>
                                                </td>
                                                <?php endif;?>
                                                <td class="d-debit" align="right"> 
                                                    <?= number_format($move->saidas, 2, ',', '.') ?>
                                                </td>
                                                <td align="right">
                                                    <span class="d-credit"><?= number_format($move->transferencia_c, 2, ',', '.')?></span> + <span class="d-debit"><?= number_format($move->transferencia_d, 2, ',', '.') ?></span>
                                                </td>
                                                <?php if($move->saldo >= 0): ?>
                                                <td class="d-credit" align="right">
                                                    <?= number_format($move->saldo, 2, ',', '.') ?>
                                                </td>
                                                <?php else:?>
                                                <td class="d-debit" align="right">
                                                    <?= number_format($move->saldo, 2, ',', '.') ?>
                                                </td>
                                                <?php endif;?>
                                                <?php if($move->saldoAcumulado >= 0): ?>
                                                <td class="d-credit" align="right">
                                                    <?= number_format($move->saldoAcumulado, 2, ',', '.') ?>
                                                </td>
                                                <?php else:?>
                                                <td class="d-debit" align="right">
                                                    <?= number_format($move->saldoAcumulado, 2, ',', '.') ?>
                                                </td>
                                                <?php endif;?>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <strong class="text-grey">Total</strong>
                                                </td>
                                                <?php if($resultMoves[1]->totalEntradas >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultMoves[1]->totalEntradas, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultMoves[1]->totalEntradas, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultMoves[1]->totalSaidas, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php if($resultMoves[1]->totalTransferencias >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultMoves[1]->totalTransferencias, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultMoves[1]->totalTransferencias, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                                <?php if($resultMoves[1]->totalSaldo >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultMoves[1]->totalSaldo, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultMoves[1]->totalSaldo, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                                <?php if($resultMoves[1]->totalAcumulado >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultMoves[1]->totalAcumulado, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultMoves[1]->totalAcumulado, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="col-sm-5 no-padding connectedSortable">
            <div class=" col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="p-t-10 table-responsive no-padding background-white">
                                    <table class="table table-hover">
                                        <thead>
                                            <div class="col-sm-12 text-center">
                                                <strong>Em Aberto</strong>
                                            </div>
                                            <tr>
                                                <th>
                                                    <div class="text-center">Dia
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Rec.Convênio
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Rec.Caixa
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Pagar
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Sal do dia
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Sal Acumulado
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="text-right">Sal Projetado
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($resultBills[0] as $resultBill): ?>
                                            <tr>
                                                <td align="center" id="billDay" value="<?= $resultBill->dia ?>">
                                                    <?= $resultBill->dia ?>
                                                </td>
                                                <td class="d-credit" align="right" id="receber-convenio">
                                                    <?= number_format($resultBill->contasReceberConvenio, 2, ',', '.')?>
                                                </td>
                                                <td class="d-credit" align="right" id="receber">
                                                    <?= number_format($resultBill->contasReceber, 2, ',', '.') ?>
                                                </td>
                                                <td class="d-debit" align="right" id="pagar">
                                                    <?= number_format($resultBill->contasPagar, 2, ',', '.') ?>
                                                </td>
                                                <?php if($resultBill->saldoDoDia >= 0): ?>
                                                <td class="d-credit" align="right">
                                                    <?= number_format($resultBill->saldoDoDia, 2, ',', '.') ?>
                                                </td>
                                                <?php else:?>
                                                <td class="d-debit" align="right">
                                                    <?= number_format($resultBill->saldoDoDia, 2, ',', '.') ?>
                                                </td>
                                                <?php endif;?>
                                                <?php if($resultBill->saldoAcumulado >= 0): ?>
                                                <td class="d-credit" align="right">
                                                    <?= number_format($resultBill->saldoAcumulado, 2, ',', '.') ?>
                                                </td>
                                                <?php else:?>
                                                <td class="d-debit" align="right">
                                                    <?= number_format($resultBill->saldoAcumulado, 2, ',', '.') ?>
                                                </td>
                                                <?php endif;?>
                                                <?php if($resultBill->saldoProjetado >= 0): ?>
                                                <td class="d-credit" align="right">
                                                    <?= number_format($resultBill->saldoProjetado, 2, ',', '.') ?>
                                                </td>
                                                <?php else:?>
                                                <td class="d-debit" align="right">
                                                    <?= number_format($resultBill->saldoProjetado, 2, ',', '.') ?>
                                                </td>
                                                <?php endif;?>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>
                                                    <strong class="text-grey">Total</strong>
                                                </td>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultBills[1]->totalContasReceberConvenio, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultBills[1]->totalContasReceber, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultBills[1]->totalContasPagar, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php if($resultBills[1]->totalSaldoDia >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultBills[1]->totalSaldoDia, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultBills[1]->totalSaldoDia, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                                <?php if($resultBills[1]->saldoAcumulado >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultBills[1]->saldoAcumulado, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultBills[1]->saldoAcumulado, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                                <?php if($resultBills[1]->saldoProjetado >= 0): ?>
                                                <td align="right">
                                                    <strong class="d-credit">
                                                        <?= number_format($resultBills[1]->saldoProjetado, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php else: ?>
                                                <td align="right">
                                                    <strong class="d-debit">
                                                        <?= number_format($resultBills[1]->saldoProjetado, 2, ',', '.') ?>
                                                    </strong>
                                                </td>
                                                <?php endif; ?>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('chart',['block' => 'scriptBottom']);
echo $this->Html->script('controllers/DashboardFinanceiro',['block' => 'scriptBottom']);
?>