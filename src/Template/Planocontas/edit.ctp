
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Planocontas</h2>
        <ol class="breadcrumb">
            <li>Planocontas</li>
            <li class="active">
                <strong>                        Editar Planocontas
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="planocontas form">
    <?= $this->Form->create($planoconta) ?>
    <fieldset>
                <legend><?= __('Editar Planoconta') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                echo $this->Form->input('classificacao');
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('status');
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('nome');
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('rateio');
                echo "</div>";
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

