

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Planocontas</h2>
        <ol class="breadcrumb">
            <li>Planocontas</li>
            <li class="active">
                <strong>Litagem de Planocontas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="planocontas">
    <h3><?= h($planoconta->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($planoconta->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($planoconta->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Classificacao') ?></th>
            <td><?= $this->Number->format($planoconta->classificacao) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= $this->Number->format($planoconta->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Rateio') ?></th>
            <td><?= $this->Number->format($planoconta->rateio) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

