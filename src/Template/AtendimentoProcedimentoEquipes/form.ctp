<?= $this->Form->create($atendimentoProcedimentoEquipe) ?>
    <div class="col-md-6">
        <?= $this->Form->input('atendimento_procedimento_id', ['label' => 'Procedimentos', 'empty' => 'Selecione', 'options' => $atendProcedimentos, 'class' => 'select2', 'required' => true]);?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('profissional_id', ['options' => $profissionais]); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('tipo_equipe_id', ['options' => $tipo_equipes]); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('tipo_equipegrau_id', ['id' => 'tipo_equipegrau', 'options' => $tipo_equipegrau]); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('porcentagem', ['id' => 'tipo_equipegrau_function_porcentual', 'type' => 'text']); ?>
    </div>
    <div class='col-md-6'>
        <?=$this->Form->input('configuracao_apuracao_id', ['options' => $configuracao_apuracao]); ?>
    </div>
    <div class='col-md-12'>
        <?=$this->Form->input('obs', ['rows' => 3]); ?>
    </div>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
<?= $this->Form->end() ?>