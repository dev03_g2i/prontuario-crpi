<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cadastrar Equipe</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <?php if(!empty($dados_atendimento)):?>
                        <div class="col-lg-9">
                            <h5>
                                <strong>Paciente: </strong>
                                <?=$dados_atendimento->atendimento->cliente->nome?>
                            </h5>
                            <h5>
                                <strong>Convênio: </strong>
                                <?=$dados_atendimento->atendimento->convenio->nome?>
                            </h5>
                        </div>
                        <div class="col-lg-3 text-right">
                            <h5>
                                <strong>Atendimento: </strong>
                                <?=$dados_atendimento->atendimento_id?>
                            </h5>
                            <h5>
                                <strong>Data: </strong>
                                <?=$dados_atendimento->atendimento->data?>
                            </h5>
                        </div>
                        <?php endif; ?>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="atendimentoProcedimentoEquipes form">
                            <?php require_once('form.ctp'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>