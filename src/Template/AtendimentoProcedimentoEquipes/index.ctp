<div class="this-place">
        <div class="white-bg page-heading">
            <?php if(!empty($dados_atendimento)):?>
                <div class="col-lg-9">
                    <h5><strong>Paciente: </strong><?=$dados_atendimento->atendimento->cliente->nome?></h5>
                    <h5><strong>Convênio: </strong><?=$dados_atendimento->atendimento->convenio->nome?></h5>
                </div>
                <div class="col-lg-3 text-right">
                    <h5><strong>Atendimento: </strong><?=$dados_atendimento->atendimento_id?></h5>
                    <h5><strong>Data: </strong><?=$dados_atendimento->atendimento->data?></h5>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('AtendimentoProcedimentoEquipe',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?= $this->Form->input('atendimento_procedimento_id', ['name'=>'AtendimentoProcedimentoEquipes__atendimento_procedimento_id', 'options' => $atendProcedimentos, 'class' => 'select2', 'empty' => 'Selecione']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 fill-content-search">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                    <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>

                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add', $dados_atendimento->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Equipes','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?= $this->Paginator->sort('atendimento_procedimento_id') ?></th>
                                            <th><?= $this->Paginator->sort('procedimento') ?></th>
                                            <th>
                                                <?= $this->Paginator->sort('ordem') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('profissional_id', 'Profissional') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('tipo_equipe_id', 'Equipe/Tipo') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('tipo_equipegrau_id', 'Grau') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('porcentagem') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('configuracao_apuracao_id', 'Apurar Para') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('obs') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_insert', 'Cadastrado Por') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_update', 'Alterado Por') ?>
                                            </th>                                            
                                            <th>
                                                <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($atendimentoProcedimentoEquipes as $atendimentoProcedimentoEquipe): ?>
                                        <tr>
                                            <td><?= $atendimentoProcedimentoEquipe->atendimento_procedimento_id ?></td>
                                            <td><?= (!empty($dados_atendimento)) ? $dados_atendimento->procedimento->nome : null ?></td>
                                            <td><?= $atendimentoProcedimentoEquipe->ordem ?></td>
                                            <td>
                                                <?= $atendimentoProcedimentoEquipe->has('medico_responsavei') ? $atendimentoProcedimentoEquipe->medico_responsavei->nome : '' ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoProcedimentoEquipe->has('tipo_equipe') ? $atendimentoProcedimentoEquipe->tipo_equipe->nome : '' ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoProcedimentoEquipe->has('tipo_equipegrau') ? $atendimentoProcedimentoEquipe->tipo_equipegrau->descricao : '' ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoProcedimentoEquipe->has('tipo_equipegrau') ? $atendimentoProcedimentoEquipe->tipo_equipegrau->porcentual : ''  ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoProcedimentoEquipe->has('configuracao_apuracao') ? $atendimentoProcedimentoEquipe->configuracao_apuracao->nome : '' ?>
                                            </td>
                                            <td>
                                                <?= $atendimentoProcedimentoEquipe->obs ?>
                                            </td>
                                            <td>
                                                <?= ($atendimentoProcedimentoEquipe->has('user_reg')? $atendimentoProcedimentoEquipe->user_reg->nome : null) ?>
                                            </td>
                                            <td>
                                                <?= ($atendimentoProcedimentoEquipe->has('user_alt')? $atendimentoProcedimentoEquipe->user_alt->nome : null) ?>
                                            </td>
                                            <td>
                                                <?= h($atendimentoProcedimentoEquipe->modified) ?>
                                            </td>
                                            <td class="actions">
                                                <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'atendimentoProcedimentoEquipes','action' => 'view', $atendimentoProcedimentoEquipe->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>-->
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'atendimentoProcedimentoEquipes','action' => 'edit', $atendimentoProcedimentoEquipe->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'atendimentoProcedimentoEquipes','action'=>'delete', $atendimentoProcedimentoEquipe->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                                <?= $this->Html->link('<i class="fa fa-money"></i>', 'javascript:script(0)',['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Recebimento','escape' => false,'class'=>'btn btn-xs btn-warning']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>