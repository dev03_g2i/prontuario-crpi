<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Atendimento Procedimento Equipes</h2>
            <ol class="breadcrumb">
                <li>Atendimento Procedimento Equipes</li>
                <li class="active">
                    <strong>Litagem de Atendimento Procedimento Equipes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Atendimento Procedimento Equipes</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Atendimento Procedimento') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->has('atendimento_procedimento') ? $this->Html->link($atendimentoProcedimentoEquipe->atendimento_procedimento->id, ['controller' => 'AtendimentoProcedimentos', 'action' => 'view', $atendimentoProcedimentoEquipe->atendimento_procedimento->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Medico Responsavei') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->has('medico_responsavei') ? $this->Html->link($atendimentoProcedimentoEquipe->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $atendimentoProcedimentoEquipe->medico_responsavei->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Tipo Equipe') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->has('tipo_equipe') ? $this->Html->link($atendimentoProcedimentoEquipe->tipo_equipe->id, ['controller' => 'TipoEquipes', 'action' => 'view', $atendimentoProcedimentoEquipe->tipo_equipe->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Tipo Equipegrau') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->has('tipo_equipegrau') ? $this->Html->link($atendimentoProcedimentoEquipe->tipo_equipegrau->id, ['controller' => 'TipoEquipegrau', 'action' => 'view', $atendimentoProcedimentoEquipe->tipo_equipegrau->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Configuracao Apuracao') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->has('configuracao_apuracao') ? $this->Html->link($atendimentoProcedimentoEquipe->configuracao_apuracao->id, ['controller' => 'ConfiguracaoApuracao', 'action' => 'view', $atendimentoProcedimentoEquipe->configuracao_apuracao->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situação Cadastro') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->has('situacao_cadastro') ? $this->Html->link($atendimentoProcedimentoEquipe->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $atendimentoProcedimentoEquipe->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($atendimentoProcedimentoEquipe->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Ordem') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($atendimentoProcedimentoEquipe->ordem) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Porcentagem') ?>
                                    </th>
                                    <td>
                                        <?= $atendimentoProcedimentoEquipe->porcentagem; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('User Insert') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($atendimentoProcedimentoEquipe->user_insert) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('User Update') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($atendimentoProcedimentoEquipe->user_update) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($atendimentoProcedimentoEquipe->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($atendimentoProcedimentoEquipe->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Obs') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(h($atendimentoProcedimentoEquipe->obs)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>