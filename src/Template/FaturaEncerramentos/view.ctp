
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Encerramentos</h2>
        <ol class="breadcrumb">
            <li>Fatura Encerramentos</li>
            <li class="active">
                <strong>Litagem de Fatura Encerramentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Encerramentos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Competencia') ?></th>
                                <td><?= h($faturaEncerramento->competencia) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Convenio') ?></th>
                                                                <td><?= $faturaEncerramento->has('convenio') ? $this->Html->link($faturaEncerramento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $faturaEncerramento->convenio->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Unidade') ?></th>
                                <td><?= h($faturaEncerramento->unidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Total Bruto') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->total_bruto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Num Exames') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->num_exames) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Taxas') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->taxas) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Descontos') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->descontos) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Qtd Total') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->qtd_total) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Id') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->user_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Fatura') ?></th>
                                <td><?= $this->Number->format($faturaEncerramento->situacao_fatura) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Vencimento') ?></th>
                                                                <td><?= h($faturaEncerramento->vencimento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Encerramento') ?></th>
                                                                <td><?= h($faturaEncerramento->data_encerramento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaEncerramento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaEncerramento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($faturaEncerramento->observacao)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


