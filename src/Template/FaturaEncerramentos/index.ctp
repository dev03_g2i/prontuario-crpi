<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Encerramentos</h2>
            <ol class="breadcrumb">
                <li>Fatura Encerramentos</li>
                <li class="active">
                    <strong>Litagem de Fatura Encerramentos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <!--<div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaEncerramento',['type'=>'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('competencia',['name'=>'FaturaEncerramentos__competencia']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('vencimento', ['name'=>'FaturaEncerramentos__vencimento','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('data_encerramento', ['name'=>'FaturaEncerramentos__data_encerramento','type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('total_bruto',['name'=>'FaturaEncerramentos__total_bruto','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('num_exames',['name'=>'FaturaEncerramentos__num_exames']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('convenio_id', ['name'=>'FaturaEncerramentos__convenio_id','data'=>'select','controller'=>'convenios','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('unidade',['name'=>'FaturaEncerramentos__unidade']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('taxas',['name'=>'FaturaEncerramentos__taxas','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('descontos',['name'=>'FaturaEncerramentos__descontos','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('qtd_total',['name'=>'FaturaEncerramentos__qtd_total']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('user_id',['name'=>'FaturaEncerramentos__user_id']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('situacao_fatura',['name'=>'FaturaEncerramentos__situacao_fatura']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>-->
                <div class="text-right">
                    <p>
                        <!--<?= $this->Html->link($this->Html->icon('plus').' Cadastrar Fatura Encerramentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fatura Encerramentos','class'=>'btn btn-primary','escape' => false]) ?>-->
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fatura Encerramentos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('competencia') ?></th>
                                        <th><?= $this->Paginator->sort('vencimento') ?></th>
                                        <th><?= $this->Paginator->sort('data_encerramento') ?></th>
                                        <th><?= $this->Paginator->sort('total_bruto') ?></th>
                                        <!--<th><?= $this->Paginator->sort('num_exames') ?></th>-->
                                        <th><?= $this->Paginator->sort('convenio_id') ?></th>
                                        <th><?= $this->Paginator->sort('unidade_id') ?></th>
                                        <th><?= $this->Paginator->sort('taxas') ?></th>
                                        <th><?= $this->Paginator->sort('descontos') ?></th>
                                        <!--<th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>-->
                                        <th><?= $this->Paginator->sort('qtd_total') ?></th>
                                        <!--<th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>-->
                                        <th><?= $this->Paginator->sort('situacao_fatura', 'Situacão') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaEncerramentos as $faturaEncerramento): ?>
                                        <tr>
                                            <td><?= $this->Number->format($faturaEncerramento->id) ?></td>
                                            <td><?= h($faturaEncerramento->competencia) ?></td>
                                            <td><?= h($faturaEncerramento->vencimento) ?></td>
                                            <td><?= h($faturaEncerramento->data_encerramento) ?></td>
                                            <td><?= $this->Number->format($faturaEncerramento->total_bruto) ?></td>
                                            <!--<td><?= $this->Number->format($faturaEncerramento->num_exames) ?></td>-->
                                            <td><?= $faturaEncerramento->has('convenio') ? $this->Html->link($faturaEncerramento->convenio->nome, ['controller' => 'Convenios', 'action' => 'view', $faturaEncerramento->convenio->id]) : '' ?></td>
                                            <td><?= h($faturaEncerramento->unidade->nome) ?></td>
                                            <td><?= $this->Number->format($faturaEncerramento->taxas) ?></td>
                                            <td><?= $this->Number->format($faturaEncerramento->descontos) ?></td>
                                            <!--<td><?= h($faturaEncerramento->created) ?></td>
                                            <td><?= h($faturaEncerramento->modified) ?></td>-->
                                            <td><?= $this->Number->format($faturaEncerramento->qtd_total) ?></td>
                                            <!--<td><?= $this->Number->format($faturaEncerramento->user_id) ?></td>-->
                                            <td><?= ($faturaEncerramento->has('situacao_faturaencerramento'))?$faturaEncerramento->situacao_faturaencerramento->nome:'' ?></td>
                                            <td class="actions">
                                                <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaEncerramentos','action' => 'view', $faturaEncerramento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>-->
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaEncerramentos','action' => 'edit', $faturaEncerramento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturaEncerramentos','action'=>'delete', $faturaEncerramento->id],['onclick'=>'Deletar(\'faturaEncerramentos\','.$faturaEncerramento->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                                <?= $this->Html->link('<i class="fa fa-file-text-o"></i>',  ['controller'=>'Faturamentos','action'=>'rec-listar', $faturaEncerramento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Recebimentos','escape' => false,'class'=>'btn btn-xs btn-success','listen' => 'f', 'target' => '_blank']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
