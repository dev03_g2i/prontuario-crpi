    <div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Editar Fatura</h2>
            <ol class="breadcrumb">
                <li class="active">
                    <strong>N° Fatura:
                        <?=$faturaEncerramento->id?>
                    </strong>
                    <br>
                    <strong>Convênio:
                        <?=$faturaEncerramento->convenio->nome?>
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="faturaEncerramentos form">
                        <?= $this->Form->create($faturaEncerramento) ?>
                            <fieldset>
                                <div class="ibox-title fundoHeaders m-t-20">
                                    <h5 class="fontHeaders">Faturas</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-6 no-padding">
                                            <div class="col-sm-3">
                                                <?=$this->Form->input('competencia_mes', ['label' => 'Mês', 'options' => $meses, 'required' => 'required', 'empty' => 'Selecione', 'selected' => $faturaEncerramento->competencia_mes]);?>
                                            </div>
                                            <div class="col-sm-3">
                                                <?=$this->Form->input('competencia_ano', ['label' => 'Ano', 'options' => $anos, 'required' => 'required', 'empty' => 'Selecione']);?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?=$this->Form->input('competencia', ['readonly' => 'readonly']);?>
                                            </div>
                                        </div>
                                        <div class='col-sm-6'>
                                            <?= $this->Form->input('data_encerramento', ['label' => 'Dt.Encer/Entrega', 'type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($faturaEncerramento->data_encerramento,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon('calendar'), ['type' => 'button'])]]);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('vencimento', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($faturaEncerramento->vencimento,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon('calendar'), ['type' => 'button'])]]);?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('total_bruto',['readonly' => true, 'prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('Valor material',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'value' => $faturaEncerramento->valor_mat_med, 'readonly' => 'readonly']);?>
                                        </div>
                                        <div class='col-md-6'>
                                        <?= $this->Form->input('Valor equipe',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money', 'value' => $faturaEncerramento->valor_equipe, 'readonly' => 'readonly']);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('taxas',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('descontos',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('qtd_total', ['readonly' => true]);?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('unidade_id', ['label' => 'Unidade', 'options' => $branches, 'required' => 'required']);?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('situacao_faturaencerramento_id', ['label' => 'Situação Fatura', 'options' => $situacaoFaturaencerramento, 'selected' => $faturaEncerramento->situacao_faturaencerramento_id]);?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('observacao', ['rows' => 2]);?>
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-title fundoHeaders m-t-20">
                                    <h5 class="fontHeaders">Conferência</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('valor_recebido', ['label' => 'Total Recebido Conferência', 'readonly', 'value' => $this->Faturamento->somaValorRecebido($faturaEncerramento->id)]);?>
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox-title fundoHeaders m-t-20">
                                    <h5 class="fontHeaders">Recebimento</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('data_quitacao', ['label' => 'Data Quitação', 'type' => 'text', 'class' => 'datepicker', 'value' => !empty($faturaEncerramento->data_quitacao) ? $this->Time->format($faturaEncerramento->data_quitacao, 'dd/MM/Y') : '']);?>
                                        </div>
                                        <div class='col-md-6'>
                                            <?= $this->Form->input('vl_received_bank', ['label' => 'Valor Recebido Banco']);?>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class='col-md-6'>
                                 $this->Form->input('num_exames');
                                 </div>
                                <div class='col-md-6'>
                                 $this->Form->input('convenio_id', ['data'=>'select','controller'=>'convenios','action'=>'fill','data-value'=>$faturaEncerramento->convenio_id, 'empty' => 'selecione']);
                                 </div> -->
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class='col-md-6 m-t-20'>
                                            <div class='col-md-12' style='padding: 0'>
                                                <div class='col-md-6' style='padding: 0'>
                                                    <label>Criado por: </label>
                                                    <?= ($faturaEncerramento->user->nome) ? $faturaEncerramento->user->nome : '';?>
                                                </div>
                                                <div class='col-md-6' style='padding: 0'>
                                                    <?= ($faturaEncerramento->created) ? $faturaEncerramento->created : '';?>
                                                </div>
                                            </div>
                                            <?php if ($faturaEncerramento->user_edit):?>
                                            <div class='col-md-12' style='padding: 0'>
                                                <div class='col-md-6' style='padding: 0'>
                                                    <label>Alterado por: </label>
                                                    <?= $faturaEncerramento->user_edit->nome;?>
                                                </div>
                                                <div class='col-md-6' style='padding: 0'>
                                                    <?= ($faturaEncerramento->modified) ? $faturaEncerramento->modified : '';?>
                                                </div>
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-md-1 col-md-offset-9 text-right">
                                            <?= $this->Html->link('<i class="fa fa-file-text-o"></i> Conferência', ['controller' => 'Faturamentos', 'action' => 'rec_listar', $faturaEncerramento->id], ['class' => 'btn btn-success', 'escape' => false, 'target' => '_blank'])?>
                                        </div>
                                        <div class="col-md-1 pull-right">
                                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/ispinia/jquery-2.1.1.js"></script>
<script>
    var anoSelecionado = '';
    var mesSelecionado = '';
    $('#ano').on('change', function (event) {
        anoSelecionado = $('#ano option:selected').text();
        $('#competencia').val(mesSelecionado +'-'+ anoSelecionado);
    });

    $('#mes').on('change', function (event) {
        mesSelecionado = $('#mes option:selected').val();
        if (mesSelecionado < 10) {
            mesSelecionado = '0' + mesSelecionado;
        }
        $('#competencia').val(mesSelecionado +'-'+ anoSelecionado);
    });
</script>