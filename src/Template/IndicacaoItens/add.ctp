<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Indicacao Itens</h2>
        <ol class="breadcrumb">
            <li>Indicacao Itens</li>
            <li class="active">
                <strong>                    Cadastrar Indicacao Itens
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="indicacaoItens form">
                        <?= $this->Form->create($indicacaoIten) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Indicacao Iten') ?></legend>
                            <?php
                                if(!empty($indicacao_id)){
                                    echo $this->Form->input('indicacao_id', ['value' => $indicacao_id,'type'=>'hidden']);
                                }else {
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('indicacao_id', ['data' => 'select', 'controller' => 'indicacao', 'action' => 'fill']);
                                    echo "</div>";
                                }
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('nome');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('cep', ['mask' => 'cep', 'role' => 'cep']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('endereco', ['label' => 'Endereço', 'data-cep' => 'endereco']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('numero', ['label' => 'Número']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('complemento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('bairro', ['data-cep' => 'bairro']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('cidade', ['data-cep' => 'cidade']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('estado', ['data-cep' => 'uf']);
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

