
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Indicacao Itens</h2>
        <ol class="breadcrumb">
            <li>Indicacao Itens</li>
            <li class="active">
                <strong>Litagem de Indicacao Itens</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Indicacao Itens</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Indicacao') ?></th>
                                                                <td><?= $indicacaoIten->has('indicacao') ? $this->Html->link($indicacaoIten->indicacao->nome, ['controller' => 'Indicacao', 'action' => 'view', $indicacaoIten->indicacao->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($indicacaoIten->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($indicacaoIten->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Endereco') ?></th>
                                <td><?= h($indicacaoIten->endereco) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= h($indicacaoIten->numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Complemento') ?></th>
                                <td><?= h($indicacaoIten->complemento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td><?= h($indicacaoIten->bairro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cidade') ?></th>
                                <td><?= h($indicacaoIten->cidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Estado') ?></th>
                                <td><?= h($indicacaoIten->estado) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $indicacaoIten->has('situacao_cadastro') ? $this->Html->link($indicacaoIten->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $indicacaoIten->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $indicacaoIten->has('user') ? $this->Html->link($indicacaoIten->user->nome, ['controller' => 'Users', 'action' => 'view', $indicacaoIten->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($indicacaoIten->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($indicacaoIten->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($indicacaoIten->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


