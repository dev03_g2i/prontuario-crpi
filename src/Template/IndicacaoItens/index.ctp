<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Indicacao Itens</h2>
            <ol class="breadcrumb">
                <li>Indicacao Itens</li>
                <li class="active">
                    <strong>Litagem de Indicacao Itens</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('IndicacaoIten', ['type' => 'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('indicacao_id', ['name' => 'IndicacaoItens__indicacao_id', 'data' => 'select', 'controller' => 'indicacao', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('nome', ['name' => 'IndicacaoItens__nome']);
                            echo "</div>";

                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Indicacao Itens', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Indicacao Itens', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Indicacao Itens') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('indicacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('cep') ?></th>
                                        <th><?= $this->Paginator->sort('endereco') ?></th>
                                        <th><?= $this->Paginator->sort('numero') ?></th>
                                        <th><?= $this->Paginator->sort('complemento') ?></th>
                                        <th><?= $this->Paginator->sort('bairro') ?></th>
                                        <th><?= $this->Paginator->sort('cidade') ?></th>
                                        <th><?= $this->Paginator->sort('estado') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th><?= $this->Paginator->sort('user_id') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($indicacaoItens as $indicacaoIten): ?>
                                        <tr>
                                            <td><?= $indicacaoIten->has('indicacao') ? $this->Html->link($indicacaoIten->indicacao->nome, ['controller' => 'Indicacao', 'action' => 'view', $indicacaoIten->indicacao->id]) : '' ?></td>
                                            <td><?= h($indicacaoIten->nome) ?></td>
                                            <td><?= h($indicacaoIten->cep) ?></td>
                                            <td><?= h($indicacaoIten->endereco) ?></td>
                                            <td><?= h($indicacaoIten->numero) ?></td>
                                            <td><?= h($indicacaoIten->complemento) ?></td>
                                            <td><?= h($indicacaoIten->bairro) ?></td>
                                            <td><?= h($indicacaoIten->cidade) ?></td>
                                            <td><?= h($indicacaoIten->estado) ?></td>
                                            <td><?= $indicacaoIten->has('situacao_cadastro') ? $this->Html->link($indicacaoIten->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $indicacaoIten->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= h($indicacaoIten->created) ?></td>
                                            <td><?= h($indicacaoIten->modified) ?></td>
                                            <td><?= $indicacaoIten->has('user') ? $this->Html->link($indicacaoIten->user->nome, ['controller' => 'Users', 'action' => 'view', $indicacaoIten->user->id]) : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $indicacaoIten->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'indicacaoItens\',' . $indicacaoIten->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

