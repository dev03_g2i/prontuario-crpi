<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Log Laudos</h2>
            <ol class="breadcrumb">
                <li>Log Laudos</li>
                <li class="active">
                    <strong>
                                                Cadastrar Log Laudos
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="logLaudos form">
                            <?= $this->Form->create($logLaudo) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Log Laudo') ?></legend>
                                                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('laudo_id', ['data'=>'select','controller'=>'laudos','action'=>'fill']); ?>
                                                                                                </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('texto'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('rtf'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('assinado_por'); ?>
                                                                    </div>
                                                                    <div class='col-md-6'>
                                        <?php                                             echo $this->Form->input('dt_assinatura', ['type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                             ?>
                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('texto_html'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('imagens'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('filme'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('papel'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('rtf_html'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('paginas'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

