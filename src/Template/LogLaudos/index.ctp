<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Log Laudos</h2>
            <ol class="breadcrumb">
                <li>Log Laudos</li>
                <li class="active">
                    <strong>Listagem de Log Laudos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Log Laudos') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('laudo_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('texto') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('rtf') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('text_html') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('rtf_html') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('imagens') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('filme') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('papel') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('paginas') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('assinado_por') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('dt_assinatura') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($logLaudos as $logLaudo): ?>
                                        <tr>
                                            <td>
                                                <?= $logLaudo->laudo_id ?>
                                            </td>
                                            <td>
                                                <?= nl2br($logLaudo->texto) ?>
                                            </td>
                                            <td>
                                                <?= nl2br($logLaudo->rtf) ?>
                                            </td>
                                            <td>
                                                <?= nl2br($logLaudo->texto_html) ?>
                                            </td>
                                            <td>
                                                <?= nl2br($logLaudo->rtf_html) ?>
                                            </td>
                                            <td>
                                                <?= $this->Number->format($logLaudo->imagens) ?>
                                            </td>
                                            <td>
                                                <?= $this->Number->format($logLaudo->filme) ?>
                                            </td>
                                            <td>
                                                <?= $this->Number->format($logLaudo->papel) ?>
                                            </td>
                                            <td>
                                                <?= $this->Number->format($logLaudo->paginas) ?>
                                            </td>
                                            <td>
                                                <?= $logLaudo->has('user_assinado') ? $logLaudo->user_assinado->nome : null ?>
                                            </td>
                                            <td>
                                                <?= h($logLaudo->dt_assinatura) ?>
                                            </td>
                                            <td>
                                                <?= $logLaudo->has('user_reg') ? $logLaudo->user_reg->nome : null ?>
                                            </td>
                                            <td>
                                                <?= h($logLaudo->created) ?>
                                            </td>
                                            <td>
                                                <?= h($logLaudo->modified) ?>
                                            </td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'logLaudos','action' => 'view', $logLaudo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>