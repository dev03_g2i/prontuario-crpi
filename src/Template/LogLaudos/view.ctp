<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Log Laudos</h2>
            <ol class="breadcrumb">
                <li>Log Laudos</li>
                <li class="active">
                    <strong>Litagem de Log Laudos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Log Laudos</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($logLaudo->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Laudo') ?>
                                    </th>
                                    <td>
                                        <?= $logLaudo->has('laudo') ? $this->Html->link($logLaudo->laudo->id, ['controller' => 'Laudos', 'action' => 'view', $logLaudo->laudo->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('User Id') ?>
                                    </th>
                                    <td>
                                        <?= $logLaudo->has('user_reg') ? $logLaudo->user_reg->nome : null ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Assinado Por') ?>
                                    </th>
                                    <td>
                                        <?= $logLaudo->has('user_assinado') ? $logLaudo->user_assinado->nome : null ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Imagens') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($logLaudo->imagens) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Filme') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($logLaudo->filme) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Papel') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($logLaudo->papel) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Paginas') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($logLaudo->paginas) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($logLaudo->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($logLaudo->modified) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt Assinatura') ?>
                                    </th>
                                    <td>
                                        <?= h($logLaudo->dt_assinatura) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Texto') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(h($logLaudo->texto)); ?>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Rtf') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(h($logLaudo->rtf)); ?>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Texto Html') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(h($logLaudo->texto_html)); ?>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>
                            <?= __('Rtf Html') ?>
                        </h3>
                    </div>
                    <div class="ibox-content">
                        <?= $this->Text->autoParagraph(h($logLaudo->rtf_html)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>