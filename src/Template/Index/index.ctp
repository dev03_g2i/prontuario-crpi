<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Início</h2>
        <ol class="breadcrumb">
            <li>Painel</li>
            <li class="active">
                <strong>Indicadores</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <i class="fa fa-birthday-cake"></i> Aniversariantes
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'lista', '?' => ['tipo' => 1]]) ?>" data-toggle="modal" data-target="#modal_lg">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right"><i
                                            class="fa fa-birthday-cake"></i></span>
                                    <h5>Dia</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $dia ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'lista', '?' => ['tipo' => 2]]) ?>" data-toggle="modal" data-target="#modal_lg">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-info pull-right"><i class="fa fa-birthday-cake"></i></span>
                                    <h5>Semana</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $semana ?></h1>


                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'lista', '?' => ['tipo' => 3]]) ?>" data-toggle="modal" data-target="#modal_lg">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-warning pull-right"><i
                                            class="fa fa-birthday-cake"></i></span>
                                    <h5>Mês</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $mes ?></h1>


                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <i class="fa fa-calendar"></i> Retornos
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <a href="<?php echo $this->Url->build(['controller' => 'Retornos', 'action' => 'index', '?' => ['tipo' => 1]]) ?>">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right">
                                        <i class="fa fa-calendar"></i></span>
                                    <h5>Expirados</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $expirados->total ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="<?php echo $this->Url->build(['controller' => 'Retornos', 'action' => 'index', '?' => ['situacao_retorno_id' => 3]]) ?>" >
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-info pull-right"><i class="fa fa-calendar"></i></span>
                                    <h5>Remarcados</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $reprogramados->total ?></h1>


                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-4">
                        <a href="<?php echo $this->Url->build(['controller' => 'Retornos', 'action' => 'index', '?' => ['situacao_retorno_id' => 1]]) ?>" >
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-warning pull-right"><i class="fa fa-calendar"></i></span>
                                    <h5>Programados</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $programado->total ?></h1>


                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <i class="fa fa-bookmark"></i> Dados Complementares
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <a href="<?php echo $this->Url->build(['controller' => 'Clientes', 'action' => 'index']) ?>" >
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right">
                                        <i class="fa fa-group"></i></span>
                                    <h5>Pacientes</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $clientes->total ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3">
                        <a href="<?php echo $this->Url->build(['controller' => 'Orcamentos', 'action' => 'index', '?' => ['tipo' => 0]]) ?>" >
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-info pull-right"><i class="fa fa-calendar-o"></i></span>
                                    <h5>Orçamento não efetivado</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $reprovado ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3">
                        <a href="<?php echo $this->Url->build(['controller' => 'Orcamentos', 'action' => 'index', '?' => ['tipo' => 1]]) ?>">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-warning pull-right"><i class="fa fa-calendar-o"></i></span>
                                    <h5>Orçamento parcial</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $parcial ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-3">
                        <a href="<?php echo $this->Url->build(['controller' => 'Atendimentos', 'action' => 'index', '?' => ['tipo' => 2]]) ?>">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-success pull-right"><i class="fa fa-calendar-o"></i></span>
                                    <h5>Tratamentos</h5>
                                </div>
                                <div class="ibox-content">
                                    <h1 class="no-margins"><?= $tratamentos->total ?></h1>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <i class="fa fa-clock-o"></i> Acompanhamentos
                </div>
                <div class="row">
                    <div class="col-lg-5">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-primary pull-right">
                                        <i class="fa fa-clock-o"></i></span>
                                    <h5>Agendamentos</h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <tr>
                                                <td class="text-left">&nbsp;</td>
                                                <td class="text-left">Dia</td>
                                                <td class="text-left">Semana</td>
                                                <td class="text-left">Mes</td>
                                            </tr>
                                            <?php
                                                foreach ($agendas as $agenda =>$value){
                                                    echo '<tr>';
                                                        if(!empty($value['cor'])) {
                                                            echo '<td style="background-color: ' . $value['cor'] . ';color:#fff" class="text-left">';
                                                        }else{
                                                            echo '<td class="text-left">';
                                                        }
                                                            echo '<strong>'.$agenda.'</strong>';
                                                        echo '</td>';
                                                        echo '<td class="text-left">'.$value['dia'].'</td>';
                                                        echo '<td class="text-left">'.$value['semana'].'</td>';
                                                        echo '<td class="text-left">'.$value['mes'].'</td>';
                                                    echo '</tr>';
                                                }
                                            ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-7">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <span class="label label-info pull-right"><i class="fa fa-calendar"></i></span>
                                    <h5>Agendamentos Atendidos - mês a mês</h5>
                                </div>
                                <div class="ibox-content">
                                    <canvas id="grafico" width="600" height="275" ></canvas>
                                </div>
                            </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->scriptBlock(
    ' 
        $(window).load(function(){
            $.ajax({
                url:site_path+"/Index/grafico",
                dataType:"JSON",
                success:function(txt){
                console.log(txt)
                var data = {
                    datasets:[{
                        label: "Atendidos",
                        data: txt.dados,
                        backgroundColor:"#36A2EB"
                        },
                        
                    ],
                    labels: txt.meses,
                     
                };
               var ctx = document.getElementById("grafico");        
               var myChart = new Chart(ctx, {
                    type: "line",
                    data:data,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
                }
            })
        })
       
    ',
    ['inline' => false, 'block' => 'scriptLast']
);
?>