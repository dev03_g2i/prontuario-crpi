<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Formação de kit/pacote</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FaturaKitartigo',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('fatura_kit_id', ['name'=>'FaturaKitartigo__fatura_kit_id','data'=>'select','controller'=>'faturaKit','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?= $this->Form->input('artigo_id', ['name'=>'FaturaKitartigo__artigo_id','data'=>'select','controller'=>'estqArtigos','action'=>'fill', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('quantidade',['name'=>'FaturaKitartigo__quantidade']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Adicionar Artigos', ['action' => 'add', '?' => ['fatura_kit_id' => $fatura_kit_id, 'first' => 1]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fatura Kitartigo','class'=>'btn btn-primary','escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('fatura_kit_id', 'Fatura kit') ?></th>
                                        <th><?= $this->Paginator->sort('artigo_id') ?></th>
                                        <th><?= $this->Paginator->sort('quantidade') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id', 'Usuário') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaKitartigo as $faturaKitartigo): ?>
                                        <tr>
                                            <td><?= $this->Number->format($faturaKitartigo->id) ?></td>
                                            <td><?= $faturaKitartigo->has('fatura_kit') ? $this->Html->link($faturaKitartigo->fatura_kit->nome, ['controller' => 'FaturaKit', 'action' => 'view', $faturaKitartigo->fatura_kit->id]) : '' ?></td>
                                            <td><?= $faturaKitartigo->has('estq_artigo') ? $this->Html->link($faturaKitartigo->estq_artigo->nome, ['controller' => 'EstqArtigos', 'action' => 'view', $faturaKitartigo->estq_artigo->id]) : '' ?></td>
                                            <td><?= $this->Number->format($faturaKitartigo->quantidade) ?></td>
                                            <td><?= $faturaKitartigo->has('situacao_cadastro') ? $this->Html->link($faturaKitartigo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaKitartigo->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= $faturaKitartigo->user->nome .' '.$faturaKitartigo->created ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaKitartigo','action' => 'view', $faturaKitartigo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaKitartigo','action' => 'edit', $faturaKitartigo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link('<i class="fa fa-money"></i>',  ['controller'=>'FaturaPrecoartigo','action'=>'index', '?' => ['artigo_id' => $faturaKitartigo->artigo_id]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Precificar/Codificar Artigos x Convênios','escape' => false,'class'=>'btn btn-xs btn-success', 'target' => '_blank', 'listen' => 'f']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'faturaKitartigo','action'=>'delete', $faturaKitartigo->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
