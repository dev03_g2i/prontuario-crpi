
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Kitartigo</h2>
        <ol class="breadcrumb">
            <li>Fatura Kitartigo</li>
            <li class="active">
                <strong>Litagem de Fatura Kitartigo</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Kitartigo</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Fatura Kit') ?></th>
                                                                <td><?= $faturaKitartigo->has('fatura_kit') ? $this->Html->link($faturaKitartigo->fatura_kit->id, ['controller' => 'FaturaKit', 'action' => 'view', $faturaKitartigo->fatura_kit->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Estq Artigo') ?></th>
                                                                <td><?= $faturaKitartigo->has('estq_artigo') ? $this->Html->link($faturaKitartigo->estq_artigo->nome, ['controller' => 'EstqArtigos', 'action' => 'view', $faturaKitartigo->estq_artigo->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faturaKitartigo->has('user') ? $this->Html->link($faturaKitartigo->user->nome, ['controller' => 'Users', 'action' => 'view', $faturaKitartigo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturaKitartigo->has('situacao_cadastro') ? $this->Html->link($faturaKitartigo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaKitartigo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaKitartigo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($faturaKitartigo->quantidade) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaKitartigo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaKitartigo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


