<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Kitartigo</h2>
            <ol class="breadcrumb">
                <li>Fatura Kitartigo</li>
                <li class="active">
                    <strong>
                        Editar Fatura Kitartigo
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaKitartigo form">
                            <?= $this->Form->create($faturaKitartigo) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Fatura Kitartigo') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('fatura_kit_id', ['data'=>'select','controller'=>'faturaKit','action'=>'fill','data-value'=>$faturaKitartigo->fatura_kit_id]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('artigo_id', ['data'=>'select','controller'=>'estqArtigos','action'=>'fill','data-value'=>$faturaKitartigo->artigo_id]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('quantidade'); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>