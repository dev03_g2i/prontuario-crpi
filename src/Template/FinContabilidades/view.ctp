
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Contabilidades</h2>
        <ol class="breadcrumb">
            <li>Fin Contabilidades</li>
            <li class="active">
                <strong>Litagem de Fin Contabilidades</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Contabilidades</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($finContabilidade->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($finContabilidade->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Rua') ?></th>
                                <td><?= h($finContabilidade->rua) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td><?= h($finContabilidade->bairro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cidade') ?></th>
                                <td><?= h($finContabilidade->cidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Uf') ?></th>
                                <td><?= h($finContabilidade->uf) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finContabilidade->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= $this->Number->format($finContabilidade->numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?></th>
                                <td><?= $this->Number->format($finContabilidade->status) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Descricao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($finContabilidade->descricao)); ?>
                </div>
            </div>
    <!-- <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contabilidade Bancos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_contabilidade_bancos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_contabilidade_bancos as $finContabilidadeBancos): ?>
                <tr>
                    <td><?= h($finContabilidadeBancos->id) ?></td>
                    <td><?= h($finContabilidadeBancos->fin_contabilidade_id) ?></td>
                    <td><?= h($finContabilidadeBancos->fin_banco_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContabilidadeBancos','action' => 'view', $finContabilidadeBancos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContabilidadeBancos','action' => 'edit', $finContabilidadeBancos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContabilidadeBancos','action' => 'delete', $finContabilidadeBancos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Pagar') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_contas_pagar)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Vencimento') ?></th>
                        <th><?= __('Valor') ?></th>
                        <th><?= __('Juros') ?></th>
                        <th><?= __('Multa') ?></th>
                        <th><?= __('Data Pagamento') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Valor Bruto') ?></th>
                        <th><?= __('Desconto') ?></th>
                        <th><?= __('Fin Tipo Pagamento Id') ?></th>
                        <th><?= __('Fin Tipo Documento Id') ?></th>
                        <th><?= __('Numero Documento') ?></th>
                        <th><?= __('Data Cadastro') ?></th>
                        <th><?= __('Parcela') ?></th>
                        <th><?= __('Fin Contas Pagar Planejamento Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_contas_pagar as $finContasPagar): ?>
                <tr>
                    <td><?= h($finContasPagar->id) ?></td>
                    <td><?= h($finContasPagar->data) ?></td>
                    <td><?= h($finContasPagar->vencimento) ?></td>
                    <td><?= h($finContasPagar->valor) ?></td>
                    <td><?= h($finContasPagar->juros) ?></td>
                    <td><?= h($finContasPagar->multa) ?></td>
                    <td><?= h($finContasPagar->data_pagamento) ?></td>
                    <td><?= h($finContasPagar->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasPagar->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasPagar->situacao) ?></td>
                    <td><?= h($finContasPagar->complemento) ?></td>
                    <td><?= h($finContasPagar->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasPagar->valor_bruto) ?></td>
                    <td><?= h($finContasPagar->desconto) ?></td>
                    <td><?= h($finContasPagar->fin_tipo_pagamento_id) ?></td>
                    <td><?= h($finContasPagar->fin_tipo_documento_id) ?></td>
                    <td><?= h($finContasPagar->numero_documento) ?></td>
                    <td><?= h($finContasPagar->data_cadastro) ?></td>
                    <td><?= h($finContasPagar->parcela) ?></td>
                    <td><?= h($finContasPagar->fin_contas_pagar_planejamento_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasPagar','action' => 'view', $finContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasPagar','action' => 'edit', $finContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasPagar','action' => 'delete', $finContasPagar->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Pagar Planejamentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_contas_pagar_planejamentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Periodicidade') ?></th>
                        <th><?= __('Plazo Determinado') ?></th>
                        <th><?= __('Valor Parcela') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Dia Vencimento') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('A Vista') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Proxima Geracao') ?></th>
                        <th><?= __('Ultima Geracao') ?></th>
                        <th><?= __('Geradas') ?></th>
                        <th><?= __('Tipo Pgmt') ?></th>
                        <th><?= __('Tipo Dcmt') ?></th>
                        <th><?= __('Ultimo Gerado') ?></th>
                        <th><?= __('Mes Vencimento') ?></th>
                        <th><?= __('Ultima Atualizacao') ?></th>
                        <th><?= __('Data Documento') ?></th>
                        <th><?= __('Primeiro Vencimento') ?></th>
                        <th><?= __('Mumero Documento') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_contas_pagar_planejamentos as $finContasPagarPlanejamentos): ?>
                <tr>
                    <td><?= h($finContasPagarPlanejamentos->id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->periodicidade) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->plazo_determinado) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->valor_parcela) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->cliente_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->complemento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->dia_vencimento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->situacao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->a_vista) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->correcao_monetaria) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->proxima_geracao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->ultima_geracao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->geradas) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->tipo_pgmt) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->tipo_dcmt) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->ultimo_gerado) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->mes_vencimento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->ultima_atualizacao) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->data_documento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->primeiro_vencimento) ?></td>
                    <td><?= h($finContasPagarPlanejamentos->mumero_documento) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasPagarPlanejamentos','action' => 'view', $finContasPagarPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasPagarPlanejamentos','action' => 'edit', $finContasPagarPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasPagarPlanejamentos','action' => 'delete', $finContasPagarPlanejamentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Contas Receber Planejamentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_contas_receber_planejamentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Periodicidade') ?></th>
                        <th><?= __('Plazo Determinado') ?></th>
                        <th><?= __('Valor Parcela') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Dia Vencimento') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('A Vista') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Proxima Geracao') ?></th>
                        <th><?= __('Ultima Geracao') ?></th>
                        <th><?= __('Geradas') ?></th>
                        <th><?= __('Tipo Pgmt') ?></th>
                        <th><?= __('Tipo Dcmt') ?></th>
                        <th><?= __('Ultimo Gerado') ?></th>
                        <th><?= __('Mes Vencimento') ?></th>
                        <th><?= __('Ultima Atualizacao') ?></th>
                        <th><?= __('Data Documento') ?></th>
                        <th><?= __('Primeiro Vencimento') ?></th>
                        <th><?= __('Mumero Documento') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_contas_receber_planejamentos as $finContasReceberPlanejamentos): ?>
                <tr>
                    <td><?= h($finContasReceberPlanejamentos->id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->periodicidade) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->plazo_determinado) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->valor_parcela) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->cliente_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->complemento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->dia_vencimento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->situacao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->a_vista) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->correcao_monetaria) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->proxima_geracao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->ultima_geracao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->geradas) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->tipo_pgmt) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->tipo_dcmt) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->ultimo_gerado) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->mes_vencimento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->ultima_atualizacao) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->data_documento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->primeiro_vencimento) ?></td>
                    <td><?= h($finContasReceberPlanejamentos->mumero_documento) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinContasReceberPlanejamentos','action' => 'view', $finContasReceberPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinContasReceberPlanejamentos','action' => 'edit', $finContasReceberPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinContasReceberPlanejamentos','action' => 'delete', $finContasReceberPlanejamentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Grupo Contabilidades') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_grupo_contabilidades)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Grupo Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_grupo_contabilidades as $finGrupoContabilidades): ?>
                <tr>
                    <td><?= h($finGrupoContabilidades->id) ?></td>
                    <td><?= h($finGrupoContabilidades->fin_grupo_id) ?></td>
                    <td><?= h($finGrupoContabilidades->fin_contabilidade_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinGrupoContabilidades','action' => 'view', $finGrupoContabilidades->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinGrupoContabilidades','action' => 'edit', $finGrupoContabilidades->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinGrupoContabilidades','action' => 'delete', $finGrupoContabilidades->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Documento') ?></th>
                        <th><?= __('Credito') ?></th>
                        <th><?= __('Debito') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th><?= __('Status Lancamento') ?></th>
                        <th><?= __('Categoria') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Contas Pagar Id') ?></th>
                        <th><?= __('Fin Banco Movimento Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Data Criacao') ?></th>
                        <th><?= __('Data Alteracao') ?></th>
                        <th><?= __('Criado Por') ?></th>
                        <th><?= __('Atualizado Por') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_movimentos as $finMovimentos): ?>
                <tr>
                    <td><?= h($finMovimentos->id) ?></td>
                    <td><?= h($finMovimentos->situacao) ?></td>
                    <td><?= h($finMovimentos->data) ?></td>
                    <td><?= h($finMovimentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finMovimentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finMovimentos->complemento) ?></td>
                    <td><?= h($finMovimentos->documento) ?></td>
                    <td><?= h($finMovimentos->credito) ?></td>
                    <td><?= h($finMovimentos->debito) ?></td>
                    <td><?= h($finMovimentos->fin_banco_id) ?></td>
                    <td><?= h($finMovimentos->status_lancamento) ?></td>
                    <td><?= h($finMovimentos->categoria) ?></td>
                    <td><?= h($finMovimentos->cliente_id) ?></td>
                    <td><?= h($finMovimentos->fin_contas_pagar_id) ?></td>
                    <td><?= h($finMovimentos->fin_banco_movimento_id) ?></td>
                    <td><?= h($finMovimentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finMovimentos->data_criacao) ?></td>
                    <td><?= h($finMovimentos->data_alteracao) ?></td>
                    <td><?= h($finMovimentos->criado_por) ?></td>
                    <td><?= h($finMovimentos->atualizado_por) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinMovimentos','action' => 'view', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinMovimentos','action' => 'edit', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinMovimentos','action' => 'delete', $finMovimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Planejamentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContabilidade->fin_planejamentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Periodicidade') ?></th>
                        <th><?= __('Plazo Determinado') ?></th>
                        <th><?= __('Valor Parcela') ?></th>
                        <th><?= __('Cliente Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Dia Vencimento') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('A Vista') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Correcao Monetaria') ?></th>
                        <th><?= __('Proxima Geracao') ?></th>
                        <th><?= __('Ultima Geracao') ?></th>
                        <th><?= __('Geradas') ?></th>
                        <th><?= __('Tipo Pgmt') ?></th>
                        <th><?= __('Tipo Dcmt') ?></th>
                        <th><?= __('Ultimo Gerado') ?></th>
                        <th><?= __('Mes Vencimento') ?></th>
                        <th><?= __('Ultima Atualizacao') ?></th>
                        <th><?= __('Data Documento') ?></th>
                        <th><?= __('Primeiro Vencimento') ?></th>
                        <th><?= __('Mumero Documento') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContabilidade->fin_planejamentos as $finPlanejamentos): ?>
                <tr>
                    <td><?= h($finPlanejamentos->id) ?></td>
                    <td><?= h($finPlanejamentos->periodicidade) ?></td>
                    <td><?= h($finPlanejamentos->plazo_determinado) ?></td>
                    <td><?= h($finPlanejamentos->valor_parcela) ?></td>
                    <td><?= h($finPlanejamentos->cliente_id) ?></td>
                    <td><?= h($finPlanejamentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finPlanejamentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finPlanejamentos->complemento) ?></td>
                    <td><?= h($finPlanejamentos->dia_vencimento) ?></td>
                    <td><?= h($finPlanejamentos->situacao) ?></td>
                    <td><?= h($finPlanejamentos->a_vista) ?></td>
                    <td><?= h($finPlanejamentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finPlanejamentos->correcao_monetaria) ?></td>
                    <td><?= h($finPlanejamentos->proxima_geracao) ?></td>
                    <td><?= h($finPlanejamentos->ultima_geracao) ?></td>
                    <td><?= h($finPlanejamentos->geradas) ?></td>
                    <td><?= h($finPlanejamentos->tipo_pgmt) ?></td>
                    <td><?= h($finPlanejamentos->tipo_dcmt) ?></td>
                    <td><?= h($finPlanejamentos->ultimo_gerado) ?></td>
                    <td><?= h($finPlanejamentos->mes_vencimento) ?></td>
                    <td><?= h($finPlanejamentos->ultima_atualizacao) ?></td>
                    <td><?= h($finPlanejamentos->data_documento) ?></td>
                    <td><?= h($finPlanejamentos->primeiro_vencimento) ?></td>
                    <td><?= h($finPlanejamentos->mumero_documento) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinPlanejamentos','action' => 'view', $finPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinPlanejamentos','action' => 'edit', $finPlanejamentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinPlanejamentos','action' => 'delete', $finPlanejamentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContabilidade->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div> -->
</div>
</div>
</div>
</div>


