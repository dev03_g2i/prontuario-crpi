<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Empresas</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finContabilidades form">
                            <?= $this->Form->create($finContabilidade) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Cadastrar Empresa') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('nome', ['required' => 'required']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('cep', ['mask' => 'cep']); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('rua'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('bairro'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('numero'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('cidade'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('uf'); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('descricao'); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>