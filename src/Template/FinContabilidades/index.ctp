<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Empresas</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinContabilidade',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?= $this->Form->input('empresa', ['label' => 'Nome', 'type' => 'select', 'data-contabilidades' => "contabilidades", 'empty' => 'Selecione', 'options' => [@$fin_contabilidades->id => @$fin_contabilidades->nome], 'default' => @$fin_contabilidades->id]); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Empresa', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Empresa','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Lista de empresas') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('nome') ?>
                                            </th>
                                            <th class="actions text-right" align="right">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($finContabilidades as $finContabilidade): ?>
                                        <tr>
                                            <td>
                                                <?= $this->Number->format($finContabilidade->id) ?>
                                            </td>
                                            <td>
                                                <?= h($finContabilidade->nome) ?>
                                            </td>
                                            <td class="actions" align="right">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'finContabilidades','action' => 'edit', $finContabilidade->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'finContabilidades','action'=>'delete', $finContabilidade->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>