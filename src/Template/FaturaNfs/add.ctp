<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Nfs</h2>
            <ol class="breadcrumb">
                <li>Fatura Nfs</li>
                <li class="active">
                    <strong>                    Cadastrar Fatura Nfs
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaNfs form">
                            <?= $this->Form->create($faturaNf) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Fatura Nf') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('numero_nf', ['type' => 'text']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_bruto',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('imposto_nao_retido',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('imposto_retido',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('descontos',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('valor_liquido',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('obs');
                                echo "</div>";

                                echo $this->Form->input('unidade_id', ['type' => 'hidden', 'value' => $unidade_id]);
                                echo $this->Form->input('fatura_encerramento_id', ['type' => 'hidden', 'value' => $fatura_encerramento_id]);
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

