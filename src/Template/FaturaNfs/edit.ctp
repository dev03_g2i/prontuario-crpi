<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura Nfs</h2>
            <ol class="breadcrumb">
                <li>Fatura Nfs</li>
                <li class="active">
                    <strong>
                            Editar Fatura Nfs
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaNfs form">
                            <?= $this->Form->create($faturaNf) ?>
                            <fieldset>
                                <legend><?= __('Editar Fatura Nf') ?></legend>
                                <?php
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker','value'=>$this->Time->format($faturaNf->data,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('valor_bruto',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('imposto_nao_retido',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='clearfix'></div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('imposto_retido',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('descontos',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('valor_liquido',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('obs', ['rows' => 2]);
                                echo "</div>";
                                /*echo "<div class='col-md-6'>";
                                echo $this->Form->input('unidade_id', ['data'=>'select','controller'=>'unidades','action'=>'fill','data-value'=>$faturaNf->unidade_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('fatura_encerramento_id', ['data'=>'select','controller'=>'faturaEncerramentos','action'=>'fill','data-value'=>$faturaNf->fatura_encerramento_id]);
                                echo "</div>";*/
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

