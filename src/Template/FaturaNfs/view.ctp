
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fatura Nfs</h2>
        <ol class="breadcrumb">
            <li>Fatura Nfs</li>
            <li class="active">
                <strong>Litagem de Fatura Nfs</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fatura Nfs</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Unidade') ?></th>
                                                                <td><?= $faturaNf->has('unidade') ? $this->Html->link($faturaNf->unidade->nome, ['controller' => 'Unidades', 'action' => 'view', $faturaNf->unidade->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fatura Encerramento') ?></th>
                                                                <td><?= $faturaNf->has('fatura_encerramento') ? $this->Html->link($faturaNf->fatura_encerramento->id, ['controller' => 'FaturaEncerramentos', 'action' => 'view', $faturaNf->fatura_encerramento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $faturaNf->has('situacao_cadastro') ? $this->Html->link($faturaNf->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaNf->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $faturaNf->has('user') ? $this->Html->link($faturaNf->user->nome, ['controller' => 'Users', 'action' => 'view', $faturaNf->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($faturaNf->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Bruto') ?></th>
                                <td><?= $this->Number->format($faturaNf->valor_bruto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Imposto Nao Retido') ?></th>
                                <td><?= $this->Number->format($faturaNf->imposto_nao_retido) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Imposto Retido') ?></th>
                                <td><?= $this->Number->format($faturaNf->imposto_retido) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Descontos') ?></th>
                                <td><?= $this->Number->format($faturaNf->descontos) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Liquido') ?></th>
                                <td><?= $this->Number->format($faturaNf->valor_liquido) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($faturaNf->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($faturaNf->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($faturaNf->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Obs') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($faturaNf->obs)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


