<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fatura: <?=$fatura_id?> - <?=$convenio?></h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">

                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Fatura Nfs', ['action' => 'add', '?' => ['fatura_id' => $fatura_id, 'unidade_id' => $unidade_id, 'ajax' => 1, 'first' => 1]],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Fatura Nfs','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Fatura Nfs') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <!--<th><?= $this->Paginator->sort('id') ?></th>-->
                                        <th><?= $this->Paginator->sort('numero_nf') ?></th>
                                        <th><?= $this->Paginator->sort('data') ?></th>
                                        <th><?= $this->Paginator->sort('valor_bruto') ?></th>
                                        <th><?= $this->Paginator->sort('imposto_nao_retido') ?></th>
                                        <th><?= $this->Paginator->sort('imposto_retido') ?></th>
                                        <th><?= $this->Paginator->sort('descontos') ?></th>
                                        <th><?= $this->Paginator->sort('valor_liquido') ?></th>
                                        <th><?= $this->Paginator->sort('unidade_id') ?></th>
                                        <th><?= $this->Paginator->sort('fatura_encerramento_id', 'N° Fatura') ?></th>
                                        <th><?= $this->Paginator->sort('obs', 'Obs') ?></th>
                                       <!--<th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                                        <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>-->
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($faturaNfs as $faturaNf): ?>
                                        <tr class="delete<?=$faturaNf->id?>">
                                            <!--<td><?= $this->Number->format($faturaNf->id) ?></td>-->
                                            <td><?= h($faturaNf->numero_nf) ?></td>
                                            <td><?= h($faturaNf->data) ?></td>
                                            <td><?= $this->Number->format($faturaNf->valor_bruto, ['places' => 2]) ?></td>
                                            <td><?= $this->Number->format($faturaNf->imposto_nao_retido, ['places' => 2]) ?></td>
                                            <td><?= $this->Number->format($faturaNf->imposto_retido, ['places' => 2]) ?></td>
                                            <td><?= $this->Number->format($faturaNf->descontos, ['places' => 2]) ?></td>
                                            <td><?= $this->Number->format($faturaNf->valor_liquido, ['places' => 2]) ?></td>
                                            <td><?= $faturaNf->has('unidade') ? $this->Html->link($faturaNf->unidade->nome, ['controller' => 'Unidades', 'action' => 'view', $faturaNf->unidade->id]) : '' ?></td>
                                            <td><?= $faturaNf->has('fatura_encerramento') ? $this->Html->link($faturaNf->fatura_encerramento->id, ['controller' => 'FaturaEncerramentos', 'action' => 'view', $faturaNf->fatura_encerramento->id]) : '' ?></td>
                                            <!--<td><?= $faturaNf->has('situacao_cadastro') ? $this->Html->link($faturaNf->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $faturaNf->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= $faturaNf->has('user') ? $this->Html->link($faturaNf->user->nome, ['controller' => 'Users', 'action' => 'view', $faturaNf->user->id]) : '' ?></td>
                                            <td><?= h($faturaNf->created) ?></td>
                                            <td><?= h($faturaNf->modified) ?></td>-->
                                            <td><?=$faturaNf->obs?></td>
                                            <td class="actions">
                                                <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'faturaNfs','action' => 'view', $faturaNf->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>-->
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'faturaNfs','action' => 'edit', $faturaNf->id, 'ajax' => 1, 'first' => 1],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Form->button($this->Html->icon('remove'), ['type' => 'button', 'onclick'=>'DeletarModal(\'faturaNfs\','.$faturaNf->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
