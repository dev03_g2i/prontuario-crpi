<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Procedimento Detalhes</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('ProcedimentoCaddetalhe',['type'=>'get']) ?>
                                <div class='col-md-4'>
                                    <?=$this->Form->input('nome',['name'=>'ProcedimentoCaddetalhes__nome']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 fill-content-search">
                                    <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                    <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Procedimento Caddetalhes','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Procedimento Caddetalhes') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('nome') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('situacao_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($procedimentoCaddetalhes as $procedimentoCaddetalhe): ?>
                                        <tr>
                                            <td>
                                                <?= $this->Number->format($procedimentoCaddetalhe->id) ?>
                                            </td>
                                            <td>
                                                <?= h($procedimentoCaddetalhe->nome) ?>
                                            </td>
                                            <td>
                                                <?= $this->Number->format($procedimentoCaddetalhe->situacao_id) ?>
                                            </td>
                                            <td>
                                                <?= $procedimentoCaddetalhe->has('user') ? $this->Html->link($procedimentoCaddetalhe->user->nome, ['controller' => 'Users', 'action' => 'view', $procedimentoCaddetalhe->user->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= h($procedimentoCaddetalhe->created) ?>
                                            </td>
                                            <td>
                                                <?= h($procedimentoCaddetalhe->modified) ?>
                                            </td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'procedimentoCaddetalhes','action' => 'view', $procedimentoCaddetalhe->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'procedimentoCaddetalhes','action' => 'edit', $procedimentoCaddetalhe->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                        <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'procedimentoCaddetalhes','action'=>'delete', $procedimentoCaddetalhe->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>