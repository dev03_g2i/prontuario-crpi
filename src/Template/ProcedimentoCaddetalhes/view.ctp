<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Procedimento Caddetalhes</h2>
            <ol class="breadcrumb">
                <li>Procedimento Caddetalhes</li>
                <li class="active">
                    <strong>Litagem de Procedimento Caddetalhes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Procedimento Caddetalhes</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($procedimentoCaddetalhe->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Nome') ?>
                                    </th>
                                    <td>
                                        <?= h($procedimentoCaddetalhe->nome) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $procedimentoCaddetalhe->has('user') ? $this->Html->link($procedimentoCaddetalhe->user->nome, ['controller' => 'Users', 'action' => 'view', $procedimentoCaddetalhe->user->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situacao Cadastro') ?>
                                    </th>
                                    <td>
                                        <?= $procedimentoCaddetalhe->situacao_cadastro->nome ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($procedimentoCaddetalhe->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($procedimentoCaddetalhe->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>