<div class="this-place">
    <div class="wrapper  white-bg page-heading">
        <div class="col-lg-9">
            <h2>Iten Cobrancas</h2>
            <ol class="breadcrumb">
                <li>Iten Cobrancas</li>
                <li class="active">
                    <strong>Litagem de Iten Cobrancas</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Editar', ['action' => 'edit', '?'=>['procedimento'=> $procedimentos->id,'ajax'=>1]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Iten Cobrancas', 'class' => 'btn btn-primary btn-sm', 'escape' => false]) ?>
                        <?php if(count($itenCobrancas)<=0): ?>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar', ['action' => 'add', '?'=>['procedimento'=> $procedimentos->id,'ajax'=>1]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Iten Cobrancas', 'class' => 'btn btn-primary btn-sm', 'escape' => false]) ?>
                        <?php endif; ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Iten Cobrancas') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('tipo') ?></th>
                                        <th><?= $this->Paginator->sort('descricao') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_id') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($itenCobrancas as $itenCobranca): ?>
                                        <tr>
                                            <td><?= $this->Number->format($itenCobranca->id) ?></td>
                                            <?php
                                                if($itenCobranca->tipo=='d'){
                                                    echo '<td>Dentes</td>';
                                                    echo '<td>';
                                                    echo $itenCobranca->has('dente') ? $this->Html->link($itenCobranca->dente->descricao, ['controller' => 'Dentes', 'action' => 'view', $itenCobranca->dente->id]) : '';
                                                    echo '</td>';
                                                }else if($itenCobranca->tipo=='r'){
                                                    echo '<td>Região</td>';
                                                    echo '<td>';
                                                    echo $itenCobranca->has('regio') ? $this->Html->link($itenCobranca->regio->nome, ['controller' => 'Regioes', 'action' => 'view', $itenCobranca->regio->id]) : '';
                                                    echo '</td>';
                                                }else if($itenCobranca->tipo=='f'){
                                                    echo '<td>Faces</td>';
                                                    echo '<td>';
                                                    echo $itenCobranca->has('face') ? $this->Html->link($itenCobranca->face->nome, ['controller' => 'Faces', 'action' => 'view', $itenCobranca->face->id]) : '';
                                                    echo '</td>';
                                                }
                                            ?>
                                            <td><?= $itenCobranca->has('situacao_cadastro') ? $this->Html->link($itenCobranca->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $itenCobranca->situacao_cadastro->id]) : '' ?></td>
                                            <td><?= $itenCobranca->has('procedimento') ? $this->Html->link($itenCobranca->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $itenCobranca->procedimento->id]) : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick'=>'Deletar(\'ItenCobrancas\','.$itenCobranca->id.',\'procedimentos\','.$itenCobranca->procedimento->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen'=>'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

