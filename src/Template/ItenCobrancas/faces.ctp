<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Iten Cobrancas</h2>
            <ol class="breadcrumb">
                <li>Iten Cobrancas</li>
                <li class="active">
                    <strong> Cadastrar Iten Cobrancas - Faces </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="itenCobrancas form">
                            <?= $this->Form->create($itenCobranca,['id'=>'frm-itens']) ?>
                            <fieldset>
                                <input type="hidden" name="tipo" value="0" checked />
                                <?php
                                echo "<div class='col-md-12' style='padding-left: 0; margin-bottom:20px'>";
                                echo '<label class="radio-inline">
                                      <input type="radio" name="tipo" id="tipo" value="f"> <strong>Cobrança para faces?</strong>
                                    </label>';
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                    foreach ($faces as $face) {
                                        echo "<div class='col-md-12' style='padding-left: 0;'>";
                                        echo '<label class="checkbox-inline">
                                                  <input type="checkbox" name="faces[]" id="f-'.$face->id.'" value="'.$face->id.'"> '.$face->nome.'
                                              </label>';
                                        echo "</div>";
                                    }

                                echo "</div>";

                                if(!empty($procedimento)){
                                    echo $this->Form->input('procedimento_id', ['value' => $procedimento->id,'type'=>'hidden']);
                                }else{
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('procedimento_id', ['options' => $procedimentos]);
                                    echo "</div>";
                                }
                            ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Concluir'), ['class' => 'btn btn-primary', 'onclick' => 'salvar_faces("frm-itens")']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

