

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Iten Cobrancas</h2>
        <ol class="breadcrumb">
            <li>Iten Cobrancas</li>
            <li class="active">
                <strong>Litagem de Iten Cobrancas</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="itenCobrancas">
    <h3><?= h($itenCobranca->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= h($itenCobranca->tipo) ?></td>
        </tr>
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($itenCobranca->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $itenCobranca->has('situacao_cadastro') ? $this->Html->link($itenCobranca->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $itenCobranca->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Procedimento') ?></th>
            <td><?= $itenCobranca->has('procedimento') ? $this->Html->link($itenCobranca->procedimento->nome, ['controller' => 'Procedimentos', 'action' => 'view', $itenCobranca->procedimento->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($itenCobranca->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($itenCobranca->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($itenCobranca->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

