<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Iten Cobrancas</h2>
            <ol class="breadcrumb">
                <li>Iten Cobrancas</li>
                <li class="active">
                    <strong> Cadastrar Iten Cobrancas
                    </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="itenCobrancas form">
                            <?= $this->Form->create($itenCobranca,['id'=>'frm-itens']) ?>
                            <fieldset>
                                <?php
                                echo "<div class='col-md-12' style='padding-left: 0; margin-bottom:20px'>";
                                echo '<label class="radio-inline">
                                      <input type="radio" name="tipo" id="tipo" value="d"> <strong>Nos dentes</strong>
                                    </label>';
                                echo '<label class="radio-inline">';
                                echo '<input type="checkbox" name="stodos" id="stodos" value="d"> <strong>Selecionar todos</strong>';
                                echo '</label>';
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                    foreach ($dentes as $dente) {
                                        echo "<div class='col-md-2' style='padding-left: 0;padding-right: 0;max-width: 93px;'>";
                                        echo '<label class="checkbox-inline">
                                                  <input type="checkbox" name="dentes[]" id="d-'.$dente->id.'" value="'.$dente->id.'"> '.$dente->descricao.'
                                              </label>';
                                        echo "</div>";
                                    }

                                echo "</div>";

                                echo "<div class='col-md-12' style='padding-left: 0; margin-top:20px; margin-bottom:20px'>";
                                echo '<label class="radio-inline">
                                      <input type="radio" name="tipo" id="tipo" value="r"> <strong>Nas regiões</strong>
                                    </label>';
                                echo '<label class="radio-inline">';
                                echo '<input type="checkbox" name="stodos" id="stodos" value="r"> <strong>Selecionar todos</strong>';
                                echo '</label>';
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                foreach ($regioes as $regio) {
                                    echo "<div class='col-md-6' style='padding-left: 0;margin-bottom: 10px'>";
                                    echo '<label class="checkbox-inline">
                                                  <input type="checkbox" name="regioes[]" id="r-'.$regio->id.'" value="'.$regio->id.'"> '.$regio->nome.'
                                              </label>';
                                    echo "</div>";
                                }

                                echo "</div>";

                                if(!empty($procedimento)){
                                    echo $this->Form->input('procedimento_id', ['value' => $procedimento->id,'type'=>'hidden']);
                                }else{
                                    echo "<div class='col-md-6'>";
                                    echo $this->Form->input('procedimento_id', ['options' => $procedimentos]);
                                    echo "</div>";
                                }
                            ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'continuar_itens("frm-itens")']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
</div>

