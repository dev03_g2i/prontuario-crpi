
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Grupos</h2>
        <ol class="breadcrumb">
            <li>Fin Grupos</li>
            <li class="active">
                <strong>Litagem de Fin Grupos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Grupos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($finGrupo->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Criadopor') ?></th>
                                <td><?= h($finGrupo->criadopor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Alteradopor') ?></th>
                                <td><?= h($finGrupo->alteradopor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finGrupo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao') ?></th>
                                <td><?= $this->Number->format($finGrupo->situacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dtcriacao') ?></th>
                                                                <td><?= h($finGrupo->dtcriacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dtalteracao') ?></th>
                                                                <td><?= h($finGrupo->dtalteracao) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


