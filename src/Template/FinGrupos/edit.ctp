<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Grupos</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finGrupos form">
                            <?= $this->Form->create($finGrupo) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Grupo') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('descricao', ['label' => 'Nome', 'required' => 'required']); ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $this->Form->input('empresas', ['type' => 'select', 'empty' => 'Selecione', 'class' => 'select2', 'multiple' => 'multiple', 'options' => $contabilidades, 'default' => $idsContabilidades]); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>