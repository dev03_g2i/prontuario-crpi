<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Bem vindo Gestor</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('FinIndicadores', ['type' => 'get']) ?>
                                <div class="col-sm-6 no-padding">
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('month', ['id' => 'month', 'label'=> 'Meses', 'type' => 'select', 'options' => $months, 'default' => $actualMonth]);?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $this->Form->input('year', ['id' => 'year', 'label'=> 'Anos', 'type' => 'select', 'options' => $years, 'default' => $actualYear] )?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'indicators-search', 'class' => 'btn btn-default m-t-23', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                    </div>
                                <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="row" id="first-row">
        <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding connectedSortable">
            <div class="ibox ibox-content float-e-margins col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="ibox-title">
                    Gráfico
                </div>
                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                    <div class="chart-container" id="chart-container-moves-bar" style="position: relative;">
                        <canvas id="MovesOfBalanceBar"></canvas>
                    </div>
                </div>
                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                    <div class="chart-container" id="chart-container-moves-line" style="position: relative;">
                        <canvas id="MovesOfBalanceLine"></canvas>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('chart',['block' => 'scriptBottom']);
echo $this->Html->script('controllers/IndicadoresController',['block' => 'scriptBottom']);
?>