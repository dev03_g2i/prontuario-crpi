<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Relatório</li>
            <li class="active">
                <strong>Artigos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Relatorios', ['method' => 'get']) ?>

                        <div class="col-md-3">
                            <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'type' => 'text', 'class' => "datepicker", 'value' => $this->Time->format($inicio, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class="col-md-3">
                            <?= $this->Form->input('fim', ['label' => 'Data Final', 'type' => 'text', 'class' => "datepicker", 'value' => $this->Time->format($fim, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class='col-md-3'>
                            <?= $this->Form->input('artigo_id', ['data' => 'select', 'controller' => 'EstqArtigo', 'action' => 'fill']); ?>
                        </div>

                        <div class="col-md-3" style="margin-top: 28px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Executores') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Tratamento</th>
                                    <th>Paciente</th>
                                    <th>Procedimento</th>
                                    <th>Tipo</th>
                                    <th>Descrição</th>
                                    <th>Faces</th>
                                    <th>Data</th>
                                    <th>Cód Artigo</th>
                                    <th>Artigo</th>
                                    <th>Utiliza Repasse</th>
                                    <th>Valor Repasse</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($itens as $i): ?>
                                    <tr>
                                        <td><?= $i->atendimento_id ?></td>
                                        <td><?= $i->cliente ?></td>
                                        <td><?= $i->procedimento ?></td>
                                        <td><?= $i->tipo ?></td>
                                        <td><?= $i->descricao ?></td>
                                        <td><?= $i->face ?></td>
                                        <td><?= $this->Time->format($i->created, 'dd/MM/Y') ?></td>
                                        <td><?= $i->codigo?></td>
                                        <td><?= $i->artigo ?></td>
                                        <td><?= $i->repasse==1 ? 'Sim' :'Não'; ?> </td>
                                        <td><?= $this->Number->currency($i->valor_repasse) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>