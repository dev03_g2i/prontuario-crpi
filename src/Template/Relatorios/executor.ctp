<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Relatório</li>
            <li class="active">
                <strong>Executores</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Relatorios', ['method' => 'get']) ?>

                        <div class="col-md-3">
                            <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'type' => 'text', 'class' => "datepicker", 'value' => $this->Time->format($inicio, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class="col-md-3">
                            <?= $this->Form->input('fim', ['label' => 'Data Final', 'type' => 'text', 'class' => "datepicker", 'value' => $this->Time->format($fim, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class='col-md-3'>
                            <?= $this->Form->input('profissional_id', ['data' => 'select', 'controller' => 'medicoResponsaveis', 'action' => 'fill']); ?>
                        </div>

                        <div class="col-md-3" style="margin-top: 28px">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Executores') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Paciente</th>
                                    <th>Executor</th>
                                    <th>Tratamento</th>
                                    <th>Procedimento</th>
                                    <th>Tipo</th>
                                    <th>Descrição</th>
                                    <th>Faces</th>
                                    <th>Data</th>
                                    <th>Valor Iten</th>
                                    <th>% Pagamento</th>
                                    <th>% Desconto</th>
                                    <th>Liquido</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $credito = 0;
                                $debito = 0;
                                $liquido = 0;
                                foreach ($itens as $i):
                                    $credito += $i->valor_faturar;
                                    $debito += $i->valor_desconto;
                                    $liquido += $i->valor_liquido;
                                    ?>
                                    <tr>
                                        <td><?= $i->cliente ?></td>
                                        <td><?= $i->executor ?></td>
                                        <td><?= $i->atendimento_id ?></td>
                                        <td><?= $i->procedimento ?></td>
                                        <td><?= $i->tipo ?></td>
                                        <td><?= $i->descricao ?></td>
                                        <td><?= $i->face ?></td>
                                        <td><?= $this->Time->format($i->data_executor, 'dd/MM/Y') ?></td>
                                        <td><?= $this->Number->currency($i->valor_iten) ?></td>
                                        <td><?= $this->Number->currency($i->valor_faturar) ?></td>
                                        <td><?= $this->Number->currency($i->valor_desconto) ?></td>
                                        <td><?= $this->Number->currency($i->valor_liquido) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <th colspan="9">Sub-total</th>
                                    <th><?= $this->Number->currency($credito) ?></th>
                                    <th><?= $this->Number->currency($debito) ?></th>
                                    <th><?= $this->Number->currency($liquido) ?></th>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>

            <?php
            if (!empty($quantidade_prof)): ?>
                <div class="clearfix">
                    <div class="table-responsive">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Profissional - Crédito/Débito</h5>
                            </div>
                            <div class="ibox-content">

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Descrição</th>
                                        <th>Profissional</th>
                                        <th>Data</th>
                                        <th>Observação</th>
                                        <th>Crédito</th>
                                        <th>Débito</th>
                                        <th>Liquido</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $cred_prof = 0;
                                    $deb_prof = 0;
                                    foreach ($profissional_creditos as $p):
                                        $cred_prof += $p->credito;
                                        $deb_prof += $p->debito;
                                        ?>
                                        <tr>
                                            <td><?= $p->descricao ?></td>
                                            <td><?= $p->medico_responsavei->nome ?></td>
                                            <td><?= $p->data ?></td>
                                            <td><?= $p->observacao ?></td>
                                            <td><?= $this->Number->currency($p->credito) ?></td>
                                            <td><?= $this->Number->currency($p->debito) ?></td>
                                            <td><?= $this->Number->currency($p->credito - $p->debito) ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    <tr>
                                        <th colspan="4">Sub-total</th>
                                        <th><?= $this->Number->currency($cred_prof) ?></th>
                                        <th><?= $this->Number->currency($deb_prof) ?></th>
                                        <th><?= $this->Number->currency($cred_prof - $deb_prof) ?></th>
                                    </tr>
                                    <?php
                                    $t_liquido = ($cred_prof + $credito) - ($deb_prof + $debito);
                                    ?>
                                    <tr>
                                        <th colspan="4">Total</th>
                                        <th><?= $this->Number->currency($cred_prof + $credito) ?></th>
                                        <th><?= $this->Number->currency($deb_prof + $debito) ?></th>
                                        <th><?= $this->Number->currency($t_liquido) ?></th>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>