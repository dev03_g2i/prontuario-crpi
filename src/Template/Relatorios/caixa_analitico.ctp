<script type="text/javascript">
    // Create the report viewer with default options
    var viewer = new Stimulsoft.Viewer.StiViewer(null, "StiViewer", false);

    // Create a new report instance
    var report = new Stimulsoft.Report.StiReport();
    // Load report from url
    report.loadFile("<?= $this->Url->image('/reports/relatorios/'.$report); ?>");
    // Assign report to the viewer, the report will be built automatically after rendering the viewer
    viewer.report = report;
    viewer.renderHtml();
</script>
