<div class="wrapper  white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Relatório</li>
            <li class="active">
                <strong>Relatório de Conferência de Faturamento</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Relatorios', ['url' => ['action' => 'conferencia-faturamento'], 'target' => '_blank']) ?>

                        <div class="col-md-6">
                            <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class="col-md-6">
                            <?= $this->Form->input('fim', ['label' => 'Data Final', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?=$this->Form->input('medico_id', ['label' => 'Profissional', 'empty' => 'Selecione','data' => 'select','controller'=>'MedicoResponsaveis', 'action'=>'fill','data-value'=>(empty($user_logado['codigo_medico']) ? '' : $user_logado['codigo_medico'])])?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('convenio_id', ['label' => 'Convênio', 'type' => 'select', 'empty' => 'Selecione', 'options' => $convenios]); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?=$this->Form->input('tipo', ['label' => 'Relatório','type' => 'select','options' =>[ 1=>'P/ Profissional', 2=>'Convênio',3=>' Prof. X Exe.', 4=>'Excel']]);?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $this->Form->button($this->Html->icon('print') . ' Gerar Relatório', ['type' => 'submit', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Relatório', 'escape' => false, 'listen' => 'f']) ?>
                        </div>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>