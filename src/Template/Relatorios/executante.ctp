<div class="wrapper  white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Relatório</li>
            <li class="active">
                <strong>Caixa Executante</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Relatorios', ['url' => ['action' => 'caixa-executante'], 'target' => '_blank']) ?>

                        <div class="col-md-6">
                            <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class="col-md-6">
                            <?= $this->Form->input('fim', ['label' => 'Data Final', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?= $this->Form->input('periodo_id', ['label' => 'Periodo', 'type' => 'select', 'empty' => 'Selecione', 'options' => $periodos, 'default' => null]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('medico_id', ['label' => 'Profissional', 'empty' => 'Selecione', 'data' => 'select', 'controller' => 'MedicoResponsaveis', 'action' => 'fill', 'data-value' => (empty($user_logado['codigo_medico']) ? '' : $user_logado['codigo_medico'])]) ?>
                        </div>

                        <div class="clearfix"></div>
                        <!--                        <div class="col-md-6">-->
                        <!--                            <=$this->Form->input('solicitante_id', ['label' => 'Solicitante', 'empty' => 'Selecione','data' => 'select','controller'=>'Solicitantes', 'action'=>'fill'])?>-->
                        <!--                        </div>-->
                        <div class="col-md-6">
                            <?= $this->Form->input('report', ['label' => 'Relatório', 'required' => 'required', 'type' => 'select', 'empty' => 'Selecione', 'options' => $tiposRelatorios, 'default' => null]) ?>
                        </div>
                        <?php if ($user_logado['caixa_outros_usuarios'] == 0): ?>
                            <?= $this->Form->input('usuario_id', ['type' => 'hidden', 'value' => $user_logado['id']]) ?>
                        <?php else: ?>
                            <div class="col-md-6">
                                <?= $this->Form->input('usuario_id', ['label' => 'Usuário', 'empty' =>
                                    'Selecione', 'data' => 'select', 'controller' => 'Users', 'action' => 'fill']) ?>
                            </div>
                        <?php endif; ?>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $this->Form->button($this->Html->icon('print') . ' Gerar Relatório', ['type' => 'submit', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Relatório', 'escape' => false, 'listen' => 'f']) ?>
                        </div>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>