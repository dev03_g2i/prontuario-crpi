<div class="wrapper  white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatórios</h2>
        <ol class="breadcrumb">
            <li>Relatório</li>
            <li class="active">
                <strong>Relatório de Produtividade</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Selecione o relatório desejado</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Relatorios', ['url' => ['action' => 'report-produtividade'], 'target' => '_blank']) ?>

                        <div class="col-md-6">
                            <?=$this->Form->input('tipo', ['label' => 'Relatório','type' => 'select','options' =>[ 0=>'Geral',1=>'Analítico', 3=>'Excel']]);?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <?= $this->Form->button($this->Html->icon('print') . ' Gerar Relatório', ['type' => 'submit', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Relatório', 'escape' => false, 'listen' => 'f']) ?>
                        </div>

                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>