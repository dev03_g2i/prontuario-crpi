<div class="this-place">
    <?= $this->Form->create($cliente, ['type' => 'file']) ?>
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-11">
            <h2>Atendimentos</h2>
            <ol class="breadcrumb">
                <li>Pacientes</li>
                <li class="active">
                    <strong> Editar Paciente
                    </strong>
                </li>
            </ol>
            <?php
            echo "<div class='text-info'>";
            echo $this->Form->input('digital', ['label' => 'Prontuário digital?']);
            echo "</div>";
            ?>
        </div>
        <div class="col-md-1 col-sm-2">
            <h5 class="text-center" onclick="upload()" id="uploadFoto" style="cursor:pointer">
                <?php if (empty($cliente->caminho)) {
                    echo $this->Html->image('/img/content/img/empty-full-avatar.jpg', ['title' => 'Upload Imagem', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'style' => 'cursor:pointer', 'class' => 'img-thumbnail img-responsive']);
                } else {
                    echo $this->Html->image('/files/clientes/caminho/' . $cliente->caminho_dir . '/portrait_' . $cliente->caminho, ['class' => 'img-thumbnail img-responsive', 'title' => 'Alterar Imagem', 'toggle' => 'tooltip', 'data-placement' => 'bottom']);
                } ?>
                <strong>Editar</strong>

            </h5>
            <input type="file" name="caminho" id="foto" style="visibility: hidden;">
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="clientes form">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= __('Identificação') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome');
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nascimento', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($cliente->nascimento, 'dd/MM/YYYY')]);
                                    echo "</div>";
                                    if ($this->Configuracao->getConfigArquivo() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('arquivo');
                                        echo "</div>";
                                    } elseif ($this->Configuracao->getConfigArquivo() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('arquivo', ['required' => true]);
                                        echo "</div>";
                                    }
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('sexo', ['options' => ['M' => 'Masculino', 'F' => 'Feminino'], 'empty' => 'Selecione']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cpf', ['mask' => 'cpf','onchange' => 'validaCPF("form-cadastro-cliente", $(this))', 'required' => false]);
                                    echo "</div>";
                                    if ($this->Configuracao->getConfigRg() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('rg');
                                        echo "</div>";
                                    } elseif ($this->Configuracao->getConfigRg() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('rg', ['required' => true]);
                                        echo "</div>";
                                    }
                                    if ($this->Configuracao->getConfigCid() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('cid');
                                        echo "</div>";
                                    } elseif ($this->Configuracao->getConfigCid() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('cid', ['required' => true]);
                                        echo "</div>";
                                    }
                                    if ($this->Configuracao->getConfigProfissao() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('profissao', ['label' => 'Profissão']);
                                        echo "</div>";
                                    } elseif ($this->Configuracao->getConfigProfissao() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('profissao', ['required' => true, 'label' => 'Profissão']);
                                        echo "</div>";
                                    }

                                    if ($this->Configuracao->mostraReligiaoOnClientes() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('religiao_id', ['options' => $religioes, 'empty' => 'Selecione', 'default' => '', 'label' => 'Religião']);
                                        echo "</div>";
                                    } else if ($this->Configuracao->mostraReligiaoOnClientes() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('religiao_id', ['options' => $religioes, 'empty' => 'Selecione', 'default' => '', 'required' => true, 'label' => 'Religião']);
                                        echo "</div>";
                                    }
                                    if ($this->Configuracao->getConfigNacionalidade() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('nascionalidade', ['label' => 'Nacionalidade']);
                                        echo "</div>";
                                    } else if ($this->Configuracao->getConfigNacionalidade() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('nascionalidade', ['label' => 'Nacionalidade', 'required' => true]);
                                        echo "</div>";
                                    }

                                    if ($this->Configuracao->getConfigNaturalidade() == 2) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('naturalidade', ['label' => 'Naturalidade']);
                                        echo "</div>";
                                    } else if ($this->Configuracao->getConfigNaturalidade() == 3) {
                                        echo "<div class='col-md-4'>";
                                        echo $this->Form->input('naturalidade', ['label' => 'Naturalidade', 'required' => true]);
                                        echo "</div>";
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Contato') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('celular', ['mask' => 'fone']);
                                echo "</div>";

                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('telefone', ['mask' => 'telefone']);
                                echo "</div>";

                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('comercial', ['mask' => 'telefone']);
                                echo "</div>";

                                if ($this->Configuracao->getConfigEndereco() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cep', ['mask' => 'cep', 'role' => 'cep']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('endereco', ['label' => 'Endereço', 'data-cep' => 'endereco']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('numero', ['label' => 'Número', 'type' => 'number']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('bairro', ['data-cep' => 'bairro']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('estado', ['data-cep' => 'uf']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cidade', ['data-cep' => 'cidade']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigEndereco() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cep', ['mask' => 'cep', 'role' => 'cep', 'required' => true]);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('endereco', ['label' => 'Endereço', 'data-cep' => 'endereco', 'required' => true]);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('numero', ['label' => 'Número', 'type' => 'number', 'required' => true]);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('bairro', ['data-cep' => 'bairro', 'required' => true]);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('estado', ['data-cep' => 'uf', 'required' => true]);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cidade', ['data-cep' => 'cidade', 'required' => true]);
                                    echo "</div>";
                                }

                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('complemento');
                                echo "</div>";

                                if ($this->Configuracao->getConfigEmail() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('email');
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigEmail() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('email', ['required' => true]);
                                    echo "</div>";
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Dados de atendimento') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                if ($this->Configuracao->getConfigMatriculaValidade() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('matricula', ['label' => 'Matricula/Carteira']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('validade_carteira', ['label' => 'Validade', 'type' => 'text', 'class' => 'datepicker']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigMatriculaValidade() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('matricula', ['label' => 'Matricula/Carteira', 'required' => true]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('validade_carteira', ['label' => 'Validade', 'type' => 'text', 'class' => 'datepicker', 'required' => true]);
                                    echo "</div>";
                                }

                                if ($this->Configuracao->getConfigConvenio() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'Selecione', 'label' => 'Convênios']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigConvenio() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'Selecione', 'label' => 'Convênios', 'required' => true]);
                                    echo "</div>";
                                }

                                if ($this->Configuracao->getConfigMedico() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'empty' => 'Selecione', 'label' => 'Médico']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigMedico() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'empty' => 'Selecione', 'label' => 'Médico', 'required' => true]);
                                    echo "</div>";
                                }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Dados complementares') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php

                                if ($this->Configuracao->getConfigNomePais() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_pai', ['label' => 'Nome Pai']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_mae', ['label' => 'Nome Mãe']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigNomePais() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_pai', ['label' => 'Nome Pai', 'required' => true]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_mae', ['label' => 'Nome Mãe', 'required' => true]);
                                    echo "</div>";
                                }

                                if ($this->Configuracao->getConfigProfissaoPais() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('pai_profissao', ['label' => 'Profissão Pai']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('mae_profissao', ['label' => 'Profissão Mãe']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigProfissaoPais() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('pai_profissao', ['label' => 'Profissão Pai', 'required' => true]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('mae_profissao', ['label' => 'Profissão Mãe', 'required' => true]);
                                    echo "</div>";
                                }

                                if ($this->Configuracao->getConfigIdadePais() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('pai_idade', ['label' => 'Idade Pai']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('mae_idade', ['label' => 'Idade Mãe']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigIdadePais() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('pai_idade', ['label' => 'Idade Pai', 'required' => true]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('mae_idade', ['label' => 'Idade Mãe', 'required' => true]);
                                    echo "</div>";
                                }


                                if ($this->Configuracao->getConfigEstadoCivil() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('estadocivil_id', ['options' => $estadoCivis, 'empty' => 'Selecione', 'label' => 'Estado civil']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigEstadoCivil() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('estadocivil_id', ['options' => $estadoCivis, 'empty' => 'Selecione', 'label' => 'Estado civil', 'required' => true]);
                                    echo "</div>";
                                }


                                if ($this->Configuracao->getConfigNomeConjuge() == 2) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_conjuge', ['label' => 'Nome do Conjugue']);
                                    echo "</div>";
                                } elseif ($this->Configuracao->getConfigNomeConjuge() == 3) {
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_conjuge', ['label' => 'Nome do Conjugue', 'required' => true]);
                                    echo "</div>";
                                }

                                echo "<div class='col-md-4'>";
                                echo $this->Form->input('indicacao_id', ['required' => $this->Configuracao->requiredIndicacao(), 'label' => 'Indicação', 'options' => $indicacao, 'empty' => 'selecione', 'id' => 'indicacao_id']);
                                echo "</div>";
                                echo "<div class='col-md-4' id='indicacao-itens' " . (empty($cliente->indicacao_paciente) ? '' : 'style="display: none"') . ">";
                                echo $this->Form->input('indicacao_itens_id', ['label' => 'Indicação Itens', 'data' => 'select', 'controller' => 'IndicacaoItens', 'action' => 'fill', 'data-value' => $cliente->indicacao_itens_id]);
                                echo "</div>";

                                echo "<div class='col-md-4' id='cliente-itens' " . (empty($cliente->indicacao_paciente) ? ' style="display: none" ' : '') . ">";
                                echo $this->Form->input('indicacao_paciente', ['label' => 'Indicação Paciente', 'type' => 'select', 'data' => 'select', 'controller' => 'Clientes', 'action' => 'fill', 'data-value' => $cliente->indicacao_paciente]);
                                echo "</div>";

                                echo '<div class="col-lg-12 text-right">';
                                echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary']);
                                echo '</div>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Observações/Particularidades') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2]);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('particularidades', ['label' => 'Particularidades da saúde geral', 'rows' => 2]);
                                echo "</div>";
                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('recepcao', ['label' => 'Observação da recepção', 'rows' => 2]);
                                echo "</div>";

                                echo '<div class="col-lg-12 text-right">';
                                echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary']);
                                echo '</div>';

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>