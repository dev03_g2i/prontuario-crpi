<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h3>Pacientes</h3>
                <ol class="breadcrumb">
                    <li>Pacientes</li>
                    <li class="active">
                        <strong>Listagem de Pacientes</strong>
                    </li>
                </ol>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros ">
                    <?= $this->Form->create('Clientes', ['type' => 'get','id'=>'form-cliente']) ?>
                    <div class="col-md-3">
                        <?= $this->Form->input('nome',['type'=>'select','data-full-cliente'=>"cliente",'empty'=>'Selecione','label'=>'Paciente']);?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('id', ['placeholder' => 'Número do prontuário','label'=>'Prontuário']); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('arquivo', ['label'=>'Arquivo']); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('cpf', ['placeholder' => 'Cpf',"mask"=>"cpf"]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('digital', ['type' => 'select',"label"=>"Prontuário digial",'options'=>['Não','Sim'],'empty'=>'Selecione']); ?>
                    </div>
                    <div class="col-md-3" style="margin-top: 28px">
                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'escape' => false]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="text-right">
            <p>
                <?= $this->Html->link($this->Html->icon('plus') . ' Novo', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Paciente', 'class' => 'btn btn-primary', 'escape' => false]) ?>
            </p>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Pacientes') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('id', 'Prontuário') ?></th>
                                <th><?= $this->Paginator->sort('nome') ?></th>
                                <th><?= $this->Paginator->sort('cpf') ?></th>
                                <th><?= $this->Paginator->sort('telefone') ?></th>
                                <th><?= $this->Paginator->sort('celular') ?></th>
                                <th><?= $this->Paginator->sort('comercial') ?></th>
                                <th><?= $this->Paginator->sort('digital') ?></th>
                                <th><?= $this->Paginator->sort('arquivo') ?></th>
                                <th class="actions text-right"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($clientes as $cliente): ?>
                                <tr>
                                    <td><?= h($cliente->id) ?></td>
                                    <td><?= h($cliente->nome) ?></td>
                                    <td><?= h($cliente->cpf) ?></td>
                                    <td><?= h($cliente->telefone) ?></td>
                                    <td><?= h($cliente->celular) ?></td>
                                    <td><?= h($cliente->comercial) ?></td>
                                    <td><?= h(isset($cliente->digital)?$cliente->digital==1?'Sim':'Não':'') ?></td>
                                    <td><?= h($cliente->arquivo) ?></td>
                                    <td class="actions text-right">
                                        <?= $this->Html->link('<i class="fa fa-user-md"></i>', ['controller'=>'Atendimentos','action' => 'clientes', 'clientes'=>$cliente->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Atendimentos', 'escape' => false, 'class' => 'btn btn-xs btn-default','target'=>'_blanck']) ?>
                                        <?= $this->Html->link('<i class="fa fa-stethoscope"></i>', ['action' => 'prontuario', $cliente->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Prontuário', 'escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'modal','data-target'=>'#modal_lg','id'=>'btn-pront-'.$cliente->id]) ?>
                                        <?= $this->Html->link($this->Html->icon('resize-full'), ['action' => 'nview', $cliente->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
echo $this->Html->scriptBlock(
    ' 
            $(window).load(function(){
                if(('.$open.'==1) && !empty('.$pront.')){
                    $("#btn-pront-'.$pront.'").click()
                }
                 if(('.$open.'==1) && !empty('.$resp.')){
                    $("#btn-resp-'.$resp.'").click()
                }
            })
            ',
    ['inline' => false, 'block' => 'scriptLast']
);
?>