<?= $this->Form->create($cliente, ['id' => 'form-cliente', 'type' => 'file', 'url'=>['controller'=>'Clientes','action'=>'naddSave']]) ?>
<div class="white-bg page-heading">
    <div class="col-md-11 col-sm-10">
        <h2>Pacientes</h2>
        <ol class="breadcrumb">
            <li>Pacientes</li>
            <li class="active">
                <strong> Cadastrar Paciente
                </strong>
            </li>
        </ol>
        <?php
        echo "<div class='text-info'>";
        echo $this->Form->input('digital', ['label' => 'Prontuário digital?']);
        echo "</div>";
        ?>
    </div>
    <div class="col-md-1 col-sm-2">
        <h5 class="text-center" onclick="upload()" id="uploadFoto" style="cursor:pointer">
            <?php if (empty($cliente->caminho)) {
                echo $this->Html->image('/img/content/img/empty-full-avatar.jpg', ['title' => 'Upload Imagem', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'style' => 'cursor:pointer', 'class' => 'img-thumbnail img-responsive']);
            } else {
                echo $this->Html->image('/files/clientes/caminho/' . $cliente->caminho_dir . '/portrait_' . $cliente->caminho, ['class' => 'img-thumbnail img-responsive', 'title' => 'Alterar Imagem', 'toggle' => 'tooltip', 'data-placement' => 'bottom']);
            } ?>
            <strong>Editar</strong>

        </h5>
        <input type="file" name="caminho" id="foto" style="visibility: hidden;">
    </div>
    <div class='clearfix'></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="clientes form">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= __('Identificação') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome');
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nascimento', ['type' => 'text', 'mask' => 'data']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('arquivo', ['type' => 'hidden']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('sexo', ['options' => ['M' => 'Masculino', 'F' => 'Feminino'], 'empty' => 'Selecione']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cpf', ['mask' => 'cpf', 'onchange' => 'validaCPF("form-cliente", $(this))', 'required' => true]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('rg');
                                    echo "</div>";
                                    echo "<div class='col-md-4 hidden'>";
                                    echo $this->Form->input('cid');
                                    echo "</div>";
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= __('Contato') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('celular', ['mask' => 'fone']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('telefone', ['mask' => 'telefone']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('comercial', ['mask' => 'telefone']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cep', ['mask' => 'cep', 'role' => 'cep']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('endereco', ['label' => 'Endereço', 'data-cep' => 'endereco']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('numero', ['label' => 'Número', 'type' => 'number']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('bairro', ['data-cep' => 'bairro']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('estado', ['data-cep' => 'uf']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('cidade', ['data-cep' => 'cidade']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('email');
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('complemento');
                                    echo "</div>"; ?>
                                    <div class="row">
                                        <div class="col-lg-12 text-right">
                                          <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'onclick' => 'SalveForm(\'form-cliente\')']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hidden">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= __('Dados de atendimento') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('matricula', ['label' => 'Matricula/Carteira']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('validate_carteira', ['label' => 'Validade', 'type' => 'text', 'class' => 'datepicker']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'Selecione', 'label' => 'Convênios']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'empty' => 'Selecione', 'label' => 'Médico']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('particularidades', ['label' => 'Particularidades da saúde geral', 'rows' => 2]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('recepcao', ['label' => 'Observação da recepção', 'rows' => 2]);
                                    echo "</div>";
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hidden">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= __('Dados complementares') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_pai', ['label' => 'Nome Pai']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('pai_profissao', ['label' => 'Profissão Pai']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('pai_idade', ['label' => 'Idade Pai']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_mae', ['label' => 'Nome Mãe']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('mae_profissao', ['label' => 'Profissão Mãe']);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('mae_idade', ['label' => 'Idade Mãe']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('profissao', ['label' => 'Profissão']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('estadocivil_id', ['options' => $estadoCivis, 'empty' => 'Selecione', 'label' => 'Estado civil']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('nome_conjuge', ['label' => 'Nome do Conjugue']);
                                    echo "</div>";

                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('indicacao_id', ['label' => 'Indicação', 'options' => $indicacao, 'empty' => 'selecione', 'id' => 'indicacao_id']);
                                    echo "</div>";
                                    echo "<div class='col-md-4' id='indicacao-itens' " . (empty($cliente->indicacao_paciente) ? '' : 'style="display: none"') . ">";
                                    echo $this->Form->input('indicacao_itens_id', ['label' => 'Indicação Itens', 'data' => 'select', 'controller' => 'IndicacaoItens', 'action' => 'fill', 'data-value' => $cliente->indicacao_itens_id]);
                                    echo "</div>";

                                    echo "<div class='col-md-4' id='cliente-itens' " . (empty($cliente->indicacao_paciente) ? ' style="display: none" ' : '') . ">";
                                    echo $this->Form->input('indicacao_paciente', ['label' => 'Indicação Paciente', 'type' => 'select', 'data' => 'select', 'controller' => 'Clientes', 'action' => 'fill', 'data-value' => $cliente->indicacao_paciente]);
                                    echo "</div>";
                                    echo "<div class='col-md-4'>";
                                    echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2]);
                                    echo "</div>";

                                    /*echo $this->Form->input('cor', ['type' => 'hidden']);
                                    echo $this->Form->input('matricula', ['label' => 'Matrícula', 'type' => 'hidden']);
                                    echo $this->Form->input('medico_id', ['type' => 'hidden', 'options' => $medicoResponsaveis, 'empty' => 'Selecione', 'required' => 'required', 'label' => 'Profissional']);
                                    echo $this->Form->input('nascionalidade', ['type' => 'hidden', 'label' => 'Nacionalidade']);
                                    echo $this->Form->input('naturalidade', ['type' => 'hidden', 'label' => 'Naturalidade']);
                                    echo $this->Form->input('observacao', ['type' => 'hidden', 'label' => 'Observação']);*/
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-right">
<<<<<<< HEAD
=======
                                    <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'onclick' => 'SalveForm("form-cliente")']) ?>
                                    <!--<?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary', 'escape' => false]) ?>-->
>>>>>>> 150de92b1622f594b7fdf0662ab5aabb189da73d
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>

