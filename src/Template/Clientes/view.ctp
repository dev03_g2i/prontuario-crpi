<div class="white-bg page-heading">
    <div class="col-md-6">
        <h2>Pacientes</h2>
        <ol class="breadcrumb">
            <li>Pacientes</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class='ibox-title'>
                    <div class="col-md-8">
                        <h3>Paciente:<?= h($cliente->nome) ?></h3>
                    </div>
                    <div class="col-md-4">
                        <h3>Idade:<?= $this->Data->idade($cliente->nascimento) ?></h3>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Código') ?></th>
                                <td><?= h($cliente->id) ?></td>
                                <th><?= __('Arquivo') ?></th>
                                <td colspan="3"><?= h($cliente->arquivo) ?></td>
                            </tr>


                            <tr>
                                <th><?= __('Sexo') ?></th>
                                <td><?= h($cliente->sexo) ?></td>
                                <th><?= __('Estado Civil') ?></th>
                                <td ><?= $cliente->has('estado_civi') ? $cliente->estado_civi->nome : '' ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Nascimento') ?></th>
                                <td ><?= h($cliente->nascimento) ?></td>
                                <th><?= __('CID') ?></th>
                                <td><?= h($cliente->cid) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Profissão') ?></th>
                                <td colspan="3"><?= h($cliente->profissao) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Cpf') ?></th>
                                <td colspan="3"><?= h($cliente->cpf) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Rg') ?></th>
                                <td colspan="3"><?= h($cliente->rg) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Nome Pai') ?></th>
                                <td><?= h($cliente->nome_pai) ?></td>
                                <th><?= __('Idade') ?></th>
                                <td><?= h($cliente->pai_idade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Profissão') ?></th>
                                <td colspan="3"><?= h($cliente->pai_profissao) ?></td>
                            </tr>


                            <tr>
                                <th><?= __('Nome Mãe') ?></th>
                                <td><?= h($cliente->nome_mae) ?></td>
                                <th><?= __('Idade') ?></th>
                                <td><?= h($cliente->mae_idade) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Profissão') ?></th>
                                <td colspan="3"><?= h($cliente->mae_profissao) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td colspan="3"><?= h($cliente->cep) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Endereço') ?></th>
                                <td><?= h($cliente->endereco) ?></td>
                                <th><?= __('Número') ?></th>
                                <td><?= h($cliente->numero) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td colspan="3"><?= h($cliente->bairro) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Cidade') ?></th>
                                <td><?= h($cliente->cidade) ?></td>
                                <th><?= __('UF') ?></th>
                                <td><?= h($cliente->estado) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Celular') ?></th>
                                <td colspan="3"><?= h($cliente->celular) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Fone Residencial') ?></th>
                                <td colspan="3"><?= h($cliente->telefone) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Fone Comercial') ?></th>
                                <td colspan="3"><?= h($cliente->comercial) ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Email') ?></th>
                                <td colspan="3"><?= h($cliente->email) ?></td>
                            </tr>


                            <tr>
                                <th><?= __('Indicação') ?></th>
                                <td colspan="3"><?= $cliente->has('indicacao') ? $cliente->indicacao->nome : '' ?></td>

                            </tr>
                            <tr>
                                <?php if(!empty($cliente->indicacao_itens_id)): ?>
                                    <th><?= __('Indicação Itens') ?></th>
                                    <td><?= $cliente->has('indicacao_iten') ? $cliente->indicacao_iten->nome : '' ?></td>
                                <?php elseif(!empty($cliente->indicacao_paciente)): ?>
                                    <th><?= __('Indicação Paciente') ?></th>
                                    <td><?= $cliente->has('indicacao_cliente') ? $cliente->indicacao_cliente->nome : '' ?></td>
                                <?php endif; ?>
                            </tr>

                            <tr>
                                <th><?= __('Médico') ?></th>
                                <td colspan="3"><?= $cliente->has('medico_responsavei')? h($cliente->medico_responsavei->nome) : ''; ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Convênio') ?></th>
                                <td colspan="3"><?= $cliente->has('convenio')? h($cliente->convenio->nome): '' ?></td>
                            </tr>

                            <tr>
                                <th><?= __('Matricula/Carteira') ?></th>
                                <td><?= h($cliente->matricula) ?></td>
                                <th><?= __('Validade') ?></th>
                                <td><?= h($cliente->validade_carteira) ?></td>
                            </tr>

                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    echo '<br /><small>Cadastrado por:<strong>'.$cliente->user->nome.'</strong>  Em:<strong>'.$cliente->created.'</strong></small><br />';
                    if(!empty($cliente->user_edit)) {
                        echo '<small>Alterado por:<strong>' . $cliente->user_edit->nome . '</strong>  Em:<strong>' . $cliente->modified . '</strong></small>';
                    }
                    ?>
                </div>
            </div>
        </div>


    </div>
</div>

