<div class=" wrapper  white-bg page-heading">
    <div class="col-md-6">
        <h2>Pacientes</h2>
        <ol class="breadcrumb">
            <li>Pacientes</li>
            <li class="active">
                <strong>Detalhes</strong>
            </li>
        </ol>
    </div>
    <div class="col-md-6 text-right">
        <br/>
        <br/>
        <?= $this->Html->link('<i class="fa fa-print"></i>Imprimir Cadastro', ['controller' => 'Clientes', 'action' => 'reportCadastro', $cliente->id], ['target' => '_blank', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir Cadastro', 'escape' => false, 'class' => 'btn btn-xs btn-success', 'listen' => 'f']) ?>
        <?= $this->Html->link('<i class="fa fa-stethoscope"></i> Prontuário', ['action' => 'prontuario', $cliente->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Prontuário', 'escape' => false, 'class' => 'btn btn-xs btn-info', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
        <?= $this->Html->link($this->Html->icon('retweet') . ' Vinculos', ['controller' => 'cliente-ligacoes', 'action' => 'index', '?' => ['cliente_id' => $cliente->id, 'first' => 1, 'ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Vinculos', 'escape' => false, 'class' => 'btn btn-xs btn-primary', 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
        <?= $this->Html->link($this->Html->icon('pencil') . ' Editar', ['action' => 'edit', $cliente->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Identificação') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Nome:</label> <?= h($cliente->nome) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Idade:</label> <?= $this->Data->idade($cliente->nascimento) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Arquivo:</label> <?= h($cliente->arquivo) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Sexo:</label> <?= ($cliente->sexo == 'F') ? 'Feminino' : 'Masculino'; ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Nascimento:</label> <?= h($cliente->nascimento) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>CPF:</label> <?= h($cliente->cpf) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>RG:</label> <?= h($cliente->rg) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Cid:</label> <?= h($cliente->cid) ?>
                    </div>
                    <?php if ($this->Configuracao->mostraReligiaoOnClientes() != 1): ?>
                        <div class="col-lg-4 form-group">
                            <label>Religião:</label> <?= h($cliente->religiao->descricao) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Contato') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Celular:</label> <?= h($cliente->celular) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Telefone:</label> <?= h($cliente->telefone) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Comercial:</label> <?= h($cliente->comercial) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>CEP:</label> <?= h($cliente->cep) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Endereço:</label> <?= h($cliente->endereco) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Número:</label> <?= h($cliente->numero) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Bairro:</label> <?= h($cliente->bairro) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Estado:</label> <?= h($cliente->estado) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Cidade:</label> <?= h($cliente->cidade) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Email:</label> <?= h($cliente->email) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Complemeto:</label> <?= h($cliente->complemento) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Dados de atendimento') ?></h5>
            </div>
            <div class="ibox-content">

                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Matricula/Carteira:</label> <?= h($cliente->matricula) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Validade:</label> <?= h($cliente->validade_carteira) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Convenio:</label> <?= $cliente->has('convenio') ? h($cliente->convenio->nome) : '' ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Médico:</label> <?= $cliente->has('medico_responsavei') ? h($cliente->medico_responsavei->nome) : ''; ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Particularidades da saúde geral:</label> <?= h($cliente->particularidades) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Observação da recepção:</label> <?= h($cliente->recepcao) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Dados complementares') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Nome Pai:</label> <?= h($cliente->nome_pai) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Idade:</label> <?= h($cliente->pai_idade) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Profissão:</label> <?= h($cliente->pai_profissao) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Nome Mãe:</label> <?= h($cliente->nome_mae) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Idade:</label> <?= h($cliente->mae_idade) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Profissão:</label> <?= h($cliente->mae_profissao) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Profissão:</label> <?= h($cliente->profissao) ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Estado
                            civil:</label> <?= $cliente->has('estado_civi') ? $cliente->estado_civi->nome : '' ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Nome do Conjugue:</label> <?= h($cliente->nome_conjuge) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 form-group">
                        <label>Indicação:</label> <?= $cliente->has('indicacao') ? $cliente->indicacao->nome : '' ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Indicação
                            Itens:</label> <?= $cliente->has('indicacao_iten') ? $cliente->indicacao_iten->nome : '' ?>
                    </div>
                    <div class="col-lg-4 form-group">
                        <label>Observação:</label> <?= $cliente->observacao ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <?php
                        echo '<br /><small>Cadastrado por:<strong>' . $cliente->user->nome . '</strong>  Em:<strong>' . $cliente->created . '</strong></small><br />';
                        if (!empty($cliente->user_edit)) {
                            echo '<small>Alterado por:<strong>' . $cliente->user_edit->nome . '</strong>  Em:<strong>' . $cliente->modified . '</strong></small>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

