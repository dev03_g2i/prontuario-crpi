<div class="this-place">
    <div class=" wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Pacientes</h2>
            <ol class="breadcrumb">
                <li class="active">
                    <strong> Aniversariantes <?= $tipo; ?></strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Pacientes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('nome') ?></th>
                                        <th><?= $this->Paginator->sort('nascimento') ?></th>
                                        <th><?= $this->Paginator->sort('cpf') ?></th>
                                        <th><?= $this->Paginator->sort('telefone') ?></th>
                                        <th><?= $this->Paginator->sort('celular') ?></th>
                                        <th><?= $this->Paginator->sort('comercial') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($clientes as $cliente): ?>
                                        <tr>
                                            <td><?= h($cliente->nome) ?></td>
                                            <td><?= $this->Time->format($cliente->nascimento,'dd/MM/Y') ?></td>
                                            <td><?= h($cliente->cpf) ?></td>
                                            <td><?= h($cliente->telefone) ?></td>
                                            <td><?= h($cliente->celular) ?></td>
                                            <td><?= h($cliente->comercial) ?></td>

                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

