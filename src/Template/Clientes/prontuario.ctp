<div id="prontuario-g2i" class="this-place">

    <div class="wrapper white-bg page-heading">
        <div class="col-lg-11">
            <h2>Prontuário</h2>
        </div>
        <?php if ($aniversario): ?>
            <div class="col-lg-11">
                <span class='btn btn-large btn-danger' title='Aniversariante'><i class='fa fa-birthday-cake'></i></span>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>
    </div>

    <div class="white-bg ">
        <div class="col-lg-6">
            <div class="col-md-9">
                <h3>N.Prontuário (<?= $cliente->id ?>) / N.Arquivo: <span class="text-danger">(<?= $cliente->arquivo ?>
                        )</span></h3>
                <h4>Paciente: <?= $cliente->nome ?></h4>
            </div>
            <div class="col-md-3">
                <button class="btn btn-xs btn-info disabled" toggle="tooltip" data-placement="bottom"
                        title="Prontuário <?= h(isset($cliente->digital) ? $cliente->digital == 1 ? 'Digital' : 'Não Digital' : '') ?>">
                    <?= h(isset($cliente->digital) ? $cliente->digital == 1 ? 'Digital' : 'Não Digital' : '') ?>
                </button>
            </div>
            <div class="col-md-12">
                Convênio: <strong><?= $cliente->has('convenio') ? $cliente->convenio->nome : 'Não Informado' ?></strong>
            </div>

            <div class="col-md-12">
                Profissão: <strong><?= $cliente->profissao ?></strong>
            </div>

            <div class="col-md-12">
                Dt.Nascimento: <strong><?= ($cliente->nascimento) ? $cliente->nascimento : 'Indisponível'; ?></strong>
            </div>
            <div class="col-md-12">
                <?php if ($cliente->nascimento): ?>
                    Idade: <strong><?= $this->Data->idade($cliente->nascimento); ?></strong>
                <?php else: ?>
                    Idade: <strong>Indisponível</strong>
                <?php endif; ?>
            </div>
            <div class="col-md-12">
                CID: <strong><?= $cliente->cid ?></strong>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                Particularidades:
                <a class="edit_local" href="#" data-action="parcial_edit" data-type="textarea"
                   data-controller="Clientes" data-param="text" data-title="particularidades"
                   data-pk="<?= $cliente->id ?>"
                   data-value="<?= h(empty($cliente->particularidades) ? ' ' : $cliente->particularidades); ?>"
                   listen="f"><?= h(empty($cliente->particularidades) ? 'Adicionar' : $cliente->particularidades); ?></a>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-lg-4">

            <div class="col-md-offset-6">
                <?php if (!empty($agenda)): ?>
                    <div class="col-md-1">
                        <?= $this->Html->link('<i class="fa fa-check-square-o"></i>', '#Javascript:void(0)', ['onclick' => 'FinalizarAgenda(' . $agenda . ',\'n\')', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Finalizar Atendimento', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                    </div>
                <?php endif; ?>
                <div class="col-md-1">
                    <?= $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'Clientes', 'action' => 'report', $cliente->id], ['target' => '_blank', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir Prontuário', 'escape' => false, 'class' => 'btn btn-xs btn-success', 'listen' => 'f']) ?>
                </div>
                <div class="col-md-1">
                    <?= $this->Html->link('<i class="fa fa-print"></i>', ['controller' => 'Clientes', 'action' => 'reportCadastro', $cliente->id], ['target' => '_blank', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Imprimir Ficha Cadastral', 'escape' => false, 'class' => 'btn btn-xs btn-success', 'listen' => 'f']) ?>
                </div>
            </div>

            <div class="col-md-12">
                <h2>&nbsp;</h2>
                <?php if (!empty($cliente->indicacao_id)): ?>
                    <p>Indicação: <?= $cliente->indicacao->nome; ?></p>
                <?php endif; ?>
                <?php if (!empty($cliente->nome_pai)): ?>
                    <p>Pai: <?= $cliente->nome_pai ?> - Idade: <?= $cliente->pai_idade ?> <br/>
                        Profissão: <?= $cliente->pai_profissao ?> </p>
                <?php endif; ?>
                <?php if (!empty($cliente->nome_mae)): ?>
                    <p>Mãe: <?= $cliente->nome_mae ?> - Idade: <?= $cliente->mae_idade ?><br/>
                        Profissão: <?= $cliente->mae_profissao ?> </p>
                <?php endif; ?>
                <?php if ($this->Configuracao->mostraReligiaoOnClientes() == 2 || $this->Configuracao->mostraReligiaoOnClientes() == 3): ?>
                <p>Religião: <?= $cliente->religiao->descricao ?><br/>
                    <?php endif; ?>
            </div>
        </div>
        <div class="col-lg-2 col-xs-4 col-sm-4 text-center" style="margin-top: 15px;">
            <?php if (empty($cliente->caminho)) {
                echo $this->Html->image('/img/content/img/empty-full-avatar.jpg', ['title' => 'Cliente sem foto', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'style' => 'cursor:pointer', 'class' => 'img-thumbnail img-responsive']);
            } else {
                echo $this->Html->image('/files/clientes/caminho/' . $cliente->caminho_dir . '/' . $cliente->caminho, ['class' => 'img-thumbnail img-responsive', 'title' => $cliente->nome, 'toggle' => 'tooltip', 'data-placement' => 'bottom']);
            } ?>
            <div class="file-name">
                <small><?= $this->Html->icon('time') ?> Última atualização</small>
                <br/>
                <small><?= $cliente->modified ?> </small>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#aba-historicos" aria-controls="aba-historicos" role="tab" data-toggle="tab"
                               controller="ClienteHistoricos" action="all"
                               data-cliente="<?= $cliente->id ?>">Prontuário</a>
                        </li>

                        <li role="presentation">
                            <a href="#aba-documentos" aria-controls="aba-documentos" role="tab" data-toggle="tab"
                               controller="ClienteDocumentos" action="all" data-cliente="<?= $cliente->id ?>">Documentos
                                <span class="badge badge-warning badge-g2i"><?= $countClienteDocumentos ?></span></a>
                        </li>
                        <li role="presentation">
                            <a href="#aba-atendimentos" aria-controls="aba-atendimentos" role="tab" data-toggle="tab"
                               controller="AtendimentoProcedimentos" action="all" data-cliente="<?= $cliente->id ?>">Atendimentos</a>
                        </li>
                        <li role="presentation">
                            <a href="#aba-anexos" aria-controls="aba-anexos" role="tab" data-toggle="tab"
                               controller="ClienteAnexos" action="all" load-script="ClienteAnexos"
                               data-cliente="<?= $cliente->id ?>">Anexos <span
                                        class="badge badge-info badge-g2i"><?= $countClienteAnexos ?></span></a>
                        </li>

                        <li role="presentation">
                            <a href="#aba-ligacoes" aria-controls="aba-ligacoes" role="tab" data-toggle="tab"
                               controller="ClienteLigacoes" action="all" data-cliente="<?= $cliente->id ?>">Vinculos</a>
                        </li>

                        <li role="presentation">
                            <a href="#aba-agendas" aria-controls="aba-agendas" role="tab" data-toggle="tab"
                               controller="Agendas" action="all" data-cliente="<?= $cliente->id ?>">Agenda</a>
                        </li>

                        <li role="presentation">
                            <a href="Javascript:void(0)" listen="f" id="abrir_cadastro" data-id="<?= $cliente->id ?>">Cadastro</a>
                        </li>
                        <?php if ($configurar->imagem): ?>
                            <li role="presentation">
                                <a href="<?= $configurar->url_imagens ?>" listen="f" target="_blank">Imagens</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="aba-historicos"></div>
                        <div role="tabpanel" class="tab-pane" id="aba-documentos"></div>
                        <div role="tabpanel" class="tab-pane" id="aba-anexos"></div>
                        <div role="tabpanel" class="tab-pane" id="aba-ligacoes"></div>
                        <div role="tabpanel" class="tab-pane" id="aba-agendas"></div>
                        <div role="tabpanel" class="tab-pane" id="aba-financeiro"></div>
                        <div role="tabpanel" class="tab-pane" id="aba-atendimentos"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var filtro = {
        cliente_id: '<?=$cliente->id?>',
        ajax: 1
    }
    $(function () {
        carregarAba("aba-historicos", "ClienteHistoricos", filtro);
        $("[role='tab']").click(function () {
            carregarAba($(this).attr('aria-controls'), $(this).attr('controller'), filtro, $(this).attr('action'), $(this).attr('load-script'));
        });
        $('body').on('click', ".area_pront a", function () {
            if (empty($(this).attr('listen'))) {
                $("div#" + $(".area_pront").parent('.active').attr('id')).loadGrid(this.href, null, ".area_pront", loadComponentes);
                return false;
            }
        });
    })
</script>