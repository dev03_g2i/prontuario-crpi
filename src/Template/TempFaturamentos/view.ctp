
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Temp Faturamentos</h2>
        <ol class="breadcrumb">
            <li>Temp Faturamentos</li>
            <li class="active">
                <strong>Litagem de Temp Faturamentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Temp Faturamentos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Atendimento') ?></th>
                                                                <td><?= $tempFaturamento->has('atendimento') ? $this->Html->link($tempFaturamento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $tempFaturamento->atendimento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Convenio') ?></th>
                                <td><?= h($tempFaturamento->convenio) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Profissional') ?></th>
                                <td><?= h($tempFaturamento->profissional) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Paciente') ?></th>
                                <td><?= h($tempFaturamento->paciente) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Matricula') ?></th>
                                <td><?= h($tempFaturamento->matricula) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Codigo Tabela') ?></th>
                                <td><?= h($tempFaturamento->codigo_tabela) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Procedimento') ?></th>
                                <td><?= h($tempFaturamento->procedimento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Senha') ?></th>
                                <td><?= h($tempFaturamento->senha) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $tempFaturamento->has('user') ? $this->Html->link($tempFaturamento->user->nome, ['controller' => 'Users', 'action' => 'view', $tempFaturamento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($tempFaturamento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Selecionado') ?></th>
                                <td><?= $this->Number->format($tempFaturamento->selecionado) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($tempFaturamento->quantidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Fatura') ?></th>
                                <td><?= $this->Number->format($tempFaturamento->valor_fatura) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Material') ?></th>
                                <td><?= $this->Number->format($tempFaturamento->valor_material) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Total') ?></th>
                                <td><?= $this->Number->format($tempFaturamento->total) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Atendimento') ?></th>
                                                                <td><?= h($tempFaturamento->data_atendimento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Hora') ?></th>
                                                                <td><?= h($tempFaturamento->hora) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($tempFaturamento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($tempFaturamento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Guia') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($tempFaturamento->guia)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


