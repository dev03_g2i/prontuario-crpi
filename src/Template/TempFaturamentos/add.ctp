<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Temp Faturamentos</h2>
        <ol class="breadcrumb">
            <li>Temp Faturamentos</li>
            <li class="active">
                <strong>                    Cadastrar Temp Faturamentos
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="tempFaturamentos form">
                        <?= $this->Form->create($tempFaturamento) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Temp Faturamento') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('selecionado');
                                echo "</div>";
echo "<div class='col-md-6'>";
    echo $this->Form->input('atendimento_id', ['data'=>'select','controller'=>'atendimentos','action'=>'fill',  'empty' => 'selecione']);
echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('convenio');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('profissional');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('data_atendimento', ['empty' => true,'type'=>'text','class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('hora');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('paciente');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('matricula');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('codigo_tabela');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('procedimento');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('guia');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('senha');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('quantidade');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('valor_fatura',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('valor_material',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('total',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

