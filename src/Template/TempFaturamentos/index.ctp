<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Temp Faturamentos</h2>
        <ol class="breadcrumb">
            <li>Temp Faturamentos</li>
            <li class="active">
                <strong>Litagem de Temp Faturamentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('TempFaturamento',['type'=>'get']) ?>
                        <?php
                                                    echo "<div class='col-md-4'>";
                                echo $this->Form->input('selecionado',['name'=>'TempFaturamentos__selecionado']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('atendimento_id', ['name'=>'TempFaturamentos__atendimento_id','data'=>'select','controller'=>'atendimentos','action'=>'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('convenio',['name'=>'TempFaturamentos__convenio']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('profissional',['name'=>'TempFaturamentos__profissional']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('data_atendimento', ['name'=>'TempFaturamentos__data_atendimento','empty' => true,'type'=>'text','class'=>'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('hora',['name'=>'TempFaturamentos__hora']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('paciente',['name'=>'TempFaturamentos__paciente']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('matricula',['name'=>'TempFaturamentos__matricula']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('codigo_tabela',['name'=>'TempFaturamentos__codigo_tabela']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('procedimento',['name'=>'TempFaturamentos__procedimento']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('senha',['name'=>'TempFaturamentos__senha']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('quantidade',['name'=>'TempFaturamentos__quantidade']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('valor_fatura',['name'=>'TempFaturamentos__valor_fatura','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('valor_material',['name'=>'TempFaturamentos__valor_material','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                            echo "<div class='col-md-4'>";
                                echo $this->Form->input('total',['name'=>'TempFaturamentos__total','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                        ?>
                        <div class="clearfix"></div>
                        <div class="col-md-4">
                            <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Temp Faturamentos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Temp Faturamentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Temp Faturamentos') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('selecionado') ?></th>
                <th><?= $this->Paginator->sort('atendimento_id') ?></th>
                <th><?= $this->Paginator->sort('convenio') ?></th>
                <th><?= $this->Paginator->sort('profissional') ?></th>
                <th><?= $this->Paginator->sort('data_atendimento') ?></th>
                <th><?= $this->Paginator->sort('hora') ?></th>
                <th><?= $this->Paginator->sort('paciente') ?></th>
                <th><?= $this->Paginator->sort('matricula') ?></th>
                <th><?= $this->Paginator->sort('codigo_tabela') ?></th>
                <th><?= $this->Paginator->sort('procedimento') ?></th>
                <th><?= $this->Paginator->sort('senha') ?></th>
                <th><?= $this->Paginator->sort('quantidade') ?></th>
                <th><?= $this->Paginator->sort('valor_fatura') ?></th>
                <th><?= $this->Paginator->sort('valor_material') ?></th>
                <th><?= $this->Paginator->sort('total') ?></th>
                <th><?= $this->Paginator->sort('user_id',['label'=>'Quem Cadastrou']) ?></th>
                <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tempFaturamentos as $tempFaturamento): ?>
            <tr>
                <td><?= $this->Number->format($tempFaturamento->id) ?></td>
                <td><?= $this->Number->format($tempFaturamento->selecionado) ?></td>
                <td><?= $tempFaturamento->has('atendimento') ? $this->Html->link($tempFaturamento->atendimento->id, ['controller' => 'Atendimentos', 'action' => 'view', $tempFaturamento->atendimento->id]) : '' ?></td>
                <td><?= h($tempFaturamento->convenio) ?></td>
                <td><?= h($tempFaturamento->profissional) ?></td>
                <td><?= h($tempFaturamento->data_atendimento) ?></td>
                <td><?= h($tempFaturamento->hora) ?></td>
                <td><?= h($tempFaturamento->paciente) ?></td>
                <td><?= h($tempFaturamento->matricula) ?></td>
                <td><?= h($tempFaturamento->codigo_tabela) ?></td>
                <td><?= h($tempFaturamento->procedimento) ?></td>
                <td><?= h($tempFaturamento->senha) ?></td>
                <td><?= $this->Number->format($tempFaturamento->quantidade) ?></td>
                <td><?= $this->Number->format($tempFaturamento->valor_fatura) ?></td>
                <td><?= $this->Number->format($tempFaturamento->valor_material) ?></td>
                <td><?= $this->Number->format($tempFaturamento->total) ?></td>
                <td><?= $tempFaturamento->has('user') ? $this->Html->link($tempFaturamento->user->nome, ['controller' => 'Users', 'action' => 'view', $tempFaturamento->user->id]) : '' ?></td>
                <td><?= h($tempFaturamento->created) ?></td>
                <td><?= h($tempFaturamento->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'tempFaturamentos','action' => 'view', $tempFaturamento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'tempFaturamentos','action' => 'edit', $tempFaturamento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'tempFaturamentos','action'=>'delete', $tempFaturamento->id],['onclick'=>'Deletar(\'tempFaturamentos\','.$tempFaturamento->id.',recarregar)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
