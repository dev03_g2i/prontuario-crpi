<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Anotacão Agendas - Visualizar</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Anotacao Agendas</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($anotacaoAgenda->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Descricao') ?>
                                    </th>
                                    <td>
                                        <?= h($anotacaoAgenda->descricao) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Grupo Agenda') ?>
                                    </th>
                                    <td>
                                        <?= $anotacaoAgenda->has('grupo_agenda') ? $this->Html->link($anotacaoAgenda->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $anotacaoAgenda->grupo_agenda->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situação Cadastro') ?>
                                    </th>
                                    <td>
                                        <?= $anotacaoAgenda->has('situacao_cadastro') ? $this->Html->link($anotacaoAgenda->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $anotacaoAgenda->situacao_cadastro->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                    <?= $anotacaoAgenda->has('user_reg') ? $this->Html->link($anotacaoAgenda->user_reg->nome, ['controller' => 'Users', 'action' => 'view', $anotacaoAgenda->user_reg->id]).' - '.$anotacaoAgenda->created : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('User Update') ?>
                                    </th>
                                    <td>
                                    <?= $anotacaoAgenda->has('user_alt') ? $this->Html->link($anotacaoAgenda->user_alt->nome, ['controller' => 'Users', 'action' => 'view', $anotacaoAgenda->user_alt->id]).' - '.$anotacaoAgenda->modified : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Data') ?>
                                    </th>
                                    <td>
                                        <?= h($anotacaoAgenda->data) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($anotacaoAgenda->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($anotacaoAgenda->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>