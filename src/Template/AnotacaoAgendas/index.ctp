<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Anotacão Agenda</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                <div class="ibox-title">
                        <div class="text-right">
                            <p>
                                <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Agenda Filaespera','class'=>'btn btn-primary','escape' => false]) ?>
                            </p>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('descricao') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('data') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('grupo_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('situacao_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_id',['label'=>'Cadastrado por']) ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('user_update', ['label' => 'Alterado por']) ?>
                                            </th>
                                            <th class="actions">
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($anotacaoAgendas as $anotacaoAgenda): ?>
                                        <tr class="delete<?=$anotacaoAgenda->id?>">
                                            <td>
                                                <?= h($anotacaoAgenda->descricao) ?>
                                            </td>
                                            <td>
                                                <?= h($anotacaoAgenda->data) ?>
                                            </td>
                                            <td>
                                                <?= $anotacaoAgenda->has('grupo_agenda') ? $this->Html->link($anotacaoAgenda->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $anotacaoAgenda->grupo_agenda->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $anotacaoAgenda->has('situacao_cadastro') ? $this->Html->link($anotacaoAgenda->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $anotacaoAgenda->situacao_cadastro->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $anotacaoAgenda->has('user_reg') ? $this->Html->link($anotacaoAgenda->user_reg->nome, ['controller' => 'Users', 'action' => 'view', $anotacaoAgenda->user_reg->id]).' - '.$anotacaoAgenda->created : '' ?>
                                            </td>
                                            <td>
                                                <?= $anotacaoAgenda->has('user_alt') ? $this->Html->link($anotacaoAgenda->user_alt->nome, ['controller' => 'Users', 'action' => 'view', $anotacaoAgenda->user_alt->id]).' - '.$anotacaoAgenda->modified : '' ?>
                                            </td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'anotacaoAgendas','action' => 'view', $anotacaoAgenda->id],['style' => 'padding-top: 4px;padding-bottom: 4px;', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'anotacaoAgendas','action' => 'edit', $anotacaoAgenda->id],['style' => 'padding-top: 4px;padding-bottom: 4px;', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Form->button($this->Html->icon('remove'), ['onclick'=>'DeletarModal(\'AnotacaoAgendas\','.$anotacaoAgenda->id.')','style' => 'padding-top: 4px;padding-bottom: 4px;', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>