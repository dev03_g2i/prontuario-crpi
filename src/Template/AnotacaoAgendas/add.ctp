<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Anotacão Agendas - Cadastrar</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="anotacaoAgendas form">
                            <?= $this->Form->create($anotacaoAgenda) ?>
                            <?php require_once('form.ctp'); ?>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>