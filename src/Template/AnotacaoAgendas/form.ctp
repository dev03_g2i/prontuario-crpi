<div class='col-md-6'>
    <?= $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => @date_format($anotacaoAgenda->data, 'd/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
</div>
<div class='col-md-6'>
    <?=$this->Form->input('grupo_id', ['required' => true, 'options' => $grupos, 'empty' => 'Selecione']); ?>
</div>
<div class='col-md-12'>
    <?=$this->Form->input('descricao', ['type' => 'textarea', 'rows' => 2]); ?>
</div>
<div class="col-md-12 text-right">
    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
</div>