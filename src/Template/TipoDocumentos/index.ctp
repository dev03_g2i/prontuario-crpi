<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Documentos</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right btnAdd">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').'Novo Documento', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Tipo Documentos','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                                    <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($tipoDocumentos as $tipoDocumento): ?>
                                    <tr>
                                        <td><?= $this->Number->format($tipoDocumento->id) ?></td>
                                        <td><?= h($tipoDocumento->nome) ?></td>
                                        <td><?= $tipoDocumento->has('situacao_cadastro') ? $this->Html->link($tipoDocumento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoDocumento->situacao_cadastro->id]) : '' ?></td>
                                        <td><?= $tipoDocumento->has('user') ? $this->Html->link($tipoDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoDocumento->user->id]) : '' ?></td>
                                        <td><?= h($tipoDocumento->created) ?></td>
                                        <td><?= h($tipoDocumento->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-tags"></i>', ['controller'=>'Modelos','action' => 'index','?'=>['tipo_documento'=>$tipoDocumento->id,'first'=>1]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Modelos','escape' => false,'class'=>'btn btn-xs btn-info','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $tipoDocumento->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                            <?= $this->Html->link($this->Html->icon('remove'),  ['action'=>'delete', $tipoDocumento->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

