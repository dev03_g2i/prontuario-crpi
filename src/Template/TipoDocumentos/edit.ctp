<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Documentos -Editar</h2>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="tipoDocumentos form">
                        <?= $this->Form->create($tipoDocumento) ?>
                        <fieldset>
                            <legend><?= __('Editar Tipo Documento') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            ?>
                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

