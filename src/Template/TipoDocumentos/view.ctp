<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Documentos - Visualizar</h2>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="tipoDocumentos">
                        <h3><?= h($tipoDocumento->nome) ?></h3>
                        <table class="vertical-table">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($tipoDocumento->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Cadastro') ?></th>
                                <td><?= $tipoDocumento->has('situacao_cadastro') ? $this->Html->link($tipoDocumento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoDocumento->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User') ?></th>
                                <td><?= $tipoDocumento->has('user') ? $this->Html->link($tipoDocumento->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoDocumento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($tipoDocumento->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Created') ?></th>
                                <td><?= h($tipoDocumento->created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Modified') ?></th>
                                <td><?= h($tipoDocumento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

