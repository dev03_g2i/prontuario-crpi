

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grau Parentescos</h2>
        <ol class="breadcrumb">
            <li>Grau Parentescos</li>
            <li class="active">
                <strong>Listagem de Grau Parentescos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="grauParentescos">
    <h3><?= h($grauParentesco->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($grauParentesco->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $grauParentesco->has('situacao_cadastro') ? $this->Html->link($grauParentesco->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grauParentesco->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $grauParentesco->has('user') ? $this->Html->link($grauParentesco->user->nome, ['controller' => 'Users', 'action' => 'view', $grauParentesco->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($grauParentesco->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($grauParentesco->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($grauParentesco->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

