<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Convenio Tipoimposto</h2>
            <ol class="breadcrumb">
                <li>Convenio Tipoimposto</li>
                <li class="active">
                    <strong>
                                                Editar Convenio Tipoimposto
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="convenioTipoimposto form">
                            <?= $this->Form->create($convenioTipoimposto) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Convenio Tipoimposto') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_inicial',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_final',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('porcentual',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('retido'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('observacao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_created'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('user_updated'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

