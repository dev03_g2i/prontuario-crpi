<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Impostos</h2>
            <ol class="breadcrumb">
                <li>Parametrização de impostos</li>
                <li class="active">
                    <strong>Listagem de impostos</strong>
                </li>
            </ol>
            <div>
            <strong>Convênio: <?= $nome?></strong>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('ConvenioTipoimposto',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('descricao',['name'=>'ConvenioTipoimposto__descricao']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('valor_inicial',['name'=>'ConvenioTipoimposto__valor_inicial','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                                        </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('valor_final',['name'=>'ConvenioTipoimposto__valor_final','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                                        </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('porcentual',['name'=>'ConvenioTipoimposto__porcentual','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                                        </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('retido',['name'=>'ConvenioTipoimposto__retido']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('user_created',['name'=>'ConvenioTipoimposto__user_created']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('user_updated',['name'=>'ConvenioTipoimposto__user_updated']); ?>
                                                                                    </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Imposto', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Convenio Tipoimposto','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Impostos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?= $this->Paginator->sort('descricao') ?></th>
                                            <th><?= $this->Paginator->sort('valor_inicial') ?></th>
                                            <th><?= $this->Paginator->sort('valor_final') ?></th>
                                            <th><?= $this->Paginator->sort('porcentual') ?></th>
                                            <th><?= $this->Paginator->sort('retido') ?></th>
                                            <th><?= $this->Paginator->sort('user_created') ?></th>
                                            <th><?= $this->Paginator->sort('user_updated') ?></th>
                                            <th><?= $this->Paginator->sort('created',['label'=>'Dt. Criação']) ?></th>
                                            <th><?= $this->Paginator->sort('modified',['label'=>'Dt. Modificação']) ?></th>
                                            <th class="actions"><?= __('Ações') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($convenioTipoimposto as $convenioTipoimposto): ?>
                                            <tr>
                                                <td><?= h($convenioTipoimposto->descricao) ?></td>
                                                <td><?= $this->Number->format($convenioTipoimposto->valor_inicial) ?></td>
                                                <td><?= $this->Number->format($convenioTipoimposto->valor_final) ?></td>
                                                <td><?= $this->Number->format($convenioTipoimposto->porcentual) ?></td>
                                                <td><?= $this->Number->format($convenioTipoimposto->retido) ?></td>
                                                <td><?= $this->Number->format($convenioTipoimposto->user_created) ?></td>
                                                <td><?= $this->Number->format($convenioTipoimposto->user_updated) ?></td>
                                                <td><?= h($convenioTipoimposto->created) ?></td>
                                                <td><?= h($convenioTipoimposto->modified) ?></td>
                                                <td class="actions">
                                                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'convenioTipoimposto','action' => 'view', $convenioTipoimposto->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'convenioTipoimposto','action' => 'edit', $convenioTipoimposto->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                    <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'convenioTipoimposto','action'=>'delete', $convenioTipoimposto->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
