
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Convenio Tipoimposto</h2>
        <ol class="breadcrumb">
            <li>Convenio Tipoimposto</li>
            <li class="active">
                <strong>Litagem de Convenio Tipoimposto</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Convenio Tipoimposto</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($convenioTipoimposto->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Inicial') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->valor_inicial) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Final') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->valor_final) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Porcentual') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->porcentual) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Retido') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->retido) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Created') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->user_created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User Updated') ?></th>
                                <td><?= $this->Number->format($convenioTipoimposto->user_updated) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($convenioTipoimposto->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($convenioTipoimposto->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($convenioTipoimposto->observacao)); ?>
                </div>
            </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Convenios') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($convenioTipoimposto->convenios)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Nome') ?></th>
                        <th><?= __('Tipo Id') ?></th>
                        <th><?= __('Situacao Id') ?></th>
                        <th><?= __('User Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th><?= __('Usa Tiss') ?></th>
                        <th><?= __('Usa Cobranca') ?></th>
                        <th><?= __('Obrigatorio Nr Guia') ?></th>
                        <th><?= __('Obrigatorio Autorizacao Senha') ?></th>
                        <th><?= __('Obrigatorio Carteira Paciente') ?></th>
                        <th><?= __('Usa Valor Original') ?></th>
                        <th><?= __('Tiss Registroans') ?></th>
                        <th><?= __('Tiss Codcliconvenio') ?></th>
                        <th><?= __('Tiss Tipocodigo') ?></th>
                        <th><?= __('Tiss Codtabpreco') ?></th>
                        <th><?= __('Tiss Codtabmedicamento') ?></th>
                        <th><?= __('Tiss Codtabmaterial') ?></th>
                        <th><?= __('Tiss Codtaxas') ?></th>
                        <th><?= __('Bank Id') ?></th>
                        <th><?= __('Pymt Date') ?></th>
                        <th><?= __('Pymt Hist') ?></th>
                        <th><?= __('Tel') ?></th>
                        <th><?= __('Contact') ?></th>
                        <th><?= __('PlanAcc Id') ?></th>
                        <th><?= __('Convenio Tipoimposto Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($convenioTipoimposto->convenios as $convenios): ?>
                <tr>
                    <td><?= h($convenios->id) ?></td>
                    <td><?= h($convenios->nome) ?></td>
                    <td><?= h($convenios->tipo_id) ?></td>
                    <td><?= h($convenios->situacao_id) ?></td>
                    <td><?= h($convenios->user_id) ?></td>
                    <td><?= h($convenios->created) ?></td>
                    <td><?= h($convenios->modified) ?></td>
                    <td><?= h($convenios->usa_tiss) ?></td>
                    <td><?= h($convenios->usa_cobranca) ?></td>
                    <td><?= h($convenios->obrigatorio_nr_guia) ?></td>
                    <td><?= h($convenios->obrigatorio_autorizacao_senha) ?></td>
                    <td><?= h($convenios->obrigatorio_carteira_paciente) ?></td>
                    <td><?= h($convenios->usa_valor_original) ?></td>
                    <td><?= h($convenios->tiss_registroans) ?></td>
                    <td><?= h($convenios->tiss_codcliconvenio) ?></td>
                    <td><?= h($convenios->tiss_tipocodigo) ?></td>
                    <td><?= h($convenios->tiss_codtabpreco) ?></td>
                    <td><?= h($convenios->tiss_codtabmedicamento) ?></td>
                    <td><?= h($convenios->tiss_codtabmaterial) ?></td>
                    <td><?= h($convenios->tiss_codtaxas) ?></td>
                    <td><?= h($convenios->bank_id) ?></td>
                    <td><?= h($convenios->pymt_date) ?></td>
                    <td><?= h($convenios->pymt_hist) ?></td>
                    <td><?= h($convenios->tel) ?></td>
                    <td><?= h($convenios->contact) ?></td>
                    <td><?= h($convenios->planAcc_id) ?></td>
                    <td><?= h($convenios->convenio_tipoimposto_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Convenios','action' => 'view', $convenios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Convenios','action' => 'edit', $convenios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Convenios','action' => 'delete', $convenios->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $convenioTipoimposto->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


