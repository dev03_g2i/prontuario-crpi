
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Contaspagar</h2>
        <ol class="breadcrumb">
            <li>Contaspagar</li>
            <li class="active">
                <strong>Litagem de Contaspagar</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Contaspagar</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Planoconta') ?></th>
                                                                <td><?= $contaspagar->has('planoconta') ? $this->Html->link($contaspagar->planoconta->nome, ['controller' => 'Planocontas', 'action' => 'view', $contaspagar->planoconta->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fornecedore') ?></th>
                                                                <td><?= $contaspagar->has('fornecedore') ? $this->Html->link($contaspagar->fornecedore->nome, ['controller' => 'Fornecedores', 'action' => 'view', $contaspagar->fornecedore->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Contabilidade') ?></th>
                                                                <td><?= $contaspagar->has('contabilidade') ? $this->Html->link($contaspagar->contabilidade->nome, ['controller' => 'Contabilidades', 'action' => 'view', $contaspagar->contabilidade->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Tipo Pagamento') ?></th>
                                                                <td><?= $contaspagar->has('tipo_pagamento') ? $this->Html->link($contaspagar->tipo_pagamento->descricao, ['controller' => 'TipoPagamento', 'action' => 'view', $contaspagar->tipo_pagamento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Tipo Documento') ?></th>
                                                                <td><?= $contaspagar->has('tipo_documento') ? $this->Html->link($contaspagar->tipo_documento->descricao, ['controller' => 'TipoDocumento', 'action' => 'view', $contaspagar->tipo_documento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero Documento') ?></th>
                                <td><?= h($contaspagar->numero_documento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Parcela') ?></th>
                                <td><?= h($contaspagar->parcela) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Contaspagar Planejamento') ?></th>
                                                                <td><?= $contaspagar->has('contaspagar_planejamento') ? $this->Html->link($contaspagar->contaspagar_planejamento->id, ['controller' => 'ContaspagarPlanejamentos', 'action' => 'view', $contaspagar->contaspagar_planejamento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($contaspagar->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor') ?></th>
                                <td><?= $this->Number->format($contaspagar->valor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Juros') ?></th>
                                <td><?= $this->Number->format($contaspagar->juros) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Multa') ?></th>
                                <td><?= $this->Number->format($contaspagar->multa) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao') ?></th>
                                <td><?= $this->Number->format($contaspagar->situacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Bruto') ?></th>
                                <td><?= $this->Number->format($contaspagar->valor_bruto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Desconto') ?></th>
                                <td><?= $this->Number->format($contaspagar->desconto) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($contaspagar->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Vencimento') ?></th>
                                                                <td><?= h($contaspagar->vencimento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Pagamento') ?></th>
                                                                <td><?= h($contaspagar->data_pagamento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Cadastro') ?></th>
                                                                <td><?= h($contaspagar->data_cadastro) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Complemento') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($contaspagar->complemento)); ?>
                </div>
            </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($contaspagar->movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Planoconta Id') ?></th>
                        <th><?= __('Fornecedor Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Documento') ?></th>
                        <th><?= __('Credito') ?></th>
                        <th><?= __('Debito') ?></th>
                        <th><?= __('Banco Id') ?></th>
                        <th><?= __('Status Lancamento') ?></th>
                        <th><?= __('Categoria') ?></th>
                        <th><?= __('IdCliente') ?></th>
                        <th><?= __('Contaspagar Id') ?></th>
                        <th><?= __('Contabilidade Id') ?></th>
                        <th><?= __('Data Criacao') ?></th>
                        <th><?= __('Data Alteracao') ?></th>
                        <th><?= __('Criado Por') ?></th>
                        <th><?= __('Atualizado Por') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($contaspagar->movimentos as $movimentos): ?>
                <tr>
                    <td><?= h($movimentos->id) ?></td>
                    <td><?= h($movimentos->situacao) ?></td>
                    <td><?= h($movimentos->data) ?></td>
                    <td><?= h($movimentos->planoconta_id) ?></td>
                    <td><?= h($movimentos->fornecedor_id) ?></td>
                    <td><?= h($movimentos->complemento) ?></td>
                    <td><?= h($movimentos->documento) ?></td>
                    <td><?= h($movimentos->credito) ?></td>
                    <td><?= h($movimentos->debito) ?></td>
                    <td><?= h($movimentos->banco_id) ?></td>
                    <td><?= h($movimentos->status_lancamento) ?></td>
                    <td><?= h($movimentos->categoria) ?></td>
                    <td><?= h($movimentos->idCliente) ?></td>
                    <td><?= h($movimentos->contaspagar_id) ?></td>
                    <td><?= h($movimentos->contabilidade_id) ?></td>
                    <td><?= h($movimentos->data_criacao) ?></td>
                    <td><?= h($movimentos->data_alteracao) ?></td>
                    <td><?= h($movimentos->criado_por) ?></td>
                    <td><?= h($movimentos->atualizado_por) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Movimentos','action' => 'view', $movimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Movimentos','action' => 'edit', $movimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Movimentos','action' => 'delete', $movimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $contaspagar->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


