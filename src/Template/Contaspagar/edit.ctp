<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contaspagar</h2>
            <ol class="breadcrumb">
                <li>Contaspagar</li>
                <li class="active">
                    <strong>
                        Editar Contaspagar
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contaspagar form">
                            <?= $this->Form->create($contaspagar) ?>
                            <fieldset>
                                <legend><?= __('Editar Contaspagar') ?></legend>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contaspagar->data, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('vencimento', ['type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contaspagar->vencimento, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('valor'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('juros'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('multa'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data_pagamento', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contaspagar->data_pagamento, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('planoconta_id', ['data' => 'select', 'controller' => 'planocontas', 'action' => 'fill', 'data-value' => $contaspagar->planoconta_id, 'empty' => 'selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('fornecedor_id', ['data' => 'select', 'controller' => 'fornecedores', 'action' => 'fill', 'data-value' => $contaspagar->fornecedor_id, 'empty' => 'selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('situacao'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('complemento'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('contabilidade_id', ['data' => 'select', 'controller' => 'contabilidades', 'action' => 'fill', 'data-value' => $contaspagar->contabilidade_id, 'empty' => 'selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('valor_bruto'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('desconto'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('tipo_pagamento_id', ['data' => 'select', 'controller' => 'tipoPagamento', 'action' => 'fill', 'data-value' => $contaspagar->tipo_pagamento_id, 'empty' => 'selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('tipo_documento_id', ['data' => 'select', 'controller' => 'tipoDocumento', 'action' => 'fill', 'data-value' => $contaspagar->tipo_documento_id, 'empty' => 'selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('numero_documento'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?php echo $this->Form->input('data_cadastro', ['type' => 'text', 'class' => 'datetimepicker', 'value' => $this->Time->format($contaspagar->data_cadastro, 'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                    ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('parcela'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('contaspagar_planejamento_id', ['data' => 'select', 'controller' => 'contaspagarPlanejamentos', 'action' => 'fill', 'data-value' => $contaspagar->contaspagar_planejamento_id, 'empty' => 'selecione']); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

