<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agenda Bloqueios</h2>
            <ol class="breadcrumb">
                <li>Agenda Bloqueios</li>
                <li class="active">
                    <strong>Litagem de Agenda Bloqueios</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Agenda Bloqueios', ['action' => 'add', $grupo_agenda_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Agenda Bloqueios','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= __('Agenda Bloqueios') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>
                                                <?= $this->Paginator->sort('id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('inicio') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('fim') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('agenda_grupo_id') ?>
                                            </th>
                                            <th>
                                                <?= $this->Paginator->sort('nome_provisorio') ?>
                                            </th>
                                            <!--<th><?= $this->Paginator->sort('status', 'Mostrar calendario') ?></th>-->
                                            <th class="actions"><?= __('Ações') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($agendaBloqueios as $agendaBloqueio):

                                        $nome_provisorio = null;
                                        foreach ($agendaBloqueio->agendas as $agenda){
                                            $nome_provisorio = $agenda->nome_provisorio;
                                        }
                                        ?>
                                        <tr class="delete<?=$agendaBloqueio->id?>">
                                            <td>
                                                <?= $this->Number->format($agendaBloqueio->id) ?>
                                            </td>
                                            <td>
                                                <?= $this->Time->format($agendaBloqueio->inicio, 'dd/MM/YYYY HH:mm') ?>
                                            </td>
                                            <td>
                                                <?= $agendaBloqueio->fim ?>
                                            </td>
                                            <td>
                                                <?= $agendaBloqueio->has('grupo_agenda') ? $this->Html->link($agendaBloqueio->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agendaBloqueio->grupo_agenda->id]) : '' ?>
                                            </td>
                                            <td>
                                                <?= $nome_provisorio ?>
                                            </td>
                                            <!--<td><?= $this->Form->checkbox('status', ['name' => 'status[]', 'value' => $agendaBloqueio->id,  'checked' => ($agendaBloqueio->status==1)?true:false]) ?></td>
                                            <td>
                                                <a href="javascript:void(0)" class="edit-status-periodos" data-type="select"
                                                    controller="AgendaBloqueios" action="altera_status" data-pk="<?=$agendaBloqueio->id?>"
                                                    data-value="<?=$agendaBloqueio->status?>" data-title="Select status" listen="f">
                                                    <?=($agendaBloqueio->status==1)?'Sim':'Não'?>
                                                </a>
                                            </td>-->
                                            <td class="actions">
                                            <?= $this->Form->button($this->Html->icon('remove'), ['onclick'=>'DeletarModal(\'agendaBloqueios\','.$agendaBloqueio->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                            <!--<?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'agendaBloqueios','action' => 'view', $agendaBloqueio->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'agendaBloqueios','action' => 'edit', $agendaBloqueio->id, 'grupo_agenda_id' => $grupo_agenda_id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>-->
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                                <?= $this->Paginator->numbers() ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p>
                                            <?= $this->Paginator->counter() ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>