
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Bloqueios</h2>
        <ol class="breadcrumb">
            <li>Agenda Bloqueios</li>
            <li class="active">
                <strong>Litagem de Agenda Bloqueios</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Agenda Bloqueios</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Grupo Agenda') ?></th>
                                                                <td><?= $agendaBloqueio->has('grupo_agenda') ? $this->Html->link($agendaBloqueio->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agendaBloqueio->grupo_agenda->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $agendaBloqueio->has('situacao_cadastro') ? $this->Html->link($agendaBloqueio->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $agendaBloqueio->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $agendaBloqueio->has('user') ? $this->Html->link($agendaBloqueio->user->nome, ['controller' => 'Users', 'action' => 'view', $agendaBloqueio->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($agendaBloqueio->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Inicio') ?></th>
                                                                <td><?= h($agendaBloqueio->data_inicio) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Final') ?></th>
                                                                <td><?= h($agendaBloqueio->data_final) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Hora Inicio') ?></th>
                                                                <td><?= h($agendaBloqueio->hora_inicio) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Hora Final') ?></th>
                                                                <td><?= h($agendaBloqueio->hora_final) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($agendaBloqueio->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($agendaBloqueio->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


