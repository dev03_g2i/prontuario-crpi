<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Bloqueios</h2>
        <ol class="breadcrumb">
            <li>Agenda Bloqueios</li>
            <li class="active">
                <strong>                    Editar Agenda Bloqueios
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendaBloqueios form">
                        <?= $this->Form->create($agendaBloqueio) ?>
                        <fieldset>
                            <legend><?= __('Editar Agenda Bloqueio') ?></legend>
                            <?php
                               /* echo "<div class='col-md-6'>";
                                    echo $this->Form->input('data_inicio', ['type' => 'text', 'class' => 'datepicker','value'=>$this->Time->format($agendaBloqueio->data_inicio,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('data_final', ['type' => 'text', 'class' => 'datepicker','value'=>$this->Time->format($agendaBloqueio->data_final,'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('hora_inicio');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('hora_final');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('agenda_grupo_id', ['data'=>'select','controller'=>'grupoAgendas','action'=>'fill','data-value'=>$agendaBloqueio->agenda_grupo_id]);
                                echo "</div>";*/
                            ?>
                            <div class="col-md-6">
                                <?php echo $this->Form->input('status', ['name' => 'status', 'type' => 'select', 'options' => [1 => 'Mostrar Calendario', 2 => 'Ocultar Calendario']]) ?>
                            </div>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

