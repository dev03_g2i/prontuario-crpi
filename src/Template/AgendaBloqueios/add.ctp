<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Bloqueios</h2>
        <ol class="breadcrumb">
            <li>Agenda Bloqueios</li>
            <li class="active">
                <strong>                    Cadastrar Agenda Bloqueios
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendaBloqueios form">
                        <?= $this->Form->create($agendaBloqueio, ['id' => 'frm-agendabloqueio', 'class' => 'validateDateInicioFim']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Agenda Bloqueio') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('inicio', ['type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('fim', ['type' => 'text', 'class' => 'datetimepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('nome_provisorio', ['required' => true]);
                                echo "</div>";
                                /*echo "<div class='col-md-6'>";
                                        echo $this->Form->input('hora_inicio', ['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])],'value'=>'07:00']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('hora_final', ['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])],'value'=>'19:00']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('agenda_grupo_id', ['data'=>'select','controller'=>'grupoAgendas','action'=>'fill']);
                                echo "</div>";*/
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['type' => 'submit', 'class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

