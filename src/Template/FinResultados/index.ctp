<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Resultados</h2>
        <ol class="breadcrumb">
            <li>Resultados</li>
            <li class="active">
                <strong>Listagem de Resultados</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Resultados', ['type' => 'get']) ?>
                    <div class="col-md-3">
                        <?= $this->Form->input('ano', ['name' => 'ano']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('tipo', ['type' => 'select', 'empty' => 'Selecione', 'label' => 'Tipo', 'options' => [1 => 'Entrada', 2 => 'Saída']]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('plano_conta', ['type' => 'select', 'empty' => 'Selecione', 'label' => 'Plano de Contas', 'options' => $planoContas]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('classificacao_conta', ['type' => 'select', 'empty' => 'Selecione', 'label' => 'Classificação Contas', 'options' => $classificacaoContas]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('relatorio', ['type' => 'select', 'empty' => 'Selecione', 'label' => 'Tipo de relatório', 'options' => [1 => 'Sintético', 2 => 'Analítico']]); ?>
                    </div>
                    <div class="col-md-12" style="margin-top: 22px">
                        <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                    </div>
                    <?= $this->Form->end() ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <?php $options = ['locale' => 'pt_BR', 'places' => 2]; ?>
            <div class="table-responsive">
            <?php if(isset($tabelaHistoricos)): ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2><?= __('Resultados Históricos') ?></h2>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div>
                                <table class="table table-hover">
                                    <thead class="alert-success">
                                    <tr>
                                        <th>Entrada</th>
                                        <th class="text-right">Jan</th>
                                        <th class="text-right">Fev</th>
                                        <th class="text-right">Mar</th>
                                        <th class="text-right">Abr</th>
                                        <th class="text-right">Mai</th>
                                        <th class="text-right">Jun</th>
                                        <th class="text-right">Jul</th>
                                        <th class="text-right">Ago</th>
                                        <th class="text-right">Set</th>
                                        <th class="text-right">Out</th>
                                        <th class="text-right">Nov</th>
                                        <th class="text-right">Dez</th>
                                        <th class="text-right">Total</th>
                                        <th class="text-right">% Entrada</th>
                                        <th class="text-right">% Saida</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $somaEntrada = 0; ?>
                                    <?php foreach ($tabelaHistoricos as $th): //Histórico Fechado ?>
                                        <?php
                                        $th['porc_entrada'] = $valoresTotaisEntradaSaida['entrada'] != 0 ? $th['total'] / $valoresTotaisEntradaSaida['entrada'] * 100 : 0;
                                        ?>
                                        <?php if ($th['tipo'] == 1): //Entradas?>
                                            <?php $somaEntrada += $th['porc_entrada']; ?>
                                            <tr>
                                                <td><?php echo !empty($th['nome']) ? $th['nome'] : 'Não Informado' ?></td>
                                                <td class="text-right"><?php echo !empty($th['janeiro']) ? $this->Number->format($th['janeiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['fevereiro']) ? $this->Number->format($th['fevereiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['marco']) ? $this->Number->format($th['marco'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['abril']) ? $this->Number->format($th['abril'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['maio']) ? $this->Number->format($th['maio'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['junho']) ? $this->Number->format($th['junho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['julho']) ? $this->Number->format($th['julho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['agosto']) ? $this->Number->format($th['agosto'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['setembro']) ? $this->Number->format($th['setembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['outubro']) ? $this->Number->format($th['outubro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['novembro']) ? $this->Number->format($th['novembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['dezembro']) ? $this->Number->format($th['dezembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['total']) ? $this->Number->format($th['total'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['porc_entrada']) ? $this->Number->toPercentage($th['porc_entrada']) : '0%' ?></td>
                                                <td class="text-right"> -</td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <tr>
                                        <th></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['janeiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['fevereiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['marco'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['abril'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['maio'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['junho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['julho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['agosto'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['setembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['outubro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['novembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['dezembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisEntradaSaida['entrada'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->toPercentage($somaEntrada) ?></th>
                                        <th class="text-right"> -</th>
                                    </tr>
                                    </tbody>
                                    <thead class="alert-danger">
                                    <tr>
                                        <th>Saída</th>
                                        <th class="text-right">Jan</th>
                                        <th class="text-right">Fev</th>
                                        <th class="text-right">Mar</th>
                                        <th class="text-right">Abr</th>
                                        <th class="text-right">Mai</th>
                                        <th class="text-right">Jun</th>
                                        <th class="text-right">Jul</th>
                                        <th class="text-right">Ago</th>
                                        <th class="text-right">Set</th>
                                        <th class="text-right">Out</th>
                                        <th class="text-right">Nov</th>
                                        <th class="text-right">Dez</th>
                                        <th class="text-right">Total</th>
                                        <th class="text-right">% Entrada</th>
                                        <th class="text-right">% Saida</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $somaSaida = 0; ?>
                                    <?php foreach ($tabelaHistoricos as $th): //Histórico Fechado ?>
                                        <?php
                                        $th['porc_saida'] = $valoresTotaisEntradaSaida['saida'] != 0 ? ($th['total'] / $valoresTotaisEntradaSaida['saida']) * 100 : 0;
                                        ?>
                                        <?php if ($th['tipo'] == 2): //Saída ?>
                                            <?php $somaSaida += $th['porc_saida']; ?>
                                            <tr>
                                                <td><?php echo !empty($th['nome']) ? $th['nome'] : 'Não Informado' ?></td>
                                                <td class="text-right"><?php echo !empty($th['janeiro']) ? $this->Number->format($th['janeiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['fevereiro']) ? $this->Number->format($th['fevereiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['marco']) ? $this->Number->format($th['marco'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['abril']) ? $this->Number->format($th['abril'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['maio']) ? $this->Number->format($th['maio'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['junho']) ? $this->Number->format($th['junho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['julho']) ? $this->Number->format($th['julho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['agosto']) ? $this->Number->format($th['agosto'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['setembro']) ? $this->Number->format($th['setembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['outubro']) ? $this->Number->format($th['outubro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['novembro']) ? $this->Number->format($th['novembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['dezembro']) ? $this->Number->format($th['dezembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['total']) ? $this->Number->format($th['total'], $options) : 0.00 ?></td>
                                                <td class="text-right"> -</td>
                                                <td class="text-right"><?php echo !empty($th['porc_saida']) ? $this->Number->toPercentage($th['porc_saida']) : '0%' ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <tr>
                                        <th></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['janeiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['fevereiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['marco'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['abril'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['maio'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['junho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['julho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['agosto'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['setembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['outubro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['novembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['dezembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisEntradaSaida['saida'], $options) ?></th>
                                        <th class="text-right"> -</th>
                                        <th class="text-right"><?php echo $this->Number->toPercentage($somaSaida) ?></th>
                                    </tr>
                                    <tr>
                                        <th>Movimentação Total</th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['janeiro'] + $valoresTotaisMesAMes['saida']['janeiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['fevereiro'] + $valoresTotaisMesAMes['saida']['fevereiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['marco'] + $valoresTotaisMesAMes['saida']['marco'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['abril'] + $valoresTotaisMesAMes['saida']['abril'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['maio'] + $valoresTotaisMesAMes['saida']['maio'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['junho'] + $valoresTotaisMesAMes['saida']['junho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['julho'] + $valoresTotaisMesAMes['saida']['julho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['agosto'] + $valoresTotaisMesAMes['saida']['agosto'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['setembro'] + $valoresTotaisMesAMes['saida']['setembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['outubro'] + $valoresTotaisMesAMes['saida']['outubro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['novembro'] + $valoresTotaisMesAMes['saida']['novembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['dezembro'] + $valoresTotaisMesAMes['saida']['dezembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisEntradaSaida['entrada'] - $valoresTotaisEntradaSaida['saida'], $options) ?></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
            <?php elseif(isset($tabelaHistoricosAbertos)):?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h2><?= __('Resultados Históricos (Abertos)') ?></h2>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div>
                                <table class="table table-hover">
                                    <thead class="alert-success">
                                    <tr>
                                        <th>Entrada</th>
                                        <th class="text-right">Jan</th>
                                        <th class="text-right">Fev</th>
                                        <th class="text-right">Mar</th>
                                        <th class="text-right">Abr</th>
                                        <th class="text-right">Mai</th>
                                        <th class="text-right">Jun</th>
                                        <th class="text-right">Jul</th>
                                        <th class="text-right">Ago</th>
                                        <th class="text-right">Set</th>
                                        <th class="text-right">Out</th>
                                        <th class="text-right">Nov</th>
                                        <th class="text-right">Dez</th>
                                        <th class="text-right">Total</th>
                                        <th class="text-right">% Entrada</th>
                                        <th class="text-right">% Saida</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $somaEntrada = 0; ?>
                                    <?php $auxIdClassificacao = -1; ?>
                                    <?php foreach ($tabelaHistoricosAbertos as $th): //Histórico Aberto ?>
                                        <?php
                                        $th['porc_entrada'] = $valoresTotaisEntradaSaida['entrada'] != 0 ? $th['total'] / $valoresTotaisEntradaSaida['entrada'] * 100 : 0;
                                        ?>
                                        <?php if ($th['tipo'] == 1): ?>
                                            <?php if ($auxIdClassificacao != $th['classificacao_id']): ?>
                                                <?php $auxIdClassificacao = $th['classificacao_id'] ?>
                                                <tr>
                                                    <td class="alert-warning" colspan="16">
                                                        <strong><?php echo $th['classificacao_conta'] ?></strong></td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php $somaEntrada += $th['porc_entrada']; ?>
                                            <tr>
                                                <td><?php echo !empty($th['nome']) ? $th['nome'] : 'Não Informado' ?></td>
                                                <td class="text-right"><?php echo !empty($th['janeiro']) ? $this->Number->format($th['janeiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['fevereiro']) ? $this->Number->format($th['fevereiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['marco']) ? $this->Number->format($th['marco'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['abril']) ? $this->Number->format($th['abril'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['maio']) ? $this->Number->format($th['maio'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['junho']) ? $this->Number->format($th['junho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['julho']) ? $this->Number->format($th['julho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['agosto']) ? $this->Number->format($th['agosto'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['setembro']) ? $this->Number->format($th['setembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['outubro']) ? $this->Number->format($th['outubro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['novembro']) ? $this->Number->format($th['novembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['dezembro']) ? $this->Number->format($th['dezembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['total']) ? $this->Number->format($th['total'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['porc_entrada']) ? $this->Number->toPercentage($th['porc_entrada']) : '0%' ?></td>
                                                <td class="text-right"> -</td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <tr>
                                        <th></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['janeiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['fevereiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['marco'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['abril'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['maio'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['junho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['julho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['agosto'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['setembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['outubro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['novembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['dezembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisEntradaSaida['entrada'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->toPercentage($somaEntrada) ?></th>
                                        <th class="text-right"> -</th>
                                    </tr>
                                    </tbody>
                                    <thead class="alert-danger">
                                    <tr>
                                        <th>Saída</th>
                                        <th class="text-right">Jan</th>
                                        <th class="text-right">Fev</th>
                                        <th class="text-right">Mar</th>
                                        <th class="text-right">Abr</th>
                                        <th class="text-right">Mai</th>
                                        <th class="text-right">Jun</th>
                                        <th class="text-right">Jul</th>
                                        <th class="text-right">Ago</th>
                                        <th class="text-right">Set</th>
                                        <th class="text-right">Out</th>
                                        <th class="text-right">Nov</th>
                                        <th class="text-right">Dez</th>
                                        <th class="text-right">Total</th>
                                        <th class="text-right">% Entrada</th>
                                        <th class="text-right">% Saida</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $somaSaida = 0; ?>
                                    <?php $auxIdClassificacao = -1; ?>
                                    <?php foreach ($tabelaHistoricosAbertos as $th): //Histórico Aberto ?>
                                        <?php
                                        $th['porc_saida'] = $valoresTotaisEntradaSaida['saida'] != 0 ? ($th['total'] / $valoresTotaisEntradaSaida['saida']) * 100 : 0;
                                        ?>
                                        <?php if ($th['tipo'] == 2): // Saída ?>
                                            <?php if ($auxIdClassificacao != $th['classificacao_id']): ?>
                                                <?php $auxIdClassificacao = $th['classificacao_id'] ?>
                                                <tr>
                                                    <td class="alert-warning" colspan="16">
                                                        <strong><?php echo $th['classificacao_conta'] ?></strong></td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php $somaSaida += $th['porc_saida']; ?>
                                            <tr>
                                                <td><?php echo !empty($th['nome']) ? $th['nome'] : 'Não Informado' ?></td>
                                                <td class="text-right"><?php echo !empty($th['janeiro']) ? $this->Number->format($th['janeiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['fevereiro']) ? $this->Number->format($th['fevereiro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['marco']) ? $this->Number->format($th['marco'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['abril']) ? $this->Number->format($th['abril'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['maio']) ? $this->Number->format($th['maio'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['junho']) ? $this->Number->format($th['junho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['julho']) ? $this->Number->format($th['julho'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['agosto']) ? $this->Number->format($th['agosto'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['setembro']) ? $this->Number->format($th['setembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['outubro']) ? $this->Number->format($th['outubro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['novembro']) ? $this->Number->format($th['novembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['dezembro']) ? $this->Number->format($th['dezembro'], $options) : 0.00 ?></td>
                                                <td class="text-right"><?php echo !empty($th['total']) ? $this->Number->format($th['total'], $options) : 0.00 ?></td>
                                                <td class="text-right"> -</td>
                                                <td class="text-right"><?php echo !empty($th['porc_saida']) ? $this->Number->toPercentage($th['porc_saida']) : '0%' ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                    <tr>
                                        <th></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['janeiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['fevereiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['marco'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['abril'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['maio'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['junho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['julho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['agosto'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['setembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['outubro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['novembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['saida']['dezembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisEntradaSaida['saida'], $options) ?></th>
                                        <th class="text-right"> -</th>
                                        <th class="text-right"><?php echo $this->Number->toPercentage($somaSaida) ?></th>
                                    </tr>
                                    <tr>
                                        <th>Movimentação Total</th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['janeiro'] + $valoresTotaisMesAMes['saida']['janeiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['fevereiro'] + $valoresTotaisMesAMes['saida']['fevereiro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['marco'] + $valoresTotaisMesAMes['saida']['marco'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['abril'] + $valoresTotaisMesAMes['saida']['abril'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['maio'] + $valoresTotaisMesAMes['saida']['maio'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['junho'] + $valoresTotaisMesAMes['saida']['junho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['julho'] + $valoresTotaisMesAMes['saida']['julho'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['agosto'] + $valoresTotaisMesAMes['saida']['agosto'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['setembro'] + $valoresTotaisMesAMes['saida']['setembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['outubro'] + $valoresTotaisMesAMes['saida']['outubro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['novembro'] + $valoresTotaisMesAMes['saida']['novembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisMesAMes['entrada']['dezembro'] + $valoresTotaisMesAMes['saida']['dezembro'], $options) ?></th>
                                        <th class="text-right"><?php echo $this->Number->format($valoresTotaisEntradaSaida['entrada'] - $valoresTotaisEntradaSaida['saida'], $options) ?></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif;?>
                
            </div>
    </div>
</div>