
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Profissional Creditos</h2>
        <ol class="breadcrumb">
            <li>Profissional Creditos</li>
            <li class="active">
                <strong>Litagem de Profissional Creditos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Profissional Creditos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Medico Responsavei') ?></th>
                                                                <td><?= $profissionalCredito->has('medico_responsavei') ? $this->Html->link($profissionalCredito->medico_responsavei->nome, ['controller' => 'MedicoResponsaveis', 'action' => 'view', $profissionalCredito->medico_responsavei->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($profissionalCredito->descricao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $profissionalCredito->has('situacao_cadastro') ? $this->Html->link($profissionalCredito->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $profissionalCredito->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $profissionalCredito->has('user') ? $this->Html->link($profissionalCredito->user->nome, ['controller' => 'Users', 'action' => 'view', $profissionalCredito->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($profissionalCredito->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Credito') ?></th>
                                <td><?= $this->Number->format($profissionalCredito->credito) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Debito') ?></th>
                                <td><?= $this->Number->format($profissionalCredito->debito) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($profissionalCredito->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($profissionalCredito->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($profissionalCredito->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?>6</th>
                                <td><?= $profissionalCredito->status ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Observacao') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($profissionalCredito->observacao)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


