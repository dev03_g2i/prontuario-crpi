<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Profissional Creditos</h2>
            <ol class="breadcrumb">
                <li>Profissional Creditos</li>
                <li class="active">
                    <strong>Litagem de Profissional Creditos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Créditos', ['action' => 'add', '?' => ['profissional_id' => $profissional_id, 'ajax' => 1]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Créditos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Créditos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('data') ?></th>
                                        <th><?= $this->Paginator->sort('credito') ?></th>
                                        <th><?= $this->Paginator->sort('debito') ?></th>
                                        <th><?= $this->Paginator->sort('descricao') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th><?= $this->Paginator->sort('status',['label'=>'Quitado']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($profissionalCreditos as $profissionalCredito): ?>
                                        <tr>
                                            <td><?= h($profissionalCredito->data) ?></td>
                                            <td><?= $this->Number->currency($profissionalCredito->credito) ?></td>
                                            <td><?= $this->Number->currency($profissionalCredito->debito) ?></td>
                                            <td><?= h($profissionalCredito->descricao) ?></td>
                                            <td><?= h($profissionalCredito->created) ?></td>
                                            <td><?= h($profissionalCredito->modified) ?></td>
                                            <td><?= h(($profissionalCredito->status==1) ? 'Sim':'Nào') ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $profissionalCredito->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $profissionalCredito->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'profissionalCreditos\',' . $profissionalCredito->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
