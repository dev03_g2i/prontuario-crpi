<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Profissional Creditos</h2>
            <ol class="breadcrumb">
                <li>Profissional Creditos</li>
                <li class="active">
                    <strong> Editar Profissional Creditos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="profissionalCreditos form">
                            <?= $this->Form->create($profissionalCredito) ?>
                            <fieldset>
                                <legend><?= __('Editar Profissional Credito') ?></legend>
                                <?php
                                echo $this->Form->input('profissional_id', ['type' => 'hidden']);
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('descricao',['label'=>'Descrição']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('data', ['empty' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($profissionalCredito->data, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('credito', ['label'=>'Crédito','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('debito', ['label'=>'Débito','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('observacao',['label'=>'Observação']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('status',['label'=>' Quitado?']);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

