<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Controladores</h2>
        <ol class="breadcrumb">
            <li>Grupo Controladores</li>
            <li class="active">
                <strong>                    Editar Grupo Controladores
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="grupoControladores form">
                        <?= $this->Form->create($grupoControladore) ?>
                        <fieldset>
                            <legend><?= __('Editar Grupo Controladore') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('grupo_id', ['data'=>'select','controller'=>'grupoUsuarios','action'=>'fill','data-value'=>$grupoControladore->grupo_id]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('controlador_id', ['data'=>'select','controller'=>'controladores','action'=>'fill','data-value'=>$grupoControladore->controlador_id]);
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

