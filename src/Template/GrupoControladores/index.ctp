<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Grupo Controladores</h2>
            <ol class="breadcrumb">
                <li>Grupo Controladores</li>
                <li class="active">
                    <strong>Litagem de Grupo Controladores</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Grupo Controladores') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Grupo</th>
                                        <th>Página</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($grupoControladores as $grupoControladore): ?>
                                        <tr>
                                            <td><?= $grupoControladore->has('grupo_usuario') ? $this->Html->link($grupoControladore->grupo_usuario->nome, ['controller' => 'GrupoUsuarios', 'action' => 'view', $grupoControladore->grupo_usuario->id]) : '' ?></td>
                                            <td><?= $grupoControladore->has('controladore') ? $this->Html->link($grupoControladore->controladore->nome, ['controller' => 'Controladores', 'action' => 'view', $grupoControladore->controladore->id]) : '' ?></td>
                                            <?php
                                                if(!empty($grupoControladore->grupo_controlador_acoes)):
                                                    $grupo_acoes = $grupoControladore->grupo_controlador_acoes;
                                                    foreach ($grupo_acoes as $grupo_acoe): ?>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" <?= ($grupo_acoe->acesso==1) ? 'checked' : '' ?> value="<?= $grupo_acoe->id ?>" name="checkacesso"> <?= $grupo_acoe->aco->nome ?>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    <?php endforeach;
                                                endif;
                                             ?>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
