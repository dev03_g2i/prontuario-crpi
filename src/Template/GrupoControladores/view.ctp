
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Grupo Controladores</h2>
        <ol class="breadcrumb">
            <li>Grupo Controladores</li>
            <li class="active">
                <strong>Litagem de Grupo Controladores</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Grupo Controladores</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Grupo Usuario') ?></th>
                                                                <td><?= $grupoControladore->has('grupo_usuario') ? $this->Html->link($grupoControladore->grupo_usuario->nome, ['controller' => 'GrupoUsuarios', 'action' => 'view', $grupoControladore->grupo_usuario->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Controladore') ?></th>
                                                                <td><?= $grupoControladore->has('controladore') ? $this->Html->link($grupoControladore->controladore->nome, ['controller' => 'Controladores', 'action' => 'view', $grupoControladore->controladore->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $grupoControladore->has('situacao_cadastro') ? $this->Html->link($grupoControladore->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoControladore->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $grupoControladore->has('user') ? $this->Html->link($grupoControladore->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoControladore->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($grupoControladore->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($grupoControladore->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($grupoControladore->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


