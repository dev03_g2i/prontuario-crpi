<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Gasto Itens</h2>
            <ol class="breadcrumb">
                <li>Gasto Itens</li>
                <li class="active">
                    <strong>Litagem de Gasto Itens</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('GastoIten', ['type' => 'get']) ?>
                            <?php
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('gasto_id', ['name' => 'GastoItens__gasto_id', 'data' => 'select', 'controller' => 'gastos', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('artigo_id', ['name' => 'GastoItens__artigo_id', 'data' => 'select', 'controller' => 'estqArtigo', 'action' => 'fill', 'empty' => 'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('quantidade', ['name' => 'GastoItens__quantidade']);
                            echo "</div>";
                            echo "<div class='col-md-4'>";
                            echo $this->Form->input('situacao_id', ['name' => 'GastoItens__situacao_id']);
                            echo "</div>";
                            ?>
                            <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Gasto Itens', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Gasto Itens', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Gasto Itens') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('gasto_id') ?></th>
                                        <th><?= $this->Paginator->sort('artigo_id') ?></th>
                                        <th><?= $this->Paginator->sort('quantidade') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th><?= $this->Paginator->sort('user_id') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_id') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($gastoItens as $gastoIten): ?>
                                        <tr>
                                            <td><?= $this->Number->format($gastoIten->id) ?></td>
                                            <td><?= $gastoIten->has('gasto') ? $this->Html->link($gastoIten->gasto->nome, ['controller' => 'Gastos', 'action' => 'view', $gastoIten->gasto->id]) : '' ?></td>
                                            <td><?= $gastoIten->has('estq_artigo') ? $this->Html->link($gastoIten->estq_artigo->nome, ['controller' => 'EstqArtigo', 'action' => 'view', $gastoIten->estq_artigo->id]) : '' ?></td>
                                            <td><?= $this->Number->format($gastoIten->quantidade) ?></td>
                                            <td><?= h($gastoIten->created) ?></td>
                                            <td><?= h($gastoIten->modified) ?></td>
                                            <td><?= $gastoIten->has('user') ? $this->Html->link($gastoIten->user->nome, ['controller' => 'Users', 'action' => 'view', $gastoIten->user->id]) : '' ?></td>
                                            <td><?= $this->Number->format($gastoIten->situacao_id) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $gastoIten->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $gastoIten->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'gastoItens\',' . $gastoIten->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
