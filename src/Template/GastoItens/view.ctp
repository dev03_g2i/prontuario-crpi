
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Gasto Itens</h2>
        <ol class="breadcrumb">
            <li>Gasto Itens</li>
            <li class="active">
                <strong>Litagem de Gasto Itens</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Gasto Itens</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Gasto') ?></th>
                                                                <td><?= $gastoIten->has('gasto') ? $this->Html->link($gastoIten->gasto->nome, ['controller' => 'Gastos', 'action' => 'view', $gastoIten->gasto->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Estq Artigo') ?></th>
                                                                <td><?= $gastoIten->has('estq_artigo') ? $this->Html->link($gastoIten->estq_artigo->nome, ['controller' => 'EstqArtigo', 'action' => 'view', $gastoIten->estq_artigo->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $gastoIten->has('user') ? $this->Html->link($gastoIten->user->nome, ['controller' => 'Users', 'action' => 'view', $gastoIten->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($gastoIten->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($gastoIten->quantidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Id') ?></th>
                                <td><?= $this->Number->format($gastoIten->situacao_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($gastoIten->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($gastoIten->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


