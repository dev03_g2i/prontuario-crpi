<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Gasto Itens</h2>
        <ol class="breadcrumb">
            <li>Gasto Itens</li>
            <li class="active">
                <strong>                    Cadastrar Gasto Itens
                    </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="gastoItens form">
                        <?= $this->Form->create($gastoIten) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Gasto Iten') ?></legend>
                            <?php
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('gasto_id', ['data'=>'select','controller'=>'gastos','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                    echo $this->Form->input('artigo_id', ['data'=>'select','controller'=>'estqArtigo','action'=>'fill']);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('quantidade');
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                        echo $this->Form->input('situacao_id');
                                echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

