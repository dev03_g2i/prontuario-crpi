<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estq Kit Artigo</h2>
            <ol class="breadcrumb">
                <li>Estq Kit Artigo</li>
                <li class="active">
                    <strong>Litagem de Estq Kit Artigo</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Estq Kit Artigo</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th>
                                        <?= __('Quem Cadastrou') ?>
                                    </th>
                                    <td>
                                        <?= $estqKitArtigo->has('user') ? $this->Html->link($estqKitArtigo->user->nome, ['controller' => 'Users', 'action' => 'view', $estqKitArtigo->user->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($estqKitArtigo->id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Fatura Kit') ?>
                                    </th>
                                    <td>
                                        <?= $estqKitArtigo->has('estq_kit') ? $this->Html->link($estqKitArtigo->estq_kit->nome, ['controller' => 'EstqKit', 'action' => 'view', $estqKitArtigo->estq_kit->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Artigo') ?>
                                    </th>
                                    <td>
                                    <?= $estqKitArtigo->has('estq_artigo') ? $this->Html->link($estqKitArtigo->estq_artigo->nome, ['controller' => 'EstqKit', 'action' => 'view', $estqKitArtigo->estq_artigo->id]) : '' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Quantidade') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($estqKitArtigo->quantidade) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Situacao Id') ?>
                                    </th>
                                    <td>
                                        <?= $this->Number->format($estqKitArtigo->situacao_id) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Criação') ?>
                                    </th>
                                    <td>
                                        <?= h($estqKitArtigo->created) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        <?= __('Dt. Modificação') ?>
                                    </th>
                                    <td>
                                        <?= h($estqKitArtigo->modified) ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>