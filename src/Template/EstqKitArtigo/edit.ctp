<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Estq Kit Artigo</h2>
            <ol class="breadcrumb">
                <li>Estq Kit Artigo</li>
                <li class="active">
                    <strong>
                        Editar Estq Kit Artigo
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="estqKitArtigo form">
                            <?= $this->Form->create($estqKitArtigo) ?>
                                <fieldset>
                                    <legend>
                                        <?= __('Editar Estq Kit Artigo') ?>
                                    </legend>
                                    <div class='col-md-6'>
                                    <?=$this->Form->input('fatura_kit_id', ['data'=>'select','controller'=>'EstqKit','action'=>'fill','data-value'=>$estqKitArtigo->fatura_kit_id]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                    <?=$this->Form->input('artigo_id', ['data'=>'select','controller'=>'estqArtigos','action'=>'fill','data-value'=>$estqKitArtigo->artigo_id]); ?>
                                    </div>
                                    <div class='col-md-6'>
                                        <?=$this->Form->input('quantidade'); ?>
                                    </div>
                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                                </div>
                                <div class="clearfix"></div>
                                <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>