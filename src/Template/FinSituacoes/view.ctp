
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Situacoes</h2>
        <ol class="breadcrumb">
            <li>Fin Situacoes</li>
            <li class="active">
                <strong>Litagem de Fin Situacoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Situacoes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($finSituaco->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finSituaco->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('DataCadastro') ?></th>
                                                                <td><?= h($finSituaco->dataCadastro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Status') ?>6</th>
                                <td><?= $finSituaco->status ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


