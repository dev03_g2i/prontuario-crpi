<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Fin Situacoes</h2>
            <ol class="breadcrumb">
                <li>Fin Situacoes</li>
                <li class="active">
                    <strong>
                                                Cadastrar Fin Situacoes
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="finSituacoes form">
                            <?= $this->Form->create($finSituaco) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Fin Situaco') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('nome'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('dataCadastro'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('status'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

