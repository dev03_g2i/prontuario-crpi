
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Fin Contas Pagar</h2>
        <ol class="breadcrumb">
            <li>Fin Contas Pagar</li>
            <li class="active">
                <strong>Litagem de Fin Contas Pagar</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Fin Contas Pagar</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Fin Plano Conta') ?></th>
                                                                <td><?= $finContasPagar->fin_plano_conta ? $this->Html->link($finContasPagar->fin_plano_conta->nome, ['controller' => 'FinPlanoContas', 'action' => 'view', $finContasPagar->fin_plano_conta->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Fornecedore') ?></th>
                                                                <td><?= $finContasPagar->fin_fornecedor ? $this->Html->link($finContasPagar->fin_fornecedore->nome, ['controller' => 'FinFornecedores', 'action' => 'view', $finContasPagar->fin_fornecedore->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Contabilidade') ?></th>
                                                                <td><?= $finContasPagar->fin_contabilidade ? $this->Html->link($finContasPagar->fin_contabilidade->nome, ['controller' => 'FinContabilidades', 'action' => 'view', $finContasPagar->fin_contabilidade->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Tipo Pagamento') ?></th>
                                                                <td><?= $finContasPagar->fin_tipo_pagamento ? $this->Html->link($finContasPagar->fin_tipo_pagamento->id, ['controller' => 'FinTipoPagamento', 'action' => 'view', $finContasPagar->fin_tipo_pagamento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Tipo Documento') ?></th>
                                                                <td><?= $finContasPagar->fin_tipo_documento ? $this->Html->link($finContasPagar->fin_tipo_documento->id, ['controller' => 'FinTipoDocumento', 'action' => 'view', $finContasPagar->fin_tipo_documento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero Documento') ?></th>
                                <td><?= h($finContasPagar->numero_documento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Parcela') ?></th>
                                <td><?= h($finContasPagar->parcela) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fin Contas Pagar Planejamento') ?></th>
                                                                <td><?= $finContasPagar->fin_contas_pagar_planejamento ? $this->Html->link($finContasPagar->fin_contas_pagar_planejamento->id, ['controller' => 'FinContasPagarPlanejamentos', 'action' => 'view', $finContasPagar->fin_contas_pagar_planejamento->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($finContasPagar->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor') ?></th>
                                <td><?= $this->Number->format($finContasPagar->valor) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Juros') ?></th>
                                <td><?= $this->Number->format($finContasPagar->juros) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Multa') ?></th>
                                <td><?= $this->Number->format($finContasPagar->multa) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao') ?></th>
                                <td><?= $this->Number->format($finContasPagar->situacao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Bruto') ?></th>
                                <td><?= $this->Number->format($finContasPagar->valor_bruto) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Desconto') ?></th>
                                <td><?= $this->Number->format($finContasPagar->desconto) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data') ?></th>
                                                                <td><?= h($finContasPagar->data) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Vencimento') ?></th>
                                                                <td><?= h($finContasPagar->vencimento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Pagamento') ?></th>
                                                                <td><?= h($finContasPagar->data_pagamento) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Data Cadastro') ?></th>
                                                                <td><?= h($finContasPagar->data_cadastro) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Complemento') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($finContasPagar->complemento)); ?>
                </div>
            </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Abatimentos Contas Pagar') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContasPagar->fin_abatimentos_contas_pagar)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Fin Abatimento Id') ?></th>
                        <th><?= __('Fin Contas Pagar Id') ?></th>
                        <th><?= __('Valor') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContasPagar->fin_abatimentos_contas_pagar as $finAbatimentosContasPagar): ?>
                <tr>
                    <td><?= h($finAbatimentosContasPagar->id) ?></td>
                    <td><?= h($finAbatimentosContasPagar->fin_abatimento_id) ?></td>
                    <td><?= h($finAbatimentosContasPagar->fin_contas_pagar_id) ?></td>
                    <td><?= h($finAbatimentosContasPagar->valor) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinAbatimentosContasPagar','action' => 'view', $finAbatimentosContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinAbatimentosContasPagar','action' => 'edit', $finAbatimentosContasPagar->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinAbatimentosContasPagar','action' => 'delete', $finAbatimentosContasPagar->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContasPagar->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Fin Movimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($finContasPagar->fin_movimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Situacao') ?></th>
                        <th><?= __('Data') ?></th>
                        <th><?= __('Fin Plano Conta Id') ?></th>
                        <th><?= __('Fin Fornecedor Id') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Documento') ?></th>
                        <th><?= __('Credito') ?></th>
                        <th><?= __('Debito') ?></th>
                        <th><?= __('Fin Banco Id') ?></th>
                        <th><?= __('Status Lancamento') ?></th>
                        <th><?= __('Categoria') ?></th>
                        <th><?= __('IdCliente') ?></th>
                        <th><?= __('Fin Contas Pagar Id') ?></th>
                        <th><?= __('Fin Contabilidade Id') ?></th>
                        <th><?= __('Data Criacao') ?></th>
                        <th><?= __('Data Alteracao') ?></th>
                        <th><?= __('Criado Por') ?></th>
                        <th><?= __('Atualizado Por') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($finContasPagar->fin_movimentos as $finMovimentos): ?>
                <tr>
                    <td><?= h($finMovimentos->id) ?></td>
                    <td><?= h($finMovimentos->situacao) ?></td>
                    <td><?= h($finMovimentos->data) ?></td>
                    <td><?= h($finMovimentos->fin_plano_conta_id) ?></td>
                    <td><?= h($finMovimentos->fin_fornecedor_id) ?></td>
                    <td><?= h($finMovimentos->complemento) ?></td>
                    <td><?= h($finMovimentos->documento) ?></td>
                    <td><?= h($finMovimentos->credito) ?></td>
                    <td><?= h($finMovimentos->debito) ?></td>
                    <td><?= h($finMovimentos->fin_banco_id) ?></td>
                    <td><?= h($finMovimentos->status_lancamento) ?></td>
                    <td><?= h($finMovimentos->categoria) ?></td>
                    <td><?= h($finMovimentos->idCliente) ?></td>
                    <td><?= h($finMovimentos->fin_contas_pagar_id) ?></td>
                    <td><?= h($finMovimentos->fin_contabilidade_id) ?></td>
                    <td><?= h($finMovimentos->data_criacao) ?></td>
                    <td><?= h($finMovimentos->data_alteracao) ?></td>
                    <td><?= h($finMovimentos->criado_por) ?></td>
                    <td><?= h($finMovimentos->atualizado_por) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'FinMovimentos','action' => 'view', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'FinMovimentos','action' => 'edit', $finMovimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'FinMovimentos','action' => 'delete', $finMovimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $finContasPagar->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


