<div class="row">
    <div class="row">
        <div class="col-sm-12 no-padding">
            <section class="col-sm-12 connectedSortable no-padding p-b-5">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h3>Contas à pagar</h3>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('Contaspagar', ['type' => 'get', 'id' => 'contas-pagar-form']) ?>
                            <div class="col-sm-12 no-padding">
                                <div class="col-sm-3">
                                    <?= $this->Form->input('inicio', ['label' => 'Inicio', 'type' => 'text', 'class' => "datepicker", 'value' => date('01/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('fim', ['label' => 'Fim', 'type' => 'text', 'class' => "datepicker", 'value' => date('t/m/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('planoconta', ['label' => 'Plano de contas', 'multiple' => 'multiple', 'type' => 'select', 'data-planocontas' => "planocontas", 'empty' => 'Selecione', 'options' => [@$fin_plano_contas->id => @$fin_plano_contas->nome], 'default' => @$fin_plano_contas->id]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('fornecedor', ['label' => 'Credores', 'type' => 'select', 'multiple' => 'multiple', 'data-fornecedores' => "fornecedores", 'empty' => 'Selecione', 'options' => [@$fin_fornecedores->id => @$fin_fornecedores->nome], 'default' => @$fin_fornecedores->id]); ?>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="col-sm-3">
                                    <?= $this->Form->input('contabilidade', ['label' => 'Empresas', 'empty' => 'Selecione', 'options' => $contabilidades]); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('situacao', ['label' => 'Situação', 'type' => 'select', 'options' => [1 => 'Pagas', 2 => 'Pendentes'], 'class' => 'form-control select2']) ?>
                                </div>
                                <div class="col-sm-3">
                                    <?= $this->Form->input('status_conta', ['label' => 'Status', 'type' => 'select', 'options' => [1 => 'Ativo', 2 => 'Inativo'], 'class' => 'form-control select2']) ?>
                                </div>
                                <div class='col-sm-3'>
                                    <?= $this->Form->input('relatorio', ['label' => 'Relatório', 'type' => 'select',
                                        'options' => $tiposRelatorios,
                                        'append' => [
                                            $this->Form->button($this->Html->icon('print'), ['id' => 'gerar-report',
                                                'type' => 'button', 'class' => 'btn btn-success',
                                                'title' => 'Imprirmir Relatório', 'escape' => false, 'controller' => 'relatorios',
                                                'action' => 'financeiroContasPagar'])]
                                    ]) ?>
                                </div>
                            </div>
                            <div class="col-sm-12 no-padding">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 text-center">
                                    <?= $this->Form->input('documento', ['label' => 'Documento', 'type' => 'text']); ?>
                                </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            <div class="col-sm-12 text-center p-t-25">
                                <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default btnFiltrarContasPagar', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default btnRefreshContasPagar', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                <!--                                            <= $this->Html->link('<i class="fa fa-print"></i>', '', ['escape' => false, 'class'=>'btn btn-default', 'title' => 'Relatório por vencimento']) ?>-->
                                <!--                                                <= $this->Html->link('<i class="fa fa-print"></i>', '', ['escape' => false, 'class'=>'btn btn-default', 'title' => 'Relatório geral']) ?>-->
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12 text-left">
                        <div class="col-md-2">
                            <label>Qtd.</label>
                            </br>
                            <strong class="quantidade_reg count">0</strong>
                        </div>
                        <div class="col-md-2">
                            <label>Vl. Bruto</label>
                            </br>
                            <strong class="valor_bruto total">0,00</strong>
                        </div>
                        <div class="col-md-2">
                            <label>Vl. C/ Dedução</label>
                            </br>
                            <strong class="valor_deducao total">0,00</strong>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <section class="col-sm-12 no-padding connectedSortable">
            <div class="col-sm-12 no-padding p-t-10">
                <div class="col-sm-12 no-padding">
                    <div class="ibox float-e-margins">
                        <div class="col-sm-12 ibox-content btn-group p-b-0 p-l-20">
                            <div class="col-sm-2 no-padding">
                                <h3>Contas à pagar</h3>
                            </div>
                            <div class="col-sm-8 col-sm-offset-2 no-padding text-right">
                                <?= $this->Html->link('<i class="fa fa-plus-circle"></i> Individual', '/finContasPagar/add', ['escape' => false, 'class' => 'btn btn-info', 'target' => '_blank']) ?>
                                <?= $this->Html->link('<i class="fa fa-plus-circle"></i> Programação', '', ['escape' => false, 'class' => 'btn btn-primary']) ?>
                                <?= $this->Html->link('<i class="fa fa-refresh"></i>', '', ['escape' => false, 'class' => 'btn btn-default', 'title' => 'Recarregar página']) ?>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="clearfix">
                                <div class="p-t-10 table-responsive no-padding background-white">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th name="vencimentoCol">
                                                <?= $this->Paginator->sort('vencimento') ?>
                                            </th>
                                            <th name="fornecedorCol">
                                                <?= $this->Paginator->sort('fin_fornecedor_id', 'Fornecedor') ?>
                                            </th>
                                            <th name="planoCol">
                                                <?= $this->Paginator->sort('fin_plano_conta_id', 'Plano de contas') ?>
                                            </th>
                                            <th name="contabilidadeCol">
                                                <?= $this->Paginator->sort('fin_contabilidade_id', 'Empresa') ?>
                                            </th>
                                            <th name="documentoCol">
                                                <?= $this->Paginator->sort('fin_tipo_documento_id', 'Tipo de documento') ?>
                                            </th>
                                            <th name="parcelaCol">
                                                <?= $this->Paginator->sort('parcela') ?>
                                            </th>
                                            <th name="complementoCol">
                                                <?= $this->Paginator->sort('complemento') ?>
                                            </th>
                                            <th name="valorCol">
                                                <?= $this->Paginator->sort('valor_bruto') ?>
                                            </th>
                                            <th name="valorDedCol">
                                                <?= $this->Paginator->sort('valor', 'Valor c/ ded.') ?>
                                            </th>
                                            <th name="pagamentoCol">
                                                <?= $this->Paginator->sort('data_pagamento', 'Data pgmt') ?>
                                            </th>
                                            <th>
                                                <?= __('Ações') ?>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($contaspagar as $contaspagar): ?>
                                            <tr>
                                                <td name="vencimentoCol">
                                                    <?= date_format($contaspagar->vencimento, 'd/m/Y') ?>
                                                </td>
                                                <td name="fornecedorCol">
                                                    <?= h($contaspagar->fin_fornecedore->nome) ?>
                                                </td>
                                                <td name="planoCol">
                                                    <?= h($contaspagar->fin_plano_conta->nome) ?>
                                                </td>
                                                <td name="contabilidadeCol">
                                                    <?= h($contaspagar->fin_contabilidade->nome) ?>
                                                </td>
                                                <td name="documentoCol">
                                                    <?= h($contaspagar->fin_tipo_documento->descricao) ?>
                                                </td>
                                                <td name="parcelaCol">
                                                    <?= h($contaspagar->parcela) ?>
                                                </td>
                                                <td name="complementoCol">
                                                    <?= $contaspagar->complemento ? h($contaspagar->complemento) : '' ?>
                                                </td>
                                                <td name="valorCol">
                                                    <?= $contaspagar->valor_bruto ? number_format($contaspagar->valor_bruto, 2, ',', '.') : '0,00' ?>
                                                </td>
                                                <td name="valorDedCol">
                                                    <?= $contaspagar->valor ? number_format($contaspagar->valor, 2, ',', '.') : '0,00' ?>
                                                </td>
                                                <td name="pagamentoCol">
                                                    <?= $contaspagar->data_pagamento ? date_format($contaspagar->data_pagamento, 'd/m/Y') : '' ?>
                                                </td>
                                                <td class="actions" style="white-space:nowrap">
                                                    <div class="no-padding dropdown">
                                                        <?= $this->Form->button('<i class="fa fa-list"></i> Opções<i class="m-l-5 fa fa-caret-down"></i>', ['escape' => false, 'class' => 'btn btn-default btn-xs dropdown-toggle', 'data-toggle' => "dropdown"]) ?>
                                                        <ul class="dropdown-menu dropdown-menu-left">
                                                            <li>
                                                                <?= $this->Html->link('Visualizar', ['action' => 'view', $contaspagar->id], ['target' => '_blank']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Editar', ['action' => 'edit', $contaspagar->id], ['target' => '_blank']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Editar Rateio', ['']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Anexos', ['']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Excluir', '#Javascript:void(0)', ['onclick' => 'Deletar(\'FinContasPagar\',' . $contaspagar->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'listen' => 'f']) ?>
                                                            </li>
                                                            <li>
                                                                <?= $this->Html->link('Vinculos', ['']) ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                    <div style="text-align:center">
                                        <div class="paginator">
                                            <ul class="pagination">
                                                <?php if ($this->Paginator->numbers()): ?>
                                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                                <?php endif; ?>
                                                <?= $this->Paginator->numbers() ?>
                                                <?php if ($this->Paginator->numbers()): ?>
                                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                                <?php endif; ?>
                                                <p>
                                                    <?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?>
                                                </p>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
echo $this->Html->script('controllers/Contaspagar', ['block' => 'scriptBottom']);
?>
