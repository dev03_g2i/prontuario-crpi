<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Contas Pagar</h2>
            <ol class="breadcrumb">
                <li>Contas Pagar</li>
                <li class="active">
                    <strong>
                        Editar Contas Pagar
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="contaspagar form">
                            <?= $this->Form->create($contaspagar) ?>
                            <fieldset>
                                <legend>
                                    <?= __('Editar Conta') ?>
                                </legend>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('vencimento', ['label' => 'Primeiro Vencimento', 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contaspagar->vencimento, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('parcela', ['name' => 'parcela', 'label' => 'Parcela']); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data', ['label' => 'Data do documento', 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($contaspagar->data, 'dd/MM/Y'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('valor_bruto', ['type' => 'text', 'value' => number_format($contaspagar->valor_bruto, 2), 'label' => 'Valor bruto', 'mask' => 'money', 'prepend' => [$this->Form->button('R$', ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('juros', ['type' => 'text', 'value' => number_format($contaspagar->juros, 2), 'label' => 'Juros', 'mask' => 'money', 'prepend' => [$this->Form->button('R$', ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('multa', ['type' => 'text', 'value' => number_format($contaspagar->multa, 2), 'label' => 'Multa', 'mask' => 'money', 'prepend' => [$this->Form->button('R$', ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('desconto', ['type' => 'text', 'value' => number_format($contaspagar->desconto, 2), 'label' => 'Desconto', 'mask' => 'money', 'prepend' => [$this->Form->button('R$', ['type' => 'button'])]]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('valor', ['type' => 'text', 'redonly', 'value' => number_format($contaspagar->valor, 2),  'label' => 'Valor líquido', 'mask' => 'money', 'prepend' => [$this->Form->button('R$', ['type' => 'button'])]]); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('data_pagamento', ['empty' => true, 'type' => 'text', 'class' => '', 'redonly', 'disabled', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group select required">
                                        <label class="control-label" for="fin-fornecedor-id">Credor</label>
                                        <?= $this->Html->link('Novo', '/FinFornecedores/add', ['escape' => false, 'class' => 'btn btn-xs btn-primary m-l-5"', 'target' => '_blank']) ?>
                                        <select name="fin_fornecedor_id" class="form-control select2-hidden-accessible"
                                                required="required"
                                                data-value="<?= $contaspagar->fin_fornecedor_id?>"
                                                data="select" controller="finFornecedores" action="fill"
                                                id="fin-fornecedor-id" tabindex="-1" aria-hidden="true">
                                            <option value="">selecione</option>
                                        </select>
                                        <span class="select2 select2-container select2-container--bootstrap" dir="ltr"
                                              style="width: 100%;">
                                            <span class="selection"></span>
                                            <span class="dropdown-wrapper" aria-hidden="true"></span>
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('fin_plano_conta_id', ['label' => 'Plano de contas', 'required' => 'required', 'data' => 'select', 'data-value' => $contaspagar->fin_plano_conta_id, 'controller' => 'finPlanoContas', 'action' => 'fill', 'empty' => 'selecione']); ?>
                                </div>

                                <div class='col-md-6'>
                                    <?= $this->Form->input('fin_tipo_documento_id', ['label' => 'Tipo Documento', 'value' => $contaspagar->fin_tipo_documento_id, 'required' => 'required', 'options' => $tipoDocumento, 'empty' => 'Selecione']); ?>
                                </div>

                                <div class='col-md-6'>
                                    <?= $this->Form->input('numero_documento', ['required' => 'required']); ?>
                                </div>

                                <div class='col-md-6'>
                                    <?= $this->Form->input('fin_tipo_pagamento_id', ['label' => 'Forma de pagamento', 'required' => 'required', 'value' => $contaspagar->fin_tipo_pagamento_id, 'options' => $tipoPagamento, 'empty' => 'Selecione']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?= $this->Form->input('fin_contabilidade_id', ['label' => 'Empresas', 'required' => 'required', 'data-value' => $contaspagar->fin_contabilidade_id, 'options' => $contabilidades, 'controller' => 'finContabilidades', 'action' => 'fill', 'empty' => 'Selecione']); ?>
                                </div>
                                <div class='col-md-12'>
                                    <?= $this->Form->input('complemento'); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script('controllers/Contaspagar', ['block' => 'scriptBottom']);
?>