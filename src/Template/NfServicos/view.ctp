
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Nf Servicos</h2>
        <ol class="breadcrumb">
            <li>Nf Servicos</li>
            <li class="active">
                <strong>Litagem de Nf Servicos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Nf Servicos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Descricao') ?></th>
                                <td><?= h($nfServico->descricao) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($nfServico->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Nota Fiscal Id') ?></th>
                                <td><?= $this->Number->format($nfServico->nota_fiscal_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Quantidade') ?></th>
                                <td><?= $this->Number->format($nfServico->quantidade) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Unitario') ?></th>
                                <td><?= $this->Number->format($nfServico->valor_unitario) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Valor Total') ?></th>
                                <td><?= $this->Number->format($nfServico->valor_total) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tributavel') ?>6</th>
                                <td><?= $nfServico->tributavel ? __('Sim') : __('Não'); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


