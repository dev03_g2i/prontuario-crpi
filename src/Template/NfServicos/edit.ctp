<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Servicos</h2>
            <ol class="breadcrumb">
                <li>Nf Servicos</li>
                <li class="active">
                    <strong>
                                                Editar Nf Servicos
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="nfServicos form">
                            <?= $this->Form->create($nfServico) ?>
                            <fieldset>
                                                                <legend><?= __('Editar Nf Servico') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('nota_fiscal_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('tributavel'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('descricao'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('quantidade'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_unitario',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('valor_total',['prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                        </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

