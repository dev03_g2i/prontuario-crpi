<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Servicos</h2>
            <ol class="breadcrumb">
                <li>Nf Servicos</li>
                <li class="active">
                    <strong>Listagem de Nf Servicos</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('NfServico',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('nota_fiscal_id',['name'=>'NfServicos__nota_fiscal_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tributavel',['name'=>'NfServicos__tributavel']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('descricao',['name'=>'NfServicos__descricao']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('quantidade',['name'=>'NfServicos__quantidade']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('valor_unitario',['name'=>'NfServicos__valor_unitario','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                                        </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('valor_total',['name'=>'NfServicos__valor_total','prepend' => 'R$', 'type' => 'text', 'mask' => 'money']); ?>
                                                                                        </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Nf Servicos', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Nf Servicos','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Nf Servicos') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('nota_fiscal_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('tributavel') ?></th>
                                                                                <th><?= $this->Paginator->sort('descricao') ?></th>
                                                                                <th><?= $this->Paginator->sort('quantidade') ?></th>
                                                                                <th><?= $this->Paginator->sort('valor_unitario') ?></th>
                                                                                <th><?= $this->Paginator->sort('valor_total') ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($nfServicos as $nfServico): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($nfServico->id) ?></td>
                                                                                <td><?= $this->Number->format($nfServico->nota_fiscal_id) ?></td>
                                                                                <td><?= h($nfServico->tributavel) ?></td>
                                                                                <td><?= h($nfServico->descricao) ?></td>
                                                                                <td><?= $this->Number->format($nfServico->quantidade) ?></td>
                                                                                <td><?= $this->Number->format($nfServico->valor_unitario) ?></td>
                                                                                <td><?= $this->Number->format($nfServico->valor_total) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'nfServicos','action' => 'view', $nfServico->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'nfServicos','action' => 'edit', $nfServico->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'nfServicos','action'=>'delete', $nfServico->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
