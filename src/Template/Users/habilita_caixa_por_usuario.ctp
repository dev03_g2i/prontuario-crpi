<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agendas</h2>
            <ol class="breadcrumb">
                <li>Agendas</li>
                <li class="active">
                    <strong>Habilitar acesso à agendas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?= $this->Paginator->sort('relatorio_id', ['label' => 'Relatório']) ?></th>
                                            <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                                            <th class="actions text-center"><?= __('Ações') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($relatorioCaixas as $relatorio) : ?>
                                            <tr>
                                                <td><?= $relatorio->descricao ?></td>
                                                <td><?= !empty($user) ? $user->nome : '' ?></td>
                                            <?php 
                                                // se existe o vínculo usuário - relatório, significa que ele tem acesso
                                                if (count($relatorio->configuracao_rel_caixa_usuario) > 0): ?>
                                                <td class="text-center"><?= $this->Form->button('Retirar acesso', ['class' => 'btn btn-danger btn-xs', 'onclick' => 'atualizaAcessoRelCaixa($(this), ' . $user->id . ', ' . $relatorio->id . ')']); ?></td>
                                            <?php else: ?>
                                                <td class="text-center"><?= $this->Form->button('Liberar acesso', ['class' => 'btn btn-primary btn-xs', 'onclick' => 'atualizaAcessoRelCaixa($(this), ' . $user->id . ', ' . $relatorio->id . ')']); ?></td>
                                            <?php endif; ?>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>