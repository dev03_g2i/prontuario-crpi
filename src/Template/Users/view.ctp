

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Users</h2>
        <ol class="breadcrumb">
            <li>Users</li>
            <li class="active">
                <strong>Litagem de Users</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="users">
    <h3><?= h($user->nome) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Grupo Usuario') ?></th>
            <td><?= $user->has('grupo_usuario') ? $this->Html->link($user->grupo_usuario->id, ['controller' => 'GrupoUsuarios', 'action' => 'view', $user->grupo_usuario->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($user->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Login') ?></th>
            <td><?= h($user->login) ?></td>
        </tr>
        <tr>
            <th><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $user->has('situacao_cadastro') ? $this->Html->link($user->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $user->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
        <tr>
            <th><?= __('Atualizar') ?></th>
            <td><?= $user->atualizar ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Atendimento Procedimentos') ?></h4>
        <?php if (!empty($user->atendimento_procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Procedimento Id') ?></th>
                <th><?= __('Atendimento Id') ?></th>
                <th><?= __('Valor Fatura') ?></th>
                <th><?= __('Quantidade') ?></th>
                <th><?= __('Desconto') ?></th>
                <th><?= __('Porc Desconto') ?></th>
                <th><?= __('Valor Caixa') ?></th>
                <th><?= __('Digitado') ?></th>
                <th><?= __('Material') ?></th>
                <th><?= __('Num Controle') ?></th>
                <th><?= __('Medico Id') ?></th>
                <th><?= __('Valor Matmed') ?></th>
                <th><?= __('Documento Guia') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->atendimento_procedimentos as $atendimentoProcedimentos): ?>
            <tr>
                <td><?= h($atendimentoProcedimentos->id) ?></td>
                <td><?= h($atendimentoProcedimentos->procedimento_id) ?></td>
                <td><?= h($atendimentoProcedimentos->atendimento_id) ?></td>
                <td><?= h($atendimentoProcedimentos->valor_fatura) ?></td>
                <td><?= h($atendimentoProcedimentos->quantidade) ?></td>
                <td><?= h($atendimentoProcedimentos->desconto) ?></td>
                <td><?= h($atendimentoProcedimentos->porc_desconto) ?></td>
                <td><?= h($atendimentoProcedimentos->valor_caixa) ?></td>
                <td><?= h($atendimentoProcedimentos->digitado) ?></td>
                <td><?= h($atendimentoProcedimentos->material) ?></td>
                <td><?= h($atendimentoProcedimentos->num_controle) ?></td>
                <td><?= h($atendimentoProcedimentos->medico_id) ?></td>
                <td><?= h($atendimentoProcedimentos->valor_matmed) ?></td>
                <td><?= h($atendimentoProcedimentos->documento_guia) ?></td>
                <td><?= h($atendimentoProcedimentos->situacao_id) ?></td>
                <td><?= h($atendimentoProcedimentos->user_id) ?></td>
                <td><?= h($atendimentoProcedimentos->created) ?></td>
                <td><?= h($atendimentoProcedimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'AtendimentoProcedimentos','action' => 'view', $atendimentoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'AtendimentoProcedimentos','action' => 'edit', $atendimentoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'AtendimentoProcedimentos','action' => 'delete', $atendimentoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Atendimentos') ?></h4>
        <?php if (!empty($user->atendimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Hora') ?></th>
                <th><?= __('Data') ?></th>
                <th><?= __('Observacao') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Unidade Id') ?></th>
                <th><?= __('Total Liquido') ?></th>
                <th><?= __('Desconto') ?></th>
                <th><?= __('Total Geral') ?></th>
                <th><?= __('Convenio Id') ?></th>
                <th><?= __('Total Pagoato') ?></th>
                <th><?= __('Tipoatendimento Id') ?></th>
                <th><?= __('Num Ficha Externa') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->atendimentos as $atendimentos): ?>
            <tr>
                <td><?= h($atendimentos->id) ?></td>
                <td><?= h($atendimentos->hora) ?></td>
                <td><?= h($atendimentos->data) ?></td>
                <td><?= h($atendimentos->observacao) ?></td>
                <td><?= h($atendimentos->cliente_id) ?></td>
                <td><?= h($atendimentos->unidade_id) ?></td>
                <td><?= h($atendimentos->total_liquido) ?></td>
                <td><?= h($atendimentos->desconto) ?></td>
                <td><?= h($atendimentos->total_geral) ?></td>
                <td><?= h($atendimentos->convenio_id) ?></td>
                <td><?= h($atendimentos->total_pagoato) ?></td>
                <td><?= h($atendimentos->tipoatendimento_id) ?></td>
                <td><?= h($atendimentos->num_ficha_externa) ?></td>
                <td><?= h($atendimentos->situacao_id) ?></td>
                <td><?= h($atendimentos->user_id) ?></td>
                <td><?= h($atendimentos->created) ?></td>
                <td><?= h($atendimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Atendimentos','action' => 'view', $atendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Atendimentos','action' => 'edit', $atendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Atendimentos','action' => 'delete', $atendimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Cliente Anexos') ?></h4>
        <?php if (!empty($user->cliente_anexos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Tipoanexo Id') ?></th>
                <th><?= __('Anexo') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->cliente_anexos as $clienteAnexos): ?>
            <tr>
                <td><?= h($clienteAnexos->id) ?></td>
                <td><?= h($clienteAnexos->cliente_id) ?></td>
                <td><?= h($clienteAnexos->tipoanexo_id) ?></td>
                <td><?= h($clienteAnexos->anexo) ?></td>
                <td><?= h($clienteAnexos->user_id) ?></td>
                <td><?= h($clienteAnexos->situacao_id) ?></td>
                <td><?= h($clienteAnexos->created) ?></td>
                <td><?= h($clienteAnexos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'ClienteAnexos','action' => 'view', $clienteAnexos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'ClienteAnexos','action' => 'edit', $clienteAnexos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'ClienteAnexos','action' => 'delete', $clienteAnexos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Cliente Documentos') ?></h4>
        <?php if (!empty($user->cliente_documentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Tipo Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->cliente_documentos as $clienteDocumentos): ?>
            <tr>
                <td><?= h($clienteDocumentos->id) ?></td>
                <td><?= h($clienteDocumentos->cliente_id) ?></td>
                <td><?= h($clienteDocumentos->user_id) ?></td>
                <td><?= h($clienteDocumentos->descricao) ?></td>
                <td><?= h($clienteDocumentos->situacao_id) ?></td>
                <td><?= h($clienteDocumentos->tipo_id) ?></td>
                <td><?= h($clienteDocumentos->created) ?></td>
                <td><?= h($clienteDocumentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'ClienteDocumentos','action' => 'view', $clienteDocumentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'ClienteDocumentos','action' => 'edit', $clienteDocumentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'ClienteDocumentos','action' => 'delete', $clienteDocumentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Cliente Historicos') ?></h4>
        <?php if (!empty($user->cliente_historicos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Tipohistoria Id') ?></th>
                <th><?= __('Descricao') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->cliente_historicos as $clienteHistoricos): ?>
            <tr>
                <td><?= h($clienteHistoricos->id) ?></td>
                <td><?= h($clienteHistoricos->cliente_id) ?></td>
                <td><?= h($clienteHistoricos->tipohistoria_id) ?></td>
                <td><?= h($clienteHistoricos->descricao) ?></td>
                <td><?= h($clienteHistoricos->situacao_id) ?></td>
                <td><?= h($clienteHistoricos->user_id) ?></td>
                <td><?= h($clienteHistoricos->created) ?></td>
                <td><?= h($clienteHistoricos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'ClienteHistoricos','action' => 'view', $clienteHistoricos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'ClienteHistoricos','action' => 'edit', $clienteHistoricos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'ClienteHistoricos','action' => 'delete', $clienteHistoricos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Cliente Responsaveis') ?></h4>
        <?php if (!empty($user->cliente_responsaveis)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Grau Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Nascionalidade') ?></th>
                <th><?= __('Estadocivil Id') ?></th>
                <th><?= __('Profissao') ?></th>
                <th><?= __('Rg') ?></th>
                <th><?= __('Cpf') ?></th>
                <th><?= __('Cep') ?></th>
                <th><?= __('Endereco') ?></th>
                <th><?= __('Numero') ?></th>
                <th><?= __('Complemento') ?></th>
                <th><?= __('Cidade') ?></th>
                <th><?= __('Estado') ?></th>
                <th><?= __('Telefone') ?></th>
                <th><?= __('Celular') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->cliente_responsaveis as $clienteResponsaveis): ?>
            <tr>
                <td><?= h($clienteResponsaveis->id) ?></td>
                <td><?= h($clienteResponsaveis->cliente_id) ?></td>
                <td><?= h($clienteResponsaveis->grau_id) ?></td>
                <td><?= h($clienteResponsaveis->nome) ?></td>
                <td><?= h($clienteResponsaveis->nascionalidade) ?></td>
                <td><?= h($clienteResponsaveis->estadocivil_id) ?></td>
                <td><?= h($clienteResponsaveis->profissao) ?></td>
                <td><?= h($clienteResponsaveis->rg) ?></td>
                <td><?= h($clienteResponsaveis->cpf) ?></td>
                <td><?= h($clienteResponsaveis->cep) ?></td>
                <td><?= h($clienteResponsaveis->endereco) ?></td>
                <td><?= h($clienteResponsaveis->numero) ?></td>
                <td><?= h($clienteResponsaveis->complemento) ?></td>
                <td><?= h($clienteResponsaveis->cidade) ?></td>
                <td><?= h($clienteResponsaveis->estado) ?></td>
                <td><?= h($clienteResponsaveis->telefone) ?></td>
                <td><?= h($clienteResponsaveis->celular) ?></td>
                <td><?= h($clienteResponsaveis->email) ?></td>
                <td><?= h($clienteResponsaveis->situacao_id) ?></td>
                <td><?= h($clienteResponsaveis->user_id) ?></td>
                <td><?= h($clienteResponsaveis->created) ?></td>
                <td><?= h($clienteResponsaveis->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'ClienteResponsaveis','action' => 'view', $clienteResponsaveis->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'ClienteResponsaveis','action' => 'edit', $clienteResponsaveis->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'ClienteResponsaveis','action' => 'delete', $clienteResponsaveis->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Clientes') ?></h4>
        <?php if (!empty($user->clientes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Sexo') ?></th>
                <th><?= __('Nascimento') ?></th>
                <th><?= __('Cpf') ?></th>
                <th><?= __('Rg') ?></th>
                <th><?= __('Telefone') ?></th>
                <th><?= __('Celular') ?></th>
                <th><?= __('Comercial') ?></th>
                <th><?= __('Cep') ?></th>
                <th><?= __('Endereco') ?></th>
                <th><?= __('Numero') ?></th>
                <th><?= __('Complemento') ?></th>
                <th><?= __('Bairro') ?></th>
                <th><?= __('Cidade') ?></th>
                <th><?= __('Estado') ?></th>
                <th><?= __('Cor') ?></th>
                <th><?= __('Profissao') ?></th>
                <th><?= __('Matricula') ?></th>
                <th><?= __('Cod Municipio') ?></th>
                <th><?= __('Validate Carteira') ?></th>
                <th><?= __('Medico Id') ?></th>
                <th><?= __('Convenio Id') ?></th>
                <th><?= __('Nascionalidade') ?></th>
                <th><?= __('Naturalidade') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Nome Mae') ?></th>
                <th><?= __('Nome Pai') ?></th>
                <th><?= __('Estadocivil Id') ?></th>
                <th><?= __('Indicacao') ?></th>
                <th><?= __('Login') ?></th>
                <th><?= __('Password') ?></th>
                <th><?= __('Bloqueado') ?></th>
                <th><?= __('Observacao') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->clientes as $clientes): ?>
            <tr>
                <td><?= h($clientes->id) ?></td>
                <td><?= h($clientes->nome) ?></td>
                <td><?= h($clientes->sexo) ?></td>
                <td><?= h($clientes->nascimento) ?></td>
                <td><?= h($clientes->cpf) ?></td>
                <td><?= h($clientes->rg) ?></td>
                <td><?= h($clientes->telefone) ?></td>
                <td><?= h($clientes->celular) ?></td>
                <td><?= h($clientes->comercial) ?></td>
                <td><?= h($clientes->cep) ?></td>
                <td><?= h($clientes->endereco) ?></td>
                <td><?= h($clientes->numero) ?></td>
                <td><?= h($clientes->complemento) ?></td>
                <td><?= h($clientes->bairro) ?></td>
                <td><?= h($clientes->cidade) ?></td>
                <td><?= h($clientes->estado) ?></td>
                <td><?= h($clientes->cor) ?></td>
                <td><?= h($clientes->profissao) ?></td>
                <td><?= h($clientes->matricula) ?></td>
                <td><?= h($clientes->cod_municipio) ?></td>
                <td><?= h($clientes->validate_carteira) ?></td>
                <td><?= h($clientes->medico_id) ?></td>
                <td><?= h($clientes->convenio_id) ?></td>
                <td><?= h($clientes->nascionalidade) ?></td>
                <td><?= h($clientes->naturalidade) ?></td>
                <td><?= h($clientes->email) ?></td>
                <td><?= h($clientes->nome_mae) ?></td>
                <td><?= h($clientes->nome_pai) ?></td>
                <td><?= h($clientes->estadocivil_id) ?></td>
                <td><?= h($clientes->indicacao) ?></td>
                <td><?= h($clientes->login) ?></td>
                <td><?= h($clientes->password) ?></td>
                <td><?= h($clientes->bloqueado) ?></td>
                <td><?= h($clientes->observacao) ?></td>
                <td><?= h($clientes->situacao_id) ?></td>
                <td><?= h($clientes->user_id) ?></td>
                <td><?= h($clientes->created) ?></td>
                <td><?= h($clientes->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Clientes','action' => 'view', $clientes->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Clientes','action' => 'edit', $clientes->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Clientes','action' => 'delete', $clientes->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Convenios') ?></h4>
        <?php if (!empty($user->convenios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Tipo Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->convenios as $convenios): ?>
            <tr>
                <td><?= h($convenios->id) ?></td>
                <td><?= h($convenios->nome) ?></td>
                <td><?= h($convenios->tipo_id) ?></td>
                <td><?= h($convenios->situacao_id) ?></td>
                <td><?= h($convenios->user_id) ?></td>
                <td><?= h($convenios->created) ?></td>
                <td><?= h($convenios->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Convenios','action' => 'view', $convenios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Convenios','action' => 'edit', $convenios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Convenios','action' => 'delete', $convenios->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Estado Civis') ?></h4>
        <?php if (!empty($user->estado_civis)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->estado_civis as $estadoCivis): ?>
            <tr>
                <td><?= h($estadoCivis->id) ?></td>
                <td><?= h($estadoCivis->nome) ?></td>
                <td><?= h($estadoCivis->situacao_id) ?></td>
                <td><?= h($estadoCivis->user_id) ?></td>
                <td><?= h($estadoCivis->created) ?></td>
                <td><?= h($estadoCivis->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'EstadoCivis','action' => 'view', $estadoCivis->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'EstadoCivis','action' => 'edit', $estadoCivis->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'EstadoCivis','action' => 'delete', $estadoCivis->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Grau Parentescos') ?></h4>
        <?php if (!empty($user->grau_parentescos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->grau_parentescos as $grauParentescos): ?>
            <tr>
                <td><?= h($grauParentescos->id) ?></td>
                <td><?= h($grauParentescos->nome) ?></td>
                <td><?= h($grauParentescos->situacao_id) ?></td>
                <td><?= h($grauParentescos->user_id) ?></td>
                <td><?= h($grauParentescos->created) ?></td>
                <td><?= h($grauParentescos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'GrauParentescos','action' => 'view', $grauParentescos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'GrauParentescos','action' => 'edit', $grauParentescos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'GrauParentescos','action' => 'delete', $grauParentescos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Grupo Procedimentos') ?></h4>
        <?php if (!empty($user->grupo_procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->grupo_procedimentos as $grupoProcedimentos): ?>
            <tr>
                <td><?= h($grupoProcedimentos->id) ?></td>
                <td><?= h($grupoProcedimentos->nome) ?></td>
                <td><?= h($grupoProcedimentos->situacao_id) ?></td>
                <td><?= h($grupoProcedimentos->user_id) ?></td>
                <td><?= h($grupoProcedimentos->created) ?></td>
                <td><?= h($grupoProcedimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'GrupoProcedimentos','action' => 'view', $grupoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'GrupoProcedimentos','action' => 'edit', $grupoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'GrupoProcedimentos','action' => 'delete', $grupoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Medico Responsaveis') ?></h4>
        <?php if (!empty($user->medico_responsaveis)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Conselho') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Sigla') ?></th>
                <th><?= __('Nome Laudo') ?></th>
                <th><?= __('Conselho Laudo') ?></th>
                <th><?= __('Cpf') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->medico_responsaveis as $medicoResponsaveis): ?>
            <tr>
                <td><?= h($medicoResponsaveis->id) ?></td>
                <td><?= h($medicoResponsaveis->conselho) ?></td>
                <td><?= h($medicoResponsaveis->nome) ?></td>
                <td><?= h($medicoResponsaveis->sigla) ?></td>
                <td><?= h($medicoResponsaveis->nome_laudo) ?></td>
                <td><?= h($medicoResponsaveis->conselho_laudo) ?></td>
                <td><?= h($medicoResponsaveis->cpf) ?></td>
                <td><?= h($medicoResponsaveis->situacao_id) ?></td>
                <td><?= h($medicoResponsaveis->user_id) ?></td>
                <td><?= h($medicoResponsaveis->created) ?></td>
                <td><?= h($medicoResponsaveis->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'MedicoResponsaveis','action' => 'view', $medicoResponsaveis->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'MedicoResponsaveis','action' => 'edit', $medicoResponsaveis->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'MedicoResponsaveis','action' => 'delete', $medicoResponsaveis->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Preco Procedimentos') ?></h4>
        <?php if (!empty($user->preco_procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Convenio Id') ?></th>
                <th><?= __('Procedimento Id') ?></th>
                <th><?= __('Valor Faturar') ?></th>
                <th><?= __('Valor Particular') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->preco_procedimentos as $precoProcedimentos): ?>
            <tr>
                <td><?= h($precoProcedimentos->id) ?></td>
                <td><?= h($precoProcedimentos->convenio_id) ?></td>
                <td><?= h($precoProcedimentos->procedimento_id) ?></td>
                <td><?= h($precoProcedimentos->valor_faturar) ?></td>
                <td><?= h($precoProcedimentos->valor_particular) ?></td>
                <td><?= h($precoProcedimentos->user_id) ?></td>
                <td><?= h($precoProcedimentos->created) ?></td>
                <td><?= h($precoProcedimentos->modified) ?></td>
                <td><?= h($precoProcedimentos->situacao_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'PrecoProcedimentos','action' => 'view', $precoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'PrecoProcedimentos','action' => 'edit', $precoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'PrecoProcedimentos','action' => 'delete', $precoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Procedimentos') ?></h4>
        <?php if (!empty($user->procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Grupo Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->procedimentos as $procedimentos): ?>
            <tr>
                <td><?= h($procedimentos->id) ?></td>
                <td><?= h($procedimentos->nome) ?></td>
                <td><?= h($procedimentos->grupo_id) ?></td>
                <td><?= h($procedimentos->user_id) ?></td>
                <td><?= h($procedimentos->situacao_id) ?></td>
                <td><?= h($procedimentos->created) ?></td>
                <td><?= h($procedimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Procedimentos','action' => 'view', $procedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Procedimentos','action' => 'edit', $procedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Procedimentos','action' => 'delete', $procedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tipo Anexos') ?></h4>
        <?php if (!empty($user->tipo_anexos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->tipo_anexos as $tipoAnexos): ?>
            <tr>
                <td><?= h($tipoAnexos->id) ?></td>
                <td><?= h($tipoAnexos->nome) ?></td>
                <td><?= h($tipoAnexos->user_id) ?></td>
                <td><?= h($tipoAnexos->situacao_id) ?></td>
                <td><?= h($tipoAnexos->created) ?></td>
                <td><?= h($tipoAnexos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'TipoAnexos','action' => 'view', $tipoAnexos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'TipoAnexos','action' => 'edit', $tipoAnexos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'TipoAnexos','action' => 'delete', $tipoAnexos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tipo Atendimentos') ?></h4>
        <?php if (!empty($user->tipo_atendimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->tipo_atendimentos as $tipoAtendimentos): ?>
            <tr>
                <td><?= h($tipoAtendimentos->id) ?></td>
                <td><?= h($tipoAtendimentos->nome) ?></td>
                <td><?= h($tipoAtendimentos->situacao_id) ?></td>
                <td><?= h($tipoAtendimentos->user_id) ?></td>
                <td><?= h($tipoAtendimentos->created) ?></td>
                <td><?= h($tipoAtendimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'TipoAtendimentos','action' => 'view', $tipoAtendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'TipoAtendimentos','action' => 'edit', $tipoAtendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'TipoAtendimentos','action' => 'delete', $tipoAtendimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tipo Convenios') ?></h4>
        <?php if (!empty($user->tipo_convenios)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->tipo_convenios as $tipoConvenios): ?>
            <tr>
                <td><?= h($tipoConvenios->id) ?></td>
                <td><?= h($tipoConvenios->nome) ?></td>
                <td><?= h($tipoConvenios->user_id) ?></td>
                <td><?= h($tipoConvenios->situacao_id) ?></td>
                <td><?= h($tipoConvenios->created) ?></td>
                <td><?= h($tipoConvenios->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'TipoConvenios','action' => 'view', $tipoConvenios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'TipoConvenios','action' => 'edit', $tipoConvenios->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'TipoConvenios','action' => 'delete', $tipoConvenios->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tipo Documentos') ?></h4>
        <?php if (!empty($user->tipo_documentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->tipo_documentos as $tipoDocumentos): ?>
            <tr>
                <td><?= h($tipoDocumentos->id) ?></td>
                <td><?= h($tipoDocumentos->nome) ?></td>
                <td><?= h($tipoDocumentos->situacao_id) ?></td>
                <td><?= h($tipoDocumentos->user_id) ?></td>
                <td><?= h($tipoDocumentos->created) ?></td>
                <td><?= h($tipoDocumentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'TipoDocumentos','action' => 'view', $tipoDocumentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'TipoDocumentos','action' => 'edit', $tipoDocumentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'TipoDocumentos','action' => 'delete', $tipoDocumentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tipo Historias') ?></h4>
        <?php if (!empty($user->tipo_historias)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->tipo_historias as $tipoHistorias): ?>
            <tr>
                <td><?= h($tipoHistorias->id) ?></td>
                <td><?= h($tipoHistorias->nome) ?></td>
                <td><?= h($tipoHistorias->user_id) ?></td>
                <td><?= h($tipoHistorias->situacao_id) ?></td>
                <td><?= h($tipoHistorias->created) ?></td>
                <td><?= h($tipoHistorias->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'TipoHistorias','action' => 'view', $tipoHistorias->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'TipoHistorias','action' => 'edit', $tipoHistorias->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'TipoHistorias','action' => 'delete', $tipoHistorias->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Unidades') ?></h4>
        <?php if (!empty($user->unidades)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nome') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->unidades as $unidades): ?>
            <tr>
                <td><?= h($unidades->id) ?></td>
                <td><?= h($unidades->nome) ?></td>
                <td><?= h($unidades->situacao_id) ?></td>
                <td><?= h($unidades->user_id) ?></td>
                <td><?= h($unidades->created) ?></td>
                <td><?= h($unidades->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Unidades','action' => 'view', $unidades->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Unidades','action' => 'edit', $unidades->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Unidades','action' => 'delete', $unidades->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $user->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

