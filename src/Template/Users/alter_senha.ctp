<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cadastro</h2>
        <ol class="breadcrumb">
            <li>Usuários</li>
            <li class="active">
                <strong> Alterar Senha
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>

</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="users form">
                        <?= $this->Form->create($user,['id'=>'form-senhas']) ?>
                        <fieldset>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('password', ['label' => 'Senha','required'=>'required','value'=>'']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('confirmar',['type'=>'password','label'=>'Confirmar Senha','required'=>'required']);
                            echo "</div>";
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary','onclick'=>'Checar_senhas(\'#form-senhas\')','type'=>'button']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

