<div>
    <?= $this->Form->create() ?>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" name="login" id="login" placeholder="Login" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" name="password" id="password" class="form-control" placeholder="Senha" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <input type="hidden" name="mobile" id="mobile" />
        <button type="submit" class="btn btn-primary block full-width m-b">Logar</button>
    <?= $this->Form->end() ?>
</div>
