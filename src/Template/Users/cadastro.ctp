<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Usuário</h2>
        <ol class="breadcrumb">
            <li>Painel</li>
            <li class="active">
                <strong>Cadastro</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="col-md-6">
                        <div class="ibox float-e-margins panel">
                            <div class="ibox-title">
                                <h5>Detalhes Conta</h5>
                            </div>
                            <div>
                                <div class="ibox-content no-padding border-left-right">
                                    <div class="ibox-content profile-content">
                                        <?php if (empty($user->caminho)) {
                                            echo $this->Html->image('content/img/empty-avatar.png', ['alt' => $user->nome, 'class' => 'img-responsive', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Alterar Imagem', 'id' => 'user-image']);
                                        } else {
                                            echo $this->Html->image('/files/users/caminho/' . $user->caminho_dir . '/' . $user->caminho, ['class' => 'img-responsive', 'title' => $user->nome, 'alt' => $user->nome, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'id' => 'user-image']);
                                        } ?>
                                    </div>
                                </div>

                                <div class="ibox-content profile-content">
                                    <h4>Nome:
                                        <strong>
                                            <?php echo $this->Html->link($user->nome, '#', ['class' => 'edit_local', 'data-type' => 'text', 'data-controller' => 'users', 'data-param' => 'text', 'data-title' => 'nome', 'data-pk' => $user->id, 'data-value' => $user->nome, 'listen' => 'f', 'escape' => false]) ?>
                                        </strong>
                                    </h4>
                                    <p><i class="fa fa-map-marker"></i> Login: <?php echo $user->login; ?></p>
                                    <p>
                                        <i class="fa fa-group"></i> Grupo Usuário:
                                        <?php echo $this->Html->link($user->grupo_usuario->nome, '#', ['class' => 'edit_local', 'data-type' => 'select2', 'data-controller' => 'users', 'data-param' => 'select', 'data-title' => 'grupo_id', 'data-find' => 'GrupoUsuarios', 'data-pk' => $user->id, 'data-value' => $user->grupo_usuario->nome, 'listen' => 'f', 'escape' => false]) ?>

                                    </p>
                                    <div class="user-button">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <?php echo $this->Html->link('<i class="fa fa-key"></i> Alterar Senha', ['controller' => 'Users', 'action' => 'alter-senha', $user->id, '?' => ['first' => 1]], ['class' => 'btn btn-primary btn-sm btn-block', 'data-toggle' => 'modal', 'escape' => false]) ?>
                                            </div>
                                            <div class="col-md-6">
                                                <?php echo $this->Html->link('<i class="fa fa-image"></i> Alterar Imagem', ['controller' => 'Users', 'action' => 'alter-image', $user->id, '?' => ['first' => 1]], ['class' => 'btn btn-info btn-sm btn-block', 'data-toggle' => 'modal', 'escape' => false]) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ibox float-e-margins panel">
                            <div class="ibox-title">
                                <h5>Dados Complementares</h5>
                            </div>
                            <div>
                                <div class="ibox-content no-padding border-left-right ">
                                    <div class="ibox-content profile-content">
                                        <h4> Código:
                                            <?= $user->id ?>
                                        </h4>
                                        <p>
                                            <i class="fa fa-envelope"></i> E-mail:
                                            <?php echo $this->Html->link($user->email, '#', ['class' => 'edit_local', 'data-type' => 'text', 'data-controller' => 'users', 'data-param' => 'text', 'data-title' => 'email', 'data-pk' => $user->id, 'data-value' => $user->email, 'listen' => 'f', 'escape' => false]) ?>
                                        </p>

                                        <p>
                                            <i class="fa fa-clock-o"></i> Situação Cadastro:
                                            <?php echo $this->Html->link($user->situacao_cadastro->nome, '#', ['class' => 'edit_local', 'data-type' => 'select2', 'data-controller' => 'users', 'data-param' => 'select', 'data-title' => 'situacao_id', 'data-find' => 'SituacaoCadastros', 'data-pk' => $user->id, 'data-value' => $user->situacao_cadastro->nome, 'listen' => 'f', 'escape' => false]) ?>
                                        </p>
                                        <p><i class="fa fa-calendar"></i> Data Cadastro:
                                            <strong><?= $user->created ?></strong></p>
                                        <p><i class="fa fa-calendar-o"></i> Última Atualização:
                                            <strong><?= $user->modified ?></strong></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

