<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2><?= __('Usuários'); ?></h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">

        <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create($users, ['type' => 'get']) ?>
                        <div class="col-md-4">
                            <?= $this->Form->input('nome', ['required' => false]); ?>
                        </div>
                        <div class="col-md-12 fill-content-search">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            <?= $this->Html->link('<i class="fa fa-plus"></i>', ['action' => 'add'], ['class' => 'btn btn-primary', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo', 'target' => '_blank', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Users','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Users') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id',['label'=>'Código']) ?></th>
                <th><?= $this->Paginator->sort('grupo_id') ?></th>
                <th><?= $this->Paginator->sort('nome') ?></th>
                <th><?= $this->Paginator->sort('login') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('atualizar') ?></th>
                <th><?= $this->Paginator->sort('codigo_medico',['label'=>'Código Profissional']) ?></th>
                <th><?= $this->Paginator->sort('codigo_agenda',['label'=>'Código Agenda']) ?></th>
                <th><?= $this->Paginator->sort('modified',['label' => 'Dt. Modificação']) ?></th>
                <th class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= $user->has('grupo_usuario') ? $this->Html->link($user->grupo_usuario->nome, ['controller' => 'GrupoUsuarios', 'action' => 'view', $user->grupo_usuario->id]) : '' ?></td>
                <td><?= h($user->nome) ?></td>
                <td><?= h($user->login) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= h($user->atualizar) ?></td>
                <td><?= $user->has('medico_responsavei') ? $user->medico_responsavei->nome :''; ?></td>
                <td><?= $user->has('grupo_agenda') ? $user->grupo_agenda->nome :''; ?></td>
                <td><?= !empty($user->modified) ? date_format($user->modified, 'd/m/Y H:i:s') : '' ?></td>
                <td class="actions">
                    <div class="dropdown">
                        <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown">
                            <span class="fa fa-list"></span> Opções
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <?= $this->Html->link('Relatórios - Caixa Atendimento', ['action' => 'habilitaCaixaPorUsuario', $user->id, '?' => ['ajax' => true, 'first' => 1]], ['data-toggle' => 'modal', 'data-target' => '#modal_lg', 'escape' => false]); ?>
                            </li>
                            <li>
                                <?= $this->Html->link('Editar', ['action' => 'edit', $user->id], ['escape' => false]); ?>
                            </li>
                            <li>
                                <?= $this->Html->link('Deletar', 'javascript:void(0)', ['onclick'=>'Deletar(\'Users\','.$user->id.')', 'escape' => false]) ?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div style="text-align:center">
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>