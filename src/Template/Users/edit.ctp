<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2><?= __('Usuário - Editar'); ?></h2>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="users form">
                    <?= $this->Form->create($user, ['type' => 'file', 'id' => 'form-edituser']) ?>
                    <?php require_once('form.ctp'); ?>
                    <div class="col-md-12 text-right">
                        <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'formanexos("form-edituser", "users", "index")']); ?>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>
</div>

