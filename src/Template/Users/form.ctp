<div class='col-md-6'>
    <?php echo $this->Form->input('nome'); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('login'); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('email'); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('grupo_id', ['options' => $grupoUsuarios, 'id'=>'grupo_id_users']); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('new_password', ['label' => __('Nova senha'), 'placeholder' => 'Senha', 'type' => 'password']); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('confirm_password', ['label' => __('Confirmar senha'), 'placeholder' => 'Confirmar senha', 'type' => 'password']); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('codigo_medico', ['options' => $medicoResponsaveis, 'label' => 'Código Profissional','empty'=>'Selecione']); ?>
</div>
<div class='col-md-6'>
    <?php echo $this->Form->input('codigo_agenda', ['options' => $grupoAgendas, 'label' => 'Código Agenda','empty'=>'Selecione']); ?>
</div>
<div class="col-md-3">
    <div id='div_caixa'><?php echo $this->Form->input('caixa_outros_usuarios', ['id' => 'caixa_usuarios_input', 'label' => 'Permite Puxar Caixas Individuais','type' => 'select', 'options' => [0 => 'Não', 1 => 'Sim']]); ?></div>
</div>
<div class="col-md-3" style='margin-top: 20px;'>
    <?php echo $this->Form->input('chat_module', ['label' => ' Utiliza chat?', 'type' => 'checkbox']); ?>
</div>
<div class="col-md-4" style='margin-top: 20px;'>
    <?php echo $this->Form->input('agenda_reserva_liberacao', ['label' => ' Agenda - Edita/Libera horários reservados', 'type' => 'checkbox']); ?>
</div>
<div class='col-md-2' style='margin-top: 20px;'>
    <?php echo $this->Form->input('atualizar'); ?>
</div>
<div class="clearfix"></div>
<div class='col-md-3'>
    <?php echo $this->Form->input('limite_desconto', ['label' => '% Desconto', 'type' => 'text', 'mask' => 'money', 'disabled' => ($user_logado['grupo_id'] == 1) ?false:true]); ?>
</div>

<div class="col-md-12">
    <div class="form-group">
        <?php
        $link_img = '';
        if(!empty($user->caminho_dir) && !empty($user->caminho_dir)){
            $link_img = 'img/users/caminho/'.$user->caminho_dir.'/'.$user->caminho;
        }
        ?>
        <label for="files">Adicionar foto</label>
        <input id="files" type="file" name="caminho" class="input-file-edit-user"
                link-img="<?=$link_img?>"
                descricao-img="<?= $user->caminho ?>"
                controller-img="users" img-id="<?= $user->id ?>"
                data-overwrite-initial="false" data-allowed-file-extensions='["jpg", "gif", "png"]'>
    </div>
</div>

<div class="col-md-12">
    <div class="progress" style="display: none;">
        <div class="progress-bar progress-bar-striped active" role="progressbar"
                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            <span class="sr-only">0% Complete</span>
        </div>
    </div>
</div>