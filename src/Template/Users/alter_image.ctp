<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cadastro</h2>
        <ol class="breadcrumb">
            <li>Usuários</li>
            <li class="active">
                <strong> Alterar Senha
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>

</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="users form">
                        <?= $this->Form->create($user,['id'=>'form-image','type'=>'file']) ?>
                        <fieldset>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="files">Imagem</label>
                                    <input id="files" type="file" name="caminho" class="input-file-edit" link-img="/files/users/caminho/<?= $user->caminho_dir ?>/<?= $user->caminho ?>" descricao-img="<?= $user->nome ?>"
                                           controller-img="Users" img-id="<?= $user->id ?>" data-overwrite-initial="false" >
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="progress" style="display: none;">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                                         aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                        <span class="percent">0% Complete</span>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'return change_photo("form-image")']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

