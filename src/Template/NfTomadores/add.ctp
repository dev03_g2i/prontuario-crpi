<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Tomadores</h2>
            <ol class="breadcrumb">
                <li>Nf Tomadores</li>
                <li class="active">
                    <strong>
                                                Cadastrar Nf Tomadores
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="nfTomadores form">
                            <?= $this->Form->create($nfTomadore) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Nf Tomadore') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('razao_social'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('inscricao_municipal'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('cpfcnpj'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('doc_tomador_estrangeiro'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('tipo_logradouro_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('logradouro'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('numero'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('complemento'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('tipo_bairro_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('bairro'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('cidade_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('cep'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('email'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('ddd'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('telefone'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('texto_nf'); ?>
                                                                    </div>
                                                                            <div class='col-md-6'>
                                                                                                    <?=$this->Form->input('cliente_id', ['data'=>'select','controller'=>'clientes','action'=>'fill',  'empty' => 'selecione']); ?>
                                                                                             </div>
                                                                            <div class='col-md-6'>
                                                                            <?=$this->Form->input('responsavel_id'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('tipo_tomador'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

