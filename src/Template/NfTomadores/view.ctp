
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Nf Tomadores</h2>
        <ol class="breadcrumb">
            <li>Nf Tomadores</li>
            <li class="active">
                <strong>Litagem de Nf Tomadores</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Nf Tomadores</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Razao Social') ?></th>
                                <td><?= h($nfTomadore->razao_social) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Inscricao Municipal') ?></th>
                                <td><?= h($nfTomadore->inscricao_municipal) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cpfcnpj') ?></th>
                                <td><?= h($nfTomadore->cpfcnpj) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Doc Tomador Estrangeiro') ?></th>
                                <td><?= h($nfTomadore->doc_tomador_estrangeiro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Logradouro') ?></th>
                                <td><?= h($nfTomadore->logradouro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Complemento') ?></th>
                                <td><?= h($nfTomadore->complemento) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Bairro') ?></th>
                                <td><?= h($nfTomadore->bairro) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cep') ?></th>
                                <td><?= h($nfTomadore->cep) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Email') ?></th>
                                <td><?= h($nfTomadore->email) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Ddd') ?></th>
                                <td><?= h($nfTomadore->ddd) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Telefone') ?></th>
                                <td><?= h($nfTomadore->telefone) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Cliente') ?></th>
                                                                <td><?= $nfTomadore->has('cliente') ? $this->Html->link($nfTomadore->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $nfTomadore->cliente->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($nfTomadore->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tipo Logradouro Id') ?></th>
                                <td><?= $this->Number->format($nfTomadore->tipo_logradouro_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Numero') ?></th>
                                <td><?= $this->Number->format($nfTomadore->numero) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tipo Bairro Id') ?></th>
                                <td><?= $this->Number->format($nfTomadore->tipo_bairro_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Cidade Id') ?></th>
                                <td><?= $this->Number->format($nfTomadore->cidade_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Responsavel Id') ?></th>
                                <td><?= $this->Number->format($nfTomadore->responsavel_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Tipo Tomador') ?></th>
                                <td><?= $this->Number->format($nfTomadore->tipo_tomador) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3><?= __('Texto Nf') ?></h3>
                </div>
                <div class="ibox-content">
                    <?= $this->Text->autoParagraph(h($nfTomadore->texto_nf)); ?>
                </div>
            </div>
</div>
</div>
</div>
</div>


