<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Nf Tomadores</h2>
            <ol class="breadcrumb">
                <li>Nf Tomadores</li>
                <li class="active">
                    <strong>Listagem de Nf Tomadores</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('NfTomadore',['type'=>'get']) ?>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('razao_social',['name'=>'NfTomadores__razao_social']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('inscricao_municipal',['name'=>'NfTomadores__inscricao_municipal']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cpfcnpj',['name'=>'NfTomadores__cpfcnpj']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('doc_tomador_estrangeiro',['name'=>'NfTomadores__doc_tomador_estrangeiro']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tipo_logradouro_id',['name'=>'NfTomadores__tipo_logradouro_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('logradouro',['name'=>'NfTomadores__logradouro']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('numero',['name'=>'NfTomadores__numero']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('complemento',['name'=>'NfTomadores__complemento']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tipo_bairro_id',['name'=>'NfTomadores__tipo_bairro_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('bairro',['name'=>'NfTomadores__bairro']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cidade_id',['name'=>'NfTomadores__cidade_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('cep',['name'=>'NfTomadores__cep']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('email',['name'=>'NfTomadores__email']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('ddd',['name'=>'NfTomadores__ddd']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('telefone',['name'=>'NfTomadores__telefone']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                            <?= $this->Form->input('cliente_id', ['name'=>'NfTomadores__cliente_id','data'=>'select','controller'=>'clientes','action'=>'fill', 'empty' => 'Selecione']); ?>
                                        </div>
                                                                                <div class='col-md-4'>
                                                                                            <?=$this->Form->input('responsavel_id',['name'=>'NfTomadores__responsavel_id']); ?>
                                                                                    </div>
                                                                    <div class='col-md-4'>
                                                                                            <?=$this->Form->input('tipo_tomador',['name'=>'NfTomadores__tipo_tomador']); ?>
                                                                                    </div>
                                                        <div class="clearfix"></div>
                            <div class="col-md-4">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right btnAdd">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Nf Tomadores', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Nf Tomadores','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Nf Tomadores') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                                                                <th><?= $this->Paginator->sort('id') ?></th>
                                                                                <th><?= $this->Paginator->sort('razao_social') ?></th>
                                                                                <th><?= $this->Paginator->sort('inscricao_municipal') ?></th>
                                                                                <th><?= $this->Paginator->sort('cpfcnpj') ?></th>
                                                                                <th><?= $this->Paginator->sort('doc_tomador_estrangeiro') ?></th>
                                                                                <th><?= $this->Paginator->sort('tipo_logradouro_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('logradouro') ?></th>
                                                                                <th><?= $this->Paginator->sort('numero') ?></th>
                                                                                <th><?= $this->Paginator->sort('complemento') ?></th>
                                                                                <th><?= $this->Paginator->sort('tipo_bairro_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('bairro') ?></th>
                                                                                <th><?= $this->Paginator->sort('cidade_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('cep') ?></th>
                                                                                <th><?= $this->Paginator->sort('email') ?></th>
                                                                                <th><?= $this->Paginator->sort('ddd') ?></th>
                                                                                <th><?= $this->Paginator->sort('telefone') ?></th>
                                                                                <th><?= $this->Paginator->sort('cliente_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('responsavel_id') ?></th>
                                                                                <th><?= $this->Paginator->sort('tipo_tomador') ?></th>
                                                                                <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($nfTomadores as $nfTomadore): ?>
                                    <tr>
                                                                                <td><?= $this->Number->format($nfTomadore->id) ?></td>
                                                                                <td><?= h($nfTomadore->razao_social) ?></td>
                                                                                <td><?= h($nfTomadore->inscricao_municipal) ?></td>
                                                                                <td><?= h($nfTomadore->cpfcnpj) ?></td>
                                                                                <td><?= h($nfTomadore->doc_tomador_estrangeiro) ?></td>
                                                                                <td><?= $this->Number->format($nfTomadore->tipo_logradouro_id) ?></td>
                                                                                <td><?= h($nfTomadore->logradouro) ?></td>
                                                                                <td><?= $this->Number->format($nfTomadore->numero) ?></td>
                                                                                <td><?= h($nfTomadore->complemento) ?></td>
                                                                                <td><?= $this->Number->format($nfTomadore->tipo_bairro_id) ?></td>
                                                                                <td><?= h($nfTomadore->bairro) ?></td>
                                                                                <td><?= $this->Number->format($nfTomadore->cidade_id) ?></td>
                                                                                <td><?= h($nfTomadore->cep) ?></td>
                                                                                <td><?= h($nfTomadore->email) ?></td>
                                                                                <td><?= h($nfTomadore->ddd) ?></td>
                                                                                <td><?= h($nfTomadore->telefone) ?></td>
                                                                                <td><?= $nfTomadore->has('cliente') ? $this->Html->link($nfTomadore->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $nfTomadore->cliente->id]) : '' ?></td>
                                                                                <td><?= $this->Number->format($nfTomadore->responsavel_id) ?></td>
                                                                                <td><?= $this->Number->format($nfTomadore->tipo_tomador) ?></td>
                                                                                <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'nfTomadores','action' => 'view', $nfTomadore->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'nfTomadores','action' => 'edit', $nfTomadore->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                   <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'nfTomadores','action'=>'delete', $nfTomadore->id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
