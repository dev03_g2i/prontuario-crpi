<div class="wrapper  white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda</h2>
        <ol class="breadcrumb">
            <li>Agendas</li>
            <li class="active">
                <strong>Impressão de agenda</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Agendas', ['url' => ['action' => 'report'], 'target' => '_blank']) ?>

                        <div class="col-md-6">
                            <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>

                        <div class="col-md-6">
                            <?= $this->Form->input('fim', ['label' => 'Data Final', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?= $this->Form->input('grupo_id', ['label' => 'Agenda', 'name' => 'grupos[]', 'type' => 'select', 'empty' => 'Selecione', 'multiple', 'options' => $grupoAgendas, 'class' => 'select2']); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('periodo', ['label' => 'Periodo', 'type' => 'select', 'options' => $periodos]); ?>
                        </div>
                        <div class="col-md-12" style="margin-top: 5px; margin-bottom: 15px;">
                            <?= $this->Form->checkbox('horarios_utilizados', ['checked', 'value' => 1, 'hiddenField' => 2]); ?>
                            <label style="margin-left: 10px">Imprimir Apenas Horários Utilizados</label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <?= $this->Form->input('relatorio_agenda', ['label' => 'Relatório', 'type' => 'select',
                                'options' => $relatorios]); ?>
                        </div>
                        <div class="col-md-3" style="margin-top: 25px">
                            <?= $this->Form->button($this->Html->icon('print') . ' Gerar Relatório', ['type' => 'submit', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Gerar Relatório', 'escape' => false, 'listen' => 'f']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>