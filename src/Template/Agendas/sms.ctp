<div class="this-place">

    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>SMS</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <ul>
                        <li><b>Cliente Resposta: </b><?= $agenda->sms ?></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</div>