<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title"><h3>Lista de espera</h3></div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title"><h3>Filtros</h3></div>
            <div class="ibox-content">
                <div class="filtros ">
                    <?= $this->Form->create('Agendas',['method'=>'get']) ?>
                    <div class="col-md-3">
                        <?= $this->Form->input('nome_provisorio', ['label' => 'Paciente']); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('grupo_id', ['label' => 'Agenda', 'name' => 'grupos', 'type' => 'select', 'empty' => 'Selecione', 'options' => $grupoAgendas,'default'=>$grupo]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('tipo_id', ['label' => 'Tipo Agenda', 'name' => 'tipos', 'empty' => 'Selecione', 'options' => $tipoAgendas]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('situacao_agenda_id', ['label' => 'Situação Agenda', 'name' => 'situacoes', 'empty' => 'Selecione', 'options' => $situacaoAgendas]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('inicio', ['label' => 'Data Inicial', 'value'=>$this->Time->format($inicio,'dd/MM/Y'), 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>

                    <div class="col-md-3">
                        <?= $this->Form->input('fim', ['label' => 'Data Final', 'value'=>$this->Time->format($fim,'dd/MM/Y'),'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $this->Form->input('periodo', ['label' => 'Período', 'options'=>$periodos,'default'=>$periodo,'empty'=>'Selecione']); ?>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12 fill-content-search">
                        <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                        <?= $this->Form->button('',['class'=>'btn btn-primary m-r-sm numero_clientes','type'=>'button','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Qt.Pacientes'])?>
                    </div>
                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Agendas') ?></h5>

            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('inicio',['label'=>'Hora']) ?></th>
                                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                                <th><?= $this->Paginator->sort('procedimento_provisorio',['label'=>'Procedimento']) ?></th>
                                <th><?= $this->Paginator->sort('lembrete',['label'=>'']) ?></th>
                                <th><?= $this->Paginator->sort('cliente_id',['label'=>'Paciente']) ?></th>
                                <th><?= $this->Paginator->sort('convenio_provisorio',['label'=>'Convênio']) ?></th>
                                <th><?= $this->Paginator->sort('fone_provisorio',['label'=>'Telefone/Observações']) ?></th>
                                <th><?= $this->Paginator->sort('situacao_agenda_id',['label'=>'Situação']) ?></th>
                                <th><?= $this->Paginator->sort('chegada',['label'=>'Hora chegada']) ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody >
                            <?php foreach ($agendas as $agenda):
                                $cor_atendimento = $agenda->situacao_agenda->cor_atendimento;
                                ?>
                                <tr style="color: <?=$cor_atendimento ?>">
                                    <td class="color-links"><?= $this->Time->format($agenda->inicio, 'HH:mm') ?></td>
                                    <td class="color-links" style="background-color: <?=$agenda->tipo_agenda->cor_salaespera?> !important;"><?= $agenda->has('tipo_agenda') ? $agenda->tipo_agenda->nome : '' ?></td>
                                    <td class="color-links"><?= $agenda->procedimento_concat ?></td>
                                    <td class="color-links"><?= !empty($agenda->lembrete) ? $this->Html->link('<i class="fa fa-comment"></i>','javascript:void(0)',['escape' => false,'toggle'=>'popover','title'=>'Lembrete','data-content'=>$agenda->lembrete,'data-placement'=>'bottom']):'' ?></td>
                                    <td class="color-links"><?= $agenda->has('cliente') ? (($agenda->cliente->id!='-1') ? $this->Html->link($agenda->cliente->arquivo." - ".$agenda->cliente->nome,['controller'=>'clientes','action'=>'prontuario',$agenda->cliente_id,'?'=>['agenda'=>$agenda->id]],['style' => 'color:'.$cor_atendimento, 'data-toggle'=>'modal','data-target'=>'#modal_lg' ,'data-backdrop'=>'false']) : 'Prov: '.$agenda->nome_provisorio) : '' ?></td>
                                    <td class="color-links"><?= !empty($agenda->convenio_id) ? $agenda->convenio->nome : $agenda->convenio_provisorio ?></td>
                                    <td class="color-links"><?= (($agenda->has('cliente') && $agenda->cliente->id!='-1') ? $agenda->cliente->telefone."/".$agenda->cliente->celular : $agenda->fone_provisorio.'/'.$agenda->observacao ) ?></td>
                                    <td class="color-links"><?= $agenda->has('situacao_agenda') ? $agenda->situacao_agenda->nome : '' ?></td>
                                    <td class="color-links"><?= empty($agenda->chegada)?'':$agenda->chegada->format('d/m/Y H:i') ?></td>
                                    <td class="actions">
                                        <?php
                                        // situacao 1000 = Bloqueio
                                        if($agenda->situacao_agenda_id != 1000){
                                            echo $this->Html->link('<i class="fa fa-stethoscope"></i>', ['controller'=>'Clientes','action' => 'prontuario', $agenda->cliente_id,'?'=>['agenda'=>$agenda->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Prontuário', 'escape' => false, 'class' => 'btn btn-xs btn-info '.($agenda->cliente_id=='-1' ? 'disabled': ''),'data-toggle'=>'modal','data-target'=>'#modal_lg','data-backdrop'=>'false']);
                                            echo $this->Html->link('<i class="fa fa-pencil"></i>', ['action' => 'nedit', $agenda->id,'?'=>['first'=>1,'ajax'=>1]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary','data-toggle'=>'modal','data-target'=>'#modal_lg']);
                                            echo $this->Html->link('<i class="fa fa-check-square-o"></i>', 'javascript:void(0)', ['onclick' => 'FinalizarAgenda(' . $agenda->id . ',recarregar_tela)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Finalizar Atendimento', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']);
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


