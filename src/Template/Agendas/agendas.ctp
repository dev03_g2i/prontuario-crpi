<div class="this-place">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>Agenda - Lista</h3>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title"><h3>Filtros</h3></div>
                <div class="ibox-content">
                    <div class="filtros ">
                        <?= $this->Form->create('Agendas', ['method' => 'get', 'id' => 'fill-agendamentos']) ?>

                        <div class="col-md-3">
                            <?= $this->Form->input('nome_provisorio', ['label' => 'Paciente']); ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->input('grupo_id', ['label' => 'Agenda', /*'data-fill' => 'send',*/ 'name' => 'grupos', 'type' => 'select', 'empty' => 'Selecione', 'options' => $grupoAgendas, 'default' => $grupo]); ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->input('tipo_id', ['label' => 'Tipo Agenda', 'name' => 'tipos', 'empty' => 'Selecione', 'options' => $tipoAgendas]); ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->input('situacao_agenda_id', ['label' => 'Situação Agenda', 'name' => 'situacoes', 'empty' => 'Selecione', 'options' => $situacaoAgendas]); ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->input('periodo', ['label' => 'Período', 'options' => $periodos]); ?>
                        </div>
                        <div class="col-md-3">
                            <?= $this->Form->input('inicio', ['label' => 'Data', 'value' => $this->Time->format($inicio, 'dd/MM/Y'), 'type' => 'text', 'class' => "datepicker", 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                        </div>
                        <div class="col-md-3" style="margin-top: 23px">
                            <?= $this->Form->button('<i class="fa fa-angle-double-left"></i> <i class="fa fa-calendar"></i> ', ['class' => 'btn btn-default', 'type' => 'button', 'id' => 'ant-data', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Data Anterior']) ?>
                            <?= $this->Form->button('<i class="fa fa-calendar"></i> <i class="fa fa-angle-double-right"></i> ', ['class' => 'btn btn-default', 'type' => 'button', 'id' => 'prox-data', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Próxima Data']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 fill-content-search">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'btn-fill', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'btn-refresh', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                            <?= $this->Form->button('', ['class' => 'btn btn-primary m-r-sm clientes_agendas', 'type' => 'button', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Qt.Pacientes']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-4">
                            <h5><?= __('Agendas') ?></h5>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-3">
                                    <?= $this->Html->link('<i class="fa fa-clock-o"></i> Abrir Horários', ['action' => 'add-horarios', '?' => ['first' => 1, 'ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Abrir Horários', 'class' => 'btn btn-info ' . $this->Configuracao->showGeracaoHorarios(), 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Html->link('<i class="fa fa-calendar-o"></i> Visualizar Semana', [], ['default-view' => 'agendaWeek', 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Visualizar Semana', 'class' => 'btn btn-success visualizar-agenda', 'escape' => false, 'target' => '_blank']) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Html->link('<i class="fa fa-calendar"></i> Visualizar Mensal', [], ['default-view' => 'agendaMonth', 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Visualizar Mês', 'class' => 'btn btn-warning visualizar-agenda', 'escape' => false, 'target' => '_blank']) ?>
                                </div>
                                <div class="col-md-3">
                                    <?= $this->Html->link($this->Html->icon('plus') . ' Novo Agendamento', ['action' => 'new-add', '?' => ['first' => 1, 'ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar agenda', 'class' => 'btn btn-primary', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="text-right">
                        <p>
                            <?= $this->Html->link($this->Html->icon('plus') . ' Novo Agendamento', ['action' => 'new-add', '?' => ['first' => 1, 'ajax' => 1]], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar agenda', 'class' => 'btn btn-primary', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                        </p>
                    </div>-->
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th><?= $this->Paginator->sort('inicio', ['label' => 'Hora']) ?></th>
                                    <th><?= $this->Paginator->sort('inicio', ['label' => 'Data']) ?></th>
                                    <th><?= $this->Paginator->sort('grupo_id', ['label' => 'Agenda']) ?></th>
                                    <th><?= $this->Paginator->sort('tipo_id') ?></th>
                                    <th><?= $this->Paginator->sort('procedimento_provisorio', ['label' => 'Procedimento']) ?></th>
                                    <th><?= $this->Paginator->sort('lembrete', ['label' => '']) ?></th>
                                    <th><?= $this->Paginator->sort('cliente_id', ['label' => 'Paciente']) ?></th>
                                    <th><?= $this->Paginator->sort('idade') ?></th>
                                    <th><?= $this->Paginator->sort('convenio_provisorio', ['label' => 'Convênio']) ?></th>
                                    <th><?= $this->Paginator->sort('fone_provisorio', ['label' => 'Telefone/Observações']) ?></th>
                                    <th><?= $this->Paginator->sort('situacao_agenda_id', ['label' => 'Situação']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Última Interação']) ?></th>
                                    <th class="actions"><?= __('Opções') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($agendas as $agenda):
                                    $cor_atendimento = $agenda->situacao_agenda->cor_atendimento;
                                    ?>
                                    <tr>
                                        <td><?= $this->Sms->verificaSms($agenda->sms_situacao, $agenda->sms, $agenda->cliente_id); ?></td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $this->Time->format($agenda->inicio, 'HH:mm') ?></td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $this->Time->format($agenda->inicio, 'dd/MM/YY') ?></td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $agenda->has('grupo_agenda') ? $agenda->grupo_agenda->nome : '' ?></td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>; background-color: <?=$agenda->tipo_agenda->cor?> !important; "><?= $agenda->has('tipo_agenda') ? $agenda->tipo_agenda->nome : '' ?></td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $agenda->procedimento_concat ?></td>
                                        <td class="color-links" style="color: <?= $cor_atendimento ?>">
                                            <?= !empty($agenda->lembrete) ? $this->Html->link('<i class="fa fa-comment"></i>', '#Javascript:void(0)', ['escape' => false, 'toggle' => 'popover', 'title' => 'Lembrete', 'data-content' => $agenda->lembrete, 'data-placement' => 'bottom']) : '' ?>
                                        </td>
                                        <td class="color-links" style="color: <?= $cor_atendimento ?>">
                                            <?php
                                            if ($this->Convenio->verificaConvenioSistema($agenda->convenio_id) || $agenda->cliente->id != '-1') {
                                                echo $agenda->has('cliente') ? (($agenda->cliente->id != '-1') ? $this->Html->link($agenda->cliente->arquivo . " - " . $agenda->cliente->nome, ['controller' => 'clientes', 'action' => 'prontuario', $agenda->cliente_id, '?' => ['agenda' => $agenda->id]], ['style' => 'color:' . $cor_atendimento, 'data-toggle' => 'modal', 'data-target' => '#modal_lg', 'data-backdrop' => 'false']) : 'Prov: ' . $agenda->nome_provisorio) : '';
                                            }else{
                                                if($agenda->situacao_agenda_id == 1000)
                                                    echo $agenda->nome_provisorio;
                                            }
                                            ?>
                                        </td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $agenda->idade ?></td>
                                        <td class="color-links" style="color: <?= $cor_atendimento ?>">
                                            <?php
                                            if ($this->Convenio->verificaConvenioSistema($agenda->convenio_id)) {
                                                echo !empty($agenda->convenio_id) ? $agenda->convenio->nome : $agenda->convenio_provisorio;
                                            }
                                            ?>
                                        </td>
                                        <td class="color-links" style="color: <?= $cor_atendimento ?>">
                                            <?php
                                            if ($this->Convenio->verificaConvenioSistema($agenda->convenio_id)) {
                                                echo $agenda->has('cliente') ? (($agenda->cliente_id != -1) ? $agenda->cliente->telefone . "/" . $agenda->cliente->celular : $agenda->fone_provisorio . '/' . $agenda->observacao) : '';
                                            }
                                            ?>
                                        </td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $agenda->has('situacao_agenda') ? $agenda->situacao_agenda->nome : '' ?></td>
                                        <td class="color-links"
                                            style="color: <?= $cor_atendimento ?>"><?= $agenda->modified ?></td>
                                        <td class="actions">
                                            <div class="dropdown">
                                                <button class="btn btn-info btn-xs dropdown-toggle" type="button"
                                                        data-toggle="dropdown"><span class="fa fa-list"></span> Opções
                                                    <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-prontuario">
                                                    <li><?= $this->Html->link('<i class="fa fa-file-archive-o"></i> Visualizar', ['action' => 'view', $agenda->id], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?></li>
                                                    <li>
                                                        <?php if (!empty($this->Agenda->check_atendimento($agenda->id))) {
                                                            echo $this->Html->link('<i class="fa fa-print"></i> Ficha', ['controller' => 'Atendimentos', 'action' => 'report', $this->Agenda->check_atendimento($agenda->id)], ['escape' => false, 'target' => '_blank']);
                                                        } ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        if ($agenda->cliente_id == -1) {
                                                            echo $this->Html->link('<i class="fa fa-user"></i> Cadastrar Paciente', ['controller' => 'Clientes', 'action' => 'add', '?' => ['agenda' => $agenda->id, 'first' => 1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']);
                                                        } else {
                                                            echo $this->Html->link('<i class="fa fa-user"></i> Cadastro do Paciente', ['controller' => 'Clientes', 'action' => 'edit', $agenda->cliente_id, '?' => ['agenda' => $agenda->id, 'first' => 1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_2']);
                                                            echo $this->Html->link('<i class="fa fa-stethoscope"></i> Prontuário', ['controller' => 'Clientes', 'action' => 'prontuario', $agenda->cliente_id, '?' => ['agenda' => $agenda->id]], ['escape' => false, 'class' => '' . ($agenda->cliente_id == '-1' ? 'disabled' : ''), 'data-toggle' => 'modal', 'data-target' => '#modal_lg']);
                                                        }
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?= $this->Html->link('<i class="fa fa-pencil"></i> Editar', ['action' => 'nedit', $agenda->id, '?' => ['first' => 1, 'ajax' => 1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']) ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        if (!empty($this->Agenda->check_atendimento($agenda->id))) {
                                                            //echo $this->Html->link('<i class="fa fa-user-md"></i> Editar atendimento', ['controller' => 'Atendimentos', 'action' => 'editar',$this->Agenda->check_atendimento($agenda->id), '?' => ['agenda' => $agenda->id, 'first' => 1, 'ajax' => 1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg']);
                                                            echo $this->Html->link('<i class="fa fa-user-md"></i> Editar atendimento', ['controller' => 'Atendimentos', 'action' => 'edit', $this->Agenda->check_atendimento($agenda->id)], ['escape' => false, 'target' => '_blank', 'listen' => 'f']);
                                                        } else {
                                                            if ($agenda->cliente_id == '-1') {
                                                                echo $this->Html->link('<i class="fa fa-user-md"></i> Deseja abrir atendimento?', 'javascript:void(0)', ['onclick' => 'required(\'Atenção\',\'Não é permitido abrir agendamento para pacientes não cadastrados!\')', 'escape' => false, 'listen' => 'f']);
                                                            } else {
                                                                if (empty($agenda->convenio_id)) {
                                                                    echo $this->Html->link('<i class="fa fa-user-md"></i> Deseja abrir atendimento?', 'javascript:void(0)', ['onclick' => 'required(\'Atenção\',\'Não é permitido abrir atendimento sem convênio no agendamento! <br /> Por favor, edite o agendamento e adicione um convênio. \')', 'escape' => false, 'listen' => 'f']);
                                                                } else {
                                                                    echo $this->Html->link('<i class="fa fa-user-md"></i> Deseja abrir atendimento?', ['controller' => 'Atendimentos', 'action' => 'add', '?' => ['agenda' => $agenda->id, 'first' => 1, 'ajax' => 1]], ['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#modal_lg', 'load-script' => 'Atendimentos']);
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        if ($this->Convenio->verificaConvenioSistema($agenda->convenio_id)):
                                                            echo $this->Html->link('<i class="fa fa-exchange"></i> Transferir', ['controller' => 'Agendas', 'action' => 'agendas', '?' => ['transferencia' => $agenda->id]], ['escape' => false, 'target' => '_blank', 'class' => 'txtTransferirAgendamento']);
                                                            echo $this->Html->link('<i class="fa fa-copy"></i> Copiar', 'javascript:void(0)', ['onclick' => "copiarAgendamento($agenda->id)",'escape' => false, 'target' => '_blank', 'class' => 'txtCopiarAgendamento']);
                                                        elseif (!empty($transferencia) && $agenda->cliente_id == -1):
                                                            echo $this->Html->link('<i class="fa fa-exchange"></i> Finalizar Transferencia', ['controller' => 'Agendas', 'action' => 'transferencia', $transferencia, $agenda->id], ['escape' => false, 'onclick' => "transferirHorario(event, this, 'transferir')", 'class' => 'txtTransferirAgendamento']);
                                                        elseif (!empty($copiar) && $agenda->cliente_id == -1):
                                                            echo $this->Html->link('<i class="fa fa-copy"></i> Finalizar Cópia', ['controller' => 'Agendas', 'action' => 'transferencia', $copiar, $agenda->id], ['escape' => false, 'onclick' => "transferirHorario(event, this, 'copiar')", 'class' => 'txtCopiarAgendamento']);
                                                        endif;
                                                        ?>
                                                    </li>
                                                    <li>
                                                        <?php echo $this->Html->link('<i class="fa fa-list"></i> Rastrear Agendamento', ['controller' => 'LogAgendas', 'action' => 'index', $agenda->id], ['escape' => false, 'target' => '_blank']); ?>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
