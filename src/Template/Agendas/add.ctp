<div class="this-place">
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <p class="text-right">
                    <?= $this->Form->button('<i class="fa fa-calendar"></i> Reservar', ['class' => 'btn btn-sm btn-warning', 'id' => 'reservar-agendamento', 'escape' => false]); ?>
                </p>
                <?php require_once ('form.ctp'); ?>
            </div>
        </div>
    </div>
</div>