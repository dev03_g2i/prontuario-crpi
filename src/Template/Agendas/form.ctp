<div class="agendas form">
    <?= $this->Form->create($agenda, ['id' => 'frm-agenda']) ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins ibox-g2i">
                <div class="ibox-title ibox-title-g2i">
                    <h5>Dados Cliente</h5>
                </div>
                <div class="ibox-content ibox-content-g2i">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            echo "<div class='col-md-6'>";
                                echo $this->Form->input('cliente_id', ['id' => 'cliente_id', 'options' =>[@$clientes->id => @$clientes->nome],'default'=>$agenda->cliente_id,'empty' => 'selecione', 'data-cliente' => 'cliente','label'=>'Paciente','local'=>'l', 'get' => 'convenio',
                                'append' => [
                                    $this->Html->link('<span class="glyphicon glyphicon-plus-sign"></span>', ['controller' => 'Clientes', 'action' => 'add'], ['escape' => false, 'class' => 'btn btn-default', 'target' => '_blank', 'listen' => 'f', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Paciente', 'style' => 'padding-top: 9px;padding-bottom: 9px;']),
                                    $this->Html->link('<span class="fa fa-user"></span>', 'javascript:void(0)', ['escape' => false, 'class' => 'btn btn-default btnView', 'controller' => 'Clientes', 'action' => 'nview', 'target' => '_blank', 'listen' => 'f', 'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Dados Paciente', 'style' => 'padding-top: 9px;padding-bottom: 9px;'])
                                ],]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome_provisorio', ['placeholder' => 'Ex. Jõao da Silva', 'value' => @$agenda->nome_provisorio]);
                            echo "</div>";
                            echo "<div class='clearfix'></div>";
                            echo "<div id='agenda-fone' class='col-md-3'>";
                            echo $this->Form->input('fone_provisorio', ['mask' => 'fone','required'=>'required', 'label' => 'Celular', 'placeholder' => '99 9999-9999', 'value' => @$agenda->fone_provisorio]);
                            echo "</div>";
                            echo "<div class='col-md-3'>";
                            echo $this->Form->input('fone_provisorio2', ['mask' => 'fone', 'label' => 'Fixo/Outro', 'placeholder' => '99 9999-9999', 'value' => @$agenda->fone_provisorio2]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('idade', ['required' => $this->Configuracao->idadeObrigatorio(), 'placeholder' => 'Ex. 30', 'value' => @$agenda->idade]);
                            echo "</div>";

                            echo "<div class='clearfix'></div>";
                            echo "<div class='col-md-6 retorn' style='display: none'>";
                            echo $this->Form->input('retorno_id', ['id' => 'retorno_agenda', 'type' => "select", 'empty' => 'Selecione', 'label' => 'Retornos programados']);
                            echo "</div>";
                            echo "<div class='col-md-6 retorn' style='display: none'>";
                            echo $this->Form->input('confirma_retorno', ['label' => "Confirmar Retorno?", "type" => "checkbox", "empty" => true]);
                            echo "</div>";
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-g2i float-e-margins">
                <div class="ibox-title ibox-title-g2i">
                    <h5>Dados Agendamento</h5>
                </div>
                <div class="ibox-content ibox-content-g2i">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('tipo_id', ['required'   => true, 'options' => $tipoAgendas, 'empty' => 'Selecione', 'default' => @$tipo_agenda_calendario]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('grupo_id', ['label' => 'Agenda', 'options' => $grupoAgendas, 'empty' => 'Selecione', 'default' => (@$action == 'add')?$group:'' ]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('situacao_agenda_id', ['required' => true, 'options' => $situacaoAgendas, 'empty' => 'Selecione', 'label' => 'Situação agenda', 'default' => 5]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'selecione', 'required' => 'required']);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('inicio', ['type' => 'text', 'class' => 'datetimepicker', 'value' => $inicio]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('fim', ['type' => 'text', 'class' => 'datetimepicker', 'value' => $fm]);
                            echo "</div>";

                            echo $this->Form->input('situacao_anterior',['type'=>'hidden','value'=> $agenda->situacao_agenda_id]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-g2i float-e-margins">
                <div class="ibox-title ibox-title-g2i">
                    <h5>Outros</h5>
                </div>
                <div class="ibox-content ibox-content-g2i">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('lembrete');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('agenda_procedimento_id', ['class' => 'select-procedimento', 'name' => 'agenda_procedimentos', 'label' => 'Procedimentos', 'id' => 'select-procedimento', 'type' => 'select', 'empty' => 'Selecione', 'multiple' => 'multiple', 'options' => $agendaProcedimentos, 'value' => @$agendaProcedimentosSelecionados,
                                'append' => [
                                    $this->Form->button('<span class="fa fa-files-o"></span>', [
                                        'id' => 'show-orientacoes', 'class' => $this->Configuracao->showOrientacao(), 'type'=>'button', 'escape' => false, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Orientações'
                                    ])
                                ]]);
                            echo "</div>";
                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('procedimento', ['label' => 'Obs.Procedimentos', 'rows' => 2]);
                            echo "</div>";
                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 2]);
                            echo "</div>";

                            echo $this->Form->input('agenda_id',['type'=>'hidden', 'value'=> @$agenda->id]); ?>

                            <div class='col-md-12 text-right'>
                                <?php
                                if($view == 'calendario'):
                                    echo $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'style' => 'display:none']);
                                elseif($view == 'lista' && @$action != 'view'):?>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <?php echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type'=>'button','class' => 'btn btn-primary validation-agenda']);
                                endif;
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>