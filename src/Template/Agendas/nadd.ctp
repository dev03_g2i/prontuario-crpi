<div class="this-place">
    <div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agendas</h2>
        <ol class="breadcrumb">
            <li>Agendas</li>
            <li class="active">
                <strong> Cadastrar Agendas
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="">
    <div class="row area">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendas form">
                        <?= $this->Form->create($agenda, ['id' => 'frm-agenda']) ?>
                        <fieldset>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('tipo_id', ['options' => $tipoAgendas, 'empty' => 'Selecione', 'default' => 1]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('grupo_id', ['label' => 'Agenda', 'options' => $grupoAgendas, 'empty' => 'Selecione', 'default' => $group]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('situacao_agenda_id', ['options' => $situacaoAgendas, 'empty' => 'Selecione', 'label' => 'Situação agenda', 'default' => 1]);
                            echo "</div>";
                            echo "<div class='col-md-6 cli'>";
                            echo $this->Form->input('cliente_id', ['id' => 'cliente_id', 'empty' => 'selecione', 'data-chech' => 'marcados', 'data-cliente' => 'cliente', 'options' => [@$cliente->id => @$cliente->nome], 'default' => @$cliente->id,
                                'append' => [
                                    $this->Form->button('<span class="glyphicon glyphicon-plus-sign"></span>', [
                                        'id' => 'add-provisorio','type'=>'button', 'escape' => false, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo Paciente'
                                    ])
                                ],]);
                            echo "</div>";

                            echo "<div class='clearfix'></div>";
                            echo "<div class='col-md-6 prov' style='display: none'>";
                            echo $this->Form->input('nome_provisorio');
                            echo "</div>";
                            echo "<div class='col-md-6 prov' style='display: none'>";
                            echo $this->Form->input('fone_provisorio', ['mask' => 'fone']);
                            echo "</div>";
                            echo "<div class='clearfix'></div>";

                            echo "<div class='col-md-6 retorn' style='display: none'>";
                            echo $this->Form->input('retorno_id', ['id' => 'retorno_agenda', 'type' => "select", 'empty' => 'Selecione', 'label' => 'Retornos programados']);
                            echo "</div>";
                            echo "<div class='col-md-6 retorn' style='display: none'>";
                            echo $this->Form->input('confirma_retorno', ['label' => "Confirmar Retorno?", "type" => "checkbox", "empty" => true]);
                            echo "</div>";
                            echo "<div class='clearfix'></div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('inicio', ['type' => 'text', 'class' => 'datetimepicker', 'value' => $inicio]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('fim', ['type' => 'text', 'class' => 'datetimepicker', 'value' => $fm]);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('convenio_id', ['options' => $convenios, 'empty' => 'selecione', 'required' => 'required']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('lembrete');
                            echo "</div>";

                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('procedimento');
                            echo "</div>";

                            echo "<div class='col-md-12'>";
                            echo $this->Form->input('observacao', ['label' => 'Observação']);
                            echo "</div>";
                            echo $this->Form->input('form', ['type' => 'hidden', 'value' => 'add']);
                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->button(__('Cancelar'), ['type'=>'button','class' => 'btn btn-danger','onclick'=>'Navegar(\'\',\'back\')']) ?>
                            <?= $this->Form->button(__('Salvar'), ['type'=>'submit','class' => 'btn btn-primary','onclick'=>'send_agenda(\'frm-agenda\')']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>