<div class="this-place">
    <div class="white-bg page-heading">
        <div class="row">
            <div class="col-md-6">
                <h2>Agendamento - Detalhes</h2>
                <small>Cadastrado por: <strong><?= $agenda->user->nome; ?> </strong> Em: <strong><?= $agenda->created ;?></strong></small><br>
                <small>Alterado por: <strong><?= $agenda->user_edit->nome; ?></strong> Em: <strong><?= $agenda->modified ;?></strong></small>
            </div>
            <div class="col-md-6 text-right">
                <?php if($agenda->cliente_id != '-1'): ?>
                    <p>
                        <?= $this->Html->link($this->Html->icon('list-alt') . ' Prontuário', ['controller' => 'Clientes', 'action' => 'openpront', '?' => ['pront' => $agenda->cliente_id,'nome'=>$agenda->cliente->nome]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Abrir Prontuário', 'escape' => false, 'class' => 'btn btn-xs btn-primary', 'listen' => 'f', 'target' => '_blank']) ?>
                        <?php
                        if(empty($agenda->atendimentos)){
                            echo $this->Html->link('<i class="fa fa-user-md"></i> Abrir Atendimento', ['controller' => 'Atendimentos', 'action' => 'add', '?' => ['agenda' => $agenda->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Abrir Atendimento', 'escape' => false, 'class' => 'btn btn-xs btn-info', 'listen' => 'f', 'target' => '_blank']);
                        }
                        ?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12" id="frm-view-agenda">
                <?php require_once ('form.ctp'); ?>
            </div>
        </div>
    </div>
</div>