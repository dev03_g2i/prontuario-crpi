<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="col-lg-9">
            <h2>Cadastrar Horários</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row area">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendas form">
                        <?= $this->Form->create($agenda, ['id' => 'frm-agenda']) ?>
                        <div class='col-md-6'>
                            <?php echo $this->Form->input('grupo_id', ['label' => 'Agenda', 'options' => $grupoAgendas, 'empty' => 'Selecione']);?>
                        </div>

                        <div class='col-md-6'>
                            <?php echo $this->Form->input('tipo_id', ['options' => $tipoAgendas, 'empty' => 'Selecione', 'default' => 1]); ?>
                        </div>

                        <div class='col-md-3'>
                            <?php echo $this->Form->input('inicio', ['type' => 'text', 'class' => 'datetimepicker']); ?>
                        </div>
                        <div class='col-md-3'>
                            <?php echo $this->Form->input('fim', ['type' => 'text', 'class' => 'datetimepicker', 'required' => true]); ?>
                        </div>
                        <div class='col-md-5'>
                            <?php echo $this->Form->input('observacao', ['label' => 'Observação', 'rows' => 1]); ?>
                        </div>
                        <div class="col-md-1">
                            <div style="margin-top: 20px;">
                                <?php echo $this->Form->button('<i class="fa fa-plus"></i>', ['type' => 'button','id' => 'btnAddHorarios', 'class' => 'btn btn-primary dim', 'escape' => false]) ?>
                            </div>
                        </div>


                        <!--<div class="col-md-12 text-left" style="margin-top: 25px">
                                <?= $this->Form->button(__('Cancelar'), ['type'=>'button','class' => 'btn btn-danger','onclick'=>'Navegar(\'\',\'back\')']) ?>
                                <?= $this->Form->button(__('Salvar'), ['type'=>'button','class' => 'btn btn-primary validation-agenda']) ?>
                            </div>-->
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>


                    <div class="table-responsive">
                        <table class="table table-hover" id="show-horarios">
                            <thead>
                            <tr>
                                <th>Agenda</th>
                                <th>Tipo Agenda</th>
                                <th>Inicio</th>
                                <th>Fim</th>
                                <th>Observação</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody >

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>