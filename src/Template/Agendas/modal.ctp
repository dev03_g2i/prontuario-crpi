<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <div class="col-lg-9">
            <h2>Agendas</h2>
            <ol class="breadcrumb">
                <li>Agendas</li>
                <li class="active">
                    <strong>Listagem de Agendas</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Agendas') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('inicio',['label'=>'Data']) ?></th>
                                        <th><strong><?= $this->Paginator->sort('grupo_id',['label'=>'Agenda']) ?></strong></th>
                                        <th><?= $this->Paginator->sort('tipo_id') ?></th>
                                        <th><?= $this->Paginator->sort('procedimento_provisorio',['label'=>'Procedimento']) ?></th>
                                        <th><?= $this->Paginator->sort('lembrete',['label'=>'']) ?></th>
                                        <th><?= $this->Paginator->sort('cliente_id',['label'=>'Paciente']) ?></th>
                                        <th><?= $this->Paginator->sort('convenio_provisorio',['label'=>'Convênio']) ?></th>
                                        <th><?= $this->Paginator->sort('fone_provisorio',['label'=>'Telefone/Observações']) ?></th>
                                        <th><?= $this->Paginator->sort('situacao_agenda_id',['label'=>'Situação']) ?></th>
                                    </tr>
                                    </thead>
                                    <tbody >
                                    <?php foreach ($agendas as $agenda):
                                        $cor_atendimento = $agenda->situacao_agenda->cor_atendimento;
                                        ?>
                                        <tr style="color: <?=$cor_atendimento ?>">
                                            <td class="color-links"><?= @date_format($agenda->inicio, 'd/m/Y H:i') ?></td>
                                            <td class="color-links"><strong><?= $agenda->has('grupo_agenda') ? $agenda->grupo_agenda->nome : '' ?></strong></td>
                                            <td class="color-links"><?= $agenda->has('tipo_agenda') ? $agenda->tipo_agenda->nome : '' ?></td>
                                            <td class="color-links"><?= $agenda->procedimento_provisorio ?></td>
                                            <td class="color-links"><?= !empty($agenda->lembrete) ? $this->Html->link('<i class="fa fa-comment"></i>','#Javascript:void(0)',['escape' => false,'toggle'=>'popover','title'=>'Lembrete','data-content'=>$agenda->lembrete,'data-placement'=>'bottom']):'' ?></td>
                                            <td class="color-links"><?= $agenda->has('cliente') ? (($agenda->cliente->id!='-1') ? $this->Html->link($agenda->cliente->arquivo." - ".$agenda->cliente->nome,['controller'=>'clientes','action'=>'prontuario',$agenda->cliente_id,'?'=>['agenda'=>$agenda->id]],['style' => 'color:'.$cor_atendimento, 'data-toggle'=>'modal','data-target'=>'#modal_2' ,'data-backdrop'=>'false', 'id' => 'abrir-prontuario-pela-agenda']) : 'Prov: '.$agenda->nome_provisorio) : '' ?></td>
                                            <td class="color-links"><?= !empty($agenda->convenio_id) ? $agenda->convenio->nome : $agenda->convenio_provisorio ?></td>
                                            <td class="color-links"><?= (($agenda->cliente->id!='-1') ? $agenda->cliente->telefone."/".$agenda->cliente->celular : $agenda->fone_provisorio.'/'.$agenda->observacao ) ?></td>
                                            <td class="color-links"><?= $agenda->has('situacao_agenda') ? $agenda->situacao_agenda->nome : '' ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginato">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>