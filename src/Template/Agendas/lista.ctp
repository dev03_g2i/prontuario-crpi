<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title"><h3>Agenda</h3></div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="filtros ">
                    <?= $this->Form->create('Agendas', ['type' => 'get', 'id' => 'lista-agenda']) ?>

                    <div class="col-md-11">
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <?= $this->Form->input('nome_provisorio', ['label' => 'Paciente']); ?>
                            </div>
                            <div class="col-md-3">
                                <?= $this->Form->input('grupo_id', ['label' => 'Agenda', 'name' => 'grupos[]', 'type' => 'select', 'empty' => 'Selecione', 'multiple', 'options' => $grupoAgendas,'default'=>$grupo,'class'=>'select2']); ?>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <?= $this->Form->input('tipo_id', ['label' => 'Tipo Agenda', 'name' => 'tipos[]', 'empty' => 'Selecione', 'multiple', 'options' => $tipoAgendas,'class'=>'select2']); ?>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 m-t-23">
                                <?= $this->Form->button($this->Html->icon('search'), ['style' => 'margin-bottom: 0px; height: 35px', 'type' => 'button', 'id' => 'fill-agenda', 'class' => 'btn btn-default ', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['style' => 'margin-bottom: 0px; height: 35px', 'type' => 'button', 'id' => 'refresh-agenda', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                                <!-- <?= $this->Html->link($this->Html->icon('plus'), ['action' => 'new-add'], ['id' => 'btnEncaixe', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Encaixe/Novo Horário', 'class' => 'btn btn-primary', 'escape' => false, 'data-target' => '#modal_lg']) ?> -->
                                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span class="fa fa-list"></span> Opções
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" x-placement="bottom-start">
                                    <!-- <li class="dropdown-item">
                                            <?= $this->Html->link($this->Html->icon('plus') . ' Encaixe/Novo Horário', ['action' => 'new-add', true], ['id' => 'btnEncaixe', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Encaixe/Novo Horário', 'escape' => false, 'data-target' => '#modal_lg']) ?>
                                    </li> -->
                                    <!-- <li class="dropdown-item">
                                            <?= $this->Html->link('<i class="fa fa-calendar-plus-o"></i>' . ' Horários Especiais', ['controller' => 'GrupoAgendaHorarios', 'action' => 'add'], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Horários Especiais', 'class' => ' horarios-especiais', 'escape' => false, 'target' => '_blank']) ?>
                                    </li> -->
                                    <li class="dropdown-item"> <?= $this->Html->link('<i class="fa fa-commenting"></i> ' . ' Adicionar Anotações', ['controller' => 'AnotacaoAgendas', 'action' => 'index', '?' => ['first' => 1, 'ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Adicionar Anotações', 'class' => ' openAnotacao', 'id' => 'openAnotacao', 'escape' => false, 'data-target' => '#modal_lg']) ?> </li>
                                    <li class="dropdown-item"> <?= $this->Html->link('<i class="fa fa-address-book"></i>' . ' Adicionar Particularidades', ['controller' => 'GrupoAgendas', 'action' => 'index'], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Adicionar Particularidades', 'class' => ' openParticularidades', 'escape' => false]) ?> </li>
                                    <!-- <li class="dropdown-item"> <?= $this->Html->link('<i class="fa fa-lock"></i>' . ' Bloqueio Agenda', 'javascript:void(0)', ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Adicionar Bloqueio na Agenda', 'escape' => false, 'class' => 'openBloqueioAgenda', 'data-target' => '#modal_lg']) ?> </li> -->
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <?= $this->Form->input('situacao_agenda_id', ['label' => 'Situação Agenda', 'name' => 'situacoes[]', 'multiple', 'empty' => 'Selecione', 'options' => $situacaoAgendas,'class'=>'select2']); ?>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <?= $this->Form->input('start', ['label' => 'Data', 'type' => 'text', 'class' => "datepicker", 'value' => $inicio, 'append' => [$this->Form->button($this->Html->icon("calendar", ['style' => 'height: 20px;margin-bottom:0px']), ['type' => 'button'])]]); ?>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <?= $this->Html->link('Fila de espera <span class="badge count-filaespera">0</span>', ['controller' => 'AgendaFilaespera', 'action' => 'index', '?' => ['first' => 1, 'ajax' => 1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Abrir fila de espera', 'class' => 'btn btn-warning openFilaEspera', 'escape' => false, 'data-target' => '#modal_lg', 'style' => 'margin-top: 22px;']) ?>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <?=$this->Form->input('default_view', ['type' => 'hidden', 'default-view' => ($defaultView)?$defaultView:'agendaDay'])?>

                    <div class="clearfix"></div>
                    <?= $this->Form->end() ?>
                </div>

            </div>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="gif-agenda"></div>
                <div class="clearfix"></div>
                <div class="table-responsive" style="overflow: auto">
                    <table class="table">
                        <tr id="area_cal">
                            <td id="calendar"></td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="floating-modal floating-anotacoes float-fixed-right d-none no-data">
    <p>Sem dados!</p>
</div>
<div class="floating-modal floating-particularidades float-fixed-right d-none no-data">
    <p>Sem dados!</p>
</div>
<?= $this->Form->button('<i class="fa fa-comment-o"></i> <span class="label label-inverse count-anotacao">0</span> ', ['id' => 'btn-comentarios', 'class' => 'btn btn-danger btn-circle btn-lg box-shadow-float-button float-fixed-right', 'onclick' => 'checkFloatingButtons(0)']) ?>
<?= $this->Form->button('<i class="fa fa-book"></i> <span class="label label-inverse count-particularidades">0</span> ', ['id' => 'btn-particularidades', 'class' => 'btn btn-warning btn-circle btn-lg box-shadow-float-button float-fixed-right', 'onclick' => 'checkFloatingButtons(1)']) ?>

<div id='external-events'>
    <div class="external-event-title"></div>
    <div id='external-events-listing'></div>
    <div class="external-event-footer text-center">
        <button class="btn btn-danger btn-xs" id="btnCancelarTransferencia">Cancelar</button>
    </div>
</div>

<?php
echo $this->Html->scriptBlock(
    " 
    var documento = null;
        $(document).ready(function(){
            documento = document;
       });
       $(window).load(function(){
            agenda(documento,'',1);
        setInterval(function(){
        $('#fill-agenda').click();
    },120000);
    })
    
    window.onload = function(){
        $('#fill-agenda').click();
    }
    
    ",
    ['inline' => false, 'block' => 'scriptLast']
);
?>