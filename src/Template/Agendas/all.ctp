<div class="this-place">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agendas</h2>
        <ol class="breadcrumb">
            <li>Agendas</li>
            <li class="active">
                <strong>Litagem de Agendas</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Agendas') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('inicio',['label'=>'Data']) ?></th>
                                    <th><?= $this->Paginator->sort('inicio',['label'=>'Hora']) ?></th>
                                    <th><?= $this->Paginator->sort('grupo_id',['label'=>'Agenda']) ?></th>
                                    <th><?= $this->Paginator->sort('situacao_agenda_id', ['label' => 'Situação']) ?></th>
                                    <th class="actions"><?= __('Opções') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($agendas as $agenda): ?>
                                    <tr>
                                        <td><?= $this->Time->format($agenda->inicio, 'dd/MM/Y') ?></td>
                                        <td><?= $this->Time->format($agenda->inicio, 'HH:mm') ?></td>
                                        <td><?= $agenda->has('grupo_agenda') ? $this->Html->link($agenda->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agenda->grupo_agenda->id]) : '' ?></td>
                                        <td><?= $agenda->has('situacao_agenda') ? $this->Html->link($agenda->situacao_agenda->nome, ['controller' => 'SituacaoAgendas', 'action' => 'view', $agenda->situacao_agenda->id]) : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $agenda->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>