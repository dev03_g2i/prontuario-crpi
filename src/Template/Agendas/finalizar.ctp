<div class="this-place">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agendas</h2>
            <ol class="breadcrumb">
                <li>Agendas</li>
                <li class="active">
                    <strong> Finalizar Agendamento
                    </strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="">
        <div class="row area">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="agendas form">
                            <?= $this->Form->create($agenda, ['id' => 'fim-agenda']) ?>
                            <fieldset>
                                <?php

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('cliente_id', ['options' => [$clientes->id => $clientes->nome], 'default' => $agenda->cliente_id, 'empty' => 'selecione', 'label' => 'Paciente']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('tipohistoria_id', ['options' => $tipoHistorias, 'label' => 'Tipo de Procedimento', 'empty' => 'Selecione', 'default' => 1, 'required' => 'required']);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('medico_id', ['options' => $medicoResponsaveis, 'label' => 'Profissional', 'empty' => 'Selecione', 'default' => $agenda->grupo_agenda->medico_id, 'required' => 'required']);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('anotacao', ['label' => 'Dente/Área', 'type' => 'textarea', 'required' => 'required', 'rows' => 1]);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('descricao', ['label' => 'Descrição', 'type' => 'textarea', 'required' => 'required']);
                                echo "</div>";

                                ?>
                            </fieldset>
                            <fieldset>
                                <h3>Retorno</h3>
                                <hr/>

                                <?php
                                echo "<div class='col-md-12' data-toggle='popover' title='Alerta' data-content='Lembre-se de verificar se existe grupos vinculados ao cliente selecionado!' data-placement='bottom' data-trigger='hover' >";
                                echo $this->Form->input('grupo_retorno', ['label' => 'Grupo', 'type' => 'select', 'options' => $grupoRetornos, 'empty' => 'selecione', 'default' => '', 'required' => 'required',
                                    'append' => [
                                        $this->Form->button('<span class="glyphicon glyphicon-plus-sign"></span>', [
                                            'data' => 'modal', 'controller' => 'ClienteGrupoRetornos', 'action' => 'naddmodal', 'vinculo' => 'cliente-id', 'escape' => false, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Novo Grupo', 'type' => 'button'
                                        ])
                                    ]]);
                                echo "</div>";
                                echo '<div class="clearfix"></div>';
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('previsao', ['label' => 'Data', 'type' => 'text', 'class' => "datepicker", 'required' => 'required', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('dias', ['required' => 'required']);
                                echo "</div>";

                                echo "<div class='col-md-12'>";
                                echo $this->Form->input('observacao', ['label' => 'Próximo procedimento', 'value' => '']);
                                echo "</div>";
                                ?>
                                <div class="clearfix"></div>
                            </fieldset>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>