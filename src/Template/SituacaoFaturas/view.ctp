
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Situacao Faturas</h2>
        <ol class="breadcrumb">
            <li>Situacao Faturas</li>
            <li class="active">
                <strong>Litagem de Situacao Faturas</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Situacao Faturas</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($situacaoFatura->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $situacaoFatura->has('user') ? $this->Html->link($situacaoFatura->user->nome, ['controller' => 'Users', 'action' => 'view', $situacaoFatura->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $situacaoFatura->has('situacao_cadastro') ? $this->Html->link($situacaoFatura->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $situacaoFatura->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($situacaoFatura->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($situacaoFatura->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($situacaoFatura->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h3><?= __('Related Atendimento Procedimentos') ?></h3>
        </div>
        <div class="ibox-content">
            <?php if (!empty($situacaoFatura->atendimento_procedimentos)): ?>
            <div class="table-responsive">
                <table class="table table-hover">
                <tr>
                        <th><?= __('Id') ?></th>
                        <th><?= __('Procedimento Id') ?></th>
                        <th><?= __('Atendimento Id') ?></th>
                        <th><?= __('Valor Base') ?></th>
                        <th><?= __('Valor Fatura') ?></th>
                        <th><?= __('Quantidade') ?></th>
                        <th><?= __('Desconto') ?></th>
                        <th><?= __('Porc Desconto') ?></th>
                        <th><?= __('Valor Caixa') ?></th>
                        <th><?= __('Digitado') ?></th>
                        <th><?= __('Material') ?></th>
                        <th><?= __('Num Controle') ?></th>
                        <th><?= __('Medico Id') ?></th>
                        <th><?= __('Valor Matmed') ?></th>
                        <th><?= __('Documento Guia') ?></th>
                        <th><?= __('Situacao Id') ?></th>
                        <th><?= __('User Id') ?></th>
                        <th><?= __('Created') ?></th>
                        <th><?= __('Modified') ?></th>
                        <th><?= __('Complemento') ?></th>
                        <th><?= __('Controle') ?></th>
                        <th><?= __('Nr Guia') ?></th>
                        <th><?= __('Dt Emissao Guia') ?></th>
                        <th><?= __('Autorizacao Senha') ?></th>
                        <th><?= __('Dt Autorizacao') ?></th>
                        <th><?= __('Percentual Cobranca') ?></th>
                        <th><?= __('Apurar Id') ?></th>
                        <th><?= __('Situacao Fatura Id') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                </tr>
                <?php foreach ($situacaoFatura->atendimento_procedimentos as $atendimentoProcedimentos): ?>
                <tr>
                    <td><?= h($atendimentoProcedimentos->id) ?></td>
                    <td><?= h($atendimentoProcedimentos->procedimento_id) ?></td>
                    <td><?= h($atendimentoProcedimentos->atendimento_id) ?></td>
                    <td><?= h($atendimentoProcedimentos->valor_base) ?></td>
                    <td><?= h($atendimentoProcedimentos->valor_fatura) ?></td>
                    <td><?= h($atendimentoProcedimentos->quantidade) ?></td>
                    <td><?= h($atendimentoProcedimentos->desconto) ?></td>
                    <td><?= h($atendimentoProcedimentos->porc_desconto) ?></td>
                    <td><?= h($atendimentoProcedimentos->valor_caixa) ?></td>
                    <td><?= h($atendimentoProcedimentos->digitado) ?></td>
                    <td><?= h($atendimentoProcedimentos->material) ?></td>
                    <td><?= h($atendimentoProcedimentos->num_controle) ?></td>
                    <td><?= h($atendimentoProcedimentos->medico_id) ?></td>
                    <td><?= h($atendimentoProcedimentos->valor_matmed) ?></td>
                    <td><?= h($atendimentoProcedimentos->documento_guia) ?></td>
                    <td><?= h($atendimentoProcedimentos->situacao_id) ?></td>
                    <td><?= h($atendimentoProcedimentos->user_id) ?></td>
                    <td><?= h($atendimentoProcedimentos->created) ?></td>
                    <td><?= h($atendimentoProcedimentos->modified) ?></td>
                    <td><?= h($atendimentoProcedimentos->complemento) ?></td>
                    <td><?= h($atendimentoProcedimentos->controle) ?></td>
                    <td><?= h($atendimentoProcedimentos->nr_guia) ?></td>
                    <td><?= h($atendimentoProcedimentos->dt_emissao_guia) ?></td>
                    <td><?= h($atendimentoProcedimentos->autorizacao_senha) ?></td>
                    <td><?= h($atendimentoProcedimentos->dt_autorizacao) ?></td>
                    <td><?= h($atendimentoProcedimentos->percentual_cobranca) ?></td>
                    <td><?= h($atendimentoProcedimentos->apurar_id) ?></td>
                    <td><?= h($atendimentoProcedimentos->situacao_fatura_id) ?></td>
                    <td class="actions">
                        <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'AtendimentoProcedimentos','action' => 'view', $atendimentoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                        <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'AtendimentoProcedimentos','action' => 'edit', $atendimentoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                        <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'AtendimentoProcedimentos','action' => 'delete', $atendimentoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $situacaoFatura->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>
</div>
</div>


