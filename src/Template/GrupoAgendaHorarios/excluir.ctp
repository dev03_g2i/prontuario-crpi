<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2>Deletar Horários</h2>
            <p><strong>Agenda</strong>: <?=$grupoAgendaHorario->grupo_agenda->nome; ?></p>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form">
                            <?= $this->Form->create($grupoAgendaHorario, ['class' => 'validateDateInicioFim', 'id' => 'frm-gerar-horario']) ?>
                            <div class='col-md-12'>
                                <?php echo $this->Form->input('grupo_id', ['type' => 'hidden', 'required' => true, 'label' => 'Agenda', 'options' => $grupoAgendas, 'empty' => 'Selecione', 'class' => 'select2', 'default' => $grupo_id]);?>
                            </div>
                            <!--<div class='col-md-6'>
                                <?php echo $this->Form->input('tipo_id', ['required' => true, 'options' => $tipoAgendas, 'empty' => 'Selecione', 'class' => 'select2']); ?>
                            </div>-->
                            <div class='col-md-3'>
                                <?php echo $this->Form->input('data_inicio', ['required' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($grupoAgendaHorario->data_inicio, 'dd/MM/YYYY'),  'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-3'>
                                <?php echo $this->Form->input('data_fim', ['required' => true, 'type' => 'text', 'class' => 'datepicker', 'value' => $this->Time->format($grupoAgendaHorario->data_fim, 'dd/MM/YYYY'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-2'>
                                <?php echo $this->Form->input('hora_inicio',['required' => true, 'class'=>'timepiker','type'=>'text','append' => [$this->Form->button("<i class='fa fa-clock-o'></i>", ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-2'>
                                <?php echo $this->Form->input('hora_fim',['required' => true, 'class'=>'timepiker', 'type'=>'text','append' => [$this->Form->button("<i class='fa fa-clock-o'></i>", ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-2'>
                                <?php echo $this->Form->input('intervalo', ['required' => true, 'readonly' => true, 'type' => 'text', 'label' => 'Intervalo em minutos', 'append' => [$this->Form->button('<i class="fa fa-clock-o"></i>', ['type' => 'button'])]]); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class='col-md-12 text-center dias-semana'>
                                <label for="">Dias da semana</label><br>
                                <?php foreach($this->Data->diasSemana() as $key => $value): ?>
                                    <label class="checkbox-inline">
                                        <input readonly disabled type="checkbox" name="dias_semana[]" <?=($grupoAgendaHorario->dia_semana == $key)? 'checked':''?> value="<?=$key?>"> <?=$value?>
                                    </label>
                                <?php endforeach; ?>
                            </div>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Deletar'),['type' => 'button', 'class'=>'btn btn-primary', 'id' => 'btnSalvarHorarios']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>