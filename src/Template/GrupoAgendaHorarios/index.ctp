<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Listagem de Horários</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="filtros">
                            <?= $this->Form->create('GrupoAgendaHorario',['type'=>'get']) ?>
                            <div class='col-md-4'>
                                <?= $this->Form->input('grupo_id', ['name'=>'GrupoAgendaHorarios__grupo_id', 'options' => $grupoAgendas, 'class' => 'select2', 'empty' => 'Selecione', 'default' => $grupo_id, 'empty' => 'SElecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('tipo_agenda_id',['name'=>'GrupoAgendaHorarios__tipo_agenda_id', 'options' => $tipoAgendas, 'class' => 'select2', 'empty' => 'Selecione']); ?>
                            </div>
                            <div class='col-md-4'>
                                <?=$this->Form->input('dia_semana',['options' => $this->Data->diasSemana(), 'empty' => 'Selecione']); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 fill-content-search">
                                <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                                <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus').' Nova Operação', ['action' => 'add', $grupo_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Nova Operações','class'=>'btn btn-primary','escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id', 'Código') ?></th>
                                        <th><?= $this->Paginator->sort('dia_semana') ?></th>
                                        <th><?= $this->Paginator->sort('data_inicio') ?></th>
                                        <th><?= $this->Paginator->sort('data_fim') ?></th>
                                        <th><?= $this->Paginator->sort('hora_fim') ?></th>
                                        <th><?= $this->Paginator->sort('hora_fim') ?></th>
                                        <th><?= $this->Paginator->sort('intervalo') ?></th>
                                        <th><?= $this->Paginator->sort('grupo_id') ?></th>
                                        <th><?= $this->Paginator->sort('tipo_agenda_id') ?></th>
                                        <th><?= $this->Paginator->sort('situacao_agenda', 'Operação') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($grupoAgendaHorarios as $grupoAgendaHorario): ?>
                                        <tr class="<?=$this->GrupoAgendaHorario->verificaOperacao($grupoAgendaHorario->operacao_agenda_horario_id); ?>">
                                            <td><?= $grupoAgendaHorario->id ?></td>
                                            <td><?= $this->Data->diasSemana($grupoAgendaHorario->dia_semana) ?></td>
                                            <td><?= $this->Time->format($grupoAgendaHorario->data_inicio, 'dd/MM/Y') ?></td>
                                            <td><?= $this->Time->format($grupoAgendaHorario->data_fim, 'dd/MM/Y') ?></td>
                                            <td><?= $this->Time->format($grupoAgendaHorario->hora_inicio, 'HH:mm') ?></td>
                                            <td><?= $this->Time->format($grupoAgendaHorario->hora_fim, 'HH:mm') ?></td>
                                            <td><?= $this->Time->format($grupoAgendaHorario->intervalo, 'HH:mm') ?></td>
                                            <td><?= $grupoAgendaHorario->has('grupo_agenda') ? $this->Html->link($grupoAgendaHorario->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $grupoAgendaHorario->grupo_agenda->id]) : '' ?></td>
                                            <td><?= $grupoAgendaHorario->has('tipo_agenda') ? $this->Html->link($grupoAgendaHorario->tipo_agenda->nome, ['controller' => 'TipoAgendas', 'action' => 'view', $grupoAgendaHorario->tipo_agenda->id]) : '' ?></td>
                                            <td><?= ($grupoAgendaHorario->has('operacao_agenda_horario')) ? $grupoAgendaHorario->operacao_agenda_horario->nome : '' ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'grupoAgendaHorarios','action' => 'view', $grupoAgendaHorario->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info', 'target' => '_blank']) ?>
                                                <!--<?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'grupoAgendaHorarios','action' => 'edit', $grupoAgendaHorario->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link('<i class="fa fa-trash"></i>',  ['controller'=>'grupoAgendaHorarios','action'=>'excluir', $grupoAgendaHorario->id, $grupo_id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Excluir Horárois do intervalo','escape' => false,'class'=>'btn btn-xs btn-warning','listen' => 'f']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'),  ['controller'=>'grupoAgendaHorarios','action'=>'delete', $grupoAgendaHorario->id, $grupo_id],['onclick'=>'excluir(event, this)','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Excluir todos os horários','escape' => false,'class'=>'btn btn-xs btn-danger','listen' => 'f']) ?>-->
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div class="text-center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter('Página {{page}} de {{pages}}') ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
