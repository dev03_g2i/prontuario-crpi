<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2>Gerar Horários</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form">
                            <?= $this->Form->create($grupoAgendaHorario, ['class' => 'validateDateInicioFim', 'id' => 'frm-gerar-horario']) ?>
                            <div class='col-md-4'>
                                <?php echo $this->Form->input('operacao_agenda_horario_id', ['required' => true, 'id' => 'operacao', 'label' => 'Operação', 'options' => $operacoes, 'empty' => 'Selecione']);?>
                            </div>
                            <div class='col-md-4 hidden'>
                                <?php echo $this->Form->input('nome_provisorio', ['required' => true, 'label' => 'Descrição do Bloqueio']);?>
                            </div>
                            <div class='col-md-4'>
                                <?php echo $this->Form->input('grupo_id', ['required' => true, 'label' => 'Agenda', 'options' => $grupoAgendas, 'empty' => 'Selecione', 'class' => 'select2', 'default' => $grupo_id]);?>
                            </div>
                            <div class='col-md-4'>
                                <?php echo $this->Form->input('tipo_id', ['required' => true, 'options' => $tipoAgendas, 'empty' => 'Selecione', 'class' => 'select2', 'default' => 1]); ?>
                            </div>
                            <div class='col-md-3'>
                                <?php echo $this->Form->input('data_inicio', ['required' => true, 'type' => 'text', 'class' => 'datepicker ', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-3 hidden'>
                                <?php echo $this->Form->input('data_fim', ['required' => true, 'type' => 'text', 'class' => 'datepicker', 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-2'>
                                <?php echo $this->Form->input('hora_inicio',['required' => true, 'class'=>'timepiker','type'=>'text','append' => [$this->Form->button("<i class='fa fa-clock-o'></i>", ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-2'>
                                <?php echo $this->Form->input('hora_fim',['required' => true, 'class'=>'timepiker', 'type'=>'text','append' => [$this->Form->button("<i class='fa fa-clock-o'></i>", ['type' => 'button'])]]); ?>
                            </div>
                            <div class='col-md-2'>
                                <?php echo $this->Form->input('intervalo', ['required' => true, 'type' => 'text', 'label' => 'Intervalo em minutos', 'append' => [$this->Form->button('<i class="fa fa-clock-o"></i>', ['type' => 'button'])]]); ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class='col-md-12 text-center dias-semana'>
                                <label for="">Dias da semana</label><br>
                                <?php foreach($this->Data->diasSemana() as $key => $value): ?>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" name="dias_semana[]" value="<?=$key?>"> <?=$value?>
                                    </label>
                                <?php endforeach; ?>
                            </div>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['type' => 'button', 'class'=>'btn btn-primary', 'id' => 'btnSalvarHorarios']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>