<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Grupo Agenda Horarios</h2>
            <ol class="breadcrumb">
                <li>Grupo Agenda Horarios</li>
                <li class="active">
                    <strong>
                        Editar Grupo Agenda Horarios
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="grupoAgendaHorarios form">
                            <?= $this->Form->create($grupoAgendaHorario) ?>
                            <fieldset>
                                <legend><?= __('Editar Grupo Agenda Horario') ?></legend>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('dia_semana'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?php                                             echo $this->Form->input('inicio', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($grupoAgendaHorario->inicio,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                    ?>
                                </div>
                                <div class='col-md-6'>
                                    <?php                                             echo $this->Form->input('fim', ['type' => 'text', 'class' => 'datetimepicker','value'=>$this->Time->format($grupoAgendaHorario->fim,'dd/MM/Y H:m'), 'append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                                    ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('intervalo'); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('grupo_id', ['data'=>'select','controller'=>'grupoAgendas','action'=>'fill','data-value'=>$grupoAgendaHorario->grupo_id]); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('tipo_agenda_id'); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

