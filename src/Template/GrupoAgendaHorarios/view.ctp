
<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Detalhes Horário - Geração</h2>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <h3>Detalhes Horário</h3>
                            <table class="table table-hover">
                                <tr>
                                    <th><?= __('Id') ?></th>
                                    <td><?= $this->Number->format($grupoAgendaHorario->id) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dia Semana') ?></th>
                                    <td><?= $this->Data->diasSemana($grupoAgendaHorario->dia_semana) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Data Inicio') ?></th>
                                    <td><?= $this->Time->format($grupoAgendaHorario->data_inicio, 'dd/MM/Y') ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Data Fim') ?></th>
                                    <td><?= $this->Time->format($grupoAgendaHorario->data_fim, 'dd/MM/Y') ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Hora Inicio') ?></th>
                                    <td><?= $this->Time->format($grupoAgendaHorario->hora_inicio, 'HH:mm') ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Hora Fim') ?></th>
                                    <td><?= $this->Time->format($grupoAgendaHorario->hora_fim, 'HH:mm') ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Intervalo') ?></th>
                                    <td><?= $this->Time->format($grupoAgendaHorario->intervalo, 'HH:mm') ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Agenda') ?></th>
                                    <td><?= $grupoAgendaHorario->has('grupo_agenda') ? $this->Html->link($grupoAgendaHorario->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $grupoAgendaHorario->grupo_agenda->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Tipo Agenda') ?></th>
                                    <td><?= $grupoAgendaHorario->tipo_agenda->nome ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Situação Cadastro') ?></th>
                                    <td><?= $grupoAgendaHorario->has('situacao_cadastro') ? $this->Html->link($grupoAgendaHorario->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $grupoAgendaHorario->situacao_cadastro->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Quem Cadastrou') ?></th>
                                    <td><?= $grupoAgendaHorario->has('user') ? $this->Html->link($grupoAgendaHorario->user->nome, ['controller' => 'Users', 'action' => 'view', $grupoAgendaHorario->user->id]) : '' ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Criação') ?></th>
                                    <td><?= h($grupoAgendaHorario->created) ?></td>
                                </tr>
                                <tr>
                                    <th><?= __('Dt. Modificação') ?></th>
                                    <td><?= h($grupoAgendaHorario->modified) ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


