<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Convenio Regracalculo</h2>
            <ol class="breadcrumb">
                <li>Convenio Regracalculo</li>
                <li class="active">
                    <strong>
                                                Cadastrar Convenio Regracalculo
                                            </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="convenioRegracalculo form">
                            <?= $this->Form->create($convenioRegracalculo) ?>
                            <fieldset>
                                                                <legend><?= __('Cadastrar Convenio Regracalculo') ?></legend>
                                                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('apelido'); ?>
                                                                    </div>
                                                                <div class='col-md-6'>
                                                                            <?=$this->Form->input('orientacao'); ?>
                                                                    </div>
                                                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

