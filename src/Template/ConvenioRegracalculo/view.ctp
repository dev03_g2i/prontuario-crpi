
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Convenio Regracalculo</h2>
        <ol class="breadcrumb">
            <li>Convenio Regracalculo</li>
            <li class="active">
                <strong>Litagem de Convenio Regracalculo</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Convenio Regracalculo</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Apelido') ?></th>
                                <td><?= h($convenioRegracalculo->apelido) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Orientacao') ?></th>
                                <td><?= h($convenioRegracalculo->orientacao) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $convenioRegracalculo->has('situacao_cadastro') ? $this->Html->link($convenioRegracalculo->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $convenioRegracalculo->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $convenioRegracalculo->has('user') ? $this->Html->link($convenioRegracalculo->user->nome, ['controller' => 'Users', 'action' => 'view', $convenioRegracalculo->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($convenioRegracalculo->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($convenioRegracalculo->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($convenioRegracalculo->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


