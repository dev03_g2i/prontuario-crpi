
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Profissionais</h2>
        <ol class="breadcrumb">
            <li>Profissionais</li>
            <li class="active"><strong>Litagem de Profissionais</strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="medicoResponsaveis">
    <h3><?= h($medicoResponsavei->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Conselho') ?></th>
            <td><?= h($medicoResponsavei->conselho) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($medicoResponsavei->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Sigla') ?></th>
            <td><?= h($medicoResponsavei->sigla) ?></td>
        </tr>
        <tr>
            <th><?= __('Nome Laudo') ?></th>
            <td><?= h($medicoResponsavei->nome_laudo) ?></td>
        </tr>
        <tr>
            <th><?= __('Conselho Laudo') ?></th>
            <td><?= h($medicoResponsavei->conselho_laudo) ?></td>
        </tr>
        <tr>
            <th><?= __('Cpf') ?></th>
            <td><?= h($medicoResponsavei->cpf) ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $medicoResponsavei->has('situacao_cadastro') ? $this->Html->link($medicoResponsavei->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $medicoResponsavei->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $medicoResponsavei->has('user') ? $this->Html->link($medicoResponsavei->user->nome, ['controller' => 'Users', 'action' => 'view', $medicoResponsavei->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($medicoResponsavei->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($medicoResponsavei->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($medicoResponsavei->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

