
<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Profissionais</h2>
        <ol class="breadcrumb">
            <li>Profissionais</li>
            <li class="active">
                <strong>
                    Editar Profissionais
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="medicoResponsaveis form">
                        <?= $this->Form->create($medicoResponsavei, ['type' => 'file', 'frm-medico-responsaveis']) ?>
                        <fieldset>
                            <legend><?= __('Editar Profissionais') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('conselho');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('sigla');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome_laudo');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('conselho_laudo');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('cpf');
                            echo "</div>";

                            $url_img = (!empty($medicoResponsavei->caminho_dir)) ? "/files/medicoresponsaveis/caminho/$medicoResponsavei->caminho_dir/$medicoResponsavei->caminho" : '';
                            ?>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="files">Anexo Assinatura</label>
                                    <input id="files" type="file" name="caminho" class="input-file-edit"
                                           img-id="<?=$medicoResponsavei->id?>"
                                           link-img="<?=$url_img?>"
                                           descricao-img="<?= $medicoResponsavei->descricao_anexo ?>"
                                           controller-img="MedicoResponsaveis"
                                           data-overwrite-initial="false">
                                </div>
                            </div>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

