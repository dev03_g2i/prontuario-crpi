
<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Profissionais</h2>
        <ol class="breadcrumb">
            <li>Profissionais</li>
            <li class="active">
                <strong>Litagem</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right btnAdd">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Profissionais', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Profissionais','class'=>'btn btn-primary','escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Profissionais') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id',['label'=>'Código']) ?></th>
                                    <th><?= $this->Paginator->sort('conselho') ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('sigla') ?></th>
                                    <th><?= $this->Paginator->sort('nome_laudo') ?></th>
                                    <th><?= $this->Paginator->sort('conselho_laudo') ?></th>
                                    <th><?= $this->Paginator->sort('cpf') ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($medicoResponsaveis as $medicoResponsavei): ?>
                                    <tr>
                                        <td><?= $this->Number->format($medicoResponsavei->id) ?></td>
                                        <td><?= h($medicoResponsavei->conselho) ?></td>
                                        <td><?= h($medicoResponsavei->nome) ?></td>
                                        <td><?= h($medicoResponsavei->sigla) ?></td>
                                        <td><?= h($medicoResponsavei->nome_laudo) ?></td>
                                        <td><?= h($medicoResponsavei->conselho_laudo) ?></td>
                                        <td><?= h($medicoResponsavei->cpf) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link('<i class="fa fa-money"></i>', ['controller'=>'ProfissionalCreditos','action' => 'index','?'=> ['profissional_id'=>$medicoResponsavei->id,'first'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Créditos/Débitos', 'escape' => false, 'class' => 'btn btn-xs btn-info','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                            <?= $this->Html->link('<i class="fa fa-line-chart"></i>', ['controller'=>'ProdutividadeProfissionais','action' => 'index','?'=> ['profissional_id'=>$medicoResponsavei->id,'first'=>1]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Produtividade', 'escape' => false, 'class' => 'btn btn-xs btn-warning','data-toggle'=>'modal','data-target'=>'#modal_lg']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $medicoResponsavei->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $medicoResponsavei->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $medicoResponsavei->id),'toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

