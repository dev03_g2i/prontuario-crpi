<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Profissionais</h2>
        <ol class="breadcrumb">
            <li>Profissionais</li>
            <li class="active">
                <strong> Profissionais
                </strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="medicoResponsaveis form">
                        <?= $this->Form->create($medicoResponsavei) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Profissionais') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('conselho');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('sigla');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome_laudo');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('conselho_laudo');
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('cpf', ['mask' => 'cpf']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('cadastra_agenda', ['label' => 'Cadastrar Agenda?']);
                            echo "</div>";

                            ?>

                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

