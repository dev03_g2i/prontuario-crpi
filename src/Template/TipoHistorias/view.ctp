<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Historias - Visualizar</h2>
    </div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="tipoHistorias">
                        <h3><?= h($tipoHistoria->nome) ?></h3>
                        <table class="vertical-table">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($tipoHistoria->nome) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('User') ?></th>
                                <td><?= $tipoHistoria->has('user') ? $this->Html->link($tipoHistoria->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoHistoria->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Situacao Cadastro') ?></th>
                                <td><?= $tipoHistoria->has('situacao_cadastro') ? $this->Html->link($tipoHistoria->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoHistoria->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($tipoHistoria->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Created') ?></th>
                                <td><?= h($tipoHistoria->created) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Modified') ?></th>
                                <td><?= h($tipoHistoria->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

