
<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Histórias - Cadastrar</h2>
    </div>
    <div class="clearfix"></div>
</div>
<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="tipoHistorias form">
                        <?= $this->Form->create($tipoHistoria) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Tipo História') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('nome');
                            echo "</div>";
                            ?>
                        </fieldset>
                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

