<div class="border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Tipo Histórias</h2>
    </div>
    <div class="clearfix"></div>
</div>

<div class="area">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right btnAdd">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Tipo Histórias', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Tipo Histórias', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Tipo Histórias') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                                    <th><?= $this->Paginator->sort('nome') ?></th>
                                    <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                                    <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($tipoHistorias as $tipoHistoria): ?>
                                    <tr>
                                        <td><?= $this->Number->format($tipoHistoria->id) ?></td>
                                        <td><?= h($tipoHistoria->nome) ?></td>
                                        <td><?= $tipoHistoria->has('user') ? $this->Html->link($tipoHistoria->user->nome, ['controller' => 'Users', 'action' => 'view', $tipoHistoria->user->id]) : '' ?></td>
                                        <td><?= $tipoHistoria->has('situacao_cadastro') ? $this->Html->link($tipoHistoria->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $tipoHistoria->situacao_cadastro->id]) : '' ?></td>
                                        <td><?= h($tipoHistoria->created) ?></td>
                                        <td><?= h($tipoHistoria->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $tipoHistoria->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $tipoHistoria->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $tipoHistoria->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
echo $this->Html->scriptBlock(
    ' $(document).ready(function(){
        $(".paginator a").click(function(){
            $("div.table-responsive").load(this.href+".table-responsive");
            return false;
        })
    });',
    ['inline' => false, 'block' => 'scriptLast']
);
?>
