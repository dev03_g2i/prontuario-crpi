<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Link Ligacoes</h2>
            <ol class="breadcrumb">
                <li>Link Ligacoes</li>
                <li class="active">
                    <strong> Cadastrar Link Ligacoes
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="linkLigacoes form">
                            <?= $this->Form->create($linkLigaco) ?>
                            <fieldset>
                                <legend><?= __('Cadastrar Link Ligaco') ?></legend>
                                <?php
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('in_id',['label'=>'Grau','empty'=>'selecione','options'=>$grau]);
                                echo "</div>";
                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('out_id',['label'=>'Ligação','empty'=>'selecione','options'=>$grau]);
                                echo "</div>";

                                echo "<div class='col-md-6'>";
                                echo $this->Form->input('param',['label'=>'Parametro adicional','empty'=>'selecione','options'=>['M'=>'Masculino','F'=>'Feminino']]);
                                echo "</div>";
                                ?>

                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

