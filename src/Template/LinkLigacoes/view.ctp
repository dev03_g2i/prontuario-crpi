
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Link Ligacoes</h2>
        <ol class="breadcrumb">
            <li>Link Ligacoes</li>
            <li class="active">
                <strong>Litagem de Link Ligacoes</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Link Ligacoes</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $linkLigaco->has('situacao_cadastro') ? $this->Html->link($linkLigaco->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $linkLigaco->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $linkLigaco->has('user') ? $this->Html->link($linkLigaco->user->nome, ['controller' => 'Users', 'action' => 'view', $linkLigaco->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($linkLigaco->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('In Id') ?></th>
                                <td><?= $this->Number->format($linkLigaco->in_id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Out Id') ?></th>
                                <td><?= $this->Number->format($linkLigaco->out_id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($linkLigaco->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($linkLigaco->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


