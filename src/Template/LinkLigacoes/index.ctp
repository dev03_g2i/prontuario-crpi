<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Link Ligacoes</h2>
            <ol class="breadcrumb">
                <li>Link Ligacoes</li>
                <li class="active">
                    <strong>Litagem de Link Ligacoes</strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="wrapper wrapper-content area">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-right">
                    <p>
                        <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Link Ligacoes', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Link Ligacoes', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                    </p>
                </div>

                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= __('Link Ligacoes') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('in_id',['label'=>'Grau']) ?></th>
                                        <th><?= $this->Paginator->sort('out_id',['label'=>'Ligação']) ?></th>
                                        <th><?= $this->Paginator->sort('param',['label'=>'Parametro adicional']) ?></th>
                                        <th><?= $this->Paginator->sort('user_id') ?></th>
                                        <th><?= $this->Paginator->sort('created', ['label' => 'Dt. Criação']) ?></th>
                                        <th><?= $this->Paginator->sort('modified', ['label' => 'Dt. Modificação']) ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($linkLigacoes as $linkLigaco): ?>
                                        <tr>
                                            <td><?= $linkLigaco->has('grau_in') ? $this->Html->link($linkLigaco->grau_in->nome, ['controller' => 'GrauParentescos', 'action' => 'view', $linkLigaco->grau_in->id]) : '' ?></td>
                                            <td><?= $linkLigaco->has('grau_out') ? $this->Html->link($linkLigaco->grau_out->nome, ['controller' => 'GrauParentescos', 'action' => 'view', $linkLigaco->grau_out->id]) : '' ?></td>
                                            <td><?= h($linkLigaco->param) ?></td>
                                            <td><?= $linkLigaco->has('user') ? $this->Html->link($linkLigaco->user->nome, ['controller' => 'Users', 'action' => 'view', $linkLigaco->user->id]) : '' ?></td>
                                            <td><?= h($linkLigaco->created) ?></td>
                                            <td><?= h($linkLigaco->modified) ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $linkLigaco->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                                <?= $this->Html->link($this->Html->icon('remove'), '#Javascript:void(0)', ['onclick' => 'Deletar(\'linkLigacoes\',' . $linkLigaco->id . ',recarregar)', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger', 'listen' => 'f']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

