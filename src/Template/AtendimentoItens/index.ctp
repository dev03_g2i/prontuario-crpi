<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Itens</h2>
        <ol class="breadcrumb">
            <li>Atendimento Itens</li>
            <li class="active">
                <strong>Litagem de Atendimento Itens</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Cadastrar Atendimento Itens', ['action' => 'add'], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Atendimento Itens', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= __('Atendimento Itens') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('tipo') ?></th>
                                    <th><?= $this->Paginator->sort('descricao') ?></th>
                                    <th><?= $this->Paginator->sort('atendproc_id') ?></th>
                                    <th><?= $this->Paginator->sort('situacao_id') ?></th>

                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($atendimentoItens as $atendimentoIten): ?>
                                    <tr>
                                        <td><?= $this->Number->format($atendimentoIten->id) ?></td>
                                        <td><?= h($atendimentoIten->tipo) ?></td>
                                        <td><?= $atendimentoIten->has('dente') ? $this->Html->link($atendimentoIten->dente->descricao, ['controller' => 'Dentes', 'action' => 'view', $atendimentoIten->dente->id]) : '' ?></td>
                                        <td><?= $this->Number->format($atendimentoIten->atendproc_id) ?></td>
                                        <td><?= $atendimentoIten->has('situacao_cadastro') ? $this->Html->link($atendimentoIten->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $atendimentoIten->situacao_cadastro->id]) : '' ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('list-alt'), ['action' => 'view', $atendimentoIten->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Detalhes', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $atendimentoIten->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Form->postLink($this->Html->icon('remove'), ['action' => 'delete', $atendimentoIten->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $atendimentoIten->id), 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

