

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Itens</h2>
        <ol class="breadcrumb">
            <li>Atendimento Itens</li>
            <li class="active">
                <strong>Litagem de Atendimento Itens</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="atendimentoItens">
    <h3><?= h($atendimentoIten->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Tipo') ?></th>
            <td><?= h($atendimentoIten->tipo) ?></td>
        </tr>
        <tr>
            <th><?= __('Dente') ?></th>
            <td><?= $atendimentoIten->has('dente') ? $this->Html->link($atendimentoIten->dente->descricao, ['controller' => 'Dentes', 'action' => 'view', $atendimentoIten->dente->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situacao Cadastro') ?></th>
            <td><?= $atendimentoIten->has('situacao_cadastro') ? $this->Html->link($atendimentoIten->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $atendimentoIten->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('User') ?></th>
            <td><?= $atendimentoIten->has('user') ? $this->Html->link($atendimentoIten->user->nome, ['controller' => 'Users', 'action' => 'view', $atendimentoIten->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($atendimentoIten->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Atendproc Id') ?></th>
            <td><?= $this->Number->format($atendimentoIten->atendproc_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($atendimentoIten->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($atendimentoIten->modified) ?></td>
        </tr>
    </table>
</div>
</div>
</div>
</div>
</div>
</div>

