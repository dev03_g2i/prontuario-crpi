
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Atendimento Itens</h2>
        <ol class="breadcrumb">
            <li>Atendimento Itens</li>
            <li class="active">
                <strong>                        Editar Atendimento Itens
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="atendimentoItens form">
    <?= $this->Form->create($atendimentoIten) ?>
    <fieldset>
                <legend><?= __('Editar Atendimento Iten') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                echo $this->Form->input('tipo');
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('descricao', ['options' => $regioes]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                echo $this->Form->input('atendproc_id');
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('situacao_id', ['options' => $situacaoCadastros]);
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('user_id', ['options' => $users]);
                echo "</div>";
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

