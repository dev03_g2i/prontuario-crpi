<ul id="lightgallery" class="compare-image">
    <?php foreach ($lista_anexos as $lista_anexo):?>
        <li data-src="<?= $this->Url->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['fullBase' => true]) ?>" data-sub-html="<h4><?=$lista_anexo->descricao_anexo?></h4>">
            <a href="">
                <?= $this->Html->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['class' => 'img-responsive', 'id' => $lista_anexo->id]); ?>
            </a>
        </li>
    <?php endforeach;?>
</ul>
<script type="text/javascript">
    window.onload = function () {
        $('.compare-image li').each(function (i, v) {
            if(i == <?=$position?>){
                v.click();
            }
        })
    };
</script>
