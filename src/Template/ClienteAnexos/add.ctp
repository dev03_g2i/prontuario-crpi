<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Anexos</h2>
        <ol class="breadcrumb">
            <li>Cliente Anexos</li>
            <li class="active">
                <strong> Cadastrar Cliente Anexos
                </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="clienteAnexos form">
                        <?= $this->Form->create($clienteAnexo, ['id' => 'form-resp']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Anexo') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('descricao', ['label' => 'Descrição galeria']);
                            echo "</div>";

                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('tipoanexo_id', ['options' => $tipoAnexos, 'label' => 'Tipo Anexo', 'empty' => 'Selecione']);
                            echo "</div>";

                            echo $this->Form->input('cliente_id', ['type' => 'hidden', 'value' => $cliente]);
                            ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="files">Anexos</label>
                                    <input id="files" type="file" name="caminho[]" multiple class="input-file"
                                           data-overwrite-initial="false" data-min-file-count="1">
                                </div>
                            </div>
                        </fieldset>

                        <div class="progress" style="display: none;">
                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                <span class="sr-only">0% Complete</span>
                            </div>
                        </div>


                        <div class="col-md-12 text-right">
                            <?= $this->Form->submit(__('Salvar'), ['class' => 'btn btn-primary', 'onclick' => 'senduploads("form-resp")']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

