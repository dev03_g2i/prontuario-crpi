<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Paciente/Anexos</h2>
        <ol class="breadcrumb">
            <li>Paciente/Anexos</li>
            <li class="active">
                <strong>Detalhes do Anexos</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Data') ?></th>
                                <th><?= __('Paciente') ?></th>
                                <th><?= __('Tipo Anexo') ?></th>
                                <th><?= __('Quem cadastrou') ?></th>
                                <th><?= __('Data Modificação') ?></th>
                            </tr>
                            <tr>
                                <td><?= h($clienteAnexo->created) ?></td>
                                <td><?= $clienteAnexo->has('cliente') ? $this->Html->link($clienteAnexo->cliente->nome, ['controller' => 'Clientes', 'action' => 'view', $clienteAnexo->cliente->id]) : '' ?></td>
                                <td><?= $clienteAnexo->has('tipo_anexo') ? $this->Html->link($clienteAnexo->tipo_anexo->nome, ['controller' => 'TipoAnexos', 'action' => 'view', $clienteAnexo->tipo_anexo->id]) : '' ?></td>
                                <td><?= $clienteAnexo->has('user') ? $this->Html->link($clienteAnexo->user->nome, ['controller' => 'Users', 'action' => 'view', $clienteAnexo->user->id]) : '' ?></td>
                                <td><?= h($clienteAnexo->modified) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Descrição') ?></th>
                                <td colspan="5"><?= h($clienteAnexo->descricao) ?></td>
                            </tr>
                        </table>
                    </div>

                    <?php
                    if ($clienteAnexo->has('lista_anexos')): ?>
                        <div class="mail-attachment ">
                            <p>
                            <span><i class="fa fa-paperclip"></i> <?= count($clienteAnexo->lista_anexos) ?>
                                anexos - </span>
                            </p>
                            <div class="attachment">
                                <div id="lightBoxGallery">
                                    <?php foreach ($clienteAnexo->lista_anexos as $lista_anexo):
                                        $ext = explode('.', $lista_anexo->caminho);
                                        ?>
                                        <div class="file-box">
                                            <div class="file">
                                                <span class="corner"></span>
                                                <?php if (in_array($ext[count($ext)-1], ['jpg', 'gif', 'png'])) { ?>
                                                    <div class="image">
                                                        <a href="<?= $this->Url->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['fullBase' => true]) ?>"
                                                           title="<?= $lista_anexo->caminho ?>" class="lightview" data-lightview-group="clientes-galery<?=$lista_anexo->anexo_id?>" data-lightview-group-options="skin: 'mac'" listen="f">
                                                            <?= $this->Html->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['class' => 'img-fluid']); ?>
                                                        </a>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="icon">
                                                        <i class="fa fa-file"></i>
                                                    </div>
                                                <?php } ?>
                                                <div class="file-name">
                                                    <div class="col-md-6">
                                                        <?= $lista_anexo->caminho ?>
                                                    </div>
                                                    <div class="col-md-6 text-right">
                                                        <?= $this->Html->link('<i class="fa fa-cloud-download"></i>', ['controller' => 'ListaAnexos', 'action' => 'baixar', $lista_anexo->id], ['escape' => false, 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Download', 'class' => 'link-sm', 'listen' => 'f']) ?>
                                                    </div>
                                                    <br>
                                                    <small>Adicionado em: <?= $lista_anexo->created ?></small>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    <?php endforeach; ?>

                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

