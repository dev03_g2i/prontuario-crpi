<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Anexos</h2>
        <ol class="breadcrumb">
            <li>Cliente Anexos</li>
            <li class="active">
                <strong>Listagem de Cliente Anexos</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-right">
                <p>
                    <?= $this->Html->link('<i class="fa fa-photo"></i> Visualização Geral', ['action' => 'galeria', $cliente_id], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Visualização Geral', 'class' => 'btn btn-info', 'escape' => false, 'target' => '_blank', 'listen' => 'f']) ?>
                    <?= $this->Html->link($this->Html->icon('plus') . ' Novo', ['action' => 'add', $cliente_id], ['data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Cadastrar Anexos', 'class' => 'btn btn-primary', 'escape' => false]) ?>
                </p>
            </div>

            <div class="ibox float-e-margins">

                <div class="ibox-content">
                    <div class="clearfix">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('created', ['label' => 'Data']) ?></th>
                                    <th><?= $this->Paginator->sort('tipoanexo_id', ['label' => 'Tipo']) ?></th>
                                    <th><?= $this->Paginator->sort('descricao', ['label' => 'Descrição']) ?></th>
                                    <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado em']) ?></th>
                                    <th class="actions"><?= __('Ações') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($clienteAnexos as $clienteAnexo): ?>
                                    <tr>
                                        <td><?= h($clienteAnexo->created) ?></td>
                                        <td><?= $clienteAnexo->has('tipo_anexo') ? $this->Html->link($clienteAnexo->tipo_anexo->nome, ['controller' => 'TipoAnexos', 'action' => 'view', $clienteAnexo->tipo_anexo->id]) : '' ?></td>
                                        <td><?= h($clienteAnexo->descricao) ?></td>
                                        <td><?= h($clienteAnexo->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link($this->Html->icon('eye-open'), ['action' => 'view', $clienteAnexo->id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Visualizar anexos', 'escape' => false, 'class' => 'btn btn-xs btn-success']) ?>
                                            <?= $this->Html->link($this->Html->icon('picture'), ['controller' => 'ListaAnexos', 'action' => 'index', '?' => ['cliente_anexo' => $clienteAnexo->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Arquivos', 'escape' => false, 'class' => 'btn btn-xs btn-info']) ?>
                                            <?= $this->Html->link($this->Html->icon('pencil'), ['action' => 'edit', $clienteAnexo->id, $cliente_id], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Editar Galeria', 'escape' => false, 'class' => 'btn btn-xs btn-primary']) ?>
                                            <?= $this->Html->link($this->Html->icon('remove'), ['controller' => 'ListaAnexos', 'action' => 'index', '?' => ['cliente_anexo' => $clienteAnexo->id]], ['toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Deletar Arquivos', 'escape' => false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                            <div style="text-align:center">
                                <div class="paginator">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev($this->Html->icon('chevron-left'), ['escape' => false]) ?>
                                        <?= $this->Paginator->numbers() ?>
                                        <?= $this->Paginator->next($this->Html->icon('chevron-right'), ['escape' => false]) ?>
                                    </ul>
                                    <p><?= $this->Paginator->counter() ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

