<div class="container-fixed">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-8">
            <h2>Anexos</h2>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-8">
                            <?php
                            $cliente = null;
                            foreach ($clienteAnexos as $clienteAnexo){$cliente = $clienteAnexo->cliente->nome;}?>
                            <?php echo ($cliente) ? "<h2 class='text-uppercase'><strong>Paciente:</strong> $cliente</h2>" : '';?>
                        </div>
                        <div class="col-md-4">
                            <div class="alert alert-warning" role="alert">
                                Selecione duas imagens para comparação!
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading title-tipos">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseImagem" aria-expanded="true">
                                    <h4 class="panel-title text-center">Imagens <span class="badge badge-warning"><?=$countImagens?></span></h4>
                                </a>
                            </div>
                            <div id="collapseImagem" class="panel-collapse collapse" aria-expanded="true">
                                <div class="panel-body">
                                    <?php foreach ($clienteAnexoImages as $clienteAnexoImage): ?>
                                        <div class="container-galeria">
                                            <div class="row" style="margin-bottom: -10px">
                                                <div class="col-md-9 col-sm-6 col-xs-6">
                                                    <h3 class="title-galeria"><?php echo $this->Time->format($clienteAnexoImage->created, 'dd/MM/YYYY').'  '.$clienteAnexoImage->descricao ?> </h3>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-6 text-right">
                                                    <?php echo $this->Form->button('<i class="fa fa-image"></i> Abrir Galeria', ['type' => 'button', 'escape' => false, 'class' => 'btn btn-primary btnAbrirGalreia'])?>
                                                </div>
                                            </div>
                                            <ul class="galeria-oftalmo">
                                                <?php foreach ($clienteAnexoImage->lista_anexos as $i => $lista_anexo):
                                                    $ext = explode('.', $lista_anexo->caminho);
                                                    if (in_array($ext[count($ext)-1], ['jpg', 'gif', 'png'])): ?>
                                                        <li data-src="<?= $this->Url->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['fullBase' => true]) ?>" data-sub-html="<h4><?=$lista_anexo->descricao_anexo?></h4>">
                                                            <?php
                                                            echo $this->Html->link(
                                                                $this->Html->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho,
                                                                    ['class' => 'img-responsive tipo-image', 'id' => $lista_anexo->anexo_id]), ['action' => 'compareImage', $lista_anexo->id],
                                                                ['escape' => false, 'position' => $i]
                                                            );
                                                            ?>
                                                            <span class="footer-galeria"><?=$lista_anexo->caminho?></span>
                                                        </li>
                                                    <?php else: ?>
                                                        <li data-src="https://images.vexels.com/media/users/3/135092/isolated/preview/8d5eef2139fe9c3c94dbf880f0bc8cc7-documento-icono-del-archivo-de-sombra-by-vexels.png">
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].':7878/prontuario'?>/webroot/files/listaanexos/caminho/<?=$lista_anexo->caminho_dir?>/<?=$lista_anexo->caminho?>" target="_blank">
                                                                <img class="img-responsive" src="https://images.vexels.com/media/users/3/135092/isolated/preview/8d5eef2139fe9c3c94dbf880f0bc8cc7-documento-icono-del-archivo-de-sombra-by-vexels.png">
                                                            </a>
                                                            <span class="footer-galeria"><?=$lista_anexo->caminho?></span>
                                                        </li>
                                                    <?php endif; endforeach;?>
                                            </ul>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading title-tipos">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseExames" aria-expanded="false" class="collapsed">
                                    <h5 class="panel-title text-center">Exames <span class="badge badge-warning"><?=$countExames?></span></h5>
                                </a>
                            </div>
                            <div id="collapseExames" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <?php foreach ($clienteAnexoExames as $clienteAnexoExame): ?>
                                        <div class="container-galeria">
                                            <div class="row" style="margin-bottom: -10px">
                                                <div class="col-md-9 col-sm-6 col-xs-6">
                                                    <h3 class="title-galeria"><?php echo $this->Time->format($clienteAnexoExame->created, 'dd/MM/YYYY').'  '.$clienteAnexoExame->descricao ?> </h3>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-6 text-right">
                                                    <?php echo $this->Form->button('<i class="fa fa-image"></i> Abrir Galeria', ['type' => 'button', 'escape' => false, 'class' => 'btn btn-primary btnAbrirGalreia'])?>
                                                </div>
                                            </div>
                                            <ul class="galeria-oftalmo">
                                                <?php foreach ($clienteAnexoExame->lista_anexos as $i => $lista_anexo):
                                                    $ext = explode('.', $lista_anexo->caminho);
                                                    if (in_array($ext[count($ext)-1], ['jpg', 'gif', 'png'])): ?>
                                                        <li data-src="<?= $this->Url->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['fullBase' => true]) ?>" data-sub-html="<h4><?=$lista_anexo->descricao_anexo?></h4>">
                                                            <?php
                                                            echo $this->Html->link(
                                                                $this->Html->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho,
                                                                    ['class' => 'img-responsive tipo-image', 'id' => $lista_anexo->anexo_id]), ['action' => 'compareImage', $lista_anexo->id],
                                                                ['escape' => false, 'position' => $i]
                                                            );
                                                            ?>
                                                            <span class="footer-galeria"><?=$lista_anexo->caminho?></span>
                                                        </li>
                                                    <?php else: ?>
                                                        <li data-src="https://images.vexels.com/media/users/3/135092/isolated/preview/8d5eef2139fe9c3c94dbf880f0bc8cc7-documento-icono-del-archivo-de-sombra-by-vexels.png">
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].':7878/prontuario'?>/webroot/files/listaanexos/caminho/<?=$lista_anexo->caminho_dir?>/<?=$lista_anexo->caminho?>" target="_blank">
                                                                <img class="img-responsive" src="https://images.vexels.com/media/users/3/135092/isolated/preview/8d5eef2139fe9c3c94dbf880f0bc8cc7-documento-icono-del-archivo-de-sombra-by-vexels.png">
                                                            </a>
                                                            <span class="footer-galeria"><?=$lista_anexo->caminho?></span>
                                                        </li>
                                                    <?php endif; endforeach;?>
                                            </ul>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading title-tipos">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseDocumentos" aria-expanded="false" class="collapsed">
                                    <h5 class="panel-title text-center">Documentos <span class="badge badge-warning"><?=$countDocumentos?></span></h5>
                                </a>
                            </div>
                            <div id="collapseDocumentos" class="panel-collapse collapse" aria-expanded="false">
                                <div class="panel-body">
                                    <?php foreach ($clienteAnexoDocs as $clienteAnexoDoc): ?>
                                        <div class="container-galeria">
                                            <div class="row" style="margin-bottom: -10px">
                                                <div class="col-md-9 col-sm-6 col-xs-6">
                                                    <h3 class="title-galeria"><?php echo $this->Time->format($clienteAnexoDoc->created, 'dd/MM/YYYY').'  '.$clienteAnexoDoc->descricao ?> </h3>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-6 text-right">
                                                    <?php echo $this->Form->button('<i class="fa fa-image"></i> Abrir Galeria', ['type' => 'button', 'escape' => false, 'class' => 'btn btn-primary btnAbrirGalreia'])?>
                                                </div>
                                            </div>
                                            <ul class="galeria-oftalmo">
                                                <?php
                                                foreach ($clienteAnexoDoc->lista_anexos as $i => $lista_anexo):
                                                    $ext = explode('.', $lista_anexo->caminho);
                                                    if (in_array($ext[count($ext)-1], ['jpg', 'gif', 'png'])): ?>
                                                        <li data-src="<?= $this->Url->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho, ['fullBase' => true]) ?>" data-sub-html="<h4><?=$lista_anexo->descricao_anexo?></h4>">
                                                            <?php
                                                            echo $this->Html->link(
                                                                $this->Html->image('/files/listaanexos/caminho/' . $lista_anexo->caminho_dir . '/' . $lista_anexo->caminho,
                                                                    ['class' => 'img-responsive tipo-image', 'id' => $lista_anexo->anexo_id]), ['action' => 'compareImage', $lista_anexo->id],
                                                                ['escape' => false, 'position' => $i]
                                                            );
                                                            ?>
                                                            <span class="footer-galeria"><?=$lista_anexo->caminho?></span>
                                                        </li>
                                                    <?php else: ?>
                                                        <li data-src="https://images.vexels.com/media/users/3/135092/isolated/preview/8d5eef2139fe9c3c94dbf880f0bc8cc7-documento-icono-del-archivo-de-sombra-by-vexels.png">
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].':7878/prontuario'?>/webroot/files/listaanexos/caminho/<?=$lista_anexo->caminho_dir?>/<?=$lista_anexo->caminho?>" target="_blank">
                                                                <img class="img-responsive" src="https://images.vexels.com/media/users/3/135092/isolated/preview/8d5eef2139fe9c3c94dbf880f0bc8cc7-documento-icono-del-archivo-de-sombra-by-vexels.png">
                                                            </a>
                                                            <span class="footer-galeria"><?=$lista_anexo->caminho?></span>
                                                        </li>
                                                    <?php endif; endforeach;?>
                                            </ul>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>