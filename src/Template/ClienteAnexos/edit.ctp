
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cliente Anexos</h2>
        <ol class="breadcrumb">
            <li>Cliente Anexos</li>
            <li class="active">
                <strong>                        Editar Cliente Anexos
                    </strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content area_pront">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="clienteAnexos form">
    <?= $this->Form->create($clienteAnexo,['id'=>'form-resp']) ?>
    <fieldset>
                <legend><?= __('Editar Anexo') ?></legend>
                <?php
                echo "<div class='col-md-6'>";
                echo $this->Form->input('descricao',['label'=>'Descrição']);
                echo "</div>";
                echo "<div class='col-md-6'>";
                    echo $this->Form->input('tipoanexo_id', ['options' => $tipoAnexos, 'label' => 'Tipo Anexo']);
                echo "</div>";

                echo $this->Form->input('cliente_id', ['type' => 'hidden']);
               
        ?>

    </fieldset>
    <div class="col-md-12 text-right">
        <?= $this->Form->submit(__('Salvar'),['class'=>'btn btn-primary', 'onclick' => 'EnviarForm("form-resp")']) ?>
    </div>
    <div class="clearfix"></div>
    <?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
</div>
</div>

