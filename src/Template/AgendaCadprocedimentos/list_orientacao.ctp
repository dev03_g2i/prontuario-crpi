<?php if(!empty($agendaCadproc)):?>
    <ul class="list-group">
        <?php foreach ($agendaCadproc as $agendaCad):?>
            <li class="list-group-item">
                <h4><?=$agendaCad->nome?></h4>
                <?php
                    if(!empty($agendaCad->orientacao)):
                        echo '<small>'.nl2br($agendaCad->orientacao).'</small>';
                        //echo '<div class="text-right">'.$this->Html->link('<i class="fa fa-pencil-square"></i>', ['action' => 'orientacao', $agendaCad->id], ['toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Editar orientação', 'class' => 'btn btn-warning btn-xs', 'escape' => false, 'target' => '_blank']).'</div>';
                    else:
                        echo '<small>(não há registros!)</small>';
                        //echo '<div class="text-right">'.$this->Html->link('<i class="fa fa-plus-square"></i>', ['action' => 'orientacao', $agendaCad->id], ['toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Cadastrar orientação', 'class' => 'btn btn-primary btn-xs', 'escape' => false, 'target' => '_blank']).'</div>';
                    endif;
                ?>
            </li>
        <?php endforeach; ?>
    </ul>
<?php endif;?>
