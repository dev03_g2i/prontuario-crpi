
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Cadprocedimentos</h2>
        <ol class="breadcrumb">
            <li>Agenda Cadprocedimentos</li>
            <li class="active">
                <strong>Litagem de Agenda Cadprocedimentos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Agenda Cadprocedimentos</h3>
                        <table class="table table-hover">
                            <tr>
                                <th><?= __('Nome') ?></th>
                                <td><?= h($agendaCadprocedimento->nome) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Situação Cadastro') ?></th>
                                                                <td><?= $agendaCadprocedimento->has('situacao_cadastro') ? $this->Html->link($agendaCadprocedimento->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $agendaCadprocedimento->situacao_cadastro->id]) : '' ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Quem Cadastrou') ?></th>
                                                                <td><?= $agendaCadprocedimento->has('user') ? $this->Html->link($agendaCadprocedimento->user->nome, ['controller' => 'Users', 'action' => 'view', $agendaCadprocedimento->user->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($agendaCadprocedimento->id) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Criação') ?></th>
                                                                <td><?= h($agendaCadprocedimento->created) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Dt. Modificação') ?></th>
                                                                <td><?= h($agendaCadprocedimento->modified) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


