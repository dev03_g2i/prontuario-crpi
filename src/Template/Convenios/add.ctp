<div class="this-place">
    <?= $this->Form->create($convenio) ?>
    <div class="wrapper white-bg page-heading">
        <div class="col-sm-9">
            <h2>Convênios</h2>
            <ol class="breadcrumb">
                <li>Convênios</li>
                <li class="active">
                    <strong> Cadastrar Convênios
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="mastologias form">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Dados do Convênio') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class='col-sm-6'>
                                            <?php echo $this->Form->input('nome'); ?>
                                        </div>
                                        <div class='col-sm-6'>
                                            <?php echo $this->Form->input('tipo_id', ['options' => $tipoConvenios]); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('Atendimento') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="col-sm-2">
                                            <?php echo $this->Form->input('usa_tiss', ['label' => ' Usa Tiss?', 'type' => 'checkbox']); ?>
                                        </div>
                                        <div class="col-sm-2">
                                            <?php echo $this->Form->input('usa_cobranca', ['label' => ' Usa Cobrança?', 'type' => 'checkbox']); ?>
                                        </div>
                                        <div class="col-sm-2">
                                            <?php echo $this->Form->input('obrigatorio_nr_guia', ['label' => ' Obrigatório nr guia?', 'type' => 'checkbox']); ?>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php echo $this->Form->input('obrigatorio_autorizacao_senha', ['label' => ' Obrigatório autorização senha?', 'type' => 'checkbox']); ?>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php echo $this->Form->input('obrigatorio_carteira_paciente', ['label' => ' Obrigatório carteira paciente?', 'type' => 'checkbox']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title fundoHeaders">
                                <h5 class="fontHeaders"><?= __('TISS') ?></h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class='col-sm-4'>
                                            <?php echo $this->Form->input('tiss_registroans', ['label' => 'Registro ANS']); ?>
                                        </div>
                                        <div class='col-sm-4'>
                                            <?php echo $this->Form->input('tiss_tipocodigo', ['label' => 'Tipo Cód.', 'type' => 'select', 'options' => $tipoCodClinica]); ?>
                                        </div>
                                        <div class='col-sm-4'>
                                            <?php echo $this->Form->input('tiss_codcliconvenio', ['label' => 'Cód. Clínica Convênio']); ?>
                                        </div>
                                        <div class='col-sm-3'>
                                            <?php echo $this->Form->input('tiss_codtabpreco', ['label' => 'Cód. Tab. Preço']); ?>
                                        </div>
                                        <div class='col-sm-3'>
                                            <?php echo $this->Form->input('tiss_codtabmedicamento', ['label' => 'Cód Tab. Medicamento']); ?>
                                        </div>
                                        <div class='col-sm-3'>
                                            <?php echo $this->Form->input('tiss_codtabmaterial', ['label' => 'Cód Tab. Material']); ?>
                                        </div>
                                        <div class='col-sm-3'>
                                            <?php echo $this->Form->input('tiss_codtaxas', ['label' => 'Cód. Taxas']); ?>
                                        </div>
                                    </div>
                                    <?php if(!$this->Configuracao->usaConvenioCadastroContrato()): ?>
                                        <div class="col-sm-12 text-right">
                                            <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary']); ?>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($this->Configuracao->usaConvenioCadastroContrato()): ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title fundoHeaders">
                                    <h5 class="fontHeaders"><?= __('Contrato/Faturamento') ?></h5>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-4">
                                                <?= $this->Form->input('bank_id', ['label' => 'Local pgto','type' => 'select', 'data' => 'select', 'controller' => 'Banco', 'action' => 'fill']);?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $this->Form->input('pymt_date', ['label' => 'Dias para pgto', 'min' => 0]);?>
                                            </div>
                                            <div class="col-sm-4">
                                                <?= $this->Form->input('pymt_hist', ['label' => 'Plano de contas', 'options' => $planAcc]);?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $this->Form->input('tel', ['label' => 'Telefones']);?>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $this->Form->input('contact', ['label' => 'Contato']);?>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-right">
                                            <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['type' => 'submit', 'class' => 'btn btn-primary']); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>