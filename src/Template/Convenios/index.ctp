<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Convênios</h2>
        <ol class="breadcrumb">
            <li>Convênios</li>
            <li class="active">
                <strong>Listagem de Convênios</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>

<div class="row">
    <div class="col-lg-12">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filtros</h5>
            </div>
            <div class="ibox-content">
                <div class="filtros">
                    <?= $this->Form->create('Convenios',['type'=>'get', 'id' => 'form-faturamentos']) ?>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('nome', ['name' => 'Convenios__nome'])?>
                    </div>
                    <div class="col-md-4" style="margin-top: 22px">
                        <?= $this->Form->button($this->Html->icon('search'), ['type'=>'button','id'=>'btn-fill','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Pesquisar','escape' => false]) ?>
                        <?= $this->Form->button($this->Html->icon('refresh'), ['type'=>'button','id'=>'btn-refresh','class' => 'btn btn-default','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Limpar Filtros','escape' => false]) ?>
                    </div>
                    <?= $this->Form->end()?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="text-right">
            <p>
                <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Convenios', ['action' => 'add'],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Convenios','class'=>'btn btn-primary','escape' => false]) ?>
            </p>
        </div>

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= __('Convênios') ?></h5>
            </div>
            <div class="ibox-content">
                <div class="clearfix">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th><?= $this->Paginator->sort('id', ['label' => 'Codigo']) ?></th>
                                <th><?= $this->Paginator->sort('nome') ?></th>
                                <th><?= $this->Paginator->sort('tipo_id') ?></th>
                                <th><?= $this->Paginator->sort('situacao_id', ['label' => 'Situação']) ?></th>
                                <th><?= $this->Paginator->sort('user_id', ['label' => 'Usuário']) ?></th>
                                <th><?= $this->Paginator->sort('created', ['label' => 'Criado']) ?></th>
                                <th><?= $this->Paginator->sort('modified', ['label' => 'Modificado']) ?></th>
                                <th class="actions"><?= __('Ações') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($convenios as $convenio):
                                $procedimento_id = '';
                                foreach ($convenio->preco_procedimentos as $pp){
                                    $procedimento_id = $pp->procedimento_id;
                                };
                                ?>
                                <tr>
                                    <td><?= $this->Number->format($convenio->id) ?></td>
                                    <td><?= h($convenio->nome) ?></td>
                                    <td><?= $convenio->has('tipo_convenio') ? $this->Html->link($convenio->tipo_convenio->nome, ['controller' => 'TipoConvenios', 'action' => 'view', $convenio->tipo_convenio->id]) : '' ?></td>
                                    <td><?= $convenio->has('situacao_cadastro') ? $this->Html->link($convenio->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $convenio->situacao_cadastro->id]) : '' ?></td>
                                    <td><?= $convenio->has('user') ? $this->Html->link($convenio->user->nome, ['controller' => 'Users', 'action' => 'view', $convenio->user->id]) : '' ?></td>
                                    <td><?= h($convenio->created) ?></td>
                                    <td><?= h($convenio->modified) ?></td>
                                    <td class="actions">
                                        <div class="dropdown">
                                            <button class="btn btn-info btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><span class="fa fa-list"></span> Opções <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu dropdown-prontuario">
                                                <li><?= $this->Html->link($this->Html->icon('pencil').' Editar', ['action' => 'edit', $convenio->id],['escape' => false]) ?></li>
                                                <li><?= $this->Form->postLink($this->Html->icon('remove').' Deletar', ['action' => 'delete', $convenio->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $convenio->id), 'escape' => false]) ?></li>
                                                <li><?= $this->Html->link('<i class="fa fa-money"></i> Preços Procedimentos', ['controller' => 'PrecoProcedimentos', 'action' => 'all', '?'=>['convenio'=>$convenio->id]],['escape' => false, 'target' => '_blank']) ?></li>
                                                <li><?= $this->Html->link('<i class="fa fa-dollar"></i> Preços Artigos', ['controller' => 'FaturaPrecoartigo', 'action' => 'index', '?' => ['convenio_id' => $convenio->id]],['escape' => false, 'target' => '_blank']) ?></li>
                                                <li><?= $this->Html->link('<i class="fa fa-clipboard"></i> Parametrização de Impostos', ['controller' => 'ConvenioImpostosParametro', 'action' => 'index', $convenio->id],['escape' => false, 'target' => '_blank']) ?></li>
                                                <li><?= $this->Html->link('<i class="fa fa-line-chart"></i> Indices de Tabelas', ['controller' => 'ConvenioIndicetabelas', 'action' => 'index', $convenio->id],['escape' => false, 'target' => '_blank']) ?></li>
                                                <li><?= $this->Html->link('<i class="fa fa-reply"></i> Recalcular Exames', ['action' => 'newAdd', $convenio->id],['escape' => false, 'target' => '_blank']) ?></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <div style="text-align:center">
                            <div class="paginator">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                    <?= $this->Paginator->numbers() ?>
                                    <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                </ul>
                                <p><?= $this->Paginator->counter() ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


