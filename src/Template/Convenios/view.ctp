

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Convênios</h2>
        <ol class="breadcrumb">
            <li>Convênios</li>
            <li class="active">
                <strong>Listagem de Convênios</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
<div class="convenios">
    <h3><?= h($convenio->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nome') ?></th>
            <td><?= h($convenio->nome) ?></td>
        </tr>
        <tr>
            <th><?= __('Tipo Convênio') ?></th>
            <td><?= $convenio->has('tipo_convenio') ? $this->Html->link($convenio->tipo_convenio->id, ['controller' => 'TipoConvenios', 'action' => 'view', $convenio->tipo_convenio->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Situação Cadastro') ?></th>
            <td><?= $convenio->has('situacao_cadastro') ? $this->Html->link($convenio->situacao_cadastro->nome, ['controller' => 'SituacaoCadastros', 'action' => 'view', $convenio->situacao_cadastro->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Usuário') ?></th>
            <td><?= $convenio->has('user') ? $this->Html->link($convenio->user->nome, ['controller' => 'Users', 'action' => 'view', $convenio->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Codigo') ?></th>
            <td><?= $this->Number->format($convenio->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Criado') ?></th>
            <td><?= h($convenio->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modificado') ?></th>
            <td><?= h($convenio->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Atendimentos') ?></h4>
        <?php if (!empty($convenio->atendimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id', ['label' => 'Codigo']) ?></th>
                <th><?= __('Hora') ?></th>
                <th><?= __('Data') ?></th>
                <th><?= __('Observacao', ['label' => 'Observação']) ?></th>
                <th><?= __('Cliente Id') ?></th>
                <th><?= __('Unidade Id') ?></th>
                <th><?= __('Total Liquido', ['label' => 'Total Líquido']) ?></th>
                <th><?= __('Desconto') ?></th>
                <th><?= __('Total Geral') ?></th>
                <th><?= __('Convenio Id') ?></th>
                <th><?= __('Total Pagoato') ?></th>
                <th><?= __('Tipoatendimento Id') ?></th>
                <th><?= __('Num Ficha Externa') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($convenio->atendimentos as $atendimentos): ?>
            <tr>
                <td><?= h($atendimentos->id) ?></td>
                <td><?= h($atendimentos->hora) ?></td>
                <td><?= h($atendimentos->data) ?></td>
                <td><?= h($atendimentos->observacao) ?></td>
                <td><?= h($atendimentos->cliente_id) ?></td>
                <td><?= h($atendimentos->unidade_id) ?></td>
                <td><?= h($atendimentos->total_liquido) ?></td>
                <td><?= h($atendimentos->desconto) ?></td>
                <td><?= h($atendimentos->total_geral) ?></td>
                <td><?= h($atendimentos->convenio_id) ?></td>
                <td><?= h($atendimentos->total_pagoato) ?></td>
                <td><?= h($atendimentos->tipoatendimento_id) ?></td>
                <td><?= h($atendimentos->num_ficha_externa) ?></td>
                <td><?= h($atendimentos->situacao_id) ?></td>
                <td><?= h($atendimentos->user_id) ?></td>
                <td><?= h($atendimentos->created) ?></td>
                <td><?= h($atendimentos->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'Atendimentos','action' => 'view', $atendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'Atendimentos','action' => 'edit', $atendimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'Atendimentos','action' => 'delete', $atendimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $convenio->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Preco Procedimentos') ?></h4>
        <?php if (!empty($convenio->preco_procedimentos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Convenio Id') ?></th>
                <th><?= __('Procedimento Id') ?></th>
                <th><?= __('Valor Faturar') ?></th>
                <th><?= __('Valor Particular') ?></th>
                <th><?= __('User Id') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th><?= __('Situacao Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($convenio->preco_procedimentos as $precoProcedimentos): ?>
            <tr>
                <td><?= h($precoProcedimentos->id) ?></td>
                <td><?= h($precoProcedimentos->convenio_id) ?></td>
                <td><?= h($precoProcedimentos->procedimento_id) ?></td>
                <td><?= h($precoProcedimentos->valor_faturar) ?></td>
                <td><?= h($precoProcedimentos->valor_particular) ?></td>
                <td><?= h($precoProcedimentos->user_id) ?></td>
                <td><?= h($precoProcedimentos->created) ?></td>
                <td><?= h($precoProcedimentos->modified) ?></td>
                <td><?= h($precoProcedimentos->situacao_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link($this->Html->icon('list-alt'), ['controller' => 'PrecoProcedimentos','action' => 'view', $precoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                    <?= $this->Html->link($this->Html->icon('pencil'), ['controller' => 'PrecoProcedimentos','action' => 'edit', $precoProcedimentos->id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                    <?= $this->Form->postLink($this->Html->icon('remove'), ['controller' => 'PrecoProcedimentos','action' => 'delete', $precoProcedimentos->id], ['confirm' => __('Você tem certeza que deseja excluir o registro # {0}?', $convenio->id),'data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

