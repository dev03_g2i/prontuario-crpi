<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Recalcular Fatura</h2>
            <p>Convênio: <?=$convenio->nome; ?></p>
        </div>
        <div class="col-lg-3">
            <br><br>
            <p class="text-warning text-right">Apenas itens não faturados!</p>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="area">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="faturaRecalcular form">
                            <?= $this->Form->create('FaturaCalcular', ['class' => 'validateDateInicioFim']) ?>
                            <fieldset>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('data_inicio', ['type' => 'text', 'class' => 'datepicker', 'id' => 'inicio']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('unidade', ['options' => $unidades]); ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('data_fim', ['type' => 'text', 'class' => 'datepicker', 'id' => 'fim']); ?>
                                </div>
                                <div class='col-md-6'>
                                    <?=$this->Form->input('grupo_procedimentos', ['options' => $grupo_procedimentos]); ?>
                                </div>
                            </fieldset>
                            <div class="col-md-12 text-right">
                                <?= $this->Form->submit(__('Recalcular'),['class'=>'btn btn-primary']) ?>
                            </div>
                            <div class="clearfix"></div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>