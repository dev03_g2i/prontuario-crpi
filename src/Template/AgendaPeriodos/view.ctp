
<div class="this-place">
<div class="wrapper white-bg page-heading">
    <div class="col-lg-9">
        <h2>Agenda Periodos</h2>
        <ol class="breadcrumb">
            <li>Agenda Periodos</li>
            <li class="active">
                <strong>Litagem de Agenda Periodos</strong>
            </li>
        </ol>
    </div>
    <div class="clearfix"></div>
</div>
<div class="wrapper wrapper-content area">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <h3>Detalhes Agenda Periodos</h3>
                        <table class="table table-hover">
                            <tr>
                                                                <th><?= __('Grupo Agenda') ?></th>
                                                                <td><?= $agendaPeriodo->has('grupo_agenda') ? $this->Html->link($agendaPeriodo->grupo_agenda->nome, ['controller' => 'GrupoAgendas', 'action' => 'view', $agendaPeriodo->grupo_agenda->id]) : '' ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Id') ?></th>
                                <td><?= $this->Number->format($agendaPeriodo->id) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Dia Semana') ?></th>
                                <td><?= $this->Number->format($agendaPeriodo->dia_semana) ?></td>
                            </tr>
                            <tr>
                                <th><?= __('Periodo') ?></th>
                                <td><?= $this->Number->format($agendaPeriodo->periodo) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Intervalo') ?></th>
                                                                <td><?= h($agendaPeriodo->intervalo) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Inicio') ?></th>
                                                                <td><?= h($agendaPeriodo->inicio) ?></td>
                            </tr>
                            <tr>
                                                                <th><?= __('Fim') ?></th>
                                                                <td><?= h($agendaPeriodo->fim) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

</div>
</div>
</div>
</div>


