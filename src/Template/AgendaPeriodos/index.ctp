<div class="this-place">
    <div class="white-bg page-heading">
        <div class="col-lg-9">
            <h2>Período Atendimento</h2>
           <strong>Agenda: </strong> <?= $this->Agenda->getGrupoAgenda($grupo_agenda_id)?>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="area">
        <div class="row">
            <div class="col-lg-12">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filtros</h5>
                </div>
                <div class="ibox-content">
                    <div class="filtros">
                        <?= $this->Form->create('Grupo Agenda', ['type' => 'get']) ?>
                        <div class="col-md-4">
                            <?php echo $this->Form->input('AgendaPeriodos__dia_semana', ['options' => $this->Data->diasSemana(), 'empty' => 'Selecione', 'label' => 'Dia Semana']); ?>
                        </div>
                        <div class="col-md-4" style="margin-top: 24px;">
                            <?= $this->Form->button($this->Html->icon('search'), ['type' => 'button', 'id' => 'fill-modal', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Pesquisar', 'escape' => false]) ?>
                            <?= $this->Form->button($this->Html->icon('refresh'), ['type' => 'button', 'id' => 'refresh-modal', 'class' => 'btn btn-default', 'toggle' => 'tooltip', 'data-placement' => 'bottom', 'title' => 'Limpar Filtros', 'escape' => false]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title text-right">
                        <p>
                            <?= $this->Html->link($this->Html->icon('plus').' Cadastrar Periodos', ['action' => 'add', $grupo_agenda_id],['data-toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Cadastrar Agenda Periodos','class'=>'btn btn-primary','escape' => false]) ?>
                        </p>
                    </div>
                    <div class="ibox-content">
                        <div class="clearfix">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= $this->Paginator->sort('id') ?></th>
                                        <th><?= $this->Paginator->sort('dia_semana') ?></th>
                                        <th><?= $this->Paginator->sort('periodo') ?></th>
                                        <th><?= $this->Paginator->sort('inicio') ?></th>
                                        <th><?= $this->Paginator->sort('fim') ?></th>
                                        <th><?= $this->Paginator->sort('tipo_agenda_id', 'Tipo agenda') ?></th>
                                        <th><?= $this->Paginator->sort('intervalo') ?></th>
                                        <th class="actions"><?= __('Ações') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($agendaPeriodos as $agendaPeriodo): ?>
                                        <?php //TODO deletar registro dentro dos modais ?>
                                        <tr class="delete<?=$agendaPeriodo->id?>">
                                            <td><?= $this->Number->format($agendaPeriodo->id) ?></td>
                                            <td><?= $this->Data->diasSemana($agendaPeriodo->dia_semana) ?></td>
                                            <td><?= $this->Data->periodos($agendaPeriodo->periodo) ?></td>
                                            <td><?= $this->Time->format($agendaPeriodo->inicio,'HH:mm:ss') ?></td>
                                            <td><?= $this->Time->format($agendaPeriodo->fim,'HH:mm:ss') ?></td>
                                            <td><?= $agendaPeriodo->has('tipo_agenda') ? $this->Html->link($agendaPeriodo->tipo_agenda->nome, ['controller' => 'TipoAgendas', 'action' => 'view', $agendaPeriodo->tipo_agenda->id]) : '' ?></td>
                                            <td><?=  $this->Time->format($agendaPeriodo->intervalo,'HH:mm:ss'); ?></td>
                                            <td class="actions">
                                                <?= $this->Html->link($this->Html->icon('list-alt'), ['controller'=>'agendaPeriodos','action' => 'view', $agendaPeriodo->id],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Detalhes','escape' => false,'class'=>'btn btn-xs btn-info']) ?>
                                                <?= $this->Html->link($this->Html->icon('pencil'), ['controller'=>'agendaPeriodos','action' => 'edit', $agendaPeriodo->id, '?' => ['grupo_agenda_id' => $grupo_agenda_id]],['toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Editar','escape' => false,'class'=>'btn btn-xs btn-primary']) ?>
                                                <?php //TODO deletar registro dentro dos modais ?>
                                                <?= $this->Form->button($this->Html->icon('remove'), ['onclick'=>'DeletarModal(\'agendaPeriodos\','.$agendaPeriodo->id.')','toggle'=>'tooltip','data-placement'=>'bottom','title'=>'Deletar','escape' => false,'class'=>'btn btn-xs btn-danger']) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                                <div style="text-align:center">
                                    <div class="paginator">
                                        <ul class="pagination">
                                            <?= $this->Paginator->prev($this->Html->icon('chevron-left'),['escape' => false]) ?>
                                            <?= $this->Paginator->numbers() ?>
                                            <?= $this->Paginator->next($this->Html->icon('chevron-right'),['escape' => false]) ?>
                                        </ul>
                                        <p><?= $this->Paginator->counter() ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
