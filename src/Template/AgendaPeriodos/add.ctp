<div class="this-place">
    <div class="wrapper white-bg page-heading">
        <div class="col-lg-9">
            <h2>Agenda Periodos</h2>
            <ol class="breadcrumb">
                <li>Agenda Periodos</li>
                <li class="active">
                    <strong>
                        Cadastrar Agenda Periodos
                    </strong>
                </li>
            </ol>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="row area">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="agendaPeriodos form">
                        <?= $this->Form->create($agendaPeriodo, ['id' => 'frm-agenda-periodos']) ?>
                        <fieldset>
                            <legend><?= __('Cadastrar Agenda Periodo') ?></legend>
                            <?php
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('dia_semana', ['type' => 'select', 'options' => $this->Data->diasSemana()]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('periodo', ['type' => 'select', 'options' => $this->Data->periodos()]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('inicio',['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('fim',['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('tipo_agenda_id',['type'=>'select','options'=> $tipo_agendas,'empty'=>'Selecione']);
                            echo "</div>";
                            echo "<div class='col-md-6'>";
                            echo $this->Form->input('intervalo',['class'=>'timepiker','type'=>'text','append' => [$this->Form->button($this->Html->icon("calendar"), ['type' => 'button'])]]);
                            echo "</div>";
                            echo $this->Form->input('grupo_agenda_id', ['type' => 'hidden', 'value' => $grupo_agenda_id])
                            ?>

                        </fieldset>
                        <div class="text-right">
                            <?= $this->Form->button(__('Cancelar'), ['type' => 'button', 'class' => 'btn btn-default', 'onclick' => 'Navegar(\'\',\'back\')', 'listen']) ?>
                            <?= $this->Form->button(__('Salvar'),['type' => 'button', 'class'=>'btn btn-primary', 'onclick' => 'validaPeriodos(\'frm-agenda-periodos\')']) ?>
                        </div>
                        <div class="clearfix"></div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

