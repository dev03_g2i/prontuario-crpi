<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Procedimentos Controller
 *
 * @property \App\Model\Table\ProcedimentosTable $Procedimentos
 */
class ProcedimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Procedimentos->find('all')->contain(['GrupoProcedimentos', 'Users', 'SituacaoCadastros'])
            ->where(['Procedimentos.situacao_id = ' => '1']);
        if(!empty($this->request->query['nome'])){
            $query->andWhere(['Procedimentos.nome LIKE'=>'%'.$this->request->query['nome'].'%']);
        }
        $procedimentos = $this->paginate($query);

        $this->set(compact('procedimentos'));
        $this->set('_serialize', ['procedimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Procedimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $procedimento = $this->Procedimentos->get($id, [
            'contain' => ['GrupoProcedimentos', 'Users', 'SituacaoCadastros', 'AtendimentoProcedimentos', 'PrecoProcedimentos']
        ]);

        $this->set('procedimento', $procedimento);
        $this->set('_serialize', ['procedimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $procedimento = $this->Procedimentos->newEntity();
        if ($this->request->is('post')) {
            $procedimento = $this->Procedimentos->patchEntity($procedimento, $this->request->data);
            if ($this->Procedimentos->save($procedimento)) {
                $this->Flash->success(__('O procedimento foi salvo com sucesso.'));
            } else {
                $this->Flash->error(__('O procedimento não foi salvo. Por favor, tente novamente.'));
            }
            return $this->redirect(['action' => 'index']);
        }
        $grupoProcedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200]);
        $users = $this->Procedimentos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Procedimentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('procedimento', 'grupoProcedimentos', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['procedimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Procedimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $procedimento = $this->Procedimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $procedimento = $this->Procedimentos->patchEntity($procedimento, $this->request->data);
            if ($this->Procedimentos->save($procedimento)) {
                $this->Flash->success(__('O procedimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O procedimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $grupoProcedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200]);
        $users = $this->Procedimentos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Procedimentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('procedimento', 'grupoProcedimentos', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['procedimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Procedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $procedimento = $this->Procedimentos->get($id);
                $procedimento->situacao_id = 2;
        if ($this->Procedimentos->save($procedimento)) {
            $this->Flash->success(__('O procedimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O procedimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function fill(){
        $this->viewBuilder()->layout('ajax');
        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) ||$this->request->data['page']<1) ? 1 : $this->request->data['page'] ;

        if(!isset($termo))
            $termo ='';
        if(!isset($size) || $size < 1)
            $size = 10;

        $Procedimentos= $this->Procedimentos;

        $query = $Procedimentos->find('all')
            ->leftJoin('preco_procedimentos', 'preco_procedimentos.procedimento_id = Procedimentos.id')
            ->distinct()
            ->where(['Procedimentos.situacao_id =' => 1])
            ->andWhere(['UPPER(Procedimentos.nome) LIKE' => '%' . strtoupper($termo) . '%'])
            ->orWhere(['preco_procedimentos.codigo LIKE' => '%'.$termo.'%']);

        /* Verficia se tem preço cadastrado para este convenio
           verifica_preco = convenio_id
        */
        if(!empty($this->request->getData('verifica_preco'))){
            $query->andWhere(['preco_procedimentos.convenio_id' => $this->request->getData('verifica_preco'), 'preco_procedimentos.situacao_id' => 1]);
        }

        $cont = $query->count();

        $ret["total"] = $cont;
        $ret["dados"] = array();
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach($query as $d){
            $ret["dados"][] = array('id' => $d->id, 'text' => !empty($d->anotacao_folha) ? $d->anotacao_folha.' - '.$d->nome : $d->nome);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function isrestauracao($id = null){
        if(!empty($id)){
            $procedimento = $this->Procedimentos->get($id);
            $this->response->type('json');
            $json = json_encode(['res'=>$procedimento->restauracao]);
            $this->response->body($json);
        }
        $this->autoRender=false;
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Gasto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $procedimento = $this->Procedimentos->get($this->request->data['id']);
            $res = ['nome'=>$procedimento->nome,'id'=>$procedimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
