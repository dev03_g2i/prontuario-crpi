<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    public $helpers = [
        'Html' => [
            'className' => 'Bootstrap.BootstrapHtml'
        ],
        'Form' => [
            'className' => 'Bootstrap.BootstrapForm'
        ],
        'Paginator' => [
            'className' => 'Bootstrap.BootstrapPaginator'
        ],
        'Modal' => [
            'className' => 'Bootstrap.BootstrapModal'
        ]
    ];
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        ini_set('memory_limit', '512M');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Paginator');
        $this->loadComponent('Permissoes');
        $this->loadComponent('Auth',[
            'authorize'=> 'Controller',//added this line
            'authenticate'=>[
                'Form'=>[
                    'fields'=>[
                        'username'=>'login',
                        'password'=>'password'
                    ]
                ]
            ],
            'loginAction'=>[
                'controller'=>'Users',
                'action'    =>'login'
            ],
            'unauthorizedRedirect' => $this->referer()

        ]);

        $this->Auth->allow(['display']);
        $this->Auth->config('authError', "Para ter acesso a esta página faça login!.");

        $user_logado = $this->Auth->user();
        if(!empty($user_logado)){
            $this->loadModel('Users');
            $user_image = $this->Users->find('all')->where(['Users.id' => $user_logado['id']])->toArray();
            $this->set(compact('user_logado', 'user_image'));
        }
    }
    public function isAuthorized($user) {
        if(!empty($user)) {
            $this->Auth->config('authError', "Você não tem acesso a esta página!.");
            $res = ['res'=>'erro','msg'=>'Você não tem acesso a esta página!'];

            $this->loadModel('GrupoControladores');
            $grupo_controlador = $this->GrupoControladores->find('all')
                ->contain(['Controladores','GrupoControladorAcoes'=>['Acoes','conditions'=>['GrupoControladorAcoes.acesso'=>0]]])
                ->where(['GrupoControladores.grupo_id'=>$user['grupo_id']])
                ->andWhere(['Controladores.controlador'=>$this->request->controller]);
            $acoes = [];
            if(!empty($grupo_controlador)){
                foreach ($grupo_controlador as $g){
                    if(!empty($g->grupo_controlador_acoes)){
                        foreach ($g->grupo_controlador_acoes as $grupo_acoes) {
                            if (!empty($grupo_acoes->aco)){
                                if(substr_count($grupo_acoes->aco->acao, ',')){
                                    $acts = explode(',',$grupo_acoes->aco->acao);
                                    foreach ($acts as $act) {
                                        if($act==$this->request->action){
                                            if(!empty($this->request->data['check_auth'])){
                                                header('Content-type: application/json');
                                                echo json_encode($res);
                                                exit;
                                            }else {
                                                return false;
                                            }
                                        }
                                        $acoes[] = $act;
                                    }
                                }else{
                                    if($grupo_acoes->aco->acao==$this->request->action){
                                        if(!empty($this->request->data['check_auth'])){
                                            header('Content-type: application/json');
                                            echo json_encode($res);
                                            exit;
                                        }else {
                                            return false;
                                        }
                                    }
                                    $acoes[] = $grupo_acoes->aco->acao;
                                }
                            }
                        }
                    }
                }
            }else{
                return true;
            }

            $this->set(compact('grupo_controlador','acoes'));
            $this->set('_serialize', ['grupo_controlador']);
        }
        return true;
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        $acoes = [];
        if(!empty($this->Auth->user('grupo_id'))) {
            $permissoes = $this->Permissoes;
            $permissoes->iniciar($this->Auth->user('grupo_id'));
            $acoes = $permissoes->getActions();
        }

        $this->set(compact('acoes'));

        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        )
        {
            $this->set('_serialize', true);
        }

        if(empty($this->viewBuilder()->layout())) {
            $this->viewBuilder()->layout('ispinia2');
        }

        if(!empty($this->request->query['ajax'])){
            $this->viewBuilder()->layout('ajax');
        }
        $this->set('site_path',Router::fullBaseUrl());

        $this->loadModel('Configuracoes');
        $configurar = $this->Configuracoes->find('all')->first();
        $this->set('configurar',$configurar);
    }
}
