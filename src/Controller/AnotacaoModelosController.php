<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AnotacaoModelos Controller
 *
 * @property \App\Model\Table\AnotacaoModelosTable $AnotacaoModelos
 */
class AnotacaoModelosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
        ];
        $anotacaoModelos = $this->paginate($this->AnotacaoModelos);


        $this->set(compact('anotacaoModelos'));
        $this->set('_serialize', ['anotacaoModelos']);

    }

    /**
     * View method
     *
     * @param string|null $id Anotacao Modelo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $anotacaoModelo = $this->AnotacaoModelos->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('anotacaoModelo', $anotacaoModelo);
        $this->set('_serialize', ['anotacaoModelo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $anotacaoModelo = $this->AnotacaoModelos->newEntity();
        if ($this->request->is('post')) {
            $anotacaoModelo = $this->AnotacaoModelos->patchEntity($anotacaoModelo, $this->request->data);
            if ($this->AnotacaoModelos->save($anotacaoModelo)) {
                $this->Flash->success(__('O anotacao modelo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O anotacao modelo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('anotacaoModelo'));
        $this->set('_serialize', ['anotacaoModelo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Anotacao Modelo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $anotacaoModelo = $this->AnotacaoModelos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anotacaoModelo = $this->AnotacaoModelos->patchEntity($anotacaoModelo, $this->request->data);
            if ($this->AnotacaoModelos->save($anotacaoModelo)) {
                $this->Flash->success(__('O anotacao modelo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O anotacao modelo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('anotacaoModelo'));
        $this->set('_serialize', ['anotacaoModelo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Anotacao Modelo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $anotacaoModelo = $this->AnotacaoModelos->get($id);
                $anotacaoModelo->situacao_id = 2;
        if ($this->AnotacaoModelos->save($anotacaoModelo)) {
            $this->Flash->success(__('O anotacao modelo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O anotacao modelo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Anotacao Modelo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AnotacaoModelos->find('all')
        ->where(['AnotacaoModelos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AnotacaoModelos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Anotacao Modelo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $anotacaoModelo = $this->AnotacaoModelos->get($this->request->data['id']);
            $res = ['nome'=>$anotacaoModelo->nome,'id'=>$anotacaoModelo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function getModelo($id){
        $this->autoRender=false;
        $this->response->type('json');

        $modelo = $this->AnotacaoModelos->get($id);

        $json = json_encode($modelo->modelo);
        $this->response->body($json);
    }

    public function loadSelect2()
    {
        $dados = $this->AnotacaoModelos->find('list')->where(['AnotacaoModelos.situacao_id' => 1])->orderAsc('AnotacaoModelos.nome');
        $json = json_encode($dados);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }
}
