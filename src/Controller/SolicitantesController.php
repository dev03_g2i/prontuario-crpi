<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Solicitantes Controller
 *
 * @property \App\Model\Table\SolicitantesTable $Solicitantes
 */
class SolicitantesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users', 'SolicitanteCbo']
        ];
        $solicitantes = $this->paginate($this->Solicitantes);

        $this->set(compact('solicitantes'));
        $this->set('_serialize', ['solicitantes']);

    }

    /**
     * View method
     *
     * @param string|null $id Solicitante id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $solicitante = $this->Solicitantes->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'Atendimentos', 'SolicitanteCbo']
        ]);
        

        $this->set('solicitante', $solicitante);
        $this->set('_serialize', ['solicitante']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $solicitante = $this->Solicitantes->newEntity();
        if ($this->request->is('post')) {
            $solicitante = $this->Solicitantes->patchEntity($solicitante, $this->request->data);
            if ($this->Solicitantes->save($solicitante)) {
                $this->Flash->success(__('O solicitante foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O solicitante não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('solicitante'));
        $this->set('_serialize', ['solicitante']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Solicitante id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $solicitante = $this->Solicitantes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $solicitante = $this->Solicitantes->patchEntity($solicitante, $this->request->data);
            if ($this->Solicitantes->save($solicitante)) {
                $this->Flash->success(__('O solicitante foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O solicitante não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('solicitante'));
        $this->set('_serialize', ['solicitante']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Solicitante id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $solicitante = $this->Solicitantes->get($id);
                $solicitante->situacao_id = 2;
        if ($this->Solicitantes->save($solicitante)) {
            $this->Flash->success(__('O solicitante foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O solicitante não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Solicitante id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {   
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 20;

        $query = $this->Solicitantes->find('all')
        ->where(['Solicitantes.nome LIKE ' =>  $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Solicitantes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Solicitante id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){

        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $solicitante = $this->Solicitantes->get($this->request->data['id']);
            $res = ['nome'=>$solicitante->nome,'id'=>$solicitante->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
