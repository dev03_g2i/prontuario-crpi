<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use DateTime;
class ReportsController extends AppController
{

    public $components = array('Data', 'Json');

    public function create()
    {
        $this->viewBuilder()->layout('report_2016');
    }
    public function report()
    {
        $this->viewBuilder()->layout('report_2016');
        $this->set('report','relatorios/prontuario.mrt');
    }

    public function edit(){
        $this->viewBuilder()->layout('report_2016');
    }


}