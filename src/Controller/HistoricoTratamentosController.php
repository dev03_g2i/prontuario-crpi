<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HistoricoTratamentos Controller
 *
 * @property \App\Model\Table\HistoricoTratamentosTable $HistoricoTratamentos
 */
class HistoricoTratamentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ClienteHistoricos', 'Atendimentos', 'AtendimentoProcedimentos', 'AtendimentoItens', 'FaceItens', 'Situacaos']
        ];
        $historicoTratamentos = $this->paginate($this->HistoricoTratamentos);


        $this->set(compact('historicoTratamentos'));
        $this->set('_serialize', ['historicoTratamentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Historico Tratamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $historicoTratamento = $this->HistoricoTratamentos->get($id, [
            'contain' => ['ClienteHistoricos', 'Atendimentos', 'AtendimentoProcedimentos', 'AtendimentoItens', 'FaceItens', 'Situacaos']
        ]);

        $this->set('historicoTratamento', $historicoTratamento);
        $this->set('_serialize', ['historicoTratamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $historicoTratamento = $this->HistoricoTratamentos->newEntity();
        if ($this->request->is('post')) {
            $historicoTratamento = $this->HistoricoTratamentos->patchEntity($historicoTratamento, $this->request->data);
            if ($this->HistoricoTratamentos->save($historicoTratamento)) {
                $this->Flash->success(__('O historico tratamento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O historico tratamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('historicoTratamento'));
        $this->set('_serialize', ['historicoTratamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Historico Tratamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $historicoTratamento = $this->HistoricoTratamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $historicoTratamento = $this->HistoricoTratamentos->patchEntity($historicoTratamento, $this->request->data);
            if ($this->HistoricoTratamentos->save($historicoTratamento)) {
                $this->Flash->success(__('O historico tratamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O historico tratamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('historicoTratamento'));
        $this->set('_serialize', ['historicoTratamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Historico Tratamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $historicoTratamento = $this->HistoricoTratamentos->get($id);
                $historicoTratamento->situacao_id = 2;
        if ($this->HistoricoTratamentos->save($historicoTratamento)) {
            $this->Flash->success(__('O historico tratamento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O historico tratamento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Historico Tratamento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->HistoricoTratamentos->find('all')
        ->where(['HistoricoTratamentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('HistoricoTratamentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Historico Tratamento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $historicoTratamento = $this->HistoricoTratamentos->get($this->request->data['id']);
            $res = ['nome'=>$historicoTratamento->nome,'id'=>$historicoTratamento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
