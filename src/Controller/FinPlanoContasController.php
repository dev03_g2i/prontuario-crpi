<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinPlanoContas Controller
 *
 * @property \App\Model\Table\FinPlanoContasTable $FinPlanoContas
 */
class FinPlanoContasController extends AppController
{

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Plano de contas');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finPlanoContas = $this->FinPlanoContas->find()->contain(['FinClassificacaoContas'])->order(['FinPlanoContas.nome' => 'ASC']);
        $classificacoes = $this->FinPlanoContas->FinClassificacaoContas->find('list');
        
        $id = !empty($this->request->query(['planocontas'])) ? $this->request->query(['planocontas']) : null;
        $classificacao = !empty($this->request->query(['classificacao'])) ? $this->request->query(['classificacao']) : null;

        if (isset($id)) $finPlanoContas = $finPlanoContas->where(['FinPlanoContas.id' => $id]);

        if (isset($classificacao)) $finPlanoContas = $finPlanoContas->where(['FinPlanoContas.fin_classificacao_conta_id' => $classificacao]);

        $finPlanoContas = $this->paginate($finPlanoContas);

        $this->set(compact('finPlanoContas', 'classificacoes'));
        $this->set('_serialize', ['finPlanoContas']);
    }

    /**
     * View method
     *
     * @param string|null $id Fin Plano Conta id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finPlanoConta = $this->FinPlanoContas->get($id, [
            'contain' => []
        ]);

        $this->set('finPlanoConta', $finPlanoConta);
        $this->set('_serialize', ['finPlanoConta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finPlanoConta = $this->FinPlanoContas->newEntity();
        if ($this->request->is('post')) {
            $newPlanoConta = $this->request->data;
            $newPlanoConta['situacao'] = 1;
            $newPlanoConta['rateio'] = 2;
            $finPlanoConta = $this->FinPlanoContas->patchEntity($finPlanoConta, $newPlanoConta);
            if ($this->FinPlanoContas->save($finPlanoConta)) {
                $this->Flash->success(__('O plano de contas foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O plano de contas não foi salvo. Por favor, tente novamente.'));
            }
        }
        $classificacoes = $this->FinPlanoContas->FinClassificacaoContas->find('list');

        $this->set(compact('finPlanoConta', 'classificacoes'));
        $this->set('_serialize', ['finPlanoConta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Plano Conta id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finPlanoConta = $this->FinPlanoContas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newPlanoConta = $this->request->data;
            $newPlanoConta['situacao'] = 1;
            $newPlanoConta['rateio'] = 2;
            $finPlanoConta = $this->FinPlanoContas->patchEntity($finPlanoConta, $newPlanoConta);
            if ($this->FinPlanoContas->save($finPlanoConta)) {
                $this->Flash->success(__('O plano de contas foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O plano de contas não foi salvo. Por favor, tente novamente.'));
            }
        }

        $classificacoes = $this->FinPlanoContas->FinClassificacaoContas->find('list');
        $this->set(compact('finPlanoConta', 'classificacoes'));
        $this->set('_serialize', ['finPlanoConta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Plano Conta id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finPlanoConta = $this->FinPlanoContas->get($id);
        $finPlanoConta->situacao = 2;
        if ($this->FinPlanoContas->save($finPlanoConta)) {
            $this->Flash->success(__('O plano de contas foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O plano de contas não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Plano Conta id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;
        $query = $this->FinPlanoContas->find('all')
        ->where(['FinPlanoContas.nome LIKE ' => '%' . $termo . '%']);
        $cont = $query->count();
        $query->orderAsc('FinPlanoContas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Plano Conta id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finPlanoConta = $this->FinPlanoContas->get($this->request->data['id']);
            $res = ['nome'=>$finPlanoConta->nome,'id'=>$finPlanoConta->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
