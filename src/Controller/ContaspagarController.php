<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contaspagar Controller
 *
 * @property \App\Model\Table\ContaspagarTable $Contaspagar
 */
class ContaspagarController extends AppController
{

    //Componentes externos para serem usados.
    public $components = ['Date', 'Data'];
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Planocontas', 'Fornecedores', 'Contabilidades', 'ContaspagarPlanejamentos', 'TipoDocumento']
        ];
        $selectData = $this->getDataForSelects();
        $planocontas = $selectData[0];
        $fornecedores = $selectData[1];
        $contabilidades = $selectData[2];

        $inicio = !empty($this->request->query(['inicio'])) ? $this->Date->formatDateForSql($this->request->query(['inicio'])) : null;
        $fim = !empty($this->request->query(['fim'])) ? $this->Date->formatDateForSql($this->request->query(['fim'])) : null;
        $planocontas = !empty($this->request->query(['planoconta'])) ? $this->request->query(['planoconta']) : null;
        $fornecedores = !empty($this->request->query(['fornecedor'])) ? $this->request->query(['fornecedor']) : null;
        $contabilidades = !empty($this->request->query(['contabilidade'])) ? $this->request->query(['contabilidade']) : null;
        $situacoes = !empty($this->request->query(['situacao'])) ? $this->request->query(['situacao']) : null;
        $status_contas = !empty($this->request->query(['status_conta'])) ? $this->request->query(['status_conta']) : null;

        $contaspagar = $this->Contaspagar->find('all')
        ->contain(['Planocontas', 'Fornecedores', 'Contabilidades', 'ContaspagarPlanejamentos', 'TipoDocumento']);

        if (isset($inicio) && isset($fim)) {
            $contaspagar = $contaspagar->andWhere(['Contaspagar.data >=' => $inicio, 'Contaspagar.data <=' => $fim]);
        } else if (isset($inicio)) {
            $contaspagar = $contaspagar->andWhere(['Contaspagar.data >=' => $inicio]);
        } else if (isset($fim)) {
            $contaspagar = $contaspagar->andWhere(['Contaspagar.data <=' => $fim]);
        }

        if (isset($planocontas)) {
            $string = '';
            $last_key = key(array_slice($planocontas, -1, 1, TRUE));
            foreach($planocontas as $key => $planoconta)
            {
                if ($key === 0){
                    $string = '('.$planoconta;
                } else {
                    $string = $string.$planoconta;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }

            $contaspagar = $contaspagar->andWhere('(Contaspagar.planoconta_id IN '.$string.')');
        }

        if (isset($fornecedores)){
            $string = '';
            $last_key = key(array_slice($fornecedores, -1, 1, TRUE));
            foreach($fornecedores as $key => $fornecedor)
            {
                if ($key === 0){
                    $string = '('.$fornecedor;
                } else {
                    $string = $string.$fornecedor;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.fornecedor_id IN '.$string.')');
        }

        if (isset($contabilidades)){
            $string = '';
            $last_key = key(array_slice($contabilidades, -1, 1, TRUE));
            foreach($contabilidades as $key => $contabilidade)
            {
                if ($key === 0){
                    $string = '('.$contabilidade;
                } else {
                    $string = $string.$contabilidade;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.contabilidade_id IN '.$string.')');
        }

        if (isset($situacoes)){
            $string = '';
            foreach($situacoes as $key => $situacao)
            {   
                if ($situacao === '2')
                    $situacao = 'NULL';
                else
                    $situacao = 'NOT NULL and Contaspagar.data_pagamento != "0000-00-00"';
                
                if ($key == 0)
                    $string = $string.'Contaspagar.data_pagamento IS '.$situacao;
                else
                    $string = $string.' OR Contaspagar.data_pagamento IS '.$situacao;
            }
            $contaspagar = $contaspagar->andWhere('('.$string.')');
        }

        if (isset($status_conta)){
            $string = '';
            $last_key = key(array_slice($status_contas, -1, 1, TRUE));
            foreach($status_contas as $key => $status)
            {
                if ($key === 0){
                    $string = '('.$status;
                } else {
                    $string = $string.$status;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.situacao IN '.$string.')');
        }

        $contaspagar = $this->paginate($contaspagar);

        $this->set(compact('contaspagar', 'planocontas', 'fornecedores', 'contabilidades'));
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * View method
     *
     * @param string|null $id Contaspagar id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contaspagar = $this->Contaspagar->get($id, [
            'contain' => ['Planocontas', 'Fornecedores', 'Contabilidades', 'ContaspagarPlanejamentos', 'TipoDocumento', 'TipoPagamento', 'AbatimentosContaspagar', 'Movimentos']
        ]);

        $this->set('contaspagar', $contaspagar);
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contaspagar = $this->Contaspagar->newEntity();
        if ($this->request->is('post')) {
            
            $newConta = $this->request->getData();
            
            $contaspagar = $this->Contaspagar->patchEntity($contaspagar, $newConta);

            if ($this->Contaspagar->save($contaspagar)) {
                $this->Flash->success(__('The contaspagar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contaspagar could not be saved. Please, try again.'));
        }
        $planocontas = $this->Contaspagar->Planocontas->find('list', ['limit' => 200]);
        $fornecedores = $this->Contaspagar->Fornecedores->find('list', ['limit' => 200]);
        $contabilidades = $this->Contaspagar->Contabilidades->find('list', ['limit' => 200]);
        $contaspagarPlanejamentos = $this->Contaspagar->ContaspagarPlanejamentos->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contaspagar->TipoDocumento->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contaspagar->TipoPagamento->find('list', ['limit' => 200]);
        $abatimentosContaspagar = $this->Contaspagar->AbatimentosContaspagar->find('list', ['limit' => 200]);
        $this->set(compact('contaspagar', 'planocontas', 'fornecedores', 'contabilidades', 'contaspagarPlanejamentos', 'tipoDocumento', 'tipoPagamento', 'abatimentosContaspagar'));
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contaspagar id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contaspagar = $this->Contaspagar->get($id, [
            'contain' => ['AbatimentosContaspagar']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contaspagar = $this->Contaspagar->patchEntity($contaspagar, $this->request->getData());
            if ($this->Contaspagar->save($contaspagar)) {
                $this->Flash->success(__('The contaspagar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contaspagar could not be saved. Please, try again.'));
        }
        $planocontas = $this->Contaspagar->Planocontas->find('list', ['limit' => 200]);
        $fornecedores = $this->Contaspagar->Fornecedores->find('list', ['limit' => 200]);
        $contabilidades = $this->Contaspagar->Contabilidades->find('list', ['limit' => 200]);
        $contaspagarPlanejamentos = $this->Contaspagar->ContaspagarPlanejamentos->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contaspagar->TipoDocumento->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contaspagar->TipoPagamento->find('list', ['limit' => 200]);
        $abatimentosContaspagar = $this->Contaspagar->AbatimentosContaspagar->find('list', ['limit' => 200]);
        $this->set(compact('contaspagar', 'planocontas', 'fornecedores', 'contabilidades', 'contaspagarPlanejamentos', 'tipoDocumento', 'tipoPagamento', 'abatimentosContaspagar'));
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contaspagar id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contaspagar = $this->Contaspagar->get($id);
        if ($this->Contaspagar->delete($contaspagar)) {
            $this->Flash->success(__('The contaspagar has been deleted.'));
        } else {
            $this->Flash->error(__('The contaspagar could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /*Rotas para o jquery*/

    //Rota para buscar pelas opções
    public function searchForOptions()
    {   
        $inicio = !empty($this->request->query(['inicio'])) ? $this->Date->formatDateForSql($this->request->query(['inicio'])) : null;
        $fim = !empty($this->request->query(['fim'])) ? $this->Date->formatDateForSql($this->request->query(['fim'])) : null;
        $planocontas = !empty($this->request->query(['planoconta'])) ? $this->request->query(['planoconta']) : null;
        $fornecedores = !empty($this->request->query(['fornecedor'])) ? $this->request->query(['fornecedor']) : null;
        $contabilidades = !empty($this->request->query(['contabilidade'])) ? $this->request->query(['contabilidade']) : null;
        $situacoes = !empty($this->request->query(['situacao'])) ? $this->request->query(['situacao']) : null;
        $status_contas = !empty($this->request->query(['status_conta'])) ? $this->request->query(['status_conta']) : null;

        $contaspagar = $this->Contaspagar->find('all')
        ->contain(['Planocontas', 'Fornecedores', 'Contabilidades', 'ContaspagarPlanejamentos', 'TipoDocumento']);

        if (isset($inicio) && isset($fim)) {
            $contaspagar = $contaspagar->andWhere(['Contaspagar.data >=' => $inicio, 'Contaspagar.data <=' => $fim]);
        } else if (isset($inicio)) {
            $contaspagar = $contaspagar->andWhere(['Contaspagar.data >=' => $inicio]);
        } else if (isset($fim)) {
            $contaspagar = $contaspagar->andWhere(['Contaspagar.data <=' => $fim]);
        }

        if (isset($planocontas)) {
            $string = '';
            $last_key = key(array_slice($planocontas, -1, 1, TRUE));
            foreach($planocontas as $key => $planoconta)
            {
                if ($key === 0){
                    $string = '('.$planoconta;
                } else {
                    $string = $string.$planoconta;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.planoconta_id IN '.$string.')');
        }

        if (isset($fornecedores)){
            $string = '';
            $last_key = key(array_slice($fornecedores, -1, 1, TRUE));
            foreach($fornecedores as $key => $fornecedor)
            {
                if ($key === 0){
                    $string = '('.$fornecedor;
                } else {
                    $string = $string.$fornecedor;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.fornecedor_id IN '.$string.')');
        }

        if (isset($contabilidades)){
            $string = '';
            $last_key = key(array_slice($contabilidades, -1, 1, TRUE));
            foreach($contabilidades as $key => $contabilidade)
            {
                if ($key === 0){
                    $string = '('.$contabilidade;
                } else {
                    $string = $string.$contabilidade;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.contabilidade_id IN '.$string.')');
        }

        if (isset($situacoes)){
            $string = '';
            foreach($situacoes as $key => $situacao)
            {   
                if ($situacao === 1)
                    $situacao = 'NULL';
                else
                    $situacao = 'NOT NULL';
                
                if ($key == 0)
                    $string = $string.'Contaspagar.data IS '.$situacao;
                else
                    $string = $string.' OR Contaspagar.data IS '.$situacao;
            }
            $contaspagar = $contaspagar->andWhere('('.$string.')');
        }

        if (isset($status_conta)){
            $string = '';
            $last_key = key(array_slice($status_contas, -1, 1, TRUE));
            foreach($status_contas as $key => $status)
            {
                if ($key === 0){
                    $string = '('.$status;
                } else {
                    $string = $string.$status;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $contaspagar = $contaspagar->andWhere('(Contaspagar.situacao IN '.$string.')');
        }

        $contaspagar = $this->paginate($contaspagar);
        $this->set(compact('contaspagar'));
        $this->set('_serialize', ['contaspagar']);
        $this->render('/index');
    }

    //Função para pegar os credores
    public function getCreditors()
    {
        $credores =  $this->Contaspagar->Fornecedores->find('list', ['limit' => 200])->toArray();
        $this->set(compact('credores'));
        $this->set('_serialize', ['credores']);
    }

    
    /*Funções privadas dessa controller*/

    //Função para pegar os dados dos selects
    private function getDataForSelects()
    {
        $planocontas = $this->Contaspagar->Planocontas->find('list', ['limit' => 200])->toArray();
        $fornecedores = $this->Contaspagar->Fornecedores->find('list', ['limit' => 200])->toArray();
        $contabilidades = $this->Contaspagar->Contabilidades->find('list', ['limit' => 200])->toArray();

        return [$planocontas, $fornecedores, $contabilidades];
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Contaspagar id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Contaspagar->find('all')
        ->where(['Contaspagar.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Contaspagar.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Contaspagar id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $contaspagar = $this->Contaspagar->get($this->request->data['id']);
            $res = ['nome'=>$contaspagar->nome,'id'=>$contaspagar->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
