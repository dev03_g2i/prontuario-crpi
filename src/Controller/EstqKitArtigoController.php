<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EstqKitArtigo Controller
 *
 * @property \App\Model\Table\EstqKitArtigoTable $EstqKitArtigo
 */
class EstqKitArtigoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->EstqKitArtigo->find('all')
                ->contain(['EstqKit', 'EstqArtigos', 'Users', 'SituacaoCadastros'])
                ->where(['EstqKitArtigo.situacao_id' => 1]);

        $fatura_kit_id = null;
        if(!empty($this->request->query('fatura_kit_id'))){
            $fatura_kit_id = $this->request->query('fatura_kit_id');
            $query->andWhere(['EstqKitArtigo.fatura_kit_id' => $fatura_kit_id]);
        }

        $estqKitArtigo = $this->paginate($query);

        $this->set(compact('estqKitArtigo', 'fatura_kit_id'));
        $this->set('_serialize', ['estqKitArtigo']);

    }

    /**
     * View method
     *
     * @param string|null $id Estq Kit Artigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estqKitArtigo = $this->EstqKitArtigo->get($id, [
            'contain' => ['EstqKit', 'EstqArtigos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('estqKitArtigo', $estqKitArtigo);
        $this->set('_serialize', ['estqKitArtigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fatura_kit_id = null;
        if(!empty($this->request->query('fatura_kit_id'))){
            $fatura_kit_id = $this->request->query('fatura_kit_id');
        }

        $estqKitartigo = $this->EstqKitArtigo->newEntity();
        if ($this->request->is('post')) {
            $estqKitartigo = $this->EstqKitArtigo->patchEntity($estqKitartigo, $this->request->data);
            if ($this->EstqKitArtigo->save($estqKitartigo)) {
                $this->Flash->success(__('O kitartigo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O kitartigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('estqKitartigo', 'fatura_kit_id'));
        $this->set('_serialize', ['faturaKitartigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estq Kit Artigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $estqKitArtigo = $this->EstqKitArtigo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estqKitArtigo = $this->EstqKitArtigo->patchEntity($estqKitArtigo, $this->request->data);
            if ($this->EstqKitArtigo->save($estqKitArtigo)) {
                $this->Flash->success(__('O estq kit artigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq kit artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('estqKitArtigo'));
        $this->set('_serialize', ['estqKitArtigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estq Kit Artigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $estqKitArtigo = $this->EstqKitArtigo->get($id);
                $estqKitArtigo->situacao_id = 2;
        if ($this->EstqKitArtigo->save($estqKitArtigo)) {
            $this->Flash->success(__('O estq kit artigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O estq kit artigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Estq Kit Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->EstqKitArtigo->find('all')
        ->where(['EstqKitArtigo.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('EstqKitArtigo.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Estq Kit Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $estqKitArtigo = $this->EstqKitArtigo->get($this->request->data['id']);
            $res = ['nome'=>$estqKitArtigo->nome,'id'=>$estqKitArtigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
