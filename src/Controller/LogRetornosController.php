<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LogRetornos Controller
 *
 * @property \App\Model\Table\LogRetornosTable $LogRetornos
 */
class LogRetornosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->LogRetornos->find('all')
                 ->contain(['Retornos', 'Clientes', 'GrupoRetornos', 'SituacaoRetornos', 'Users', 'SituacaoCadastros'])
                 ->where(['LogRetornos.situacao_id = ' => '1']);

        if(!empty($this->request->query['retorno'])){
            $query->andWhere(['LogRetornos.retorno_id'=>$this->request->query['retorno']]);
            $query->orderDesc('LogRetornos.created');
        }



        $logRetornos = $this->paginate($query);

        $this->set(compact('logRetornos'));
        $this->set('_serialize', ['logRetornos']);
    }

    /**
     * View method
     *
     * @param string|null $id Log Retorno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $logRetorno = $this->LogRetornos->get($id, [
            'contain' => ['Retornos', 'Clientes', 'GrupoRetornos', 'SituacaoRetornos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('logRetorno', $logRetorno);
        $this->set('_serialize', ['logRetorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $logRetorno = $this->LogRetornos->newEntity();
        if ($this->request->is('post')) {
            $logRetorno = $this->LogRetornos->patchEntity($logRetorno, $this->request->data);
            if ($this->LogRetornos->save($logRetorno)) {
                $this->Flash->success(__('O log retorno foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O log retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $retornos = $this->LogRetornos->Retornos->find('list', ['limit' => 200]);
        $clientes = $this->LogRetornos->Clientes->find('list', ['limit' => 200]);
        $grupoRetornos = $this->LogRetornos->GrupoRetornos->find('list', ['limit' => 200]);
        $situacaoRetornos = $this->LogRetornos->SituacaoRetornos->find('list', ['limit' => 200]);
        $users = $this->LogRetornos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->LogRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('logRetorno', 'retornos', 'clientes', 'grupoRetornos', 'situacaoRetornos', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['logRetorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Log Retorno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $logRetorno = $this->LogRetornos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $logRetorno = $this->LogRetornos->patchEntity($logRetorno, $this->request->data);
            if ($this->LogRetornos->save($logRetorno)) {
                $this->Flash->success(__('O log retorno foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O log retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $retornos = $this->LogRetornos->Retornos->find('list', ['limit' => 200]);
        $clientes = $this->LogRetornos->Clientes->find('list', ['limit' => 200]);
        $grupoRetornos = $this->LogRetornos->GrupoRetornos->find('list', ['limit' => 200]);
        $situacaoRetornos = $this->LogRetornos->SituacaoRetornos->find('list', ['limit' => 200]);
        $users = $this->LogRetornos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->LogRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('logRetorno', 'retornos', 'clientes', 'grupoRetornos', 'situacaoRetornos', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['logRetorno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Log Retorno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $logRetorno = $this->LogRetornos->get($id);
                $logRetorno->situacao_id = 2;
        if ($this->LogRetornos->save($logRetorno)) {
            $this->Flash->success(__('O log retorno foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O log retorno não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
