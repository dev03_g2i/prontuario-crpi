<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoAnexos Controller
 *
 * @property \App\Model\Table\TipoAnexosTable $TipoAnexos
 */
class TipoAnexosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
                        ,'conditions' => ['TipoAnexos.situacao_id = ' => '1']
                    ];
        $tipoAnexos = $this->paginate($this->TipoAnexos);

        $this->set(compact('tipoAnexos'));
        $this->set('_serialize', ['tipoAnexos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Anexo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoAnexo = $this->TipoAnexos->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('tipoAnexo', $tipoAnexo);
        $this->set('_serialize', ['tipoAnexo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoAnexo = $this->TipoAnexos->newEntity();
        if ($this->request->is('post')) {
            $tipoAnexo = $this->TipoAnexos->patchEntity($tipoAnexo, $this->request->data);
            if ($this->TipoAnexos->save($tipoAnexo)) {
                $this->Flash->success(__('O tipo anexo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo anexo não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoAnexos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoAnexos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoAnexo', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoAnexo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Anexo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoAnexo = $this->TipoAnexos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoAnexo = $this->TipoAnexos->patchEntity($tipoAnexo, $this->request->data);
            if ($this->TipoAnexos->save($tipoAnexo)) {
                $this->Flash->success(__('O tipo anexo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo anexo não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoAnexos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoAnexos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoAnexo', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoAnexo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Anexo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoAnexo = $this->TipoAnexos->get($id);
                $tipoAnexo->situacao_id = 2;

        if ($this->TipoAnexos->save($tipoAnexo)) {
            $this->Flash->success(__('O tipo anexo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo anexo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
