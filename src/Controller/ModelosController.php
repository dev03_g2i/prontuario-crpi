<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Modelos Controller
 *
 * @property \App\Model\Table\ModelosTable $Modelos
 */
class ModelosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Modelos->find('all')
            ->contain(['TipoDocumentos', 'SituacaoCadastros', 'Users'])
            ->where(['Modelos.situacao_id = ' => '1']);

        $tipo_id = null;
        if(!empty($this->request->query['tipo_documento'])){
            $this->loadModel('TipoDocumentos');
                $tipo_id = $this->request->query['tipo_documento'];
                $query->andWhere(['tipo_id'=>$this->request->query['tipo_documento']]);
        }

        $modelos = $this->paginate($query);

        $this->set(compact('modelos','tipo_id'));
        $this->set('_serialize', ['modelos']);
    }

    /**
     * View method
     *
     * @param string|null $id Modelo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $modelo = $this->Modelos->get($id, [
            'contain' => ['TipoDocumentos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('modelo', $modelo);
        $this->set('_serialize', ['modelo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($tipo_id=null)
    {
        $modelo = $this->Modelos->newEntity();
        if ($this->request->is('post')) {
            $modelo = $this->Modelos->patchEntity($modelo, $this->request->data);
            if ($this->Modelos->save($modelo)) {
                if(!empty($tipo_id)){
                    echo $tipo_id;
                    $this->autoRender=false;
                }else {
                    $this->Flash->success(__('O modelo foi salvo com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                if(!empty($tipo_id)){
                    echo 'erro';
                    $this->autoRender=false;
                }else {
                    $this->Flash->error(__('O modelo não foi salvo. Por favor, tente novamente.'));
                }
            }
        }
        $this->loadModel('MedicoResponsaveis');
        $medico_responsaveis = $this->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'])->orderAsc('MedicoResponsaveis.nome');
        $tipoDocumentos = $this->Modelos->TipoDocumentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Modelos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Modelos->Users->find('list', ['limit' => 200]);
        $this->set(compact('modelo', 'tipoDocumentos', 'situacaoCadastros', 'users','tipo_id', 'medico_responsaveis'));
        $this->set('_serialize', ['modelo']);
    }

    public function nadd($tipo_id=null)
    {
        $modelo = $this->Modelos->newEntity();
        if ($this->request->is('post')) {
            $modelo = $this->Modelos->patchEntity($modelo, $this->request->data);
            if ($this->Modelos->save($modelo)) {
                if(!empty($tipo_id)){
                    $this->response->type('json');
                    $json = json_encode(array('id'=>$modelo->id,'nome'=>$modelo->nome));
                    $this->response->body($json);
                    $this->autoRender=false;
                }else {
                    $this->Flash->success(__('O modelo foi salvo com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                }
            } else {
                if(!empty($tipo_id)){
                    echo 'erro';
                    $this->autoRender=false;
                }else {
                    $this->Flash->error(__('O modelo não foi salvo. Por favor, tente novamente.'));
                }
            }
        }
        $this->loadModel('MedicoResponsaveis');
        $medico_responsaveis = $this->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'])->orderAsc('MedicoResponsaveis.nome');
        $tipoDocumentos = $this->Modelos->TipoDocumentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Modelos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Modelos->Users->find('list', ['limit' => 200]);
        $this->set(compact('modelo', 'tipoDocumentos', 'situacaoCadastros', 'users','tipo_id', 'medico_responsaveis'));
        $this->set('_serialize', ['modelo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Modelo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $modelo = $this->Modelos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modelo = $this->Modelos->patchEntity($modelo, $this->request->data);
            if ($this->Modelos->save($modelo)) {
                $this->Flash->success(__('O modelo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O modelo não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('MedicoResponsaveis');
        $medico_responsaveis = $this->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'])->orderAsc('MedicoResponsaveis.nome');
        $tipoDocumentos = $this->Modelos->TipoDocumentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Modelos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Modelos->Users->find('list', ['limit' => 200]);
        $this->set(compact('modelo', 'tipoDocumentos', 'situacaoCadastros', 'users', 'medico_responsaveis'));
        $this->set('_serialize', ['modelo']);
    }

    public function fill($tipo = null){
        $this->autoRender=false;
        $codigo_medico = $this->Auth->user('codigo_medico');

        if(!empty($tipo)) {
            $modelos = $this->Modelos->find('all')
                ->where(['Modelos.situacao_id' => 1])
                ->andWhere(['Modelos.tipo_id' => $tipo])
                ->orderAsc('Modelos.nome');

            /*
              Se o usuário logado estiver um codigo do médico e este código estiver vinculado ao(s) modelo(s),
              traz os modelos deste médico e os publicos = null.
              proprietario = codigo_medico
           */
            if(!empty($codigo_medico)){
                $modelos->andWhere([
                    'or' => [
                        'Modelos.proprietario' => $codigo_medico,
                        'Modelos.proprietario IS' => null]
                ]);
            }

            $this->response->type('json');
            $json = json_encode($modelos);
            $this->response->body($json);
        }
    }

    public function get($id = null){
        $this->autoRender=false;
        if(!empty($id)) {
            $modelos = $this->Modelos->get($id);
            $this->response->type('json');
            $json = json_encode(['modelo'=>$modelos->modelo]);
            $this->response->body($json);
        }
    }
    /**
     * Delete method
     *
     * @param string|null $id Modelo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $modelo = $this->Modelos->get($id);
                $modelo->situacao_id = 2;
        if ($this->Modelos->save($modelo)) {
            $this->Flash->success(__('O modelo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O modelo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
