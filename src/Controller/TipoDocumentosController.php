<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoDocumentos Controller
 *
 * @property \App\Model\Table\TipoDocumentosTable $TipoDocumentos
 */
class TipoDocumentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['TipoDocumentos.situacao_id = ' => '1']
                    ];
        $tipoDocumentos = $this->paginate($this->TipoDocumentos);

        $this->set(compact('tipoDocumentos'));
        $this->set('_serialize', ['tipoDocumentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoDocumento = $this->TipoDocumentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('tipoDocumento', $tipoDocumento);
        $this->set('_serialize', ['tipoDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoDocumento = $this->TipoDocumentos->newEntity();
        if ($this->request->is('post')) {
            $tipoDocumento = $this->TipoDocumentos->patchEntity($tipoDocumento, $this->request->data);
            if ($this->TipoDocumentos->save($tipoDocumento)) {
                $this->Flash->success(__('O tipo documento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo documento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->TipoDocumentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->TipoDocumentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('tipoDocumento', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['tipoDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoDocumento = $this->TipoDocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoDocumento = $this->TipoDocumentos->patchEntity($tipoDocumento, $this->request->data);
            if ($this->TipoDocumentos->save($tipoDocumento)) {
                $this->Flash->success(__('O tipo documento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo documento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->TipoDocumentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->TipoDocumentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('tipoDocumento', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['tipoDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $tipoDocumento = $this->TipoDocumentos->get($id);
                $tipoDocumento->situacao_id = 2;
        if ($this->TipoDocumentos->save($tipoDocumento)) {
            $this->Flash->success(__('O tipo documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo documento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
