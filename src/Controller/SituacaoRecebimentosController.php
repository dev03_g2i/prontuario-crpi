<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoRecebimentos Controller
 *
 * @property \App\Model\Table\SituacaoRecebimentosTable $SituacaoRecebimentos
 */
class SituacaoRecebimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $situacaoRecebimentos = $this->paginate($this->SituacaoRecebimentos);


        $this->set(compact('situacaoRecebimentos'));
        $this->set('_serialize', ['situacaoRecebimentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Situacao Recebimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoRecebimento = $this->SituacaoRecebimentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'AtendimentoProcedimentos']
        ]);

        $this->set('situacaoRecebimento', $situacaoRecebimento);
        $this->set('_serialize', ['situacaoRecebimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoRecebimento = $this->SituacaoRecebimentos->newEntity();
        if ($this->request->is('post')) {
            $situacaoRecebimento = $this->SituacaoRecebimentos->patchEntity($situacaoRecebimento, $this->request->data);
            if ($this->SituacaoRecebimentos->save($situacaoRecebimento)) {
                $this->Flash->success(__('O situacao recebimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao recebimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoRecebimento'));
        $this->set('_serialize', ['situacaoRecebimento']);
    }

    public function situacao($id)
    {
        $this->loadModel('AtendimentoProcedimentos');
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($id);
        if($this->request->is('post')){
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->patchEntity($atendimentoProcedimento, $this->request->data);
            $atendimentoProcedimento->dt_recebimento = (!empty($this->request->data('dt_recebimento'))? $this->request->data('dt_recebimento') : null);
            if($this->AtendimentoProcedimentos->save($atendimentoProcedimento)){
                $this->Flash->success(__('A situacao foi alterada com sucesso!'));
                return $this->redirect(['controller' => 'Faturamentos', 'action' => 'rec-listar']);
            }
        }

        $situacoes = $this->SituacaoRecebimentos->find('list', ['limit' => 200])->where(['SituacaoRecebimentos.situacao_id' => 1])->orderAsc('SituacaoRecebimentos.nome');
        $this->set(compact(['situacoes', 'atendimentoProcedimento']));
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Recebimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoRecebimento = $this->SituacaoRecebimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoRecebimento = $this->SituacaoRecebimentos->patchEntity($situacaoRecebimento, $this->request->data);
            if ($this->SituacaoRecebimentos->save($situacaoRecebimento)) {
                $this->Flash->success(__('O situacao recebimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao recebimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoRecebimento'));
        $this->set('_serialize', ['situacaoRecebimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Recebimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoRecebimento = $this->SituacaoRecebimentos->get($id);
                $situacaoRecebimento->situacao_id = 2;
        if ($this->SituacaoRecebimentos->save($situacaoRecebimento)) {
            $this->Flash->success(__('O situacao recebimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao recebimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Situacao Recebimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->SituacaoRecebimentos->find('all')
        ->where(['SituacaoRecebimentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('SituacaoRecebimentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Situacao Recebimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $situacaoRecebimento = $this->SituacaoRecebimentos->get($this->request->data['id']);
            $res = ['nome'=>$situacaoRecebimento->nome,'id'=>$situacaoRecebimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
