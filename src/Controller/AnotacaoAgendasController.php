<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AnotacaoAgendas Controller
 *
 * @property \App\Model\Table\AnotacaoAgendasTable $AnotacaoAgendas
 */
class AnotacaoAgendasController extends AppController
{
    public $components = array('Data');
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->AnotacaoAgendas->find('all')
            ->contain(['GrupoAgendas', 'SituacaoCadastros', 'UserAlt', 'UserReg'])
            ->where(['AnotacaoAgendas.situacao_id' => 1]);

        $grupos = null;
        if(!empty($this->request->getQuery('grupo'))){
            $grupos = explode(',', $this->request->getQuery('grupo'));
            $query->andWhere(['AnotacaoAgendas.grupo_id IN' => $grupos]);
        }

        if(!empty($this->request->getQuery('data'))){
            $query->andWhere(['AnotacaoAgendas.data' => $this->Data->DataSQL($this->request->getQuery('data'))]);
        }

        $anotacaoAgendas = $this->paginate($query);

        $this->set(compact('anotacaoAgendas'));
        $this->set('_serialize', ['anotacaoAgendas']);

    }

    /**
     * View method
     *
     * @param string|null $id Anotacao Agenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $anotacaoAgenda = $this->AnotacaoAgendas->get($id, [
            'contain' => ['GrupoAgendas', 'SituacaoCadastros', 'UserAlt', 'UserReg']
        ]);

        $this->set('anotacaoAgenda', $anotacaoAgenda);
        $this->set('_serialize', ['anotacaoAgenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $anotacaoAgenda = $this->AnotacaoAgendas->newEntity();
        if ($this->request->is('post')) {
            $anotacaoAgenda = $this->AnotacaoAgendas->patchEntity($anotacaoAgenda, $this->request->data);
            if ($this->AnotacaoAgendas->save($anotacaoAgenda)) {
                $this->Flash->success(__('O anotacao agenda foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O anotacao agenda não foi salvo. Por favor, tente novamente.'));
            }
        }

        $grupos = $this->AnotacaoAgendas->GrupoAgendas->getList();
        $this->set(compact('anotacaoAgenda', 'grupos'));
        $this->set('_serialize', ['anotacaoAgenda']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Anotacao Agenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $anotacaoAgenda = $this->AnotacaoAgendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $anotacaoAgenda = $this->AnotacaoAgendas->patchEntity($anotacaoAgenda, $this->request->data);
            if ($this->AnotacaoAgendas->save($anotacaoAgenda)) {
                $this->Flash->success(__('O anotacao agenda foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O anotacao agenda não foi salvo. Por favor, tente novamente.'));
            }
        }

        $grupos = $this->AnotacaoAgendas->GrupoAgendas->getList();
        $this->set(compact('anotacaoAgenda', 'grupos'));
        $this->set('_serialize', ['anotacaoAgenda']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Anotacao Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $anotacaoAgenda = $this->AnotacaoAgendas->get($id);
                $anotacaoAgenda->situacao_id = 2;
        if ($this->AnotacaoAgendas->save($anotacaoAgenda)) {
            $this->Flash->success(__('O anotacao agenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O anotacao agenda não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete Modal
     *
     * @return void
     */
    public function deleteModal()
    {
        $anotacaoAgenda = $this->AnotacaoAgendas->get($this->request->getData('id'));
        $anotacaoAgenda->situacao_id = 2;
        if ($this->AnotacaoAgendas->save($anotacaoAgenda)) {
            $json = ['res' => 0];
        } else {
            $json = ['res' => 1];
        }
        $json_data = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Anotacao Agenda id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AnotacaoAgendas->find('all')
        ->where(['AnotacaoAgendas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AnotacaoAgendas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Anotacao Agenda id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $anotacaoAgenda = $this->AnotacaoAgendas->get($this->request->data['id']);
            $res = ['nome'=>$anotacaoAgenda->nome,'id'=>$anotacaoAgenda->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
