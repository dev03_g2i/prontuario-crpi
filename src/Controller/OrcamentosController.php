<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;


/**
 * Atendimentos Controller
 *
 * @property \App\Model\Table\AtendimentosTable $Atendimentos
 */
class OrcamentosController extends AppController
{

    public $components = array('Data');

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('ContasReceber');
        $contas = $this->ContasReceber->find('all')->limit(1);

        $this->loadModel('Atendimentos');
        $this->loadModel('Convenios');
        $this->loadModel('TipoAtendimentos');
        $query = $this->Atendimentos->find('all')
            ->contain(['Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos', 'SituacaoCadastros', 'Users','AtendimentoProcedimentos' => ['AtendimentoItens']])
            ->where(['Atendimentos.situacao_id ' => '1','Atendimentos.tipo ' => '0']);
        if(!empty($this->request->query['cliente_id'])) {
            $this->loadModel('Clientes');
            $clientes = $this->Clientes->get($this->request->query['cliente_id']);
            $query->andWhere(['Atendimentos.cliente_id ' => $this->request->query['cliente_id']]);
        }else{
            $clientes = null;
        }
        if(!empty($this->request->query['data'])){
            $query->andWhere(['Atendimentos.data ' => $this->Data->DataSQL($this->request->query['data'])]);
        }
        if(!empty($this->request->query['convenio_id'])){
            $query->andWhere(['Atendimentos.convenio_id ' => $this->request->query['convenio_id']]);
        }
        if(!empty($this->request->query['tipoatendimento_id'])){
            $query->andWhere(['Atendimentos.tipoatendimento_id ' => $this->request->query['tipoatendimento_id']]);
        }


        if(isset($this->request->query['tipo']) && $this->request->query['tipo']!="" ){
            $query->andWhere(['Atendimentos.status'=>$this->request->query['tipo']]);
        }else{
            $query->andWhere(['Atendimentos.status <'=>2]);
        }

        $convenio = $this->Convenios->find('list')->where(['Convenios.situacao_id'=>1]);
        $tipoatendimento = $this->TipoAtendimentos->find('list')->where(['TipoAtendimentos.situacao_id'=>1]);

        if(empty($this->request->query['sort'])){
            $query->orderDesc('Atendimentos.id');
        }

        $atendimentos = $this->paginate($query);
        $this->set(compact('atendimentos','clientes','convenio','tipoatendimento','contas'));
        $this->set('_serialize', ['atendimentos']);
    }


    /**
     * View method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Atendimentos');

        $atendimento = $this->Atendimentos->get($id, [
            'contain' => ['Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos',  'AtendimentoProcedimentos'=>['MedicoResponsaveis','Procedimentos','AtendimentoItens'=>['Dentes','Regioes','FaceItens'=>['Faces']]]]
        ]);

        $this->set('atendimento', $atendimento);
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('Atendimentos');
        $atendimento = $this->Atendimentos->newEntity();
        $this->loadModel('Procedimentos');
        $this->loadModel('MedicoResponsaveis');
        if ($this->request->is('post')) {
            $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
            if ($this->Atendimentos->save($atendimento)) {
                if(!empty($_SESSION['itens'])) {
                    $this->loadModel('AtendimentoProcedimentos');

                    foreach ($_SESSION['itens'] as $i => $item) {
                        $AtendProc = $this->AtendimentoProcedimentos->newEntity();
                        $AtendProc->procedimento_id = $item['procedimento_id'];
                        $AtendProc->atendimento_id = $atendimento->id;
                        $AtendProc->valor_base= $item['valor_base'];
                        $AtendProc->valor_fatura = $item['valor_faturar'];
                        $AtendProc->quantidade = 1;
                        $AtendProc->desconto = $this->request->data['desconto'];
                        $AtendProc->porc_desconto = number_format((float)($this->request->data['desconto']*100)/$this->request->data['total_geral'],2,'.','');
                        $AtendProc->valor_caixa = $item['valor_caixa'];
                        $AtendProc->digitado = 'Sim';
                        $AtendProc->medico_id = $item['medico_id'];
                        $AtendProc->valor_matmed = $this->request->data['total_liquido'];
                        if($this->AtendimentoProcedimentos->save($AtendProc)){
                            $this->loadModel('ItenCobrancas');
                                if(!empty($item['cobrancas'])) {
                                    foreach ($item['cobrancas'] as $cobranca => $cob) {
                                        $this->loadModel('AtendimentoItens');
                                        $it = $this->AtendimentoItens->newEntity();
                                        $it->tipo = $cob['type'];
                                        $it->descricao = $cob['describle'];
                                        $it->atendproc_id = $AtendProc->id;
                                        $it->valor_faturar = $cob['valor_faturar'];
                                        $it->status = 0;
                                        if ($this->AtendimentoItens->save($it)) {
                                            if(!empty($cob['faces'])) {
                                                $this->loadModel('FaceItens');
                                                foreach ($cob['faces'] as $face) {
                                                    $faceitens = $this->FaceItens->newEntity();
                                                    $faceitens->iten_id = $it->id;
                                                    $faceitens->face_id = $face;
                                                    $this->FaceItens->save($faceitens);
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                    unset($_SESSION['itens']);
                }
                $this->Flash->success(__('O orçamento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'view',$atendimento->id]);
            } else {
                $this->Flash->error(__('O orçamento não foi salvo. Por favor, tente novamente.'));
            }
        }else{
            unset($_SESSION['itens']);
        }
        $procedimentos = $this->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
        $medicos = $this->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');
        $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200])->orderAsc('TipoAtendimentos.nome');
        $this->set(compact('atendimento', 'unidades', 'convenios', 'tipoAtendimentos','procedimentos','medicos'));
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        unset($_SESSION['itens']);
        $this->loadModel('Atendimentos');
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
            if ($this->Atendimentos->save($atendimento)) {
                $this->Flash->success(__('O atendimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'view',$atendimento->id]);
            } else {
                $this->Flash->error(__('O atendimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');
        $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200])->orderAsc('TipoAtendimentos.nome');
        $clientes = $this->Atendimentos->Clientes->find('list')->where(['Clientes.id'=>$atendimento->cliente_id]);

        $this->set(compact('atendimento', 'clientes', 'unidades', 'convenios', 'tipoAtendimentos'));
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('Atendimentos');
        $this->request->allowMethod(['post', 'delete']);
        $atendimento = $this->Atendimentos->get($id);
                $atendimento->situacao_id = 2;
        if ($this->Atendimentos->save($atendimento)) {
            $this->Flash->success(__('O atendimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O atendimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function proc(){
        $this->loadModel('Atendimentos');
        $this->viewBuilder()->layout('ajax');
        if (!isset($_SESSION['itens'])) $_SESSION['itens'] = array();
        $add = false;
        $quant= 0;
        $id= 0;
        if (!empty($this->request->query['procedimento_id']) && !empty($this->request->query['convenio_id']) && !empty($this->request->query['medico_id'])) {
            foreach ($_SESSION['itens'] as $i => $item) {
                if ($this->request->query['procedimento_id'] == $item['procedimento_id']){
                    if($this->request->query['convenio_id']==$item['convenio_id']) {
                        if($this->request->query['convenio_id']==$item['convenio_id']) {
                            $add = false;
                        }
                    }
                }
            }
            if (!$add){
                $this->loadModel('PrecoProcedimentos');
                $this->loadModel('Procedimentos');
                $this->loadModel('Convenios');
                $this->loadModel('MedicoResponsaveis');

                $Preco = $this->PrecoProcedimentos;
                $Procedimento = $this->Procedimentos;
                $Convenios = $this->Convenios;
                $Medicos= $this->MedicoResponsaveis;

                $query = $Preco->find('all')
                    ->where(['convenio_id =' => $this->request->query['convenio_id'],
                        'procedimento_id =' => $this->request->query['procedimento_id'],
                        'situacao_id'=>1]);
                $preco_procedimento = $query->first();

                if($preco_procedimento) {
                    $procedimentos = $Procedimento->get((int)$preco_procedimento->procedimento_id);
                    $convenios = $Convenios->get((int)$preco_procedimento->convenio_id);
                    $Medicos = $Medicos->get((int)$this->request->query['medico_id']);

                    $_SESSION['itens'][] = array(
                        'procedimento_id' => $preco_procedimento->procedimento_id,
                        'convenio_id' => $preco_procedimento->convenio_id,
                        'medico_id' => $Medicos->id,
                        'medico_nome' => $Medicos->nome,
                        'valor_base' => $preco_procedimento->valor_base,
                        'valor_faturar' => $preco_procedimento->valor_faturar,
                        'valor_caixa' => $preco_procedimento->valor_particular,
                        'preco_id' => $preco_procedimento->id,
                        'procedimento' => $procedimentos->nome,
                        'convenio' => $convenios->nome
                    );
                }
            }
        }
        $itens = $_SESSION['itens'];
        $this->set(compact('itens'));
        $this->set('_serialize', ['itens']);
    }

    public function removeitem()
    {
        $this->viewBuilder()->layout('ajax');
        unset($_SESSION['itens'][$this->request->query['pos']]);
        exit;
    }

    public function convenios(){
        $this->loadModel('Atendimentos');
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Clientes');
        $cliente = $this->Clientes;
        $clientes = $cliente->get((int)$this->request->data['id']);
        if(empty($clientes))
            echo -1;
        else
            echo json_encode($clientes);
        exit;
    }

    public function getvalores()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $valor = 0;
        foreach ($_SESSION['itens'] as $i => $item) {
            if (!empty($item['cobrancas'])) {
                foreach ($item['cobrancas'] as $cobranca => $cob) {
                    if(!empty($cob['faces'])){
                        foreach ($cob['faces'] as $f){
                            $valor += (float)$item['valor_base'];
                        }
                    }else{
                        $valor += (float)$item['valor_base'];
                    }
                }
                $item['valor_faturar'] = $valor;
            }
        }
        echo json_encode((float)$valor);
    }


    public function editprocs(){
        $this->loadModel('Atendimentos');
        $atendimento = $this->Atendimentos->get($this->request->data['orcamento'], [
            'contain' => []
        ]);
        if (!empty($_SESSION['itens'])) {
            $this->loadModel('AtendimentoProcedimentos');
            foreach ($_SESSION['itens'] as $i => $item) {
                $AtendProc = $this->AtendimentoProcedimentos->newEntity();
                $AtendProc->procedimento_id = $item['procedimento_id'];
                $AtendProc->atendimento_id = $atendimento->id;
                $AtendProc->valor_base = $item['valor_base'];
                $AtendProc->valor_fatura = $item['valor_faturar'];
                $AtendProc->quantidade = 1;
                $AtendProc->desconto = 0;
                $AtendProc->porc_desconto = 0;
                $AtendProc->valor_caixa = $item['valor_caixa'];
                $AtendProc->digitado = 'Sim';
                $AtendProc->medico_id = $item['medico_id'];
                $AtendProc->valor_matmed = 0;
                if ($this->AtendimentoProcedimentos->save($AtendProc)) {
                    $this->loadModel('ItenCobrancas');
                    if (!empty($item['cobrancas'])) {
                        foreach ($item['cobrancas'] as $cobranca => $cob){
                            $this->loadModel('AtendimentoItens');
                            $it = $this->AtendimentoItens->newEntity();
                            $it->tipo = $cob['type'];
                            $it->descricao = $cob['describle'];
                            $it->valor_faturar = $cob['valor_faturar'];
                            $it->atendproc_id = $AtendProc->id;
                            $it->status = 0;
                            if ($this->AtendimentoItens->save($it)) {
                                if (!empty($cob['faces'])) {
                                    $this->loadModel('FaceItens');
                                    foreach ($cob['faces'] as $face) {
                                        $faceitens = $this->FaceItens->newEntity();
                                        $faceitens->iten_id = $it->id;
                                        $faceitens->face_id = $face;
                                        $this->FaceItens->save($faceitens);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            unset($_SESSION['itens']);
        }
        echo 1;
        $this->autoRender=false;
    }



}

