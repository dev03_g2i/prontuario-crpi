<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LogAgendas Controller
 *
 * @property \App\Model\Table\LogAgendasTable $LogAgendas
 */
class LogAgendasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($agenda_id = null)
    {
        $query = $this->LogAgendas->find('all')->orderDesc('LogAgendas.id');
        if(!empty($agenda_id))
            $query->andWhere(['LogAgendas.agenda_id' => $agenda_id]);

        $logAgendas = $this->paginate($query);

        $this->set(compact('logAgendas'));
        $this->set('_serialize', ['logAgendas']);

    }

    /**
     * View method
     *
     * @param string|null $id Log Agenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $logAgenda = $this->LogAgendas->get($id, [
            'contain' => []
        ]);

        $this->set('logAgenda', $logAgenda);
        $this->set('_serialize', ['logAgenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $logAgenda = $this->LogAgendas->newEntity();
        if ($this->request->is('post')) {
            $logAgenda = $this->LogAgendas->patchEntity($logAgenda, $this->request->data);
            if ($this->LogAgendas->save($logAgenda)) {
                $this->Flash->success(__('O log agenda foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O log agenda não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('logAgenda'));
        $this->set('_serialize', ['logAgenda']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Log Agenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $logAgenda = $this->LogAgendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $logAgenda = $this->LogAgendas->patchEntity($logAgenda, $this->request->data);
            if ($this->LogAgendas->save($logAgenda)) {
                $this->Flash->success(__('O log agenda foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O log agenda não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('logAgenda'));
        $this->set('_serialize', ['logAgenda']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Log Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $logAgenda = $this->LogAgendas->get($id);
                $logAgenda->situacao_id = 2;
        if ($this->LogAgendas->save($logAgenda)) {
            $this->Flash->success(__('O log agenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O log agenda não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Log Agenda id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->LogAgendas->find('all')
        ->where(['LogAgendas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('LogAgendas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Log Agenda id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $logAgenda = $this->LogAgendas->get($this->request->data['id']);
            $res = ['nome'=>$logAgenda->nome,'id'=>$logAgenda->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
