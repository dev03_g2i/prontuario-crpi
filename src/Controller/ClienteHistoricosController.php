<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;
use Dompdf\Dompdf;

/**
 * ClienteHistoricos Controller
 *
 * @property \App\Model\Table\ClienteHistoricosTable $ClienteHistoricos
 */
class ClienteHistoricosController extends AppController
{

    public $components = ['Configuracao'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $this->paginate = [
            'contain' => ['Clientes', 'TipoHistorias', 'SituacaoCadastros', 'Users','MedicoResponsaveis']
                        ,'conditions' => ['ClienteHistoricos.situacao_id = ' => '1']
                    ];
        $clienteHistoricos = $this->paginate($this->ClienteHistoricos);

        $this->set(compact('clienteHistoricos'));
        $this->set('_serialize', ['clienteHistoricos']);
    }
    public function all()
    {
        $session = new Session();
        $session->delete('retorno_update');
        $session->delete('retorno_itens');
        $session->delete('retorno_faces');

        $this->viewBuilder()->layout('ajax');
        $query = $this->ClienteHistoricos->find('all')
            ->contain(['Clientes', 'TipoHistorias', 'MedicoResponsaveis'])
            ->where(['ClienteHistoricos.situacao_id' => '1']);

        if(!empty($this->request->query['cliente_id'])){
            $query->andWhere(['ClienteHistoricos.cliente_id = ' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        }else{
            $cliente_id = null;
        }

        /** Prontuario Permissão Visualização
         *  Se tiver um medico setado na tabela @class Users e o campo prontuario_permissao_visualizacao
         *  da tabela @class Configuracoes estiver setado 2, filtra a listagem de acordo com medico setado.
         *  @author Luciano - 08/03/2018
         */
        $codigo_medico = $this->Auth->user('codigo_medico');
        if(!empty($codigo_medico) && $this->Configuracao->prontuarioPermissaoVisualizacao() == 2)
            $query->andWhere(['ClienteHistoricos.medico_id' => $codigo_medico]);
       /**********************************/

        $query->orderDesc('ClienteHistoricos.created');

        $clienteHistoricos = $this->paginate($query);

        $this->set(compact('clienteHistoricos','cliente_id'));
        $this->set('_serialize', ['clienteHistoricos']);
    }
    
    public function imprimir()
    {
        $clienteHistoricos = $this->ClienteHistoricos->get($this->request->query['id']);

        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.header_footer')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');

        $domPdf = new Dompdf();
        $domPdf->loadHtml($clienteHistoricos->descricao);
        $domPdf->setPaper('a4');
        $domPdf->render();

        $this->set(compact('clienteHistoricos'));
        $this->set('_serialize', ['clienteHistoricos   ']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Historico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $clienteHistorico = $this->ClienteHistoricos->get($id, [
            'contain' => ['Clientes', 'TipoHistorias', 'SituacaoCadastros', 'Users','MedicoResponsaveis',
                'HistoricoTratamentos'=>
                    [
                        'AtendimentoProcedimentos'=>['Procedimentos'],
                        'AtendimentoItens'=>['Dentes','Regioes','Financeiro','Executor','User_Financeiro','User_Executor'],
                        'FaceItens'=>['Faces','Financeiro','Executor','User_Financeiro','User_Executor']
                    ],
                'HistoricoFaceArtigos'=>[
                    'FaceArtigos'=>[
                        'Artigos'=>['strategy'=>'select'],
                        'FaceItens'=>['AtendimentoItens'=>['AtendimentoProcedimentos'=>['Procedimentos'],'Dentes','Regioes'],'Faces']
                    ]
                ],
                'HistoricoItenArtigos'=>[
                    'AtendimentoitenArtigos'=>[
                        'Artigos'=>['strategy'=>'select'],
                        'AtendimentoItens'=>['AtendimentoProcedimentos'=>['Procedimentos'],'Dentes','Regioes']
                    ]
                ]
            ]
        ]);

        $this->set('clienteHistorico', $clienteHistorico);
        $this->set('_serialize', ['clienteHistorico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($cliente)
    {
        $clienteHistorico = $this->ClienteHistoricos->newEntity();
        if ($this->request->is('post')) {
            $clienteHistorico = $this->ClienteHistoricos->patchEntity($clienteHistorico, $this->request->data);
            if ($this->ClienteHistoricos->save($clienteHistorico)) {

                //checo para ver se existe algum tratamento para vincular com o historico
                $session = new Session();
                $ret_tratamento = (object)$session->read('retorno_update');
                if(!empty($ret_tratamento)){
                    foreach ($ret_tratamento as $t) {
                        $t = (object)$t;

                        $this->loadModel('HistoricoTratamentos');
                        $find = $this->HistoricoTratamentos->find('all')
                            ->where(['HistoricoTratamentos.situacao_id'=>1])
                            ->andWhere(['HistoricoTratamentos.historico_id'=>$clienteHistorico->id])
                            ->andWhere(['HistoricoTratamentos.atendimento_id'=>$t->atendimento_id])
                            ->andWhere(['HistoricoTratamentos.atendimento_procedimento_id'=>$t->atendimento_procedimento_id])
                            ->andWhere(['HistoricoTratamentos.iten_id'=>$t->iten_id])
                            ->andWhere(['HistoricoTratamentos.origen'=>$t->origen]);
                            if(!empty($t->face_id)){
                                $find->andWhere(['HistoricoTratamentos.face_id'=>$t->face_id]);
                            }
                        $total = $find->count();

                        if($total<=0){
                            $this->loadModel('HistoricoTratamentos');
                            $hist_trat = $this->HistoricoTratamentos->newEntity();
                            $hist_trat->historico_id =$clienteHistorico->id;
                            $hist_trat->atendimento_id =$t->atendimento_id;
                            $hist_trat->atendimento_procedimento_id =$t->atendimento_procedimento_id;
                            $hist_trat->iten_id =$t->iten_id;
                            $hist_trat->origen =$t->origen;
                            $hist_trat->face_id =$t->face_id;

                            $this->HistoricoTratamentos->save($hist_trat);
                        }

                    }
                }


                $ret_itens = (object)$session->read('retorno_itens');
                if(!empty($ret_itens)){
                    $this->loadModel('HistoricoItenArtigos');
                    foreach ($ret_itens as $t) {
                        $t = (object)$t;
                        $iten_artigos = $this->HistoricoItenArtigos->find()
                            ->where(['HistoricoItenArtigos.situacao_id'=>1])
                            ->andWhere(['HistoricoItenArtigos.historico_id'=>$clienteHistorico->id])
                            ->andWhere(['HistoricoItenArtigos.aiten_artigo_id'=>$t->iten_id]);
                        $total = $iten_artigos->count();
                        if($total<=0){
                            $hist_inte= $this->HistoricoItenArtigos->newEntity();
                            $hist_inte->historico_id =$clienteHistorico->id;
                            $hist_inte->aiten_artigo_id =$t->iten_id;
                            $this->HistoricoItenArtigos->save($hist_inte);
                        }
                    }
                }

                $ret_faces= (object)$session->read('retorno_faces');

                if(!empty($ret_faces)){
                    $this->loadModel('HistoricoFaceArtigos');
                    foreach ($ret_faces as $t) {
                        $t = (object)$t;
                        $faces_artigos = $this->HistoricoFaceArtigos->find()
                            ->where(['HistoricoFaceArtigos.situacao_id'=>1])
                            ->andWhere(['HistoricoFaceArtigos.historico_id'=>$clienteHistorico->id])
                            ->andWhere(['HistoricoFaceArtigos.face_artigo_id'=>$t->iten_id]);
                        $totalart = $faces_artigos->count();

                        if($totalart<=0){
                            $hist_faces= $this->HistoricoFaceArtigos->newEntity();
                            $hist_faces->historico_id =$clienteHistorico->id;
                            $hist_faces->face_artigo_id =$t->iten_id;
                            if($this->HistoricoFaceArtigos->save($hist_faces)){
                             echo 1;
                            }else{
                                echo 'erro';
                            }
                        }
                    }
                }

                echo $cliente;
            } else {
                echo -1;
            }
            $this->autoRender = false;
        }
        $this->loadModel('TipoHistoriaModelos');
        $clientes = $this->ClienteHistoricos->Clientes->find('list', ['limit' => 200]);
        $tipoHistorias = $this->ClienteHistoricos->TipoHistorias->find('list', ['limit' => 200]);
        $medicoResponsaveis = $this->ClienteHistoricos->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'=>1]);
        $this->set(compact('clienteHistorico', 'clientes', 'tipoHistorias', 'cliente','medicoResponsaveis'));
        $this->set('_serialize', ['clienteHistorico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Historico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null,$cliente)
    {
        $clienteHistorico = $this->ClienteHistoricos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteHistorico = $this->ClienteHistoricos->patchEntity($clienteHistorico, $this->request->data);
            if ($this->ClienteHistoricos->save($clienteHistorico)) {
                echo $cliente;
            } else {
                echo -1;
            }
            $this->autoRender = false;
        }
        $clientes = $this->ClienteHistoricos->Clientes->find('list', ['limit' => 200]);
        $tipoHistorias = $this->ClienteHistoricos->TipoHistorias->find('list', ['limit' => 200]);
        $medicoResponsaveis = $this->ClienteHistoricos->MedicoResponsaveis->find('list', ['limit' => 200])->where(['MedicoResponsaveis.situacao_id'=>1]);

        $this->set(compact('clienteHistorico', 'clientes', 'tipoHistorias', 'cliente','medicoResponsaveis'));
        $this->set('_serialize', ['clienteHistorico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Historico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteHistorico = $this->ClienteHistoricos->get($id);
                $clienteHistorico->situacao_id = 2;
        if ($this->ClienteHistoricos->save($clienteHistorico)) {
            $this->Flash->success(__('O cliente historico foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente historico não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
