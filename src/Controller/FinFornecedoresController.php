<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinFornecedores Controller
 *
 * @property \App\Model\Table\FinFornecedoresTable $FinFornecedores
 */
class FinFornecedoresController extends AppController
{

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Fornecedores');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finFornecedores = $this->FinFornecedores->find();

        $fornecedor_id = !empty($this->request->query(['fornecedor'])) ? $this->request->query(['fornecedor']) : null;

        if(isset($fornecedor_id)) $finFornecedores = $finFornecedores->where(['FinFornecedores.id'=>$fornecedor_id]);

        $finFornecedores = $this->paginate($finFornecedores);
        $this->set(compact('finFornecedores'));
        $this->set('_serialize', ['finFornecedores']);

    }

    /**
     * View method
     *
     * @param string|null $id Fin Fornecedore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finFornecedore = $this->FinFornecedores->get($id, [
            'contain' => []
        ]);

        $this->set('finFornecedore', $finFornecedore);
        $this->set('_serialize', ['finFornecedore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finFornecedore = $this->FinFornecedores->newEntity();
        if ($this->request->is('post')) {
            $newFornecedor = $this->request->data;
            $newFornecedor['situacao'] = 1;
            $finFornecedore = $this->FinFornecedores->patchEntity($finFornecedore, $newFornecedor);
            if ($this->FinFornecedores->save($finFornecedore)) {
                $this->Flash->success(__('O fornecedor foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fornecedor não foi salvo. Por favor, tente novamente.'));
            }
        }

        $planoContas = $this->FinFornecedores->FinPlanoContas->find('list');

        $this->set(compact('finFornecedore', 'planoContas'));
        $this->set('_serialize', ['finFornecedore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Fornecedore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finFornecedore = $this->FinFornecedores->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $newFornecedor = $this->request->data;
            $newFornecedor['situacao'] = 1;

            $finFornecedore = $this->FinFornecedores->patchEntity($finFornecedore, $newFornecedor);

            if ($this->FinFornecedores->save($finFornecedore)) {
                $this->Flash->success(__('O fornecedor foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fornecedor não foi salvo. Por favor, tente novamente.'));
            }
        }

        $planoContas = $this->FinFornecedores->FinPlanoContas->find('list');

        $this->set(compact('finFornecedore', 'planoContas'));
        $this->set('_serialize', ['finFornecedore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Fornecedore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finFornecedore = $this->FinFornecedores->get($id);
        $finFornecedore->situacao = 2;
        if ($this->FinFornecedores->save($finFornecedore)) {
            $this->Flash->success(__('O fornecedor foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fornecedor não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Fornecedore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinFornecedores->find('all')
        ->where(['FinFornecedores.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinFornecedores.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Fornecedore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finFornecedore = $this->FinFornecedores->get($this->request->data['id']);
            $res = ['nome'=>$finFornecedore->nome,'id'=>$finFornecedore->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
