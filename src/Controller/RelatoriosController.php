<?php

namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\ClienteClone;
use App\Model\Entity\FinContabilidade;
use App\Model\Entity\FinFornecedore;
use App\Model\Entity\FinPlanoConta;
use Cake\I18n\Time;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;
use DateTime;
use function GuzzleHttp\Psr7\_caseless_remove;

class RelatoriosController extends AppController
{

    public $components = array('Data', 'Json', 'Relatorios', 'Xml', 'Faturamento', 'ContaPagar', 'Movimentos');

    public function create()
    {
        $this->viewBuilder()->layout('relatorios');
    }

    public function executor()
    {
        $this->loadModel('AtendimentoItens');

        if (!empty($this->request->query['inicio'])) {
            $inicio = new DateTime();
            $inic = $inicio->createFromFormat('d/m/Y', $this->request->query['inicio']);
            $inicio = $inic->format('Y-m-d') . " 01:00:00";
        } else {
            $inicio = date('Y-m-') . "01 01:00:00";
        }

        if (!empty($this->request->query['fim'])) {
            $fim = new DateTime();
            $fin = $fim->createFromFormat('d/m/Y', $this->request->query['fim']);
            $fim = $fin->format('Y-m-d') . " 23:59:00";
        } else {
            $fim = date('Y-m-t') . " 23:59:00";
        }

        $profissional_id = null;
        $quantidade_prof = 0;
        $profissional_creditos = [];
        if (!empty($this->request->query['profissional_id'])) {
            $profissional_id = $this->request->query['profissional_id'];
            $this->loadModel('ProfissionalCreditos');
            $profissional_creditos = $this->ProfissionalCreditos->find('all')
                ->contain(['MedicoResponsaveis'])
                ->where(['ProfissionalCreditos.status' => 0])
                ->andWhere(['ProfissionalCreditos.profissional_id' => $profissional_id]);

            $quantidade_prof = $profissional_creditos->count();
        }

        $atendimento_itens = $this->AtendimentoItens->find('all')
            ->contain(['Dentes', 'Regioes', 'Executor',
                'AtendimentoProcedimentos' => [
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'Atendimentos' => ['Clientes']]
            ])
            ->where(['AtendimentoItens.data_executor >=' => $inicio])
            ->andWhere(['AtendimentoItens.data_executor <=' => $fim]);

        if (!empty($profissional_id)) {
            $atendimento_itens->andWhere(['AtendimentoItens.executor_id' => $profissional_id]);
        }

        $this->loadModel('FaceItens');
        $face_itens = $this->FaceItens->find('all')
            ->contain(['Executor', 'Faces',
                'AtendimentoItens' => [
                    'Dentes', 'Regioes',
                    'AtendimentoProcedimentos' => [
                        'Procedimentos' => ['GrupoProcedimentos'],
                        'Atendimentos' => ['Clientes']]
                ]])
            ->where(['FaceItens.data_executor >=' => $inicio])
            ->andWhere(['FaceItens.data_executor <=' => $fim])
            ->andWhere(['FaceItens.situacao_id' => 1]);

        if (!empty($profissional_id)) {
            $face_itens->andWhere(['FaceItens.executor_id' => $profissional_id]);
        }

        $itens = [];
        if (!empty($atendimento_itens)) {
            foreach ($atendimento_itens as $a):
                $atendimento = $a->atendimento_procedimento->atendimento;
                $procedimento = $a->atendimento_procedimento->procedimento;

                $valor_base = empty($a->atendimento_procedimento->valor_base) ? $a->atendimento_procedimento->valor_fatura : $a->valor_faturar;
                $valor_faturar = $valor_base * ($procedimento->grupo_procedimento->percentual_lucro / 100);
                $valor_desconto = $valor_faturar * ($procedimento->grupo_procedimento->percentual_desconto / 100);
                $valor_liquido = $valor_faturar - $valor_desconto;

                $itens[] = (object)[
                    'cliente' => $atendimento->cliente->nome,
                    'executor' => $a->executor->nome,
                    'atendimento_id' => $atendimento->id,
                    'procedimento' => $procedimento->nome,
                    'tipo' => ($a->tipo == 'd' ? 'Dente' : 'Região'),
                    'descricao' => (!empty($a->dente) ? $a->dente->descricao : $a->regio->nome),
                    'face' => 'Sem Faces',
                    'valor_iten' => $valor_base,
                    'valor_faturar' => $valor_faturar,
                    'valor_desconto' => $valor_desconto,
                    'valor_liquido' => $valor_liquido,
                    'data_executor' => $a->data_executor
                ];

            endforeach;
        }

        if (!empty($face_itens)) {
            foreach ($face_itens as $f):
                $atendimento = $f->atendimento_iten->atendimento_procedimento->atendimento;
                $procedimento = $f->atendimento_iten->atendimento_procedimento->procedimento;
                $atendimento_procedimento = $f->atendimento_iten->atendimento_procedimento;

                if (!empty($atendimento_procedimento->valor_base)) {
                    //conta quantas faces existem para dividir pelo valor
                    $qt_faces = $this->FaceItens->find()
                        ->where(['FaceItens.situacao_id' => 1])
                        ->andWhere(['FaceItens.iten_id' => $f->iten_id])
                        ->count();

                    $valor_base = ($f->atendimento_iten->valor_faturar / $qt_faces);
                } else {
                    $valor_base = $atendimento_procedimento->valor_fatura;
                }

                $valor_faturar = $valor_base * ($procedimento->grupo_procedimento->percentual_lucro / 100);
                $valor_desconto = $valor_faturar * ($procedimento->grupo_procedimento->percentual_desconto / 100);
                $valor_liquido = $valor_faturar - $valor_desconto;

                $itens[] = (object)[
                    'cliente' => $atendimento->cliente->nome,
                    'executor' => $f->executor->nome,
                    'atendimento_id' => $atendimento->id,
                    'procedimento' => $procedimento->nome,
                    'tipo' => ($f->atendimento_iten->tipo == 'd' ? 'Dente' : 'Região'),
                    'descricao' => (!empty($f->atendimento_iten->dente) ? $f->atendimento_iten->dente->descricao : $f->atendimento_iten->regio->descricao),
                    'face' => $f->face->nome,
                    'valor_iten' => $valor_base,
                    'valor_faturar' => $valor_faturar,
                    'valor_desconto' => $valor_desconto,
                    'valor_liquido' => $valor_liquido,
                    'data_executor' => $f->data_executor

                ];

            endforeach;
        }

        $itens = (object)$itens;
        $this->set(compact('itens', 'inicio', 'fim', 'atendimento_itens', 'profissional_creditos', 'quantidade_prof'));
        $this->set('_serialize', ['itens']);

    }

    public function financeiro()
    {
        $this->loadModel('AtendimentoItens');

        if (!empty($this->request->query['inicio'])) {
            $inicio = new DateTime();
            $inic = $inicio->createFromFormat('d/m/Y', $this->request->query['inicio']);
            $inicio = $inic->format('Y-m-d') . " 01:00:00";
        } else {
            $inicio = date('Y-m-') . "01 01:00:00";
        }

        if (!empty($this->request->query['fim'])) {
            $fim = new DateTime();
            $fin = $fim->createFromFormat('d/m/Y', $this->request->query['fim']);
            $fim = $fin->format('Y-m-d') . " 23:59:00";
        } else {
            $fim = date('Y-m-t') . " 23:59:00";
        }

        $atendimento_itens = $this->AtendimentoItens->find('all')
            ->contain(['Dentes', 'Regioes', 'Executor',
                'AtendimentoProcedimentos' => [
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'Atendimentos' => ['Clientes', 'ContasReceber' => [
                        'conditions' => [
                            'ContasReceber.vencimento >=' => $inicio,
                            'ContasReceber.vencimento <=' => $fim
                        ]
                    ]
                    ]
                ]
            ])
            ->where(['AtendimentoItens.data_executor >=' => $inicio])
            ->andWhere(['AtendimentoItens.data_executor <=' => $fim]);

        $this->loadModel('FaceItens');
        $face_itens = $this->FaceItens->find('all')
            ->contain(['Executor', 'Faces',
                'AtendimentoItens' => [
                    'Dentes', 'Regioes',
                    'AtendimentoProcedimentos' => [
                        'Procedimentos' => ['GrupoProcedimentos'],
                        'Atendimentos' => ['Clientes', 'ContasReceber' => [
                            'conditions' => [
                                'ContasReceber.vencimento >=' => $inicio,
                                'ContasReceber.vencimento <=' => $fim
                            ]
                        ]]]
                ]])
            ->where(['FaceItens.data_executor >=' => $inicio])
            ->andWhere(['FaceItens.data_executor <=' => $fim])
            ->andWhere(['FaceItens.situacao_id' => 1]);
        $itens = [];
        if (!empty($atendimento_itens)) {
            foreach ($atendimento_itens as $a):
                $atendimento = $a->atendimento_procedimento->atendimento;
                $procedimento = $a->atendimento_procedimento->procedimento;
                $valor_base = empty($a->atendimento_procedimento->valor_base) ? $a->atendimento_procedimento->valor_fatura : $a->valor_faturar;

                $valor_faturar = $valor_base * ($procedimento->grupo_procedimento->percentual_lucro / 100);
                $valor_desconto = $valor_faturar * ($procedimento->grupo_procedimento->percentual_desconto / 100);
                $valor_liquido = $valor_faturar - $valor_desconto;
                $contas_receber = $atendimento->contas_receber;

                $total_contas = 0;
                if (!empty($contas_receber)) {
                    foreach ($contas_receber as $conta) {
                        $total_contas += $conta->valor;
                    }
                }
                $itens[] = (object)[
                    'cliente' => $atendimento->cliente->nome,
                    'executor' => $a->executor->nome,
                    'atendimento_id' => $atendimento->id,
                    'procedimento' => $procedimento->nome,
                    'tipo' => ($a->tipo == 'd' ? 'Dente' : 'Região'),
                    'descricao' => (!empty($a->dente) ? $a->dente->descricao : $a->regio->nome),
                    'face' => 'Sem Faces',
                    'valor_iten' => $valor_base,
                    'valor_faturar' => $valor_faturar,
                    'valor_desconto' => $valor_desconto,
                    'valor_liquido' => $valor_liquido,
                    'data_executor' => $a->data_executor,
                    'valor_conta' => $total_contas
                ];

            endforeach;
        }

        if (!empty($face_itens)) {
            foreach ($face_itens as $f):
                $atendimento = $f->atendimento_iten->atendimento_procedimento->atendimento;
                $procedimento = $f->atendimento_iten->atendimento_procedimento->procedimento;
                $atendimento_procedimento = $f->atendimento_iten->atendimento_procedimento;
                if (!empty($atendimento_procedimento->valor_base)) {
                    //conta quantas faces existem para dividir pelo valor
                    $qt_faces = $this->FaceItens->find()
                        ->where(['FaceItens.situacao_id' => 1])
                        ->andWhere(['FaceItens.iten_id' => $f->iten_id])
                        ->count();

                    $valor_base = ($f->atendimento_iten->valor_faturar / $qt_faces);
                } else {
                    $valor_base = $atendimento_procedimento->valor_fatura;
                }
                $valor_faturar = $valor_base * ($procedimento->grupo_procedimento->percentual_lucro / 100);
                $valor_desconto = $valor_faturar * ($procedimento->grupo_procedimento->percentual_desconto / 100);
                $valor_liquido = $valor_faturar - $valor_desconto;
                $contas_receber = $atendimento->contas_receber;

                $total_contas = 0;
                if (!empty($contas_receber)) {
                    foreach ($contas_receber as $conta) {
                        $total_contas += $conta->valor;
                    }
                }
                $itens[] = (object)[
                    'cliente' => $atendimento->cliente->nome,
                    'executor' => $f->executor->nome,
                    'atendimento_id' => $atendimento->id,
                    'procedimento' => $procedimento->nome,
                    'tipo' => ($f->atendimento_iten->tipo == 'd' ? 'Dente' : 'Região'),
                    'descricao' => (!empty($f->atendimento_iten->dente) ? $f->atendimento_iten->dente->descricao : $f->atendimento_iten->regio->descricao),
                    'face' => $f->face->nome,
                    'valor_iten' => $valor_base,
                    'valor_faturar' => $valor_faturar,
                    'valor_desconto' => $valor_desconto,
                    'valor_liquido' => $valor_liquido,
                    'data_executor' => $f->data_executor,
                    'valor_conta' => $total_contas

                ];

            endforeach;
        }

        $itens = (object)$itens;
        $this->set(compact('itens', 'inicio', 'fim', 'atendimento_itens'));
        $this->set('_serialize', ['itens']);

    }

    public function artigos()
    {
        $this->loadModel('AtendimentoitenArtigos');

        if (!empty($this->request->query['inicio'])) {
            $inicio = new DateTime();
            $inic = $inicio->createFromFormat('d/m/Y', $this->request->query['inicio']);
            $inicio = $inic->format('Y-m-d') . " 01:00:00";
        } else {
            $inicio = date('Y-m-') . "01 01:00:00";
        }

        if (!empty($this->request->query['fim'])) {
            $fim = new DateTime();
            $fin = $fim->createFromFormat('d/m/Y', $this->request->query['fim']);
            $fim = $fin->format('Y-m-d') . " 23:59:00";
        } else {
            $fim = date('Y-m-t') . " 23:59:00";
        }

        $atendimento_itens = $this->AtendimentoitenArtigos->find('all')
            ->contain(['Artigos' => ['strategy' => 'select'],
                'AtendimentoItens' => [
                    'Dentes', 'Regioes',
                    'AtendimentoProcedimentos' => ['Procedimentos', 'Atendimentos' => ['Clientes']]
                ]
            ])
            ->where(['AtendimentoitenArtigos.created >=' => $inicio])
            ->andWhere(['AtendimentoitenArtigos.created <=' => $fim]);

        if (!empty($this->request->query['artigo_id'])) {
            $atendimento_itens->andWhere(['AtendimentoitenArtigos.artigo_id' => $this->request->query['artigo_id']]);

        }


        $this->loadModel('FaceArtigos');
        $face_itens = $this->FaceArtigos->find('all')
            ->contain(['Artigos' => ['strategy' => 'select'],
                'FaceItens' => [
                    'Faces',
                    'AtendimentoItens' => [
                        'AtendimentoProcedimentos' => ['Procedimentos', 'Atendimentos' => ['Clientes']], 'Dentes', 'Regioes'
                    ]
                ]])
            ->where(['FaceArtigos.created >=' => $inicio])
            ->andWhere(['FaceArtigos.created <= ' => $fim]);

        if (!empty($this->request->query['artigo_id'])) {
            $face_itens->andWhere(['FaceArtigos.artigo_id' => $this->request->query['artigo_id']]);
        }

        $itens = [];
        if (!empty($atendimento_itens)) {
            foreach ($atendimento_itens as $a):
                $atendimento = $a->atendimento_iten->atendimento_procedimento->atendimento;
                $procedimento = $a->atendimento_iten->atendimento_procedimento->procedimento;
                $itens[] = (object)[
                    'cliente' => $atendimento->cliente->nome,
                    'artigo' => $a->artigo->nome,
                    'codigo' => $a->artigo->codigo_livre,
                    'repasse' => $a->artigo->utiliza_repasse,
                    'valor_repasse' => $a->artigo->valor_repasse,
                    'atendimento_id' => $atendimento->id,
                    'procedimento' => $procedimento->nome,
                    'tipo' => ($a->atendimento_iten->tipo == 'd' ? 'Dente' : 'Região'),
                    'descricao' => (!empty($a->atendimento_iten->dente) ? $a->atendimento_iten->dente->descricao : $a->atendimento_iten->regio->nome),
                    'face' => 'Sem Faces',
                    'created' => $a->created
                ];

            endforeach;
        }

        if (!empty($face_itens)) {
            foreach ($face_itens as $f):
                $atendimento = $f->face_iten->atendimento_iten->atendimento_procedimento->atendimento;
                $procedimento = $f->face_iten->atendimento_iten->atendimento_procedimento->procedimento;


                $itens[] = (object)[
                    'cliente' => $atendimento->cliente->nome,
                    'artigo' => $f->artigo->nome,
                    'codigo' => $f->artigo->codigo_livre,
                    'repasse' => $f->artigo->utiliza_repasse,
                    'valor_repasse' => $f->artigo->valor_repasse,
                    'atendimento_id' => $atendimento->id,
                    'procedimento' => $procedimento->nome,
                    'tipo' => ($f->face_iten->atendimento_iten->tipo == 'd' ? 'Dente' : 'Região'),
                    'descricao' => (!empty($f->face_iten->atendimento_iten->dente) ? $f->face_iten->atendimento_iten->dente->descricao : $f->face_iten->atendimento_iten->regio->descricao),
                    'face' => $f->face_iten->face->nome,
                    'created' => $f->created

                ];

            endforeach;
        }

        $itens = (object)$itens;
        $this->set(compact('itens', 'inicio', 'fim', 'atendimento_itens'));
        $this->set('_serialize', ['itens']);

    }

    public function caixaSimplificado()
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('Contasreceber');
        $contas = $this->Contasreceber->find('all')
            ->contain([
                'Atendimentos' => [
                    'strategy' => 'select',
                    'Agendas' => [
                        'GrupoAgendas'
                    ]
                ],
                'Status',
                'TipoPagamento'
            ])
            ->where(['Contasreceber.status_id' => 1]);
        $inicio = -1;
        $fim = -1;
        if (!empty($this->request->data['inicio'])) {
            $inicio = new DateTime();
            $now = $inicio->createFromFormat('d/m/Y', $this->request->data['inicio']);
            $contas->andWhere(['Contasreceber.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
            $inicio = $this->request->data['inicio'];
        }

        if (!empty($this->request->data['fim'])) {
            $fim = new DateTime();
            $now = $fim->createFromFormat('d/m/Y', $this->request->data['fim']);
            $contas->andWhere(['Contasreceber.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
            $fim = $this->request->data['fim'];
        }

        $json['dados'] = null;
        foreach ($contas as $a) {
            $json['dados'][] = [
                'data' => $this->Data->data_sms($a->data),
                'tipo_pagamento' => $a->tipo_pagamento->descricao,
                'valor' => $a->valor,
                'agenda' => $a->atendimento->agenda->grupo_agenda->nome
            ];
        }
        $json['alias'][] = [
            'inicio' => $inicio,
            'fim' => $fim
        ];
        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
        $this->Json->create('caixa_simplificado.json', json_encode($json));
        $this->set('report', 'caixa_simplificado.mrt');
        $this->render('report');
    }

    public function caixaDiario()
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('ContasReceber');
        $contas = $this->ContasReceber->find('all')
            ->contain(['Planocontas', 'ClienteClone', 'Status', 'Contabilidade', 'TipoPagamento', 'TipoDocumento'])
            ->where(['Contasreceber.status_id' => 1]);
        $inicio = -1;
        $fim = -1;

        if (!empty($this->request->data['inicio'])) {
            $inicio = new DateTime();
            $now = $inicio->createFromFormat('d/m/Y', $this->request->data['inicio']);
            $contas->andWhere(['Contasreceber.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
            $inicio = $this->request->data['inicio'];
        }

        if (!empty($this->request->data['fim'])) {
            $fim = new DateTime();
            $now = $fim->createFromFormat('d/m/Y', $this->request->data['fim']);
            $contas->andWhere(['Contasreceber.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
            $fim = $this->request->data['fim'];
        }

        $json['dados'] = null;
        $contas->orderAsc('ClienteClone.nome');
        foreach ($contas as $a) {
            $json['dados'][] = [
                'id' => $a->id,
                'cliente' => !empty($a->cliente_clone->nome) ? $a->cliente_clone->nome : null,
                'cliente_id' => $a->cliente_id,
                'data' => $this->Data->data_sms($a->data),
                'vencimento' => $this->Data->data_sms($a->vencimento),
                'plano_contas' => !empty($a->planoconta->nome) ? $a->planoconta->nome : null,
                'empresa' => !empty($a->contabilidade->nome) ? $a->contabilidade->nome : null,
                'parcela' => $a->parcela,
                'doc_fiscal' => $a->numfiscal,
                'tipo_pagamento' => $a->tipo_pagamento->descricao,
                'tipo_documento' => $a->tipo_documento->descricao,
                'numero_documento' => $a->numerodocumento,
                'valor' => $a->valor,
                'juros' => $a->juros,
                'multa' => $a->multa,
                'valor_bruto' => $a->valorBruto,
                'desconto' => $a->desconto,
                'data_pagamento' => !empty($a->data_pagamento) ? $this->Data->data_sms($a->data_pagamento) : null,
                'data_fiscal' => !empty($a->datafiscal) ? $this->Data->data_sms($a->datafiscal) : null,
                'complemento' => !empty($a->complemento) ? $a->complemento : null,

            ];
        }
        $json['alias'][] = [
            'inicio' => $inicio,
            'fim' => $fim
        ];

        $this->Json->create('caixa_diario.json', json_encode($json));
        $this->set('report', 'caixa_diario.mrt');
        $this->render('report');

    }

    public function caixaAnalitico()
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('Atendimentos');
        $this->loadModel('ContasReceber');
        $contas = $this->Atendimentos->find('all')
            ->contain([
                'ContasReceber' => [
                    'strategy' => 'select',
                    'conditions' => ['Contasreceber.status_id' => 1]
                ],
                'Clientes',
                'Agendas' => ['GrupoAgendas'],
                'Convenios',
                'AtendimentoProcedimentos' => ['Procedimentos']

            ]);

        $inicio = -1;
        $fim = -1;
        $validador = false;

        if (!empty($this->request->data['inicio'])) {
            $inicio = new DateTime();
            $now = $inicio->createFromFormat('d/m/Y', $this->request->data['inicio']);
            $contas->where(['Atendimentos.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
            $inicio = $this->request->data['inicio'];
            $validador = true;
        }

        if (!empty($this->request->data['fim'])) {
            $fim = new DateTime();
            $now = $fim->createFromFormat('d/m/Y', $this->request->data['fim']);
            if ($validador)
                $contas->andWhere(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
            else
                $contas->where(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
            $fim = $this->request->data['fim'];
        }

        $this->set('contas', $contas);
        $json['atendimentos'] = null;
        foreach ($contas as $a) {
            $valor = 0;
            foreach ($a->contas_receber as $c)
                $valor += $c->valor;
            if (!empty($a->atendimento_procedimentos)) {
                $count = 0;
                foreach ($a->atendimento_procedimentos as $ap) {
                    $json['atendimentos'][] = [
                        'atendimento' => $a->id,
                        'data' => $this->Data->data_sms($a->data),
                        'paciente' => $a->cliente->nome,
                        'convenio' => $a->convenio->nome,
                        'nnf' => empty($a->conta_receber->numfiscal) ? '' : $a->conta_receber->numfiscal,
                        'procedimento' => $ap->procedimento->nome,
                        'valor' => empty($ap->valor_caixa) ? 0 : $ap->valor_caixa,
                        'caixa' => ($count > 0) ? 0 : $valor,
                        'agenda' => $a->agenda->grupo_agenda->nome
                    ];
                    $count++;
                }
            }
        }


        if ($inicio != -1 && $fim != -1) {
            $json['alias'][] = [
                'inicio' => $inicio,
                'fim' => $fim
            ];
        } elseif ($inicio != -1 && $fim == -1) {
            $json['alias'][] = [
                'inicio' => $inicio,
                'fim' => ''
            ];
        } elseif ($inicio == -1 && $fim != -1) {
            $json['alias'][] = [
                'inicio' => '',
                'fim' => $fim
            ];
        } else
            $json['alias'][] = [
                'inicio' => '',
                'fim' => ''
            ];
        $this->Json->create('caixa_analitico.json', json_encode($json));
        $this->set('report', 'caixa_analitico.mrt');
        $this->render('report');

    }

    /**
     * This function is responsible for creating a json for the cashier report
     * Esta função é responsável por criar um json para o relatório de caixa executante
     * @author Iohan
     */
    public function caixaExecutante()
    {
        $this->viewBuilder()->layout('report_2016');
        $report = $this->request->data['report'];

        if (!empty($this->request->data['usuario_id'])) {
            $user = $this->request->data['usuario_id'];
        } else {
            $user = $this->Auth->user('id');
        }

        switch ($report) {

            case 1: //Opção desabilitada
                $json = $this->Relatorios->caixa_executante(
                    $this->request->data['medico_id'],
                    $this->request->data['inicio'],
                    $this->request->data['fim'],
                    null,
                    $this->request->data['periodo_id']);
                $this->set('report', 'caixa_analitico_executante.mrt');
                break;
            case 2:
                $json = $this->Relatorios->caixa_analitico_geral(
                    $this->request->data['medico_id'],
                    $this->request->data['inicio'],
                    $this->request->data['fim'],
                    null,
                    $this->request->data['periodo_id'],
                    null,
                    false,
                    'Relatório de Registros (Fatores/Particulares) x Recebimento - Geral'
                );
                $this->set('report', 'caixa_analitico_geral.mrt');
                break;
            case 3:
                $json = $this->Relatorios->caixa_analitico_geral(
                    $this->request->data['medico_id'],
                    $this->request->data['inicio'],
                    $this->request->data['fim'],
                    null,
                    $this->request->data['periodo_id'],
                    $user,
                    //                   $user['id'],
                    false,
                    'Relatório de Registros (Fatores/Particulares) x Recebimento - Individual'
                );
                $this->set('report', 'caixa_analitico_geral.mrt');
                break;
            case 4:
                $json = $this->Relatorios->relatorio_recebimento_individual(
                    $this->request->data['medico_id'],
                    $this->request->data['inicio'],
                    $this->request->data['fim'],
                    null,
                    $this->request->data['periodo_id'],
                    $user,
                    //$user['id'],
                    true,
                    'Relatório de Caixa - Apenas Recebimento Individual'
                );
                $this->set('report', 'caixa_analitico_geral.mrt');
                break;
            case 5:
                $json = $this->Relatorios->relatorio_recebimento(
                    $this->request->data['medico_id'],
                    $this->request->data['inicio'],
                    $this->request->data['fim'],
                    null,
                    $this->request->data['periodo_id'],
                    null,
                    true,
                    'Relatório de Caixa - Recebimento Geral'
                );
                $this->set('report', 'caixa_analitico_geral.mrt');
                break;
            case 6:
                $json = $this->Relatorios->caixa_devedor(
                    $this->request->getData('inicio'),
                    $this->request->getData('fim'),
                    $user,
                    $this->request->getData('periodo_id'),
                    'Relatório de Atendimento Devedores - Individual');
                break;
            case 7:
                $json = $this->Relatorios->caixa_devedor(
                    $this->request->getData('inicio'),
                    $this->request->getData('fim'),
                    null,
                    $this->request->getData('periodo_id'),
                    'Relatório de Atendimento Devedores - Geral'
                );
                $this->set('report', 'caixa_devedor.mrt');
                break;
            case 8: //Opção Desabilitada
                $json = $this->Relatorios->atendimento_solicitante($this->request->getData(['solicitante_id']),
                    $this->request->getData(['inicio']), $this->request->getData(['fim']));
                $this->set('report', 'atendimento_solicitante.mrt');
        }
        $this->Json->create('caixa.json', json_encode($json));
        $this->set('user', $user);

    }

    public function financeiroMovimentos()
    {
        $this->viewBuilder()->setLayout('report_2016');
        $this->loadModel('FinMovimentos');
        $this->loadModel('FinBancoMovimentos');

        $movimentos = $this->FinMovimentos->find()->contain(['FinBancoMovimentos', 'FinPlanoContas', 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes']);

        $movimentos = $this->Movimentos->filtroMovimentosRelatorios($movimentos, $this->request->getData());
        $saldoInicial = $this->FinBancoMovimentos->getSaldosPorIdBancoMovimento($this->request->getData('movimento'), 'inicial');
        $movimentos = $this->Movimentos->calculaSaldoMovimentos($movimentos, $saldoInicial);

        $json['dados'] = null;
        $aux = 0;
        foreach ($movimentos as $movimento) {
            $aux++;
            $credor_cliente = '';
            $bancomovimento = '';

            if ($movimento->has('fin_fornecedore')) {
                $credor_cliente = $movimento->fin_fornecedore->nome;
            } else if ($movimento->has('cliente')) {
                $credor_cliente = $movimento->cliente->nome;
            }

            if (!empty($movimento->fin_banco->nome) && !empty($movimento->fin_banco_movimento->data_movimento)) {
                $bancomovimento = $movimento->fin_banco->nome . ' - ' . $movimento->fin_banco_movimento->data_movimento;
            }

            $json['dados'][] = [
                'id' => $movimento->id,
                'data' => !empty($movimento->data) ? $this->Data->data_sms($movimento->data) : '',
                'credorcliente' => $credor_cliente,
                'planocontas' => !empty($movimento->fin_plano_conta->nome) ? $movimento->fin_plano_conta->nome : '',
                'empresacontabilidade' => !empty($movimento->fin_contabilidade->nome) ? $movimento->fin_contabilidade->nome : '',
                'documento' => !empty($movimento->documento) ? $movimento->documento : '',
                'credito' => !empty($movimento->credito) ? $movimento->credito : 0.00,
                'debito' => !empty($movimento->debito) ? $movimento->debito : 0.00,
                'saldo' => !empty($movimento->saldo) ? $movimento->saldo : 0.00,
                'ordem' => $aux,
                'complemento' => $movimento->complemento,
                'bancomovimento' => $bancomovimento
            ];
        }

        switch ($this->request->getData('relatorio')) {
            case 1:
                $nomeRelatorio = 'Relatório Movimentação Geral';
                $mrt = 'financeiro_movimentacao_geral.mrt';
                break;
            case 2:
                $nomeRelatorio = 'Relatório Movimentações';
                $mrt = 'financeiro_movimentacoes.mrt';
                break;
            default:
                $nomeRelatorio = '';
                $mrt = '';
                break;
        }

        $json['alias'][] = [
            'inicio' => !empty($this->request->getData('inicio')) ? $this->request->getData('inicio') : date('d/m/Y'),
            'fim' => !empty($this->request->getData('fim')) ? $this->request->getData('fim') : date('d/m/Y'),
            'nome_relatorio' => $nomeRelatorio,
            'saldo_inicial' => !empty($saldoInicial) ? $saldoInicial : 0.00
        ];

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
        $this->Json->create('financeiro_movimentacao.json', json_encode($json));
        $this->set('report', $mrt);
        $this->render('report');
    }

    public function financeiroMovimentosGeral()
    {
        $this->viewBuilder()->setLayout('report_2016');
        $this->loadModel('FinMovimentos');

        $movimentos = $this->FinMovimentos->find()->contain(['FinBancoMovimentos', 'FinPlanoContas' => ['FinClassificacaoContas'], 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes']);
        $movimentos = $this->Movimentos->filterMovesGeneral($this->request->getData(), $movimentos)[0];

        switch ($this->request->getData('relatorio')) {
            case 1:
                $nomeRelatorio = 'Relatório Geral';
                $mrt = 'financeiro_movimentacaogeral_geral.mrt';
                break;
            case 2:
                $nomeRelatorio = 'Demonstrativo Resultado - Sintético';
                $mrt = 'financeiro_movimentacaogeral_sintetico.mrt';
                $movimentos->andWhere(['FinClassificacaoContas.considera' => 1]);
                break;
            case 3:
                $nomeRelatorio = 'Demonstrativo Resultado - Analítico';
                $mrt = 'financeiro_movimentacaogeral_analitico.mrt';
                $movimentos->andWhere(['FinClassificacaoContas.considera' => 1]);
                break;
            case 4:
                $nomeRelatorio = 'Demonstrativo Resultado - Analítico e %Participação';
                $mrt = 'financeiro_movimentacaogeral_analitico_participacao.mrt';
                $movimentos->andWhere(['FinClassificacaoContas.considera' => 1]);
                break;
            case 5:
                $nomeRelatorio = 'Demonstrativo Resultado - Sintético e %Participação';
                $mrt = 'financeiro_movimentacaogeral_sintetico_participacao.mrt';
                $movimentos->andWhere(['FinClassificacaoContas.considera' => 1]);
                break;
            default:
                $nomeRelatorio = '';
                $mrt = '';
                break;
        }

        //Clonando o objeto de movimento para pegar os totais gerais
        $movimentosParaSaldos = clone($movimentos);
        $totalDebito = $movimentosParaSaldos->select(['total_debito' => 'SUM(FinMovimentos.debito)'])->first();
        $totalDebito = $totalDebito->total_debito;

        $totalCredito = $movimentosParaSaldos->select(['total_credito' => 'SUM(FinMovimentos.credito)'])->first();
        $totalCredito = $totalCredito->total_credito;

        //Clonando o objeto de movimento para pegar os totais separados por grupo
        $movimentosParaTotalGrupo = clone($movimentos);
        $totalPorGrupoQuery = $movimentosParaTotalGrupo->select([
            'id' => 'FinContabilidades.id',
            'total_grupo_credito' => 'SUM(FinMovimentos.credito)',
            'total_grupo_debito' => 'SUM(FinMovimentos.debito)'])
            ->group(['FinContabilidades.id']);

        foreach ($totalPorGrupoQuery as $totalGrupo) {
            $totalPorGrupoCredito[$totalGrupo->id] = !empty($totalGrupo->total_grupo_credito) ? $totalGrupo->total_grupo_credito : 0;
            $totalPorGrupoDebito[$totalGrupo->id] = !empty($totalGrupo->total_grupo_debito) ? $totalGrupo->total_grupo_debito : 0;
        }

        $json['dados'] = null;
        $aux = 0;

        foreach ($movimentos as $movimento) {
            $aux++;
            $credor_cliente = '';
            $bancomovimento = '';
            $classificacao = '';
            $credito = 0;
            $debito = 0;
            $totalGeralParticipacaoCredito = 0;
            $totalGeralParticipacaoDebito = 0;
            $totalGrupoParticipacaoCredito = 0;
            $totalGrupoParticipacaoDebito = 0;

            //Verificação para definir o credor se vai ser um fornecedor ou um cliente
            if ($movimento->has('fin_fornecedore')) {
                $credor_cliente = $movimento->fin_fornecedore->nome;
            } else if ($movimento->has('cliente')) {
                $credor_cliente = $movimento->cliente->nome;
            }

            //Verificação para definir o nome inteiro do banco-movimento
            if (!empty($movimento->fin_banco->nome) && !empty($movimento->fin_banco_movimento->data_movimento)) {
                $bancomovimento = $movimento->fin_banco->nome . ' - ' . $movimento->fin_banco_movimento->data_movimento;
            }

            //Verificação para definir o tipo de classificacao da conta
            if (!empty($movimento->fin_plano_conta->fin_classificacao_conta->tipo)) {
                if ($movimento->fin_plano_conta->fin_classificacao_conta->tipo == 1) {
                    $classificacao = 'Entrada';
                } else {
                    $classificacao = 'Saída';
                }
            }

            if (!empty($movimento->credito)) {
                $credito = $movimento->credito;
            }
            if (!empty($movimento->debito)) {
                $debito = $movimento->debito;
            }

            //Condições para calcular a participação em ralação ao total
            if ($totalCredito > 0) {
                $totalGeralParticipacaoCredito = $credito / $totalCredito;
            }
            if ($totalDebito > 0) {
                $totalGeralParticipacaoDebito = $debito / $totalDebito;
            }

            //Condições para calcular a participação em ralação ao total de cada grupo
            if (!empty($totalPorGrupoCredito[$movimento->fin_contabilidade_id]) && $totalPorGrupoCredito[$movimento->fin_contabilidade_id] > 0) {
                $totalGrupoParticipacaoCredito = $credito / $totalPorGrupoCredito[$movimento->fin_contabilidade_id];
            }
            if (!empty($totalPorGrupoDebito[$movimento->fin_contabilidade_id]) && $totalPorGrupoDebito[$movimento->fin_contabilidade_id] > 0) {
                $totalGrupoParticipacaoDebito = $debito / $totalPorGrupoDebito[$movimento->fin_contabilidade_id];
            }


            $json['dados'][] = [
                'id' => $movimento->id,
                'data' => !empty($movimento->data) ? $this->Data->data_sms($movimento->data) : '',
                'credorcliente' => $credor_cliente,
                'planocontas' => !empty($movimento->fin_plano_conta->nome) ? $movimento->fin_plano_conta->nome : '',
                'empresacontabilidade' => !empty($movimento->fin_contabilidade->nome) ? $movimento->fin_contabilidade->nome : '',
                'documento' => !empty($movimento->documento) ? $movimento->documento : '',
                'credito' => $credito,
                'debito' => $debito,
                'saldo' => !empty($movimento->saldo) ? $movimento->saldo : 0.00,
                'ordem' => $aux,
                'complemento' => $movimento->complemento,
                'bancomovimento' => $bancomovimento,
                'banco' => !empty($movimento->fin_banco->nome) ? $movimento->fin_banco->nome : '',
                'movimento' => !empty($movimento->fin_banco_movimento->data_movimento) ? $movimento->fin_banco_movimento->data_movimento : '',
                'tipo_conta_classificacao' => $classificacao,
                'conta_classificacao' => !empty($movimento->fin_plano_conta->fin_classificacao_conta->descricao) ? $movimento->fin_plano_conta->fin_classificacao_conta->descricao : '',
                'total_geral_partic_credito' => $totalGeralParticipacaoCredito,
                'total_geral_partic_debito' => $totalGeralParticipacaoDebito,
                'total_grupo_partic_credito' => $totalGrupoParticipacaoCredito,
                'total_grupo_partic_debito' => $totalGrupoParticipacaoDebito
            ];
        }

        $json['alias'][] = [
            'inicio' => !empty($this->request->getData('inicio')) ? $this->request->getData('inicio') : date('d/m/Y'),
            'fim' => !empty($this->request->getData('fim')) ? $this->request->getData('fim') : date('d/m/Y'),
            'nome_relatorio' => $nomeRelatorio,
            'saldo_inicial' => !empty($saldoInicial) ? $saldoInicial : 0.00,
            'total_credito' => $totalCredito,
            'total_debito' => $totalDebito
        ];

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
        $this->Json->create('financeiro_movimentacao.json', json_encode($json));
        $this->set('report', $mrt);
        $this->render('report');
    }

    /**
     * Método que monta os relatórios da tela de Contas a Pagar
     * Relatórios: Relatório Geral - financeiro_contaspagar_geral.mrt
     * Relatório por Vencimento - financeiro_contaspagar_por_vencimento.mrt
     */
    public function financeiroContasPagar()
    {
        $this->viewBuilder()->setLayout('report_2016');
        $contasPagarTable = TableRegistry::get('FinContasPagar');
        $contas = $contasPagarTable->find('all')
            ->contain(['FinFornecedores', 'FinPlanoContas', 'FinContabilidades']);

        $contas = $this->ContaPagar->filtroContasPagar($contas, $this->request->getData());

        $json['dados'] = null;
        $aux = 0;
        foreach ($contas as $conta) {
            $aux++;
            $json['dados'][] = [
                'vencimento' => $this->Data->data_sms($conta->vencimento),
                'credor' => $conta->fin_fornecedore->nome,
                'planocontas' => $conta->fin_plano_conta->nome,
                'numdocumento' => $conta->numero_documento,
                'empresacontabilidade' => $conta->fin_contabilidade->nome,
                'observacao' => $conta->complemento,
                'valor' => $conta->valor,
                'datapagamento' => $conta->data_pagamento,
                'ordem' => $aux
            ];
        }

        switch ($this->request->getData('relatorio')) {
            case 1:
                $nomeRelatorio = 'Relatório Contas a Pagar Geral';
                $mrt = 'financeiro_contaspagar_geral.mrt';
                break;
            case 2:
                $nomeRelatorio = 'Relatório Contas a Pagar por Vencimento';
                $mrt = 'financeiro_contaspagar_por_vencimento.mrt';
                break;
            default:
                $nomeRelatorio = '';
                $mrt = '';
                break;
        }

        $json['alias'][] = [
            'inicio' => $this->request->getData('inicio'),
            'fim' => $this->request->getData('fim'),
            'nome_relatorio' => $nomeRelatorio
        ];

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
        $this->Json->create('financeiro_contaspagar.json', json_encode($json));
        $this->set('report', $mrt);
        $this->render('report');
    }


    public function faturamentoXml()
    {
        $this->loadModel('Atendimentos');
        $atendimentos = $this->Atendimentos->getAtendimentos($this->request->getData());

        foreach ($atendimentos as $atendimento) {
            $convenio = $atendimento->convenio;
        }

        $randomNumber = str_pad(rand(0, 99999), 5, '0', STR_PAD_LEFT);
        $xml = '<?xml version="1.0" encoding="ISO-8859-1" ?>';
        switch ($this->request->getData('xml')) {
            case 1: //SDAT - 3.03.01
                $xml .= $this->Xml->getCabecalho30301($convenio, $randomNumber);
                $xml .= $this->Xml->getPrestadorParaOperadora30302($atendimentos, $convenio, $randomNumber);
                $xml .= $this->Xml->getEpilogo();
                break;
            case 2: //SDAT - 3.03.02
                $xml .= $this->Xml->getCabecalho30302($convenio, $randomNumber);
                $xml .= $this->Xml->getPrestadorParaOperadora30302($atendimentos, $convenio, $randomNumber);
                $xml .= $this->Xml->getEpilogo();
                break;
            case 3: //SDAT - 3.02.00
                $xml .= $this->Xml->getCabecalho30200($convenio, $randomNumber);
                $xml .= $this->Xml->getPrestadorParaOperadora30200($atendimentos, $convenio, $randomNumber);
                $xml .= $this->Xml->getEpilogo();
                break;
            case 4: //SDAT - 3.03.03
                $xml .= $this->Xml->getCabecalho30303($convenio, $randomNumber);
                $xml .= $this->Xml->getPrestadorParaOperadora30302($atendimentos, $convenio, $randomNumber);
                $xml .= $this->Xml->getEpilogo();
                break;
        }

        $nomeArquivo = date('YmdHis') . '_' . $randomNumber . '.xml';
        $caminho = WWW_ROOT . 'files/xml/faturamento/' . $nomeArquivo;
        $file = new File($caminho);
        $file->create();
        $file->write($xml);
        $file->close();
        $this->response->type('json');
        $this->response->body(json_encode($nomeArquivo));

        $this->autoRender = false;

    }

    public function baixarXml($nome)
    {
        $this->response->file(
            WWW_ROOT . 'files/xml/faturamento/' . $nome,
            ['download' => true, 'name' => $nome]
        );

        return $this->response;
    }

    /**
     * This function is responsible for creating a json for the billing conference report
     * Esta função é responsável por criar um json para o relatório de conferência de faturamento
     * @author Iohan
     */
    public function conferenciaFaturamento()
    {
        $this->loadModel('Convenios');
        $this->loadModel('AtendimentoProcedimentoEquipes');
        $this->viewBuilder()->setLayout('report_2016');
        $medicos = empty($this->request->getData(['medicos'])) ? null : $this->request->getData(['medicos']);
        $convenios = empty($this->request->getData(['convenios'])) ? null : $this->request->getData(['convenios']);
        $this->loadModel('AtendimentoProcedimentos');
        $atendimento_procedimentos = $this->AtendimentoProcedimentos->getAtendimentoProcedimento($this->request->getData());
        $json['atendimentos'] = null;
        $inicio = empty($this->request->data['data_inicio']) ? null : $this->request->data['data_inicio'];
        $fim = empty($this->request->data['data_fim']) ? null : $this->request->data['data_fim'];

        if (!empty($this->request->getData('ordenar_por'))) {
            $atendimento_procedimentos->order($this->request->getData('ordenar_por') . ' ASC');
        }

        $convenios = $this->Convenios->find('all')
            ->where(['Convenios.id IN' => $convenios]);
        $conveniosConcat = '';
        foreach ($convenios as $convenio) {
            $conveniosConcat .= $convenio->nome . '; ';
        }
        $aux = 1;
        foreach ($atendimento_procedimentos as $pa) {
            $totalProfissional = $this->Faturamento->calculaTotalProfissionalPorAtendimentoProcedimento($pa->id);
            //O campo ordem serve de parametro para o mrt ordenar o relatório
            $json['atendimentos'][] = [
                'atendimento' => $pa->atendimento->id,
                'senha' => !empty($pa->autorizacao_senha) ? $pa->autorizacao_senha : '',
                'grupo_procedimento' => $pa->procedimento->grupo_procedimento->nome,
                'codigo_procedimento' => $pa->procedimento->id,
                'data' => $this->Data->data_sms($pa->atendimento->data),
                'paciente' => $pa->atendimento->cliente->nome,
                'convenio' => $pa->atendimento->convenio->nome,
                'quantidade' => $pa->quantidade,
                'procedimento' => $pa->procedimento->nome,
                'executante' => $pa->medico_responsavei->nome,
                'usuario' => $pa->atendimento->user->nome,
                'codigo' => empty($pa->codigo) ? '' : $pa->codigo,
                'valor_caixa' => $pa->valor_caixa,
                'valor_unitario' => $pa->valor_fatura,
                'valor_total' => $pa->total,
                'valor_medicamento' => $pa->valor_material,
                'valor_equipe' => $totalProfissional,
                'valor_total_com_medicamento' => ($pa->total + $pa->valor_material),
                'guia' => $pa->nr_guia,
                'percentual_cobranca' => !empty($pa->percentual_cobranca) ? $pa->percentual_cobranca : 0,
                'ordenacao' => !empty($pa->ordenacao) ? $pa->ordenacao : 0,
                'ordem' => $aux,
                'carater_atendimento' => !empty($pa->atendimento->internacao_carater_atendimento->descricao) ? $pa->atendimento->internacao_carater_atendimento->descricao : ''
            ];
            $aux++;
        }

        $json['alias'][] = [
            'convenios' => !empty($conveniosConcat) ? $conveniosConcat : 'Não Informado',
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim
        ];

        $this->Json->create('conferencia_faturamento.json', json_encode($json));

        switch ($this->request->getData(['relatorio'])) {
            case 1 :
                $this->set('report', 'pre_faturamento_profissional.mrt');
                break;
            case 2 :
                $this->Json->create('conferencia_faturamento2.json', json_encode($json)); //Isso foi feito pois o aplicativo de report não estava reconhecendo novos campos no json conferencia_faturamento.
                $this->set('report', 'pre_faturamento_convenio.mrt');
                break;
            case 3:
                $this->Json->create('conferencia_faturamento2.json', json_encode($json)); //Isso foi feito pois o aplicativo de report não estava reconhecendo novos campos no json conferencia_faturamento.
                $this->set('report', 'pre_faturamento_procedimento.mrt');
                break;
            case 4 :
                $this->set('report', 'faturamento_excel.mrt');
                break;
            case 5 :
                $this->Json->create('conferencia_faturamento2.json', json_encode($json)); //Isso foi feito pois o aplicativo de report não estava reconhecendo novos campos no json conferencia_faturamento.
                $this->set('report', 'pre_faturamento_grupo_procedimento.mrt');
                break;
            case 6 :
                $this->set('report', 'pre_faturamento_paciente_com_valor_caixa.mrt');
                break;
            case 7 :
                $this->set('report', 'pre_faturamento_impcg.mrt');
                break;
            case 8 :
                $this->set('report', 'pre_faturamento_fusexs.mrt');
                break;
            case 9 :
                $this->set('report', 'pre_faturamento_base_area_cg.mrt');
                break;
            case 10 :
                $this->set('report', 'pre_faturamento_unimed.mrt');
                break;
            case 11 :
                $this->set('report', 'pre_faturamento_paciente_sem_valor_caixa.mrt');
                break;
            case 12:
                $this->Json->create('conferencia_faturamento2.json', json_encode($json)); //Isso foi feito pois o aplicativo de report não estava reconhecendo novos campos no json conferencia_faturamento.
                $this->set('report', 'pre_faturamento_convenio_com_valor_equipe.mrt');
                break;
            case 13:
                $this->Json->create('conferencia_faturamento2.json', json_encode($json)); //Isso foi feito pois o aplicativo de report não estava reconhecendo novos campos no json conferencia_faturamento.
                $this->set('report', 'pre_faturamento_paciente_com_valor_equipe.mrt');

                break;
        }
        $this->render('report');
    }

    /**
     * Esta função é responsável por criar um json para os relatórios de conferência de faturamento das equipes
     */
    public function conferenciaFaturamentoEquipes()
    {
        $this->loadModel('Convenios');
        $this->viewBuilder()->setLayout('report_2016');
        $convenios = empty($this->request->getData(['convenios'])) ? null : $this->request->getData(['convenios']);
        $atendimento_procedimentos_equipes = $this->Faturamento->filterAtendProcToEquipes($this->request->getData());

        $json['atendimentos'] = null;
        $inicio = empty($this->request->getData('data_inicio')) ? null : $this->request->getData('data_inicio');
        $fim = empty($this->request->getData('data_fim')) ? null : $this->request->getData('data_fim');

        if (!empty($this->request->getData('ordenar_por'))) {
            $atendimento_procedimentos_equipes->order($this->request->getData('ordenar_por') . ' ASC');
        }

        if (!empty($convenios)) {
            $convenios = $this->Convenios->find('all')
                ->where(['Convenios.id IN' => $convenios]);
            $conveniosConcat = '';
            foreach ($convenios as $convenio) {
                $conveniosConcat .= $convenio->nome . '; ';
            }
        } else {
            $conveniosConcat = 'Todos';
        }

        $aux = 1;
        foreach ($atendimento_procedimentos_equipes as $ape) {
            $totalProfissional = $this->Faturamento->calculaTotalProfissional($ape->id);
            $precoInstrumentador = $this->Convenios->getPrecoInstrumento($ape->atendimento_procedimento->procedimento->id, $ape->atendimento_procedimento->atendimento->convenio->id);

            $json['atendimentos'][] = [
                'atendimento' => $ape->atendimento_procedimento->atendimento->id,
                'senha' => !empty($ape->atendimento_procedimento->autorizacao_senha) ? $ape->atendimento_procedimento->autorizacao_senha : '',
                'grupo_procedimento' => $ape->atendimento_procedimento->procedimento->grupo_procedimento->nome,
                'codigo_procedimento' => $ape->atendimento_procedimento->procedimento->id,
                'data' => $this->Data->data_sms($ape->atendimento_procedimento->atendimento->data),
                'paciente' => $ape->atendimento_procedimento->atendimento->cliente->nome,
                'convenio' => $ape->atendimento_procedimento->atendimento->convenio->nome,
                'quantidade' => $ape->atendimento_procedimento->quantidade,
                'procedimento' => $ape->atendimento_procedimento->procedimento->nome,
                'executante' => $ape->medico_responsavei->nome,
                'usuario' => $ape->atendimento_procedimento->atendimento->user->nome,
                'codigo' => empty($ape->atendimento_procedimento->codigo) ? '' : $ape->atendimento_procedimento->codigo,
                'valor_caixa' => $ape->atendimento_procedimento->valor_caixa,
                'valor_unitario' => $ape->atendimento_procedimento->valor_fatura,
                'valor_total' => $ape->atendimento_procedimento->total,
                'valor_medicamento' => $ape->atendimento_procedimento->valor_material,
                'valor_total_com_medicamento' => ($ape->atendimento_procedimento->total + $ape->atendimento_procedimento->valor_material),
                'valor_total_profissional' => $totalProfissional,
                'guia' => $ape->atendimento_procedimento->nr_guia,
                'percentual_cobranca' => !empty($ape->atendimento_procedimento->percentual_cobranca) ? ($ape->atendimento_procedimento->percentual_cobranca) : 0,
                'porcentagem' => $ape->porcentagem,
                'ordenacao' => !empty($ape->atendimento_procedimento->ordenacao) ? $ape->atendimento_procedimento->ordenacao : 0,
                'tipo_equipe' => $ape->tipo_equipe->nome,
                'grau_equipe' => $ape->tipo_equipegrau->descricao,
                'preco_instrumentador' => $precoInstrumentador,
                'ordem' => $aux,
                'carater_atendimento' => !empty($ape->atendimento_procedimento->atendimento->internacao_carater_atendimento->descricao) ? $ape->atendimento_procedimento->atendimento->internacao_carater_atendimento->descricao : ''
            ];

            $aux++;
        }

        $json['alias'][] = [
            'convenios' => !empty($conveniosConcat) ? $conveniosConcat : 'Não Informado',
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim
        ];

        $this->Json->create('equipe_conferencia_faturamento.json', json_encode($json));

        switch ($this->request->getData(['relatorio'])) {
            case 1 :
                $this->set('report', 'equipe_pre_faturamento_profissional.mrt');
                break;
            case 2 :
                $this->set('report', 'equipe_pre_faturamento_convenio.mrt');
                break;
            case 3:
                $this->set('report', 'equipe_pre_faturamento_tipo_instrumentador.mrt');
                break;
            case 4 :
                $this->set('report', 'equipe_faturamento_excel.mrt');
                break;
            case 6 :
                $this->set('report', 'equipe_pre_faturamento_paciente_com_valor_caixa.mrt');
                break;
            case 11 :
                $this->set('report', 'equipe_pre_faturamento_paciente_sem_valor_caixa.mrt');
                break;
        }
        $this->render('report');
    }

    public function conferencia()
    {
        $this->loadModel('Convenios');
        $convenios = $this->Convenios->find('all')->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $this->set('convenios', $convenios);
    }

    public function reportProdutividade()
    {
        $this->viewBuilder()->setLayout('report_2016');

        $this->loadModel('TempProdutividadeprofissionais');
        $produtividade = $this->TempProdutividadeprofissionais->find('all');

        $json = [
            'inicio' => $this->request->getQuery('data_inicio'),
            'fim' => $this->request->getQuery('data_fim')
        ];
        $this->Json->create('produtividade.json', json_encode($produtividade));

        $this->Json->create('produtividade_aux.json', json_encode($json));
        switch ($this->request->getQuery('relatorio')) {
            case 1:
                $this->set('report', 'produtividade_executante_simples.mrt');
                break;
            case 2:
                $this->set('report', 'produtividade.mrt');
                break;
            case 3 :
                $this->set('report', 'produtividade_solicitante.mrt');
                break;
            case 4:
                $this->set('report', 'produtividade_excel.mrt');
                break;
            case 5:
                $this->set('report', 'simplificado_grupo_procedimento.mrt');
                break;
            case 6:
                $this->set('report', 'produtividade_executante_sintetico.mrt');
                break;
        }
    }

    public function produtividade()
    {
    }

    public function analitico()
    {

    }

    public function executante()
    {
        $this->loadModel('ConfiguracaoPeriodos');
        $periodos = $this->ConfiguracaoPeriodos->find('list');
        $tiposRelatorios = $this->Relatorios->getTipoRelatorioFechamentoCaixa();
        $this->set(compact('periodos', 'tiposRelatorios'));
    }

    public function simplificado()
    {

    }

    public function produtividadeSolicitante()
    {

        if ($this->request->is('post')) {
            $this->viewBuilder()->setLayout('report_2016');
            $json = $this->Relatorios->produtividade_solicitante();
            $this->Json->create('produtividade_solicitante.json', json_encode($json));
            $this->set('report', 'produtividade_solicitante.mrt');
            $this->render('report_produtividade');
        }
    }

    /**
     * 1ª Documentação - 05/06/2020, por Fernanda Taso
     * Função que gera o relatório para Orcamentos de Procedimentos por convenio
     * Utilizado em: Atendimentos/ Orçamento/Vl de Procedimentos
     *
     */
    public function relatorioOrcamentoVlProcedimentos()
    {
        $this->loadModel('Users');
        $this->viewBuilder()->setLayout('report_2016');
        $this->loadModel('PrecoProcedimentos');
        $this->loadModel('Configuracoes');
        $user = $this->Auth->user();

        $procedimentos = $this->PrecoProcedimentos->find('all')
            ->contain(['Convenios', 'Procedimentos'])
            ->where(['Procedimentos.situacao_id' => 1]);

        if (!empty($this->request->data['convenio'])) {
            $procedimentos->andWhere(['Convenios.id IN' => $this->request->data['convenio']]);
        }
        if (!empty($this->request->data['procedimentos'])) {
            $procedimentos->andWhere(['Procedimentos.id IN' => $this->request->data['procedimentos']]);
        }

        $nome = !empty($this->request->data['nome']) ? $this->request->data['nome'] : null;
        $observacoes = !empty($this->request->data['observacoes']) ? $this->request->data['observacoes'] : null;


        $json['dados'] = null;
        foreach ($procedimentos as $proc) {

            $json['dados'][] = [
                'procecimento_id' => $proc->procedimento_id,
                'convenio_id' => $proc->convenio_id,
                'procedimento_nome' => $proc->procedimento->nome,
                'convenio_nome' => $proc->convenio->nome,
                'valor_faturar' => $proc->valor_faturar,
                'valor_particular' => $proc->valor_particular,
            ];
        }


        $json['alias'][] = [
            'nome' => $nome,
            'observacoes' => $observacoes,
            'user_logado' => $user['nome'],
        ];

        $configuracao = $this->Configuracoes->find('all');

        $json['dados_cliente'][] = [
            'nome' =>  $configuracao->toArray()
        ];
        $this->Json->create('orcamento_vl_procedimento.json', json_encode($json));

        $this->set('report', 'atendimento_orcamento_vl_procedimento.mrt');
        $this->render('report');
    }

}