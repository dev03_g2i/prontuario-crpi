<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

/**
 * FaturaMatmed Controller
 *
 * @property \App\Model\Table\FaturaMatmedTable $FaturaMatmed
 */
class FaturaMatmedController extends AppController
{

    public $components = array('Atendimento', 'Data', 'Query', 'Matmed');

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id)
    {

        $query = $this->FaturaMatmed->find('all')
            ->contain(['EstqArtigos' => ['FaturaPrecoartigo']])
            ->where(['FaturaMatmed.situacao_id' => 1])
            ->andWhere(['FaturaMatmed.atendimento_id' => $id]);

        if(!empty($this->request->query('atendimento_procedimento_id'))){
            $query->andWhere(['FaturaMatmed.atendimento_procedimento_id' => $this->request->query('atendimento_procedimento_id')]);
        }
        if(!empty($this->request->query('data'))){
            $date = $this->Data->DataSQL($this->request->query('data'));
            $query->andWhere(['FaturaMatmed.data' => $date]);
        }

        $dadosPaciente = $this->FaturaMatmed->AtendimentoProcedimentos->find('all')
                ->contain(['Atendimentos' => ['Convenios', 'Clientes']])
                ->where(['AtendimentoProcedimentos.atendimento_id' => $id])->first();

        $atendProcedimentos  = $this->Atendimento->procedimentos($id);
        $faturaMatmed = $this->paginate($query);

        $faturaKit = $this->FaturaMatmed->EstqArtigos->FaturaKitartigo->FaturaKit->find('list')->where(['FaturaKit.situacao_id' => 1])->orderAsc('FaturaKit.nome');

        $this->set(compact('faturaMatmed', 'procedimentos', 'dadosPaciente', 'atendProcedimentos', 'faturaKit'));
        $this->set('_serialize', ['faturaMatmed']);

    }

    public function importarPrescricao($atendimento_id)
    {
        $this->loadModel('EstqSaida');
        $estq_saida = $this->EstqSaida->find('all')
                        ->where(['EstqSaida.atendimento_id' => $atendimento_id])
                        ->andWhere(['EstqSaida.situacao_id' => 1])->toArray();
        $data = [];
        foreach($estq_saida as $es){
            $data[] = $es->toArray();
        }
        $faturaMatmed = $this->FaturaMatmed->newEntities($data);
        $i = 0;
        $error = 0;
        foreach($faturaMatmed as $fm){
            $fm->estq_saida_id = $data[$i]['id'];
            if($this->FaturaMatmed->save($fm)){
                $error = 0;
            }else{
                $error++;
            }
            $i++;
        }
        if($error == 0){
            $this->Flash->success(__('Prescrição importada com sucesso!'));
            return $this->redirect(['action' => 'index', $atendimento_id]);
        }else{
            $this->Flash->error(__('Falha ao tentar importar prescrição. Por favor, tente novamente.'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Fatura Matmed id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaMatmed = $this->FaturaMatmed->get($id, [
            'contain' => ['Artigos', 'AtendimentoProcedimentos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('faturaMatmed', $faturaMatmed);
        $this->set('_serialize', ['faturaMatmed']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faturaMatmed = $this->FaturaMatmed->newEntity();
        if ($this->request->is('post')) {
            $faturaMatmed = $this->FaturaMatmed->patchEntity($faturaMatmed, $this->request->data);
            if ($this->FaturaMatmed->save($faturaMatmed)) {
                $this->Flash->success(__('O fatura matmed foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura matmed não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaMatmed'));
        $this->set('_serialize', ['faturaMatmed']);
    }

    public function addArtigo()
    {
        $dados = null;
        if(!empty($this->request->query('atendProc_id'))){
            $dados = $this->FaturaMatmed->AtendimentoProcedimentos->get($this->request->query('atendProc_id'), [
                'contain' => ['Procedimentos', 'Atendimentos' => ['Clientes']]
            ]);
        }

        $artigos = $this->FaturaMatmed->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('artigos', 'dados'));
    }

    public function newArtigo()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $faturaMatmed = $this->FaturaMatmed->newEntity();
        if ($this->request->is('post')) {
            $faturaMatmed = $this->FaturaMatmed->patchEntity($faturaMatmed, $this->request->data);
            $dados = $this->Matmed->precoArtigo($this->request->data('artigo_id'));
            $faturaMatmed->codigo_tiss = ($dados)? $dados->codigo : null;
            $faturaMatmed->vl_venda = ($dados) ? $dados->valor : null;
            $faturaMatmed->total = ($dados) ? $this->request->data('quantidade') * $dados->valor : '';
            $faturaMatmed->codigo_convenio = ($dados) ? $dados->codigo : null;
            $artigo = $this->FaturaMatmed->EstqArtigos->get($this->request->getData('artigo_id'));
            $faturaMatmed->codigo_tiss = $artigo->codigo_tiss;
            $faturaMatmed->codigo_tuss = $artigo->codigo_tuss;
            if ($this->FaturaMatmed->save($faturaMatmed)) {
                // Atualiza o campo valor material da tabela atendimento_procedimento
                $this->Matmed->atualizaValorMaterial($faturaMatmed->atendimento_procedimento_id);

                $dados = $this->FaturaMatmed->get($faturaMatmed->id, ['contain' => ['EstqArtigos']]);
                $date = new Time($dados->data);
                $newDate = $date->format('d/m/Y');
                $res = ['id' => $dados->id, 'data' => $newDate, 'codigo' => $dados->estq_artigo->codigo_livre, 'artigo' => $dados->estq_artigo->nome, 'qtd' => $dados->quantidade];
            }else {
                $res = 'error';
            }
        }

        $this->response->body(json_encode($res));
    }

    public function receberTodos()
    {
        $atendimento_id  = $this->request->query('atendimento_id');
        if($this->request->is('post')){
            $dateRec = (!empty($this->request->data('dt_recebimento'))) ? $this->Data->DataSQL($this->request->data('dt_recebimento')) : null;
            $conn = ConnectionManager::get('default');
            $receberTodos = $this->Query->matmedReceberTodos($dateRec, $atendimento_id, $conn);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Matmed id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaMatmed = $this->FaturaMatmed->get($id, [
            'contain' => ['AtendimentoProcedimentos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaMatmed = $this->FaturaMatmed->patchEntity($faturaMatmed, $this->request->data);
            if ($this->FaturaMatmed->save($faturaMatmed)) {
                $this->Flash->success(__('O fatura matmed foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index', $faturaMatmed->atendimento_procedimento->atendimento_id]);
            } else {
                $this->Flash->error(__('O fatura matmed não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaMatmed'));
        $this->set('_serialize', ['faturaMatmed']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Matmed id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {

        $faturaMatmed = $this->FaturaMatmed->get($id);
        $faturaMatmed->situacao_id = 2;
        if ($this->FaturaMatmed->save($faturaMatmed)) {
            $this->Flash->success(__('O fatura matmed foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura matmed não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function deleteModal()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $faturaMatmed = $this->FaturaMatmed->get($this->request->data['id']);
        $faturaMatmed->situacao_id = 2;
        if ($this->FaturaMatmed->save($faturaMatmed)) {
            $res = 0;
        } else {
            $res = 1;
        }

        $json = json_encode(['res' => $res]);
        $this->response->body($json);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Fatura Matmed id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->FaturaMatmed->find('all')
            ->where(['FaturaMatmed.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaMatmed.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Fatura Matmed id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $faturaMatmed = $this->FaturaMatmed->get($this->request->data['id']);
            $res = ['nome'=>$faturaMatmed->nome,'id'=>$faturaMatmed->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function filterTotais(){
        $this->autoRender=false;
        $this->response->type('json');

        $query = $this->FaturaMatmed->find()
                        ->contain(['EstqArtigos' => ['FaturaPrecoartigo']])
                        ->where(['FaturaMatmed.situacao_id' => 1])
                        ->andWhere(['FaturaMatmed.atendimento_id' => $this->request->data('atendimento_id')]);

        $query_select = $query->select([
            'total' => $query->func()->sum('FaturaMatmed.total')
        ])->toArray();

        $res = ['total' => ($query_select[0]->total)?$query_select[0]->total: 0];
        $this->response->body(json_encode($res));
    }

    public function addKitMatmed(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = [];

        $atendProc = $this->FaturaMatmed->AtendimentoProcedimentos->get($this->request->data('atend_proc_id'), [
            'contain' => ['Atendimentos']
        ]);
        $faturaKitartigo = $this->FaturaMatmed->EstqArtigos->FaturaKitartigo->find('all')->where(['FaturaKitartigo.fatura_kit_id' => $this->request->data('fatura_kit_id')]);
        $countFaturaKitartigo = $faturaKitartigo->count();
        if($countFaturaKitartigo > 0){
            foreach ($faturaKitartigo as $fka) {
                $dados = $this->Matmed->precoArtigo($fka->artigo_id);

                $faturaMatmed = $this->FaturaMatmed->newEntity();
                //$date = new Time($atendProc->atendimento->data);
                $faturaMatmed->data = $atendProc->atendimento->data;
                $faturaMatmed->artigo_id = $fka->artigo_id;
                $faturaMatmed->quantidade = $fka->quantidade;
                $faturaMatmed->atendimento_procedimento_id = $atendProc->id;
                $faturaMatmed->atendimento_id = $atendProc->atendimento_id;
                $faturaMatmed->vl_venda = $dados->valor;
                $faturaMatmed->total = $fka->quantidade * $dados->valor;
                $faturaMatmed->codigo_convenio = ($dados) ? $dados->codigo : null;
                $artigo = $this->FaturaMatmed->EstqArtigos->get($fka->artigo_id);
                $faturaMatmed->codigo_tiss = $artigo->codigo_tiss;
                $faturaMatmed->codigo_tuss = $artigo->codigo_tuss;

                if($this->FaturaMatmed->save($faturaMatmed)){
                    $res = ['res' => 0, 'msg' => 'O kit/Pacote foi adicionado com sucesso!'];
                }else {
                    $res = ['res' => 1, 'msg' => 'Não foi possivel adicionar o Kit/Pacote, por favor tente mais tarde!'];
                }
            }
            $this->Matmed->atualizaValorMaterial($atendProc->id);
        }else {
            $res = ['res' => 1, 'msg' => 'Não foi encontrado artigos cadastrados para este Kit/Pacote!'];
        }

        $this->response->body(json_encode($res));
    }

    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name']) && !empty($this->request->data['value'])){
            $name = $this->request->data['name'];
            $faturaMatmed = $this->FaturaMatmed->get($this->request->data['pk']);
            $faturaMatmed->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->FaturaMatmed->save($faturaMatmed)) {
                $res = ['res' => $faturaMatmed->$name,'msg'=>'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }
}
