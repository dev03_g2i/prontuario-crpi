<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 17/08/2017
 * Time: 16:18
 */

namespace App\Controller;


use Cake\I18n\Time;

class EstatisticasController extends AppController
{
    public $components = array('Estatistica', 'Data');

    public function index()
    {   
        $page = 'estatistica';
        $tiposEstatisticas = $this->Estatistica->getTipoEstatisticas($page);
        $anos = $this->Estatistica->getAnos();
        $convenios = $this->Estatistica->getConvenios();
        $gruposFiltro = $this->Estatistica->getGrupos('list');
        $unidades = $this->Estatistica->getUnidades();

        if ($this->request->is('post')) {
            if (!empty($this->request->data['ano']) && !empty($this->request->data['tipo_estatistica'])) {
                $ano = $this->request->data['ano'];
                $tipo_estatistica = $this->request->data['tipo_estatistica'];
                $tipo_resultado = $this->request->data['tipo_resultado'];
                $unidades_filtro = $this->request->data['unidade'];
                (!empty($this->request->data['convenio']) ? $convenio_filtro = $this->request->data['convenio'] : $convenio_filtro = null);
                (!empty($this->request->data['grupo_procedimento']) ? $grupos_filtro = $this->request->data['grupo_procedimento'] : $grupos_filtro = null);
                
                if ($tipo_estatistica == 1) {
                    //GrupoProcedimento x Procedimento
                    return $this->estatisticaUm($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 2) {
                    //GrupoProcedimento x Convenio
                    return $this->estatisticaDois($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 3) {
                    //GrupoProcedimento
                    return $this->estatisticaTres($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 4) {
                    //GrupoProcedimento x Origens
                    return $this->estatisticaQuatro($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 5) {
                    //GrupoProcedimento x Unidades
                    return $this->estatisticaCinco($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 6) {
                    //GrupoProcedimento x Solicitantes
                    return $this->estatisticaSeis($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 7) {
                    //GrupoProcedimento x Solicitantes
                    return $this->estatisticaSete($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                }
            }
        }

        $this->set(compact('tiposEstatisticas', 'anos', 'convenios', 'gruposFiltro', 'unidades'));
        $this->set('_serialize', ['estatisticas']);
    }

    public function rh()
    {   
        $page = 'rh';
        $tiposEstatisticas = $this->Estatistica->getTipoEstatisticas($page);
        $anos = $this->Estatistica->getAnos();
        $convenios = $this->Estatistica->getConvenios();
        $gruposFiltro = $this->Estatistica->getGrupos('list');
        $unidades = $this->Estatistica->getUnidades();

        if ($this->request->is('post')) {
            if (!empty($this->request->data['ano']) && !empty($this->request->data['tipo_estatistica'])) {
                $ano = $this->request->data['ano'];
                $tipo_estatistica = $this->request->data['tipo_estatistica'];
                $tipo_resultado = $this->request->data['tipo_resultado'];
                $unidades_filtro = $this->request->data['unidade'];
                (!empty($this->request->data['convenio']) ? $convenio_filtro = $this->request->data['convenio'] : $convenio_filtro = null);
                (!empty($this->request->data['grupo_procedimento']) ? $grupos_filtro = $this->request->data['grupo_procedimento'] : $grupos_filtro = null);
                
                if ($tipo_estatistica == 8) {
                    //Atendimentos x Procedimentos
                    return $this->estatisticaOito($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 9) {
                    //Atendimento x Procedimentos x Grupo
                    return $this->estatisticaNove($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 10) {
                    //Agendas
                    return $this->estatisticaDez($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 11) {
                    //Agendas x Agenda
                    return $this->estatisticaOnze($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 12) {
                    //Laudos digitados
                    return $this->estatisticaDoze($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 13) {
                    //Laudos digitados x Grupo
                    return $this->estatisticaTreze($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 14) {
                    //Laudos assinados
                    return $this->estatisticaCatorze($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } else if ($tipo_estatistica == 15) {
                    //Laudos assinados x Grupo
                    return $this->estatisticaQuinze($ano, $tipo_resultado, $unidades_filtro, $convenio_filtro, $grupos_filtro);
                } 
            }
        }

        $this->set(compact('tiposEstatisticas', 'anos', 'convenios', 'gruposFiltro', 'unidades'));
        $this->set('_serialize', ['estatisticas']);
    }

    //Grupo Procedimento x Procedimentos
    public function estatisticaUm($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProcProcs';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getAtendProcPorGrupoProc($g->id);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $querySelect = $query->select([
                'nome' => 'Procedimentos.nome'
            ]);

            if ($tipo_resultado == 1) {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
                ]);
                $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            } else {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
                ]);
                $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
            }
            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeCabecalho = 'Procedimentos';
        //Método para definir o nome da tabela
        $nomeTabela = $this->Estatistica->nomeTabela('Grupo Procedimento x Procedimento', $ano, $tipo_resultado);

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Grupo Procedimento x Convênios
    public function estatisticaDois($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProcConv';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getGrupoProcPorConvenio($g->id);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'Convenios.nome'
            ]);

            if ($tipo_resultado == 1) {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
                ]);
                $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            } else {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
                ]);
                $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
            }

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        //Método para definir o nome da tabela
        $nomeTabela = $this->Estatistica->nomeTabela('Grupo Procedimento x Convênios', $ano, $tipo_resultado);
        $nomeCabecalho = 'Convênios';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Grupo Procedimento
    public function estatisticaTres($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProc';
        $query = $this->Estatistica->getGrupoProc();
        $query = $this->Estatistica->filtroAnos($ano, $query, $type);

        if (!empty($gruposProcedimentos)) {
            $query = $this->Estatistica->filtroGrupos($gruposProcedimentos, $query);
        }

        if (!empty($unidades)) {
            $query = $this->Estatistica->filtroUnidades($unidades, $query);
        }

        if (!empty($convenios)) {
            $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
        }

        $query->select([
            'nome' => 'GrupoProcedimentos.nome'
        ]);

        if ($tipo_resultado == 1) {
            $querySelect = $query->select([
                'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
            ]);
            $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
        } else {
            $querySelect = $query->select([
                'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
            ]);
            $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
        }

        $tabelas[] = $querySelect;
        $nomesGrupos[] = 'Grupos de Procedimentos';


        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Grupo Procedimento', $ano, $tipo_resultado);
        $nomeCabecalho = 'Grupos';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Grupo Procedimento x Origens
    public function estatisticaQuatro($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProcOrigens';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getGrupoProcPorOrigem($g->id);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'Origens.nome'
            ]);

            if ($tipo_resultado == 1) {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
                ]);
                $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            } else {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
                ]);
                $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
            }

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Grupo Procedimento x Origens', $ano, $tipo_resultado);
        $nomeCabecalho = 'Origens';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Grupo Procedimento x Unidade
    public function estatisticaCinco($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProcUnidade';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getGrupoProcPorUnidade($g->id);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'Unidades.nome'
            ]);

            if ($tipo_resultado == 1) {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
                ]);
                $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            } else {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
                ]);
                $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
            }

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Grupo Procedimento x Unidades', $ano, $tipo_resultado);
        $nomeCabecalho = 'Unidades';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Grupo Procedimento x Solicitantes
    public function estatisticaSeis($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProcSolicitantes';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getGrupoProcPorSolicitante($g->id);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'Solicitantes.nome'
            ]);

            if ($tipo_resultado == 1) {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
                ]);
                $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            } else {
                $querySelect = $query->select([
                    'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
                ]);
                $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
            }

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Grupo Procedimento x Solicitantes', $ano, $tipo_resultado);
        $nomeCabecalho = 'Solicitantes';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Solicitantes
    public function estatisticaSete($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'Solicitantes';
        $query = $this->Estatistica->getSolicitantes();
        $query = $this->Estatistica->filtroAnos($ano, $query, $type);

        if (!empty($gruposProcedimentos)) {
            $query = $this->Estatistica->filtroGrupos($gruposProcedimentos, $query);
        }

        if (!empty($unidades)) {
            $query = $this->Estatistica->filtroUnidades($unidades, $query);
        }

        if (!empty($convenios)) {
            $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
        }

        $query->select([
            'nome' => 'Solicitantes.nome'
        ]);

        if ($tipo_resultado == 1) {
            $querySelect = $query->select([
                'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')
            ])
                ->order(['total_procedimento DESC']);
            $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
        } else {
            $querySelect = $query->select([
                'total_procedimento' => $query->func()->sum('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')
            ])
                ->order(['total_procedimento DESC']);
            $querySelect = $this->Estatistica->totaisMesesValores($querySelect);
        }

        $tabelas[] = $querySelect;
        $nomesGrupos[] = 'Solicitantes Rank';


        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Solicitantes', $ano, $tipo_resultado);
        $nomeCabecalho = 'Solicitantes';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Registro geral
    public function estatisticaOito($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'AtdProc';
        $query = $this->Estatistica->getAtendProcs();
        $query = $this->Estatistica->filtroAnos($ano, $query, $type);

        if (!empty($gruposProcedimentos)) {
            $query = $this->Estatistica->filtroGrupos($gruposProcedimentos, $query);
        }

        if (!empty($unidades)) {
            $query = $this->Estatistica->filtroUnidades($unidades, $query);
        }

        if (!empty($convenios)) {
            $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
        }

        $query->select(['nome' => 'Users.nome']);

        $query = $query->select(['qtd_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')]);
        $query = $this->Estatistica->totaisMeses($query, $type);
 
        $newArray = [];

        foreach($query as $i => $q) {
            if (empty($newArray)) {
                $newArray[] = $q;
            } else {
                $size = count($newArray);
                $existsG = false;
                foreach($newArray as $j => $user) {
                    if ((($q->qtd_procedimento) > ($user->qtd_procedimento)) && !$existsG) {
                        $nestedArray = [];
                        $nestedArray[] = $q;
                        array_splice($newArray, $j, 0, $nestedArray);
                        $existsG = true;
                    } else if ($j == ($size-1) && !$existsG) {
                        $newArray[] = $q;
                    }
                }
            }
        }

        $tabelas[] = $newArray;
        $nomesGrupos[] = 'Geral';
        
        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $nomeTabela = $this->Estatistica->nomeTabela('Registro geral', $ano, $tipo_resultado);
        $nomeCabecalho = 'Atendentes';

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Registro x Grupo de Procedimento
    public function estatisticaNove($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'GrpProcSolicitantes';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getAtendProcsGrup($g->id);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'Users.nome'
            ]);

            $querySelect = $query->select(['qtd_procedimento' => $query->func()->sum('AtendimentoProcedimentos.quantidade')]);
            $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            
            $newArray = [];

            foreach($querySelect as $i => $q) {
                if (empty($newArray)) {
                    $newArray[] = $q;
                } else {
                    $size = count($newArray);
                    $existsG = false;
                    foreach($newArray as $j => $user) {
                        if ((($q->qtd_procedimento) > ($user->qtd_procedimento)) && !$existsG) {
                            $nestedArray = [];
                            $nestedArray[] = $q;
                            array_splice($newArray, $j, 0, $nestedArray);
                            $existsG = true;
                        } else if ($j == ($size-1) && !$existsG) {
                            $newArray[] = $q;
                        }
                    }
                }
            }

            $querySelect = $newArray;

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Registro x Grupo de Procedimento', $ano, $tipo_resultado);
        $nomeCabecalho = 'Atendentes';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Agendamentos
    public function estatisticaDez($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'Agendas';
        $query = $this->Estatistica->getAgendas();
        $query = $this->Estatistica->filtroAnos($ano, $query, $type);

        if (!empty($convenios)) {
            $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
        }
        
        $query->select([
            'nome' => 'Users.nome'
        ]);

        $query = $query->select(['qtd_agendamentos' => $query->func()->count('Agendas.id')]);
        $query = $this->Estatistica->totaisMeses($query, $type);
        
        $newArray = [];

        foreach($query as $i => $q) {
            if (empty($newArray)) {
                $newArray[] = $q;
            } else {
                $size = count($newArray);
                $existsG = false;
                foreach($newArray as $j => $user) {
                    if ((($q->qtd_agendamentos) > ($user->qtd_agendamentos)) && !$existsG) {
                        $nestedArray = [];
                        $nestedArray[] = $q;
                        array_splice($newArray, $j, 0, $nestedArray);
                        $existsG = true;
                    } else if ($j == ($size-1) && !$existsG) {
                        $newArray[] = $q;
                    }
                }
            }
        }

        $tabelas[] = $newArray;
        $nomesGrupos[] = 'Agendamentos';

        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $totalSomas = $this->Estatistica->totalSomaMes($somas);
        $nomeTabela = $this->Estatistica->nomeTabela('Agendamentos', $ano, $tipo_resultado);
        $nomeCabecalho = 'Agendadores';

        //Método para pegar o total das somas dos meses

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_dois');
    }

    //Agendamentos x Agenda
    public function estatisticaOnze($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'Agendas';

        $grupos = $this->Estatistica->getGrupoAgend('all');

        foreach ($grupos as $g) {
            $query = $this->Estatistica->getAgendasGrup($g->id);
            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }
            
            $query->select([
                'nome' => 'Users.nome'
            ]);

            $query = $query->select(['qtd_agendamentos' => $query->func()->count('Agendas.id')]);
            $query = $this->Estatistica->totaisMeses($query, $type);
            
            $newArray = [];

            foreach($query as $i => $q) {
                if (empty($newArray)) {
                    $newArray[] = $q;
                } else {
                    $size = count($newArray);
                    $existsG = false;
                    foreach($newArray as $j => $user) {
                        if ((($q->qtd_agendamentos) > ($user->qtd_agendamentos)) && !$existsG) {
                            $nestedArray = [];
                            $nestedArray[] = $q;
                            array_splice($newArray, $j, 0, $nestedArray);
                            $existsG = true;
                        } else if ($j == ($size-1) && !$existsG) {
                            $newArray[] = $q;
                        }
                    }
                }
            }

            $tabelas[$g->nome] = $newArray;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $totalSomas = $this->Estatistica->totalSomaMes($somas);
        $nomeTabela = $this->Estatistica->nomeTabela('Agendamentos x Agenda', $ano, $tipo_resultado);
        $nomeCabecalho = 'Agendadores';

        //Método para pegar o total das somas dos meses

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_dois');
    }

    //Laudos digitados
    public function estatisticaDoze($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'LaudosDigitados';

        $query = $this->Estatistica->getLaudos($type);
        $query = $this->Estatistica->filtroAnos($ano, $query, $type);

        if (!empty($gruposProcedimentos)) {
            $query = $this->Estatistica->filtroGrupos($gruposProcedimentos, $query);
        }

        if (!empty($unidades)) {
            $query = $this->Estatistica->filtroUnidades($unidades, $query);
        }

        if (!empty($convenios)) {
            $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
        }

        $query->select([
            'nome' => 'Users.nome'
        ]);

        $query = $query->select(['qtd_laudos' => $query->func()->count('Laudos.id')]);
        $query = $this->Estatistica->totaisMeses($query, $type);
        
        $newArray = [];

        foreach($query as $i => $q) {
            if (empty($newArray)) {
                $newArray[] = $q;
            } else {
                $size = count($newArray);
                $existsG = false;
                foreach($newArray as $j => $user) {
                    if ((($q->qtd_laudos) > ($user->qtd_laudos)) && !$existsG) {
                        $nestedArray = [];
                        $nestedArray[] = $q;
                        array_splice($newArray, $j, 0, $nestedArray);
                        $existsG = true;
                    } else if ($j == ($size-1) && !$existsG) {
                        $newArray[] = $q;
                    }
                }
            }
        }

        $tabelas[] = $query;
        $nomesGrupos[] = 'Laudos';

        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Laudos Digitados', $ano, $tipo_resultado);
        $nomeCabecalho = 'Agendadores';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Laudos digitados x Grupo
    public function estatisticaTreze($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {
        $type = 'GrpLaudoDigitado';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getLaudoForGroup($g->id, $type);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'Users.nome'
            ]);

            $querySelect = $query->select(['qtd_laudos' => $query->func()->count('Laudos.id')]);
            $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            
            $newArray = [];

            foreach($querySelect as $i => $q) {
                if (empty($newArray)) {
                    $newArray[] = $q;
                } else {
                    $size = count($newArray);
                    $existsG = false;
                    foreach($newArray as $j => $user) {
                        if ((($q->qtd_laudos) > ($user->qtd_laudos)) && !$existsG) {
                            $nestedArray = [];
                            $nestedArray[] = $q;
                            array_splice($newArray, $j, 0, $nestedArray);
                            $existsG = true;
                        } else if ($j == ($size-1) && !$existsG) {
                            $newArray[] = $q;
                        }
                    }
                }
            }

            $querySelect = $newArray;

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t){
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            } else {
                $medias[] = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Laudos digitados x Grupo', $ano, $tipo_resultado);
        $nomeCabecalho = 'Atendentes';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Laudos assinados
    public function estatisticaCatorze($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {   
        $type = 'LaudosAssinados';

        $query = $this->Estatistica->getLaudos($type);
        $query = $this->Estatistica->filtroAnos($ano, $query, $type);

        if (!empty($gruposProcedimentos)) {
            $query = $this->Estatistica->filtroGrupos($gruposProcedimentos, $query);
        }

        if (!empty($unidades)) {
            $query = $this->Estatistica->filtroUnidades($unidades, $query);
        }

        if (!empty($convenios)) {
            $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
        }

        $query->select([
            'nome' => 'UsersAssinado.nome'
        ]);

        $querySelect = $query->select(['qtd_laudos' => $query->func()->count('Laudos.id')]);
        $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);

        foreach($querySelect as $i => $q) {
            if (empty($newArray)) {
                $newArray[] = $q;
            } else {
                $size = count($newArray);
                $existsG = false;
                foreach($newArray as $j => $user) {
                    if ((($q->qtd_laudos) > ($user->qtd_laudos)) && !$existsG) {
                        $nestedArray = [];
                        $nestedArray[] = $q;
                        array_splice($newArray, $j, 0, $nestedArray);
                        $existsG = true;
                    } else if ($j == ($size-1) && !$existsG) {
                        $newArray[] = $q;
                    }
                }
            }
        }

        $tabelas[] = $newArray;
        $nomesGrupos[] = 'Laudos';

        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t) {
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Laudos Assinados', $ano, $tipo_resultado);
        $nomeCabecalho = 'Assinado por';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Laudos assinados x Grupo
    public function estatisticaQuinze($ano, $tipo_resultado, $unidades, $convenios, $gruposProcedimentos)
    {
        $type = 'GrpLaudoAssinados';
        $grupos = $this->Estatistica->getGrupos('all');
        if (!empty($gruposProcedimentos)) {
            $grupos = $this->Estatistica->filtroGrupos($gruposProcedimentos, $grupos);
        }

        //Esse foreach é para fazer a tabelas separadas por grupo procedimentos, e no final cada tabela de um grupo é salva em um array.
        foreach ($grupos as $g) {
            $query = $this->Estatistica->getLaudoForGroup($g->id, $type);

            $query = $this->Estatistica->filtroAnos($ano, $query, $type);

            if (!empty($unidades)) {
                $query = $this->Estatistica->filtroUnidades($unidades, $query);
            }

            if (!empty($convenios)) {
                $query = $this->Estatistica->filtroConvenios($convenios, $query, $type);
            }

            $query->select([
                'nome' => 'UsersAssinado.nome'
            ]);

            $querySelect = $query->select(['qtd_laudos' => $query->func()->count('Laudos.id')]);
            $querySelect = $this->Estatistica->totaisMeses($querySelect, $type);
            
            $newArray = [];

            foreach($querySelect as $i => $q) {
                if (empty($newArray)) {
                    $newArray[] = $q;
                } else {
                    $size = count($newArray);
                    $existsG = false;
                    foreach($newArray as $j => $user) {
                        if ((($q->qtd_laudos) > ($user->qtd_laudos)) && !$existsG) {
                            $nestedArray = [];
                            $nestedArray[] = $q;
                            array_splice($newArray, $j, 0, $nestedArray);
                            $existsG = true;
                        } else if ($j == ($size-1) && !$existsG) {
                            $newArray[] = $q;
                        }
                    }
                }
            }

            $querySelect = $newArray;

            $tabelas[$g->nome] = $querySelect;
            $nomesGrupos[] = $g->nome;
        }

        //Cálculo das médias de cada procedimento e também das somas dos meses
        $total = 0;
        foreach ($tabelas as $t){
            $somaMeses = $this->Estatistica->somaMes($t);
            $somas[] = $somaMeses;
            $somaMeses = null;
            foreach ($t as $q) {
                $mediasProcedimentos[] = $this->Estatistica->mediaProcedimento($q);
            }
            if (!empty($mediasProcedimentos)) {
                $medias[] = $mediasProcedimentos;
                $mediasProcedimentos = null;
            } else {
                $medias[] = null;
            }
        }

        $nomeTabela = $this->Estatistica->nomeTabela('Laudos Assinados x Grupo', $ano, $tipo_resultado);
        $nomeCabecalho = 'Assinados por';

        //Método para pegar o total das somas dos meses
        $totalSomas = $this->Estatistica->totalSomaMes($somas);

        $this->set(compact('tabelas', 'nomesGrupos', 'medias', 'somas', 'nomeTabela', 'totalSomas', 'nomeCabecalho'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('estatistica_um');
    }

    //Método para montar os gráficos Atendimentos do Mês atual e dos últimos três meses
    public function graficoAtendimento()
    {
        $this->autoRender = false;

        $this->loadModel('GrupoProcedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $grupoProcedimentos = $this->GrupoProcedimentos->find('all');

        $grupoProcedimento = [];
        $atendimentosMesatual = [];
        $atendimentosMes1 = [];
        $atendimentosMes2 = [];
        $atendimentosMes3 = [];

        $now = Time::now();
        $mes1 = $now->format('m');
        $mes2 = $now->subMonth(1)->format('m');
        $mes3 = $now->subMonth(1)->format('m');
        $mes_atual = date('m');
        foreach ($grupoProcedimentos as $gp) {

            $grupoProcedimento[] = $gp->nome;

            $queryMesAtual = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes_atual,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryMes1 = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes1,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryMes2 = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes2,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryMes3 = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes3,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            foreach ($queryMesAtual as $am) {
                $atendimentosMesatual[] = $am->quantidade;
            }
            foreach ($queryMes1 as $q1) {
                $atendimentosMes1[] = $q1->quantidade;
            }
            foreach ($queryMes2 as $q2) {
                $atendimentosMes2[] = $q2->quantidade;
            }
            foreach ($queryMes3 as $q3) {
                $atendimentosMes3[] = $q3->quantidade;
            }

        }

        $this->response->type('json');
        $json = json_encode(['grupo' => $grupoProcedimento, 'mesAtual' => $atendimentosMesatual,
            'mes1' => $atendimentosMes1, 'mes2' => $atendimentosMes2, 'mes3' => $atendimentosMes3,
            'meses' => $this->Data->ultimosMeses()]);
        $this->response->body($json);
    }

    //Método para montar o gráfico de caixa convenio dia a dia
    public function graficoCaixaConvenio()
    {
        $this->autoRender = false;
        $this->loadModel('AtendimentoProcedimentos');
        $mes_atual = date('m');

        $ultimo_dia_mes = $this->Data->ultimoDiaMes($mes_atual);

        for ($i = 1; $i <= $ultimo_dia_mes; $i++) {
            $queryValorCaixa = $this->AtendimentoProcedimentos->find('all')
                ->select(['valor_caixa' => 'sum(AtendimentoProcedimentos.valor_caixa)'])
                ->contain(['Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes_atual,
                    'DAY(Atendimentos.data)' => $i
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryValorTotal = $this->AtendimentoProcedimentos->find('all')
                ->select(['total' => 'sum(AtendimentoProcedimentos.total)'])
                ->contain(['Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes_atual,
                    'DAY(Atendimentos.data)' => $i
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            foreach ($queryValorCaixa as $qvc) {
                if (!$qvc->valor_caixa) {
                    $valoresCaixa[] = 0;
                } else {
                    $valoresCaixa[] = $qvc->valor_caixa;
                }
            }
            foreach ($queryValorTotal as $qvt) {
                if (!$qvt->total) {
                    $valoresTotal[] = 0;
                } else {
                    $valoresTotal[] = $qvt->total;
                }
            }
            $dias[] = $i;
        }

        $label[] = [0 => 'Valor Caixa', 1 => 'Valor Faturado'];
        $this->response->type('json');
        $json = json_encode(['labels' => $label, 'valoresCaixa' => $valoresCaixa, 'valoresTotal' => $valoresTotal, 'dias' => $dias]);
        $this->response->body($json);
    }

    //Método para montar o gráfico de caixa convenio dos últimos três meses
    public function graficoCaixaConveniosUltimos()
    {
        $this->autoRender = false;
        $this->loadModel('AtendimentoProcedimentos');

        $now = Time::now();
        $mes[3] = $now->format('m');
        $mes[2] = $now->subMonth(1)->format('m');
        $mes[1] = $now->subMonth(1)->format('m');

        for ($i = 1; $i <= 3; $i++) {
            $queryValorCaixa = $this->AtendimentoProcedimentos->find('all')
                ->select(['valor_caixa' => 'sum(AtendimentoProcedimentos.valor_caixa)'])
                ->contain(['Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes[$i],
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryValortotal = $this->AtendimentoProcedimentos->find('all')
                ->select(['total' => 'sum(AtendimentoProcedimentos.total)'])
                ->contain(['Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes[$i],
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);


            foreach ($queryValorCaixa as $q1) {
                if (!$q1->valor_caixa) {
                    $valoresCaixa[] = 0;
                } else {
                    $valoresCaixa[] = $q1->valor_caixa;
                }

            }

            foreach ($queryValortotal as $q2) {
                if (!$q2->total) {
                    $valoresTotal[] = 0;
                } else {
                    $valoresTotal[] = $q2->total;
                }
            }

        }

        $label[] = [0 => 'Valor Caixa', 1 => 'Valor Faturado'];
        $this->response->type('json');
        $json = json_encode(['valoresCaixa' => $valoresCaixa, 'valoresTotal' => $valoresTotal,
            'meses' => $this->Data->ultimosMeses(), 'labels' => $label]);
        $this->response->body($json);
    }

    public function graficosFinanceiro()
    {   
        $this->render('graficos_financeiro_geral');  
    }

    public function graficosFinanceiroDiario()
    {   
        $meses = [
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        ];
    
        $anos = [];

        $mesReferencia = '';
        $anoReferencia = '';
    
        for ($i = -3; $i <= 3; $i++) {
            $anos[date('Y') + ($i)] = date('Y') + ($i);
        }

        if (!empty($this->request->query('mes'))) {
            $mesReferencia = $this->request->query('mes');
            $mesReferencia = $meses[$mesReferencia];
        }

        if (!empty($this->request->query('ano'))) {
            $anoReferencia = $this->request->query('ano');
            $anoReferencia = $anos[$anoReferencia];
        }

        $date = new Time();
        $formatedDateMes = intval($date->format('m'));
        $formatedDateAno = $date->format('Y');

        $this->set(compact('meses', 'anos', 'mesReferencia', 'anoReferencia', 'formatedDateMes', 'formatedDateAno'));
        $this->set('_serialize', ['estatisticas']);
        $this->render('graficos_financeiro_diario');
    }
    
    //Método para montar o gráfico de caixa convenio dia a dia 
    // do gráfico financeiro diário
    public function graficoCaixaConvenioDiario($date = null)
    {   
        $date = strtotime($date);
        $selectedMonth = date('m', $date);
        $selectedYear = date('Y', $date);

        $this->autoRender = false;
        $this->loadModel('AtendimentoProcedimentos');

        $ultimo_dia_mes = $this->Data->ultimoDiaMes($selectedMonth);

        //For dentro dos dias do mês
        for ($selectedDay = 1; $selectedDay <= $ultimo_dia_mes; $selectedDay++) {
            $queryValorCaixa = $this->AtendimentoProcedimentos->find('all')
                ->select(['valor_caixa' => 'sum(AtendimentoProcedimentos.valor_caixa)'])
                ->contain(['Atendimentos'])
                ->where([
                    'YEAR(Atendimentos.data)' => $selectedYear,
                    'MONTH(Atendimentos.data)' => $selectedMonth,
                    'DAY(Atendimentos.data)' => $selectedDay
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryValorTotal = $this->AtendimentoProcedimentos->find('all')
                ->select(['total' => 'sum(AtendimentoProcedimentos.total)'])
                ->contain(['Atendimentos'])
                ->where([
                    'YEAR(Atendimentos.data)' => $selectedYear,
                    'MONTH(Atendimentos.data)' => $selectedMonth,
                    'DAY(Atendimentos.data)' => $selectedDay
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            foreach ($queryValorCaixa as $qvc) {
                if (!$qvc->valor_caixa) {
                    $valoresCaixa[] = 0;
                } else {
                    $valoresCaixa[] = $qvc->valor_caixa;
                }
            }
            foreach ($queryValorTotal as $qvc) {
                if (!$qvc->total) {
                    $valoresTotal[] = 0;
                } else {
                    $valoresTotal[] = $qvc->total;
                }
            }
            $dias[] = $selectedDay;
        }

        $label[] = [0 => 'Valor Caixa', 1 => 'Valor Faturado'];
        $this->response->type('json');
        $json = json_encode(['labels' => $label, 'valoresCaixa' => $valoresCaixa, 'valoresTotal' => $valoresTotal, 'dias' => $dias, 'mes' => $this->Data->mesAtual($date)]);
        $this->response->body($json);
    }

    //Método para montar os gráficos Atendimentos do Mês atual e dos últimos três meses
    public function graficoAtendimentoDiario($date = null)
    {   
        $date = strtotime($date);
        $date = date('Y-m-d', $date);
        
        $this->autoRender = false;

        $this->loadModel('GrupoProcedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $grupoProcedimentos = $this->GrupoProcedimentos->find('all');

        $grupoProcedimento = [];
        $atendimentosMes1 = [];
        $atendimentosMes2 = [];
        $atendimentosMes3 = [];

        $formatedDate = new Time($date);
        $mes1 = $formatedDate->format('m');
        $mes2 = $formatedDate->subMonth(1)->format('m');
        $mes3 = $formatedDate->subMonth(1)->format('m');
        foreach ($grupoProcedimentos as $gp) {

            $grupoProcedimento[] = $gp->nome;

            $queryMes1 = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes1,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryMes2 = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes2,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryMes3 = $this->AtendimentoProcedimentos->find('all')
                ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                ->contain(['Procedimentos', 'Atendimentos'])
                ->where([
                    'MONTH(Atendimentos.data)' => $mes3,
                    'Procedimentos.grupo_id' => $gp->id
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['Procedimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            foreach ($queryMes1 as $q1) {
                $atendimentosMes1[] = $q1->quantidade;
            }
            foreach ($queryMes2 as $q2) {
                $atendimentosMes2[] = $q2->quantidade;
            }
            foreach ($queryMes3 as $q3) {
                $atendimentosMes3[] = $q3->quantidade;
            }

        }

        $this->response->type('json');
        $json = json_encode(['grupo' => $grupoProcedimento,
            'mes1' => $atendimentosMes1, 'mes2' => $atendimentosMes2, 'mes3' => $atendimentosMes3,
            'meses' => $this->Data->ultimosMeses($date)]);
        $this->response->body($json);
    }

    //Método para montar o gráfico de caixa convenio dos ultimos 30 dias
    public function graficoCaixaConveniosUltimos30Dias($date = null)
    {   
        $date = strtotime($date);
        $date = date('Y-m-d', $date);

        $this->autoRender = false;
        $this->loadModel('AtendimentoProcedimentos');
        $formatedDate = new Time($date);
        
        for($i = 3; $i >= 1; $i--) {
            $mes[$i] = new \StdClass();
            if ($i == 3) {
                $mes[$i]->year = $formatedDate->format('Y');
                $mes[$i]->month = $formatedDate->format('m');
            } else {
                $mes[$i]->year = $formatedDate->subMonth(1)->format('Y');
                $mes[$i]->month = $formatedDate->subMonth(1)->format('m');
            }
        }

        for ($i = 1; $i <= 3; $i++) {
            $queryValorCaixa = $this->AtendimentoProcedimentos->find('all')
                ->select(['valor_caixa' => 'sum(AtendimentoProcedimentos.valor_caixa)'])
                ->contain(['Atendimentos'])
                ->where([
                    'YEAR(Atendimentos.data)' => $mes[$i]->year,
                    'MONTH(Atendimentos.data)' => $mes[$i]->month,
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $queryValortotal = $this->AtendimentoProcedimentos->find('all')
                ->select(['total' => 'sum(AtendimentoProcedimentos.total)'])
                ->contain(['Atendimentos'])
                ->where([
                    'YEAR(Atendimentos.data)' => $mes[$i]->year,
                    'MONTH(Atendimentos.data)' => $mes[$i]->month,
                ])
                ->andWhere(['Atendimentos.situacao_id' => 1])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);


            foreach ($queryValorCaixa as $q1) {
                if (!$q1->valor_caixa) {
                    $valoresCaixa[] = 0;
                } else {
                    $valoresCaixa[] = $q1->valor_caixa;
                }

            }

            foreach ($queryValortotal as $q2) {
                if (!$q2->total) {
                    $valoresTotal[] = 0;
                } else {
                    $valoresTotal[] = $q2->total;
                }
            }

        }

        $label[] = [0 => 'Valor Caixa', 1 => 'Valor Faturado'];
        $this->response->type('json');
        $json = json_encode(['valoresCaixa' => $valoresCaixa, 'valoresTotal' => $valoresTotal,
            'meses' => $this->Data->ultimosMeses($date), 'labels' => $label]);
        $this->response->body($json);
    }

    public function graficoQuantidadeAtendimentosMeses($date = null)
    {   
        $date = strtotime($date);
        $selectedMonth = date('m', $date);
        $selectedYear = date('Y', $date);

        $this->autoRender = false;
        $this->loadModel('GrupoProcedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $grupoProcedimentos = $this->GrupoProcedimentos->find('all');
        $ultimo_dia_mes = $this->Data->ultimoDiaMes($selectedMonth);

        //For dentro dos dias do mês
        for ($selectedDay = 1; $selectedDay <= $ultimo_dia_mes; $selectedDay++) {
            foreach ($grupoProcedimentos as $index => $gp) {
                if ($selectedDay == 1) {
                    $grupoProcedimento[$index] = new \StdClass();
                    $grupoProcedimento[$index]->nome = $gp->nome;
                    $grupoProcedimento[$index]->color = $gp->color;
                }

                $qtdAtendimentos = $this->AtendimentoProcedimentos->find('all')
                    ->select(['qtd' => 'count(AtendimentoProcedimentos.id)'])
                    ->contain(['Atendimentos', 'Procedimentos'])
                    ->where([
                        'YEAR(Atendimentos.data)' => $selectedYear,
                        'MONTH(Atendimentos.data)' => $selectedMonth,
                        'DAY(Atendimentos.data)' => $selectedDay,
                        'Procedimentos.grupo_id' => $gp->id
                    ])
                    ->andWhere(['Atendimentos.situacao_id' => 1])
                    ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

                foreach ($qtdAtendimentos as $qvc) {
                    if (!$qvc->qtd) {
                        $qtdAtendimento[$index][] = 0;
                    } else {
                        $qtdAtendimento[$index][] = $qvc->qtd;
                    }
                }
            }

            $dias[] = $selectedDay;
        }
        
        $this->response->type('json');
        $json = json_encode(['labels' => $grupoProcedimento, 'qtdAtendimento' => $qtdAtendimento, 'dias' => $dias, 'mes' => $this->Data->mesAtual($date)]);
        $this->response->body($json);
    }
}