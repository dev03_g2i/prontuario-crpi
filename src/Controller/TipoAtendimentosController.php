<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoAtendimentos Controller
 *
 * @property \App\Model\Table\TipoAtendimentosTable $TipoAtendimentos
 */
class TipoAtendimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['TipoAtendimentos.situacao_id = ' => '1']
                    ];
        $tipoAtendimentos = $this->paginate($this->TipoAtendimentos);

        $this->set(compact('tipoAtendimentos'));
        $this->set('_serialize', ['tipoAtendimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Atendimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoAtendimento = $this->TipoAtendimentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('tipoAtendimento', $tipoAtendimento);
        $this->set('_serialize', ['tipoAtendimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoAtendimento = $this->TipoAtendimentos->newEntity();
        if ($this->request->is('post')) {
            $tipoAtendimento = $this->TipoAtendimentos->patchEntity($tipoAtendimento, $this->request->data);
            if ($this->TipoAtendimentos->save($tipoAtendimento)) {
                $this->Flash->success(__('O tipo atendimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo atendimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->TipoAtendimentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->TipoAtendimentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('tipoAtendimento', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['tipoAtendimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Atendimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoAtendimento = $this->TipoAtendimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoAtendimento = $this->TipoAtendimentos->patchEntity($tipoAtendimento, $this->request->data);
            if ($this->TipoAtendimentos->save($tipoAtendimento)) {
                $this->Flash->success(__('O tipo atendimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo atendimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->TipoAtendimentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->TipoAtendimentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('tipoAtendimento', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['tipoAtendimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Atendimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoAtendimento = $this->TipoAtendimentos->get($id);
                $tipoAtendimento->situacao_id = 2;
        if ($this->TipoAtendimentos->save($tipoAtendimento)) {
            $this->Flash->success(__('O tipo atendimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo atendimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
