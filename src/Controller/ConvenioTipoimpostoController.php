<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConvenioTipoimposto Controller
 *
 * @property \App\Model\Table\ConvenioTipoimpostoTable $ConvenioTipoimposto
 */
class ConvenioTipoimpostoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $convenioTipoimposto = $this->paginate($this->ConvenioTipoimposto);

        $this->set(compact('convenioTipoimposto'));
        $this->set('_serialize', ['convenioTipoimposto']);
    }

    /**
     * View method
     *
     * @param string|null $id Convenio Tipoimposto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convenioTipoimposto = $this->ConvenioTipoimposto->get($id, [
            'contain' => ['Convenios']
        ]);

        $this->set('convenioTipoimposto', $convenioTipoimposto);
        $this->set('_serialize', ['convenioTipoimposto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convenioTipoimposto = $this->ConvenioTipoimposto->newEntity();
        $newConvenioTipoimposto = $this->request->data;
        $newConvenioTipoimposto['user_created'] = $this->Auth->user('id');

        if ($this->request->is('post')) {
            $convenioTipoimposto = $this->ConvenioTipoimposto->patchEntity($convenioTipoimposto, $newConvenioTipoimposto);
            if ($this->ConvenioTipoimposto->save($convenioTipoimposto)) {
                $this->Flash->success(__('O convenio tipoimposto foi salvo com sucesso!'));
                return $this->redirect(['controller' => 'Convenios', 'action' => 'index']);
            } else {
                $this->Flash->error(__('O convenio tipoimposto não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('convenioTipoimposto'));
        $this->set('_serialize', ['convenioTipoimposto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Convenio Tipoimposto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convenioTipoimposto = $this->ConvenioTipoimposto->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newConvenioTipoimposto = $this->request->data;
            $newConvenioTipoimposto['user_updated'] = $this->Auth->user('id');
            $convenioTipoimposto = $this->ConvenioTipoimposto->patchEntity($convenioTipoimposto, $this->request->data);
            if ($this->ConvenioTipoimposto->save($convenioTipoimposto)) {
                $this->Flash->success(__('O convenio tipoimposto foi salvo com sucesso.'));
                return $this->redirect(['controller' => 'Convenios', 'action' => 'index']);
            } else {
                $this->Flash->error(__('O convenio tipoimposto não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('convenioTipoimposto'));
        $this->set('_serialize', ['convenioTipoimposto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convenio Tipoimposto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $convenioTipoimposto = $this->ConvenioTipoimposto->get($id);
                $convenioTipoimposto->situacao_id = 2;
        if ($this->ConvenioTipoimposto->save($convenioTipoimposto)) {
            $this->Flash->success(__('O convenio tipoimposto foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O convenio tipoimposto não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Convenio Tipoimposto id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function findById($id) {
        $convenioTipoimposto = $this->ConvenioTipoimposto->get($id);
        $this->set(compact('convenioTipoimposto'));
        $this->set('_serialize', ['convenioTipoimposto']);
    }

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ConvenioTipoimposto->find('all')
        ->where(['ConvenioTipoimposto.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ConvenioTipoimposto.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Convenio Tipoimposto id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $convenioTipoimposto = $this->ConvenioTipoimposto->get($this->request->data['id']);
            $res = ['nome'=>$convenioTipoimposto->nome,'id'=>$convenioTipoimposto->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
