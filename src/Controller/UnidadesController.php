<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Unidades Controller
 *
 * @property \App\Model\Table\UnidadesTable $Unidades
 */
class UnidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['Unidades.situacao_id = ' => '1']
                    ];
        $unidades = $this->paginate($this->Unidades);

        $this->set(compact('unidades'));
        $this->set('_serialize', ['unidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Unidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $unidade = $this->Unidades->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'Atendimentos']
        ]);

        $this->set('unidade', $unidade);
        $this->set('_serialize', ['unidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $unidade = $this->Unidades->newEntity();
        if ($this->request->is('post')) {
            $unidade = $this->Unidades->patchEntity($unidade, $this->request->data);
            if ($this->Unidades->save($unidade)) {
                $this->Flash->success(__('O unidade foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O unidade não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Unidades->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Unidades->Users->find('list', ['limit' => 200]);
        $this->set(compact('unidade', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['unidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Unidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $unidade = $this->Unidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $unidade = $this->Unidades->patchEntity($unidade, $this->request->data);
            if ($this->Unidades->save($unidade)) {
                $this->Flash->success(__('O unidade foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O unidade não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Unidades->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Unidades->Users->find('list', ['limit' => 200]);
        $this->set(compact('unidade', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['unidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Unidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $unidade = $this->Unidades->get($id);
                $unidade->situacao_id = 2;
        if ($this->Unidades->save($unidade)) {
            $this->Flash->success(__('O unidade foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O unidade não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function fill()
    {   
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 20;

        $query = $this->Unidades->find('all')
        ->where(['Unidades.nome LIKE ' =>  $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Unidades.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Delete getedit
     *
     * @param string|null $id Unidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){

        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $unidade = $this->Unidades->get($this->request->data['id']);
            $res = ['nome'=>$unidade->nome,'id'=>$unidade->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
