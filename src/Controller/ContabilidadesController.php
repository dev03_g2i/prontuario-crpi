<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contabilidades Controller
 *
 * @property \App\Model\Table\ContabilidadesTable $Contabilidades
 */
class ContabilidadesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $contabilidades = $this->paginate($this->Contabilidades);


        $this->set(compact('contabilidades'));
        $this->set('_serialize', ['contabilidades']);

    }

    /**
     * View method
     *
     * @param string|null $id Contabilidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contabilidade = $this->Contabilidades->get($id, [
            'contain' => ['ContabilidadeBancos', 'Contaspagar', 'ContaspagarPlanejamentos', 'Contasreceber', 'ContasreceberPlanejamentos', 'GrupoContabilidades', 'Movimentos']
        ]);

        $this->set('contabilidade', $contabilidade);
        $this->set('_serialize', ['contabilidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contabilidade = $this->Contabilidades->newEntity();
        if ($this->request->is('post')) {
            $contabilidade = $this->Contabilidades->patchEntity($contabilidade, $this->request->data);
            if ($this->Contabilidades->save($contabilidade)) {
                $this->Flash->success(__('O contabilidade foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contabilidade não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('contabilidade'));
        $this->set('_serialize', ['contabilidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contabilidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contabilidade = $this->Contabilidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contabilidade = $this->Contabilidades->patchEntity($contabilidade, $this->request->data);
            if ($this->Contabilidades->save($contabilidade)) {
                $this->Flash->success(__('O contabilidade foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contabilidade não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('contabilidade'));
        $this->set('_serialize', ['contabilidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contabilidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $contabilidade = $this->Contabilidades->get($id);
                $contabilidade->situacao_id = 2;
        if ($this->Contabilidades->save($contabilidade)) {
            $this->Flash->success(__('O contabilidade foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O contabilidade não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Contabilidade id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Contabilidades->find('all')
        ->where(['Contabilidades.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Contabilidades.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Contabilidade id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $contabilidade = $this->Contabilidades->get($this->request->data['id']);
            $res = ['nome'=>$contabilidade->nome,'id'=>$contabilidade->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
