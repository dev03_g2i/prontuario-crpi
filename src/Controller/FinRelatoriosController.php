<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use Cake\VirtualProperties;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class FinRelatoriosController extends AppController
{
    //Componentes externos para serem usados.
    public $components = ['Date', 'Model'];

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Relatórios');
    }

    //Função para renderizar a dashboard index.
    public function mensal()
    {
        $inicio = !empty($this->request->query(['inicio'])) ? $this->Date->formatDateForSql($this->request->query(['inicio'])) : date('Y-m-01');
        $fim = !empty($this->request->query(['fim'])) ? $this->Date->formatDateForSql($this->request->query(['fim'])) : date('Y-m-t');
        $planocontas = !empty($this->request->query(['planocontas'])) ? $this->request->query(['planocontas']) : null;
        $contabilidades = !empty($this->request->query(['contabilidades'])) ? $this->request->query(['contabilidades']) : null;

        $rankings = [
            1 => 'Receitas',
            2 => 'Fornecedores',
            4 => 'Despesas fixas',
            5 => 'Despesas variáveis',
            6 => 'Diversos',
            7 => 'Impostos',
            11 => 'Funcionários',
            12 => 'Investimentos',
            13 => 'Médicos',
            15 => 'Despesas financeiras'
        ];

        $relatorioMensal = new \StdClass();
        $relatorioMensal->entradas = new \StdClass();
        $relatorioMensal->saidas = new \StdClass();
        
        //definindo objeto de entradas
        $entradas = new \StdClass();
        $entradas->totalEntradas = new \StdClass();
        $entradas->totalEntradas->credito = 0;
        $entradas->totalEntradas->debito = 0;
        $entradas->data = [];

        //definindo objeto de saidas
        $saidas = new \StdClass();
        $saidas->totalSaidas = new \StdClass();
        $saidas->totalSaidas->credito = 0;
        $saidas->totalSaidas->debito = 0;
        $saidas->data = [];

        //criação de uma table de entrada
        $entrada = new \StdClass();
        $entrada->credito = 0;
        $entrada->debito = 0;
        $entrada->data = [];

        //criação de uma table de saida
        $saida = new \StdClass();
        $saida->credito = 0;
        $saida->debito = 0;
        $saida->data = [];

            foreach($rankings as $rankingKey => $ranking)
            {
                $movimentos = $this->Model->getModelData('FinMovimentos', 'getMovesForPlan', $rankingKey);

                if (isset($inicio) && isset($fim)) {
                    $movimentos = $movimentos->andWhere(['FinMovimentos.data >=' => $inicio, 'FinMovimentos.data <=' => $fim]);
                } else if (isset($inicio)) {
                    $movimentos = $movimentos->andWhere(['FinMovimentos.data >=' => $inicio]);
                } else if (isset($fim)) {
                    $movimentos = $movimentos->andWhere(['FinMovimentos.data <=' => $fim]);
                }
        
                if (isset($planocontas)) {
                    $string = '';
                    $last_key = key(array_slice($planocontas, -1, 1, TRUE));
                    foreach($planocontas as $key => $planoconta)
                    {
                        if ($key === 0){
                            $string = '('.$planoconta;
                        } else {
                            $string = $string.$planoconta;
                        }
        
                        if ($key < $last_key){
                            $string = $string.', ';
                        } else {
                            $string = $string.')';
                        }
                    }
                    $movimentos = $movimentos->andWhere('(FinMovimentos.fin_plano_conta_id IN '.$string.')');
                }
        
                if (isset($contabilidades)){
                    $string = '';
                    $last_key = key(array_slice($contabilidades, -1, 1, TRUE));
                    foreach($contabilidades as $key => $contabilidade)
                    {
                        if ($key === 0){
                            $string = '('.$contabilidade;
                        } else {
                            $string = $string.$contabilidade;
                        }
        
                        if ($key < $last_key){
                            $string = $string.', ';
                        } else {
                            $string = $string.')';
                        }
                    }
                    $movimentos = $movimentos->andWhere('(FinMovimentos.fin_contabilidade_id IN '.$string.')');
                }

                if ($rankingKey === 1) {
                    $entrada->data = $movimentos;
                    $entrada->name = $ranking;
                    $entradas->data[] = clone $entrada;
                } else {
                    $saida->data = $movimentos;
                    $saida->name = $ranking;
                    $saidas->data[] = clone $saida;
                }
            }

            //atribuição de entradas e saidas
            $relatorioMensal->entradas = $entradas;
            $relatorioMensal->saidas = $saidas;

            //iteração para pegar os valores totalizados das receitas
            foreach($relatorioMensal->entradas->data as $key => $entrada)
            {
                $entrada->credito = 0;
                $entrada->debito = 0;
                foreach($entrada->data as $key => $receita)
                {
                    $entrada->credito += $receita->credito;
                    $entrada->debito += $receita->debito;
                }
                $relatorioMensal->entradas->totalEntradas->credito += $entrada->credito;
                $relatorioMensal->entradas->totalEntradas->debito += $entrada->debito;
            }

            //iteração para pegar os valores totalizados dos debitos
            foreach($relatorioMensal->saidas->data as $key => $saida)
            {
                $saida->credito = 0;
                $saida->debito = 0;
                foreach($saida->data as $key => $despesa)
                {
                    $saida->credito += $despesa->credito;
                    $saida->debito += $despesa->debito;
                }
                $relatorioMensal->saidas->totalSaidas->credito += $saida->credito;
                $relatorioMensal->saidas->totalSaidas->debito += $saida->debito;
            }

            $initialDate = date('01/m/Y');
            $finalDate = date('t/m/Y');

        $this->set(compact('relatorioMensal', 'totalEntradas', 'initialDate', 'finalDate'));
        $this->set('_serialize', ['relatorioMensal']);
    }
}