<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\I18n\Time;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DashboardFinanceiroController extends AppController
{
    //Componentes externos para serem usados.
    public $components = ['Date', 'Model', 'Dashboard'];

    //Função para renderizar a dashboard index.
    public function index()
    {
        $date = $this->request->query(['date']) ? $this->Date->formatDateForSql($this->request->query(['date'])) : null;
        $idCompany = $this->request->query(['idCompany']) ? $this->request->query(['idCompany']) : null;

        //Pegando dados do banco.
        $bankMoves = $this->Model->getModelData('FinBancoMovimentos', 'getAllOpenedMovesBank', $idCompany, $date);
        $groups = $this->Model->getModelData('FinGrupos', 'getAllGroups', $date);
        $movesAndTransfs = $this->Model->getModelData('FinMovimentos', 'movesOfThisMonth', $date);
        $moves = $movesAndTransfs[0];
        $transfs = $movesAndTransfs[1];
        $billsToPay = $this->Model->getModelData('FinContasPagar', 'getBillsToPay', $date);
        $billsToReceive = $this->Model->getModelData('FinContasReceber', 'getBillsToReceive', $date);
        $invoicesToReceive = $this->Model->getModelData('FaturaEncerramentos', 'getInvoicesToReceive', $date);
        
        //Lapidando os dados pegos no passo anterior.
        $resultMoves = $this->workWithMovesData($moves->toArray(), $transfs->toArray(), $date);

        $resultBankMoves = $this->workWithBankMovesData($bankMoves);
        $resultBills = $this->workWithBillsData($billsToPay->toArray(), $billsToReceive->toArray(), $invoicesToReceive->toArray(), $date, $resultMoves[0], $resultBankMoves[0]);

        $lastBankFinalBalance = $this->Model->getModelData('FinBancoMovimentos', 'getLastMonthFinalBalance', $date);
        $now = new Time();
        $dateField = $now->format('m/Y');

        $this->set(compact('page', 'subpage', 'bankMoves', 'moves', 'resultMoves', 'groups', 'resultBankMoves', 'resultBills', 'lastBankFinalBalance', 'dateField'));
        $this->set('_serialize', ['page', 'subpage', 'bankMoves', 'moves', 'resultMoves', 'groups', 'resultBankMoves', 'resultBills', 'lastBankFinalBalance']);
    }

    //Função para pegar dados do gráfico
    public function getGraphicData()
    {
        $date = $this->request->query(['date']) ? $this->Date->formatDateForSql($this->request->query(['date'])) : null;

        $billsToPay = $this->Model->getModelData('FinContasPagar', 'getBillsToPay', $date);
        $billsToReceive = $this->Model->getModelData('FinContasReceber', 'getBillsToReceive', $date);
        $invoicesToReceive = $this->Model->getModelData('FaturaEncerramentos', 'getInvoicesToReceive', $date);

        $result = $this->Dashboard->intermediateGraphicData($billsToPay, $billsToReceive, $invoicesToReceive, $date);

        $pays = $result[0];
        $receives = $result[1];
        $invoices = $result[2];
        $days = $result[3];
        $projectedBalance = $result[4];

        $this->set(compact('pays', 'receives', 'invoices', 'projectedBalance', 'days'));
        $this->set('_serialize', ['pays', 'receives', 'invoices', 'projectedBalance', 'days']);
    }

    //Função para pegar movimentos por empresa.
    public function movesForCompany($idCompany = null)
    {
        //Pegando dados do banco.
        $bankMoves = $this->Model->getModelData('FinBancoMovimentos', 'getAllOpenedMovesBank', $idCompany);
        $groups = $this->Model->getModelData('FinGrupos', 'getAllGroups');

        //Lapidando os dados pegos no passo anterior.
        $resultBankMoves = $this->workWithBankMovesData($bankMoves);

        $this->set(compact('page', 'subpage', 'bankMoves', 'groups', 'resultBankMoves', 'somaSaldoProvisionado'));
        $this->set('_serialize', ['page', 'subpage', 'bankMoves', 'groups', 'resultBankMoves', 'somaSaldoProvisionado']);
        $this->render('/DashboardFinanceiro/index');
    }

    //Função para pegar empresas por grupo.
    //Toda requisição ajax, tem que ser passada no BeforeFilter do AppController.
    public function getCompaniesForGroup($idGroup = null)
    {
        //Pegando dados do banco
        $companies = $this->Model->getModelData('FinContabilidades', 'getCompaniesForGroup', $idGroup);

        $this->set(compact('companies'));
        $this->set('_serialize', ['companies']);
    }

    //Função para pegar dados do modal por movimentos
    public function getMovesForModal()
    {
        $dia = !empty($this->request->query(['dia'])) ? $this->Date->formatDateForSql($this->request->query(['dia'])) : null;
        if (isset($dia)) {
            $result = $this->Model->getModelData('FinMovimentos', 'getMovesForModal', $dia);
        } else {
            $result = [];
        }

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }

    //Função para pegar dados do modal por contas a pagar ou receber
    public function getBillsForModal()
    {
        $dia = !empty($this->request->query(['dia'])) ? $this->Date->formatDateForSql($this->request->query(['dia'])) : null;
        $value = !empty($this->request->query(['value'])) ? $this->request->query(['value']) : null;

        if (!isset($value)) {
            $result = [];
        } else {
            if (!isset($dia)) {
                $result = [];
            } else {
                if ($value === 'receber') {
                    $result = $this->Model->getModelData('FinContasReceber', 'getBillsToReceiveForModal', $dia);
                } else if ($value === 'pagar') {
                    $result = $this->Model->getModelData('FinContasPagar', 'getBillsToPayForModal', $dia);
                } else if ($value === 'receber-convenio') {
                    $result = $this->Model->getModelData('FaturaEncerramentos', 'getInvoicesToReceiveForModal', $dia);
                }
            }
        }

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Funções que só podem ser acessadas por essa controller. *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    //Função para iteração de soma de saldos.
    private function workWithBankMovesData($bankMoves)
    {
        $resultBankMoves = [];
        $resultBankMoves[0] = 0;
        $resultBankMoves[1] = 0;

        foreach ($bankMoves as $move) {
            $resultBankMoves[0] += floatval($move->soma_movimentos_saldos);
            $resultBankMoves[1] += $move->saldoInicial;
        }
        return $resultBankMoves;
    }

    //Função para iteração dos Movimentos.
    private function workWithMovesData($moves, $transfs, $date = null)
    {
        //Pegando o ultimo dia do mês para usar na iteração
        $lastDayMonth = $this->Date->getLastDayOfMonth($date);

        //Pegando o saldo final do mês passado, para colocar no saldo acumulado do dia 01 do mês atual.
        $lastBankFinalBalance = $this->Model->getModelData('FinBancoMovimentos', 'getLastMonthFinalBalance', $date);
        $totalResultMoves = new \stdClass();
        $totalResultMoves->totalEntradas = 0;
        $totalResultMoves->totalSaidas = 0;
        $totalResultMoves->totalTransferencias = 0;
        $totalResultMoves->totalSaldo = 0;
        $totalResultMoves->totalAcumulado = 0;
        $resultMoves = [];

        //Dando for para preencher os dias com valores.
        //Aqui é usado for, pois precisamos pegar todos os dias, inclusive os que não estavam preenchidos no banco.
        for ($i = 0; $i < $lastDayMonth; $i++) {
            $day = $i < 9 ? '0' . ($i + 1) : $i + 1;
            $day = (string) $day;

            if ($i === 0) {
                $resultMoves[] = $this->createClassForMovesData($day, $lastBankFinalBalance);
            } else {
                $resultMoves[] = $this->createClassForMovesData($day, $resultMoves[$i - 1]->saldoAcumulado);
            }

            $diaTabela = intval($day);
            foreach ($moves as $key => $move) {
                $diaBanco = intval($moves[$key]->dia);
                if ($diaBanco === $diaTabela) {
                    $resultMoves[$i] = $moves[$key];
                    if ($i === 0) {
                        $resultMoves[$i]->saldoAcumulado = $moves[$key]->saldo + $lastBankFinalBalance;
                    } else {
                        $resultMoves[$i]->saldoAcumulado = $moves[$key]->saldo + $resultMoves[$i - 1]->saldoAcumulado;
                    }
                }
            }

            foreach ($transfs as $key => $transf) {
                $diaBanco = intval($transfs[$key]->dia);
                if ($diaBanco === $diaTabela) {
                    $resultMoves[$i]->transferencia_c = $transfs[$key]->transferencia_c;
                    $resultMoves[$i]->transferencia_d = $transfs[$key]->transferencia_d;
                }
            }

            $totalResultMoves->totalEntradas += $resultMoves[$i]->entradas;
            $totalResultMoves->totalSaidas += $resultMoves[$i]->saidas;
            $totalResultMoves->totalTransferencias += $resultMoves[$i]->transferencia_c - $resultMoves[$i]->transferencia_d;
            $totalResultMoves->totalSaldo += $resultMoves[$i]->saldo;
            $totalResultMoves->totalAcumulado = $resultMoves[$i]->saldoAcumulado;
        }
        return [$resultMoves, $totalResultMoves];
    }

    //Função para criar um objeto para a iteração de movimentos
    private function createClassForMovesData($day, $saldoAcumulado = 0)
    {
        $object = new \stdClass(); //Atribuindo uma classe à posição do array!
        $object->dia = $day; //Note que o valor de $i começa em 0, pois o array pego do banco começa em 0;
        $object->entradas = 0;
        $object->saidas = 0;
        $object->transferencia_d = 0;
        $object->transferencia_c = 0;
        $object->saldo = 0;
        $object->saldoAcumulado = $saldoAcumulado;

        return $object;
    }

    //Função para criar um objeto para a iteração de movimentos
    private function createClassForBills($type, $day)
    {
        $object = new \stdClass(); //Atribuindo uma classe à posição do array!
        $object->dia = $day; //Note que o valor de $i começa em 0, pois o array pego do banco começa em 0;
        if ($type === 'contaspagar') {
            $object->contasPagar = 0;
        } else if ($type === 'contasreceber') {
            $object->contasReceber = 0;
        } else if ($type === 'contasreceberconvenio') {
            $object->contasReceberConvenio = 0;
        }
        return $object;
    }

    //Função para iteração de dados das contas à pagar e à receber
    private function workWithBillsData($billsToPay, $billsToReceive, $invoicesToReceive, $date = null, $resultMoves, $actualBalance)
    {
        //Pegando o ultimo dia do mês para usar na iteração
        $lastDayMonth = $this->Date->getLastDayOfMonth($date);

        //Pegando o saldo final do mês passado, para colocar no saldo acumulado do dia 01 do mês atual.
        $lastBankFinalBalance = $this->Model->getModelData('FinBancoMovimentos', 'getLastMonthFinalBalance', $date);

        $totalResultBills = new \stdClass();
        $totalResultBills->totalContasPagar = 0;
        $totalResultBills->totalContasReceber = 0;
        $totalResultBills->totalContasReceberConvenio = 0;
        $totalResultBills->totalSaldoDia = 0;
        $totalResultBills->saldoAcumulado = 0;
        $totalResultBills->saldoProjetado = 0;

        $billsToPay = $this->workWithPayBillsData($billsToPay, $date);
        $billsToReceive = $this->workWithReceiveBillsData($billsToReceive, $date);
        $invoicesToReceive = $this->workWithInvoiceBillsData($invoicesToReceive, $date);

        //Atribuindo o objeto e os valores 0 para referência do auto-incremento
        for ($i = 0; $i < $lastDayMonth; $i++) {
            $day = $i < 9 ? '0' . ($i + 1) : $i + 1;
            $day = (string) $day;

            $resultBills[$i] = new \stdClass();
            $resultBills[$i]->dia = $day;
            $resultBills[$i]->contasPagar = $billsToPay[$i]->contasPagar;
            $resultBills[$i]->contasReceber = $billsToReceive[$i]->contasReceber;
            $resultBills[$i]->contasReceberConvenio = $invoicesToReceive[$i]->contasReceberConvenio;
            $resultBills[$i]->saldoDoDia = ($billsToReceive[$i]->contasReceber + $invoicesToReceive[$i]->contasReceberConvenio) - $billsToPay[$i]->contasPagar;

            $saldoProjetado = $resultMoves[$i]->saldoAcumulado;

            $resultBills[$i]->saldoAcumulado = $i === 0 ? $resultBills[$i]->saldoDoDia
            : $resultBills[$i - 1]->saldoAcumulado + $resultBills[$i]->saldoDoDia;

            $resultBills[$i]->saldoProjetado = $i === 0 ? $actualBalance + $resultBills[$i]->saldoDoDia : $resultBills[$i - 1]->saldoProjetado + $resultBills[$i]->saldoDoDia;

            $totalResultBills->totalContasPagar += $resultBills[$i]->contasPagar;
            $totalResultBills->totalContasReceber += $resultBills[$i]->contasReceber;
            $totalResultBills->totalContasReceberConvenio += $resultBills[$i]->contasReceberConvenio;
            $totalResultBills->totalSaldoDia += $resultBills[$i]->saldoDoDia;
            $totalResultBills->saldoAcumulado = $resultBills[$i]->saldoAcumulado;
            $totalResultBills->saldoProjetado = $resultBills[$i]->saldoProjetado;
        }

        return [$resultBills, $totalResultBills];
    }

    //Função para ordenação das contas à pagar
    private function workWithPayBillsData($billsToPay, $date = null)
    {
        //Pegando o ultimo dia do mês para usar na iteração
        $lastDayMonth = $this->Date->getLastDayOfMonth($date);

        for ($i = 0; $i < $lastDayMonth; $i++) {
            $day = $i < 9 ? '0' . ($i + 1) : $i + 1;
            $day = (string) $day;

            $resultBills[] = $this->createClassForBills('contaspagar', $day);

            $diaTabela = intval($day);
            foreach ($billsToPay as $key => $bill) {
                $diaBanco = intval($billsToPay[$key]->dia);

                if ($diaBanco === $diaTabela) {
                    $resultBills[$i] = $billsToPay[$key];
                }
            }
        }
        return $resultBills;
    }

    //Função para ordernação das contas à receber
    private function workWithReceiveBillsData($billsToReceive, $date = null)
    {
        //Pegando o ultimo dia do mês para usar na iteração
        $lastDayMonth = $this->Date->getLastDayOfMonth($date);

        for ($i = 0; $i < $lastDayMonth; $i++) {
            $day = $i < 9 ? '0' . ($i + 1) : $i + 1;
            $day = (string) $day;

            $resultBills[] = $this->createClassForBills('contasreceber', $day);

            $diaTabela = intval($day);
            foreach ($billsToReceive as $key => $move) {
                $diaBanco = intval($billsToReceive[$key]->dia);
                if ($diaBanco === $diaTabela) {
                    $resultBills[$i] = $billsToReceive[$key];
                }
            }
        }
        return $resultBills;
    }

    //Função para ordernação das contas à receber convnio
    private function workWithInvoiceBillsData($invoicesToReceive, $date = null)
    {
        //Pegando o ultimo dia do mês para usar na iteração
        $lastDayMonth = $this->Date->getLastDayOfMonth($date);

        for ($i = 0; $i < $lastDayMonth; $i++) {
            $day = $i < 9 ? '0' . ($i + 1) : $i + 1;
            $day = (string) $day;

            $resultBills[] = $this->createClassForBills('contasreceberconvenio', $day);

            $diaTabela = intval($day);
            foreach ($invoicesToReceive as $key => $move) {
                $diaBanco = intval($invoicesToReceive[$key]->dia);
                if ($diaBanco === $diaTabela) {
                    $resultBills[$i] = $invoicesToReceive[$key];
                }
            }
        }
        return $resultBills;
    }
}
