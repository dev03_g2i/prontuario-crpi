<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TextoGrupos Controller
 *
 * @property \App\Model\Table\TextoGruposTable $TextoGrupos
 */
class TextoGruposController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $textoGrupos = $this->paginate($this->TextoGrupos);


        $this->set(compact('textoGrupos'));
        $this->set('_serialize', ['textoGrupos']);

    }

    /**
     * View method
     *
     * @param string|null $id Texto Grupo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $textoGrupo = $this->TextoGrupos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('textoGrupo', $textoGrupo);
        $this->set('_serialize', ['textoGrupo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $textoGrupo = $this->TextoGrupos->newEntity();
        if ($this->request->is('post')) {
            $textoGrupo = $this->TextoGrupos->patchEntity($textoGrupo, $this->request->data);
            if ($this->TextoGrupos->save($textoGrupo)) {
                $this->Flash->success(__('O texto grupo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O texto grupo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('textoGrupo'));
        $this->set('_serialize', ['textoGrupo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Texto Grupo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $textoGrupo = $this->TextoGrupos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $textoGrupo = $this->TextoGrupos->patchEntity($textoGrupo, $this->request->data);
            if ($this->TextoGrupos->save($textoGrupo)) {
                $this->Flash->success(__('O texto grupo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O texto grupo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('textoGrupo'));
        $this->set('_serialize', ['textoGrupo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Texto Grupo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
        $textoGrupo = $this->TextoGrupos->get($id);
                $textoGrupo->situacao_id = 2;
        if ($this->TextoGrupos->save($textoGrupo)) {
            $this->Flash->success(__('O texto grupo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O texto grupo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Texto Grupo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->TextoGrupos->find('all')
        ->where(['TextoGrupos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TextoGrupos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Texto Grupo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $textoGrupo = $this->TextoGrupos->get($this->request->data['id']);
            $res = ['nome'=>$textoGrupo->nome,'id'=>$textoGrupo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
