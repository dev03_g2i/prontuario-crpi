<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GrupoControladorAcoes Controller
 *
 * @property \App\Model\Table\GrupoControladorAcoesTable $GrupoControladorAcoes
 */
class GrupoControladorAcoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['GrupoControladores', 'Acoes', 'SituacaoCadastros', 'Users']
        ];
        $grupoControladorAcoes = $this->paginate($this->GrupoControladorAcoes);


        $this->set(compact('grupoControladorAcoes'));
        $this->set('_serialize', ['grupoControladorAcoes']);

    }

    /**
     * View method
     *
     * @param string|null $id Grupo Controlador Aco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoControladorAco = $this->GrupoControladorAcoes->get($id, [
            'contain' => ['GrupoControladores', 'Acoes', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('grupoControladorAco', $grupoControladorAco);
        $this->set('_serialize', ['grupoControladorAco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupoControladorAco = $this->GrupoControladorAcoes->newEntity();
        if ($this->request->is('post')) {
            $grupoControladorAco = $this->GrupoControladorAcoes->patchEntity($grupoControladorAco, $this->request->data);
            if ($this->GrupoControladorAcoes->save($grupoControladorAco)) {
                $this->Flash->success(__('O grupo controlador aco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo controlador aco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('grupoControladorAco'));
        $this->set('_serialize', ['grupoControladorAco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Controlador Aco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoControladorAco = $this->GrupoControladorAcoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupoControladorAco = $this->GrupoControladorAcoes->patchEntity($grupoControladorAco, $this->request->data);
            if ($this->GrupoControladorAcoes->save($grupoControladorAco)) {
                $this->Flash->success(__('O grupo controlador aco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo controlador aco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('grupoControladorAco'));
        $this->set('_serialize', ['grupoControladorAco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Controlador Aco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupoControladorAco = $this->GrupoControladorAcoes->get($id);
                $grupoControladorAco->situacao_id = 2;
        if ($this->GrupoControladorAcoes->save($grupoControladorAco)) {
            $this->Flash->success(__('O grupo controlador aco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo controlador aco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Grupo Controlador Aco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->GrupoControladorAcoes->find('all')
        ->where(['GrupoControladorAcoes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('GrupoControladorAcoes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Grupo Controlador Aco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $grupoControladorAco = $this->GrupoControladorAcoes->get($this->request->data['id']);
            $res = ['nome'=>$grupoControladorAco->nome,'id'=>$grupoControladorAco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function permitir(){
        $id = $this->request->data['id'];
        $grupoControladorAco = $this->GrupoControladorAcoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['acesso'] = $grupoControladorAco->acesso==1 ? 0 : 1;
            $grupoControladorAco = $this->GrupoControladorAcoes->patchEntity($grupoControladorAco, $this->request->data);
            if ($this->GrupoControladorAcoes->save($grupoControladorAco)){
               $res = ['msg'=>'Atualizado com sucesso!'];
            } else {
               $res = ['msg'=>'Erro ao atualizar!'];
            }

            $this->autoRender=false;
            $this->response->type('json');
            $this->response->body(json_encode($res));
        }

        $this->set(compact('grupoControladorAco'));
        $this->set('_serialize', ['grupoControladorAco']);

    }
}
