<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\GrupoRetorno;
use Cake\I18n\Time;
use DateTime;
use Cake\Network\Session;
use Cake\Mailer\Email;
/**
 * Retornos Controller
 *
 * @property \App\Model\Table\RetornosTable $Retornos
 */
class RetornosController extends AppController
{

    public $components = array('Data','Sms','Ics','Excell');
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $query = $this->Retornos->find('all')
                 ->contain(['Clientes', 'GrupoRetornos', 'SituacaoRetornos', 'Users', 'SituacaoCadastros'])
                 ->where(['Retornos.situacao_id = ' => '1']);

        $grupo=null;
        if(!empty($this->request->query['grupo']) && empty($this->request->query['grupo_id']) ){
            $query->andWhere(['Retornos.grupo_id'=>$this->request->query['grupo']]);
            $grupo = $this->request->query['grupo'];
        }

        if(!empty($this->request->query['grupo_id'])){
            $query->andWhere(['Retornos.grupo_id'=>$this->request->query['grupo_id']]);
            $grupo = $this->request->query['grupo_id'];
        }

        $cliente = null;
        if(!empty($this->request->query['cliente_id'])) {
            $this->loadModel('Clientes');
            $clientes = $this->Clientes->get($this->request->query['cliente_id']);
            $query->andWhere(['Retornos.cliente_id ' => $this->request->query['cliente_id']]);
        }


        if(!empty($this->request->query['inicio'])){
            $query->andWhere(['Retornos.previsao >=' => $this->Data->DataSQL($this->request->query['inicio'])]);
        }

        if(!empty($this->request->query['fim'])){
            $query->andWhere(['Retornos.previsao <=' => $this->Data->DataSQL($this->request->query['fim'])]);
        }

        $inicio = null;
        if(!empty($this->request->query['tipo'])){
            if($this->request->query['tipo']==1){
                $query->andWhere(['Retornos.previsao <'=>date('Y-m-d')]);

                $inicio = date('d/m/Y');
            }
        }
        $query->orderDesc('Retornos.id');
        $retornos = $this->paginate($query);

        $grupoRetornos = $this->Retornos->GrupoRetornos->find('list', ['limit' => 200]);
        $situacaoRetornos = $this->Retornos->SituacaoRetornos->find('list', ['limit' => 200]);


        $this->set(compact('retornos','grupoRetornos','situacaoRetornos','grupo','clientes','inicio'));
        $this->set('_serialize', ['retornos']);
    }

    /**
     * View method
     *
     * @param string|null $id Retorno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $retorno = $this->Retornos->get($id, [
            'contain' => ['Clientes', 'GrupoRetornos', 'SituacaoRetornos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('retorno', $retorno);
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $retorno = $this->Retornos->newEntity();

        $grupo=null;
        if(!empty($this->request->query['grupo_id'])){
            $grupo = $this->request->query['grupo_id'];
        }


        if ($this->request->is('post')) {
            $retorno = $this->Retornos->patchEntity($retorno, $this->request->data);

            if ($this->Retornos->save($retorno)) {
                $retornos = $this->Retornos->get($retorno->id,['contain'=>['Clientes']]);
                $mensagem = "Instituto Eduardo Ayub. Foi marcado um retorno para o dia ".$this->Data->data_sms($retornos->previsao);
               /* $retorno = $this->Sms->enviar(
                    [
                        'numero' => $retornos->cliente->celular,
                        'mensagem' => $mensagem,
                        'data' => date('Y-m-d H:i:s'),
                        'id' => $retornos->id,
                        'agregado' => $retornos->id
                    ]);*/

                $this->Flash->success(__('O retorno foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index','?'=>['grupo'=>$grupo]]);
            } else {
                $this->Flash->error(__('O retorno não foi salvo. Por favor, tente novamente.'));
            }
        }


        $cliente = null;
        $grupoRetornos = null;
        if(!empty($this->request->query['cliente_id'])) {
            $this->loadModel('Clientes');
            $clientes = $this->Clientes->get($this->request->query['cliente_id']);
            $this->loadModel('ClienteGrupoRetornos');

            $cliente_grupo = $this->ClienteGrupoRetornos->find('all')
                ->where(['ClienteGrupoRetornos.situacao_id'=>1,
                        'ClienteGrupoRetornos.cliente_id'=>$clientes->id]);

            $id_grupos=[];
            foreach ($cliente_grupo as $c) {
                $id_grupos[] = $c->grupo_id;
            }
            if(!empty($id_grupos)) { // aqui filtro o vinculo do cliente ao grupo
                $grupoRetornos = $this->Retornos->GrupoRetornos->find('list')->where(['GrupoRetornos.id in ' => $id_grupos]);
            }
        }else{
            $grupoRetornos = $this->Retornos->GrupoRetornos->find('list', ['limit' => 200]);
        }

        $situacaoRetornos = $this->Retornos->SituacaoRetornos->find('list', ['limit' => 200]);
        $users = $this->Retornos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Retornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('retorno', 'clientes', 'grupoRetornos', 'situacaoRetornos', 'users', 'situacaoCadastros','grupo'));
        $this->set('_serialize', ['retorno']);
    }

    public function getdays(){
        $this->autoRender=false;
        $this->response->type('json');
        $json = null;
        if(!empty($this->request->data['date'])){
            $data = new Time();
            $now = $data->createFromFormat('d/m/Y',$this->request->data['date']);
            $dif =$now->diffInDays();

            $json = json_encode(['result'=>$dif]);

        }else if(!empty($this->request->data['dias'])){
            $data = new Time(date('Y-m-d'));
            $now = $data->addDay($this->request->data['dias']);
            $json = json_encode(['result'=>$now->format('d/m/Y')]);
        }
        $this->response->body($json);
    }


    /**
     * Edit method
     *
     * @param string|null $id Retorno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $retorno = $this->Retornos->get($id, [
            'contain' => []
        ]);

        $grupo=null;
        if(!empty($this->request->query['grupo_id'])){
            $grupo = $this->request->query['grupo_id'];
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $retorno = $this->Retornos->patchEntity($retorno, $this->request->data);
            if ($this->Retornos->save($retorno)) {

                $this->Flash->success(__('O retorno foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index','?'=>['grupo'=>$grupo]]);
            } else {
                $this->Flash->error(__('O retorno não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->loadModel('Clientes');
        $clientes = $this->Clientes->get($retorno->cliente_id);

        $grupoRetornos = $this->Retornos->GrupoRetornos->find('list', ['limit' => 200]);
        $situacaoRetornos = $this->Retornos->SituacaoRetornos->find('list', ['limit' => 200]);
        $users = $this->Retornos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Retornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('retorno', 'clientes', 'grupoRetornos', 'situacaoRetornos', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['retorno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Retorno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $retorno = $this->Retornos->get($id);
                $retorno->situacao_id = 2;
        if ($this->Retornos->save($retorno)) {
            $this->Flash->success(__('O retorno foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O retorno não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function aprovar($id = null)
    {
        $retorno = $this->Retornos->get($id, [
            'contain' => [
                'Clientes'=>[
                    'ClienteResponsaveis' => [
                        'GrauParentescos', 'conditions' => [
                            'grau_id in' => ['1', '2']
                        ]
                    ]
                ], 'GrupoRetornos', 'SituacaoRetornos', 'Users', 'SituacaoCadastros']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if($this->request->data['situacao_retorno_id']!=$retorno->situacao_retorno_id){
                $this->loadModel('LogRetornos');
                $logRetorno = $this->LogRetornos->newEntity();
                $logRetorno->retorno_id = $retorno->id;
                $logRetorno->cliente_id = $retorno->cliente_id;
                $logRetorno->grupo_id = $retorno->grupo_id;
                $logRetorno->previsao = $retorno->previsao;
                $logRetorno->dias = $retorno->dias;
                $logRetorno->situacao_retorno_id = $retorno->situacao_retorno_id;
                $logRetorno->user_id = $retorno->user_id;
                $logRetorno->observacao = $retorno->observacao;
                $logRetorno->justificativa = $retorno->justificativa;
                $logRetorno->data_confirmacao = $retorno->data_confirmacao;
                $logRetorno->quem_aprovou = $retorno->quem_aprovou;
                $this->LogRetornos->save($logRetorno);
            }
            if($this->request->data['situacao_retorno_id']==3) {
                $this->request->data['previsao'] = $this->request->data['futuro'];
            }

            if($this->request->data['situacao_retorno_id']==2) {
                $this->request->data['data_confirmacao'] = date('Y-m-d H:i:s');
                $this->request->data['quem_aprovou'] = $this->Auth->user('id');

                $this->loadModel('Agendas');
                $agenda = $this->Agendas->newEntity();
                $agenda->tipo_id = $this->request->data['tipoagenda_id'];
                $agenda->cliente_id = $retorno->cliente_id;
                $agenda->profissional_id =!empty($this->request->data['medico_id'])?$this->request->data['medico_id']:null;
                $agenda->grupo_id =$this->request->data['grupoagenda_id'];
                $agenda->situacao_agenda_id =1;
                $agenda->inicio =$this->request->data['inicio'];
                $agenda->fim =!empty($this->request->data['fim']) ? $this->request->data['fim'] : null;
                $agenda->observacao =$this->request->data['observacao'];
                $agenda->retorno_id = $retorno->id;
                if($this->Agendas->save($agenda)){
                    $agenda = $this->Agendas->get($agenda->id,['contain'=>['TipoAgendas','Clientes','MedicoResponsaveis','GrupoAgendas','SituacaoAgendas']]);

                    /*$dados = [
                        'model'=>'Agendamento',
                        'id'=>$agenda->id,
                        'start'=>$agenda->inicio,
                        'descricao'=>'Agendamento com o DR(a) '.$agenda->grupo_agenda->nome,
                        'end'=>$agenda->fim,
                        'local'=>'Instituto Eduardo Ayub',
                        'titulo'=>'Agendamento',
                    ];

                    $this->Ics->send((object)$dados,[
                        ['email'=>'tfarias@aedu.com','nome'=>'tiago'],
                        ['email'=>'tfariasg3@gmail.com','nome'=>'tiago de farias'],
                        ['email'=>'tiaguitog3@hotmail.com','nome'=>'tiago de farias']
                    ]);*/
                }
            }

            $retorno = $this->Retornos->patchEntity($retorno, $this->request->data);
            if ($this->Retornos->save($retorno)) {
               /* $dados = [
                    'model'=>'Retornos',
                    'id'=>$retorno->id,
                    'start'=>$retorno->previsao,
                    'descricao'=>'Você tem um retorno marcado!',
                    'end'=>$retorno->previsao,
                    'local'=>'Instituto Eduardo Ayub',
                    'titulo'=>'Retorno de consulta',
                ];

                $this->Ics->send((object)$dados,[
                    ['email'=>'tfarias@aedu.com','nome'=>'tiago'],
                    ['email'=>'tfariasg3@gmail.com','nome'=>'tiago de farias'],
                ]);*/
                echo 1;
            } else {
                echo 'erro';
            }
            $this->autoRender= false;
        }

        $this->loadModel('MedicoResponsaveis');
        $this->loadModel('GrupoAgendas');
        $this->loadModel('TipoAgendas');
        $medicos = $this->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'=>1]);
        $grupos = $this->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id'=>1]);
        $tipo = $this->TipoAgendas->find('list')->where(['TipoAgendas.situacao_id'=>1]);
        $situacaoRetornos = $this->Retornos->SituacaoRetornos->find('list', ['limit' => 200]);

        $this->set(compact('retorno','situacaoRetornos','medicos','grupos','tipo'));
        $this->set('_serialize', ['retorno']);
    }


    public function marcados(){
        $this->autoRender = false;
        $this->response->type('json');
        if(!empty($this->request->query['grupo_id']) && !empty($this->request->query['cliente_id'])) {
            $test = $this->Retornos->find('all')
                ->where(['Retornos.situacao_id' => 1])
                ->andWhere(['Retornos.situacao_retorno_id' => 1])
                ->andWhere(['Retornos.cliente_id' => $this->request->query['cliente_id']])
                ->andWhere(['Retornos.grupo_id' => $this->request->query['grupo_id']])->orderDesc('created');
            $result = $test->first();
            if(!empty($result)) {
                $json = json_encode(['result' => ['previsao' => $result->previsao->format('d/m/Y'),'id'=>$result->id]]);
                $this->response->body($json);
            }
        }
    }

    public function retornosMarcados(){
        $this->autoRender = false;
        $this->response->type('json');
        $json =  json_encode(['result' => null]);
        if(!empty($this->request->query['cliente_id'])) {
            $retornos = $this->Retornos->find('all')
                ->contain(['GrupoRetornos'])
                ->where(['Retornos.situacao_id' => 1])
                ->andWhere(['Retornos.situacao_retorno_id IN' => [1,3]])
                ->andWhere(['Retornos.cliente_id' => $this->request->query['cliente_id']])
                ->orderDesc('Retornos.created');
            $result=array();
            foreach ($retornos as $retorno) {
                $result[] = ['id'=>$retorno->id,'nome'=>$retorno->grupo_retorno->nome.' - '.$retorno->previsao];
            }
            if(!empty($result)) {
                $json = json_encode(['result' => $result]);

            }
        }
        $this->response->body($json);
    }

    public function gerarExcell()
    {

        $this->autoRender = false;

        $retornos = $this->Retornos->find('all')
            ->contain(['Clientes', 'GrupoRetornos'])
            ->where(['Retornos.situacao_retorno_id' => 1])
            ->andWhere(['Retornos.situacao_id' => 1])
            ->andWhere(['Retornos.previsao <= ' => date('Y-m-d')])
            ->andWhere(['Retornos.id >'=>60103]);

        $this->loadModel('ClienteHistoricos');
        $dados = [];
        foreach ($retornos as $retorno) {
            $historicos = $this->ClienteHistoricos->find('all')
                ->contain(['MedicoResponsaveis'])
                ->where(['ClienteHistoricos.situacao_id' => 1])
                ->andWhere(['ClienteHistoricos.cliente_id' => $retorno->cliente_id])
                ->orderDesc('ClienteHistoricos.created')
                ->limit(3);

            if (!empty($historicos)) {
                foreach ($historicos as $historico) {
                    $dados[] = [
                        $retorno->id,
                        $retorno->grupo_retorno->nome,
                        $this->Data->data_sms($retorno->previsao),
                        $this->Data->data_sms($historico->created),
                        $historico->descricao,
                        $historico->medico_responsavei->nome,
                        $retorno->cliente_id,
                        $retorno->cliente->nome
                    ];
                }
            } else {
                $dados[] = [
                    $retorno->id,
                    $retorno->grupo_retorno->nome,
                    $this->Data->data_sms($retorno->previsao),
                    null,
                    null,
                    null,
                    $retorno->cliente_id,
                    $retorno->cliente->nome
                ];
            }
            $header = ['ID', 'Grupo Retorno', 'Previsão', 'Data Anotação', 'Anotação', 'Profissional', 'Prontuário', 'Paciente'];
            $this->Excell->create('retornos', $header, $dados);

        }
    }
    public function gerarProgramados()
    {

        $this->autoRender = false;

        $retornos = $this->Retornos->find('all')
            ->contain(['Clientes', 'GrupoRetornos'])
            ->where(['Retornos.situacao_retorno_id' => 1])
            ->andWhere(['Retornos.situacao_id' => 1])
            ->andWhere(['Retornos.previsao <= ' => '2016-10-26']);


        $dados = [];
        foreach ($retornos as $retorno) {
                $dados[] = [
                    $retorno->id,
                    $retorno->cliente_id,
                    $retorno->cliente->nome,
                    $retorno->grupo_retorno->nome,
                    $this->Data->data_sms($retorno->previsao),
                ];
            }

            $header = ['ID', 'Prontuário', 'Paciente','Grupo Retorno', 'Previsão' ];
            $this->Excell->create('retornos', $header, $dados);


    }



    function procedimento(){
        $retorno = 1;
        if(!empty($this->request->data['retorno_id'])) {
            $retorno = $this->Retornos->get($this->request->data['retorno_id']);
            if(!empty($retorno->observacao)){
                $retorno=json_encode(['procedimento'=>$retorno->observacao]);
            }
        }
        $this->autoRender=false;
        $this->response->type('json');
        $this->response->body($retorno);

    }
}
