<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EstqArtigos Controller
 *
 * @property \App\Model\Table\EstqArtigosTable $EstqArtigos
 */
class EstqArtigosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->EstqArtigos->find('all')->where(['EstqArtigos.situacao_id' => 1]);

        if(!empty($this->request->query('nome'))){
            $query->andWhere(['EstqArtigos.nome LIKE ' => '%'.$this->request->query('nome').'%']);
        }

        $estqArtigos = $this->paginate($query);

        $this->set(compact('estqArtigos'));
        $this->set('_serialize', ['estqArtigos']);

    }

    /**
     * View method
     *
     * @param string|null $id Estq Artigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estqArtigo = $this->EstqArtigos->get($id, [
            'contain' => ['Users', 'Situacaos']
        ]);

        $this->set('estqArtigo', $estqArtigo);
        $this->set('_serialize', ['estqArtigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $estqArtigo = $this->EstqArtigos->newEntity();
        if ($this->request->is('post')) {
            $estqArtigo = $this->EstqArtigos->patchEntity($estqArtigo, $this->request->data);
            if ($this->EstqArtigos->save($estqArtigo)) {
                $this->Flash->success(__('O estq artigo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $artigos = $this->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('estqArtigo', 'artigos'));
        $this->set('_serialize', ['estqArtigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estq Artigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $estqArtigo = $this->EstqArtigos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estqArtigo = $this->EstqArtigos->patchEntity($estqArtigo, $this->request->data);
            if ($this->EstqArtigos->save($estqArtigo)) {
                $this->Flash->success(__('O estq artigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $artigos = $this->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('estqArtigo', 'artigos'));
        $this->set('_serialize', ['estqArtigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estq Artigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
        $estqArtigo = $this->EstqArtigos->get($id);
                $estqArtigo->situacao_id = 2;
        if ($this->EstqArtigos->save($estqArtigo)) {
            $this->Flash->success(__('O estq artigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O estq artigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Estq Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->EstqArtigos->find('all')
        ->where(['EstqArtigos.nome LIKE ' => $termo . '%'])
        ->orWhere(['EstqArtigos.id' => $termo]);

        $cont = $query->count();
        $query->orderAsc('EstqArtigos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Estq Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $estqArtigo = $this->EstqArtigos->get($this->request->data['id']);
            $res = ['nome'=>$estqArtigo->nome,'id'=>$estqArtigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
