<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProcedimentoGastos Controller
 *
 * @property \App\Model\Table\ProcedimentoGastosTable $ProcedimentoGastos
 */
class ProcedimentoGastosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Procedimentos', 'Gastos'=>[
                'strategy' => 'select']]
        ];

        $query = [];
        $procedimento_id = null;
        if(!empty($this->request->query['procedimento_id'])){
            $procedimento_id = $this->request->query['procedimento_id'];
            $query['conditions'][]= ['ProcedimentoGastos.procedimento_id'=>$procedimento_id];
        }
        $this->paginate = array_merge($this->paginate,$query);



        $procedimentoGastos = $this->paginate($this->ProcedimentoGastos);


        $this->set(compact('procedimentoGastos','procedimento_id'));
        $this->set('_serialize', ['procedimentoGastos']);

    }

    /**
     * View method
     *
     * @param string|null $id Procedimento Gasto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $procedimentoGasto = $this->ProcedimentoGastos->get($id, [
            'contain' => ['Procedimentos', 'Gastos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('procedimentoGasto', $procedimentoGasto);
        $this->set('_serialize', ['procedimentoGasto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $procedimentoGasto = $this->ProcedimentoGastos->newEntity();
        if ($this->request->is('post')) {
            $procedimentoGasto = $this->ProcedimentoGastos->patchEntity($procedimentoGasto, $this->request->data);
            if ($this->ProcedimentoGastos->save($procedimentoGasto)) {
                $this->Flash->success(__('O procedimento gasto foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O procedimento gasto não foi salvo. Por favor, tente novamente.'));
            }
        }

        $procedimento_id = null;
        if(!empty($this->request->query['procedimento_id'])){
            $procedimento_id = $this->request->query['procedimento_id'];
        }
        $this->set(compact('procedimentoGasto','procedimento_id'));
        $this->set('_serialize', ['procedimentoGasto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Procedimento Gasto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $procedimentoGasto = $this->ProcedimentoGastos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $procedimentoGasto = $this->ProcedimentoGastos->patchEntity($procedimentoGasto, $this->request->data);
            if ($this->ProcedimentoGastos->save($procedimentoGasto)) {
                $this->Flash->success(__('O procedimento gasto foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O procedimento gasto não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('procedimentoGasto'));
        $this->set('_serialize', ['procedimentoGasto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Procedimento Gasto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $procedimentoGasto = $this->ProcedimentoGastos->get($id);
                $procedimentoGasto->situacao_id = 2;
        if ($this->ProcedimentoGastos->save($procedimentoGasto)) {
            $this->Flash->success(__('O procedimento gasto foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O procedimento gasto não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Procedimento Gasto id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ProcedimentoGastos->find('all')
        ->where(['ProcedimentoGastos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ProcedimentoGastos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    public function fillOut()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ProcedimentoGastos->find('all')->contain([
            'Gastos'=>[
                'strategy' => 'select',
                'queryBuilder' => function ($q) {
                    return $q->where(['Gastos.nome LIKE ' => '%' . $this->request->data['termo'] . '%']);
                }
            ]]);
        if(!empty($this->request->data['auxiliar'])){
            $query->where(['ProcedimentoGastos.procedimento_id' => '%' . $this->request->data['auxiliar'] . '%']);
        }

        $cont = $query->count();
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->gasto->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Procedimento Gasto id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $procedimentoGasto = $this->ProcedimentoGastos->get($this->request->data['id']);
            $res = ['nome'=>$procedimentoGasto->nome,'id'=>$procedimentoGasto->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
