<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GastoItens Controller
 *
 * @property \App\Model\Table\GastoItensTable $GastoItens
 */
class GastoItensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Gastos', 'EstqArtigo', 'Users', 'Situacao']
        ];
        $gastoItens = $this->paginate($this->GastoItens);


        $this->set(compact('gastoItens'));
        $this->set('_serialize', ['gastoItens']);

    }

    /**
     * View method
     *
     * @param string|null $id Gasto Iten id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gastoIten = $this->GastoItens->get($id, [
            'contain' => ['Gastos', 'EstqArtigo', 'Users', 'Situacao']
        ]);

        $this->set('gastoIten', $gastoIten);
        $this->set('_serialize', ['gastoIten']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gastoIten = $this->GastoItens->newEntity();
        if ($this->request->is('post')) {
            $gastoIten = $this->GastoItens->patchEntity($gastoIten, $this->request->data);
            if ($this->GastoItens->save($gastoIten)) {
                $this->Flash->success(__('O gasto iten foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O gasto iten não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('gastoIten'));
        $this->set('_serialize', ['gastoIten']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Gasto Iten id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gastoIten = $this->GastoItens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gastoIten = $this->GastoItens->patchEntity($gastoIten, $this->request->data);
            if ($this->GastoItens->save($gastoIten)) {
                $this->Flash->success(__('O gasto iten foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O gasto iten não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('gastoIten'));
        $this->set('_serialize', ['gastoIten']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Gasto Iten id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gastoIten = $this->GastoItens->get($id);
                $gastoIten->situacao_id = 2;
        if ($this->GastoItens->save($gastoIten)) {
            $this->Flash->success(__('O gasto iten foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O gasto iten não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Gasto Iten id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->GastoItens->find('all')
        ->where(['GastoItens.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('GastoItens.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Gasto Iten id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $gastoIten = $this->GastoItens->get($this->request->data['id']);
            $res = ['nome'=>$gastoIten->nome,'id'=>$gastoIten->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
