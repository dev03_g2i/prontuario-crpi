<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use DateTime;
/**
 * Agendas Controller
 *
 * @property \App\Model\Table\AgendasTable $Agendas
 */
class IndexController extends AppController
{

    public $components = array('Data');

    public function princ(){

    }
    public function index(){


        $this->loadModel('Retornos');

        $expirados = $this->Retornos->find()
            ->select(['total'=>'count(Retornos.id)'])
            ->where(['Retornos.previsao <'=>date('Y-m-d')])
            ->andWhere(['Retornos.situacao_id'=>1])
            ->andWhere(['Retornos.situacao_retorno_id'=>1])
            ->first();

        $reprogramados= $this->Retornos->find()
            ->select(['total'=>'count(Retornos.id)'])
            ->where(['Retornos.situacao_id'=>1])
            ->andWhere(['Retornos.situacao_retorno_id'=>3])
            ->first();

        $programado = $this->Retornos->find()
            ->select(['total'=>'count(Retornos.id)'])
            ->where(['Retornos.situacao_id'=>1])
            ->andWhere(['Retornos.previsao >='=>date('Y-m-d')])
            ->andWhere(['Retornos.situacao_retorno_id'=>1])
            ->first();

        $this->loadModel('Clientes');
        $clientes = $this->Clientes->find('all')
            ->where(['Clientes.situacao_id'=>1])
            ->andWhere(['EXTRACT(MONTH from nascimento) = '=>date('m')]);

        $dia = 0;
        $semana = 0;
        $mes =0;
        $dados_semana = $this->Data->semana();
        if(!empty($clientes)) {
            foreach ($clientes as $cliente) {
                if (!empty($cliente->nascimento)) {
                    $nasc = new Time($cliente->nascimento);
                    $day = $nasc->format('d');
                    if ($day == date('d')) {
                        $dia++;
                    }
                    if ($day >= $dados_semana->domingo && $day <= $dados_semana->sabado) {
                        $semana++;
                    }
                }
                $mes++;
            }
        }
        $clientes = $this->Clientes->find()
            ->select(['total'=>'count(Clientes.id)'])
            ->where(['Clientes.situacao_id'=>1])
            ->first();


        $data = new Time(date('Y-m-d'));
        $dat = new Time(date('Y-m-').'01');
        $first = $dat;
        $this->loadModel('Atendimentos');
        $atendimentos = $this->Atendimentos->find('all',[
            'contain' => ['AtendimentoProcedimentos' => [ 'AtendimentoItens']]
        ])
            ->where(['Atendimentos.data >= '=>$first->format('Y-m-d')])
            ->andWhere(['Atendimentos.tipo '=>0]);

        $tratamentos= $this->Atendimentos->find()
            ->select(['total'=>'count(Atendimentos.id)'])
            ->where(['Atendimentos.data >= '=>$first->format('Y-m-d')])
            ->andWhere(['Atendimentos.tipo '=>1])
        ->first();

        $parcial=0;
        $aprovado=0;
        $reprovado=0;
        foreach ($atendimentos as $atendimento) {
            $aprov = 0;
            $reprov = 0;
            if (!empty($atendimento->atendimento_procedimentos)) {
                foreach ($atendimento->atendimento_procedimentos as $anted) {
                    foreach ($anted->atendimento_itens as $iten_cobranca) {
                        ($iten_cobranca->status == 0) ? $reprov++ : $aprov++;
                    }
                }
                if ($reprov > 0 && $aprov > 0) {
                    $parcial++;
                } else if ($aprov > 0 && $reprov <= 0) {
                    $aprovado++;
                } else {
                    $reprovado++;
                }
            }
        }

        $this->loadModel('SituacaoAgendas');
        $this->loadModel('Agendas');

        $situacaoAgendas = $this->SituacaoAgendas->find('all')
            ->where(['SituacaoAgendas.situacao_id'=>1]);

        $agendas =  array();

        $data_semana = $this->Data->data_semana();
        foreach ($situacaoAgendas as $situacaoAgenda) {
            $agendas[$situacaoAgenda->nome]['mes'] = $this->Agendas->getCountAgendas($situacaoAgenda->id,$first->format('Y-m-d H:i:s'))->total;
            $agendas[$situacaoAgenda->nome]['semana'] = $this->Agendas->getCountAgendas($situacaoAgenda->id,$data_semana->domingo.' 06:00:00',$data_semana->sabado.' 20:00:00')->total;
            $agendas[$situacaoAgenda->nome]['dia'] = $this->Agendas->getCountAgendas($situacaoAgenda->id,date('Y-m-d').' 06:00:00',date('Y-m-d').' 23:59:00')->total;
            $agendas[$situacaoAgenda->nome]['cor'] = $situacaoAgenda->cor;
        }


        $title = "Indicadores";
        $this->set(compact('dia', 'mes', 'semana','expirados','reprogramados','programado','clientes','parcial','reprovado','tratamentos','agendas','title'));
    }

    public function grafico(){
        $this->loadModel('Agendas');
        $agendas = $this->Agendas->find()
            ->select(
                [
                    'total'=>'count(Agendas.id)',
                    'mes_ano'=>'Agendas.inicio',
                    'mes'=> 'EXTRACT(MONTH FROM Agendas.inicio)'
                ]
            )
            ->where(['Agendas.situacao_id'=>1])
            ->andWhere(['Agendas.situacao_agenda_id'=>10])
            ->group('EXTRACT(MONTH FROM Agendas.inicio)')
            ->orderAsc('mes_ano');

        $meses = "";
        $data = "";
        $mes =  $this->Data->meses();
        foreach ($agendas as $agenda) {
            $meses[] = $mes[$agenda->mes];
            $data[]= $agenda->total;
        }
        $this->autoRender=false;
        $this->response->type('json');
        $json = json_encode(['dados'=>$data,'meses'=>$meses]);
        $this->response->body($json);
    }
}