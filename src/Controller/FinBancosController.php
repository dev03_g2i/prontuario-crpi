<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinBancos Controller
 *
 * @property \App\Model\Table\FinBancosTable $FinBancos
 */
class FinBancosController extends AppController
{

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Bancos');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finBancos = $this->FinBancos->find();

        $id = !empty($this->request->query(['banco'])) ? $this->request->query(['banco']) : null;
        
        if (isset($id)) $finBancos->where(['FinBancos.id' => $id]);

        $finBancos = $this->paginate($finBancos);

        $this->set(compact('finBancos'));
        $this->set('_serialize', ['finBancos']);
    }

    /**
     * View method
     *
     * @param string|null $id Fin Banco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finBanco = $this->FinBancos->get($id, [
            'contain' => ['FinBancoMovimentos', 'FinContabilidadeBancos', 'FinMovimentos']
        ]);

        $this->set('finBanco', $finBanco);
        $this->set('_serialize', ['finBanco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finBanco = $this->FinBancos->newEntity();
        if ($this->request->is('post')) {
            $newBank = $this->request->data;
            $newBank['situacao'] = 1;
            $finBanco = $this->FinBancos->patchEntity($finBanco, $newBank);
            if ($this->FinBancos->save($finBanco)) {
                $this->Flash->success(__('O banco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O banco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finBanco'));
        $this->set('_serialize', ['finBanco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Banco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finBanco = $this->FinBancos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newBank = $this->request->data;
            $newBank['situacao'] = 1;
            $finBanco = $this->FinBancos->patchEntity($finBanco, $newBank);
            if ($this->FinBancos->save($finBanco)) {
                $this->Flash->success(__('O banco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O banco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finBanco'));
        $this->set('_serialize', ['finBanco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Banco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finBanco = $this->FinBancos->get($id);
        $finBanco->situacao = 2;
        if ($this->FinBancos->save($finBanco)) {
            $this->Flash->success(__('O banco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O banco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Banco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinBancos->find('all')
        ->where(['FinBancos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinBancos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Banco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finBanco = $this->FinBancos->get($this->request->data['id']);
            $res = ['nome'=>$finBanco->nome,'id'=>$finBanco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
