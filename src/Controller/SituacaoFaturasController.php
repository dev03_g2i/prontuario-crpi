<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoFaturas Controller
 *
 * @property \App\Model\Table\SituacaoFaturasTable $SituacaoFaturas
 */
class SituacaoFaturasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
        ];
        $situacaoFaturas = $this->paginate($this->SituacaoFaturas);


        $this->set(compact('situacaoFaturas'));
        $this->set('_serialize', ['situacaoFaturas']);

    }

    /**
     * View method
     *
     * @param string|null $id Situacao Fatura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoFatura = $this->SituacaoFaturas->get($id, [
            'contain' => ['Users', 'SituacaoCadastros', 'AtendimentoProcedimentos']
        ]);

        $this->set('situacaoFatura', $situacaoFatura);
        $this->set('_serialize', ['situacaoFatura']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoFatura = $this->SituacaoFaturas->newEntity();
        if ($this->request->is('post')) {
            $situacaoFatura = $this->SituacaoFaturas->patchEntity($situacaoFatura, $this->request->data);
            if ($this->SituacaoFaturas->save($situacaoFatura)) {
                $this->Flash->success(__('O situacao fatura foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao fatura não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoFatura'));
        $this->set('_serialize', ['situacaoFatura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Fatura id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoFatura = $this->SituacaoFaturas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoFatura = $this->SituacaoFaturas->patchEntity($situacaoFatura, $this->request->data);
            if ($this->SituacaoFaturas->save($situacaoFatura)) {
                $this->Flash->success(__('O situacao fatura foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao fatura não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoFatura'));
        $this->set('_serialize', ['situacaoFatura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Fatura id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoFatura = $this->SituacaoFaturas->get($id);
                $situacaoFatura->situacao_id = 2;
        if ($this->SituacaoFaturas->save($situacaoFatura)) {
            $this->Flash->success(__('O situacao fatura foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao fatura não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Situacao Fatura id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->SituacaoFaturas->find('all')
        ->where(['SituacaoFaturas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('SituacaoFaturas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Situacao Fatura id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $situacaoFatura = $this->SituacaoFaturas->get($this->request->data['id']);
            $res = ['nome'=>$situacaoFatura->nome,'id'=>$situacaoFatura->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
