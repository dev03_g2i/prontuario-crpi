<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoPagamento Controller
 *
 * @property \App\Model\Table\TipoPagamentoTable $TipoPagamento
 */
class TipoPagamentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Status']
                        ,'conditions' => ['TipoPagamento.situacao_id = ' => '1']
                    ];
        $tipoPagamento = $this->paginate($this->TipoPagamento);

        $this->set(compact('tipoPagamento'));
        $this->set('_serialize', ['tipoPagamento']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Pagamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoPagamento = $this->TipoPagamento->get($id, [
            'contain' => ['Status', 'Contasreceber']
        ]);

        $this->set('tipoPagamento', $tipoPagamento);
        $this->set('_serialize', ['tipoPagamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoPagamento = $this->TipoPagamento->newEntity();
        if ($this->request->is('post')) {
            $tipoPagamento = $this->TipoPagamento->patchEntity($tipoPagamento, $this->request->data);
            if ($this->TipoPagamento->save($tipoPagamento)) {
                $this->Flash->success(__('O tipo pagamento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo pagamento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $status = $this->TipoPagamento->Status->find('list', ['limit' => 200]);
        $this->set(compact('tipoPagamento', 'status'));
        $this->set('_serialize', ['tipoPagamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Pagamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoPagamento = $this->TipoPagamento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoPagamento = $this->TipoPagamento->patchEntity($tipoPagamento, $this->request->data);
            if ($this->TipoPagamento->save($tipoPagamento)) {
                $this->Flash->success(__('O tipo pagamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo pagamento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $status = $this->TipoPagamento->Status->find('list', ['limit' => 200]);
        $this->set(compact('tipoPagamento', 'status'));
        $this->set('_serialize', ['tipoPagamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Pagamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoPagamento = $this->TipoPagamento->get($id);
                $tipoPagamento->situacao_id = 2;
        if ($this->TipoPagamento->save($tipoPagamento)) {
            $this->Flash->success(__('O tipo pagamento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo pagamento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function fill()
    {
        $this->loadModel('TipoPagamento');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->TipoPagamento->find('all')
            ->where(['TipoPagamento.status_id =' => 1,
                'TipoPagamento.descricao LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TipoPagamento.descricao');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->descricao);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function getregistro(){
        $this->loadModel('TipoPagamento');
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['res'=>'-1','msg'=>'Erro ao buscar registro'];


        if(!empty($this->request->query['id'])) {
            $plano = $this->TipoPagamento->get($this->request->query['id']);
            $res = ['res'=>$plano];
        }
        $this->response->body(json_encode($res));


    }

    public function getFormaPagamento($id)
    {
        $query = $this->TipoPagamento->get($id);
        $json = json_encode($query->tipo_documento_id);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

}
