<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NfPrestadores Controller
 *
 * @property \App\Model\Table\NfPrestadoresTable $NfPrestadores
 */
class NfPrestadoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TipoLogradouros', 'TipoBairros', 'Cidades', 'Tributacaos']
        ];
        $nfPrestadores = $this->paginate($this->NfPrestadores);


        $this->set(compact('nfPrestadores'));
        $this->set('_serialize', ['nfPrestadores']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf Prestadore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nfPrestadore = $this->NfPrestadores->get($id, [
            'contain' => ['TipoLogradouros', 'TipoBairros', 'Cidades', 'Tributacaos']
        ]);

        $this->set('nfPrestadore', $nfPrestadore);
        $this->set('_serialize', ['nfPrestadore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nfPrestadore = $this->NfPrestadores->newEntity();
        if ($this->request->is('post')) {
            $nfPrestadore = $this->NfPrestadores->patchEntity($nfPrestadore, $this->request->data);
            if ($this->NfPrestadores->save($nfPrestadore)) {
                $this->Flash->success(__('O nf prestadore foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf prestadore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfPrestadore'));
        $this->set('_serialize', ['nfPrestadore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf Prestadore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nfPrestadore = $this->NfPrestadores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nfPrestadore = $this->NfPrestadores->patchEntity($nfPrestadore, $this->request->data);
            if ($this->NfPrestadores->save($nfPrestadore)) {
                $this->Flash->success(__('O nf prestadore foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf prestadore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfPrestadore'));
        $this->set('_serialize', ['nfPrestadore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf Prestadore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nfPrestadore = $this->NfPrestadores->get($id);
                $nfPrestadore->situacao_id = 2;
        if ($this->NfPrestadores->save($nfPrestadore)) {
            $this->Flash->success(__('O nf prestadore foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf prestadore não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf Prestadore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->NfPrestadores->find('all')
        ->where(['NfPrestadores.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('NfPrestadores.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf Prestadore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nfPrestadore = $this->NfPrestadores->get($this->request->data['id']);
            $res = ['nome'=>$nfPrestadore->nome,'id'=>$nfPrestadore->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
