<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Acoes Controller
 *
 * @property \App\Model\Table\AcoesTable $Acoes
 */
class AcoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $acoes = $this->paginate($this->Acoes);


        $this->set(compact('acoes'));
        $this->set('_serialize', ['acoes']);

    }

    /**
     * View method
     *
     * @param string|null $id Aco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {

        $aco = $this->Acoes->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('aco', $aco);
        $this->set('_serialize', ['aco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $aco = $this->Acoes->newEntity();
        if ($this->request->is('post')) {
            $aco = $this->Acoes->patchEntity($aco, $this->request->data);
            if ($this->Acoes->save($aco)) {
                $this->Flash->success(__('O aco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O aco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('aco'));
        $this->set('_serialize', ['aco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Aco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $aco = $this->Acoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $aco = $this->Acoes->patchEntity($aco, $this->request->data);
            if ($this->Acoes->save($aco)) {
                $this->Flash->success(__('O aco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O aco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('aco'));
        $this->set('_serialize', ['aco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Aco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $aco = $this->Acoes->get($id);
                $aco->situacao_id = 2;
        if ($this->Acoes->save($aco)) {
            $this->Flash->success(__('O aco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O aco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Aco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Acoes->find('all')
        ->where(['Acoes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Acoes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Aco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $aco = $this->Acoes->get($this->request->data['id']);
            $res = ['nome'=>$aco->nome,'id'=>$aco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
