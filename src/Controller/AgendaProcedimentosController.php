<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AgendaProcedimentos Controller
 *
 * @property \App\Model\Table\AgendaProcedimentosTable $AgendaProcedimentos
 */
class AgendaProcedimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Agendas', 'SituacaoCadastros', 'Users']
        ];
        $agendaProcedimentos = $this->paginate($this->AgendaProcedimentos);


        $this->set(compact('agendaProcedimentos'));
        $this->set('_serialize', ['agendaProcedimentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Agenda Procedimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendaProcedimento = $this->AgendaProcedimentos->get($id, [
            'contain' => ['Agendas', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('agendaProcedimento', $agendaProcedimento);
        $this->set('_serialize', ['agendaProcedimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agendaProcedimento = $this->AgendaProcedimentos->newEntity();
        if ($this->request->is('post')) {
            $agendaProcedimento = $this->AgendaProcedimentos->patchEntity($agendaProcedimento, $this->request->data);
            if ($this->AgendaProcedimentos->save($agendaProcedimento)) {
                $this->Flash->success(__('O agenda procedimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O agenda procedimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('agendaProcedimento'));
        $this->set('_serialize', ['agendaProcedimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda Procedimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendaProcedimento = $this->AgendaProcedimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendaProcedimento = $this->AgendaProcedimentos->patchEntity($agendaProcedimento, $this->request->data);
            if ($this->AgendaProcedimentos->save($agendaProcedimento)) {
                $this->Flash->success(__('O agenda procedimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O agenda procedimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('agendaProcedimento'));
        $this->set('_serialize', ['agendaProcedimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda Procedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agendaProcedimento = $this->AgendaProcedimentos->get($id);
                $agendaProcedimento->situacao_id = 2;
        if ($this->AgendaProcedimentos->save($agendaProcedimento)) {
            $this->Flash->success(__('O agenda procedimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O agenda procedimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Agenda Procedimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AgendaProcedimentos->find('all')
        ->where(['AgendaProcedimentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AgendaProcedimentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Agenda Procedimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $agendaProcedimento = $this->AgendaProcedimentos->get($this->request->data['id']);
            $res = ['nome'=>$agendaProcedimento->nome,'id'=>$agendaProcedimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
