<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoDocumento Controller
 *
 * @property \App\Model\Table\TipoDocumentoTable $TipoDocumento
 */
class TipoDocumentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Status']
        ];
        $tipoDocumento = $this->paginate($this->TipoDocumento);


        $this->set(compact('tipoDocumento'));
        $this->set('_serialize', ['tipoDocumento']);

    }

    /**
     * View method
     *
     * @param string|null $id Tipo Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoDocumento = $this->TipoDocumento->get($id, [
            'contain' => ['Status', 'Contasreceber']
        ]);

        $this->set('tipoDocumento', $tipoDocumento);
        $this->set('_serialize', ['tipoDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoDocumento = $this->TipoDocumento->newEntity();
        if ($this->request->is('post')) {
            $tipoDocumento = $this->TipoDocumento->patchEntity($tipoDocumento, $this->request->data);
            if ($this->TipoDocumento->save($tipoDocumento)) {
                $this->Flash->success(__('O tipo documento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tipoDocumento'));
        $this->set('_serialize', ['tipoDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoDocumento = $this->TipoDocumento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoDocumento = $this->TipoDocumento->patchEntity($tipoDocumento, $this->request->data);
            if ($this->TipoDocumento->save($tipoDocumento)) {
                $this->Flash->success(__('O tipo documento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tipoDocumento'));
        $this->set('_serialize', ['tipoDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $tipoDocumento = $this->TipoDocumento->get($id);
                $tipoDocumento->situacao_id = 2;
        if ($this->TipoDocumento->save($tipoDocumento)) {
            $this->Flash->success(__('O tipo documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo documento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Tipo Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->TipoDocumento->find('all')
        ->where(['TipoDocumento.descricao LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TipoDocumento.descricao');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->descricao);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Tipo Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $tipoDocumento = $this->TipoDocumento->get($this->request->data['id']);
            $res = ['nome'=>$tipoDocumento->nome,'id'=>$tipoDocumento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
