<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EstadoCivis Controller
 *
 * @property \App\Model\Table\EstadoCivisTable $EstadoCivis
 */
class EstadoCivisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['EstadoCivis.situacao_id = ' => '1']
                    ];
        $estadoCivis = $this->paginate($this->EstadoCivis);

        $this->set(compact('estadoCivis'));
        $this->set('_serialize', ['estadoCivis']);
    }

    /**
     * View method
     *
     * @param string|null $id Estado Civi id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estadoCivi = $this->EstadoCivis->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('estadoCivi', $estadoCivi);
        $this->set('_serialize', ['estadoCivi']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $estadoCivi = $this->EstadoCivis->newEntity();
        if ($this->request->is('post')) {
            $estadoCivi = $this->EstadoCivis->patchEntity($estadoCivi, $this->request->data);
            if ($this->EstadoCivis->save($estadoCivi)) {
                $this->Flash->success(__('O estado civi foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estado civi não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->EstadoCivis->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->EstadoCivis->Users->find('list', ['limit' => 200]);
        $this->set(compact('estadoCivi', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['estadoCivi']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estado Civi id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $estadoCivi = $this->EstadoCivis->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estadoCivi = $this->EstadoCivis->patchEntity($estadoCivi, $this->request->data);
            if ($this->EstadoCivis->save($estadoCivi)) {
                $this->Flash->success(__('O estado civi foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estado civi não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->EstadoCivis->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->EstadoCivis->Users->find('list', ['limit' => 200]);
        $this->set(compact('estadoCivi', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['estadoCivi']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estado Civi id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $estadoCivi = $this->EstadoCivis->get($id);
                $estadoCivi->situacao_id = 2;
        if ($this->EstadoCivis->save($estadoCivi)) {
            $this->Flash->success(__('O estado civi foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O estado civi não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
