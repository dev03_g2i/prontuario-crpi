<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Controladores Controller
 *
 * @property \App\Model\Table\ControladoresTable $Controladores
 */
class ControladoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $controladores = $this->paginate($this->Controladores);


        $this->set(compact('controladores'));
        $this->set('_serialize', ['controladores']);

    }

    /**
     * View method
     *
     * @param string|null $id Controladore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $controladore = $this->Controladores->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('controladore', $controladore);
        $this->set('_serialize', ['controladore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $controladore = $this->Controladores->newEntity();
        if ($this->request->is('post')) {
            $controladore = $this->Controladores->patchEntity($controladore, $this->request->data);
            if ($this->Controladores->save($controladore)) {
                $this->Flash->success(__('O controladore foi salvo com sucesso!'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('O controladore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('controladore'));
        $this->set('_serialize', ['controladore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Controladore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $controladore = $this->Controladores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $controladore = $this->Controladores->patchEntity($controladore, $this->request->data);
            if ($this->Controladores->save($controladore)) {
                $this->Flash->success(__('O controladore foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O controladore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('controladore'));
        $this->set('_serialize', ['controladore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Controladore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $controladore = $this->Controladores->get($id);
                $controladore->situacao_id = 2;
        if ($this->Controladores->save($controladore)) {
            $this->Flash->success(__('O controladore foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O controladore não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Controladore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Controladores->find('all')
        ->where(['Controladores.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Controladores.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Controladore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $controladore = $this->Controladores->get($this->request->data['id']);
            $res = ['nome'=>$controladore->nome,'id'=>$controladore->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
