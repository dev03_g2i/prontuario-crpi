<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EstqTipoMovimento Controller
 *
 * @property \App\Model\Table\EstqTipoMovimentoTable $EstqTipoMovimento
 */
class EstqTipoMovimentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Situacaos']
        ];
        $estqTipoMovimento = $this->paginate($this->EstqTipoMovimento);


        $this->set(compact('estqTipoMovimento'));
        $this->set('_serialize', ['estqTipoMovimento']);

    }

    /**
     * View method
     *
     * @param string|null $id Estq Tipo Movimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estqTipoMovimento = $this->EstqTipoMovimento->get($id, [
            'contain' => ['Users', 'Situacaos']
        ]);

        $this->set('estqTipoMovimento', $estqTipoMovimento);
        $this->set('_serialize', ['estqTipoMovimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $estqTipoMovimento = $this->EstqTipoMovimento->newEntity();
        if ($this->request->is('post')) {
            $estqTipoMovimento = $this->EstqTipoMovimento->patchEntity($estqTipoMovimento, $this->request->data);
            if ($this->EstqTipoMovimento->save($estqTipoMovimento)) {
                $this->Flash->success(__('O estq tipo movimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq tipo movimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('estqTipoMovimento'));
        $this->set('_serialize', ['estqTipoMovimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estq Tipo Movimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $estqTipoMovimento = $this->EstqTipoMovimento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estqTipoMovimento = $this->EstqTipoMovimento->patchEntity($estqTipoMovimento, $this->request->data);
            if ($this->EstqTipoMovimento->save($estqTipoMovimento)) {
                $this->Flash->success(__('O estq tipo movimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq tipo movimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('estqTipoMovimento'));
        $this->set('_serialize', ['estqTipoMovimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estq Tipo Movimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $estqTipoMovimento = $this->EstqTipoMovimento->get($id);
                $estqTipoMovimento->situacao_id = 2;
        if ($this->EstqTipoMovimento->save($estqTipoMovimento)) {
            $this->Flash->success(__('O estq tipo movimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O estq tipo movimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Estq Tipo Movimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->EstqTipoMovimento->find('all')
        ->where(['EstqTipoMovimento.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('EstqTipoMovimento.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Estq Tipo Movimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $estqTipoMovimento = $this->EstqTipoMovimento->get($this->request->data['id']);
            $res = ['nome'=>$estqTipoMovimento->nome,'id'=>$estqTipoMovimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
