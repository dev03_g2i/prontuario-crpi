<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinContabilidades Controller
 *
 * @property \App\Model\Table\FinContabilidadesTable $FinContabilidades
 */
class FinContabilidadesController extends AppController
{

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Empresas');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {   
        $finContabilidades = $this->FinContabilidades->find();

        $id = !empty($this->request->query(['empresa'])) ? $this->request->query(['empresa']) : null;

        if (isset($id)) $finContabilidades->where(['FinContabilidades.id' => $id]);

        $finContabilidades = $this->paginate($finContabilidades);

        $this->set(compact('finContabilidades'));
        $this->set('_serialize', ['finContabilidades']);
    }

    /**
     * View method
     *
     * @param string|null $id Fin Contabilidade id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finContabilidade = $this->FinContabilidades->get($id, [
            'contain' => ['FinContabilidadeBancos', 'FinContasPagar', 'FinContasPagarPlanejamentos', 'FinContasReceberPlanejamentos', 'FinGrupoContabilidades', 'FinMovimentos', 'FinPlanejamentos']
        ]);

        $this->set('finContabilidade', $finContabilidade);
        $this->set('_serialize', ['finContabilidade']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finContabilidade = $this->FinContabilidades->newEntity();
        if ($this->request->is('post')) {
            $newContabilidade = $this->request->data;
            $newContabilidade['situacao'] = 1;
            $finContabilidade = $this->FinContabilidades->patchEntity($finContabilidade, $newContabilidade);
            if ($this->FinContabilidades->save($finContabilidade)) {
                $this->Flash->success(__('A empresa foi salva com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A empresa não foi salva. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finContabilidade'));
        $this->set('_serialize', ['finContabilidade']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Contabilidade id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finContabilidade = $this->FinContabilidades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newContabilidade = $this->request->data;
            $newContabilidade['situacao'] = 1;
            $finContabilidade = $this->FinContabilidades->patchEntity($finContabilidade, $newContabilidade);
            if ($this->FinContabilidades->save($finContabilidade)) {
                $this->Flash->success(__('A empresa foi salva com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A empresa não foi salva. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finContabilidade'));
        $this->set('_serialize', ['finContabilidade']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Contabilidade id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finContabilidade = $this->FinContabilidades->get($id);
        $finContabilidade->situacao = 2;
        if ($this->FinContabilidades->save($finContabilidade)) {
            $this->Flash->success(__('A empresa foi deletada com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! A empresa não foi deletada! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Contabilidade id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinContabilidades->find('all')
        ->where(['FinContabilidades.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinContabilidades.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Contabilidade id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finContabilidade = $this->FinContabilidades->get($this->request->data['id']);
            $res = ['nome'=>$finContabilidade->nome,'id'=>$finContabilidade->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
