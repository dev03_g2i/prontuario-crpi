<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoEquipegrau Controller
 *
 * @property \App\Model\Table\TipoEquipegrauTable $TipoEquipegrau
 */
class TipoEquipegrauController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    // public function index()
    // {
    //     $this->paginate = [
    //         'contain' => ['Situacaos']
    //     ];
    //     $tipoEquipegrau = $this->paginate($this->TipoEquipegrau);


    //     $this->set(compact('tipoEquipegrau'));
    //     $this->set('_serialize', ['tipoEquipegrau']);

    // }

    /**
     * View method
     *
     * @param string|null $id Tipo Equipegrau id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function view($id = null)
    // {
    //     $tipoEquipegrau = $this->TipoEquipegrau->get($id, [
    //         'contain' => ['Situacaos', 'AtendimentoProcedimentoEquipes']
    //     ]);

    //     $this->set('tipoEquipegrau', $tipoEquipegrau);
    //     $this->set('_serialize', ['tipoEquipegrau']);
    // }

    /**
     * View method
     *
     * @param string|null $id Tipo Equipegrau id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function findById($id) {
        $tipoEquipegrau = $this->TipoEquipegrau->get($id);
        $this->set(compact('tipoEquipegrau'));
        $this->set('_serialize', ['tipoEquipegrau']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $tipoEquipegrau = $this->TipoEquipegrau->newEntity();
    //     if ($this->request->is('post')) {
    //         $tipoEquipegrau = $this->TipoEquipegrau->patchEntity($tipoEquipegrau, $this->request->data);
    //         if ($this->TipoEquipegrau->save($tipoEquipegrau)) {
    //             $this->Flash->success(__('O tipo equipegrau foi salvo com sucesso!'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('O tipo equipegrau não foi salvo. Por favor, tente novamente.'));
    //         }
    //     }

    //     $this->set(compact('tipoEquipegrau'));
    //     $this->set('_serialize', ['tipoEquipegrau']);
    // }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Equipegrau id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $tipoEquipegrau = $this->TipoEquipegrau->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $tipoEquipegrau = $this->TipoEquipegrau->patchEntity($tipoEquipegrau, $this->request->data);
    //         if ($this->TipoEquipegrau->save($tipoEquipegrau)) {
    //             $this->Flash->success(__('O tipo equipegrau foi salvo com sucesso.'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('O tipo equipegrau não foi salvo. Por favor, tente novamente.'));
    //         }
    //     }

    //     $this->set(compact('tipoEquipegrau'));
    //     $this->set('_serialize', ['tipoEquipegrau']);
    // }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Equipegrau id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function delete($id = null)
    // {
    //     $tipoEquipegrau = $this->TipoEquipegrau->get($id);
    //             $tipoEquipegrau->situacao_id = 2;
    //     if ($this->TipoEquipegrau->save($tipoEquipegrau)) {
    //         $this->Flash->success(__('O tipo equipegrau foi deletado com sucesso.'));
    //     } else {
    //         $this->Flash->error(__('Desculpe! O tipo equipegrau não foi deletado! Tente novamente mais tarde.'));
    //     }
    //             return $this->redirect(['action' => 'index']);
    // }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Tipo Equipegrau id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->TipoEquipegrau->find('all')
        ->where(['TipoEquipegrau.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TipoEquipegrau.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Tipo Equipegrau id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $tipoEquipegrau = $this->TipoEquipegrau->get($this->request->data['id']);
            $res = ['nome'=>$tipoEquipegrau->nome,'id'=>$tipoEquipegrau->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
