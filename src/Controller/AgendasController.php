<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Network\Session;
use DateTime;
use DateInterval;

/**
 * Agendas Controller
 *
 * @property \App\Model\Table\AgendasTable $Agendas
 */
class AgendasController extends AppController
{

    public $components = array('Data', 'Json', 'Sms', 'Ics', 'Agenda', 'ConfiguracaoAgenda');

    public function lista()
    {
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');

        $inicio = date('d/m/Y');
        if (!empty($this->request->query('data'))) {
            $now = new DateTime($this->request->query('data'));
            $inicio = $now->format('d/m/Y');
        }

        $defaultView = null;
        if (!empty($this->request->query('defaultView'))) {
            $defaultView = $this->request->query('defaultView');
        }

        $grupo = null;
        if (!empty($this->request->query['grupos'])) {
            $grupo = $this->request->query['grupos'];
        }
        $title = "Agenda - Calendário";

        $this->set(compact('agenda', 'tipoAgendas', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'inicio', 'grupo', 'title', 'defaultView'));
        $this->set('_serialize', ['agenda']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.situacao_id = ' => '1']);

        if (empty($this->request->query('situacoes'))) {
            $query->andWhere(['SituacaoAgendas.ocultar' => 0]);
        }

        $fill = true;
        if (!empty($this->request->query['nome_provisorio'])) {
            $query->andWhere(function ($exp) {
                return $exp->or_([
                    'UPPER(Clientes.nome) LIKE' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%',
                    'UPPER(Agendas.nome_provisorio) LIKE ' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%'
                ]);
            });
            $fill = true;
        }

        if ($fill) {
            if (!empty($this->request->query['medicos'])) {
                $query->andWhere(['Agendas.profissional_id ' => $this->request->query['medicos']]);
            }

            $grupo = null;
            if (!empty($this->request->query['grupos'])) {
                $query->andWhere(['Agendas.grupo_id ' => $this->request->query['grupos']]);
                $grupo = $this->request->query['grupos'];
            } else {
                if (!empty($this->Auth->user('codigo_agenda'))) {
                    $grupo = $this->Auth->user('codigo_agenda');
                    $query->andWhere(['Agendas.grupo_id ' => $grupo]);
                }
            }

            if (!empty($this->request->query['periodo'])) {
                $periodo = $this->request->query['periodo'];
            } else {
                if (date('H') <= 12) {
                    $periodo = '1';
                } else {
                    $periodo = '2';
                }
            }
            if (!empty($this->request->query['inicio'])) {
                $inicio = new DateTime();
                $now = $inicio->createFromFormat('d/m/Y', $this->request->query['inicio']);

                if ($periodo == 2) {
                    $inicio = $now->format('Y-m-d') . " 12:00:00";
                } else {
                    $inicio = $now->format('Y-m-d') . " 01:00:00";
                }

                $query->andWhere(['Agendas.inicio >=' => $inicio]);
            } else {
                if ($periodo == 2) {
                    $inicio = date('Y-m-d') . ' 12:00:00';
                } else {
                    $inicio = date('Y-m-d') . ' 01:00:00';
                }
                $query->andWhere(['Agendas.inicio >=' => $inicio]);
            }


            if (!empty($this->request->query['fim'])) {
                $fim = new DateTime();
                $now = $fim->createFromFormat('d/m/Y', $this->request->query['fim']);
                if ($periodo == '1') {
                    $fim = $now->format('Y-m-d') . " 12:00:00";
                } else {
                    $fim = $now->format('Y-m-d') . " 23:59:00";
                }

                $query->andWhere(['Agendas.inicio <=' => $fim]);
            } else {
                if ($periodo == '1') {
                    $fim = date('Y-m-d') . ' 12:00:00';
                } else {
                    $fim = date('Y-m-d') . ' 23:59:00';
                }
                $query->andWhere(['Agendas.inicio <=' => $fim]);
            }

            if (!empty($this->request->query['tipos'])) {
                $query->andWhere(['Agendas.tipo_id ' => $this->request->query['tipos']]);
            }

            if (!empty($this->request->query['situacoes'])) {
                $query->andWhere(['Agendas.situacao_agenda_id IN' => $this->request->query['situacoes']]);
            }
        }
        $query->orderAsc('Agendas.inicio');
        $agendas = $this->paginate($query);


        $periodos = ['1' => 'Manhã', '2' => 'Tarde', '3' => 'Todos'];
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id' => 1])->orderAsc('MedicoResponsaveis.nome');;
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');

        $title = "Sala de Espera";
        $this->set(compact('agendas', 'tipoAgendas', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'inicio', 'fim', 'periodos', 'periodo', 'grupo', 'numero_clientes', 'title'));
        $this->set('_serialize', ['agendas']);

    }

    public function agendas()
    {
        $this->loadModel('ConfiguracaoPeriodosAgenda');
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.situacao_id = ' => '1']);

        if (empty($this->request->query('situacoes'))) {
            $query->andWhere(['SituacaoAgendas.ocultar' => 0]);
        }

        $fill = true;
        $nome_profisorio = null;
        if (!empty($this->request->query['nome_provisorio'])) {
            $query->andWhere(function ($exp) {
                return $exp->or_([
                    'UPPER(Clientes.nome) LIKE' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%',
                    'UPPER(Agendas.nome_provisorio) LIKE ' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%'
                ]);
            });
            $fill = false;
        }

        $grupo = null;
        if ($fill) {
            if (!empty($this->request->query['medicos'])) {
                $query->andWhere(['Agendas.profissional_id ' => $this->request->query['medicos']]);
            }

            if (!empty($this->request->query['grupos'])) {
                $query->andWhere(['Agendas.grupo_id ' => $this->request->query['grupos']]);
                $grupo = $this->request->query['grupos'];
            } else {
                if (!empty($this->Auth->user('codigo_agenda'))) {
                    $grupo = $this->Auth->user('codigo_agenda');
                    $query->andWhere(['Agendas.grupo_id ' => $grupo]);
                }
            }
            
            /** Filtro dos períodos modelo novo, tabela configuracao_periodos_agenda */
            if (!empty($this->request->getQuery('periodo'))) {
                $config_periodo = $this->ConfiguracaoPeriodosAgenda->get($this->request->getQuery('periodo'));
            } else {
                $config_periodo = $this->ConfiguracaoPeriodosAgenda->get(1);
            }

            if (!empty($this->request->getQuery('inicio'))) {
                $inicio = $this->Data->DataSQL($this->request->getQuery('inicio'));
            } else {
                $inicio = date('Y-m-d');
            }

            $i = $inicio . " " . $config_periodo->hora_inicial;
            $f = $inicio . " " . $config_periodo->hora_final;
            $query->andWhere(['Agendas.inicio >=' => $i, 'Agendas.inicio <=' => $f]);
            /** Fim novo filtro */        

            if (!empty($this->request->query['tipos'])) {
                $query->andWhere(['Agendas.tipo_id ' => $this->request->query['tipos']]);
            }

            if (!empty($this->request->query['situacoes'])) {
                $query->andWhere(['Agendas.situacao_agenda_id IN' => $this->request->query['situacoes']]);
            }
        }

        //$query->orderAsc('Agendas.inicio');
        $this->paginate = [
            'order' => ['Agendas.inicio asc'],
            'limit' => 20
        ];
        $agendas = $this->paginate($query);

        $transferencia = null;
        if (!empty($this->request->getQuery('transferencia'))) {
            $transferencia = $this->request->getQuery('transferencia');
        }
        $copiar = null;
        if (!empty($this->request->session()->read('copiar'))) {
            $copiar = $this->request->session()->read('copiar');
        }

        $periodos = $this->ConfiguracaoPeriodosAgenda->find('list');
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');

        $title = "Agenda - Lista";
        

        $this->set(compact('agendas', 'tipoAgendas', 'transferencia', 'copiar', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'inicio', 'fim', 'periodos', 'periodo', 'grupo', 'numero_clientes', 'title', 'nome_profisorio'));
        $this->set('_serialize', ['agendas']);
    }

    public function modal()
    {
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.situacao_id = ' => '1']);

        $nome_profisorio = null;
        if (!empty($this->request->query['nome_provisorio'])) {
            $nome_profisorio = $this->request->query['nome_provisorio'];
            $query->andWhere(function ($exp) {
                return $exp->or_([
                    'UPPER(Clientes.nome) LIKE' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%',
                    'UPPER(Agendas.nome_provisorio) LIKE ' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%'
                ]);
            });
        }


        $query->orderAsc('Agendas.inicio');
        $this->paginate = [
            'limit' => 10
        ];
        $agendas = $this->paginate($query);

        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');

        $title = "Agenda - Lista";

        $this->set(compact('agendas', 'tipoAgendas', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'title', 'nome_profisorio'));
        $this->set('_serialize', ['agendas']);

    }

    public function countClientes()
    {
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.situacao_id = ' => '1']);

        if (empty($this->request->query('situacoes'))) {
            $query->andWhere(['SituacaoAgendas.ocultar' => 0]);
        }

        $fill = true;
        if (!empty($this->request->query['nome_provisorio'])) {
            $query->andWhere(function ($exp) {
                return $exp->or_([
                    'UPPER(Clientes.nome) LIKE' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%',
                    'UPPER(Agendas.nome_provisorio) LIKE ' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%'
                ]);
            });
            $fill = true;
        }

        if ($fill) {
            if (!empty($this->request->query['medicos'])) {
                $query->andWhere(['Agendas.profissional_id ' => $this->request->query['medicos']]);
            }

            $grupo = null;
            if (!empty($this->request->query['grupos'])) {
                $query->andWhere(['Agendas.grupo_id ' => $this->request->query['grupos']]);
                $grupo = $this->request->query['grupos'];
            } else {
                if (!empty($this->Auth->user('codigo_agenda'))) {
                    $grupo = $this->Auth->user('codigo_agenda');
                    $query->andWhere(['Agendas.grupo_id ' => $grupo]);
                }
            }

            if (!empty($this->request->query['periodo'])) {
                $periodo = $this->request->query['periodo'];
            } else {
                if (date('H') <= 12) {
                    $periodo = '1';
                } else {
                    $periodo = '2';
                }
            }
            if (!empty($this->request->query['inicio'])) {
                $inicio = new DateTime();
                $now = $inicio->createFromFormat('d/m/Y', $this->request->query['inicio']);

                if ($periodo == 2) {
                    $inicio = $now->format('Y-m-d') . " 12:00:00";
                } else {
                    $inicio = $now->format('Y-m-d') . " 01:00:00";
                }

                $query->andWhere(['Agendas.inicio >=' => $inicio]);
            } else {
                if ($periodo == 2) {
                    $inicio = date('Y-m-d') . ' 12:00:00';
                } else {
                    $inicio = date('Y-m-d') . ' 01:00:00';
                }
                $query->andWhere(['Agendas.inicio >=' => $inicio]);
            }


            if (!empty($this->request->query['fim'])) {
                $fim = new DateTime();
                $now = $fim->createFromFormat('d/m/Y', $this->request->query['fim']);
                if ($periodo == '1') {
                    $fim = $now->format('Y-m-d') . " 12:00:00";
                } else {
                    $fim = $now->format('Y-m-d') . " 23:59:00";
                }

                $query->andWhere(['Agendas.inicio <=' => $fim]);
            } else {
                if ($periodo == '1') {
                    $fim = date('Y-m-d') . ' 12:00:00';
                } else {
                    $fim = date('Y-m-d') . ' 23:59:00';
                }
                $query->andWhere(['Agendas.inicio <=' => $fim]);
            }

            if (!empty($this->request->query['tipos'])) {
                $query->andWhere(['Agendas.tipo_id ' => $this->request->query['tipos']]);
            }

            if (!empty($this->request->query['situacoes'])) {
                $query->andWhere(['Agendas.situacao_agenda_id IN' => $this->request->query['situacoes']]);
            }
        }
        $query->groupBy('Agendas.cliente_id');

        $numero_clientes = $query->count();

        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($numero_clientes));

    }

    public function countAgendas()
    {
        $this->loadModel('ConfiguracaoPeriodosAgenda');
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.situacao_id = ' => '1']);

        if (empty($this->request->query('situacoes'))) {
            $query->andWhere(['SituacaoAgendas.ocultar' => 0]);
        }

        $fill = true;
        if (!empty($this->request->query['nome_provisorio'])) {
            $query->andWhere(function ($exp) {
                return $exp->or_([
                    'UPPER(Clientes.nome) LIKE' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%',
                    'UPPER(Agendas.nome_provisorio) LIKE ' => '%' . strtoupper($this->request->query['nome_provisorio']) . '%'
                ]);
            });
            $fill = true;
        }

        if ($fill) {

            if (!empty($this->request->query['medicos'])) {
                $query->andWhere(['Agendas.profissional_id ' => $this->request->query['medicos']]);
            }

            $grupo = null;
            if (!empty($this->request->query['grupos'])) {
                $query->andWhere(['Agendas.grupo_id ' => $this->request->query['grupos']]);
                $grupo = $this->request->query['grupos'];
            }

            /*if (!empty($this->request->query['periodo'])) {
                $periodo = $this->request->query['periodo'];
            } else {
                if (date('H') <= 12) {
                    $periodo = '1';
                } else {
                    $periodo = '2';
                }
            }

            if (!empty($this->request->query['inicio'])) {
                $inicio = new DateTime();
                $now = $inicio->createFromFormat('d/m/Y', $this->request->query['inicio']);

                if ($periodo == 1) {
                    $inicio = $now->format('Y-m-d') . " 01:00:00";
                    $fim = $now->format('Y-m-d') . " 12:00:00";
                } else if ($periodo == 2) {
                    $inicio = $now->format('Y-m-d') . " 12:00:00";
                    $fim = $now->format('Y-m-d') . " 23:59:00";
                } else {
                    $inicio = $now->format('Y-m-d') . " 01:00:00";
                    $fim = $now->format('Y-m-d') . " 23:59:00";
                }

                $query->andWhere(['Agendas.inicio >=' => $inicio]);
                $query->andWhere(['Agendas.inicio <=' => $fim]);


            } else {
                if ($periodo == 1) {
                    $inicio = date('Y-m-d') . ' 01:00:00';
                    $fim = date('Y-m-d') . ' 12:00:00';
                } else if ($periodo == 2) {
                    $inicio = date('Y-m-d') . ' 12:00:00';
                    $fim = date('Y-m-d') . ' 23:59:00';
                } else {
                    $inicio = date('Y-m-d') . ' 01:00:00';
                    $fim = date('Y-m-d') . ' 23:59:00';
                }
                $query->andWhere(['Agendas.inicio >=' => $inicio]);

                $query->andWhere(['Agendas.inicio <=' => $fim]);
            }*/
            /** Filtro dos períodos modelo novo, tabela configuracao_periodos_agenda */
            if (!empty($this->request->getQuery('periodo'))) {
                $config_periodo = $this->ConfiguracaoPeriodosAgenda->get($this->request->getQuery('periodo'));
            } else {
                $config_periodo = $this->ConfiguracaoPeriodosAgenda->get(1);
            }

            if (!empty($this->request->getQuery('inicio'))) {
                $inicio = $this->Data->DataSQL($this->request->getQuery('inicio'));
            } else {
                $inicio = date('Y-m-d');
            }

            $i = $inicio . " " . $config_periodo->hora_inicial;
            $f = $inicio . " " . $config_periodo->hora_final;
            $query->andWhere(['Agendas.inicio >=' => $i, 'Agendas.inicio <=' => $f]);
            /** Fim novo filtro */

            if (!empty($this->request->query['tipos'])) {
                $query->andWhere(['Agendas.tipo_id ' => $this->request->query['tipos']]);
            }

            if (!empty($this->request->query['situacoes'])) {
                $query->andWhere(['Agendas.situacao_agenda_id IN' => $this->request->query['situacoes']]);
            }
        }
        $query->groupBy('Agendas.cliente_id');

        $numero_clientes = $query->count();

        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($numero_clientes));

    }

    public function all()
    {
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros'])
            ->where(['Agendas.situacao_id = ' => '1']);


        if (!empty($this->request->query['cliente_id'])) {
            $query->andWhere(['Agendas.cliente_id' => $this->request->query['cliente_id']]);
        }


        $agendas = $this->paginate($query);

        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');


        $this->set(compact('agendas', 'tipoAgendas', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas'));
        $this->set('_serialize', ['agendas']);
    }

    /**
     * View method
     *
     * @param string|null $id Agenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('AgendaProcedimentos');
        $this->loadModel('AgendaCadprocedimentos');
        $this->loadModel('ConfiguracaoPeriodos');

        $agenda = $this->Agendas->get($id, [
            'contain' => ['Clientes', 'Atendimentos', 'UserEdit', 'Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data('agenda_procedimentos'))) {
                $agenda_cadprocedimento_id = $this->request->data('agenda_procedimentos');
                $this->AgendaProcedimentos->deleteAll(['agenda_id' => $id]);
                foreach ($agenda_cadprocedimento_id as $ap) {
                    $agendaProcedimento = $this->AgendaProcedimentos->newEntity();
                    $agendaProcedimento->agenda_id = $id;
                    $agendaProcedimento->agenda_cadprocedimento_id = $ap;
                    $this->AgendaProcedimentos->save($agendaProcedimento);
                }

                $procedimento = '';
                $agenda_procedimentos = $this->AgendaProcedimentos->find('all')
                    ->contain(['AgendaCadprocedimentos'])
                    ->where(['AgendaProcedimentos.agenda_id' => $id]);
                foreach ($agenda_procedimentos as $ap) {
                    $procedimento .= $ap->agenda_cadprocedimento->nome . ' | ';
                }
                $agenda->procedimento_concat = $procedimento . $this->request->data('procedimento');
                $this->Agendas->save($agenda);

            }

            if (!empty($this->request->data['inicio'])) {
                $inicio = new DateTime();
                $this->request->data['inicio'] = $inicio->createFromFormat('d/m/Y H:i', $this->request->data['inicio']);
            }
            if (!empty($this->request->data['fim'])) {
                $fim = new DateTime();
                $this->request->data['fim'] = $fim->createFromFormat('d/m/Y H:i', $this->request->data['fim']);
            }
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);

            $inicio = $this->request->data['inicio'];
            $inicio = new Time($inicio);
            $periodos = $this->ConfiguracaoPeriodos->find('all');
            foreach ($periodos as $p) {
                if ($inicio->format('H:i:s') >= $p->inicio_agenda->format('H:i:s') && $inicio->format('H:i:s') <= $p->fim_agenda->format('H:i:s')) {
                    $agenda->configuracao_periodo_id = $p->id;
                    break;
                }
            }

            $this->autoRender = false;
            $this->response->type('json');
            if ($this->Agendas->save($agenda)) {

                $agenda = $this->Agendas->get($agenda->id, ['contain' => ['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Convenios']]);

                /*$dados = [
                    'model'=>'Agendamento_alterado',
                    'id'=>$agenda->id,
                    'start'=>$agenda->inicio,
                    'descricao'=>'Agendamento com o DR(a) '.$agenda->grupo_agenda->nome,
                    'end'=>$agenda->fim,
                    'local'=>'Instituto Eduardo Ayub',
                    'titulo'=>'Agendamento Alterado',
                ];

                $this->Ics->send((object)$dados,[
                    ['email'=>'tfarias@aedu.com','nome'=>'tiago'],
                    ['email'=>'tfariasg3@gmail.com','nome'=>'tiago de farias'],
                    ['email'=>'tiaguitog3@hotmail.com','nome'=>'tiago de farias']
                ]);*/

                $inicio = $this->Data->data_iso($agenda->inicio);
                $fim = null;
                if (!empty($agenda->fim)) {
                    $fim = $this->Data->data_iso($agenda->fim);
                }

                if ($agenda->cliente_id == -1) {
                    //$title = $agenda->nome_provisorio . ". \n Tipo: " . $agenda->tipo_agenda->nome . ". \n Grupo:" . $agenda->grupo_agenda->nome . " . \n Fone: " . $agenda->fone_provisorio . ". \n Observação:" . $agenda->observacao . ".\n Procedimento:" . $agenda->procedimento . ".\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                    $title = $agenda->nome_provisorio . " \n " . $agenda->tipo_agenda->nome . " - " . $agenda->convenio->nome . "  \n Fone: " . $agenda->fone_provisorio . " \n Observação:" . $agenda->observacao . "\n Procedimento:" . $agenda->procedimento . "\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');
                } else {
                    //$title = "(" . $agenda->cliente->arquivo . ") " . $agenda->cliente->nome . ". \n Tipo: " . $agenda->tipo_agenda->nome . ". \n Grupo:" . $agenda->grupo_agenda->nome . " . \n Fone: " . $agenda->cliente->telefone . "/" . $agenda->cliente->celular . ". \n Profissão: " . $agenda->cliente->profissao . ".\n Observação:" . $agenda->observacao . ".\n Procedimento:" . $agenda->procedimento . ".\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                    $title = "(" . $agenda->cliente->arquivo . ") " . $agenda->cliente->nome . " \n  " . $agenda->tipo_agenda->nome . " - " . $agenda->convenio->nome . "  \n Fone: " . $agenda->cliente->telefone . "/" . $agenda->cliente->celular . "\n Observação:" . $agenda->observacao . "\n Procedimento:" . $agenda->procedimento . "\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                }

                $evento = array(
                    'id' => $agenda->id,
                    'title' => $title,
                    'start' => $inicio,
                    'end' => $fim,
                    'color' => !empty($agenda->situacao_agenda->cor) ? $agenda->situacao_agenda->cor : '#378006'
                );


                $json = json_encode(['result' => $evento]);
            } else {
                $json = json_encode(['result' => 'erro']);
            }
            $this->response->body($json);
        }

        $agendaProcedimentosSelecionados = [];
        $agendaProcedimentos = $this->AgendaProcedimentos->find('all')
            ->where(['AgendaProcedimentos.agenda_id' => $agenda->id])
            ->andWhere(['AgendaProcedimentos.situacao_id' => 1]);
        foreach ($agendaProcedimentos as $ap) {
            $agendaProcedimentosSelecionados[] = $ap->agenda_cadprocedimento_id;
        }

        $clientes = $this->Agendas->Clientes->find('all', ['conditions' => ['Clientes.id' => $agenda->cliente_id]])->first();
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');
        $convenios = $this->Agendas->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $agendaProcedimentos = $this->AgendaCadprocedimentos->find('list')->where(['AgendaCadprocedimentos.situacao_id' => 1])->orderAsc('AgendaCadprocedimentos.nome');

        $inicio = $this->Data->date_time($agenda->inicio);
        $fm = $this->Data->date_time($agenda->fim);
        $view = 'calendario';
        $action = 'view';

        $this->set(compact('agenda', 'tipoAgendas', 'view', 'action', 'edit', 'clientes', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'convenios', 'agendaProcedimentos', 'agendaProcedimentosSelecionados', 'inicio', 'fm'));
        $this->set('_serialize', ['agenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('ConfiguracaoPeriodos');
        $agenda = $this->Agendas->newEntity();
        if ($this->request->is('post')) {
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);
            $this->autoRender = false;
            $this->response->type('json');

            if ($this->Agendas->save($agenda)) {

                $this->loadModel('AgendaProcedimentos');
                if (!empty($this->request->data('agenda_procedimentos'))) {
                    $agenda_procedimentos = $this->request->data('agenda_procedimentos');
                    foreach ($agenda_procedimentos as $ap) {
                        $agendaProcedimento = $this->AgendaProcedimentos->newEntity();
                        $agendaProcedimento->agenda_id = $agenda->id;
                        $agendaProcedimento->agenda_cadprocedimento_id = $ap;
                        $this->AgendaProcedimentos->save($agendaProcedimento);
                    }

                    $procedimento = '';
                    $agenda_procedimentos = $this->AgendaProcedimentos->find('all')
                        ->contain(['AgendaCadprocedimentos'])
                        ->where(['AgendaProcedimentos.agenda_id' => $agenda->id]);
                    foreach ($agenda_procedimentos as $ap) {
                        $procedimento .= $ap->agenda_cadprocedimento->nome . ' | ';
                    }
                    $agenda->procedimento_concat = $procedimento . $agenda->procedimento;
                    $agenda->procedimento = $this->request->getData('procedimento');
                    $this->Agendas->save($agenda);

                }

                if (!empty($agenda->retorno_id)) {
                    $this->loadModel('Retornos');
                    $retorno = $this->Retornos->get($agenda->retorno_id, [
                        'contain' => [
                            'Clientes' => [
                                'ClienteResponsaveis' => [
                                    'GrauParentescos', 'conditions' => [
                                        'grau_id in' => ['1', '2']
                                    ]
                                ]
                            ], 'GrupoRetornos', 'SituacaoRetornos', 'Users', 'SituacaoCadastros']
                    ]);

                    $this->loadModel('LogRetornos');
                    $logRetorno = $this->LogRetornos->newEntity();
                    $logRetorno->retorno_id = $retorno->id;
                    $logRetorno->cliente_id = $retorno->cliente_id;
                    $logRetorno->grupo_id = $retorno->grupo_id;
                    $logRetorno->previsao = $retorno->previsao;
                    $logRetorno->dias = $retorno->dias;
                    $logRetorno->situacao_retorno_id = $retorno->situacao_retorno_id;
                    $logRetorno->user_id = $retorno->user_id;
                    $logRetorno->observacao = $retorno->observacao;
                    $logRetorno->justificativa = $retorno->justificativa;
                    $logRetorno->data_confirmacao = $retorno->data_confirmacao;
                    $logRetorno->quem_aprovou = $retorno->quem_aprovou;
                    $this->LogRetornos->save($logRetorno);

                    $retorno->situacao_retorno_id = 2;
                    $retorno->data_confirmacao = date('Y-m-d H:i:s');
                    $retorno->quem_aprovou = $this->Auth->user('id');
                    $this->Retornos->save($retorno);
                }
                $agenda = $this->Agendas->get($agenda->id, ['contain' => ['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Convenios']]);
                $inicio = $this->Data->data_iso($agenda->inicio);
                $fim = null;
                if (!empty($agenda->fim)) {
                    $fim = $this->Data->data_iso($agenda->fim);
                }
                $mensagem = "Instituto Eduardo Ayub. Foi feito um agendamento para o dia " . $this->Data->data_sms($agenda->inicio);
                $mensagem .= " às " . $this->Data->hora_sms($agenda->inicio) . " com o DR(a) " . $agenda->grupo_agenda->nome;

                $dados = [
                    'model' => 'Agendamento',
                    'id' => $agenda->id,
                    'start' => $agenda->inicio,
                    'descricao' => 'Agendamento com o DR(a) ' . $agenda->grupo_agenda->nome,
                    'end' => $agenda->fim,
                    'local' => 'Instituto Eduardo Ayub',
                    'titulo' => 'Agendamento',
                ];

                $title = "";
                $nome_paciente = ($agenda->cliente_id == -1) ? $agenda->nome_provisorio : $agenda->cliente->nome;
                if ($agenda->cliente_id == -1) {
                    $title = $nome_paciente . " " . $agenda->tipo_agenda->nome . " - " . $agenda->convenio->nome . " Fone: " . $agenda->fone_provisorio . " Observação:" . $agenda->observacao . " Procedimento:" . $agenda->procedimento_concat . " Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                } else {                    
                    $title = $agenda->cliente->id . " - " . $nome_paciente . " (" . $this->Data->calculaIdade($agenda->cliente->nascimento) . " A) - " . $agenda->tipo_agenda->nome . " - " . $agenda->convenio->nome . " \n Fone: " . $agenda->cliente->telefone . "/" . $agenda->cliente->celular . "\n Observação:" . $agenda->observacao . "\n Procedimento:" . $agenda->procedimento_concat . "\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                }
                $evento = array(
                    'id' => $agenda->id,
                    'title' => $title,
                    'start' => $inicio,
                    'end' => $fim,
                    'color' => !empty($agenda->situacao_agenda->cor) ? $agenda->situacao_agenda->cor : '#378006'
                );
                $json = json_encode(['result' => $evento]);
            } else {
                $json = json_encode(['result' => 'erro']);
            }
            $this->response->body($json);
        }
        $inicio = null;
        $fm = null;
        if (!empty($this->request->query['data'])) {
            $dt = $this->request->query['data'];

            $data = new Time($dt);
            $inicio = $data->format('d/m/Y H:i');

            if (!empty($this->request->query['group'])) {
                $grupoAgenda = $this->Agendas->GrupoAgendas->findById($this->request->query['group'])->first();
                if (!empty($grupoAgenda->intervalo)) {
                    $minutos = $this->Data->getMinutos($grupoAgenda->intervalo);
                    $f = $data->addMinutes($minutos);
                    $fm = $f->format('d/m/Y H:i');
                } else {
                    $f = $data->addHour(1);
                    $fm = $f->format('d/m/Y H:i');
                }
            } else {
                $f = $data->addHour(1);
                $fm = $f->format('d/m/Y H:i');
            }
        }

        if(!empty($this->request->getQuery('end')))
        {
            $fm = new Time($this->request->getQuery('end'));
            $fm = $fm->format('d/m/Y H:i');
        }

        $cliente = null;
        if (!empty($this->request->query['cliente_id'])) {
            $this->loadModel('Clientes');
            $cliente = $this->Clientes->get($this->request->query['cliente_id']);
        }

        $medico = null;
        if (!empty($this->request->query['medico_id'])) {
            $medico = $this->request->query['medico_id'];
        }

        $group = null;
        if (!empty($this->request->query['group'])) {
            $group = $this->request->query['group'];
        } else {
            if (!empty($this->Auth->user('codigo_agenda'))) {
                $group = $this->Auth->user('codigo_agenda');
            }
        }

        /**
         * Vem do calendario
         */
        $tipo_agenda_calendario = null;
        if(!empty($this->request->getQuery('tipo_agenda')))
        {
            $tipo_agenda_calendario = $this->request->getQuery('tipo_agenda');
        }

        $this->loadModel('AgendaCadprocedimentos');

        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->where(['TipoAgendas.situacao_id' => 1])->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id' => 1])->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->where(['SituacaoAgendas.situacao_id' => 1])->orderAsc('SituacaoAgendas.nome');
        $convenios = $this->Agendas->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $agendaProcedimentos = $this->AgendaCadprocedimentos->find('list')->where(['AgendaCadprocedimentos.situacao_id' => 1])->orderAsc('AgendaCadprocedimentos.nome');
        $view = 'calendario';
        $action = 'add';

        $this->set(compact('periodos', 'agenda', 'view', 'action', 'tipoAgendas', 'cliente', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'inicio', 'medico', 'fm', 'group', 'convenios', 'agendaProcedimentos', 'tipo_agenda_calendario'));
        $this->set('_serialize', ['agenda']);
    }

    public function validationAgenda()
    {
        $this->response->type('json');
        $inicio = new Time($this->request->data['inicio']);
        $fim = new Time($this->request->data['fim']);
        $validation = ['res' => 0];

        // Verificacao feita paras as telas de editar
        $agenda_id = null;
        if (!empty($this->request->data['agenda_id'])) {
            $agenda_id = $this->request->data['agenda_id'];
        }
        $checa_agendamento = $this->Agendas->checkAgendamento($this->request->data['grupo_id'], $inicio->format('Y-m-d H:i') . ':00', $agenda_id, false);

        // checa configuracao dos horarios inicio e fim
        $this->loadModel('GrupoAgendas');
        $checa_config = $this->GrupoAgendas->find()
            ->where(['GrupoAgendas.id' => $this->request->data['grupo_id']])
            ->andWhere([
                'GrupoAgendas.situacao_id' => 1,
                'AND' => [
                    ['GrupoAgendas.inicio <= ' => $inicio->format('H:i') . ':00'],
                    ['GrupoAgendas.fim >= ' => $fim->format('H:i') . ':00']
                ]
            ])
            ->limit(1)
            ->count();

        // checa bloqueio e traz os dados da agenda
        $checa_bloqueio = $this->Agendas->find()
            ->where(['Agendas.grupo_id' => $this->request->data['grupo_id']])
            ->andWhere([
                'Agendas.situacao_agenda_id' => 1000,
                'Agendas.situacao_id' => 1,
                'AND' => [
                    ['Agendas.inicio <= ' => $inicio->format('Y-m-d H:i') . ':00'],
                    ['Agendas.fim >= ' => $fim->format('Y-m-d H:i') . ':00']
                ]
            ])
            ->limit(1)
            ->count();

        $agenda = $this->Agendas->find('all')
            ->contain(['Users'])
            ->where(['Agendas.grupo_id' => $this->request->data['grupo_id']])
            ->andWhere([
                'Agendas.situacao_agenda_id' => 1000,
                'Agendas.situacao_id' => 1,
                'AND' => [
                    ['Agendas.inicio <= ' => $inicio->format('Y-m-d H:i') . ':00'],
                    ['Agendas.fim >= ' => $fim->format('Y-m-d H:i') . ':00']
                ]
            ])->first();

        if (!$checa_agendamento) {
            $validation = ['res' => 1, 'msg' => 'Já existe um agendamento marcado para este horário!', 'erro' => 'agendamento'];
        }
        if ($checa_config == 0) {
            $validation = ['res' => $checa_config, 'msg' => 'Não é permitido agendamento fora do horário inicial e/ou final da configuração da agenda!', 'erro' => 'config_horario'];
        }
        if ($checa_bloqueio > 0) {
            $validation = ['res' => $checa_bloqueio, 'msg' => 'Este horário esta bloqueado: ' . $agenda->nome_provisorio . '<br> Usuário: ' . $agenda->user->nome . ' - ' . $agenda->created, 'erro' => 'bloqueio'];
        }

        $this->response->body(json_encode($validation));
        $this->autoRender = false;
    }


    public function nadd()
    {
        $agenda = $this->Agendas->newEntity();
        if ($this->request->is('post')) {
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);
            $this->autoRender = false;
            $this->response->type('json');

            if ($this->Agendas->save($agenda)) {
                $this->Flash->success(__('Agenda cadastrada com sucesso.'));
            } else {
                $this->Flash->error(__('Desculpe! Agenda não cadastrada! Tente novamente mais tarde.'));
            }

            return $this->redirect(['action' => 'index']);
        }
        $inicio = null;
        $fm = null;
        if (!empty($this->request->query['data'])) {
            $dt = $this->request->query['data'];

            $data = new Time($dt);

            $inicio = $data->format('d/m/Y H:i');
            $f = $data->addHour(1);
            $fm = $f->format('d/m/Y H:i');
        }

        $cliente = null;
        if (!empty($this->request->query['cliente_id'])) {
            $this->loadModel('Clientes');
            $cliente = $this->Clientes->get($this->request->query['cliente_id']);
        }

        $medico = null;
        if (!empty($this->request->query['medico_id'])) {
            $medico = $this->request->query['medico_id'];
        }

        $group = null;
        if (!empty($this->request->query['group'])) {
            $group = $this->request->query['group'];
        }

        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');
        $convenios = $this->Agendas->Convenios->find('list')->orderAsc('Convenios.nome');

        $this->set(compact('agenda', 'tipoAgendas', 'cliente', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'inicio', 'medico', 'fm', 'group', 'convenios'));
        $this->set('_serialize', ['agenda']);
    }

    public function newAdd()
    {
        $agenda = $this->Agendas->newEntity();
        if ($this->request->is('post')) {
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);
            $this->autoRender = false;
            $this->response->type('json');

            $check = $this->Agendas->checkAgendamento($this->request->data['grupo_id'], $this->Data->DateTimeSql($this->request->data['inicio']), null, false);

            if (!$check) {
                $res = ['res' => 2, 'msg' => 'Já existe um agendamento para este horário!'];
            } else {
                $res = ['res' => 2, 'msg' => 'Erro ao salvar registro!'];
                if ($this->Agendas->save($agenda)) {

                    $this->loadModel('AgendaProcedimentos');
                    if (!empty($this->request->data('agenda_procedimentos'))) {
                        $agenda_procedimentos = $this->request->data('agenda_procedimentos');
                        foreach ($agenda_procedimentos as $ap) {
                            $agendaProcedimento = $this->AgendaProcedimentos->newEntity();
                            $agendaProcedimento->agenda_id = $agenda->id;
                            $agendaProcedimento->agenda_cadprocedimento_id = $ap;
                            $this->AgendaProcedimentos->save($agendaProcedimento);
                        }

                        $procedimento = '';
                        $agenda_procedimentos = $this->AgendaProcedimentos->find('all')
                            ->contain(['AgendaCadprocedimentos'])
                            ->where(['AgendaProcedimentos.agenda_id' => $agenda->id]);
                        foreach ($agenda_procedimentos as $ap) {
                            $procedimento .= $ap->agenda_cadprocedimento->nome . ' | ';
                        }
                        $agenda->procedimento_concat = $procedimento . $agenda->procedimento;
                        $agenda->procedimento = $this->request->getData('procedimento');
                        $this->Agendas->save($agenda);

                    }
                    $res = ['res' => 1, 'msg' => 'Salvo com sucesso!'];
                }
            }

            $this->autoRender = false;
            $this->response->type('json');
            $this->response->body(json_encode($res));
        }
        $inicio = null;
        $fm = null;
        if (!empty($this->request->query['data'])) {
            $dt = $this->request->query['data'];

            $data = new Time($dt);

            $inicio = $data->format('d/m/Y H:i');
            $f = $data->addHour(1);
            $fm = $f->format('d/m/Y H:i');
        }

        $cliente = null;
        if (!empty($this->request->query['cliente_id'])) {
            $this->loadModel('Clientes');
            $cliente = $this->Clientes->get($this->request->query['cliente_id']);
        }

        $medico = null;
        if (!empty($this->request->query['medico_id'])) {
            $medico = $this->request->query['medico_id'];
        }

        $group = null;
        if (!empty($this->request->query['group'])) {
            $group = $this->request->query['group'];
        } else {
            $group = $this->Auth->user('codigo_agenda');
        }

        $this->loadModel('AgendaCadprocedimentos');
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->where(['TipoAgendas.situacao_id' => 1])->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id' => 1])->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->where(['SituacaoAgendas.situacao_id' => 1])->orderAsc('SituacaoAgendas.nome');
        $convenios = $this->Agendas->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $agendaProcedimentos = $this->AgendaCadprocedimentos->find('list')->where(['AgendaCadprocedimentos.situacao_id' => 1])->orderAsc('AgendaCadprocedimentos.nome');
        $view = 'lista';
        $action = 'add';

        $this->set(compact('agenda', 'tipoAgendas', 'view', 'cliente', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'inicio', 'medico', 'fm', 'group', 'convenios', 'agendaProcedimentos', 'action'));
        $this->set('_serialize', ['agenda']);
    }

    public function addHorarios()
    {
        $agenda = $this->Agendas->newEntity();
        if ($this->request->is('post')) {
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);
        }

        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->where(['TipoAgendas.situacao_id' => 1])->orderAsc('TipoAgendas.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->where(['SituacaoAgendas.situacao_id' => 1])->orderAsc('SituacaoAgendas.nome');

        $this->set(compact('agenda', 'tipoAgendas', 'grupoAgendas', 'situacaoAgendas'));
        $this->set('_serialize', ['agenda']);
    }

    public function saveHorarios()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $agenda = $this->Agendas->newEntity();
        if ($this->request->is('post')) {

            /*$this->loadModel('GrupoAgendaHorarios');
            $date = new Time();
            $dia_semana = $date->format('w');
            $grupo_agenda_horario = $this->GrupoAgendaHorarios->newEntity();
            $grupo_agenda_horario->dia_semana = $dia_semana;
            $grupo_agenda_horario->data_inicio = date('Y-m-d');
            $grupo_agenda_horario->data_fim = date('Y-m-d');
            $grupo_agenda_horario->hora_inicio = date('H:i:s');
            $grupo_agenda_horario->hora_fim = date('H:i:s');
            $grupo_agenda_horario->intervalo = '00' . date(':i:s');
            $grupo_agenda_horario->grupo_id = $this->request->getData('grupo_id');
            $grupo_agenda_horario->tipo_agenda_id = $this->request->getData('tipo_id');
            $grupo_agenda_horario->operacao_agenda_horario_id = 1;//Gerar horarios
            $this->GrupoAgendaHorarios->save($grupo_agenda_horario);*/
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);
            $agenda->cliente_id = -1; // Cliente provisorio
            $agenda->situacao_agenda_id = 5;// Agendado
            $agenda->convenio_id = -1; // Sistema
            $agenda->horario_individual = 1;
            //$agenda->agenda_horario_id = $grupo_agenda_horario->id; // Sistema

            if ($this->Agendas->save($agenda)) {
                $dados = $this->Agendas->get($agenda->id, [
                    'contain' => ['GrupoAgendas', 'TipoAgendas']
                ]);
                $res = ['id' => $dados->id, 'agenda' => $dados->grupo_agenda->nome, 'tipo' => $dados->tipo_agenda->nome, 'inicio' => $this->request->data('inicio'), 'fim' => $this->request->data('fim'), 'obs' => $dados->observacao];
            } else {
                $res = 'error';
            }

        }

        $this->response->body(json_encode($res));
    }

    public function deleteModal()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $agenda = $this->Agendas->get($this->request->data['id']);
        $agenda->situacao_id = 2;
        if ($this->Agendas->save($agenda)) {
            $res = 0;
        } else {
            $res = 1;
        }

        $this->response->body(json_encode(['res' => $res, 'view' => 'index']));
    }

    public function eventos()
    {
        $query = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.situacao_id = ' => '1']);

        if (!empty($this->request->data['medicos'])) {
            if (is_array($this->request->data['medicos'])) {
                $medico = [];
                foreach ($this->request->data['medicos'] as $c) {
                    $medico[] = $c;
                }
                $query->andWhere(['Agendas.profissional_id IN' => $medico]);
            }
        }
        $ch_gr = 0;
        $grupo_agenda_id = null;
        if (!empty($this->request->data['group_aux'])) {
            $query->andWhere(['Agendas.grupo_id' => $this->request->data['group_aux']]);
            $ch_gr = 1;
            $grupo_agenda_id = $this->request->data['group_aux'];
        } else {
            if (!empty($this->request->data['grupos'])) {
                if (is_array($this->request->data['grupos'])) {
                    $grup = [];
                    foreach ($this->request->data['grupos'] as $c) {
                        $grup[] = $c;
                    }
                    $grupo_agenda_id = $grup;
                    $query->andWhere(['Agendas.grupo_id IN' => $grup]);
                }
            }
        }

        if (empty($this->request->data['startParam'])) {
            $inicio = new DateTime();
            $now = $inicio->createFromFormat('d/m/Y', $this->request->data['start']);
            if ($ch_gr != 1) {
                $start = $now->format('Y-m-') . '01 05:00:00';
            } else {
                $now->sub(new DateInterval('P1D'));
                $start = $now->format('Y-m-d');
            }

            $query->andWhere(['Agendas.inicio >=' => $start]);

            if ($ch_gr != 1) {
                $now->add(new DateInterval('P7D'));
                $fim = $now->format('Y-m-') . date('t') . ' 20:00:00';
                $query->andWhere(['Agendas.inicio <=' => $fim]);

            } else {
                $fim = $now->add(new DateInterval('P3D'));
                $query->andWhere(['Agendas.inicio <=' => $fim->format('Y-m-d')]);
            }

        } else {
            $query->andWhere(['Agendas.inicio >=' => $this->request->data['startParam']]);
            $query->andWhere(['Agendas.inicio <=' => $this->request->data['endParam']]);
        }

        if (!empty($this->request->data['tipos'])) {
            if (is_array($this->request->data['tipos'])) {
                $tip = [];
                foreach ($this->request->data['tipos'] as $c) {
                    $tip[] = $c;
                }
                $query->andWhere(['Agendas.tipo_id IN' => $tip]);
            }
        }

        if (!empty($this->request->data['situacoes'])) {
            $query->andWhere(['Agendas.situacao_agenda_id IN' => $this->request->data['situacoes']]);
        } else {
            $query->andWhere(['SituacaoAgendas.ocultar' => 0]);
        }

        $data = $this->Data->DataSQL($this->request->getData('start'));
        $grid = $this->Agenda->mountVirtualGride($data, $grupo_agenda_id);        

        $evento = array();
        foreach ($query as $e) {
            $inicio = $this->Data->data_iso($e->inicio);
            $fim = null;
            if (!empty($e->fim)) {
                $fim = $this->Data->data_iso($e->fim);
            }

            $title = "";
            $nome_paciente = ($e->cliente_id == -1) ? $e->nome_provisorio : $e->cliente->nome;
            if ($e->convenio_id == '-1' || $e->situacao_agenda_id == 1000) {/*Bloqueio*/
                $title = $nome_paciente;
            }elseif ($e->cliente_id == '-1') {
                $title = $nome_paciente . " - " . $e->tipo_agenda->nome . " - " . $e->convenio->nome . " Fone: " . $e->fone_provisorio . " Observação:" . $e->observacao . " Procedimento:" . $e->procedimento_concat . " Última atualização: " . date_format($e->modified, 'd/m/Y H:i');
            }else {
                $title = $e->cliente->id . " - " . $nome_paciente . " (" . $this->Data->calculaIdade($e->cliente->nascimento) . " A) - " . $e->tipo_agenda->nome . " - " . $e->convenio->nome . " - Procedimento:" . $e->procedimento_concat . " -  Fone: " . $e->cliente->telefone . "/" . $e->cliente->celular . "\n Observação:" . $e->observacao . "\n Última atualização: " . date_format($e->modified, 'd/m/Y H:i');
            }

            // Verifica SMS @param Código do Status e Texto da Resposta
            $sms = $this->Sms->verificaSms($e->sms_situacao, $e->sms, $e->cliente_id);

            $key = array_search($inicio, array_column($grid, 'start'));
            if(!empty($key) || strval($key) === '0') {
                $grid[$key]['id'] = $e->id;
                $grid[$key]['title'] = $title;
                $grid[$key]['start'] = $inicio;
                $grid[$key]['end'] = $fim;
                $grid[$key]['icon'] = !empty($sms) ? $sms['icon'] : '';
                $grid[$key]['sms'] = !empty($sms) ? $sms['msg'] : '';
                $grid[$key]['tipo'] = !empty($sms) ? $sms['tipo'] : ''; 
                $grid[$key]['backgroundColor'] = !empty($e->situacao_agenda->cor) ? $e->situacao_agenda->cor : '#378006'; 
                $grid[$key]['event_empty'] = false;
            }else {
                $grid[] = [
                    'id' => $e->id,
                    'title' => $title,
                    'start' => $inicio,
                    'end' => $fim,
                    'backgroundColor' => !empty($e->situacao_agenda->cor) ? $e->situacao_agenda->cor : '#378006',
                    'icon' => !empty($sms) ? $sms['icon'] : '',
                    'sms' => !empty($sms) ? $sms['msg'] : '',
                    'tipo' => !empty($sms) ? $sms['tipo'] : '',
                    'event_empty' => false,
                ];
            }

            /*$evento[] = array(
                'id' => $e->id,
                'title' => $title,
                'start' => $inicio,
                'end' => $fim,
                'color' => !empty($e->situacao_agenda->cor) ? $e->situacao_agenda->cor : '#378006',
                'icon' => !empty($sms) ? $sms['icon'] : '',//($e->cliente_id == -1) ? '' : 'check'
                'sms' => !empty($sms) ? $sms['msg'] : '',
                'tipo' => !empty($sms) ? $sms['tipo'] : ''
            );*/

        }

        $this->autoRender = false;
        $this->response->type('json');
        $json = json_encode($grid);
        $this->response->body($json);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('AgendaProcedimentos');
        $this->loadModel('AgendaCadprocedimentos');
        $this->loadModel('ConfiguracaoPeriodos');

        $agenda = $this->Agendas->get($id, [
            'contain' => ['Clientes', 'Atendimentos', 'UserEdit', 'Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (!empty($this->request->data('agenda_procedimentos'))) {
                $agenda_cadprocedimento_id = $this->request->data('agenda_procedimentos');
                $this->AgendaProcedimentos->deleteAll(['agenda_id' => $id]);
                foreach ($agenda_cadprocedimento_id as $ap) {
                    $agendaProcedimento = $this->AgendaProcedimentos->newEntity();
                    $agendaProcedimento->agenda_id = $id;
                    $agendaProcedimento->agenda_cadprocedimento_id = $ap;
                    $this->AgendaProcedimentos->save($agendaProcedimento);
                }

                $procedimento = '';
                $agenda_procedimentos = $this->AgendaProcedimentos->find('all')
                    ->contain(['AgendaCadprocedimentos'])
                    ->where(['AgendaProcedimentos.agenda_id' => $id]);
                foreach ($agenda_procedimentos as $ap) {
                    $procedimento .= $ap->agenda_cadprocedimento->nome . ' | ';
                }
                $agenda->procedimento_concat = $procedimento . $this->request->data('procedimento');
                $agenda->procedimento = $this->request->data('procedimento');
                //$this->Agendas->save($agenda);

            }

            if (!empty($this->request->data['inicio'])) {
                $inicio = new DateTime();
                $this->request->data['inicio'] = $inicio->createFromFormat('d/m/Y H:i', $this->request->data['inicio']);
            }
            if (!empty($this->request->data['fim'])) {
                $fim = new DateTime();
                $this->request->data['fim'] = $fim->createFromFormat('d/m/Y H:i', $this->request->data['fim']);
            }
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);

            /*$inicio = $this->request->data['inicio'];
            $inicio = new Time($inicio);
            $periodos = $this->ConfiguracaoPeriodos->find('all');
            foreach ($periodos as $p) {
                if ($inicio->format('H:i:s') >= $p->inicio_agenda->format('H:i:s') && $inicio->format('H:i:s') <= $p->fim_agenda->format('H:i:s')) {
                    $agenda->configuracao_periodo_id = $p->id;
                    break;
                }
            }*/

            $this->autoRender = false;
            $this->response->type('json');
            if ($this->Agendas->save($agenda)) {

                $agenda = $this->Agendas->get($agenda->id, ['contain' => ['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Convenios']]);

                /*$dados = [
                    'model'=>'Agendamento_alterado',
                    'id'=>$agenda->id,
                    'start'=>$agenda->inicio,
                    'descricao'=>'Agendamento com o DR(a) '.$agenda->grupo_agenda->nome,
                    'end'=>$agenda->fim,
                    'local'=>'Instituto Eduardo Ayub',
                    'titulo'=>'Agendamento Alterado',
                ];

                $this->Ics->send((object)$dados,[
                    ['email'=>'tfarias@aedu.com','nome'=>'tiago'],
                    ['email'=>'tfariasg3@gmail.com','nome'=>'tiago de farias'],
                    ['email'=>'tiaguitog3@hotmail.com','nome'=>'tiago de farias']
                ]);*/

                $inicio = $this->Data->data_iso($agenda->inicio);
                $fim = null;
                if (!empty($agenda->fim)) {
                    $fim = $this->Data->data_iso($agenda->fim);
                }

                $nome_paciente = ($agenda->cliente_id == -1) ? $agenda->nome_provisorio : $agenda->cliente->nome;
                if ($agenda->cliente_id == -1) {
                    //$title = $agenda->nome_provisorio . ". \n Tipo: " . $agenda->tipo_agenda->nome . ". \n Grupo:" . $agenda->grupo_agenda->nome . " . \n Fone: " . $agenda->fone_provisorio . ". \n Observação:" . $agenda->observacao . ".\n Procedimento:" . $agenda->procedimento . ".\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                    $title = $nome_paciente . " \n " . $agenda->tipo_agenda->nome . " - " . $agenda->convenio->nome . "  \n Fone: " . $agenda->fone_provisorio . " \n Observação:" . $agenda->observacao . "\n Procedimento:" . $agenda->procedimento . "\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                } else {
                    //$title = "(" . $agenda->cliente->arquivo . ") " . $agenda->cliente->nome . ". \n Tipo: " . $agenda->tipo_agenda->nome . ". \n Grupo:" . $agenda->grupo_agenda->nome . " . \n Fone: " . $agenda->cliente->telefone . "/" . $agenda->cliente->celular . ". \n Profissão: " . $agenda->cliente->profissao . ".\n Observação:" . $agenda->observacao . ".\n Procedimento:" . $agenda->procedimento . ".\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                    $title = "(" . $agenda->cliente->arquivo . ") " . $nome_paciente . " \n  " . $agenda->tipo_agenda->nome . " - " . $agenda->convenio->nome . "  \n Fone: " . $agenda->cliente->telefone . "/" . $agenda->cliente->celular . "\n Observação:" . $agenda->observacao . "\n Procedimento:" . $agenda->procedimento . "\n Última atualização: " . date_format($agenda->modified, 'd/m/Y H:i');;
                }

                $evento = array(
                    'id' => $agenda->id,
                    'title' => $title,
                    'start' => $inicio,
                    'end' => $fim,
                    'color' => !empty($agenda->situacao_agenda->cor) ? $agenda->situacao_agenda->cor : '#378006'
                );


                $json = json_encode(['result' => $evento]);
            } else {
                $json = json_encode(['result' => 'erro']);
            }
            $this->response->body($json);
        }

        $agendaProcedimentosSelecionados = [];
        $agendaProcedimentos = $this->AgendaProcedimentos->find('all')
            ->where(['AgendaProcedimentos.agenda_id' => $agenda->id])
            ->andWhere(['AgendaProcedimentos.situacao_id' => 1]);
        foreach ($agendaProcedimentos as $ap) {
            $agendaProcedimentosSelecionados[] = $ap->agenda_cadprocedimento_id;
        }

        $clientes = $this->Agendas->Clientes->find('all', ['conditions' => ['Clientes.id' => $agenda->cliente_id]])->first();
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');
        $convenios = $this->Agendas->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $agendaProcedimentos = $this->AgendaCadprocedimentos->find('list')->where(['AgendaCadprocedimentos.situacao_id' => 1])->orderAsc('AgendaCadprocedimentos.nome');

        $inicio = $this->Data->date_time($agenda->inicio);
        $fm = $this->Data->date_time($agenda->fim);
        $view = 'calendario';
        $action = 'edit';

        $this->set(compact('agenda', 'tipoAgendas', 'view', 'action', 'clientes', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'convenios', 'agendaProcedimentos', 'agendaProcedimentosSelecionados', 'inicio', 'fm'));
        $this->set('_serialize', ['agenda']);
    }


    public function nedit($id = null)
    {
        $this->loadModel('AgendaProcedimentos');
        $this->loadModel('AgendaCadprocedimentos');
        $this->loadModel('ConfiguracaoPeriodos');

        $agenda = $this->Agendas->get($id, [
            'contain' => ['Clientes', 'Users', 'UserEdit', 'SituacaoAgendas']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if (!empty($this->request->data('agenda_procedimentos'))) {
                $agenda_cadprocedimento_id = $this->request->data('agenda_procedimentos');
                $this->AgendaProcedimentos->deleteAll(['agenda_id' => $id]);
                foreach ($agenda_cadprocedimento_id as $ap) {
                    $agendaProcedimento = $this->AgendaProcedimentos->newEntity();
                    $agendaProcedimento->agenda_id = $id;
                    $agendaProcedimento->agenda_cadprocedimento_id = $ap;
                    $this->AgendaProcedimentos->save($agendaProcedimento);
                }

                $procedimento = '';
                $agenda_procedimentos = $this->AgendaProcedimentos->find('all')
                    ->contain(['AgendaCadprocedimentos'])
                    ->where(['AgendaProcedimentos.agenda_id' => $id]);
                foreach ($agenda_procedimentos as $ap) {
                    $procedimento .= $ap->agenda_cadprocedimento->nome . ' | ';
                }
                $agenda->procedimento_concat = $procedimento . $this->request->data('procedimento');
                $agenda->procedimento = $this->request->data('procedimento');
                //$this->Agendas->save($agenda);
            }

            if (!empty($this->request->data['inicio'])) {
                $inicio = new DateTime();
                $this->request->data['inicio'] = $inicio->createFromFormat('d/m/Y H:i', $this->request->data['inicio']);
            }
            if (!empty($this->request->data['fim'])) {
                $fim = new DateTime();
                $this->request->data['fim'] = $fim->createFromFormat('d/m/Y H:i', $this->request->data['fim']);
            }
            $agenda = $this->Agendas->patchEntity($agenda, $this->request->data);


            /*$inicio = $this->request->data['inicio'];
            $inicio = new Time($inicio);
            $periodos = $this->ConfiguracaoPeriodos->find('all');
            foreach ($periodos as $p) {
                if ($inicio->format('H:i:s') >= $p->inicio_agenda->format('H:i:s') && $inicio->format('H:i:s') <= $p->fim_agenda->format('H:i:s')) {
                    $agenda->configuracao_periodo_id = $p->id;
                    break;
                }
            }*/

            $this->autoRender = false;
            $this->response->type('json');
            if ($this->Agendas->save($agenda)) {
                $res = ['msg' => 'Atualizado com sucesso!'];
            } else {
                $res = ['msg' => 'Erro ao atualizar!'];
            }
            $this->autoRender = false;
            $this->response->type('json');
            $this->response->body(json_encode($res));
        }

        $agendaProcedimentosSelecionados = [];
        $agendaProcedimentos = $this->AgendaProcedimentos->find('all')
            ->where(['AgendaProcedimentos.agenda_id' => $agenda->id])
            ->andWhere(['AgendaProcedimentos.situacao_id' => 1]);
        foreach ($agendaProcedimentos as $ap) {
            $agendaProcedimentosSelecionados[] = $ap->agenda_cadprocedimento_id;
        }

        $clientes = $this->Agendas->Clientes->find('all', ['conditions' => ['Clientes.id' => $agenda->cliente_id]])->first();
        $tipoAgendas = $this->Agendas->TipoAgendas->find('list')->orderAsc('TipoAgendas.nome');
        $medicoResponsaveis = $this->Agendas->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $situacaoAgendas = $this->Agendas->SituacaoAgendas->find('list')->orderAsc('SituacaoAgendas.nome');
        $convenios = $this->Agendas->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $agendaProcedimentos = $this->AgendaCadprocedimentos->find('list')->where(['AgendaCadprocedimentos.situacao_id' => 1])->orderAsc('AgendaCadprocedimentos.nome');
        $inicio = $this->Data->date_time($agenda->inicio);
        $fm = $this->Data->date_time($agenda->fim);
        $view = 'lista';
        $action = 'edit';

        $this->set(compact('agenda', 'tipoAgendas', 'action', 'inicio', 'fm', 'view', 'clientes', 'medicoResponsaveis', 'grupoAgendas', 'situacaoAgendas', 'convenios', 'agendaProcedimentos', 'agendaProcedimentosSelecionados'));
        $this->set('_serialize', ['agenda']);
    }


    public function drag()
    {
        $id = $this->request->data['id'];
        $agenda = $this->Agendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $inicio = null;
            if (!empty($this->request->data['data'])) {
                $inicio = new DateTime();
                $inicio = $inicio->createFromFormat('d/m/Y H:i:s', $this->request->data['data']);

            }
            $fim = null;
            if (!empty($this->request->data['fim'])) {
                $fim = new DateTime();
                $fim = $fim->createFromFormat('d/m/Y H:i:s', $this->request->data['fim']);
            }
            $agenda->inicio = $inicio;
            $agenda->fim = $fim;
            if ($this->Agendas->save($agenda)) {
                echo 1;
            } else {
                echo 'erro';
            }
        }
        $this->autoRender = false;
    }

    public function eventResizeAgenda()
    {
        $id = $this->request->data['id'];
        $inicio = new Time($this->request->data('start'));
        $fim = new Time($this->request->data('end'));

        $agenda = $this->Agendas->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            if (!empty($this->request->data('start'))) {
                $agenda->inicio = $inicio;
            }
            if (!empty($this->request->data('end'))) {
                $agenda->fim = $fim;
            }
            if ($this->Agendas->save($agenda)) {
                echo 1;
            } else {
                echo 'erro';
            }
        }
        $this->autoRender = false;
    }

    public function eventDropAgenda()
    {
        $inicio = new Time($this->request->getData('start'));
        $fim = new Time($this->request->getData('end'));
        $agenda = $this->Agendas->checkAgendamento($this->request->getData('grupo_id'), $inicio->format('Y-m-d H:i') . ':00', null, true);
        $de = $this->request->getData('id');

        $json = [];
        if (empty($agenda)) {
            if ($this->Agendas->atualizaHorario($de, $inicio, $fim))
                $json = ['error' => 0];
        }

        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    /** Ajax dropa eventos externos no calendario
     * @return static
     */
    public function dropAgenda()
    {
        $start = new Time($this->request->getData('start'));
        $end = new Time($this->request->getData('end'));
        $inicio = $start->format('Y-m-d H:i') . ':00';
        $eventAction = $this->request->getData('eventAction');
        $agenda = $this->Agendas->checkAgendamento($this->request->getData('grupo_id'), $inicio, null, true);
        $de = $this->request->getData('id');

        $json = [];
        if (empty($agenda)) {
            $newEntity = [
                'grupo_id' => $this->request->getData('grupo_id'),
                'inicio' => $start,
                'fim' => $end,
            ];
            if ($this->Agendas->transferirAgendamentoCalendario($de, null, $newEntity, $eventAction))
                $json = ['error' => 0, 'msg' => 'Operação realizada com sucesso!'];
        }

        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agenda = $this->Agendas->get($id);
        $agenda->situacao_id = 2;
        if ($this->Agendas->save($agenda)) {
            $this->Flash->success(__('O agenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O agenda não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Finalizar Agenda method
     *
     * @param string|null $id Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function finalizarAgenda1($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agenda = $this->Agendas->get($id);
        $agenda->situacao_agenda_id = 10;
        if ($this->Agendas->save($agenda)) {
            $res = ['mgs' => 'Agenda Finalizada com sucesso!'];
        } else {
            $res = ['mgs' => 'Erro ao finalizar agenda!'];
        }
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    public function finalizarAgenda($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agenda = $this->Agendas->get($id);
        $agenda->situacao_agenda_id = 10;
        if ($this->Agendas->save($agenda)) {
            $atendimentos = $this->loadModel('Atendimentos');
            $atendimentos->query()
                ->update()->set(['finalizado' => 1])->where(['agenda_id' => $id])->execute();
            $res = ['mgs' => 'Agenda Finalizada com sucesso!'];
        } else {
            $res = ['mgs' => 'Erro ao finalizar agenda!'];
        }
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    public function imprimir()
    {
        //$this->loadModel('ConfiguracaoPeriodos');
        //$periodos = $this->ConfiguracaoPeriodos->find('list');
        $this->loadModel('ConfiguracaoPeriodosAgenda');
        $relatorios = $this->Agenda->getTiposRelatoriosAgenda();
        $periodos = $this->ConfiguracaoPeriodosAgenda->find('list');
        $grupoAgendas = $this->Agendas->GrupoAgendas->find('list')->orderAsc('GrupoAgendas.nome');
        $this->set(compact('grupoAgendas', 'periodos', 'relatorios'));
    }

    public function report()
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('ConfiguracaoPeriodosAgenda');
        $agendas = $this->Agendas->find('all')
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'UserEdit', 'Convenios']);

        if (!empty($this->request->data['grupos'])) {
            $agendas->andWhere(['Agendas.grupo_id IN ' => $this->request->data['grupos']]);
        }

        /** Filtro dos períodos modelo novo, tabela configuracao_periodos_agenda */
        $config_periodo = $this->ConfiguracaoPeriodosAgenda->get($this->request->getData('periodo'));

        $inicio = new DateTime();
        $now = $inicio->createFromFormat('d/m/Y', $this->request->getData('inicio'));
        $inicio = $now->format('Y-m-d') . " " . $config_periodo->hora_inicial;

        $fim = new DateTime();
        $now = $fim->createFromFormat('d/m/Y', $this->request->getData('fim'));
        $fim = $now->format('Y-m-d') . " " . $config_periodo->hora_final;

        $agendas->andWhere(['Agendas.inicio >=' => $inicio]);
        $agendas->andWhere(['Agendas.inicio <=' => $fim]);
        /** Fim novo filtro */

        if ($this->request->getData('horarios_utilizados') == 1) {
            $agendas
                ->andWhere(['(Agendas.convenio_id != -1 OR Agendas.cliente_id != -1)']);
        }

        /* if (!empty($this->request->data['inicio'])) {
             $inicio = new DateTime();
             $now = $inicio->createFromFormat('d/m/Y', $this->request->data['inicio']);
             $agendas->andWhere(['Agendas.inicio >=' => $now->format('Y-m-d') . " 00:00:00"]);
             $inicio = $this->request->data['inicio'];
         }
         if (!empty($this->request->data['fim'])) {
             $fim = new DateTime();
             $now = $fim->createFromFormat('d/m/Y', $this->request->data['fim']);
             $agendas->andWhere(['Agendas.inicio <=' => $now->format('Y-m-d') . " 23:59:59"]);
             $fim = $this->request->data['fim'];
         } else {
             if (!empty($this->request->data['inicio'])) {
                 $inicio = new DateTime();
                 $now = $inicio->createFromFormat('d/m/Y', $this->request->data['inicio']);
                 $agendas->andWhere(['Agendas.inicio <=' => $now->format('Y-m-d') . " 23:59:00"]);
                 $inicio = $this->request->data['inicio'];
             }
         }*/
        $agendas->andWhere(['SituacaoAgendas.ocultar' => 0]);

        $agendas->order('Agendas.inicio');

        $json['dados'] = null;
        foreach ($agendas as $a) {

            if ($a->cliente_id == -1) {
                $idade = !empty($a->idade) ? $a->idade : '1';
            } else {
                $idade = !empty($a->cliente->nascimento) ? $this->Data->idade(date_format($a->cliente->nascimento, 'd/m/Y')) : '1';
            }

            $json['dados'][] = [
                'id' => $a->id,
                'cliente' => ($a->cliente_id == -1) ? 'Prov. ' . $a->nome_provisorio : $a->cliente->nome,
                'cliente_id' => ($a->cliente_id == -1) ? null : $a->cliente_id,
                'fone' => ($a->cliente_id == -1) ? $a->fone_provisorio : $a->cliente->celular . ' - ' . $a->cliente->telefone,
                'convenio' => empty($a->convenio_id) ? $a->convenio_provisorio : $a->convenio->nome,
                'inicio' => !empty($a->inicio) ? $this->Data->hora_sms($a->inicio) : null,
                'fim' => !empty($a->fim) ? $this->Data->hora_sms($a->fim) : null,
                'data' => !empty($a->inicio) ? $this->Data->data_sms($a->inicio) : null,
                'grupo' => !empty($a->grupo_agenda) ? $a->grupo_agenda->nome : null,
                'situacao' => !empty($a->situacao_agenda) ? $a->situacao_agenda->nome : null,
                'user' => !empty($a->user) ? $a->user->login : 'importado',
                'tipo_agendamento' => empty($a->tipo_agenda) ? $a->procedimento_provisorio : $a->tipo_agenda->nome,
                'observacao' => ($a->cliente_id == -1) ? $a->fone_provisorio . ' ' . $a->observacao :
                    $a->cliente->telefone . '/' . $a->cliente->celular . ' ' . $a->observacao,
                'arquivo' => ($a->cliente_id == -1) ? '' : $a->cliente->arquivo,
                'procedimentos' => $a->procedimento_concat,
                'idade' => !empty($idade) ? $idade : '1',
                'marcado' => $a->user->nome
            ];
        }

        $json['alias'][] = [
            'count' => $agendas->count(),
            'inicio' => !empty($json['dados']) ? $inicio : null,
            'fim' => !empty($json['dados']) ? $fim : null,
            'periodo' => $config_periodo->nome
        ];

        //debug($json);exit;
        $this->Json->create('agendas2.json', json_encode($json));

        switch ($this->request->getData('relatorio_agenda')) {
            case 1:
                $this->set('report', 'agendas.mrt');
                break;
            case 2:
                $this->set('report', 'agendas_sem_dados.mrt');
                break;
        }
    }

    public function getminutes()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $json = null;
        if (!empty($this->request->data['date'])) {
            $data = new Time();
            $now = $data->createFromFormat('d/m/Y H:i', $this->request->data['date']);

            //se o grupo de agenda for setado
            if (!empty($this->request->data['grupo_id'])) {
                $grupoAgenda = $this->Agendas->GrupoAgendas->findById($this->request->data['grupo_id'])->first();
                if (!empty($grupoAgenda->intervalo)) {
                    $minutos = $this->Data->getMinutos($grupoAgenda->intervalo);
                    $dif = $now->addMinutes($minutos);
                } else {
                    $dif = $now->addHour(1);
                }
            } else {
                //caso nao exista grupo de agenda checo se existe do usuario logado
                if (!empty($this->Auth->user('codigo_agenda'))) {
                    $grupoAgenda = $this->Agendas->GrupoAgendas->findById($this->Auth->user('codigo_agenda'))->first();
                    if (!empty($grupoAgenda->intervalo)) {
                        $minutos = $this->Data->getMinutos($grupoAgenda->intervalo);
                        $dif = $now->addMinutes($minutos);
                    } else {
                        $dif = $now->addHour(1);
                    }
                } else {
                    $dif = $now->addHour(1);
                }
            }
            $json = json_encode(['result' => $dif->format('d/m/Y H:i')]);

        }
        $this->response->body($json);
    }

    public function finalizar($agenda_id)
    {
        $agenda = $this->Agendas->get($agenda_id, ['contain' => ['GrupoAgendas' => ['MedicoResponsaveis']]]);
        $this->loadModel('Retornos');
        $retorno = $this->Retornos->newEntity();

        $this->loadModel('ClienteHistoricos');
        $clienteHistorico = $this->ClienteHistoricos->newEntity();

        if ($this->request->is(['patch', 'post', 'put'])) {
            //checo se existe retornos para a agenda que está fechando caso exista eu devo finalizar este retorno
            if (!empty($agenda->retorno_id)) {
                $return = $this->Retornos->get($agenda->retorno_id);
                $this->loadModel('LogRetornos');
                $logRetorno = $this->LogRetornos->newEntity();
                $logRetorno->retorno_id = $return->id;
                $logRetorno->cliente_id = $return->cliente_id;
                $logRetorno->grupo_id = $return->grupo_id;
                $logRetorno->previsao = $return->previsao;
                $logRetorno->dias = $return->dias;
                $logRetorno->situacao_retorno_id = $return->situacao_retorno_id;
                $logRetorno->user_id = $return->user_id;
                $logRetorno->observacao = $return->observacao;
                $logRetorno->justificativa = $return->justificativa;
                $logRetorno->data_confirmacao = $return->data_confirmacao;
                $logRetorno->quem_aprovou = $return->quem_aprovou;
                $this->LogRetornos->save($logRetorno);

                $return->situacao_retorno_id = 5;
                $this->Retornos->save($return);
            }


            $result = array();
            $clienteHistorico = $this->ClienteHistoricos->patchEntity($clienteHistorico, $this->request->data);
            if ($this->ClienteHistoricos->save($clienteHistorico)) {
                $this->request->data['grupo_id'] = $this->request->data['grupo_retorno'];
                $retorno = $this->Retornos->patchEntity($retorno, $this->request->data);
                if ($this->Retornos->save($retorno)) {
                    $result['success'] = 'Retorno salvo!';
                } else {
                    $result['erro'] = 'Erro ao salvar retorno';
                }
            } else {
                $result['erro'] = 'Erro ao salvar Histórico';
            }
            $this->autoRender = false;
            $this->response->type('json');
            $json = json_encode(['result' => $result]);
            $this->response->body($json);

        }
        $tipoHistorias = $this->ClienteHistoricos->TipoHistorias->find('list');
        $medicoResponsaveis = $this->ClienteHistoricos->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id' => 1]);

        $cliente = null;
        $grupoRetornos = null;
        if (!empty($agenda->cliente_id)) {
            $this->loadModel('ClienteGrupoRetornos');
            $cliente_grupo = $this->ClienteGrupoRetornos->find('all')
                ->where(['ClienteGrupoRetornos.situacao_id' => 1,
                    'ClienteGrupoRetornos.cliente_id' => $agenda->cliente_id]);

            $id_grupos = [];
            foreach ($cliente_grupo as $c) {
                $id_grupos[] = $c->grupo_id;
            }
            if (!empty($id_grupos)) { // aqui filtro o vinculo do cliente ao grupo
                $grupoRetornos = $this->Retornos->GrupoRetornos->find('list')->where(['GrupoRetornos.id in ' => $id_grupos]);
            }
        } else {
            $grupoRetornos = $this->Retornos->GrupoRetornos->find('list');
        }
        $situacaoRetornos = $this->Retornos->SituacaoRetornos->find('list');
        $clientes = $this->Agendas->Clientes->find('all', ['conditions' => ['Clientes.id' => $agenda->cliente_id]])->first();

        $this->set(compact('agenda', 'clienteHistorico', 'clientes', 'tipoHistorias', 'medicoResponsaveis', 'retorno', 'grupoRetornos', 'situacaoRetornos'));
        $this->set('_serialize', ['agenda']);

    }

    public function getTimeGroup()
    {
        $this->loadModel('AgendaPeriodos');
        $this->loadModel('GrupoAgendas');
        $this->response->type('json');
        $periodos = [];
        $diasSemana = null;
        $start = null;
        $end = null;
        $intervalo = '00:15:00';
        $hora_inicio = '07:00:00';
        $hora_fim = '19:00:00';

        $agendaPeriodos = $this->AgendaPeriodos->find('all')
            ->contain(['GrupoAgendas'])
            ->where(['AgendaPeriodos.grupo_agenda_id' => $this->request->data('id')])
            ->andWhere(['AgendaPeriodos.situacao_id' => 1]);

        if (!empty($this->request->data('id'))) {
            $grupoAgenda = $this->GrupoAgendas->get($this->request->data('id'));
            if (!empty($grupoAgenda->inicio)) {
                $hora_inicio = new Time($grupoAgenda->inicio);
                $hora_inicio = $hora_inicio->format('H:i:s');
            }
            if (!empty($grupoAgenda->fim)) {
                $hora_fim = new Time($grupoAgenda->fim);
                $hora_fim = $hora_fim->format('H:i:s');
            }

            if (!empty($grupoAgenda->intervalo)) {
                $intervalo = new Time($grupoAgenda->intervalo);
                $intervalo = $intervalo->format('H:i').':00';
            }
        }

        foreach ($agendaPeriodos as $agendaPeriodo) {
            if (!empty($agendaPeriodo->inicio)) {
                $inicio = new Time($agendaPeriodo->inicio);
                $inicio = $inicio->format('H:i:s');
            } else {
                $inicio = '07:00:00';
            }
            if (!empty($agendaPeriodo->fim)) {
                $fim = new Time($agendaPeriodo->fim);
                $fim = $fim->format('H:i:s');
            } else {
                $fim = '19:00:00';
            }

            $periodos[] = [
                'dia_semana' => $agendaPeriodo->dia_semana,
                'periodos' => [
                    'start' => $inicio,
                    'end' => $fim
                ]
            ];

        }

        // Quantidade de pacientes
        $agenda = $this->Agendas->find('all')
            //->contain(['SituacaoAgendas'])
            ->contain(['TipoAgendas', 'Clientes', 'MedicoResponsaveis', 'GrupoAgendas', 'SituacaoAgendas', 'Users', 'SituacaoCadastros', 'Convenios'])
            ->where(['Agendas.grupo_id' => $this->request->data('id')])
            ->andWhere(['Agendas.inicio >= ' => $this->request->data('date') . ' 00:01:00'])
            ->andWhere(['Agendas.inicio <= ' => $this->request->data('date') . ' 23:59:00'])
            ->andWhere(['SituacaoAgendas.ocultar' => 0])
            ->andWhere(['Agendas.situacao_id' => 1])
            ->count();

        $this->response->body(json_encode([
            'periodos' => $periodos,
            'intervalo' => $intervalo,
            'horarios' => ['inicio' => $hora_inicio, 'fim' => $hora_fim],
            'pacientes' => $agenda
        ]));
        $this->autoRender = false;

    }

    public function sms($id = null)
    {
        $agenda = $this->Agendas->get($id);

        $this->set(compact('agenda'));
        $this->set('_serialize', ['agenda']);
    }

    public function transferencia($de, $para)
    {
        $action = $this->request->getData('action');
        $data = $this->Agendas->transferirAgendamentoLista($de, $para, $action);
        if($data)
            $json = ['error' => 0, 'msg' => "Agendamento $action com sucesso!"];
        else
            $json = ['error' => 1, 'msg' => "Falha ao tentar $action agendamento. Tente mais tarde!"];

        if($action == 'copiar')
            $this->request->session()->delete('copiar');
            
        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function copiarAgendamento($id)
    {
        $session =  new Session();
        if(!empty($session->read('copiar'))){
            $session->delete('copiar');
            $session->write('copiar', $id);
        }else{
            $session->write('copiar', $id);
        }

        $json = json_encode($id);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function getHoursEmpty()
    {
        $dates = $this->request->getData('dates');
        $grupo_id = $this->request->getData('grupo_id');
        $json = [];

        /*$dates = ["2018-04-01", "2018-04-02", "2018-04-03", "2018-04-04", "2018-04-05", "2018-04-06", "2018-04-07", "2018-04-08", "2018-04-09", "2018-04-10", "2018-04-11", "2018-04-12", "2018-04-13", "2018-04-14", "2018-04-15", "2018-04-16", "2018-04-17", "2018-04-18", "2018-04-19", "2018-04-20", "2018-04-21", "2018-04-22", "2018-04-23", "2018-04-24", "2018-04-25", "2018-04-26", "2018-04-27", "2018-04-28", "2018-04-29", "2018-04-30", "2018-05-01", "2018-05-02", "2018-05-03", "2018-05-04", "2018-05-05", "2018-05-06", "2018-05-07", "2018-05-08", "2018-05-09", "2018-05-10", "2018-05-11", "2018-05-12"];
        $grupo_id = 111;*/

        if(is_array($dates)){
            foreach($dates as $d){
                $now = new Time($d);
                $agendaPeriodo = $this->Agendas->GrupoAgendas->AgendaPeriodos->find('all')
                ->contain(['GrupoAgendas'])
                ->where(['AgendaPeriodos.grupo_agenda_id' => $grupo_id])
                ->andWhere(['AgendaPeriodos.situacao_id' => 1])
                ->andWhere(['AgendaPeriodos.dia_semana' => $now->format('w')])
                ->first();                

                if(!empty($agendaPeriodo))
                {
                    $inicio = new Time($agendaPeriodo->inicio);
                    $fim = new Time($agendaPeriodo->fim);
                    $intervalo = new Time($agendaPeriodo->grupo_agenda->intervalo);
                    $count_hours = 0;
                    while($inicio < $fim)
                    {
                        $inicio->modify('+'.$intervalo->format('i').' minute')->format('H:i').':00';
                        $count_hours++;
                    }

                    $qtd_pacientes = $this->Agendas->getQtdPacientes($grupo_id, $d);
                    $horarios_livres = $count_hours - $qtd_pacientes;
                    
                    $json[] = [
                        'date' => $d,
                        'horarios_livres' => $horarios_livres,
                    ];   

                }
            }
        }

        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    /**
     * Troca a situação do agendamento para confirmado - 4
     *
     * @param Integer $id
     * @return void
     */
    public function confirmaAgendamento($id)
    {
        $query = $this->Agendas->get($id);
        $query->situacao_agenda_id = 4;
        if($this->Agendas->save($query)) {
            $json = ['error' => 0, 'msg' => 'Agendamento confirmado com sucesso!'];
        }else {
            $json = ['error' => 1, 'msg' => 'Falha ao confirmar agendamento, por favor tente mais tarde!'];
        }
        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    /**
     * Reserva um agendamento - situacao_agenda_id = 1500
     * @return void
     */
    public function reservarAgendamento()
    {
        $agendas = $this->Agendas->newEntity();
        $agendas->tipo_id = 1;
        $agendas->cliente_id = -1;
        $agendas->profissional_id = -1;
        $agendas->grupo_id = $this->request->getData('grupo_id');
        $agendas->convenio_id = -1;
        $agendas->situacao_agenda_id = 1500;
        $agendas->nome_provisorio = "Reservado por ".$this->Auth->user('nome');
        $agendas->inicio = $this->request->getData('inicio');
        $agendas->fim = $this->request->getData('fim');

        if($this->Agendas->save($agendas)) {
            $json = ['error' => 0, 'msg' => 'Agendamento reservado com sucesso!'];
        }else {
            $json = ['error' => 1, 'msg' => 'Falha ao reservar agendamento, por favor tente mais tarde!'];
        }

        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function contadores()
    {
        $this->loadModel('AgendaFilaespera');
        $this->loadModel('AnotacaoAgendas');
        $this->loadModel('GrupoAgendas');

        $fila_espera = $this->AgendaFilaespera->find('all')
            ->where(['AgendaFilaespera.grupo_id IN' => $this->request->getData('grupo_id')])
            ->andWhere(['AgendaFilaespera.situacao_id' => 1])->count();

        $anotacao = $this->AnotacaoAgendas->find('all')
            ->contain(['GrupoAgendas'])
            ->where(['AnotacaoAgendas.grupo_id IN' => $this->request->getData('grupo_id')])
            ->andWhere(['AnotacaoAgendas.data' => $this->Data->DataSQL($this->request->getData('inicio'))])
            ->andWhere(['AnotacaoAgendas.situacao_id' => 1])
            ->order(['AnotacaoAgendas.grupo_id' => 'ASC']);

        $particularidades = $this->GrupoAgendas->find()
            ->select(['grupo_id' => 'GrupoAgendas.id', 'GrupoAgendas.nome', 'descricao' => 'GrupoAgendas.particularidades'])
            ->where(['GrupoAgendas.id IN' => $this->request->getData('grupo_id')])
            ->andWhere(['GrupoAgendas.situacao_id' => 1])
            ->andWhere(['GrupoAgendas.particularidades IS NOT NULL'])
            ->andWhere(['GrupoAgendas.particularidades !=' => '']);

        $res = [
            'fila_espera' => $fila_espera,
            'anotacoes' => $anotacao->toArray(),
            'anotacao' => $anotacao->count(),
            'particularidades' => $particularidades,
            'qtd_particularidades' => $particularidades->count(),
            'grupo' => $this->request->getData('grupo_id'),
            'mostra_avisos_agenda_aberto' => $this->ConfiguracaoAgenda->mostraAvisosAgendaAberto()
        ];

        $json = json_encode($res);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

}
