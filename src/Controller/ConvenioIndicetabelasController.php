<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConvenioIndicetabelas Controller
 *
 * @property \App\Model\Table\ConvenioIndicetabelasTable $ConvenioIndicetabelas
 */
class ConvenioIndicetabelasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($convenio_id = null)
    {
        $query = $this->ConvenioIndicetabelas->find('all')
            ->contain(['Convenios', 'GrupoProcedimentos', 'SituacaoCadastros', 'UserReg', 'UserAlt'])
            ->where(['ConvenioIndicetabelas.situacao_id' => 1]);

        if(!empty($convenio_id)){
            $query->andWhere(['ConvenioIndicetabelas.convenio_id' => $convenio_id]);
        }

        $convenioIndicetabelas = $this->paginate($query);

        $this->set(compact('convenioIndicetabelas', 'convenio_id'));
        $this->set('_serialize', ['convenioIndicetabelas']);

    }

    /**
     * View method
     *
     * @param string|null $id Convenio Indicetabela id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convenioIndicetabela = $this->ConvenioIndicetabelas->get($id, [
            'contain' => ['Convenios', 'GrupoProcedimentos', 'SituacaoCadastros']
        ]);

        $this->set('convenioIndicetabela', $convenioIndicetabela);
        $this->set('_serialize', ['convenioIndicetabela']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($convenio_id = null)
    {
        $convenioIndicetabela = $this->ConvenioIndicetabelas->newEntity();
        if ($this->request->is('post')) {
            $convenioIndicetabela = $this->ConvenioIndicetabelas->patchEntity($convenioIndicetabela, $this->request->data);
                        if ($this->ConvenioIndicetabelas->save($convenioIndicetabela)) {
                $this->Flash->success(__('O convenio indicetabela foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index', $convenio_id]);
            } else {
                $this->Flash->error(__('O convenio indicetabela não foi salvo. Por favor, tente novamente.'));
            }
        }

        $grupo_procedimentos = $this->ConvenioIndicetabelas->GrupoProcedimentos->find('list')->where(['GrupoProcedimentos.situacao_id' => 1])->orderAsc('nome');

        $this->set(compact('convenioIndicetabela', 'grupo_procedimentos', 'convenio_id'));
        $this->set('_serialize', ['convenioIndicetabela']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Convenio Indicetabela id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convenioIndicetabela = $this->ConvenioIndicetabelas->get($id, [
            'contain' => []
        ]);
        $convenio_id = $convenioIndicetabela->convenio_id;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convenioIndicetabela = $this->ConvenioIndicetabelas->patchEntity($convenioIndicetabela, $this->request->data);
            if ($this->ConvenioIndicetabelas->save($convenioIndicetabela)) {
                $this->Flash->success(__('O convenio indicetabela foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index', $convenio_id]);
            } else {
                $this->Flash->error(__('O convenio indicetabela não foi salvo. Por favor, tente novamente.'));
            }
        }

        $grupo_procedimentos = $this->ConvenioIndicetabelas->GrupoProcedimentos->find('list')->where(['GrupoProcedimentos.situacao_id' => 1])->orderAsc('nome');

        $this->set(compact('convenioIndicetabela', 'convenio_id', 'grupo_procedimentos'));
        $this->set('_serialize', ['convenioIndicetabela']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convenio Indicetabela id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $convenioIndicetabela = $this->ConvenioIndicetabelas->get($id);
                $convenioIndicetabela->situacao_id = 2;
        if ($this->ConvenioIndicetabelas->save($convenioIndicetabela)) {
            $this->Flash->success(__('O convenio indicetabela foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O convenio indicetabela não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Convenio Indicetabela id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ConvenioIndicetabelas->find('all')
        ->where(['ConvenioIndicetabelas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ConvenioIndicetabelas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Convenio Indicetabela id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $convenioIndicetabela = $this->ConvenioIndicetabelas->get($this->request->data['id']);
            $res = ['nome'=>$convenioIndicetabela->nome,'id'=>$convenioIndicetabela->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    /**
     * Método para atualizar valores com plugin editable
     */
    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name']) && !empty($this->request->data['value'])){
            $name = $this->request->data['name'];
            $convenio_indicetabela = $this->ConvenioIndicetabelas->get($this->request->data['pk']);
            $convenio_indicetabela->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->ConvenioIndicetabelas->save($convenio_indicetabela)) {
                $res = ['res' => $convenio_indicetabela->$name,'msg'=>'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

    /**
     * Método para deletar registro sem atualizar a pagina
     */
    public function deleteModal()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $convenioIndicetabela = $this->ConvenioIndicetabelas->get($this->request->data['id']);
        $convenioIndicetabela->situacao_id = 2;
        if ($this->ConvenioIndicetabelas->save($convenioIndicetabela)) {
            $res = 0;
        } else {
            $res = 1;
        }
        $json = json_encode(['res' => $res]);
        $this->response->body($json);
    }
}
