<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * GrupoAgendaHorarios Controller
 *
 * @property \App\Model\Table\GrupoAgendaHorariosTable $GrupoAgendaHorarios
 */
class GrupoAgendaHorariosController extends AppController
{

    public $components = array('Data', 'Query', 'Agenda');
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($grupo_id = null)
    {
        $query = $this->GrupoAgendaHorarios->find('all')
            ->contain(['GrupoAgendas', 'TipoAgendas', 'OperacaoAgendaHorarios'])
            ->where(['GrupoAgendaHorarios.situacao_id' => 1])->orderDesc('GrupoAgendaHorarios.id');

        if(!empty($grupo_id)){
            $query->andWhere(['GrupoAgendaHorarios.grupo_id' => $grupo_id]);
        }
        if(!empty($this->request->query('dia_semana'))){
            $query->andWhere(['GrupoAgendaHorarios.dia_semana' => $this->request->query('dia_semana')]);
        }

        $grupoAgendaHorarios = $this->paginate($query);

        $grupoAgendas = $this->GrupoAgendaHorarios->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        $tipoAgendas = $this->GrupoAgendaHorarios->TipoAgendas->find('list', ['limit' => 200])->where(['TipoAgendas.situacao_id' => 1])->orderAsc('TipoAgendas.nome');
        $this->set(compact('grupoAgendaHorarios', 'grupo_id', 'grupoAgendas', 'tipoAgendas'));
        $this->set('_serialize', ['grupoAgendaHorarios']);

    }

    /**
     * View method
     *
     * @param string|null $id Grupo Agenda Horario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoAgendaHorario = $this->GrupoAgendaHorarios->get($id, [
            'contain' => ['GrupoAgendas', 'SituacaoCadastros', 'Users', 'TipoAgendas']
        ]);

        $this->set('grupoAgendaHorario', $grupoAgendaHorario);
        $this->set('_serialize', ['grupoAgendaHorario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($grupo_id = null)
    {
        $error = 0;
        $grupoAgendaHorario = $this->GrupoAgendaHorarios->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            foreach($data['dias_semana'] as $dia){
                $grupoAgendaHorario = $this->GrupoAgendaHorarios->newEntity();
                $grupoAgendaHorario = $this->GrupoAgendaHorarios->patchEntity($grupoAgendaHorario, $data);
                $grupoAgendaHorario->dia_semana = $dia;
                $grupoAgendaHorario->tipo_agenda_id = $this->request->getData('tipo_id');
                $grupoAgendaHorario->situacao_agenda = $this->request->getData('situacao_agenda');
                $grupoAgendaHorario->operacao_agenda_horario_id = $this->request->getData('operacao_agenda_horario_id');
                $query = $this->Agenda->geraQuery($data, $dia);
                if ($this->GrupoAgendaHorarios->save($grupoAgendaHorario) && $this->Query->operacaoHorarios($query)) {
                    $error = 0;
                } else {
                    $error += 1;
                }
            }

            $json = json_encode($error);
            $response = $this->response->withType('json')->withStringBody($json);
            return $response;
        }

        $grupoAgendas = $this->GrupoAgendaHorarios->Agendas->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        $tipoAgendas = $this->GrupoAgendaHorarios->TipoAgendas->find('list')->where(['TipoAgendas.situacao_id' => 1])->orderAsc('TipoAgendas.nome');
        $operacoes = $this->GrupoAgendaHorarios->OperacaoAgendaHorarios->find('list')->where(['OperacaoAgendaHorarios.situacao_id' => 1])->orderAsc('OperacaoAgendaHorarios.nome');

        $this->set(compact('grupoAgendaHorario', 'grupoAgendas', 'tipoAgendas', 'grupo_id', 'operacoes'));
        $this->set('_serialize', ['grupoAgendaHorario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Agenda Horario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoAgendaHorario = $this->GrupoAgendaHorarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['post'])) {
            $grupoAgendaHorario = $this->GrupoAgendaHorarios->patchEntity($grupoAgendaHorario, $this->request->data);
            if ($this->GrupoAgendaHorarios->save($grupoAgendaHorario)) {
                $this->Flash->success(__('O grupo agenda horario foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo agenda horario não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('grupoAgendaHorario'));
        $this->set('_serialize', ['grupoAgendaHorario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Agenda Horario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id, $grupo_id)
    {
        $grupoAgendaHorario = $this->GrupoAgendaHorarios->get($id);
        $grupoAgendaHorario->situacao_id = 2;
        $grupoAgendaHorario->modified = date('Y-m-d H:i:s');
        $deleteHorariosAgenda = $this->GrupoAgendaHorarios->Agendas->deleteAllHorarios($id);
        if ($this->GrupoAgendaHorarios->save($grupoAgendaHorario) && $deleteHorariosAgenda) {
            $this->Flash->success(__('O grupo agenda horario foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo agenda horario não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index', $grupo_id]);
    }

    public function excluir($id, $grupo_id)
    {
        $error = 0;
        $grupoAgendaHorario = $this->GrupoAgendaHorarios->get($id, ['contain' => 'GrupoAgendas']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            // Convertendo a data_inicio e data_fim para formato SQL ex: 1111-11-11
            $data = $this->request->getData();
            $data['data_inicio'] = $this->Data->DataSQL($this->request->getData('data_inicio'));
            $data['data_fim'] = $this->Data->DataSQL($this->request->getData('data_fim'));
            if($this->GrupoAgendaHorarios->Agendas->geraOrDeleteHorarios($data, $id, $grupoAgendaHorario->dia_semana)){
                $error = 0;
            }else{
                $error += 1;
            }
            if($error == 0){
                $this->Flash->success(__('Horários deletados com sucesso!'));
                return $this->redirect(['action' => 'index', $grupo_id]);
            }else{
                $this->Flash->error(__('Ouve algum problema ao deletar os horários. Por favor, tente novamente.'));
            }
        }

        $grupoAgendas = $this->GrupoAgendaHorarios->Agendas->GrupoAgendas->find('list')->where(['GrupoAgendas.situacao_id' => 1])->orderAsc('GrupoAgendas.nome');
        $tipoAgendas = $this->GrupoAgendaHorarios->TipoAgendas->find('list', ['limit' => 200])->where(['TipoAgendas.situacao_id' => 1])->orderAsc('TipoAgendas.nome');

        $this->set(compact('grupoAgendaHorario', 'grupoAgendas', 'tipoAgendas', 'grupo_id'));
        $this->set('_serialize', ['grupoAgendaHorario']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Grupo Agenda Horario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->GrupoAgendaHorarios->find('all')
            ->where(['GrupoAgendaHorarios.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('GrupoAgendaHorarios.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Grupo Agenda Horario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $grupoAgendaHorario = $this->GrupoAgendaHorarios->get($this->request->data['id']);
            $res = ['nome'=>$grupoAgendaHorario->nome,'id'=>$grupoAgendaHorario->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
