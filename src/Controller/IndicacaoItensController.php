<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IndicacaoItens Controller
 *
 * @property \App\Model\Table\IndicacaoItensTable $IndicacaoItens
 */
class IndicacaoItensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Indicacao', 'SituacaoCadastros', 'Users']
        ];
        $indicacaoItens = $this->paginate($this->IndicacaoItens);


        $this->set(compact('indicacaoItens'));
        $this->set('_serialize', ['indicacaoItens']);

    }

    /**
     * All method
     *
     * @return \Cake\Network\Response|null
     */
    public function all()
    {
        $this->paginate = [
            'contain' => ['Indicacao']
        ];
        $indicacao_id = null;
        if(!empty($this->request->query['indicacao_id'])){
            $indicacao_id = $this->request->query['indicacao_id'];
            $query['conditions'][]= ['IndicacaoItens.indicacao_id'=>$this->request->query['indicacao_id']];
            $this->paginate = array_merge($this->paginate,$query);
        }

        $indicacaoItens = $this->paginate($this->IndicacaoItens);


        $this->set(compact('indicacaoItens','indicacao_id'));
        $this->set('_serialize', ['indicacaoItens']);

    }

    /**
     * View method
     *
     * @param string|null $id Indicacao Iten id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $indicacaoIten = $this->IndicacaoItens->get($id, [
            'contain' => ['Indicacao', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('indicacaoIten', $indicacaoIten);
        $this->set('_serialize', ['indicacaoIten']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $indicacaoIten = $this->IndicacaoItens->newEntity();
        if ($this->request->is('post')) {
            $indicacaoIten = $this->IndicacaoItens->patchEntity($indicacaoIten, $this->request->data);
            if ($this->IndicacaoItens->save($indicacaoIten)) {
                $this->Flash->success(__('O indicacao iten foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O indicacao iten não foi salvo. Por favor, tente novamente.'));
            }
        }
        $indicacao_id = null;
        if(!empty($this->request->query['indicacao_id'])){
            $indicacao_id = $this->request->query['indicacao_id'];
        }

        $this->set(compact('indicacaoIten','indicacao_id'));
        $this->set('_serialize', ['indicacaoIten']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Indicacao Iten id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $indicacaoIten = $this->IndicacaoItens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $indicacaoIten = $this->IndicacaoItens->patchEntity($indicacaoIten, $this->request->data);
            if ($this->IndicacaoItens->save($indicacaoIten)) {
                $this->Flash->success(__('O indicacao iten foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O indicacao iten não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('indicacaoIten'));
        $this->set('_serialize', ['indicacaoIten']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Indicacao Iten id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $indicacaoIten = $this->IndicacaoItens->get($id);
                $indicacaoIten->situacao_id = 2;
        if ($this->IndicacaoItens->save($indicacaoIten)) {
            $this->Flash->success(__('O indicacao iten foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O indicacao iten não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Indicacao Iten id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->IndicacaoItens->find('all')
        ->where(['IndicacaoItens.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('IndicacaoItens.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Indicacao Iten id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $indicacaoIten = $this->IndicacaoItens->get($this->request->data['id']);
            $res = ['nome'=>$indicacaoIten->nome,'id'=>$indicacaoIten->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function itensSelect(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = -1;
        if(!empty($this->request->query['indicacao_id'])){
            $res = $this->IndicacaoItens->find('all')
                ->where(['IndicacaoItens.indicacao_id'=>$this->request->query['indicacao_id']]);
        }
        $this->response->body(json_encode($res));
    }
}
