<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoEsperaagenda Controller
 *
 * @property \App\Model\Table\TipoEsperaagendaTable $TipoEsperaagenda
 */
class TipoEsperaagendaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users', 'StatusTipoespera']
        ];
        $tipoEsperaagenda = $this->paginate($this->TipoEsperaagenda);


        $status_tipo_espera = $this->TipoEsperaagenda->StatusTipoEspera->getList();
        $this->set(compact('tipoEsperaagenda', 'status_tipo_espera'));
        $this->set('_serialize', ['tipoEsperaagenda']);

    }

    /**
     * View method
     *
     * @param string|null $id Tipo Esperaagenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoEsperaagenda = $this->TipoEsperaagenda->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'StatusTipoespera']
        ]);

        $this->set('tipoEsperaagenda', $tipoEsperaagenda);
        $this->set('_serialize', ['tipoEsperaagenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoEsperaagenda = $this->TipoEsperaagenda->newEntity();
        if ($this->request->is('post')) {
            $tipoEsperaagenda = $this->TipoEsperaagenda->patchEntity($tipoEsperaagenda, $this->request->data);
            if ($this->TipoEsperaagenda->save($tipoEsperaagenda)) {
                $this->Flash->success(__('O tipo esperaagenda foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo esperaagenda não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tipoEsperaagenda'));
        $this->set('_serialize', ['tipoEsperaagenda']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Esperaagenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoEsperaagenda = $this->TipoEsperaagenda->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoEsperaagenda = $this->TipoEsperaagenda->patchEntity($tipoEsperaagenda, $this->request->data);
            if ($this->TipoEsperaagenda->save($tipoEsperaagenda)) {
                $this->Flash->success(__('O tipo esperaagenda foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo esperaagenda não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tipoEsperaagenda'));
        $this->set('_serialize', ['tipoEsperaagenda']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Esperaagenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $tipoEsperaagenda = $this->TipoEsperaagenda->get($id);
                $tipoEsperaagenda->situacao_id = 2;
        if ($this->TipoEsperaagenda->save($tipoEsperaagenda)) {
            $this->Flash->success(__('O tipo esperaagenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo esperaagenda não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Tipo Esperaagenda id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->TipoEsperaagenda->find('all')
        ->where(['TipoEsperaagenda.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TipoEsperaagenda.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Tipo Esperaagenda id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $tipoEsperaagenda = $this->TipoEsperaagenda->get($this->request->data['id']);
            $res = ['nome'=>$tipoEsperaagenda->nome,'id'=>$tipoEsperaagenda->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
