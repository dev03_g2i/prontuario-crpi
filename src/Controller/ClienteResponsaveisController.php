<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClienteResponsaveis Controller
 *
 * @property \App\Model\Table\ClienteResponsaveisTable $ClienteResponsaveis
 */
class ClienteResponsaveisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ClienteResponsaveis->find('all')
            ->contain(['Clientes', 'GrauParentescos', 'EstadoCivis', 'SituacaoCadastros', 'Users'])
            ->where(['ClienteResponsaveis.situacao_id = ' => '1']);

        if(!empty($this->request->query['cliente_id'])){
            $query->andWhere(['ClienteResponsaveis.cliente_id = ' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        }else{
            $cliente_id = null;
        }
        $clienteResponsaveis = $this->paginate($query);

        $this->set(compact('clienteResponsaveis','cliente_id'));
        $this->set('_serialize', ['clienteResponsaveis']);
    }

    public function all()
    {
        $query = $this->ClienteResponsaveis->find('all')
            ->contain(['Clientes', 'GrauParentescos', 'EstadoCivis', 'SituacaoCadastros', 'Users'])
            ->where(['ClienteResponsaveis.situacao_id = ' => '1']);

        if(!empty($this->request->query['cliente_id'])){
            $query->andWhere(['ClienteResponsaveis.cliente_id = ' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        }else{
            $cliente_id = null;
        }
        $clienteResponsaveis = $this->paginate($query);
        $this->set(compact('clienteResponsaveis','cliente_id'));
        $this->set('_serialize', ['clienteResponsaveis']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Responsavei id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteResponsavei = $this->ClienteResponsaveis->get($id, [
            'contain' => ['Clientes', 'GrauParentescos', 'EstadoCivis', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('clienteResponsavei', $clienteResponsavei);
        $this->set('_serialize', ['clienteResponsavei']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($cliente = null)
    {
        $clienteResponsavei = $this->ClienteResponsaveis->newEntity();
        if ($this->request->is('post')) {
            $clienteResponsavei = $this->ClienteResponsaveis->patchEntity($clienteResponsavei, $this->request->data);
            if ($this->ClienteResponsaveis->save($clienteResponsavei)) {
                $this->Flash->success(__('O cliente responsavei foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente responsavei não foi salvo. Por favor, tente novamente.'));
            }
        }
        $clientes = $this->ClienteResponsaveis->Clientes->find('list', ['limit' => 200]);
        $grauParentescos = $this->ClienteResponsaveis->GrauParentescos->find('list', ['limit' => 200]);
        $estadoCivis = $this->ClienteResponsaveis->EstadoCivis->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ClienteResponsaveis->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ClienteResponsaveis->Users->find('list', ['limit' => 200]);
        $this->set(compact('clienteResponsavei', 'clientes','cliente', 'grauParentescos', 'estadoCivis', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['clienteResponsavei']);
    }
    public function nadd($cliente)
    {
        $clienteResponsavei = $this->ClienteResponsaveis->newEntity();
        if ($this->request->is('post')) {
            $clienteResponsavei = $this->ClienteResponsaveis->patchEntity($clienteResponsavei, $this->request->data);
            if ($this->ClienteResponsaveis->save($clienteResponsavei)) {
              echo $cliente;
            } else {
                echo -1;
            }
            $this->autoRender = false;
        }

        $financeiro=null;
        if(!empty($cliente)){
            $financeiro = $this->ClienteResponsaveis->find('all')
                ->where(['ClienteResponsaveis.cliente_id'=>$cliente])
                ->andWhere(['ClienteResponsaveis.financeiro'=>1])
                ->andWhere(['ClienteResponsaveis.situacao_id'=>1])->count();

        }
        $grauParentescos = $this->ClienteResponsaveis->GrauParentescos->find('list', ['limit' => 200]);
        $estadoCivis = $this->ClienteResponsaveis->EstadoCivis->find('list', ['limit' => 200]);

        $this->set(compact('clienteResponsavei', 'cliente', 'grauParentescos', 'estadoCivis','financeiro'));
        $this->set('_serialize', ['clienteResponsavei']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Responsavei id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null,$cliente)
    {
        $clienteResponsavei = $this->ClienteResponsaveis->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteResponsavei = $this->ClienteResponsaveis->patchEntity($clienteResponsavei, $this->request->data);
            if ($this->ClienteResponsaveis->save($clienteResponsavei)) {
                echo $cliente;
            } else {
                echo -1;
            }
            $this->autoRender = false;
        }

        $financeiro=null;
            $financeiro = $this->ClienteResponsaveis->find('all')
                ->where(['ClienteResponsaveis.cliente_id'=>$clienteResponsavei->cliente_id])
                ->andWhere(['ClienteResponsaveis.financeiro'=>1])
                ->andWhere(['ClienteResponsaveis.id <> '=>$id])
                ->andWhere(['ClienteResponsaveis.situacao_id'=>1])->count();

        $grauParentescos = $this->ClienteResponsaveis->GrauParentescos->find('list', ['limit' => 200]);
        $estadoCivis = $this->ClienteResponsaveis->EstadoCivis->find('list', ['limit' => 200]);
        $index = !empty($this->request->query['index']) ? 1 : 0;
        $this->set(compact('clienteResponsavei', 'cliente', 'grauParentescos', 'estadoCivis','index','financeiro'));
        $this->set('_serialize', ['clienteResponsavei']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Responsavei id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteResponsavei = $this->ClienteResponsaveis->get($id);
                $clienteResponsavei->situacao_id = 2;
        if ($this->ClienteResponsaveis->save($clienteResponsavei)) {
            $this->Flash->success(__('O cliente responsavei foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente responsavei não foi deletado! Tente novamente mais tarde.'));
        }
            return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Contrato id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->ClienteResponsaveis->find('all')
            ->where(['ClienteResponsaveis.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ClienteResponsaveis.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Contrato id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $ClienteResponsaveis = $this->ClienteResponsaveis->get($this->request->data['id']);
            $res = ['nome'=>$ClienteResponsaveis->nome,'id'=>$ClienteResponsaveis->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
