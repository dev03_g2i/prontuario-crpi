<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;
use PDOException;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Users->find('all')
            ->contain(['MedicoResponsaveis', 'GrupoAgendas'])
            ->where(['Users.situacao_id' => 1]);

        if(!empty($this->request->getQuery('nome')))
        {
            $query->andWhere(['Users.nome LIKE ' => '%'.$this->request->getQuery('nome').'%']);
        }

        $users = $this->paginate($query);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);

    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['GrupoUsuarios','SituacaoCadastros']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function cadastro($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['GrupoUsuarios','SituacaoCadastros']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);

            if($this->request->data['caminho']['error'] == 0){
                $user->caminho = $this->request->data['caminho'];
            }else{
                $user->caminho = '';
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('O user foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O user não foi salvo. Por favor, tente novamente.'));
            }
        }
        $grupoUsuarios = $this->Users->GrupoUsuarios->find('list', ['limit' => 200])->orderAsc('nome');
        $medicoResponsaveis = $this->Users->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('nome');
        $grupoAgendas = $this->Users->GrupoAgendas->find('list', ['limit' => 200])->orderAsc('nome');
        $this->set(compact('user','grupoUsuarios','medicoResponsaveis','grupoAgendas'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if(!empty($this->request->data['new_password'])){
                $user->password = $this->request->data['new_password'];
            }

            if($this->request->data['caminho']['error'] == 0){
                $user->caminho = $this->request->data['caminho'];
            }else{
                $user->caminho = '';
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('O usuário foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O user não foi salvo. Por favor, tente novamente.'));
            }
        }
        $medicoResponsaveis = $this->Users->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('nome');
        $grupoUsuarios = $this->Users->GrupoUsuarios->find('list', ['limit' => 200])->orderAsc('nome');
        $grupoAgendas = $this->Users->GrupoAgendas->find('list', ['limit' => 200])->orderAsc('nome');
        $this->set(compact('user','grupoUsuarios','medicoResponsaveis','grupoAgendas'));
        $this->set('_serialize', ['user']);
    }

    public function alterSenha($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('O user foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O user não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }
    public function alterImage($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->response->type('json');
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $res=[
                    'res'=>1,
                    'caminho'=>$user->caminho,
                    'caminho_dir'=>$user->caminho_dir
                ];
            } else {
                $res=[
                    'res'=>'erro'
                ];
            }

            $this->response->body(json_encode($res));
            $this->autoRender=false;
        }

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->situacao_id = 2;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('O user foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O user não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->Users->find('all')
            ->where(['Users.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Users.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $user = $this->Users->get($this->request->data['id']);
            $res = ['nome'=>$user->nome,'id'=>$user->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function login(){
        $this->viewBuilder()->layout('login');
        if($this->request->is('post')){
            $usuarios = $this->Auth->identify();

            if($usuarios){
                $this->Auth->setUser($usuarios);
                if($usuarios['codigo_medico'] != null){
                    if($usuarios['user_diagnostico'] == 1){// Radiologista
                        return $this->redirect(['controller' => 'Laudos', 'action' => 'index']);
                    }else{
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                }else{
                    return $this->redirect(['controller' => 'Agendas', 'action' => 'index']);
                }
            }

            $this->Flash->error('Seu login ou senha estão incorretos!');


        }
    }

    public function logout(){
        $this->Flash->success('Você foi desconectado com sucesso!');
        return $this->redirect($this->Auth->logout());
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Auth->allow(['logout','add']);
    }

    public function deleteImg($id)
    {
        $userAnexos = $this->Users->get($id);
        $caminho = WWW_ROOT . 'img/users/caminho/' . $userAnexos->caminho_dir . '/' . $userAnexos->caminho;

        $file = new File($caminho);
        if ($file->exists()) {
            if ($file->delete()) {
                $userAnexos->caminho = '';
                $userAnexos->caminho_dir = '';
                $this->Users->save($userAnexos);
            }
            $file->close();
        }
    }

    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name']) && !empty($this->request->data['value'])){
            $name = $this->request->data['name'];
            $user = $this->Users->findById($this->request->data['pk'])->first();
            $user->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->Users->save($user)) {
                $res = ['res' => $user->$name,'msg'=>'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

    public function habilitaCaixaPorUsuario($user_id)
    {
        $this->loadModel('ConfiguracaoRelCaixa');
        $this->loadModel('ConfiguracaoRelCaixaUsuario');

        if ($this->request->is('post')) {

            $this->response->type('JSON');
            $this->autoRender = false;
            $this->viewBuilder()->setLayout('ajax');

            $res = ['resposta' => false];

            $relatorio_id = $this->request->getData('relatorio_id');
            $statusAtual = $this->ConfiguracaoRelCaixaUsuario
                ->find()
                ->where(['user_id' => $user_id, 'configuracao_rel_caixa_id' => $relatorio_id])
                ->first();

            if (empty($statusAtual)) {
                /**
                 * Se o $statusAtual = vazio, significa que é preciso adicionar na tabela ConfiguracaoRelCaixaUsuario
                 *    um registro vinculando usuário <-> relatório
                 */
                $caixaRelUser = $this->ConfiguracaoRelCaixaUsuario->newEntity();
                $caixaRelUser->user_id = $user_id;
                $caixaRelUser->configuracao_rel_caixa_id = $relatorio_id;
                try {
                    $this->ConfiguracaoRelCaixaUsuario->save($caixaRelUser);
                    $res['resposta'] = true;
                } catch (PDOException $exception) {
                    $res['resposta'] = false;
                }
            } else {
                /**
                 * Se o $statusAtual != vazio, significa que é preciso retirar na tabela ConfiguracaoRelCaixaUsuario 
                 *    o registro que vincula usuário <-> relatório
                 */
                try {
                    $registro = $this->ConfiguracaoRelCaixaUsuario->query();
                    $registro->delete()->where(['user_id' => $user_id, 'configuracao_rel_caixa_id' => $relatorio_id])->execute();
                    $res['resposta'] = true;
                } catch (PDOException $exception) {
                    $res['resposta'] = false;
                }
            }

            return $this->response->withStringBody(json_encode($res));
        }

        $relatorioCaixas = $this->ConfiguracaoRelCaixa->find('all')
            ->contain(['ConfiguracaoRelCaixaUsuario' => function ($q) use ($user_id) {
                return $q->where(['ConfiguracaoRelCaixaUsuario.user_id' => $user_id]);
            }])
            ->where(['situacao_id' => 1])
            ->orderAsc('ConfiguracaoRelCaixa.descricao');

        $relatorioCaixas = $this->paginate($relatorioCaixas);
        $user = $this->Users->find()->where(['id' => $user_id])->first();

        $this->set(compact('relatorioCaixas', 'user'));
        $this->set('_serialize', ['relatorioCaixas']);
    }
}
