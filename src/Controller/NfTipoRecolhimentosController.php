<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NfTipoRecolhimentos Controller
 *
 * @property \App\Model\Table\NfTipoRecolhimentosTable $NfTipoRecolhimentos
 */
class NfTipoRecolhimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $nfTipoRecolhimentos = $this->paginate($this->NfTipoRecolhimentos);


        $this->set(compact('nfTipoRecolhimentos'));
        $this->set('_serialize', ['nfTipoRecolhimentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf Tipo Recolhimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nfTipoRecolhimento = $this->NfTipoRecolhimentos->get($id, [
            'contain' => []
        ]);

        $this->set('nfTipoRecolhimento', $nfTipoRecolhimento);
        $this->set('_serialize', ['nfTipoRecolhimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nfTipoRecolhimento = $this->NfTipoRecolhimentos->newEntity();
        if ($this->request->is('post')) {
            $nfTipoRecolhimento = $this->NfTipoRecolhimentos->patchEntity($nfTipoRecolhimento, $this->request->data);
            if ($this->NfTipoRecolhimentos->save($nfTipoRecolhimento)) {
                $this->Flash->success(__('O nf tipo recolhimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf tipo recolhimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfTipoRecolhimento'));
        $this->set('_serialize', ['nfTipoRecolhimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf Tipo Recolhimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nfTipoRecolhimento = $this->NfTipoRecolhimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nfTipoRecolhimento = $this->NfTipoRecolhimentos->patchEntity($nfTipoRecolhimento, $this->request->data);
            if ($this->NfTipoRecolhimentos->save($nfTipoRecolhimento)) {
                $this->Flash->success(__('O nf tipo recolhimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf tipo recolhimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfTipoRecolhimento'));
        $this->set('_serialize', ['nfTipoRecolhimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf Tipo Recolhimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nfTipoRecolhimento = $this->NfTipoRecolhimentos->get($id);
                $nfTipoRecolhimento->situacao_id = 2;
        if ($this->NfTipoRecolhimentos->save($nfTipoRecolhimento)) {
            $this->Flash->success(__('O nf tipo recolhimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf tipo recolhimento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf Tipo Recolhimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->NfTipoRecolhimentos->find('all')
        ->where(['NfTipoRecolhimentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('NfTipoRecolhimentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf Tipo Recolhimento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nfTipoRecolhimento = $this->NfTipoRecolhimentos->get($this->request->data['id']);
            $res = ['nome'=>$nfTipoRecolhimento->nome,'id'=>$nfTipoRecolhimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
