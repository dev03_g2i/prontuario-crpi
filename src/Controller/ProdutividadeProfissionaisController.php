<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * ProdutividadeProfissionais Controller
 *
 * @property \App\Model\Table\ProdutividadeProfissionaisTable $ProdutividadeProfissionais
 */
class ProdutividadeProfissionaisController extends AppController
{

    public $components = ['Data', 'Query', 'Relatorios'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ProdutividadeProfissionais->find('all')
            ->contain(['Convenios', 'MedicoResponsaveis', 'Procedimentos', 'Users', 'SituacaoCadastros'])
            ->where(['ProdutividadeProfissionais.situacao_id' => 1]);

        $profissional_id = null;
        if (!empty($this->request->query('profissional_id'))) {
            $profissional_id = $this->request->query('profissional_id');
            $query->andWhere(['ProdutividadeProfissionais.profissional_id' => $profissional_id]);
        }

        if (!empty($this->request->query('procedimento_id'))) {
            $query->andWhere(['ProdutividadeProfissionais.procedimento_id' => $this->request->query('procedimento_id')]);
        }

        $produtividadeProfissionais = $this->paginate($query);

        $convenios = $this->ProdutividadeProfissionais->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');

        $this->set(compact('produtividadeProfissionais', 'profissional_id', 'convenios'));
        $this->set('_serialize', ['produtividadeProfissionais']);

    }

    public function listar()
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempProdutividadeprofissionais');
        $this->loadModel('Solicitantes');

        $solicitantes = $this->Solicitantes->find('list')->where(['Solicitantes.situacao_id' => 1]);

        $query = $this->TempProdutividadeprofissionais->find('all')
            ->order(['TempProdutividadeprofissionais.data_atendimento' => 'ASC', 'TempProdutividadeprofissionais.paciente' => 'ASC']);

        $escondeDados = false;
        if (!empty($this->request->query('unidade_id'))) {
            $escondeDados = true;
        }

        $this->paginate = [
            'limit' => 50
        ];
        $temp = $this->paginate($query);

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Procedimentos->find('list', ['limit' => 200])->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorio = $this->Relatorios->getTiposRelatorioProdutividade();
        $this->set(compact('solicitantes', 'unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'temp', 'situacao_faturas', 'escondeDados', 'procedimentos', 'tiposRelatorio'));
    }

    public function listarExecutantes()
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempProdutividadeprofissionais');
        $this->loadModel('Solicitantes');

        $solicitantes = $this->Solicitantes->find('list')->where(['Solicitantes.situacao_id' => 1]);

        $query = $this->TempProdutividadeprofissionais->find('all')
            ->order(['TempProdutividadeprofissionais.data_atendimento' => 'ASC', 'TempProdutividadeprofissionais.paciente' => 'ASC']);

        $escondeDados = false;
        if (!empty($this->request->query('unidade_id'))) {
            $escondeDados = true;
        }

        $this->paginate = [
            'limit' => 50
        ];
        $temp = $this->paginate($query);

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Procedimentos->find('list', ['limit' => 200])->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorio = $this->Relatorios->getTiposRelatorioProdutividadeExecutante();
        $this->set(compact('solicitantes', 'unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'temp', 'situacao_faturas', 'escondeDados', 'procedimentos', 'tiposRelatorio'));
    }

    public function listarSolicitantes()
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempProdutividadeprofissionais');
        $this->loadModel('Solicitantes');

        $solicitantes = $this->Solicitantes->find('list')->where(['Solicitantes.situacao_id' => 1]);

        $query = $this->TempProdutividadeprofissionais->find('all')
            ->order(['TempProdutividadeprofissionais.data_atendimento' => 'ASC', 'TempProdutividadeprofissionais.paciente' => 'ASC']);

        $escondeDados = false;
        if (!empty($this->request->query('unidade_id'))) {
            $escondeDados = true;
        }

        $this->paginate = [
            'limit' => 50
        ];
        $temp = $this->paginate($query);

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Procedimentos->find('list', ['limit' => 200])->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorio = $this->Relatorios->getTiposRelatorioProdutividadeSolicitante();
        $this->set(compact('solicitantes', 'unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'temp', 'situacao_faturas', 'escondeDados', 'procedimentos', 'tiposRelatorio'));
    }

    /**
     * View method
     *
     * @param string|null $id Produtividade Profissionai id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produtividadeProfissionai = $this->ProdutividadeProfissionais->get($id, [
            'contain' => ['Convenios', 'MedicoResponsaveis', 'Procedimentos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('produtividadeProfissionai', $produtividadeProfissionai);
        $this->set('_serialize', ['produtividadeProfissionai']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($profissional_id = null)
    {
        $produtividadeProfissionai = $this->ProdutividadeProfissionais->newEntity();
        if ($this->request->is('post')) {
            $produtividadeProfissionai = $this->ProdutividadeProfissionais->patchEntity($produtividadeProfissionai, $this->request->data);

            $this->autoRender = false;
            $this->response->type('json');

            $check = $this->ProdutividadeProfissionais->find('all')
                ->where(['ProdutividadeProfissionais.situacao_id' => 1])
                ->andWhere(['ProdutividadeProfissionais.convenio_id' => $this->request->data('convenio_id')])
                ->andWhere(['ProdutividadeProfissionais.profissional_id' => $profissional_id])
                ->andWhere(['ProdutividadeProfissionais.procedimento_id' => $this->request->data('procedimento_id')])
                ->count();

            if ($check > 0) {
                $json = json_encode(['res' => $check, 'msg' => 'Já existe uma produtividade cadastrada, por favor informe outro para continuar!']);
            } else {
                if (!empty($profissional_id)) {
                    $produtividadeProfissionai->profissional_id = $profissional_id;
                }
                if ($this->ProdutividadeProfissionais->save($produtividadeProfissionai)) {
                    $json = $json = json_encode(['res' => 0, 'msg' => 'A produtividade profissionais foi salvo com sucesso!']);
                } else {
                    $json = $json = json_encode(['res' => 1, 'msg' => 'O produtividade profissionai não foi salvo. Por favor, tente novamente.']);
                }
            }
            $this->response->body($json);
        }

        $convenios = $this->ProdutividadeProfissionais->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');

        $this->set(compact('produtividadeProfissionai', 'convenios'));
        $this->set('_serialize', ['produtividadeProfissionai']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Produtividade Profissionai id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produtividadeProfissionai = $this->ProdutividadeProfissionais->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produtividadeProfissionai = $this->ProdutividadeProfissionais->patchEntity($produtividadeProfissionai, $this->request->data);
            if ($this->ProdutividadeProfissionais->save($produtividadeProfissionai)) {
                $this->Flash->success(__('O produtividade profissionai foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O produtividade profissionai não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('produtividadeProfissionai'));
        $this->set('_serialize', ['produtividadeProfissionai']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produtividade Profissionai id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $produtividadeProfissionai = $this->ProdutividadeProfissionais->get($id);
        $produtividadeProfissionai->situacao_id = 2;
        if ($this->ProdutividadeProfissionais->save($produtividadeProfissionai)) {
            $this->Flash->success(__('O produtividade profissionai foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O produtividade profissionai não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Produtividade Profissionai id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->ProdutividadeProfissionais->find('all')
            ->where(['ProdutividadeProfissionais.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ProdutividadeProfissionais.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Produtividade Profissionai id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $produtividadeProfissionai = $this->ProdutividadeProfissionais->get($this->request->data['id']);
            $res = ['nome' => $produtividadeProfissionai->nome, 'id' => $produtividadeProfissionai->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function salvarTemp()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $conn = ConnectionManager::get('default');

        $this->Query->clearTemp($conn, 'temp_produtividadeprofissionais');

        $and = '';
        if (!empty($this->request->data('situacao_fatura_id'))) {
            $and .= "AND ap.situacao_fatura_id = " . $this->request->data('situacao_fatura_id');
        }

        if (!empty($this->request->data('unidade_id'))) {
            $and .= " AND a.unidade_id = " . $this->request->data('unidade_id');
        }

        if (!empty($this->request->data('origen_id'))) {
            $and .= " AND a.origen_id = " . $this->request->data('origen_id');
        }

        if (!empty($this->request->getData('convenios'))) {
            $idsConvenios = join(',', $this->request->getData('convenios'));
            $and .= " AND a.convenio_id IN ($idsConvenios)";
        }

        if (!empty($this->request->data('grupos'))) {
            $ids = join(',', $this->request->data('grupos'));
            $and .= " AND p.grupo_id IN ($ids)";
        }

        if (!empty($this->request->data('procedimentos'))) {
            $ids = join(',', $this->request->data('procedimentos'));
            $and .= " AND ap.procedimento_id IN ($ids)";
        }
        if (!empty($this->request->getData('solicitantes'))) {
            $ids = join(',', $this->request->getData('solicitantes'));
            $and .= " AND a.solicitante_id IN ($ids)";
        }
        if (!empty($this->request->data('medicos'))) {
            $ids = join(',', $this->request->data('medicos'));
            $and .= " AND ap.medico_id IN ($ids)";
        }
        if (!empty($this->request->data('status_faturamento'))) {
            $and .= " AND ap.status_faturamento = " . $this->request->data('status_faturamento');
        }

        if (!empty($this->request->data('situacao_datas'))) {
            $situacao_datas = $this->request->data('situacao_datas');

            $prefix = ($situacao_datas == 'data') ? 'a' : 'ap';
        } else {
            $situacao_datas = 'data';
            $prefix = 'a';
        }
        $data_inicio = $this->Data->DataSQL($this->request->data['data_inicio']);
        $and .= " AND " . $prefix . "." . $situacao_datas . " >= '" . $data_inicio . "'";
        $data_fim = $this->Data->DataSQL($this->request->data['data_fim']);
        $and .= " AND " . $prefix . "." . $situacao_datas . " <= '" . $data_fim . "'";

        $result = ($this->Query->insertTempProdutividadeProfissionais($conn, $and)) ? 'success' : 'error';

        $this->loadModel('TempProdutividadeprofissionais');
        $query = $this->TempProdutividadeprofissionais->find('all');
        //$count = ($query->count() != null) ? $query->count() : 0;
        $query_select = $query->select([
            //'valor_fatura' =>  $query->func()->sum('valor_fatura'),
            'total' => $query->func()->sum('total'),
            'valor_caixa' => $query->func()->sum('valor_caixa'),
            'valor_prod_fatura' => $query->func()->sum('valor_prod_fatura'),
            'valor_prod_caixa' => $query->func()->sum('valor_prod_caixa'),
            'valor_prodrec_convenio' => $query->func()->sum('valor_prodrec_convenio'),
            'valor_prodclin_recebimento' => $query->func()->sum('valor_prodclin_recebimento'),
            'valor_recebido' => $query->func()->sum('valor_recebido'),
            'valor_prodcaixa_clinica' => $query->func()->sum('valor_prodcaixa_clinica'),
            'count' => $query->func()->sum('quantidade')
        ])->toArray();

        //$valor_fatura = ($query_select[0]->valor_fatura != null) ? $query_select[0]->valor_fatura : '0.00';
        $total = ($query_select[0]->total != null) ? $query_select[0]->total : '0.00';
        $valor_caixa = ($query_select[0]->valor_caixa != null) ? $query_select[0]->valor_caixa : '0.00';
        $valor_prod_fatura = ($query_select[0]->valor_prod_fatura != null) ? $query_select[0]->valor_prod_fatura : '0.00';
        $valor_prod_caixa = ($query_select[0]->valor_prod_caixa != null) ? $query_select[0]->valor_prod_caixa : '0.00';
        $valor_prod_convenio = ($query_select[0]->valor_prodrec_convenio != null) ? $query_select[0]->valor_prodrec_convenio : '0.00';
        $valor_prodclin_recebimento = ($query_select[0]->valor_prodclin_recebimento != null) ? $query_select[0]->valor_prodclin_recebimento : '0.00';
        $valor_recebido = ($query_select[0]->valor_recebido != null) ? $query_select[0]->valor_recebido : '0.00';
        $valor_prodcaixa_clinica = ($query_select[0]->valor_prodcaixa_clinica != null) ? $query_select[0]->valor_prodcaixa_clinica : '0.00';
        $count = ($query_select[0]->count != null) ? $query_select[0]->count : '0';

        $this->response->body(json_encode([
            'result' => $result,
            'count' => $count,
            //'valor_fatura' => $valor_fatura,
            'valor_fatura' => $total,
            'valor_caixa' => $valor_caixa,
            'valor_prod_fatura' => $valor_prod_fatura,
            'valor_prod_caixa' => $valor_prod_caixa,
            'valor_prod_convenio' => $valor_prod_convenio,
            'valor_prodclin_recebimento' => $valor_prodclin_recebimento,
            'valor_recebido' => $valor_recebido,
            'valor_prodcaixa_clinica' => $valor_prodcaixa_clinica
        ]));
    }

    public function deleteModal()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $produtividadePrfissional = $this->ProdutividadeProfissionais->get($this->request->data['id']);
        $produtividadePrfissional->situacao_id = 2;
        $res = ($this->ProdutividadeProfissionais->save($produtividadePrfissional)) ? 0 : 1;

        $json = json_encode(['res' => $res, 'view' => 'index']);
        $this->response->body($json);
    }

    public function receberTodos()
    {
        if ($this->request->is('post')) {
            $data = $this->Data->DataSQL($this->request->data('producao_pg_dt'));
            $user_id = $this->Auth->user('id');
            $user_dt = date('Y-m-d H:i:s');
            $conn = ConnectionManager::get('default');
            $receberTodos = $this->Query->prodReceberTodos($data, $user_id, $user_dt, $conn);
        }
    }

}
