<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaceItens Controller
 *
 * @property \App\Model\Table\FaceItensTable $FaceItens
 */
class FaceItensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['AtendimentoItens', 'Faces', 'Users', 'SituacaoCadastros']
                        ,'conditions' => ['FaceItens.situacao_id = ' => '1']
                    ];
        $faceItens = $this->paginate($this->FaceItens);

        $this->set(compact('faceItens'));
        $this->set('_serialize', ['faceItens']);
    }

    /**
     * View method
     *
     * @param string|null $id Face Iten id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faceIten = $this->FaceItens->get($id, [
            'contain' => ['AtendimentoItens', 'Faces', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('faceIten', $faceIten);
        $this->set('_serialize', ['faceIten']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faceIten = $this->FaceItens->newEntity();
        if ($this->request->is('post')) {
            $faceIten = $this->FaceItens->patchEntity($faceIten, $this->request->data);
            if ($this->FaceItens->save($faceIten)) {
                $this->Flash->success(__('O face iten foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O face iten não foi salvo. Por favor, tente novamente.'));
            }
        }
        $atendimentoItens = $this->FaceItens->AtendimentoItens->find('list', ['limit' => 200]);
        $faces = $this->FaceItens->Faces->find('list', ['limit' => 200]);
        $users = $this->FaceItens->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->FaceItens->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('faceIten', 'atendimentoItens', 'faces', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['faceIten']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Face Iten id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faceIten = $this->FaceItens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faceIten = $this->FaceItens->patchEntity($faceIten, $this->request->data);
            if ($this->FaceItens->save($faceIten)) {
                $this->Flash->success(__('O face iten foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O face iten não foi salvo. Por favor, tente novamente.'));
            }
        }
        $atendimentoItens = $this->FaceItens->AtendimentoItens->find('list', ['limit' => 200]);
        $faces = $this->FaceItens->Faces->find('list', ['limit' => 200]);
        $users = $this->FaceItens->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->FaceItens->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('faceIten', 'atendimentoItens', 'faces', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['faceIten']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Face Iten id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faceIten = $this->FaceItens->get($id);
                $faceIten->situacao_id = 2;
        if ($this->FaceItens->save($faceIten)) {
            $this->Flash->success(__('O face iten foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O face iten não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
