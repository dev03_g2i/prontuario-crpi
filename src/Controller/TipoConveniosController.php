<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoConvenios Controller
 *
 * @property \App\Model\Table\TipoConveniosTable $TipoConvenios
 */
class TipoConveniosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
                        ,'conditions' => ['TipoConvenios.situacao_id = ' => '1']
                    ];
        $tipoConvenios = $this->paginate($this->TipoConvenios);

        $this->set(compact('tipoConvenios'));
        $this->set('_serialize', ['tipoConvenios']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Convenio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoConvenio = $this->TipoConvenios->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('tipoConvenio', $tipoConvenio);
        $this->set('_serialize', ['tipoConvenio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoConvenio = $this->TipoConvenios->newEntity();
        if ($this->request->is('post')) {
            $tipoConvenio = $this->TipoConvenios->patchEntity($tipoConvenio, $this->request->data);
            if ($this->TipoConvenios->save($tipoConvenio)) {
                $this->Flash->success(__('O tipo convenio foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo convenio não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoConvenios->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoConvenios->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoConvenio', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoConvenio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Convenio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoConvenio = $this->TipoConvenios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoConvenio = $this->TipoConvenios->patchEntity($tipoConvenio, $this->request->data);
            if ($this->TipoConvenios->save($tipoConvenio)) {
                $this->Flash->success(__('O tipo convenio foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo convenio não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoConvenios->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoConvenios->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoConvenio', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoConvenio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Convenio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoConvenio = $this->TipoConvenios->get($id);
                $tipoConvenio->situacao_id = 2;
        if ($this->TipoConvenios->save($tipoConvenio)) {
            $this->Flash->success(__('O tipo convenio foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo convenio não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
