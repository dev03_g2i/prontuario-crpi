<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoHistorias Controller
 *
 * @property \App\Model\Table\TipoHistoriasTable $TipoHistorias
 */
class TipoHistoriasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
                        ,'conditions' => ['TipoHistorias.situacao_id = ' => '1']
            ,'limit'=>2
                    ];

        $tipoHistorias = $this->paginate($this->TipoHistorias);

        $this->set(compact('tipoHistorias'));
        $this->set('_serialize', ['tipoHistorias']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Historia id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoHistoria = $this->TipoHistorias->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('tipoHistoria', $tipoHistoria);
        $this->set('_serialize', ['tipoHistoria']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoHistoria = $this->TipoHistorias->newEntity();
        if ($this->request->is('post')) {
            $tipoHistoria = $this->TipoHistorias->patchEntity($tipoHistoria, $this->request->data);
            if ($this->TipoHistorias->save($tipoHistoria)) {
                $this->Flash->success(__('O tipo historia foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo historia não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoHistorias->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoHistorias->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoHistoria', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoHistoria']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Historia id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoHistoria = $this->TipoHistorias->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoHistoria = $this->TipoHistorias->patchEntity($tipoHistoria, $this->request->data);
            if ($this->TipoHistorias->save($tipoHistoria)) {
                $this->Flash->success(__('O tipo historia foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo historia não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoHistorias->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoHistorias->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoHistoria', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoHistoria']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Historia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tipoHistoria = $this->TipoHistorias->get($id);
                $tipoHistoria->situacao_id = 2;
        if ($this->TipoHistorias->save($tipoHistoria)) {
            $this->Flash->success(__('O tipo historia foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo historia não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
