<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GrauParentescos Controller
 *
 * @property \App\Model\Table\GrauParentescosTable $GrauParentescos
 */
class GrauParentescosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['GrauParentescos.situacao_id = ' => '1']
                    ];
        $grauParentescos = $this->paginate($this->GrauParentescos);

        $this->set(compact('grauParentescos'));
        $this->set('_serialize', ['grauParentescos']);
    }

    /**
     * View method
     *
     * @param string|null $id Grau Parentesco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grauParentesco = $this->GrauParentescos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('grauParentesco', $grauParentesco);
        $this->set('_serialize', ['grauParentesco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grauParentesco = $this->GrauParentescos->newEntity();
        if ($this->request->is('post')) {
            $grauParentesco = $this->GrauParentescos->patchEntity($grauParentesco, $this->request->data);
            if ($this->GrauParentescos->save($grauParentesco)) {
                $this->Flash->success(__('O grau parentesco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grau parentesco não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrauParentescos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->GrauParentescos->Users->find('list', ['limit' => 200]);
        $this->set(compact('grauParentesco', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['grauParentesco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grau Parentesco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grauParentesco = $this->GrauParentescos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grauParentesco = $this->GrauParentescos->patchEntity($grauParentesco, $this->request->data);
            if ($this->GrauParentescos->save($grauParentesco)) {
                $this->Flash->success(__('O grau parentesco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grau parentesco não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrauParentescos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->GrauParentescos->Users->find('list', ['limit' => 200]);
        $this->set(compact('grauParentesco', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['grauParentesco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grau Parentesco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grauParentesco = $this->GrauParentescos->get($id);
                $grauParentesco->situacao_id = 2;
        if ($this->GrauParentescos->save($grauParentesco)) {
            $this->Flash->success(__('O grau parentesco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grau parentesco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
