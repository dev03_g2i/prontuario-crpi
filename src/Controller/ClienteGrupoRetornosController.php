<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClienteGrupoRetornos Controller
 *
 * @property \App\Model\Table\ClienteGrupoRetornosTable $ClienteGrupoRetornos
 */
class ClienteGrupoRetornosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ClienteGrupoRetornos->find('all')
            ->contain(['Clientes', 'GrupoRetornos', 'SituacaoCadastros', 'Users'])
            ->where(['ClienteGrupoRetornos.situacao_id = ' => '1']);

            $cliente = null;
            if(!empty($this->request->query['cliente_id'])){
                $query->andWhere(['ClienteGrupoRetornos.cliente_id'=>$this->request->query['cliente_id']]);
                $cliente = $this->request->query['cliente_id'];
            }

        $clienteGrupoRetornos = $this->paginate($query);

        $this->set(compact('clienteGrupoRetornos','cliente'));
        $this->set('_serialize', ['clienteGrupoRetornos']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Grupo Retorno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteGrupoRetorno = $this->ClienteGrupoRetornos->get($id, [
            'contain' => ['Clientes', 'GrupoRetornos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('clienteGrupoRetorno', $clienteGrupoRetorno);
        $this->set('_serialize', ['clienteGrupoRetorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($cliente = null)
    {
        $clienteGrupoRetorno = $this->ClienteGrupoRetornos->newEntity();
        if ($this->request->is('post')) {
            $clienteGrupoRetorno = $this->ClienteGrupoRetornos->patchEntity($clienteGrupoRetorno, $this->request->data);
            if ($this->ClienteGrupoRetornos->save($clienteGrupoRetorno)) {
                $this->Flash->success(__('O cliente grupo retorno foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente grupo retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        if(!empty($cliente)){
            $cliente_gretornos = $this->ClienteGrupoRetornos->find('all')
                ->where(['ClienteGrupoRetornos.cliente_id'=>$cliente])
                ->andWhere(['ClienteGrupoRetornos.situacao_id'=>1]);
            var_dump($cliente_gretornos);
            $id_grupos=[];
            foreach ($cliente_gretornos as $c) {
                $id_grupos[] = $c->grupo_id;
            }
            if(!empty($id_grupos)) {
                $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200])->where(['GrupoRetornos.id not in' => $id_grupos]);
            }else{
                $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
            }
        }else{
            $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
        }
        $clientes = $this->ClienteGrupoRetornos->Clientes->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ClienteGrupoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ClienteGrupoRetornos->Users->find('list', ['limit' => 200]);
        $this->set(compact('clienteGrupoRetorno', 'clientes', 'grupoRetornos', 'situacaoCadastros', 'users','cliente'));
        $this->set('_serialize', ['clienteGrupoRetorno']);
    }
    public function nadd()
    {
        $clienteGrupoRetorno = $this->ClienteGrupoRetornos->newEntity();
        if ($this->request->is('post')) {
            $clienteGrupoRetorno = $this->ClienteGrupoRetornos->patchEntity($clienteGrupoRetorno, $this->request->data);
            if ($this->ClienteGrupoRetornos->save($clienteGrupoRetorno)) {
                $this->loadModel('GrupoRetornos');
                $grupo = $this->GrupoRetornos->get($clienteGrupoRetorno->grupo_id);
                $this->response->type('json');
                $json = json_encode(['id'=>$grupo->id,'nome'=>$grupo->nome]);
                $this->response->body($json);
            } else {
                echo 'erro';
            }

            $this->autoRender=false;
        }

        $cliente = null;
        if(!empty($this->request->query['cliente_id'])){
            $cliente = $this->request->query['cliente_id'];
            $cliente_gretornos = $this->ClienteGrupoRetornos->find('all')->where(['ClienteGrupoRetornos.cliente_id'=>$cliente,'ClienteGrupoRetornos.situacao_id'=>1]);
            $id_grupos=[];

            foreach ($cliente_gretornos as $c) {
                $id_grupos[] = $c->grupo_id;
            }
            if (!empty($id_grupos)) {
                $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200])->where(['GrupoRetornos.id not in' => $id_grupos]);
            } else {
                $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
            }
        }else {
            $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
        }

        $clientes = $this->ClienteGrupoRetornos->Clientes->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ClienteGrupoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ClienteGrupoRetornos->Users->find('list', ['limit' => 200]);
        $this->set(compact('clienteGrupoRetorno', 'clientes', 'grupoRetornos', 'situacaoCadastros', 'users','cliente'));
        $this->set('_serialize', ['clienteGrupoRetorno']);
    }
    public function naddmodal()
    {
        $clienteGrupoRetorno = $this->ClienteGrupoRetornos->newEntity();
        if ($this->request->is('post')) {
            $clienteGrupoRetorno = $this->ClienteGrupoRetornos->patchEntity($clienteGrupoRetorno, $this->request->data);
            if ($this->ClienteGrupoRetornos->save($clienteGrupoRetorno)) {
                $this->loadModel('GrupoRetornos');
                $grupo = $this->GrupoRetornos->get($clienteGrupoRetorno->grupo_id);
                $this->response->type('json');
                $json = json_encode(['id'=>$grupo->id,'nome'=>$grupo->nome]);
                $this->response->body($json);
            } else {
                echo 'erro';
            }

            $this->autoRender=false;
        }

        $cliente = null;
        if(!empty($this->request->query['cliente_id'])){
            $cliente = $this->request->query['cliente_id'];
            $cliente_gretornos = $this->ClienteGrupoRetornos->find('all')->where(['ClienteGrupoRetornos.cliente_id'=>$cliente,'ClienteGrupoRetornos.situacao_id'=>1]);
            $id_grupos=[];

            foreach ($cliente_gretornos as $c) {
                $id_grupos[] = $c->grupo_id;
            }
            if (!empty($id_grupos)) {
                $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200])->where(['GrupoRetornos.id not in' => $id_grupos]);
            } else {
                $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
            }
        }else {
            $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
        }

        $clientes = $this->ClienteGrupoRetornos->Clientes->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ClienteGrupoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ClienteGrupoRetornos->Users->find('list', ['limit' => 200]);
        $this->set(compact('clienteGrupoRetorno', 'clientes', 'grupoRetornos', 'situacaoCadastros', 'users','cliente'));
        $this->set('_serialize', ['clienteGrupoRetorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Grupo Retorno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clienteGrupoRetorno = $this->ClienteGrupoRetornos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteGrupoRetorno = $this->ClienteGrupoRetornos->patchEntity($clienteGrupoRetorno, $this->request->data);
            if ($this->ClienteGrupoRetornos->save($clienteGrupoRetorno)) {
                $this->Flash->success(__('O cliente grupo retorno foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente grupo retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $clientes = $this->ClienteGrupoRetornos->Clientes->find('list', ['limit' => 200]);
        $grupoRetornos = $this->ClienteGrupoRetornos->GrupoRetornos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ClienteGrupoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ClienteGrupoRetornos->Users->find('list', ['limit' => 200]);
        $this->set(compact('clienteGrupoRetorno', 'clientes', 'grupoRetornos', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['clienteGrupoRetorno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Grupo Retorno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteGrupoRetorno = $this->ClienteGrupoRetornos->get($id);
                $clienteGrupoRetorno->situacao_id = 2;
        if ($this->ClienteGrupoRetornos->save($clienteGrupoRetorno)) {
            $this->Flash->success(__('O cliente grupo retorno foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente grupo retorno não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    function fill(){

        $cliente_grupos = $this->ClienteGrupoRetornos->find('all')
                            ->contain(['GrupoRetornos'])
                            ->where(['ClienteGrupoRetornos.situacao_id'=>1]);
        $cliente_grupos->andWhere(['ClienteGrupoRetornos.cliente_id'=>$this->request->data['cliente_id']]);
            $dados = [];
        foreach ($cliente_grupos as $cliente_grupo) {
                if($cliente_grupo->has('grupo_retorno')){
                    $dados[] = [
                        'id'=>$cliente_grupo->grupo_retorno->id,
                        'nome'=>$cliente_grupo->grupo_retorno->nome
                    ];
                }
        }
        $this->response->type('json');
        $json = json_encode($dados);
        $this->response->body($json);
        $this->autoRender=false;
    }
}
