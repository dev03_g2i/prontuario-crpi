<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaPrecoartigo Controller
 *
 * @property \App\Model\Table\FaturaPrecoartigoTable $FaturaPrecoartigo
 */
class FaturaPrecoartigoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $query= $this->FaturaPrecoartigo->find('all')
            ->contain(['EstqArtigos', 'Convenios', 'Users', 'SituacaoCadastros'])
            ->where(['FaturaPrecoartigo.situacao_id' => 1]);

        $convenio_id = null;
        if(!empty($this->request->query('convenio_id'))){
            $convenio_id = $this->request->query('convenio_id');
            $query->andWhere(['FaturaPrecoartigo.convenio_id' => $convenio_id]);
        }
        $artigo_id = null;
        if(!empty($this->request->query('artigo_id'))){
            $artigo_id = $this->request->query('artigo_id');
            $query->andWhere(['FaturaPrecoartigo.artigo_id' => $artigo_id]);
        }

        $faturaPrecoartigo = $this->paginate($query);

        $artigos = $this->FaturaPrecoartigo->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('faturaPrecoartigo', 'artigos', 'convenio_id', 'artigo_id'));
        $this->set('_serialize', ['faturaPrecoartigo']);
    }

    /**
     * View method
     *
     * @param string|null $id Fatura Precoartigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaPrecoartigo = $this->FaturaPrecoartigo->get($id, [
            'contain' => ['Artigos', 'Convenios', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('faturaPrecoartigo', $faturaPrecoartigo);
        $this->set('_serialize', ['faturaPrecoartigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convenio_id = null;
        if(!empty($this->request->query('convenio_id'))){
            $convenio_id = $this->request->query('convenio_id');
        }
        $artigo_id = null;
        if(!empty($this->request->query('artigo_id'))){
            $artigo_id = $this->request->query('artigo_id');
        }

        $faturaPrecoartigo = $this->FaturaPrecoartigo->newEntity();
        if ($this->request->is('post')) {
            $faturaPrecoartigo = $this->FaturaPrecoartigo->patchEntity($faturaPrecoartigo, $this->request->data);

            $this->autoRender=false;
            $this->response->type('json');

            $check = $this->FaturaPrecoartigo->find('all')
                ->where(['FaturaPrecoartigo.artigo_id' => $this->request->data('artigo_id')])
                ->where(['FaturaPrecoartigo.convenio_id' => $this->request->data('convenio_id')])
                ->count();

            if($check > 0) {
                $json = json_encode(['res' => $check, 'msg' => 'Já existe um artigo cadastrado para este convênio, por favor informe outro para continuar!']);
            }else {
                if ($this->FaturaPrecoartigo->save($faturaPrecoartigo)) {
                    $json = $json = json_encode(['res' => 0, 'msg' => 'Preço artigo foi salvo com sucesso!']);
                } else {
                    $json = $json = json_encode(['res' => 1, 'msg' => 'Preço artigo não foi salvo, Por favor tente novamente mais tarde!']);
                }
            }
            $this->response->body($json);

        }

        $artigos = $this->FaturaPrecoartigo->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('faturaPrecoartigo', 'artigos', 'convenio_id', 'artigo_id'));
        $this->set('_serialize', ['faturaPrecoartigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Precoartigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaPrecoartigo = $this->FaturaPrecoartigo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaPrecoartigo = $this->FaturaPrecoartigo->patchEntity($faturaPrecoartigo, $this->request->data);
            if ($this->FaturaPrecoartigo->save($faturaPrecoartigo)) {
                $this->Flash->success(__('O fatura precoartigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura precoartigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $artigos = $this->FaturaPrecoartigo->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('faturaPrecoartigo', 'artigos'));
        $this->set('_serialize', ['faturaPrecoartigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Precoartigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $faturaPrecoartigo = $this->FaturaPrecoartigo->get($id);
        $faturaPrecoartigo->situacao_id = 2;
        if ($this->FaturaPrecoartigo->save($faturaPrecoartigo)) {
            $this->Flash->success(__('O fatura precoartigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura precoartigo não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Fatura Precoartigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->FaturaPrecoartigo->find('all')
            ->where(['FaturaPrecoartigo.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaPrecoartigo.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Fatura Precoartigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $faturaPrecoartigo = $this->FaturaPrecoartigo->get($this->request->data['id']);
            $res = ['nome'=>$faturaPrecoartigo->nome,'id'=>$faturaPrecoartigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
