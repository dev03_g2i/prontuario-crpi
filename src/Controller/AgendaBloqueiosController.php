<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use DateInterval;
use DatePeriod;
use DateTime;

/**
 * AgendaBloqueios Controller
 *
 * @property \App\Model\Table\AgendaBloqueiosTable $AgendaBloqueios
 */
class AgendaBloqueiosController extends AppController
{

    public $components = ['Data'];
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->AgendaBloqueios->find('all')
            ->contain(['GrupoAgendas', 'SituacaoCadastros', 'Users', 'Agendas'])
            ->where(['AgendaBloqueios.situacao_id' => 1]);

        $grupo_agenda_id = null;
        if(!empty($this->request->query('grupo_agenda_id'))){
            $grupo_agenda_id = $this->request->query('grupo_agenda_id');
            $query->andWhere(['AgendaBloqueios.grupo_agenda_id' => $grupo_agenda_id]);
        }

        $agendaBloqueios = $this->paginate($query);

        $this->set(compact('agendaBloqueios', 'grupo_agenda_id'));
        $this->set('_serialize', ['agendaBloqueios']);

    }

    /**
     * View method
     *
     * @param string|null $id Agenda Bloqueio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendaBloqueio = $this->AgendaBloqueios->get($id, [
            'contain' => ['GrupoAgendas', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('agendaBloqueio', $agendaBloqueio);
        $this->set('_serialize', ['agendaBloqueio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($grupo_agenda_id = null)
    {
        $this->loadModel('ConfiguracaoPeriodos');
        $periodos = $this->ConfiguracaoPeriodos->find('all');
        $agendaBloqueio = $this->AgendaBloqueios->newEntity();
        if ($this->request->is('post')) {

            $agendaBloqueio = $this->AgendaBloqueios->patchEntity($agendaBloqueio, $this->request->data);
            $agendaBloqueio->inicio = $this->Data->formatDatetime($this->request->data('inicio'));
            $agendaBloqueio->fim = $this->Data->formatDatetime($this->request->data('fim'));

            if(!empty($grupo_agenda_id)){
                $agendaBloqueio->grupo_agenda_id = $grupo_agenda_id;
            }
            if ($this->AgendaBloqueios->save($agendaBloqueio)) {

                $this->loadModel('Agendas');
                $erro = 0;

                $inicio = new Time($agendaBloqueio->inicio);
                $fim = new Time($agendaBloqueio->fim);
                $intervalo = $inicio->diff($fim);
                $inicio->modify('-1 days');

                for($i=0; $i < $intervalo->d+1; $i++){
                    $inicio->modify('+1 days');
                    $dataInicio = $inicio->format('d/m/Y H:i');
                    $dataFim = $inicio->format('d/m/Y').' '.$fim->format('H:i');
                    $agenda = $this->Agendas->newEntity();
                    foreach ($periodos as $p) {
                        if ($inicio->format('H:i:s') >= $p->inicio_agenda->format('H:i:s') &&
                            $inicio->format('H:i:s') <= $p->fim_agenda->format('H:i:s')) {
                            $agenda->configuracao_periodo_id = $p->id;
                            break;
                        }
                    }
                    $agenda->tipo_id = 1;
                    $agenda->cliente_id = -1;
                    $agenda->convenio_id = -1;
                    $agenda->grupo_id = $grupo_agenda_id;
                    $agenda->situacao_agenda_id = 1000;// Bloqueio
                    $agenda->nome_provisorio = $this->request->data('nome_provisorio');
                    $agenda->observacao = 'Bloqueio';
                    $agenda->inicio = $dataInicio;
                    $agenda->fim = $dataFim;
                    $agenda->agenda_bloqueio_id = $agendaBloqueio->id;
                    if($this->Agendas->save($agenda)){
                        $erro = 0;
                    }else {
                        $erro += 1;
                    }
                }
                if($erro == 0){
                    $this->Flash->success(__('O agenda bloqueio foi salvo com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                }

            } else {
                $this->Flash->error(__('O agenda bloqueio não foi salvo. Por favor, tente novamente.'));
            }

        }

        $this->set(compact('agendaBloqueio'));
        $this->set('_serialize', ['agendaBloqueio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda Bloqueio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendaBloqueio = $this->AgendaBloqueios->get($id, [
            'contain' => ['Agendas']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            //$agendaBloqueio = $this->AgendaBloqueios->patchEntity($agendaBloqueio, $this->request->data);

            $this->loadModel('Agendas');
            if(!empty($this->request->data['status'])){
                $situacao = $this->request->data['status'];
                $agendaBloqueio->status = $situacao;

                $updateAgendas = $this->Agendas
                    ->query()
                    ->update()
                    ->set(['Agendas.situacao_id' => $situacao])
                    ->where(['Agendas.agenda_bloqueio_id' => $id])
                    ->andWhere(['Agendas.situacao_agenda_id' => 1000])
                    ->execute();
            }
        }

        if ($this->AgendaBloqueios->save($agendaBloqueio) && $updateAgendas) {
            $this->Flash->success(__('O agenda bloqueio foi salvo com sucesso.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('O agenda bloqueio não foi salvo. Por favor, tente novamente.'));
        }

        $this->set(compact('agendaBloqueio'));
        $this->set('_serialize', ['agendaBloqueio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda Bloqueio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->autoRender=false;
        $this->response->type('json');

        $agendaBloqueio = $this->AgendaBloqueios->get($id);
        $agendaBloqueio->situacao_id = 2;
        if ($this->AgendaBloqueios->save($agendaBloqueio)) {
            $res = 0;
            $this->Flash->success(__('O agenda bloqueio foi deletado com sucesso.'));
        } else {
            $res = 1;
            $this->Flash->error(__('Desculpe! O agenda bloqueio não foi deletado! Tente novamente mais tarde.'));
        }

        $json = json_encode(['res' => $res, 'view' => 'index']);
        $this->response->body($json);
    }

    public function deleteModal()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $agendaBloqueio = $this->AgendaBloqueios->get($this->request->data['id']);
        $agendaBloqueio->situacao_id = 2;

        $deleteAgenda = $this->AgendaBloqueios->Agendas->deleteAll([
            'agenda_bloqueio_id' => $this->request->data['id']
        ]);

        if ($this->AgendaBloqueios->save($agendaBloqueio) && $deleteAgenda) {
            $res = 0;
        } else {
            $res = 1;
        }

        $this->response->body(json_encode(['res' => $res, 'view' => 'index']));
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Agenda Bloqueio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->AgendaBloqueios->find('all')
            ->where(['AgendaBloqueios.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AgendaBloqueios.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Agenda Bloqueio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $agendaBloqueio = $this->AgendaBloqueios->get($this->request->data['id']);
            $res = ['nome'=>$agendaBloqueio->nome,'id'=>$agendaBloqueio->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function alteraStatus()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('Agendas');

        $agendaBloqueio = $this->AgendaBloqueios->get($this->request->data('pk'));
        $agendaBloqueio->status = $this->request->data('value');

        if($this->AgendaBloqueios->save($agendaBloqueio)){
            $updateAgendas = $this->Agendas
                ->query()
                ->update()
                ->set(['Agendas.situacao_id' => $this->request->data('value')])
                ->where(['Agendas.agenda_bloqueio_id' => $this->request->data('pk')])
                ->andWhere(['Agendas.situacao_agenda_id' => 1000])
                ->execute();
            $json = ['res' => 0, 'msg' => 'Status alterado com sucesso!'];
        }else {
            $json = ['res' => 1, 'msg' => 'Falha ao alterar status, por favor tente mais tarde!'];
        }

        $this->response->body(json_encode($json));
    }
}
