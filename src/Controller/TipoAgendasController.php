<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoAgendas Controller
 *
 * @property \App\Model\Table\TipoAgendasTable $TipoAgendas
 */
class TipoAgendasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->TipoAgendas->find('all')
                 ->contain(['Users', 'SituacaoCadastros'])
                 ->where(['TipoAgendas.situacao_id = ' => '1']);


        $tipoAgendas = $this->paginate($query);

        $this->set(compact('tipoAgendas'));
        $this->set('_serialize', ['tipoAgendas']);
    }

    /**
     * View method
     *
     * @param string|null $id Tipo Agenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoAgenda = $this->TipoAgendas->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('tipoAgenda', $tipoAgenda);
        $this->set('_serialize', ['tipoAgenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tipoAgenda = $this->TipoAgendas->newEntity();
        if ($this->request->is('post')) {
            $tipoAgenda = $this->TipoAgendas->patchEntity($tipoAgenda, $this->request->data);
            if ($this->TipoAgendas->save($tipoAgenda)) {
                $this->Flash->success(__('O tipo agenda foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo agenda não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoAgendas->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoAgendas->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoAgenda', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoAgenda']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Agenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoAgenda = $this->TipoAgendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoAgenda = $this->TipoAgendas->patchEntity($tipoAgenda, $this->request->data);
            if ($this->TipoAgendas->save($tipoAgenda)) {
                $this->Flash->success(__('O tipo agenda foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo agenda não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->TipoAgendas->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->TipoAgendas->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('tipoAgenda', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['tipoAgenda']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $tipoAgenda = $this->TipoAgendas->get($id);
                $tipoAgenda->situacao_id = 2;
        if ($this->TipoAgendas->save($tipoAgenda)) {
            $this->Flash->success(__('O tipo agenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo agenda não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
