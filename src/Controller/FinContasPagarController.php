<?php

namespace App\Controller;

use App\Controller\AppController;
use DateTime;

/**
 * FinContasPagar Controller
 *
 * @property \App\Model\Table\FinContasPagarTable $FinContasPagar
 */
class FinContasPagarController extends AppController
{
    //Componentes externos para serem usados.
    public $components = ['Date', 'Data', 'RelatoriosFinanceiro', 'ContaPagar', 'Model'];

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Contas a Pagar');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $tiposRelatorios = $this->RelatoriosFinanceiro->getRelatoriosContasPagar();
        $selectData = $this->getDataForSelects();
        $planocontas = $selectData[0];
        $fornecedores = $selectData[1];
        $contabilidades = $selectData[2];
        
        $contaspagar = $this->FinContasPagar->find('all')
            ->contain(['FinPlanoContas', 'FinFornecedores', 'FinContabilidades', 'FinContasPagarPlanejamentos', 'FinTipoDocumento']);
        $contaspagar = $this->ContaPagar->filtroContasPagar($contaspagar, $this->request->getQuery());
        $contaspagar->order(['FinContasPagar.vencimento ASC']);

        $contaspagar = $this->paginate($contaspagar);

        $this->set(compact('contaspagar', 'planocontas', 'fornecedores', 'contabilidades', 'tiposRelatorios'));
        $this->set('_serialize', ['contaspagar']);
    }

    public function quitacao()
    {
        $tiposRelatorios = $this->RelatoriosFinanceiro->getRelatoriosContasPagar();
        $selectData = $this->getDataForSelects();
        $planocontas = $selectData[0];
        $fornecedores = $selectData[1];
        $contabilidades = $selectData[2];
        $banco_movimentos = $selectData[3];
        $formas_pagamento = $selectData[4];

        $contaspagar = $this->FinContasPagar->find('all')
            ->contain(['FinPlanoContas', 'FinFornecedores', 'FinContabilidades', 'FinContasPagarPlanejamentos', 'FinTipoDocumento'])->where(['FinContasPagar.data_pagamento IS NULL']);

        $contaspagar = $this->ContaPagar->filtroContasPagar($contaspagar, $this->request->getQuery());
        
        $contaspagar->order(['FinContasPagar.vencimento ASC']);
        
        $totalPeriodo = clone($contaspagar);
        $totalPeriodo = $totalPeriodo->select(['soma' =>'SUM(FinContasPagar.valor)'])->first();
        $totalPeriodo = $totalPeriodo->soma;

        $this->set(compact('contaspagar', 'planocontas', 'fornecedores', 'contabilidades', 'tiposRelatorios', 'banco_movimentos', 'formas_pagamento', 'totalPeriodo'));
        $this->set('_serialize', ['contaspagar']);
    }


    //Quitando a conta a pagar
    public function finishAccount()
    {
        $form = [];
        $form[] = !empty($this->request->query(['bancoMovimento'])) ? $this->request->query(['bancoMovimento']) : null;
        $form[] = !empty($this->request->query(['documento'])) ? $this->request->query(['documento']) : null;
        $form[] = !empty($this->request->query(['formaPagamento'])) ? $this->request->query(['formaPagamento']) : null;
        $form[] = !empty($this->request->query(['ids'])) ? $this->request->query(['ids']) : null;
        $form[] = !empty($this->request->query(['dataPagamento'])) ? $this->Date->formatDateForSql($this->request->query(['dataPagamento'])) : null;
        
        if (!isset($form[0]) || !isset($form[2])) return null;

        $result = $this->Model->getModelData('FinMovimentos', 'finishAccounts', $form, 'pagar');

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }

    //Pegando o plano de contas do fornecedor selecionado
    public function getChartOfProvider()
    {
        $fornecedorId = !empty($this->request->query(['id'])) ? $this->request->query(['id']) : null;

        $planoconta = $this->FinContasPagar->FinFornecedores->find()
        ->contain(['FinPlanoContas'])->select(['id' => 'FinPlanoContas.id', 'nome' => 'FinPlanoContas.nome'])
        ->where(['FinFornecedores.id' => $fornecedorId])->first();

        $this->set(compact('planoconta'));
        $this->set('_serialize', ['planoconta']);
    }

    /**
     * View method
     *
     * @param string|null $id Contaspagar id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finContasPagar = $this->FinContasPagar->get($id, [
            'contain' => ['FinPlanoContas', 'FinFornecedores', 'FinContabilidades', 'FinContasPagarPlanejamentos', 'FinTipoDocumento', 'FinTipoPagamento', 'FinAbatimentosContasPagar', 'FinMovimentos']
        ]);

        $this->set('finContasPagar', $finContasPagar);
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contaspagar = $this->FinContasPagar->newEntity();
        if ($this->request->is('post')) {
            $newConta = $this->request->getData();
            $ap = $this->request->getData('vencimento');
            $aux = false;
            for ($x = 1; $x <= $this->request->getData('qtd_parcelas'); $x++) {
                $contaspagar = $this->FinContasPagar->newEntity();
                $contaspagar = $this->FinContasPagar->patchEntity($contaspagar, $newConta);
                $contaspagar->situacao = 1;
                $contaspagar->vencimento = $ap;
                $contaspagar->parcela = $x . '/' . $this->request->getData('qtd_parcelas');
                if ($x == $this->request->getData('parcelas')) {
                    $valor_quebrado = round(($this->request->getData('valor') / $this->request->getData('qtd_parcelas')), 2);
                    $valor_quebrado_total = round($this->request->getData('valor') - ($valor_quebrado * $this->request->getData('qtd_parcelas')), 2);

                    $contaspagar->valor_bruto = $valor_quebrado + $valor_quebrado_total;
                    $contaspagar->valor = $valor_quebrado + $valor_quebrado_total;
                } else {
                    $contaspagar->valor_bruto = number_format($this->request->getData('valor') / $this->request->getData('qtd_parcelas'), 2, '.', '');
                    $contaspagar->valor = number_format($this->request->getData('valor') / $this->request->getData('qtd_parcelas'), 2, '.', '');
                }

                if ($this->FinContasPagar->save($contaspagar)) {
                    $aux = true;
                }
                $ap = date("d/m/Y", strtotime("+1 months", strtotime(str_replace('/', '-', $ap))));
            }

            if ($aux) {
                $this->Flash->success(__('As parcelas foram salvas com sucesso!.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Não foi possível salvar as parcelas. Por favor tente novamente.'));
        }
        $planocontas = $this->FinContasPagar->FinPlanoContas->find('list', ['limit' => 200]);
        $fornecedores = $this->FinContasPagar->FinFornecedores->find('list', ['limit' => 200]);
        $contaspagarPlanejamentos = $this->FinContasPagar->FinContasPagarPlanejamentos->find('list', ['limit' => 200]);
        $tipoDocumentos = $this->FinContasPagar->FinTipoDocumento->find('list', ['limit' => 200]);
        $tipoPagamentos = $this->FinContasPagar->FinTipoPagamento->find('list', ['limit' => 200]);
        $contabilidades = $this->FinContasPagar->FinContabilidades->find('list', ['limit' => 200]);
        $abatimentosContaspagar = $this->FinContasPagar->FinAbatimentosContasPagar->find('list', ['limit' => 200]);

        $contaspagar->multa = $contaspagar->multa != 0 ? $contaspagar->multa : 0;
        $contaspagar->juros = $contaspagar->juros != 0 ? $contaspagar->juros : 0;
        $contaspagar->desconto = $contaspagar->desconto != 0 ? $contaspagar->desconto : 0;
        $contaspagar->valor_bruto = $contaspagar->valor_bruto != 0 ? $contaspagar->valor_bruto : 0;
        
        $this->set(compact('contaspagar', 'planocontas', 'fornecedores', 'contabilidades', 'contaspagarPlanejamentos', 'tipoDocumentos', 'tipoPagamentos', 'abatimentosContaspagar'));
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contaspagar id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contaspagar = $this->FinContasPagar->get($id, [
            'contain' => ['FinAbatimentosContasPagar', 'FinPlanoContas', 'FinFornecedores', 'FinContabilidades', 'FinContasPagarPlanejamentos', 'FinTipoDocumento', 'FinTipoPagamento']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contaspagar = $this->FinContasPagar->patchEntity($contaspagar, $this->request->getData());
            if ($this->FinContasPagar->save($contaspagar)) {
                $this->Flash->success(__('The contaspagar has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contaspagar could not be saved. Please, try again.'));
        }


        $planocontas = $this->FinContasPagar->FinPlanoContas->find('list', ['limit' => 200]);
        $fornecedores = $this->FinContasPagar->FinFornecedores->find('list', ['limit' => 200]);
        $contabilidades = $this->FinContasPagar->FinContabilidades->find('list', ['limit' => 200]);
        $contaspagarPlanejamentos = $this->FinContasPagar->FinContasPagarPlanejamentos->find('list', ['limit' => 200]);
        $tipoDocumento = $this->FinContasPagar->FinTipoDocumento->find('list', ['limit' => 200]);
        $tipoPagamento = $this->FinContasPagar->FinTipoPagamento->find('list', ['limit' => 200]);
        $abatimentosContaspagar = $this->FinContasPagar->FinAbatimentosContasPagar->find('list', ['limit' => 200]);

        $contaspagar->multa = $contaspagar->multa != 0 ? $contaspagar->multa : 0;
        $contaspagar->juros = $contaspagar->juros != 0 ? $contaspagar->juros : 0;
        $contaspagar->desconto = $contaspagar->desconto != 0 ? $contaspagar->desconto : 0;
        $contaspagar->valor_bruto = $contaspagar->valor_bruto != 0 ? $contaspagar->valor_bruto : 0;


        $this->set(compact('contaspagar', 'finContasPagar', 'planocontas', 'fornecedores', 'contabilidades', 'contaspagarPlanejamentos', 'tipoDocumento', 'tipoPagamento', 'abatimentosContaspagar'));
        $this->set('_serialize', ['contaspagar']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contaspagar id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $conta = $this->FinContasPagar->get($id);
        $conta->situacao = 2;
        if ($this->FinContasPagar->save($conta)) {
            $this->Flash->success(__('O movimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O movimento não foi deletado! Tente novamente mais tarde.'));
        }
    }

    /*Rotas para o jquery*/

    //Função para pegar os credores
    public function getCreditors()
    {
        $credores = $this->Contaspagar->Fornecedores->find('list', ['limit' => 200])->toArray();
        $this->set(compact('credores'));
        $this->set('_serialize', ['credores']);
    }

    /*Funções privadas dessa controller*/

    //Função para pegar os dados dos selects
    private function getDataForSelects()
    {
        $planocontas = $this->FinContasPagar->FinPlanoContas->find('list')->toArray();
        $fornecedores = $this->FinContasPagar->FinFornecedores->find('list')->toArray();
        $contabilidades = $this->FinContasPagar->FinContabilidades->find('list', ['limit' => 200])->toArray();
        $movesBank = $this->Model->getModelData('FinBancoMovimentos', 'getOpenedMovesBankName')->order(['FinBancos.nome', 'FinBancoMovimentos.id']);
        $formas_pagamento = $this->Model->getModelData('FinTipoPagamento', 'getList');

        return [$planocontas, $fornecedores, $contabilidades, $movesBank, $formas_pagamento];
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Fin Contas Pagar id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->FinContasPagar->find('all')
            ->where(['FinContasPagar.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinContasPagar.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Fin Contas Pagar id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $finContasPagar = $this->FinContasPagar->get($this->request->data['id']);
            $res = ['nome' => $finContasPagar->nome, 'id' => $finContasPagar->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    /**
     * Método responsável por devolver um JSON com a quantidade de registro e os totais dos valores baseado nos filtros
     * da tela index do contas a pagar
     */
    public function countContasPagar()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $contas = $this->FinContasPagar->find('all')
            ->contain(['FinFornecedores', 'FinPlanoContas', 'FinContabilidades']);

        $contas = $this->ContaPagar->filtroContasPagar($contas, $this->request->getData());

        $query_select = $contas->select([
            'valor_com_deducao' => $contas->func()->sum('FinContasPagar.valor'),
            'count' => $contas->func()->count('FinContasPagar.id'),
            'valor_bruto' => $contas->func()->sum('FinContasPagar.valor_bruto')
        ])->toArray();

        $json = [
            'valor_com_deducao' => ($query_select[0]->valor_com_deducao != null) ? $query_select[0]->valor_com_deducao : '0.00',
            'count' => ($query_select[0]->count != null) ? $query_select[0]->count : '0',
            'valor_bruto' => ($query_select[0]->valor_bruto != null) ? $query_select[0]->valor_bruto : '0.00',
        ];

        $this->response->body(json_encode($json));
    }
}
