<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaKit Controller
 *
 * @property \App\Model\Table\FaturaKitTable $FaturaKit
 */
class FaturaKitController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Procedimentos', 'UserReg', 'UserAlt', 'SituacaoCadastros', 'FaturaKitartigo']
        ];
        $faturaKit = $this->paginate($this->FaturaKit);

        $this->set(compact('faturaKit'));
        $this->set('_serialize', ['faturaKit']);

    }

    /**
     * View method
     *
     * @param string|null $id Fatura Kit id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaKit = $this->FaturaKit->get($id, [
            'contain' => ['Procedimentos', 'UserReg', 'UserAlt', 'SituacaoCadastros', 'FaturaKitartigo']
        ]);

        $this->set('faturaKit', $faturaKit);
        $this->set('_serialize', ['faturaKit']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faturaKit = $this->FaturaKit->newEntity();
        if ($this->request->is('post')) {
            $faturaKit = $this->FaturaKit->patchEntity($faturaKit, $this->request->data);
            if ($this->FaturaKit->save($faturaKit)) {
                $this->Flash->success(__('O fatura kit foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura kit não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaKit'));
        $this->set('_serialize', ['faturaKit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Kit id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaKit = $this->FaturaKit->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaKit = $this->FaturaKit->patchEntity($faturaKit, $this->request->data);
            if ($this->FaturaKit->save($faturaKit)) {
                $this->Flash->success(__('O fatura kit foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura kit não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaKit'));
        $this->set('_serialize', ['faturaKit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Kit id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faturaKit = $this->FaturaKit->get($id);
                $faturaKit->situacao_id = 2;
        if ($this->FaturaKit->save($faturaKit)) {
            $this->Flash->success(__('O fatura kit foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura kit não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fatura Kit id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaturaKit->find('all')
        ->where(['FaturaKit.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaKit.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fatura Kit id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faturaKit = $this->FaturaKit->get($this->request->data['id']);
            $res = ['nome'=>$faturaKit->nome,'id'=>$faturaKit->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function verificaProcKit(){
        $this->autoRender=false;
        $this->response->type('json');

        $checkFaturaKit = $this->FaturaKit->find('all')->where(['FaturaKit.situacao_id' => 1])
            ->andWhere(['FaturaKit.procedimento_id' => $this->request->data('proc_id')])->count();

        $faturaKit = $this->FaturaKit->find('list')->where(['FaturaKit.situacao_id' => 1])->orderAsc('FaturaKit.nome');

        if($checkFaturaKit == 0){
            $faturaKit = $faturaKit->andWhere(['FaturaKit.procedimento_id <> ' => $this->request->data('proc_id')]);
        }

        $this->response->body(json_encode($faturaKit));
    }
}
