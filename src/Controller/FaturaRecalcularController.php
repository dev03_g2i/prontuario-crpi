<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaRecalcular Controller
 *
 * @property \App\Model\Table\FaturaRecalcularTable $FaturaRecalcular
 */
class FaturaRecalcularController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Convenios', 'SituacaoCadastros', 'UserReg', 'UserAlt'],
            'order' => ['FaturaRecalcular.id' => 'DESC']
        ];
        $faturaRecalcular = $this->paginate($this->FaturaRecalcular);

        $this->set(compact('faturaRecalcular'));
        $this->set('_serialize', ['faturaRecalcular']);

    }

    /**
     * View method
     *
     * @param string|null $id Fatura Recalcular id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaRecalcular = $this->FaturaRecalcular->get($id, [
            'contain' => ['Convenios', 'SituacaoCadastros']
        ]);

        $this->set('faturaRecalcular', $faturaRecalcular);
        $this->set('_serialize', ['faturaRecalcular']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faturaRecalcular = $this->FaturaRecalcular->newEntity();
        if ($this->request->is('post')) {
            $faturaRecalcular = $this->FaturaRecalcular->patchEntity($faturaRecalcular, $this->request->data);
            if ($this->FaturaRecalcular->save($faturaRecalcular)) {
                $this->Flash->success(__('O fatura recalcular foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura recalcular não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaRecalcular'));
        $this->set('_serialize', ['faturaRecalcular']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Recalcular id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaRecalcular = $this->FaturaRecalcular->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaRecalcular = $this->FaturaRecalcular->patchEntity($faturaRecalcular, $this->request->data);
            if ($this->FaturaRecalcular->save($faturaRecalcular)) {
                $this->Flash->success(__('O fatura recalcular foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura recalcular não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaRecalcular'));
        $this->set('_serialize', ['faturaRecalcular']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Recalcular id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $faturaRecalcular = $this->FaturaRecalcular->get($id);
                $faturaRecalcular->situacao_id = 2;
        if ($this->FaturaRecalcular->save($faturaRecalcular)) {
            $this->Flash->success(__('O fatura recalcular foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura recalcular não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fatura Recalcular id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaturaRecalcular->find('all')
        ->where(['FaturaRecalcular.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaRecalcular.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fatura Recalcular id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faturaRecalcular = $this->FaturaRecalcular->get($this->request->data['id']);
            $res = ['nome'=>$faturaRecalcular->nome,'id'=>$faturaRecalcular->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
