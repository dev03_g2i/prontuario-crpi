<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AtendimentoItens Controller
 *
 * @property \App\Model\Table\AtendimentoItensTable $AtendimentoItens
 */
class AtendimentoItensController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Atendimentos', 'Procedimentos', 'SituacaoCadastros', 'Users', 'Dentes', 'Faces', 'Regioes']
                        ,'conditions' => ['AtendimentoItens.situacao_id = ' => '1']
                    ];
        $atendimentoItens = $this->paginate($this->AtendimentoItens);

        $this->set(compact('atendimentoItens'));
        $this->set('_serialize', ['atendimentoItens']);
    }

    /**
     * View method
     *
     * @param string|null $id Atendimento Iten id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimentoIten = $this->AtendimentoItens->get($id, [
            'contain' => ['Atendimentos', 'Procedimentos', 'SituacaoCadastros', 'Users', 'Dentes', 'Faces', 'Regioes']
        ]);

        $this->set('atendimentoIten', $atendimentoIten);
        $this->set('_serialize', ['atendimentoIten']);
    }


    public function parcialFaturar(){
        $this->autoRender=false;
        $atendimento_itens = $this->AtendimentoItens->get($_POST['pk']);

        //primeiro limpo o valor existente nos procedimentos
        $this->loadModel('AtendimentoProcedimentos');
        $atendimento_procedimentos = $this->AtendimentoProcedimentos->get($atendimento_itens->atendproc_id);
        $atendimento_procedimentos->valor_fatura = $atendimento_procedimentos->valor_fatura - $atendimento_itens->valor_faturar;
        $this->AtendimentoProcedimentos->save($atendimento_procedimentos);

        $atendimento_itens->valor_faturar = $_POST['value'];
        $this->response->type('json');
        if($this->AtendimentoItens->save($atendimento_itens)){
            //depois de atualizar o iten, adiciono o valor atualizado no procedimento
            $atendimento_procedimentos->valor_fatura = $atendimento_procedimentos->valor_fatura + $atendimento_itens->valor_faturar;
            $this->AtendimentoProcedimentos->save($atendimento_procedimentos);

            $res = ['res'=>$atendimento_procedimentos->atendimento_id];
        }else{
            $res = ['res'=>'erro'];
        }
        $this->response->body(json_encode($res));
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atendimentoIten = $this->AtendimentoItens->newEntity();
        if ($this->request->is('post')) {
            $atendimentoIten = $this->AtendimentoItens->patchEntity($atendimentoIten, $this->request->data);
            if ($this->AtendimentoItens->save($atendimentoIten)) {
                $this->Flash->success(__('O atendimento iten foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento iten não foi salvo. Por favor, tente novamente.'));
            }
        }
        $atendimentos = $this->AtendimentoItens->Atendimentos->find('list', ['limit' => 200]);
        $procedimentos = $this->AtendimentoItens->Procedimentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->AtendimentoItens->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->AtendimentoItens->Users->find('list', ['limit' => 200]);
        $dentes = $this->AtendimentoItens->Dentes->find('list', ['limit' => 200]);
        $faces = $this->AtendimentoItens->Faces->find('list', ['limit' => 200]);
        $regioes = $this->AtendimentoItens->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('atendimentoIten', 'atendimentos', 'procedimentos', 'situacaoCadastros', 'users', 'dentes', 'faces', 'regioes'));
        $this->set('_serialize', ['atendimentoIten']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento Iten id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendimentoIten = $this->AtendimentoItens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoIten = $this->AtendimentoItens->patchEntity($atendimentoIten, $this->request->data);
            if ($this->AtendimentoItens->save($atendimentoIten)) {
                $this->Flash->success(__('O atendimento iten foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento iten não foi salvo. Por favor, tente novamente.'));
            }
        }
        $atendimentos = $this->AtendimentoItens->Atendimentos->find('list', ['limit' => 200]);
        $procedimentos = $this->AtendimentoItens->Procedimentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->AtendimentoItens->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->AtendimentoItens->Users->find('list', ['limit' => 200]);
        $dentes = $this->AtendimentoItens->Dentes->find('list', ['limit' => 200]);
        $faces = $this->AtendimentoItens->Faces->find('list', ['limit' => 200]);
        $regioes = $this->AtendimentoItens->Regioes->find('list', ['limit' => 200]);
        $this->set(compact('atendimentoIten', 'atendimentos', 'procedimentos', 'situacaoCadastros', 'users', 'dentes', 'faces', 'regioes'));
        $this->set('_serialize', ['atendimentoIten']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento Iten id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post', 'delete']);
        $atendimentoIten = $this->AtendimentoItens->get($id);
        $res = ['res'=>'sucesso'];
                $atendimentoIten->situacao_id = 2;
        if ($this->AtendimentoItens->save($atendimentoIten)) {
                $this->loadModel('AtendimentoProcedimentos');
                $atendimento_procedimentos = $this->AtendimentoProcedimentos->get($atendimentoIten->atendproc_id);
            if(!empty($atendimentoIten->valor_faturar)){
                $atendimento_procedimentos->valor_fatura = $atendimento_procedimentos->valor_fatura - $atendimentoIten->valor_faturar;
            }
            $atendimento_procedimentos->situacao_id = $this->AtendimentoItens->checar_itens($atendimentoIten->atendproc_id);
            $this->AtendimentoProcedimentos->save($atendimento_procedimentos);
        } else {
            $res = ['res'=>'erro'];
        }
        $this->response->type('json');
        $this->response->body(json_encode($res));
    }

    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name']) && !empty($this->request->data['value'])){
            $name = $this->request->data['name'];
            $contas = $this->AtendimentoItens->findById($this->request->data['pk'])->first();
            $contas->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->AtendimentoItens->save($contas)) {
                $res = ['res' => $contas->$name,'msg'=>'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }
}
