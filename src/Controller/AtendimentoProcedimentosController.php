<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;
use stdClass;
use DateTime;


/**
 * AtendimentoProcedimentos Controller
 *
 * @property \App\Model\Table\AtendimentoProcedimentosTable $AtendimentoProcedimentos
 */
class AtendimentoProcedimentosController extends AppController
{

    public $components = ['Data', 'Configuracao', 'Atendimento'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Procedimentos', 'Atendimentos', 'MedicoResponsaveis', 'SituacaoCadastros', 'Users']
            , 'conditions' => ['AtendimentoProcedimentos.situacao_id = ' => '1']
        ];
        $atendimentoProcedimentos = $this->paginate($this->AtendimentoProcedimentos);

        $this->set(compact('atendimentoProcedimentos'));
        $this->set('_serialize', ['atendimentoProcedimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Atendimento Procedimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($id, [
            'contain' => ['Procedimentos', 'Atendimentos', 'MedicoResponsaveis', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('atendimentoProcedimento', $atendimentoProcedimento);
        $this->set('_serialize', ['atendimentoProcedimento']);
    }

    public function situacaoRecebimento($id = null)
    {
        $atendProc = $this->AtendimentoProcedimentos->get($id, [
            'contain' => ['SituacaoRecebimentos']
        ]);

        $this->set(compact('atendProc'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('PrecoProcedimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('Convenios');
        $this->loadModel('MedicoResponsaveis');
        $Preco = $this->PrecoProcedimentos;
        $Procedimento = $this->Procedimentos;
        $Convenios = $this->Convenios;
        $Medicos = $this->MedicoResponsaveis;

        $action = (!empty($this->request->getQuery('action'))) ? $this->request->getQuery('action') : null;
        $atendimento_id = (!empty($this->request->getQuery('atendimento_id'))) ? $this->request->getQuery('atendimento_id') : null;
        $convenio = !empty($this->request->query['convenio']) ? $this->request->query['convenio'] : null;

        if ($this->Configuracao->showCampoControleFinanceiro()) {
            $controles = $this->AtendimentoProcedimentos->ControleFinanceiro->find('list', ['limit' => 200])->where(['ControleFinanceiro.usa_atendproc' => 1]);
        } else {
            $controles = $this->Configuracao->getControleFinanceiroAtendProcPadrao();
        }


        if ($this->request->is('post')) {

            $convenios = $Convenios->get((int)$this->request->data['convenio_id']);
            $procedimentos = $Procedimento->get((int)$this->request->data['procedimento_id']);
            $Medicos = $Medicos->get((int)$this->request->data['medico_id']);
            $quantidade = !empty($this->request->data['quantidade']) ? $this->request->data['quantidade'] : 1;
            $chave_sus = !empty($this->request->data['chave_sus']) ? $this->request->data['chave_sus'] : null;

            $data = $this->request->getData();
            $data['procedimento'] = $procedimentos->nome;
            $data['convenio'] = $procedimentos->convenio;
            $data['quantidade'] = $quantidade;
            $data['medico_id'] = $Medicos->id;
            $data['medico_nome'] = $Medicos->nome;
            $data['prev_entrega_proc'] = ($this->Configuracao->showPrevEntrega()) ? $this->Atendimento->calculaPrevEntrega($this->request->getData('procedimento_id')) : null;
            $data['chave_sus'] = $chave_sus;

            if (isset($this->request->data['pos'])) {
                $_SESSION['itens'][$this->request->data['pos']] = $data;
            } else {
                $_SESSION['itens'][] = $data;
            }

        }
        $cadastro_procedimento = null;
        if (isset($this->request->query['pos'])) {
            $cadastro_procedimento = $_SESSION['itens'][$this->request->query['pos']];
            $pos = $this->request->query['pos'];
        }

        /* Ordena os procedimentos de acordo com as regras */
        $procedimentos = !empty($_SESSION['itens']) ? $_SESSION['itens'] : null;
        unset($_SESSION['itens']);
        $sort_procedimentos = $this->Atendimento->sortOrdenacao($procedimentos);
        $_SESSION['itens'] = $sort_procedimentos;

        $this->loadModel('Apurar');
        $apurar = $this->Apurar->find('list');
        $this->loadModel('ProcedimentoCaddetalhes');
        $complementos = $this->ProcedimentoCaddetalhes->getList();

        $this->set(compact('convenio', 'apurar', 'cadastro_procedimento', 'pos', 'action', 'atendimento_id', 'controles', 'complementos'));
    }


    public function nadd($id_atendimento)
    {

        $this->loadModel('Atendimentos');
        $this->loadModel('Convenios');
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->newEntity();
        $atendimento = $this->Atendimentos;
        $convenio = $this->Convenios;
        $a = $atendimento->get($id_atendimento);
        if ($this->request->is('post')) {
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->patchEntity($atendimentoProcedimento, $this->request->data);
            //$atendimentoProcedimento->solicitante_id = $a->solicitante_id;
            if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
                echo $atendimentoProcedimento->atendimento_id;
            } else {
                echo -1;
            }

            $this->autoRender = false;
        }
        $Atendimentos = $atendimento->get((int)$id_atendimento);
        $Convenios = $convenio->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');

        $medicoResponsaveis = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200]);

        $this->set(compact('atendimentoProcedimento', 'medicoResponsaveis', 'Atendimentos', 'Convenios'));
        $this->set('_serialize', ['atendimentoProcedimento']);
    }


    public function nedit($id, $id_atendimento)
    {

        $this->loadModel('Atendimentos');
        $this->loadModel('Convenios');
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($id, [
            'contain' => []
        ]);

        $atendimento = $this->Atendimentos;
        $convenio = $this->Convenios;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->patchEntity($atendimentoProcedimento, $this->request->data);
            if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
                echo $atendimentoProcedimento->atendimento_id;
            } else {
                echo -1;
            }
            $this->autoRender = false;
        }

        $Atendimentos = $atendimento->get((int)$id_atendimento);
        $Convenios = $convenio->find('list')
            ->where(['situacao_id =' => 1]);

        $procedimentos = $this->AtendimentoProcedimentos->Procedimentos->find('list', ['limit' => 200]);
        $medicoResponsaveis = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200]);

        $this->set(compact('atendimentoProcedimento', 'procedimentos', 'medicoResponsaveis', 'Atendimentos', 'Convenios'));
        $this->set('_serialize', ['atendimentoProcedimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento Procedimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convenio = $this->request->getQuery('convenio');
        $atendimento_id = $this->request->getQuery('atendimento_id');
        $action = (!empty($this->request->getQuery('action'))) ? $this->request->getQuery('action') : null;
        $res = null;

        if ($this->Configuracao->showCampoControleFinanceiro()) {
            $controles = $this->AtendimentoProcedimentos->ControleFinanceiro->find('list', ['limit' => 200])->where(['ControleFinanceiro.usa_atendproc' => 1]);
        } else {
            $controles = $this->Configuracao->getControleFinanceiroAtendProcPadrao();
        }

        if (!empty($id)) {
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($id);
            $method_xml = 'Edit';
        } else {
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->newEntity();
            $method_xml = 'Insert';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $quantidade = !empty($this->request->getData('quantidade')) ? $this->request->getData('quantidade') : 1;
            $atendimentoProcedimento->medico_id = $this->request->data['medico_id'];
            $atendimentoProcedimento->procedimento_id = $this->request->data['procedimento_id'];
            if (!empty($this->request->data['atendimento_id'])) {
                $atendimentoProcedimento->atendimento_id = $this->request->data['atendimento_id'];
            }
            $atendimentoProcedimento->ordenacao = $this->request->getData('ordenacao');
            $atendimentoProcedimento->quantidade = $quantidade;
            $atendimentoProcedimento->valor_base = $this->request->getData('valor_base');
            $atendimentoProcedimento->valor_fatura = $this->request->getData('valor_fatura');
            $atendimentoProcedimento->desconto = $this->request->getData('desconto');
            $atendimentoProcedimento->porc_desconto = 0;
            $atendimentoProcedimento->valor_caixa = $this->request->getData('valor_caixa');
            $atendimentoProcedimento->vl_caixa_original = $this->request->getData('vl_caixa_original');
            $atendimentoProcedimento->digitado = 'Sim';
            $atendimentoProcedimento->valor_matmed = $this->request->getData('valor_fatura');
            $atendimentoProcedimento->complemento = $this->request->data['complemento'];
            //$atendimentoProcedimento->controle = $this->request->data['controle'];
            $atendimentoProcedimento->codigo = $this->request->data['codigo'];
            $atendimentoProcedimento->regra = $this->request->getData('regra');
            $atendimentoProcedimento->solicitante_id = !empty($this->request->getData('solicitante_id')) ? $this->request->getData('solicitante_id') : null;
            $atendimentoProcedimento->nr_guia = $this->request->getData('nr_guia');
            $atendimentoProcedimento->autorizacao_senha = $this->request->getData('autorizacao_senha');
            $atendimentoProcedimento->dt_emissao_guia = !empty($this->request->getData('dt_emissao_guia')) ? $this->request->getData('dt_emissao_guia') : null;
            $atendimentoProcedimento->dt_autorizacao = !empty($this->request->getData('dt_autorizacao')) ? $this->request->getData('dt_autorizacao') : null;
            $atendimentoProcedimento->prev_entrega_proc = ($this->Configuracao->showPrevEntrega()) ? $this->Atendimento->calculaPrevEntrega($this->request->getData('procedimento_id')) : null;
            $atendimentoProcedimento->filme_reais = $this->request->getData('filme_reais');
            $atendimentoProcedimento->filme = $this->request->getData('filme');
            $atendimentoProcedimento->uco = $this->request->getData('uco');
            $atendimentoProcedimento->porte = $this->request->getData('porte');
            $atendimentoProcedimento->controle_financeiro_id = $this->request->getData('controle_financeiro_id');
            $atendimentoProcedimento->chave_sus = !empty($this->request->getData('chave_sus')) ? $this->request->getData('chave_sus') : null;
            if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
                $this->AtendimentoProcedimentos->reordenaProcedimentos($atendimentoProcedimento->atendimento_id);
                $this->Atendimento->gerarXML($atendimentoProcedimento->id, $method_xml);
                $res = $atendimentoProcedimento->atendimento_id;
                echo $res;
                exit;
            }
        }
        $cadastro_procedimento = $atendimentoProcedimento->toArray();

        $this->loadModel('ProcedimentoCaddetalhes');
        $complementos = $this->ProcedimentoCaddetalhes->getList();
        if(!empty($cadastro_procedimento['complemento'])){
            $complementos[$cadastro_procedimento['complemento']] = $cadastro_procedimento['complemento'];
        }
        $this->set(compact('cadastro_procedimento', 'convenio', 'atendimento_id', 'action', 'controles', 'complementos'));
        $this->set('_serialize', ['cadastro_procedimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento Procedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->viewBuilder()->layout('ajax');
        //$this->request->allowMethod(['post', 'delete']);
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($id);
        $atendimentoProcedimento->situacao_id = 2;
        if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
            $this->Atendimento->gerarXML($id, 'Delete');
            $this->AtendimentoProcedimentos->reordenaProcedimentos($atendimentoProcedimento->atendimento_id);
            //$this->Flash->success(__('O atendimento procedimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O atendimento procedimento não foi deletado! Tente novamente mais tarde.'));
        }

        $this->autoRender = false;
    }

    public function listar($id)
    {
        $this->viewBuilder()->layout('ajax');
        $AtendimentoProcedimentos = $this->AtendimentoProcedimentos->find('all', [
            'contain' => ['ControleFinanceiro', 'Procedimentos', 'MedicoResponsaveis', 'AtendimentoItens' => ['Dentes', 'Regioes', 'FaceItens' => ['Faces']]]
        ])->where(['AtendimentoProcedimentos.atendimento_id' => $id, 'AtendimentoProcedimentos.situacao_id' => 1]);

        $this->set(compact('AtendimentoProcedimentos', 'id'));
        $this->set('_serialize', ['AtendimentoProcedimentos']);
    }

    public function somaprocedimentos($id)
    {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Atendimentos');
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => ['AtendimentoProcedimentos']
        ]);
        $valor_base = 0;
        $valor_fatura = 0;
        $valor_caixa = 0;
        if (!empty($atendimento->atendimento_procedimentos)) {
            foreach ($atendimento->atendimento_procedimentos as $anted) {
                $valor_base += (float)$anted->valor_caixa;
                $valor_caixa += (float)$anted->valor_caixa;
                $valor_fatura += (float)$anted->valor_fatura;
            }
        }
        $atendimento->total_geral = $valor_caixa;
        $atendimento->total_liquido = $valor_caixa;
        $this->Atendimentos->save($atendimento);
        $dados = array(
            "valor_base" => (float)$valor_base,
            "valor_caixa" => (float)$valor_caixa,
            "valor_fatura" => (float)$valor_fatura,
            "total_recebido" => $atendimento->total_pagoato,
            "total_a_receber" => $atendimento->total_areceber,
        );
        $this->response->type('json');
        $json = json_encode($dados);
        $this->response->body($json);

        $this->autoRender = false;

    }

    public function parcialEdit()
    {
        $this->autoRender = false;
        $atendimento_proc = $this->AtendimentoProcedimentos->get($this->request->data['pk']);
        if (empty($atendimento_proc->valor_base)) {
            $atendimento_proc->valor_fatura = $_POST['value'];
        } else {
            $this->AtendimentoProcedimentos->recalcular_base($this->request->data['pk'], $this->request->data['value']);
            $atendimento_proc->valor_base = $this->request->data['value'];
        }
        $this->response->type('json');
        if ($this->AtendimentoProcedimentos->save($atendimento_proc)) {
            $res = ['res' => $atendimento_proc->atendimento_id];
        } else {
            $res = ['res' => 'erro'];
        }
        $this->response->body(json_encode($res));
    }

    public function parcialEditAp()
    {
        $this->autoRender = false;
        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if (!empty($this->request->data['name']) && !empty($this->request->data['value'])) {
            $name = $this->request->data['name'];
            $precoProc = $this->AtendimentoProcedimentos->get($this->request->data['pk']);
            $precoProc->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->AtendimentoProcedimentos->save($precoProc)) {
                $res = ['res' => $precoProc->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }


    public function getvalores()
    {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('PrecoProcedimentos');
        $Preco = $this->PrecoProcedimentos;
        $query = $Preco->find('all')
            ->where(['convenio_id =' => $this->request->data['convenio_id'],
                'procedimento_id =' => $this->request->data['procedimento_id'],
                'situacao_id' => 1]);
        $preco_procedimento = $query->first();

        echo (string)$preco_procedimento;
        $this->autoRender = false;
    }

    public function editTable()
    {
        $this->autoRender = false;
        $name = $this->request->data['name'];
        $pk = $this->request->data['pk'];
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($pk);
        $error = '';
        $msg = '';

        $limite_desconto_perc = $this->Auth->user('limite_desconto');
        if ($name == 'quantidade' || $name == 'valor_caixa' || $name == 'valor_fatura') {
            //$faturar_old = $atendimentoProcedimento->valor_fatura/($atendimentoProcedimento->quantidade>0 ? $atendimentoProcedimento->quantidade: 1);
            $caixa_old = $atendimentoProcedimento->valor_caixa / ($atendimentoProcedimento->quantidade > 0 ? $atendimentoProcedimento->quantidade : 1);
            //$atendimentoProcedimento->valor_fatura = $faturar_old * $this->request->data['value'];
            $atendimentoProcedimento->valor_caixa = $caixa_old * $this->request->data['value'];
        }

        if ($name == 'quantidade') {
            if ($atendimentoProcedimento->regra == 2 && $this->request->data['value'] == 2) {
                $atendimentoProcedimento->total = $atendimentoProcedimento->valor_fatura + ($atendimentoProcedimento->valor_fatura * 0.7);
            } else {
                $atendimentoProcedimento->total = $atendimentoProcedimento->valor_fatura * $this->request->data['value'];
            }
        } elseif ($name == 'desconto') {
            $desconto_maximo = number_format($atendimentoProcedimento->vl_caixa_original * $limite_desconto_perc / 100, 2, '.', '');
            $desconto_aplicado = $this->request->data['value'];
            if ($desconto_maximo >= $desconto_aplicado) {
                $atendimentoProcedimento->valor_caixa = $atendimentoProcedimento->vl_caixa_original - $desconto_aplicado;
            } else {
                $error = 'desconto';
                $msg = 'O desconto aplicado ultrapassa o limite do dado ao usuário.';
            }
        }

        $atendimentoProcedimento->$name = $this->request->data['value'];
        if (empty($error)) {
            $this->AtendimentoProcedimentos->save($atendimentoProcedimento);
        }
        $this->response->type('json');
        $this->response->body(json_encode(['res' => $atendimentoProcedimento->atendimento_id, 'error' => $error, 'msg' => $msg]));
    }

    public function getDesconto()
    {
        $limite_desconto_perc = number_format($this->Auth->user('limite_desconto'), 2, '.', '');
        $desconto_maximo = number_format($this->request->getData('total_liquido') * $limite_desconto_perc / 100, 2, '.', '');
        $desconto_aplicado = number_format($this->request->getData('desconto'), 2, '.', '');
        if ($desconto_maximo >= $desconto_aplicado) {
            $total_liquido = $this->request->getData('total_liquido') - $desconto_aplicado;
            $res = ['error' => 0, 'total_liquido' => $total_liquido, 'valor_maximo' => $desconto_maximo];
        } else {
            $res = ['error' => 1, 'msg' => 'O desconto aplicado ultrapassa o limite dado ao usuário.'];
        }

        $json = json_encode($res);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }


    public function all()
    {
        $this->loadModel('Atendimentos');
        $query = $this->Atendimentos->find('all')
            ->contain(
                ['Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos', 'AtendimentoProcedimentos' => ['MedicoResponsaveis', 'Procedimentos'], 'Solicitantes']
            )
            ->where(['Atendimentos.situacao_id ' => '1', 'Atendimentos.tipo ' => '1']);

        if (!empty($this->request->query['cliente_id'])) {
            $query->andWhere(['Atendimentos.cliente_id ' => $this->request->query['cliente_id']]);
        }

        if (!empty($this->request->query['data'])) {
            $query->andWhere(['Atendimentos.data ' => $this->Data->DataSQL($this->request->query['data'])]);
        }
        if (!empty($this->request->query['convenio_id'])) {
            $query->andWhere(['Atendimentos.convenio_id ' => $this->request->query['convenio_id']]);
        }
        if (!empty($this->request->query['tipoatendimento_id'])) {
            $query->andWhere(['Atendimentos.tipoatendimento_id ' => $this->request->query['tipoatendimento_id']]);
        }


        $this->paginate = [
            'limit' => 10
        ];
        $query->orderDesc('Atendimentos.id');
        $atendimentos = $this->paginate($query);

        $this->loadModel('Convenios');
        $this->loadModel('TipoAtendimentos');
        $convenio = $this->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $tipoatendimento = $this->TipoAtendimentos->find('list')->where(['TipoAtendimentos.situacao_id' => 1])->orderAsc('TipoAtendimentos.nome');
        $this->set(compact('atendimentos', 'convenio', 'tipoatendimento'));
        $this->set('_serialize', ['atendimentos']);
    }

    // Foi para action add - Luciano 09/11/2017 20:00
    /*public function cadastrar(){

        $this->loadModel('PrecoProcedimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('Convenios');
        $this->loadModel('MedicoResponsaveis');
        $Preco = $this->PrecoProcedimentos;
        $Procedimento = $this->Procedimentos;
        $Convenios = $this->Convenios;
        $Medicos = $this->MedicoResponsaveis;

        $action = (!empty($this->request->getQuery('action')))? $this->request->getQuery('action') : null;
        $atendimento_id = (!empty($this->request->getQuery('atendimento_id')))? $this->request->getQuery('atendimento_id') : null;

        if ($this->request->is('post')) {

            $query = $Preco->find('all')
                ->where(['convenio_id =' => $this->request->data['convenio_id'],
                    'procedimento_id =' => $this->request->data['procedimento_id'],
                    'situacao_id' => 1]);

            $preco_procedimento = $query->first();

            $convenios = $Convenios->get((int)$this->request->data['convenio_id']);
            if (empty($preco_procedimento)) {
                $preco_procedimento->valor_faturar = 0.00;
                $preco_procedimento->valor_particular = 0.00;
                $preco_procedimento->id = '';
            }
            $procedimentos = $Procedimento->get((int)$this->request->data['procedimento_id'],
                ['contain' => []]);
            $Medicos = $Medicos->get((int)$this->request->data['medico_id']);
            $quantidade = !empty($this->request->data['quantidade']) ? $this->request->data['quantidade'] : 1;

            $cadastro_procedimento = array(
                'procedimento_id' => $this->request->data['procedimento_id'],
                'convenio_id' => $this->request->data['convenio_id'],
                'medico_id' => $Medicos->id,
                'medico_nome' => $Medicos->nome,
                'valor_base' => $preco_procedimento->valor_faturar,
                'valor_faturar' => number_format($preco_procedimento->valor_faturar,2,'.',''),
                'valor_caixa' => number_format($quantidade*$preco_procedimento->valor_particular,2,'.',''),
                'vl_caixa_original' => number_format($quantidade*$preco_procedimento->valor_particular,2,'.',''),
                'preco_id' => $preco_procedimento->id,
                'procedimento' => $procedimentos->nome,
                'quantidade' => $quantidade,
                'convenio' => $convenios->nome,
                'complemento' => $this->request->data['complemento'],
                'controle' => !empty($this->request->data['controle']) ? $this->request->data['controle'] : '',
                //'quantidade' => !empty($this->request->data['quantidade']) ? $this->request->data['quantidade'] : 1,
                'desconto' => '0',
                'nr_guia'=>$this->request->data['nr_guia'],
                'dt_emissao_guia'=>$this->request->data['dt_emissao_guia'],
                'autorizacao_senha'=>$this->request->data['autorizacao_senha'],
                'dt_autorizacao'=>$this->request->data['dt_autorizacao'],
                'percentual_cobranca'=>$this->request->data['percentual_cobranca'],
                'apurar_id'=>$this->request->data['apurar_id'],
                'codigo'=>$this->request->data['codigo'],
                'regra'=>$this->request->data['regra'],
                'prev_entrega_proc' => ($this->Configuracao->showPrevEntrega()) ? $this->Atendimento->calculaPrevEntrega($this->request->data['procedimento_id']) : null
            );

            if(isset($this->request->data['pos'])){
                $_SESSION['itens'][$this->request->data['pos']]  = $cadastro_procedimento;
            }else{
                $_SESSION['itens'][]  = $cadastro_procedimento;
            }

        }
        $convenio = !empty($this->request->query['convenio']) ? $this->request->query['convenio'] : null;
        $cadastro_procedimento = null;

        if(isset($this->request->query['pos'])){
            $cadastro_procedimento = $_SESSION['itens'][$this->request->query['pos']];
            $pos = $this->request->query['pos'];
        }
        $this->loadModel('Apurar');
        $apurar = $this->Apurar->find('list');
        $this->set(compact('convenio','apurar','cadastro_procedimento','pos', 'action', 'atendimento_id'));

    }*/

    public function faturamento($id = null)
    {
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($this->request->query['atendimento_procedimento_id'], [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->patchEntity($atendimentoProcedimento, $this->request->data);
            if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
                $this->Flash->success(__('O faturamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O faturamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('atendimentoProcedimento'));
        $this->set('_serialize', ['atendimentoProcedimento']);
    }


    public function cobranca($id = null)
    {
        $atendimentoProcedimento = $this->AtendimentoProcedimentos->get($this->request->query['atendimento_procedimento_id'], [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoProcedimento = $this->AtendimentoProcedimentos->patchEntity($atendimentoProcedimento, $this->request->data);
            if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
                $this->Flash->success(__('O faturamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O faturamento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('Apurar');
        $apurar = $this->Apurar->find('list');
        $this->set(compact('atendimentoProcedimento', 'apurar'));
        $this->set('_serialize', ['atendimentoProcedimento']);
    }

    // Pagamento Produção
    public function pagamentoProducao($id)
    {
        $atendProc = $this->AtendimentoProcedimentos->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendProc = $this->AtendimentoProcedimentos->patchEntity($atendProc, $this->request->data);
            $atendProc->producao_pg_dt = $this->Data->DataSQL($this->request->data('producao_pg_dt'));
            $atendProc->producao_pg_dtuser = $this->Data->DateTimeSql($this->request->data('producao_pg_dtuser'));
            if ($this->AtendimentoProcedimentos->save($atendProc)) {
                $this->Flash->success(__('O Pagamento Produção foi realizado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O Pagamento Produção não foi salvo. Por favor, tente novamente.'));
            }
        }

        $users = $this->AtendimentoProcedimentos->Users->find('list')->where(['Users.situacao_id' => 1])->orderAsc('Users.nome');
        $this->set(compact('atendProc', 'users'));
    }

    public function fill()
    {
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->AtendimentoProcedimentos->find('all')
            ->where(['AtendimentoProcedimentos.situacao_id' => 1,
                'AtendimentoProcedimentos.id LIKE ' => '%' . $termo . '%'], ['AtendimentoProcedimentos.id' => 'string']);

        $cont = $query->count();

        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->id);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }
}
