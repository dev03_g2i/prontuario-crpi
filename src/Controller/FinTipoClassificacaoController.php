<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinTipoClassificacao Controller
 *
 * @property \App\Model\Table\FinTipoClassificacaoTable $FinTipoClassificacao
 */
class FinTipoClassificacaoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finTipoClassificacao = $this->paginate($this->FinTipoClassificacao);


        $this->set(compact('finTipoClassificacao'));
        $this->set('_serialize', ['finTipoClassificacao']);

    }

    /**
     * View method
     *
     * @param string|null $id Fin Tipo Classificacao id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finTipoClassificacao = $this->FinTipoClassificacao->get($id, [
            'contain' => []
        ]);

        $this->set('finTipoClassificacao', $finTipoClassificacao);
        $this->set('_serialize', ['finTipoClassificacao']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finTipoClassificacao = $this->FinTipoClassificacao->newEntity();
        if ($this->request->is('post')) {
            $finTipoClassificacao = $this->FinTipoClassificacao->patchEntity($finTipoClassificacao, $this->request->data);
            if ($this->FinTipoClassificacao->save($finTipoClassificacao)) {
                $this->Flash->success(__('O fin tipo classificacao foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin tipo classificacao não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finTipoClassificacao'));
        $this->set('_serialize', ['finTipoClassificacao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Tipo Classificacao id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finTipoClassificacao = $this->FinTipoClassificacao->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finTipoClassificacao = $this->FinTipoClassificacao->patchEntity($finTipoClassificacao, $this->request->data);
            if ($this->FinTipoClassificacao->save($finTipoClassificacao)) {
                $this->Flash->success(__('O fin tipo classificacao foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin tipo classificacao não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finTipoClassificacao'));
        $this->set('_serialize', ['finTipoClassificacao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Tipo Classificacao id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finTipoClassificacao = $this->FinTipoClassificacao->get($id);
                $finTipoClassificacao->situacao_id = 2;
        if ($this->FinTipoClassificacao->save($finTipoClassificacao)) {
            $this->Flash->success(__('O fin tipo classificacao foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fin tipo classificacao não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Tipo Classificacao id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinTipoClassificacao->find('all')
        ->where(['FinTipoClassificacao.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinTipoClassificacao.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Tipo Classificacao id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finTipoClassificacao = $this->FinTipoClassificacao->get($this->request->data['id']);
            $res = ['nome'=>$finTipoClassificacao->nome,'id'=>$finTipoClassificacao->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
