<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NfTomadores Controller
 *
 * @property \App\Model\Table\NfTomadoresTable $NfTomadores
 */
class NfTomadoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TipoLogradouros', 'TipoBairros', 'Cidades', 'Clientes', 'Responsavels']
        ];
        $nfTomadores = $this->paginate($this->NfTomadores);


        $this->set(compact('nfTomadores'));
        $this->set('_serialize', ['nfTomadores']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf Tomadore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nfTomadore = $this->NfTomadores->get($id, [
            'contain' => ['TipoLogradouros', 'TipoBairros', 'Cidades', 'Clientes', 'Responsavels']
        ]);

        $this->set('nfTomadore', $nfTomadore);
        $this->set('_serialize', ['nfTomadore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nfTomadore = $this->NfTomadores->newEntity();
        if ($this->request->is('post')) {
            $nfTomadore = $this->NfTomadores->patchEntity($nfTomadore, $this->request->data);
            if ($this->NfTomadores->save($nfTomadore)) {
                $this->Flash->success(__('O nf tomadore foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf tomadore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfTomadore'));
        $this->set('_serialize', ['nfTomadore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf Tomadore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nfTomadore = $this->NfTomadores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nfTomadore = $this->NfTomadores->patchEntity($nfTomadore, $this->request->data);
            if ($this->NfTomadores->save($nfTomadore)) {
                $this->Flash->success(__('O nf tomadore foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf tomadore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfTomadore'));
        $this->set('_serialize', ['nfTomadore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf Tomadore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nfTomadore = $this->NfTomadores->get($id);
                $nfTomadore->situacao_id = 2;
        if ($this->NfTomadores->save($nfTomadore)) {
            $this->Flash->success(__('O nf tomadore foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf tomadore não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf Tomadore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->NfTomadores->find('all')
        ->where(['NfTomadores.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('NfTomadores.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf Tomadore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nfTomadore = $this->NfTomadores->get($this->request->data['id']);
            $res = ['nome'=>$nfTomadore->nome,'id'=>$nfTomadore->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
