<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;

/**
 * AtendimentoitenArtigos Controller
 *
 * @property \App\Model\Table\AtendimentoitenArtigosTable $AtendimentoitenArtigos
 */
class AtendimentoitenArtigosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $session = new Session();
        if(!empty($session->read('artigos'))){
            $session->delete('artigos');
        }
        $vinculos =[];
        $itens_vinculos = $session->read('retorno_itens');
        if(!empty($itens_vinculos)){
            foreach ($itens_vinculos as $itens_vinculo) {
                $vinculos[]= $itens_vinculo['iten_id'];
            }
        }
        $this->paginate = [
            'contain' => ['AtendimentoItens', 'ProcedimentoGastos', 'Artigos'=>['strategy'=>'select']]
        ];
        $query = [];
        $atendimentoiten_id = null;
        if(!empty($this->request->query['atendimentoiten_id'])){
            $atendimentoiten_id = $this->request->query['atendimentoiten_id'];
            $query['conditions'][]= ['AtendimentoitenArtigos.aitens_id'=>$atendimentoiten_id];
        }

        $is_prontuario= false;
        if(!empty($this->request->query['pront'])){
            $is_prontuario = true;
        }

        $is_historico=null;
        if(!empty($this->request->query['historico'])){
            $is_historico = $this->request->query['historico'];
        }

        $this->paginate = array_merge($this->paginate,$query);

        $atendimentoitenArtigos = $this->paginate($this->AtendimentoitenArtigos);


        $this->set(compact('atendimentoitenArtigos','atendimentoiten_id','is_prontuario','vinculos','is_historico'));
        $this->set('_serialize', ['atendimentoitenArtigos']);

    }

    /**
     * View method
     *
     * @param string|null $id Atendimentoiten Artigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimentoitenArtigo = $this->AtendimentoitenArtigos->get($id, [
            'contain' => ['AtendimentoItens', 'ProcedimentoGastos', 'Artigos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('atendimentoitenArtigo', $atendimentoitenArtigo);
        $this->set('_serialize', ['atendimentoitenArtigo']);
    }

    public function vincular(){
        $session = new Session();
        $retorno = empty($session->read('retorno_itens')) ? [] : $session->read('retorno_itens');
        $add = false;
        if(!empty($retorno)){
            foreach ($retorno as $item => $value) {
                if($this->request->data['id']==$value['iten_id']){
                    $add= true;
                    unset($retorno[$item]);
                }
            }
        }

        if(!$add){
            $retorno[] = [
                'iten_id' => $this->request->data['id']
            ];
        }
        $session->delete('retorno_itens');
        $session->write('retorno_itens',$retorno);
        $this->autoRender=false;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atendimentoitenArtigo = $this->AtendimentoitenArtigos->newEntity();
        if ($this->request->is('post')) {
            $session = new Session();
            $artigos = $session->read('artigos');
            $res = ['msg'=>'Artigos salvos com sucesso!'];
            $url = $this->request->referer(true);
            $retorno = empty($session->read('retorno_itens')) ? [] : $session->read('retorno_itens');

            if(!empty($artigos)) {
                foreach ($artigos as $i => $item) {
                    $atendimentoitenArtigo = $this->AtendimentoitenArtigos->newEntity();
                    $this->request->data['procedimento_gasto_id'] = !empty($item['procedimento_gasto_id']) ?$item['procedimento_gasto_id'] :null;
                    $this->request->data['artigo_id'] = $item['artigo_id'];
                    $this->request->data['quantidade'] = $item['quantidade'];
                    $atendimentoitenArtigo = $this->AtendimentoitenArtigos->patchEntity($atendimentoitenArtigo, $this->request->data);
                    if (!$this->AtendimentoitenArtigos->save($atendimentoitenArtigo)) {
                        $res[]=['msg'=>$atendimentoitenArtigo->artigo_id.' não salvo!'];
                    }
                    if($url=="/clientes") {
                        $retorno[] = [
                            'iten_id' => $atendimentoitenArtigo->id
                        ];
                    }
                }

                $session->delete('retorno_itens');
                $session->write('retorno_itens',$retorno);
                echo json_encode($res);
            }
        }

        $atendimento_itens = null;
        $gastos=[];
        if(!empty($this->request->query['atendimentoiten_id'])){
            $this->loadModel('AtendimentoItens');
            $atendimento_itens = $this->AtendimentoItens->get($this->request->query['atendimentoiten_id'],['contain'=>['AtendimentoProcedimentos']]);

            $this->loadModel('ProcedimentoGastos');
            $procedimento_gastos = $this->ProcedimentoGastos->find('all')
                ->contain(['Gastos'=>['strategy' => 'select']])
                ->where(['ProcedimentoGastos.procedimento_id'=>$atendimento_itens->atendimento_procedimento->procedimento_id]);
            if(!empty($procedimento_gastos)){
                foreach ($procedimento_gastos as $pg) {
                    $gastos[$pg->id] =$pg->gasto->nome;
                }
            }
        }

        $this->set(compact('atendimentoitenArtigo','atendimento_itens','procedimento_gastos','gastos'));
        $this->set('_serialize', ['atendimentoitenArtigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimentoiten Artigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendimentoitenArtigo = $this->AtendimentoitenArtigos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoitenArtigo = $this->AtendimentoitenArtigos->patchEntity($atendimentoitenArtigo, $this->request->data);
            if ($this->AtendimentoitenArtigos->save($atendimentoitenArtigo)) {
                $this->Flash->success(__('O atendimentoiten artigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimentoiten artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('atendimentoitenArtigo'));
        $this->set('_serialize', ['atendimentoitenArtigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimentoiten Artigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $atendimentoitenArtigo = $this->AtendimentoitenArtigos->get($id);
                $atendimentoitenArtigo->situacao_id = 2;
        if ($this->AtendimentoitenArtigos->save($atendimentoitenArtigo)) {
            $this->Flash->success(__('O atendimentoiten artigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O atendimentoiten artigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Atendimentoiten Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AtendimentoitenArtigos->find('all')
        ->where(['AtendimentoitenArtigos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AtendimentoitenArtigos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Atendimentoiten Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $atendimentoitenArtigo = $this->AtendimentoitenArtigos->get($this->request->data['id']);
            $res = ['nome'=>$atendimentoitenArtigo->nome,'id'=>$atendimentoitenArtigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }


    public function artigos()
    {
        $session = new Session();
        $artigos = empty($session->read('artigos')) ? array() : $session->read('artigos');

        $add = false;

        if (!empty($this->request->query['artigo_id'])) {
            if (!empty($artigos)) {
                foreach ($artigos as $i => $item) {
                    if ($this->request->query['artigo_id'] == $item['artigo_id']) {
                        $add = true;
                    }
                }
            }

            if (!$add) {
                $this->loadModel('EstqArtigo');
                $artigo = $this->EstqArtigo->findById($this->request->query['artigo_id'])->first();
                if(!empty($artigo)){
                    $artigos[] = [
                        'procedimento_gasto_id'=>null,
                        'artigo_id'=>$artigo->id,
                        'artigo_codigo'=>$artigo->codigo_livre,
                        'artigo_nome'=>$artigo->nome,
                        'quantidade'=>!empty($this->request->query['quantidade']) ? $this->request->query['quantidade'] : 0

                    ];
                }
            }
        }

        if (!empty($this->request->query['procedimento_gasto_id'])) {
            $this->loadModel('ProcedimentoGastos');
            $procedimento_gasto = $this->ProcedimentoGastos->findById($this->request->query['procedimento_gasto_id'])->first();

            if(!empty($procedimento_gasto)) {
                $this->loadModel('GastoItens');
                $gasto_itens = $this->GastoItens->find('all')
                    ->contain(['EstqArtigo'])
                    ->where(['GastoItens.gasto_id' => $procedimento_gasto->gasto_id]);

                $ids=[];
                    if(!empty($artigos)) {
                        foreach ($artigos as $i => $item) {
                            $ids[] =$item['artigo_id'];
                        }
                    }

                if (!empty($gasto_itens)) {
                    foreach ($gasto_itens as $gasto_iten) {
                        if (!in_array($gasto_iten->estq_artigo->id,$ids)) {
                            $artigos[] = [
                                'procedimento_gasto_id' => $procedimento_gasto->id,
                                'artigo_id' => $gasto_iten->estq_artigo->id,
                                'artigo_codigo' => $gasto_iten->estq_artigo->codigo_livre,
                                'artigo_nome' => $gasto_iten->estq_artigo->nome,
                                'quantidade' => !empty($this->request->query['quantidade']) ? $this->request->query['quantidade'] : $gasto_iten->quantidade
                            ];
                        }
                    }
                }
            }
        }
        $session->delete('artigos');
        $session->write('artigos',$artigos);

        $this->set(compact('artigos'));
        $this->set('_serialize', ['artigos']);

    }

    public function dellArtigo()
    {
        $this->viewBuilder()->layout('ajax');
        $session = new Session();
        $artigos = $session->read('artigos');
        unset($artigos[$this->request->query['pos']]);
        $art_novo =[];
        $result = array_merge($artigos,$art_novo);
        $session->delete('artigos');
        $session->write('artigos',$result);
        $this->response->type('json');
       $this->response->body(json_encode($result));
       $this->autoRender=false;
    }

    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name'])){
            $name = $this->request->data['name'];
            $session = new Session();
            $artigos = $session->read('artigos');
            $artigos[$this->request->data['pk']][$name] =  $this->request->data['value'];

            $session->delete('artigos');
            $session->write('artigos',$artigos);
            $this->response->type('json');
                $res = ['res' => $this->request->data['value'],'msg'=>'Dados salvos com sucesso!'];
        }
        $this->response->body(json_encode($res));
    }

}
