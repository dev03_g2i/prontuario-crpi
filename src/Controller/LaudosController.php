<?php
namespace App\Controller;
use DateTime;
use Dompdf\Dompdf;
use Cake\I18n\Time;
use Cake\Routing\Router;
use App\Controller\AppController;


class LaudosController extends appController {

    public $components = array('Data', 'Laudo', 'Atendimento','Json');

    public function indexO()
    {
        $this->loadModel('AtendimentoProcedimentos');
        $query = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'SituacaoLaudos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.medico_id' => 1]);

        $user_diagnostico = $this->Auth->user('user_diagnostico');
        $profissional = $this->Auth->user('codigo_medico');
        $filtro_atalho = null;
        if($user_diagnostico == 0): // Tecnico
            $situacao_atalhos = [1 => 'Hoje', 2 => 'Aguardando', 3 => 'Realizados', 4 => 'Periodos'];
            $filtro_atalho = 1;
        elseif ($user_diagnostico == 1): // Radiologista
            $query->andWhere(['AtendimentoProcedimentos.medico_id' => $profissional]);
            $situacao_atalhos = [5 => 'Laudar/Assinar', 6 => 'Atrasados', 7 => 'Assinados', 4 => 'Periodos'];
            $filtro_atalho = 5;
        endif;

        // Inicializa o filtro por padrão com estes campos
        /*if(empty($this->request->getQuery('filtro_atalho'))){
            $this->request->query['filtro_atalho'] = $filtro_atalho;
            $this->request->query['tipo_data'] = 'Atendimentos.data';
            $this->request->query['profissional'] = $profissional;
        }*/

        $atendProc = $this->Laudo->filterAtendProc($query, $this->request->query);
        $this->paginate($atendProc);

        $atendimento_id = null;
        if(!empty($this->request->query('atendimento_id'))){
            $atendimento_id = $this->request->query('atendimento_id');
        }

        $grupo_procedimentos = $this->AtendimentoProcedimentos->Procedimentos->GrupoProcedimentos->find('list')->where(['GrupoProcedimentos.situacao_id' => 1])->orderAsc('GrupoProcedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id' => 1])->orderAsc('MedicoResponsaveis.nome');
        $situacao_laudos = $this->AtendimentoProcedimentos->SituacaoLaudos->find('list')->where(['SituacaoLaudos.situacao_id' => 1])->orderAsc('SituacaoLaudos.ordenar');
        $this->set(compact('atendProc', 'medicos', 'situacao_atalhos', 'situacao_laudos', 'atendimento_id', 'grupo_procedimentos', 'filtro_atalho', 'user_diagnostico'));

    }

    public function index()
    {
        $this->loadModel('AtendimentoProcedimentos');
        $query = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'SituacaoLaudos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1]);

        $user_diagnostico = $this->Auth->user('user_diagnostico');
        $profissional = $this->Auth->user('codigo_medico');
        $filtro_atalho = null;

        if($user_diagnostico == 0): // Tecnico
            $situacao_atalhos = [1 => 'Hoje', 2 => 'Aguardando', 3 => 'Realizados', 4 => 'Periodos'];
            $filtro_atalho = 1;
        elseif ($user_diagnostico == 1): // Radiologista
            $query->andWhere(['AtendimentoProcedimentos.medico_id' => $profissional]);
            $situacao_atalhos = [5 => 'Laudar/Assinar', 6 => 'Atrasados', 7 => 'Assinados', 4 => 'Periodos'];
            $filtro_atalho = 5;
        endif;

        $atendProc = $this->Laudo->filterAtendProc($query, $this->request->query);
        $this->paginate($atendProc);

        $atendimento_id = null;
        if(!empty($this->request->query('atendimento_id'))){
            $atendimento_id = $this->request->query('atendimento_id');
        }

        $grupo_procedimentos = $this->AtendimentoProcedimentos->Procedimentos->GrupoProcedimentos->find('list')->where(['GrupoProcedimentos.situacao_id' => 1])->orderAsc('GrupoProcedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id' => 1])->orderAsc('MedicoResponsaveis.nome');
        $situacao_laudos = $this->AtendimentoProcedimentos->SituacaoLaudos->find('list')->where(['SituacaoLaudos.situacao_id' => 1])->orderAsc('SituacaoLaudos.ordenar');
        $this->set(compact('atendProc', 'medicos', 'situacao_atalhos', 'situacao_laudos', 'atendimento_id', 'grupo_procedimentos', 'filtro_atalho', 'user_diagnostico'));

    }

    public function edit($id = null)
    {
        
        $this->loadModel('AtendimentoProcedimentos');
        $atendProc = $this->AtendimentoProcedimentos->get($id, [
            'contain' => ['Atendimentos' => ['Clientes']]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $laudos = $this->AtendimentoProcedimentos->patchEntity($atendProc, $this->request->data);
            if ($this->AtendimentoProcedimentos->save($atendProc)) {
                $this->Flash->success(__('O ficha atualizada com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A ficha não foi atualizada. Por favor, tente novamente.'));
            }
        }

        $exames = $this->AtendimentoProcedimentos->Procedimentos->find('list');
        //$profissional = $this->Laudos->AtendimentoProcedimentos->MedicoResponsaveis->find('list');
        //$this->loadModel('MedicoResponsaveis');
        $profissional = $this->AtendimentoProcedimentos->MedicoResponsaveis->getNomeLaudoList();
        $this->loadModel('Enfermeiros');
        $enfermeiro = $this->Enfermeiros->find('list');
        $this->loadModel('Tecnicos');
        $tecnicos = $this->Tecnicos->find('list');
        $this->loadModel('Prioridades');
        $prioridades = $this->Prioridades->find('list');
        $this->loadModel('AnotacaoModelos');
        $anotacaoModelos = $this->AnotacaoModelos->find('list')->where(['AnotacaoModelos.situacao_id' => 1])->orderAsc('AnotacaoModelos.nome');

        $this->set(compact('atendProc', 'exames', 'profissional', 'enfermeiro' ,'tecnicos', 'prioridades', 'anotacaoModelos'));
        $this->set('_serialize', ['atendProc']);

    }

    public function laudar($id)
    {   
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https://' : 'http://';
        $baseUrl = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'/userfiles/';
        //debug($baseUrl);exit;
        $caminho_assinatura = null;

        $atendProc = $this->Laudos->AtendimentoProcedimentos->get($id, [
            'contain' => ['Atendimentos' => ['Clientes', 'Solicitantes'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'Laudos']
        ]);
        if(!empty($atendProc->medico_responsavei->caminho_dir)){
            $caminho_assinatura = '/medicoresponsaveis/caminho/'.$atendProc->medico_responsavei->caminho_dir.
                '/'.$atendProc->medico_responsavei->caminho;
        }
        
        if(!empty($atendProc->laudos)){
            $laudos = $this->Laudos->get($atendProc->laudos[0]->id);
        }else {
            $laudos = $this->Laudos->newEntity();
        }

        if($this->request->is(['patch', 'post', 'put'])){

            if (!empty($this->request->data['assinar'])) {
                $this->request->data['dt_assinatura'] = date('d/m/Y H:i');
                $this->request->data['assinado_por'] = $this->Auth->user('id');
                $atendProc->situacao_laudo_id = 800; //Assinado
            }else {
                $atendProc->situacao_laudo_id = 400; //Em digitação
            }
            if(!empty($this->request->data('imprimir_laudo'))){
                $atendProc->situacao_laudo_id = 900; // impresso
            }
			 if($laudos->id){
                $laudos->alterado_por = $this->Auth->user('id');
            } 
			 if($laudos->digitado_por == null){
                $this->request->data['digitado_por'] = $this->Auth->user('id');                
            }
            $laudos = $this->Laudos->patchEntity($laudos, $this->request->data);
            if ($this->Laudos->save($laudos) && $this->Laudos->AtendimentoProcedimentos->save($atendProc)) {
                if (!empty($this->request->data['assinar'])) {
                    $this->Flash->success(__('O laudo foi assinado com sucesso.'));
                }else {
                    $this->Flash->success(__('O laudo foi salvo com sucesso.'));
                }
                return $this->redirect(['action' => 'laudar', $id]);
            } else {
                $this->Flash->error(__('O Laudo não foi salvo. Por favor, tente novamente.'));
            }

        }

        $this->loadModel('LaudosConfiguracoes');
        $config_laudo = $this->LaudosConfiguracoes->find()->first();
        $this->loadModel('Textos');
        $textos = $this->Textos->find('list')->where(['Textos.situacao_id' => 1])->orderAsc('Textos.codigo');
        $this->set(compact('laudos', 'atendProc', 'textos','caminho_assinatura', 'config_laudo'));
        $this->set('_serialize', ['laudos']);
    }

    public function entregarResultado($id = null)
    {
        $atendProc = $this->Laudos->AtendimentoProcedimentos->find('all')
                            ->contain(['Atendimentos' => ['Clientes'], 'Procedimentos'])
                            ->where(['Atendimentos.cliente_id' => $id]);

        if($this->request->is('post')){
            if(!empty($this->request->data('entregar'))){
                $error = 0;
                if(is_array($this->request->data('entregar'))){
                    foreach ($this->request->data('entregar') as $atendProc_id){
                        $atendProc = $this->Laudos->AtendimentoProcedimentos->get($atendProc_id);
                        $atendProc->dt_entrega_resultado = date('Y-m-d H:i:s');
                        $atendProc->entregue_por = $this->Auth->user('nome');
                        $atendProc->retirado_por = $this->request->data('retirado_por');
                        $atendProc->situacao_laudo_id = 2000; // Entregue
                        if($this->Laudos->AtendimentoProcedimentos->save($atendProc)){
                            $error = 0;
                        }else{
                            $error +=1;
                        }
                    }
                }
            }
            if($error == 0){
                $this->Flash->success(__('O laudo foi entregue com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }else {
                $this->Flash->success(__('Falha ao tentar entregar laudo.'));
                return $this->redirect(['action' => 'index']);
            }
        }

        $dados = $atendProc->first();
        $this->set(compact('atendProc', 'dados'));
    }

    public function imprimir()
    {
        $laudos = $this->Laudos->get($this->request->query['id'],[
            'contain' =>[
                'AtendimentoProcedimentos' =>[
                    'Procedimentos',
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ]
            ]
        ]);
        $idade = new Time ($laudos->atendimento_procedimento->atendimento->cliente->nascimento);
        $diff = $idade->diff(new DateTime());
        $idade = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.header_footer')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');

        $caminho_assinatura = null;
        $domPdf = new Dompdf();
        $domPdf->loadHtml($laudos->texto_html);
        $domPdf->setPaper('a4');
        $domPdf->render();
        if(!empty($laudos->atendimento_procedimento->medico_responsavei->caminho_dir)){
            $caminho_assinatura = './files/medicoresponsaveis/caminho/'.$laudos->atendimento_procedimento->medico_responsavei->caminho_dir.
                '/'.$laudos->atendimento_procedimento->medico_responsavei->caminho;
        }
        $this->set(compact('laudos','idade','caminho_assinatura'));
        $this->set('_serialize', ['laudos   ']);
    }

    /**
     * modelo03
     *
     * @return void
     */
    public function modelo03()
    {
        $laudos = $this->Laudos->get($this->request->query['id'],[
            'contain' =>[
                'AtendimentoProcedimentos' =>[
                    'Procedimentos',
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ]
            ]
        ]);
        $idade = new Time ($laudos->atendimento_procedimento->atendimento->cliente->nascimento);
        $diff = $idade->diff(new DateTime());
        $idade = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');

        $caminho_assinatura = null;
        $domPdf = new Dompdf();
        $domPdf->loadHtml($laudos->texto_html);
        $domPdf->setPaper('a4');
        $domPdf->render();
        if(!empty($laudos->atendimento_procedimento->medico_responsavei->caminho_dir)){
            $caminho_assinatura = './files/medicoresponsaveis/caminho/'.$laudos->atendimento_procedimento->medico_responsavei->caminho_dir.
                '/'.$laudos->atendimento_procedimento->medico_responsavei->caminho;
        }
        $this->set(compact('laudos','idade','caminho_assinatura'));
        $this->set('_serialize', ['laudos   ']);
    }

    /**
     * Modelo01 foi pego do CRPI;
     *
     * @return void
     */
    public function modelo01()
    {
        $this->loadModel('LaudosConfiguracoes');
        $config_laudo = $this->LaudosConfiguracoes->find()->first();

        $laudo = $this->Laudos->get($this->request->getQuery('id'),[
            'contain' => [
                'AtendimentoProcedimentos' =>[
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ],'SituacaoCadastros', 'Users','AlteradoPor','Digitado'
            ]
        ]);

        $pdf = $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.modelo01')
            ->className('Dompdf.Pdf');
        if($laudo->paginas==1) {
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser',
                'paginate' => [
                    'x' => 510,
                    'y' => 716,
                    'text' => "Página: {PAGE_NUM} / {PAGE_COUNT}",
                    'size' => "08pt"
                ]
            ]]);
        }else{
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser'
            ]]);

        }
        $json = [
            "texto" => $laudo->texto_html,
            "paciente" => $laudo->atendimento_procedimento->atendimento->cliente->nome,
            "data_nasc" => !empty($laudo->atendimento_procedimento->atendimento->cliente->nascimento)?$laudo->atendimento_procedimento->atendimento->cliente->nascimento->format('d/m/Y'):'',
            "exame" => $laudo->atendimento_procedimento->procedimento->nome,
            "solicitante" => $laudo->atendimento_procedimento->atendimento->solicitante->nome,
            "profissional" => $laudo->atendimento_procedimento->medico_responsavei->nome,
            "data" => !empty($laudo->atendimento_procedimento->atendimento->data)?$laudo->atendimento_procedimento->atendimento->data->format('d/m/Y'):'',
            "digitado_por" => $laudo->digitado->nome,
			"alterado_por" => @$laudo->alterado_por->nome ? $laudo->alterado_por->nome : '-',
            "complemento" => $laudo->atendimento_procedimento->complemento
        ];

        $laudos = $laudo;
        $laudo = (object)$json;
        $this->set(compact('laudo','laudos'));
        $this->set('_serialize', ['laudo']);
    }

    /**
     * Modelo02 SIM RADIOLOGIA;
     *
     * @return void
     */
    public function modelo02()
    {
        $this->loadModel('LaudosConfiguracoes');
        $config_laudo = $this->LaudosConfiguracoes->find()->first();

        $laudo = $this->Laudos->get($this->request->getQuery('id'),[
            'contain' => [
                'AtendimentoProcedimentos' =>[
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ],'SituacaoCadastros', 'Users'
            ]
        ]);

        $pdf = $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.modelo01')
            ->className('Dompdf.Pdf');
        if($laudo->paginas==1) {
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser',
                'paginate' => [
                    'x' => 510,
                    'y' => 716,
                    'text' => "Página: {PAGE_NUM} / {PAGE_COUNT}",
                    'size' => "08pt"
                ]
            ]]);
        }else{
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser'
            ]]);

        }
        $json = [
            "texto" => $laudo->texto_html,
            "paciente" => $laudo->atendimento_procedimento->atendimento->cliente->nome,
            "exame" => $laudo->atendimento_procedimento->procedimento->nome,
            "solicitante" => $laudo->atendimento_procedimento->atendimento->solicitante->nome,
            "profissional" => $laudo->atendimento_procedimento->medico_responsavei->nome,
            "data" => $laudo->atendimento_procedimento->atendimento->data,
            "digitado_por" => $laudo->user->nome,
            "complemento" => $laudo->atendimento_procedimento->complemento
        ];

        $laudos = $laudo;
        $laudo = (object)$json;
        $this->set(compact('laudo','laudos'));
        $this->set('_serialize', ['laudo']);
    }

    /**
     * Modelo 04
     *
     * @return void
     */
    public function modelo04()
    {
        $this->loadModel('LaudosConfiguracoes');
        $this->loadModel('Configuracoes');
        $config_laudo = $this->LaudosConfiguracoes->find()->first();
        $configuracoes = $this->Configuracoes->find()->first();
        $endereco = $configuracoes->endereco.' - Fone: '.$configuracoes->telefone.'<br> Fax: '.$configuracoes->fax;

        $laudo = $this->Laudos->get($this->request->getQuery('id'),[
            'contain' => [
                'AtendimentoProcedimentos' =>[
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ],'SituacaoCadastros', 'Users'
            ]
        ]);

        $pdf = $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.modelo01')
            ->className('Dompdf.Pdf');
        if($laudo->paginas==1) {
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser',
                'paginate' => [
                    'x' => 512,
                    'y' => 110,
                    'text' => "Página: {PAGE_NUM} / {PAGE_COUNT}",
                    'size' => "08pt"
                ]
            ]]);
        }else{
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser'
            ]]);

        }

        $caminho_assinatura = null;
        if(!empty($laudo->atendimento_procedimento->medico_responsavei->caminho_dir)){
            $caminho_assinatura = './files/medicoresponsaveis/caminho/'.$laudo->atendimento_procedimento->medico_responsavei->caminho_dir.
                '/'.$laudo->atendimento_procedimento->medico_responsavei->caminho;
        }

        $json = [
            "texto" => $laudo->texto_html,
            "paciente" => $laudo->atendimento_procedimento->atendimento->cliente->nome,
            "exame" => $laudo->atendimento_procedimento->procedimento->nome,
            "solicitante" => $laudo->atendimento_procedimento->atendimento->solicitante->nome,
            "profissional" => $laudo->atendimento_procedimento->medico_responsavei->nome,
            "data" => $laudo->atendimento_procedimento->atendimento->data,
            "digitado_por" => $laudo->user->nome,
            "complemento" => $laudo->atendimento_procedimento->complemento,
            "endereco" => $endereco,
            "titulo_clinica" => $config_laudo->titulo_clinica,
        ];

        $laudos = $laudo;
        $laudo = (object)$json;
        $this->set(compact('laudo','laudos', 'caminho_assinatura'));
        $this->set('_serialize', ['laudo']);
    }

    /**
     * modelo05
     *
     * @return void
     */
    public function modelo05()
    {
        $laudos = $this->Laudos->get($this->request->query['id'],[
            'contain' =>[
                'AtendimentoProcedimentos' =>[
                    'Procedimentos',
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ]
            ]
        ]);
        $idade = new Time ($laudos->atendimento_procedimento->atendimento->cliente->nascimento);
        $diff = $idade->diff(new DateTime());
        $idade = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');

        $caminho_assinatura = null;
        $domPdf = new Dompdf();
        $domPdf->loadHtml($laudos->texto_html);
        $domPdf->setPaper('a4');
        $domPdf->render();
        if(!empty($laudos->atendimento_procedimento->medico_responsavei->caminho_dir)){
            $caminho_assinatura = './files/medicoresponsaveis/caminho/'.$laudos->atendimento_procedimento->medico_responsavei->caminho_dir.
                '/'.$laudos->atendimento_procedimento->medico_responsavei->caminho;
        }
        $this->set(compact('laudos','idade','caminho_assinatura'));
        $this->set('_serialize', ['laudos   ']);
    }

    public function exames($id)
    {
        $this->loadModel('AtendimentoProcedimentos');
        $atendProc = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'SituacaoLaudos'])
            ->where(['Atendimentos.cliente_id' => $id]);

        $query = $atendProc->first();
        $paciente = $query->atendimento->cliente;

        $this->paginate($atendProc);

        $this->set(compact('atendProc', 'paciente'));
    }

    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $cliente = $this->Clientes->get($this->request->data['id']);
            $res = ['nome' => $cliente->nome, 'id' => $cliente->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function inicarFinalizarExame()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('AtendimentoProcedimentos');
        $atendProc = $this->AtendimentoProcedimentos->get($this->request->data('id'));
        if($this->request->data('column') == 'inicio_exame'){
            $atendProc->inicio_exame = date('Y-m-d H:i:s');
            $atendProc->situacao_laudo_id = 110;// Em sala
            $msg = 'Exame iniciado com sucesso!';
        }elseif($this->request->data('column') == 'fim_exame'){
            $atendProc->fim_exame = date('Y-m-d H:i:s');
            $atendProc->situacao_laudo_id = 200;// Realizado
            $msg = 'Exame finalizado com sucesso!';
        }
        if($this->AtendimentoProcedimentos->save($atendProc)){
            $res = ['res' => 1, 'msg' => $msg];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function countLaudos()
    {
        $this->loadModel('AtendimentoProcedimentos');
        $query = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'SituacaoLaudos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1]);

        $profissional = $this->Auth->user('codigo_medico');
        $user_diagnostico = $this->Auth->user('user_diagnostico');
        if($user_diagnostico == 1){// Radiologista
            $query->andWhere(['AtendimentoProcedimentos.medico_id' => $profissional]);
        }

        // Inicializa o filtro por padrão com estes campos
        /*if(empty($this->request->query['filtro_atalho']) && empty($this->request->query['tipo_data'])){
            $this->request->query['filtro_atalho'] = 1;// Hoje
            $this->request->query['tipo_data'] = 'Atendimentos.data';
        }*/

        $query = $this->Laudo->filterAtendProc($query, $this->request->getQuery());

        $aguardando = $query->newExpr()
            ->addCase(
                $query->newExpr()->add(['inicio_exame IS' => null, 'fim_exame IS' => null]),
                1,
                'integer'
            );
        $exame = $query->newExpr()
            ->addCase(
                $query->newExpr()->add(['inicio_exame IS NOT' => null, 'fim_exame IS' => null]),
                1,
                'integer'
            );
        $liberado = $query->newExpr()
            ->addCase(
                $query->newExpr()->add(['inicio_exame IS NOT' => null, 'fim_exame IS NOT' => null]),
                1,
                'integer'
            );

        $query_select = $query->select([
            'aguardando' => $query->func()->count($aguardando),
            'exame' => $query->func()->count($exame),
            'liberado' => $query->func()->count($liberado)
        ]);

        $json = json_encode($query_select->first());
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function etiquetas(){
        if($this->request->is(['post'])){
            $this->viewBuilder()->setLayout('report_2016');
            $json = $this->Atendimento->etiqueta($this->request->getData('ids'));
            $this->Json->create('etiqueta.json', json_encode($json));
            $this->set('report', 'etiqueta01.mrt');
            $this->render('report');
        }
    }
	
	/**
     * Backup
     */
    public function modelo01_backup()
    {
        $this->loadModel('LaudosConfiguracoes');
        $config_laudo = $this->LaudosConfiguracoes->find()->first();

        $laudo = $this->Laudos->get($this->request->getQuery('id'),[
            'contain' => [
                'AtendimentoProcedimentos' =>[
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ],'SituacaoCadastros', 'Users','AlteradoPor','Digitado'
            ]
        ]);

        $pdf = $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.modelo07')
            ->className('Dompdf.Pdf');
        if($laudo->paginas==1) {
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser',
                'paginate' => [
                    'x' => 510,
                    'y' => 716,
                    'text' => "Página: {PAGE_NUM} / {PAGE_COUNT}",
                    'size' => "08pt"
                ]
            ]]);
        }else{
            $pdf->options(['config' => [
                'filename' => 'laudo',
                'render' => 'browser'
            ]]);

        }
        $json = [
            "texto" => $laudo->texto_html,
            "paciente" => $laudo->atendimento_procedimento->atendimento->cliente->nome,
            "data_nasc" => !empty($laudo->atendimento_procedimento->atendimento->cliente->nascimento)?$laudo->atendimento_procedimento->atendimento->cliente->nascimento->format('d/m/Y'):'',
            "exame" => $laudo->atendimento_procedimento->procedimento->nome,
            "solicitante" => $laudo->atendimento_procedimento->atendimento->solicitante->nome,
            "profissional" => $laudo->atendimento_procedimento->medico_responsavei->nome,
            "data" => !empty($laudo->atendimento_procedimento->atendimento->data)?$laudo->atendimento_procedimento->atendimento->data->format('d/m/Y'):'',
            "digitado_por" => $laudo->digitado->nome,
			"alterado_por" => @$laudo->alterado_por->nome ? $laudo->alterado_por->nome : '-',
            "complemento" => $laudo->atendimento_procedimento->complemento
        ];

        $laudos = $laudo;
        $laudo = (object)$json;
        $this->set(compact('laudo','laudos'));
        $this->set('_serialize', ['laudo']);
    }

    public function printCrpiGeral()
    {
        $this->loadModel('LaudosConfiguracoes');
        $this->viewBuilder()->setLayout('ajax');
        $config_laudo = $this->LaudosConfiguracoes->find()->first();

        $laudo = $this->Laudos->get($this->request->getQuery('id'),[
            'contain' => [
                'AtendimentoProcedimentos' =>[
                    'Procedimentos' => ['GrupoProcedimentos'],
                    'MedicoResponsaveis',
                    'Atendimentos' => [
                        'Convenios',
                        'Clientes',
                        'Solicitantes'
                    ]
                ],'SituacaoCadastros', 'Users','ModificadoPors','Digitado'
            ]
        ]);

        $json = [
            "texto" => $laudo->texto_html,
            "paciente" => $laudo->atendimento_procedimento->atendimento->cliente->nome,
            "data_nasc" => !empty($laudo->atendimento_procedimento->atendimento->cliente->nascimento)?$laudo->atendimento_procedimento->atendimento->cliente->nascimento->format('d/m/Y'):'',
            "exame" => $laudo->atendimento_procedimento->procedimento->nome,
            "solicitante" => $laudo->atendimento_procedimento->atendimento->solicitante->nome,
            "profissional" => $laudo->atendimento_procedimento->medico_responsavei->nome,
            "data" => !empty($laudo->atendimento_procedimento->atendimento->data)?$laudo->atendimento_procedimento->atendimento->data->format('d/m/Y'):'',
            "digitado_por" => $laudo->digitado->nome,
            "alterado_por" => !empty($laudo->modificado_por)? $laudo->modificado_por->nome : '-',
            "complemento" => $laudo->atendimento_procedimento->complemento
        ];

        $laudos = $laudo;
        $laudo = (object)$json;
        $this->set(compact('laudo','laudos'));
        $this->set('_serialize', ['laudo']);
    }
}
?>