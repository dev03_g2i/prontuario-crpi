<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Faces Controller
 *
 * @property \App\Model\Table\FacesTable $Faces
 */
class FacesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['Faces.situacao_id = ' => '1']
                    ];
        $faces = $this->paginate($this->Faces);

        $this->set(compact('faces'));
        $this->set('_serialize', ['faces']);
    }

    /**
     * View method
     *
     * @param string|null $id Face id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $face = $this->Faces->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('face', $face);
        $this->set('_serialize', ['face']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $face = $this->Faces->newEntity();
        if ($this->request->is('post')) {
            $face = $this->Faces->patchEntity($face, $this->request->data);
            if ($this->Faces->save($face)) {
                $this->Flash->success(__('O face foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O face não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Faces->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Faces->Users->find('list', ['limit' => 200]);
        $this->set(compact('face', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['face']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Face id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $face = $this->Faces->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $face = $this->Faces->patchEntity($face, $this->request->data);
            if ($this->Faces->save($face)) {
                $this->Flash->success(__('O face foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O face não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Faces->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Faces->Users->find('list', ['limit' => 200]);
        $this->set(compact('face', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['face']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Face id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $face = $this->Faces->get($id);
                $face->situacao_id = 2;
        if ($this->Faces->save($face)) {
            $this->Flash->success(__('O face foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O face não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
