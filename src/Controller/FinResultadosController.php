<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * FinResultados Controller
 *
 * @property \App\Model\Table\FinMovimentosTable $FinMovimentos
 */
class FinResultadosController extends AppController
{
    public $components = ['Query'];

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Resultados');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('FinMovimentos');
        $ano = date('Y');
        $plano_conta = null;
        $classificacao_conta = null;
        $tipo = null;

        $relatorio = !empty($this->request->query(['relatorio'])) ? $this->request->query(['relatorio']) : null;

        if (isset($relatorio)){
            if ($relatorio === '1'){
                $tabelaHistoricos = $this->Query->selectTabelaResultadosHistoricos($ano, $plano_conta, $classificacao_conta, $tipo);
            } else if($relatorio === '2'){
                $tabelaHistoricosAbertos = $this->Query->selectTabelaResultadosHistoricosAbertos($ano, $plano_conta, $classificacao_conta, $tipo);
            }
        }
        
        if (!empty($this->request->getQuery('ano'))) {
            $ano = $this->request->getQuery('ano');
        }

        if (!empty($this->request->getQuery('plano_conta'))) {
            $plano_conta = $this->request->getQuery('plano_conta');
        }

        if (!empty($this->request->getQuery('classificacao_conta'))) {
            $classificacao_conta = $this->request->getQuery('classificacao_conta');
        }

        if (!empty($this->request->getQuery('tipo'))) {
            $tipo = $this->request->getQuery('tipo');
        }

        $valoresTotaisEntradaSaida = $this->FinMovimentos->getValorTotalPorAno($ano, $plano_conta, $classificacao_conta, $tipo);
        $valoresTotaisMesAMes = $this->FinMovimentos->getTotaisMesAMes($ano, $plano_conta, $classificacao_conta, $tipo);

        $planoContas = $this->FinMovimentos->FinPlanoContas->find('list')->where(['FinPlanoContas.situacao' => 1])->order('FinPlanoContas.nome ASC');
        $classificacaoContas = $this->FinMovimentos->FinPlanoContas->FinClassificacaoContas->find('list')->where(['FinClassificacaoContas.situacao' => 1])->order('FinClassificacaoContas.descricao ASC');

        $this->set(compact('tabelaHistoricosAbertos', 'tabelaHistoricos', 'valoresTotaisEntradaSaida', 'valoresTotaisMesAMes', 'planoContas', 'classificacaoContas'));
        $this->set('_serialize', ['finMovimentos']);
    }

}
