<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class FinIndicadoresController extends AppController
{
    //Componentes externos para serem usados.
    public $components = ['Date', 'Model'];

    //Função para renderizar a dashboard index.
    public function index()
    {
        $month = !empty($this->request->query(['month'])) ? $this->request->query(['month']) : date('m');
        $year = !empty($this->request->query(['year'])) ? $this->request->query(['year']) : date('Y');
        
        $resultMoves = $this->workWithMovesData($month, $year);
        $resultMovesForClass = $this->workWithMovesForClassData($month, $year);

        $actualMonth = date('m');
        $actualYear = date('Y');

        $months = $this->Date->getMonthsNames();
        $years = $this->Date->yearsList();

        $this->set(compact('resultMoves', 'resultMovesForClass', 'actualMonth', 'actualYear', 'months', 'years'));
        $this->set('_serialize', ['resultMoves', 'resultMovesForClass', 'actualMonth', 'actualYear', 'months', 'years']);
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Funções que só podem ser acessadas por essa controller. *
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    //Função para iteração de soma de saldos.
    private function workWithBankMovesData($bankMoves)
    {
        $resultBankMoves = [];
        $resultBankMoves[0] = 0;
        $resultBankMoves[1] = 0;

        foreach ($bankMoves as $move) {
            $resultBankMoves[0] += floatval($move->soma_movimentos_saldos);
            $resultBankMoves[1] += $move->saldoInicial;
        }
        return $resultBankMoves;
    }

    //Função para iteração dos Movimentos.
    private function workWithMovesData($month, $year)
    {   
        $actualDate = new Time($year.'-'.$month.'-'.'01');
        $firstDate = clone $actualDate; 
        $firstDate = $firstDate->subMonth(11);

        for ($i = 0; $i < 12; $i++) {

            if ($i === 0)
                $date = $firstDate->format('Y-m-01');
            else
                $date = $firstDate->addMonth(1)->format('Y-m-01');

            $movesOfMonthInUse = $this->Model->getModelData('FinMovimentos', 'movesOfLastTwelveMonths', $date);

            if (isset($movesOfMonthInUse) && $movesOfMonthInUse->competencia == null) $movesOfMonthInUse->competencia = $firstDate->format('m-Y');

            $competencia = $firstDate->format('m-Y');
            $resultMoves[] = $this->createClassForMovesData($competencia, 'moves');

            if (isset($movesOfMonthInUse)) $resultMoves[$i] = $movesOfMonthInUse;
        }
        return $resultMoves;
    }

    private function workWithMovesForClassData($month, $year)
    {
        $resultMovesForClass = new \stdClass();
        $resultMovesForClass->competencias = [];
        $resultMovesForClass->arrayMovesForClass = [];
        // $resultMovesForClass->movesForClass = new \stdClass();
        // $resultMovesForClass->movesForClass->descricao = '';
        // $resultMovesForClass->movesForClass->array = [];
        
        $actualDate = new Time($year.'-'.$month.'-'.'01');
        $firstDate = clone $actualDate; 
        $FinClassificacaoContas = TableRegistry::get('FinClassificacaoContas');
        $classificacaoContas = $FinClassificacaoContas->find('list')->toArray();

        foreach($classificacaoContas as $key => $classificacaoConta)
        {
            $firstDate = $firstDate->subMonth(11);
            $resultMovesForClass->arrayMovesForClass[$key] = new \stdClass();
            $resultMovesForClass->arrayMovesForClass[$key]->descricao = $classificacaoConta;
            $resultMovesForClass->arrayMovesForClass[$key]->moves = [];

            for ($i = 0; $i < 12; $i++) {
                
                if ($key === 1) $resultMovesForClass->competencias[] = $firstDate->format('m-Y');

                if ($i === 0)
                    $date = $firstDate->format('Y-m-01');
                else
                    $date = $firstDate->addMonth(1)->format('Y-m-01');
    
                $movesForClassOfMonthInUse = $this->Model->getModelData('FinMovimentos', 'movesForClassOfLastTwelveMonths', $key, $date);

                $resultMovesForClass->arrayMovesForClass[$key]->moves[] = $this->createClassForMovesData(null, 'movesForClass');

                if (isset($movesForClassOfMonthInUse)) $resultMovesForClass->arrayMovesForClass[$key]->moves[$i] = $movesForClassOfMonthInUse;
            }
        }
        return $resultMovesForClass;
    }

    //Função para criar um objeto para a iteração de movimentos
    private function createClassForMovesData($competencia = null, $type)
    {
        if ($type === 'moves') {
            $object = new \stdClass(); //Atribuindo uma classe à posição do array!
            $object->competencia = $competencia; //Note que o valor de $i começa em 0, pois o array pego do banco começa em 0;
            $object->entradas = "0.00";
            $object->saidas = "0.00";
            $object->saldo = "0.00";
        } else {
            $object = new \stdClass(); //Atribuindo uma classe à posição do array!
            $object->saldo = "0.00";
        }

        return $object;
    }
}
