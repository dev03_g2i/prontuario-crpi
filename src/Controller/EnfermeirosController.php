<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Enfermeiros Controller
 *
 * @property \App\Model\Table\EnfermeirosTable $Enfermeiros
 */
class EnfermeirosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
        ];
        $enfermeiros = $this->paginate($this->Enfermeiros);


        $this->set(compact('enfermeiros'));
        $this->set('_serialize', ['enfermeiros']);

    }

    /**
     * View method
     *
     * @param string|null $id Enfermeiro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $enfermeiro = $this->Enfermeiros->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('enfermeiro', $enfermeiro);
        $this->set('_serialize', ['enfermeiro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $enfermeiro = $this->Enfermeiros->newEntity();
        if ($this->request->is('post')) {
            $enfermeiro = $this->Enfermeiros->patchEntity($enfermeiro, $this->request->data);
            if ($this->Enfermeiros->save($enfermeiro)) {
                $this->Flash->success(__('O enfermeiro foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O enfermeiro não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('enfermeiro'));
        $this->set('_serialize', ['enfermeiro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Enfermeiro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $enfermeiro = $this->Enfermeiros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $enfermeiro = $this->Enfermeiros->patchEntity($enfermeiro, $this->request->data);
            if ($this->Enfermeiros->save($enfermeiro)) {
                $this->Flash->success(__('O enfermeiro foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O enfermeiro não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('enfermeiro'));
        $this->set('_serialize', ['enfermeiro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Enfermeiro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $enfermeiro = $this->Enfermeiros->get($id);
                $enfermeiro->situacao_id = 2;
        if ($this->Enfermeiros->save($enfermeiro)) {
            $this->Flash->success(__('O enfermeiro foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O enfermeiro não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Enfermeiro id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Enfermeiros->find('all')
        ->where(['Enfermeiros.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Enfermeiros.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Enfermeiro id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $enfermeiro = $this->Enfermeiros->get($this->request->data['id']);
            $res = ['nome'=>$enfermeiro->nome,'id'=>$enfermeiro->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
