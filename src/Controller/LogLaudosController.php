<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LogLaudos Controller
 *
 * @property \App\Model\Table\LogLaudosTable $LogLaudos
 */
class LogLaudosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id = null)
    {
        $query = $this->LogLaudos->find('all')->contain(['UserReg', 'UserAssinado']);

        if(!empty($id))
            $query->where(['LogLaudos.laudo_id' => $id]);

        $logLaudos = $this->paginate($query);

        $this->set(compact('logLaudos'));
        $this->set('_serialize', ['logLaudos']);

    }

    /**
     * View method
     *
     * @param string|null $id Log Laudo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $logLaudo = $this->LogLaudos->get($id, [
            'contain' => ['Laudos']
        ]);

        $this->set('logLaudo', $logLaudo);
        $this->set('_serialize', ['logLaudo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $logLaudo = $this->LogLaudos->newEntity();
        if ($this->request->is('post')) {
            $logLaudo = $this->LogLaudos->patchEntity($logLaudo, $this->request->data);
            if ($this->LogLaudos->save($logLaudo)) {
                $this->Flash->success(__('O log laudo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O log laudo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('logLaudo'));
        $this->set('_serialize', ['logLaudo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Log Laudo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $logLaudo = $this->LogLaudos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $logLaudo = $this->LogLaudos->patchEntity($logLaudo, $this->request->data);
            if ($this->LogLaudos->save($logLaudo)) {
                $this->Flash->success(__('O log laudo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O log laudo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('logLaudo'));
        $this->set('_serialize', ['logLaudo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Log Laudo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $logLaudo = $this->LogLaudos->get($id);
                $logLaudo->situacao_id = 2;
        if ($this->LogLaudos->save($logLaudo)) {
            $this->Flash->success(__('O log laudo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O log laudo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Log Laudo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->LogLaudos->find('all')
        ->where(['LogLaudos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('LogLaudos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Log Laudo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $logLaudo = $this->LogLaudos->get($this->request->data['id']);
            $res = ['nome'=>$logLaudo->nome,'id'=>$logLaudo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
