<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Planocontas Controller
 *
 * @property \App\Model\Table\PlanocontasTable $Planocontas
 */
class PlanocontasController extends AppController
{

    
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $planocontas = $this->paginate($this->Planocontas);

        $this->set(compact('planocontas'));
        $this->set('_serialize', ['planocontas']);
    }

    /**
     * View method
     *
     * @param string|null $id Planoconta id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $planoconta = $this->Planocontas->get($id, [
            'contain' => []
        ]);

        $this->set('planoconta', $planoconta);
        $this->set('_serialize', ['planoconta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $planoconta = $this->Planocontas->newEntity();
        if ($this->request->is('post')) {
            $planoconta = $this->Planocontas->patchEntity($planoconta, $this->request->data);
            if ($this->Planocontas->save($planoconta)) {
                $this->Flash->success(__('O planoconta foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O planoconta não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('planoconta'));
        $this->set('_serialize', ['planoconta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Planoconta id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $planoconta = $this->Planocontas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $planoconta = $this->Planocontas->patchEntity($planoconta, $this->request->data);
            if ($this->Planocontas->save($planoconta)) {
                $this->Flash->success(__('O planoconta foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O planoconta não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('planoconta'));
        $this->set('_serialize', ['planoconta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Planoconta id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $planoconta = $this->Planocontas->get($id);
                $planoconta->situacao_id = 2;
        if ($this->Planocontas->save($planoconta)) {
            $this->Flash->success(__('O planoconta foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O planoconta não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function fill()
    {
        $this->loadModel('Planocontas');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->Planocontas->find('all')
            ->where(['Planocontas.status =' => 1,
                'Planocontas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Planocontas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function getregistro(){
        $this->loadModel('Planocontas');
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['res'=>'-1','msg'=>'Erro ao buscar registro'];
        if(!empty($this->request->query['id'])) {
            $plano = $this->Planocontas->get($this->request->query['id']);
            $res = ['res'=>$plano];
        }
        $this->response->body(json_encode($res));


    }

    public function getlist(){
        $this->loadModel('Planocontas');
        $this->autoRender=false;
        $this->response->type('json');

        $lista = $this->Planocontas->find('all')->where(['Planocontas.status'=>1])->orderAsc('Planocontas.nome');
        $itens=[];
        foreach ($lista as $item) {
            $itens[] =[
                'value'=>$item->id,
                'text'=>$item->nome
            ];
        }

        $this->response->body(json_encode($itens));

    }
}
