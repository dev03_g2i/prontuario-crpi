<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AgendaTel Controller
 *
 * @property \App\Model\Table\AgendaTelTable $AgendaTel
 */
class AgendaTelController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id = null)
    {   

        $query = $this->AgendaTel->find('all');

        if (!empty($this->request->query['id'])) {
            $query->andWhere(['AgendaTel.id ' => $this->request->query['id']]);
        }

        $agendaTel = $this->paginate($query);
        
        $this->set(compact('agendaTel'));
        $this->set('_serialize', ['agendaTel']);

    }

    /**
     * View method
     *
     * @param string|null $id Agenda Tel id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendaTel = $this->AgendaTel->get($id, [
            'contain' => []
        ]);

        $this->set('agendaTel', $agendaTel);
        $this->set('_serialize', ['agendaTel']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agendaTel = $this->AgendaTel->newEntity();
        if ($this->request->is('post')) {
            $agendaTel = $this->AgendaTel->patchEntity($agendaTel, $this->request->data);
            if ($this->AgendaTel->save($agendaTel)) {
                $this->Flash->success(__('O contato foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contato não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('agendaTel'));
        $this->set('_serialize', ['agendaTel']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda Tel id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendaTel = $this->AgendaTel->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendaTel = $this->AgendaTel->patchEntity($agendaTel, $this->request->data);
            if ($this->AgendaTel->save($agendaTel)) {
                $this->Flash->success(__('O contato foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contato não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('agendaTel'));
        $this->set('_serialize', ['agendaTel']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda Tel id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $agendaTel = $this->AgendaTel->get($id);
                $agendaTel->situacao_id = 2;
        if ($this->AgendaTel->save($agendaTel)) {
            $this->Flash->success(__('O contato foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O contato não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Agenda Tel id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AgendaTel->find('all')
        ->where(['AgendaTel.name LIKE ' => '%' . $termo . '%'])
        ->orWhere(['AgendaTel.category LIKE ' => '%' . $termo . '%'])
        ->orWhere(['AgendaTel.observation LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AgendaTel.name');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->name);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Agenda Tel id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $agendaTel = $this->AgendaTel->get($this->request->data['id']);
            $res = ['nome'=>$agendaTel->nome,'id'=>$agendaTel->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
