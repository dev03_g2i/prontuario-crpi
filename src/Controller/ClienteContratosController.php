<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClienteContratos Controller
 *
 * @property \App\Model\Table\ClienteContratosTable $ClienteContratos
 */
class ClienteContratosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Atendimentos', 'Contratos', 'SituacaoCadastros', 'Users', 'ClienteResponsaveis']
        ];
        $clienteContratos = $this->paginate($this->ClienteContratos);
        $atendimento_id = null;
        if(!empty($this->request->query['atendimento_id'])){
            $atendimento_id = $this->request->query['atendimento_id'];
        }
        $this->set(compact('clienteContratos','atendimento_id'));
        $this->set('_serialize', ['clienteContratos']);

    }

    /**
     * View method
     *
     * @param string|null $id Cliente Contrato id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteContrato = $this->ClienteContratos->get($id, [
            'contain' => ['Atendimentos', 'Contratos', 'SituacaoCadastros', 'Users', 'ClienteResponsaveis']
        ]);

        $this->set('clienteContrato', $clienteContrato);
        $this->set('_serialize', ['clienteContrato']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $clienteContrato = $this->ClienteContratos->newEntity();
        if ($this->request->is('post')) {
            $clienteContrato = $this->ClienteContratos->patchEntity($clienteContrato, $this->request->data);
            if ($this->ClienteContratos->save($clienteContrato)) {
                $this->Flash->success(__('O cliente contrato foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente contrato não foi salvo. Por favor, tente novamente.'));
            }
        }
        $atendimento = null;
        $responsaveis = null;
        if(!empty($this->request->query['atendimento_id'])){
            $atendimento = $this->ClienteContratos->Atendimentos->get($this->request->query['atendimento_id'],['contain'=>['ContasReceber']]);
            $this->loadModel('ClienteResponsaveis');
            $responsaveis = $this->ClienteResponsaveis->find('list')
                ->where(['ClienteResponsaveis.cliente_id'=>$atendimento->cliente_id])
                ->orderDesc('ClienteResponsaveis.financeiro');
        }

        $contratos = $this->ClienteContratos->Contratos->find('list');
        $this->set(compact('clienteContrato','atendimento','responsaveis','contratos'));
        $this->set('_serialize', ['clienteContrato']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Contrato id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clienteContrato = $this->ClienteContratos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteContrato = $this->ClienteContratos->patchEntity($clienteContrato, $this->request->data);
            if ($this->ClienteContratos->save($clienteContrato)) {
                $this->Flash->success(__('O cliente contrato foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente contrato não foi salvo. Por favor, tente novamente.'));
            }
        }
        $contratos = $this->ClienteContratos->Contratos->find('list');
        $this->set(compact('clienteContrato','contratos'));
        $this->set('_serialize', ['clienteContrato']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Contrato id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteContrato = $this->ClienteContratos->get($id);
                $clienteContrato->situacao_id = 2;
        if ($this->ClienteContratos->save($clienteContrato)) {
            $this->Flash->success(__('O cliente contrato foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente contrato não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Cliente Contrato id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ClienteContratos->find('all')
        ->where(['ClienteContratos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ClienteContratos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Cliente Contrato id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $clienteContrato = $this->ClienteContratos->get($this->request->data['id']);
            $res = ['nome'=>$clienteContrato->nome,'id'=>$clienteContrato->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function imprimir(){

        $cliente_contrato = $this->ClienteContratos->get($this->request->query['id'],['contain'=>['Contratos','ClienteResponsaveis','Atendimentos'=>['Clientes']]]);

        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => $cliente_contrato->contrato->nome,
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');

        $this->set(compact('cliente_contrato'));
        $this->set('_serialize', ['cliente_contrato']);
    }
}
