<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Textos Controller
 *
 * @property \App\Model\Table\TextosTable $Textos
 */
class TextosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros', 'TextoGrupos']
        ];
        $textos = $this->paginate($this->Textos);


        $this->set(compact('textos'));
        $this->set('_serialize', ['textos']);

    }

    /**
     * View method
     *
     * @param string|null $id Texto id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $texto = $this->Textos->get($id, [
            'contain' => ['Users', 'SituacaoCadastros', 'TextoGrupos']
        ]);

        $this->set('texto', $texto);
        $this->set('_serialize', ['texto']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $texto = $this->Textos->newEntity();
        if ($this->request->is('post')) {
            $texto = $this->Textos->patchEntity($texto, $this->request->data);
            if ($this->Textos->save($texto)) {
                $this->Flash->success(__('O texto foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O texto não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('texto'));
        $this->set('_serialize', ['texto']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Texto id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $texto = $this->Textos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $texto = $this->Textos->patchEntity($texto, $this->request->data);
            if ($this->Textos->save($texto)) {
                $this->Flash->success(__('O texto foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O texto não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('texto'));
        $this->set('_serialize', ['texto']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Texto id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
        $texto = $this->Textos->get($id);
                $texto->situacao_id = 2;
        if ($this->Textos->save($texto)) {
            $this->Flash->success(__('O texto foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O texto não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Texto id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Textos->find('all')
        ->where(['Textos.codigo LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Textos.codigo');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->codigo);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    public function loadSelect2()
    {
        $dados = $this->Textos->find('list')->where(['Textos.situacao_id' => 1])->orderAsc('Textos.codigo');
        $json = json_encode($dados);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Texto id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $texto = $this->Textos->get($this->request->data['id']);
            $res = ['nome'=>$texto->nome,'id'=>$texto->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function getModelo()
    {
        $this->autoRender=false;
        $this->response->type('json');

        if($this->request->data('search') == 'all'){
            $modelo = $this->Textos->find()
                ->select(['id', 'codigo', 'texto', 'texto_html', 'rtf'])
                ->where(['Textos.situacao_id' => 1]);
                if(!empty($this->request->data('id'))){
                    $modelo->andWhere(['Textos.grupo_id' => $this->request->data('id')]);
                }
        }else if($this->request->data('search') == 'get'){
            $modelo = $this->Textos->get($this->request->data('id'));
        }

        $this->response->body(json_encode($modelo));
    }
}
