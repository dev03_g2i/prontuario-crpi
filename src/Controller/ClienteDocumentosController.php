<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * ClienteDocumentos Controller
 *
 * @property \App\Model\Table\ClienteDocumentosTable $ClienteDocumentos
 */
class ClienteDocumentosController extends AppController
{

    public $components = ['RequestHandler', 'Json'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ClienteDocumentos->find('all')
            ->contain(['Clientes', 'Users', 'SituacaoCadastros', 'TipoDocumentos', 'MedicoResponsaveis', 'Modelos'])
            ->where(['ClienteDocumentos.situacao_id = ' => '1']);

        if (!empty($this->request->query['cliente_id'])) {
            $query->andWhere(['ClienteDocumentos.cliente_id = ' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        } else {
            $cliente_id = null;
        }

        $query->orderDesc('ClienteDocumentos.created');

        $clienteDocumentos = $this->paginate($query);

        $this->set(compact('clienteDocumentos'));
        $this->set('_serialize', ['clienteDocumentos']);
    }

    public function all()
    {
        $this->loadModel('ConfiguracaoProntuarioDocs');
        $config_doc = $this->ConfiguracaoProntuarioDocs->find()->first();

        $query = $this->ClienteDocumentos->find('all')
            ->contain(['Clientes', 'TipoDocumentos', 'MedicoResponsaveis', 'Modelos'])
            ->where(['ClienteDocumentos.situacao_id = ' => '1']);

        if (!empty($this->request->query['cliente_id'])) {
            $query->andWhere(['ClienteDocumentos.cliente_id = ' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        } else {
            $cliente_id = null;
        }

        $query->orderDesc('ClienteDocumentos.created');

        $clienteDocumentos = $this->paginate($query);

        $this->set(compact('clienteDocumentos', 'cliente_id', 'config_doc'));
        $this->set('_serialize', ['clienteDocumentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteDocumento = $this->ClienteDocumentos->get($id, [
            'contain' => ['Clientes', 'Users', 'SituacaoCadastros', 'TipoDocumentos']
        ]);

        $this->set('clienteDocumento', $clienteDocumento);
        $this->set('_serialize', ['clienteDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($cliente)
    {
        $clienteDocumento = $this->ClienteDocumentos->newEntity();
        if ($this->request->is('post')) {
            $clienteDocumento = $this->ClienteDocumentos->patchEntity($clienteDocumento, $this->request->data);
            $this->autoRender = false;
            $this->response->type('json');
            if ($this->ClienteDocumentos->save($clienteDocumento)) {
                $json = json_encode(['result' => ['cliente' => $cliente, 'id' => $clienteDocumento->id]]);
            } else {
                $json = json_encode('-1');
            }
            $this->response->body($json);

        }
        $clientes = $this->ClienteDocumentos->Clientes->find('list', ['limit' => 200]);
        $tipoDocumentos = $this->ClienteDocumentos->TipoDocumentos->find('list', ['limit' => 200]);
        $medicos = $this->ClienteDocumentos->MedicoResponsaveis->find('list', ['limit' => 200]);
        $this->set(compact('clienteDocumento', 'clientes', 'cliente', 'tipoDocumentos', 'medicos'));
        $this->set('_serialize', ['clienteDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $cliente)
    {
        $clienteDocumento = $this->ClienteDocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteDocumento = $this->ClienteDocumentos->patchEntity($clienteDocumento, $this->request->data);
            $this->autoRender = false;
            $this->response->type('json');
            if ($this->ClienteDocumentos->save($clienteDocumento)) {
                $json = json_encode(['result' => ['cliente' => $cliente, 'id' => $clienteDocumento->id]]);
            } else {
                $json = json_encode('-1');
            }
            $this->response->body($json);

        }
        $clientes = $this->ClienteDocumentos->Clientes->find('list', ['limit' => 200]);
        $tipoDocumentos = $this->ClienteDocumentos->TipoDocumentos->find('list', ['limit' => 200]);
        $medicos = $this->ClienteDocumentos->MedicoResponsaveis->find('list', ['limit' => 200]);
        $modelos = $this->ClienteDocumentos->Modelos->find('list', ['limit' => 200])
            ->where(['Modelos.situacao_id' => 1, 'Modelos.tipo_id' => $clienteDocumento->tipo_id]);

        $this->set(compact('clienteDocumento', 'clientes', 'cliente', 'tipoDocumentos', 'medicos', 'modelos'));
        $this->set('_serialize', ['clienteDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
        $clienteDocumento = $this->ClienteDocumentos->get($id);
        $clienteDocumento->situacao_id = 2;
        if ($this->ClienteDocumentos->save($clienteDocumento)) {
            $this->Flash->success(__('O cliente documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente documento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method in Modal
     *
     * @return void
     */
    public function deleteModal()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $clienteDocumentos = $this->ClienteDocumentos->get($this->request->data['id']);
        $clienteDocumentos->situacao_id = 2;
        if ($this->ClienteDocumentos->save($clienteDocumentos)) {
            $res = 0;
        } else {
            $res = 1;
        }

        $json = json_encode(['res' => $res, 'view' => 'index']);
        $this->response->body($json);
    }

    public function prontDoc01()
    {
        $this->loadModel('ConfiguracaoProntuarioDocs');
        $config = $this->ConfiguracaoProntuarioDocs->find()->first();

        $documentos = $this->ClienteDocumentos->get($this->request->query['id'], [
            'contain' => ['MedicoResponsaveis', 'Clientes', 'TipoDocumentos']]);
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');
        $this->set(compact('documentos'));
        $this->set('_serialize', ['documentos']);
    }

    public function prontDoc02()
    {
        $this->loadModel('ConfiguracaoProntuarioDocs');
        $config = $this->ConfiguracaoProntuarioDocs->find()->first();

        $documentos = $this->ClienteDocumentos->get($this->request->query['id'], [
            'contain' => ['MedicoResponsaveis', 'Clientes', 'TipoDocumentos']]);
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');
        $this->set(compact('documentos'));
        $this->set('_serialize', ['documentos']);
    }

    public function prontDoc03()
    {
        $this->loadModel('ConfiguracaoProntuarioDocs');
        $config = $this->ConfiguracaoProntuarioDocs->find()->first();

        $documentos = $this->ClienteDocumentos->get($this->request->query['id'], [
            'contain' => ['MedicoResponsaveis', 'Clientes', 'TipoDocumentos']]);
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');
        $this->set(compact('documentos'));
        $this->set('_serialize', ['documentos']);
    }

    public function prontDoc04()
    {
        $this->loadModel('ConfiguracaoProntuarioDocs');
        $config = $this->ConfiguracaoProntuarioDocs->find()->first();

        $documentos = $this->ClienteDocumentos->get($this->request->query['id'], [
            'contain' => ['MedicoResponsaveis', 'Clientes', 'TipoDocumentos']]);
        $this->viewBuilder()
            ->className('Dompdf.Pdf')
            ->layout('Dompdf.default')
            ->options(['config' => [
                'filename' => 'teste',
                'render' => 'browser',
            ]])
            ->className('Dompdf.Pdf');
        $this->set(compact('documentos'));
        $this->set('_serialize', ['documentos']);
    }

    public function report()
    {
        $documentos = $this->ClienteDocumentos->get($this->request->query['id'], [
            'contain' => ['MedicoResponsaveis', 'Clientes', 'TipoDocumentos']]);
        $this->viewBuilder()->layout('report_2016');
        $json['dados'][] = [
            'cliente' => $documentos->cliente->nome,
            'corpo' => $documentos->descricao,
            'medico' => $documentos->medico_responsavei->nome_laudo,
            'crm' => $documentos->medico_responsavei->conselho_laudo
        ];
        $this->Json->create('receita.json', json_encode($json));
        $this->set('report', 'receita.mrt');

    }

}
