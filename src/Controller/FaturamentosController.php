<?php

namespace App\Controller;

use App\Controller\AppController;

use Cake\Datasource\ConnectionManager;

/**
 * Faturamentos Controller
 *
 * @property \App\Model\Table\FaturamentosTable $Faturamentos
 */

use Cake\I18n\Time;

class FaturamentosController extends AppController
{

    public $components = ['Data', 'Query', 'Json', 'Faturamento', 'Relatorios'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['AtendimentoProcedimentos', 'Users', 'Users_up', 'SituacaoCadastros']
        ];
        $faturamentos = $this->paginate($this->Faturamentos);

        $this->set(compact('faturamentos'));
        $this->set('_serialize', ['faturamentos']);

    }

    public function home()
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempFaturamentos');

        $fill = false;
        if (empty($this->request->query('data_inicio')))
            $fill = true;

        $query = $this->Faturamento->atendimentoProcedimentos(
            empty($this->request->query('situacao_fatura_id')) ? null : $this->request->query('situacao_fatura_id'),
            empty($this->request->query('data_inicio')) ? null : $this->request->query('data_inicio'),
            empty($this->request->query('data_fim')) ? null : $this->request->query('data_fim'),
            empty($this->request->query('unidade_id')) ? null : $this->request->query('unidade_id'),
            empty($this->request->query('origen_id')) ? null : $this->request->query('origen_id'),
            empty($this->request->query('convenios')) ? null : $this->request->query('convenios'),
            empty($this->request->query('grupos')) ? null : $this->request->query('grupos'),
            empty($this->request->query('procedimentos')) ? null : $this->request->query('procedimentos'),
            empty($this->request->query('medicos')) ? null : $this->request->query('medicos'),
            empty($this->request->query('controle')) ? null : $this->request->query('controle'),
            empty($this->request->query('ordenar_por')) ? null : $this->request->query('ordenar_por')
        );

        $this->paginate = [
            'limit' => 50
        ];

        $atendimentoProcedimentos = $this->paginate($query);

        if ($fill) {
            $atendimentoProcedimentos = null;
        }

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->where(['SituacaoFaturas.situacao_id' => 1])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorios = $this->Relatorios->getTiposRelatoriosFaturamento();
        $versoesXml = $this->Faturamento->getVersoesXML();

        $this->set(compact('unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'atendimentoProcedimentos', 'situacao_faturas', 'procedimentos', 'tiposRelatorios', 'versoesXml'));
    }

    public function equipes()
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TipoEquipes');

        $fill = false;
        if (empty($this->request->query('data_inicio')))
            $fill = true;

        $query = $this->Faturamento->filterAtendProcToEquipes($this->request->getQuery());

        $this->paginate = [
            'limit' => 50
        ];

        $atendimentoProcedimentos = $this->paginate($query);

        if ($fill) {
            $atendimentoProcedimentos = null;
        }

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->where(['SituacaoFaturas.situacao_id' => 1])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorios = $this->Relatorios->getTiposRelatoriosFaturamentoEquipes ();
        $versoesXml = $this->Faturamento->getVersoesXML();
        $tipo_equipes = $this->TipoEquipes->getList();

        $this->set(compact('unidades', 'origens', 'convenios', 'tipo_equipes', 'grupo_procedimentos', 'medicos', 'atendimentoProcedimentos', 'situacao_faturas', 'procedimentos', 'tiposRelatorios', 'versoesXml'));
    }

    public function listar()
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempFaturamentos');

        $query = $this->TempFaturamentos->find('all');

        if (!empty($this->request->getQuery('ordenar_por'))) {
            $query->orderAsc($this->request->getQuery('ordenar_por'));
        }

        $tempFaturamento = false;
        if (!empty($this->request->query('convenio'))) {
            $tempFaturamento = true;
        }

        $this->paginate = [
            'limit' => 50
        ];
        $atendimentoProcedimentos = $this->paginate($query);

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Procedimentos->find('list', ['limit' => 200])->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->where(['SituacaoFaturas.situacao_id' => 1])->orderAsc('SituacaoFaturas.nome');
        $this->set(compact('unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'atendimentoProcedimentos', 'situacao_faturas', 'tempFaturamento', 'procedimentos'));

    }

    public function salvarTempFaturamento()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $conn = ConnectionManager::get('default');
        $this->Query->clearTemp($conn, 'temp_faturamentos');

        $and = '';
        if (!empty($this->request->query('situacao_fatura_id'))) {
            $and .= "AND ap.situacao_fatura_id = " . $this->request->query('situacao_fatura_id');
        }

        if (!empty($this->request->query('data_inicio'))) {
            $data_inicio = $this->Data->DataSQL($this->request->query['data_inicio']);
            $and .= " AND a.data >= '" . $data_inicio . "'";
        }
        if (!empty($this->request->query('data_fim'))) {
            $data_fim = $this->Data->DataSQL($this->request->query['data_fim']);
            $and .= " AND a.data <= '" . $data_fim . "'";
        }

        if (!empty($this->request->query('unidade_id'))) {
            $and .= " AND a.unidade_id = " . $this->request->query('unidade_id');
        }

        if (!empty($this->request->query('origen_id'))) {
            $and .= " AND a.origen_id = " . $this->request->query('origen_id');
        }

        if (!empty($this->request->query('convenio'))) {
            $and .= " AND a.convenio_id = " . $this->request->query('convenio');
        }

        if (!empty($this->request->query('grupos'))) {
            $ids = join(',', $this->request->query('grupos'));
            $and .= " AND p.grupo_id IN ($ids)";
        }

        if (!empty($this->request->query('procedimentos'))) {
            $ids = join(',', $this->request->query('procedimentos'));
            $and .= " AND ap.procedimento_id IN ($ids)";
        }

        if (!empty($this->request->query('medicos'))) {
            $ids = join(',', $this->request->query('medicos'));
            $and .= " AND ap.medico_id IN ($ids)";
        }
        /*if (!empty($this->request->query('status_faturamento'))) {
            $and .= " AND ap.status_faturamento = " . $this->request->query('status_faturamento');
        }*/

        $result = ($this->Query->insertTempFaturamento($conn, $and)) ? 'success' : 'error';

        $this->loadModel('TempFaturamentos');
        $query = $this->TempFaturamentos->find('all');
        $query_select = $query->select([
            'total' => $query->func()->sum('total'),
            'count' => $query->func()->sum('quantidade'),
            'valor_material' => $query->func()->sum('valor_material'),
            'valor_caixa' => $query->func()->sum('valor_caixa')
        ])->toArray();

        $total = ($query_select[0]->total != null) ? $query_select[0]->total : '0.00';
        $count = ($query_select[0]->count != null) ? $query_select[0]->count : 0;
        $valor_material = ($query_select[0]->valor_material != null) ? $query_select[0]->valor_material : '0.00';
        $valor_caixa = ($query_select[0]->valor_caixa != null) ? $query_select[0]->valor_caixa : '0.00';

        $this->response->body(json_encode(['result' => $result, 'count' => $count, 'total' => $total, 'valor_material' => $valor_material, 'valor_caixa' => $valor_caixa]));
    }

    public function countReclistar()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('AtendimentoProcedimentos');

        $query = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos', 'Procedimentos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id' => $this->request->data('faturaEncerramentoId')]);

        if (!empty($this->request->data('situacao_datas'))) {
            $situacao_data = $this->request->data('situacao_datas');
            $collumn = ($situacao_data == 'data') ? 'Atendimentos.' . $situacao_data : 'AtendimentoProcedimentos.' . $situacao_data;

            if (!empty($this->request->data('data_inicio'))) {
                $data_inicio = $this->Data->DataSQL($this->request->data['data_inicio']);
                $query->andWhere([$collumn . ' >= ' => $data_inicio]);
            }
            if (!empty($this->request->data('data_fim'))) {
                $data_fim = $this->Data->DataSQL($this->request->data['data_fim']);
                $query->andWhere([$collumn . ' <= ' => $data_fim]);
            }

        }

        if (!empty($this->request->data('unidade_id'))) {
            $query->andWhere(['Atendimentos.unidade_id' => $this->request->data('unidade_id')]);
        }

        if (!empty($this->request->data('origen_id'))) {
            $query->andWhere(['Atendimentos.origen_id' => $this->request->data('origen_id')]);
        }

        /*if (!empty($this->request->query('convenios'))) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $this->request->query('convenios')]);
        }*/

        if (!empty($this->request->data('grupos'))) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $this->request->data('grupos')]);
        }

        if (!empty($this->request->data('medicos'))) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $this->request->data('medicos')]);
        }

        if (!empty($this->request->data('controle'))) {
            $controle = '%' . $this->request->data('controle') . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }

        if (!empty($this->request->data('procedimentos'))) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $this->request->data('procedimentos')]);
        }

        /*if(!empty($faturaEncerramentoId) && !$fill){
            $query->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id' => $faturaEncerramentoId]);
        }*/

        $query_select = $query->select([
            'total' => $query->func()->sum('total'),
            'count' => $query->func()->sum('quantidade'),
            'valor_recebido' => $query->func()->sum('valor_recebido'),
            'valor_rec_matmed' => $query->func()->sum('valor_rec_matmed'),
            'total_caixa' => $query->func()->sum('valor_caixa')
        ])->toArray();

        $total = ($query_select[0]->total != null) ? $query_select[0]->total : '0.00';
        $count = ($query_select[0]->count != null) ? $query_select[0]->count : 0;
        $valor_recebido = ($query_select[0]->valor_recebido != null) ? $query_select[0]->valor_recebido : 0;
        $valor_rec_matmed = ($query_select[0]->valor_rec_matmed != null) ? $query_select[0]->valor_rec_matmed : 0;
        $diferanca = $total - $valor_recebido;
        $total_caixa = ($query_select[0]->total_caixa != null) ? $query_select[0]->total_caixa : 0;

        $this->response->body(json_encode([
            'total' => $total,
            'valor_recebido' => $valor_recebido,
            'valor_rec_matmed' => $valor_rec_matmed,
            'count' => $count,
            'diferenca' => $diferanca,
            'total_caixa' => $total_caixa
        ]));
    }


    /**
     *
     * All method
     *
     * @return \Cake\Network\Response|null
     */

    public function all($id = null)
    {
        $faturaEncerramentos = null;

        $this->loadModel('FaturaEncerramentos');
        $query = $this->FaturaEncerramentos->find('all')
            ->contain(['Convenios', 'AtendimentoProcedimentos', 'FaturaNfs'])
            ->where(['FaturaEncerramentos.situacao_id' => 1])
            ->orderDesc('FaturaEncerramentos.id');

        if (!empty($id)) {
            $query->andWhere(['FaturaEncerramentos.id' => $id]);
        }

        if (!empty($this->request->query('convenio_id'))) {
            $query->andWhere(['FaturaEncerramentos.convenio_id' => $this->request->query('convenio_id')]);
        }

        if (!empty($this->request->query('situacao_faturaencerramento_id'))) {
            $query->andWhere(['FaturaEncerramentos.situacao_faturaencerramento_id' => $this->request->query('situacao_faturaencerramento_id')]);
        }

        if (!empty($this->request->query('have_nfs'))) {
            $have_nfs = $this->request->query['have_nfs'];
            $ids = [];
            foreach ($query as $q) {
                if ($q->has('fatura_nfs') && !empty($q->fatura_nfs)) {
                    $ids[] = $q->id;
                }
            }
            if ($have_nfs == 2) {
                $query->andWhere(['FaturaEncerramentos.id IN' => $ids]);
            } else if ($have_nfs == 1) {
                $query->andWhere(['FaturaEncerramentos.id NOT IN' => $ids]);
            }
        }

        if (!empty($this->request->query('search-billing'))) {
            $search = $this->request->query('search-billing');

            $periodo_busca_inicio = $this->request->query['periodo_busca_inicio'];
            $periodo_busca_fim = $this->request->query['periodo_busca_fim'];

            if (!empty($periodo_busca_inicio) && !empty($periodo_busca_fim)) {

                $periodo_busca_inicio = $this->Data->DataSQL($periodo_busca_inicio) . " 23:59:59";
                $periodo_busca_fim = $this->Data->DataSQL($periodo_busca_fim) . " 23:59:59";

                if ($search == '2') {
                    $query->andWhere(['FaturaEncerramentos.data_encerramento >=' => $periodo_busca_inicio]);
                    $query->andWhere(['FaturaEncerramentos.data_encerramento <=' => $periodo_busca_fim]);
                } else if ($search == '1') {
                    $query->andWhere(['FaturaEncerramentos.vencimento >=' => $periodo_busca_inicio]);
                    $query->andWhere(['FaturaEncerramentos.vencimento <=' => $periodo_busca_fim]);
                }
            }
        }

        $faturaEncerramentos = $this->paginate($query);

        $totalBrutoAll = 0;
        $totalPrevistoAll = 0;
        $totalNfs = 0;
        $totalRecebidoAll = 0;

        $countFaturas = count($faturaEncerramentos);

        foreach ($faturaEncerramentos as $f) {
            $totalTax = 0;
            $total_previsto = 0;
            $totalLiquidNfs = 0;

            $totalBrutoAll += $f->total_bruto;

            //Calculo total previsto
            $this->loadModel('ConvenioImpostosParametro');
            $convenioImpostosParametro = $this->ConvenioImpostosParametro->find()->where(['id_convenio' => $f->convenio->id]);

            foreach ($convenioImpostosParametro as $cP) {
                $totalTax += $cP->porcentual;
            }
            $total_previsto = $f->total_bruto - ($f->total_bruto * ($totalTax / 100));
            $totalPrevistoAll += $total_previsto;

            //Calculo total liquido fatura Nfs
            $this->loadModel('FaturaNfs');
            $faturaNfs = $this->FaturaNfs->find()->where(['fatura_encerramento_id' => $f->id]);

            foreach ($faturaNfs as $q) {
                $totalLiquidNfs += $q->valor_liquido;
            }
            $totalNfs += $totalLiquidNfs;

            //Calculo Total liquido
            $this->loadModel('AtendimentoProcedimentos');
            $query = $this->AtendimentoProcedimentos->find()->select('valor_recebido')
                ->where(['AtendimentoProcedimentos.fatura_encerramento_id' => $f->id])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);

            $query_select = $query->select(['valor_recebido' => $query->func()->sum('valor_recebido')])->toArray();
            $total_recebido = $query_select[0]->valor_recebido ? $query_select[0]->valor_recebido : '0.00';
            $totalRecebidoAll += $total_recebido;

        }

        $convenios = $this->FaturaEncerramentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');

        $this->set(compact('countFaturas', 'faturaEncerramentos', 'convenios', 'totalBrutoAll', 'totalPrevistoAll', 'totalRecebidoAll', 'totalNfs'));
        $this->set('_serialize', ['faturaEncerramentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Faturamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturamento = $this->Faturamentos->get($id, [
            'contain' => ['AtendimentoProcedimentos', 'Users', 'Users_up', 'SituacaoCadastros']
        ]);

        $this->set('faturamento', $faturamento);
        $this->set('_serialize', ['faturamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faturamento = $this->Faturamentos->newEntity();
        if ($this->request->is('post')) {
            $faturamento = $this->Faturamentos->patchEntity($faturamento, $this->request->data);
            if ($this->Faturamentos->save($faturamento)) {
                $this->Flash->success(__('O faturamento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O faturamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $atend_proc_id = null;
        if (!empty($this->request->query['atendimento_procedimento_id'])) {
            $atend_proc_id = $this->request->query['atendimento_procedimento_id'];
        }
        $this->set(compact('faturamento', 'atend_proc_id'));
        $this->set('_serialize', ['faturamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Faturamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturamento = $this->Faturamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturamento = $this->Faturamentos->patchEntity($faturamento, $this->request->data);
            if ($this->Faturamentos->save($faturamento)) {
                $this->Flash->success(__('O faturamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O faturamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturamento'));
        $this->set('_serialize', ['faturamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Faturamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deleteModal()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $this->loadModel('FaturaEncerramentos');
        $this->loadModel('AtendimentoProcedimentos');
        $faturaEncerramento = $this->FaturaEncerramentos->get($this->request->data['id']);
        $faturaEncerramento->situacao_id = 2;

        if ($this->FaturaEncerramentos->save($faturaEncerramento)) {
            $atendProc = $this->AtendimentoProcedimentos->query()
                ->update()
                ->set(['fatura_encerramento_id' => null, 'status_faturamento' => 0, 'situacao_fatura_id' => 1])
                ->where(['AtendimentoProcedimentos.fatura_encerramento_id' => $this->request->data['id']])
                ->execute();
            if ($atendProc) {
                $res = 0;
            } else {
                $res = 1;
            }
        }
        $this->response->body(json_encode(['res' => $res]));
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Faturamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->Faturamentos->find('all')
            ->where(['Faturamentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Faturamentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {

            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Faturamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $faturamento = $this->Faturamentos->get($this->request->data['id']);
            $res = ['nome' => $faturamento->nome, 'id' => $faturamento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function salvarSelecionado()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('AtendimentoProcedimentos');
        $error = 0;

        if (is_array($this->request->data('id'))) {
            $tempFaturamento = $this->AtendimentoProcedimentos->query()->update()
                ->set(['situacao_fatura_id' => $this->request->data('situacao')])
                ->where(['AtendimentoProcedimentos.id IN' => $this->request->data('id')])
                ->execute();
            if ($tempFaturamento)
                $error = 0;
            else
                $error += 1;

        } else {
            $tempFaturamento = $this->AtendimentoProcedimentos->get($this->request->data('id'));
            if ($tempFaturamento->situacao_fatura_id == 1) {
                $tempFaturamento->situacao_fatura_id = 2;
            } else {
                $tempFaturamento->situacao_fatura_id = 1;
            }
            if ($this->AtendimentoProcedimentos->save($tempFaturamento)) {
                $error = 0;
            } else {
                $error += 1;
            }
        }

        $json = json_encode($error);
        $this->response->body($json);
    }

    public function encerramento()
    {
        $user_id = $this->Auth->user('id');
        $conn = ConnectionManager::get('default');
        $mes = $this->request->getQuery('mes_referencia');
        $ano = $this->request->getQuery('ano_referencia');
        $mesAno = $mes . '-' . $ano;
        if (!empty($this->request->getQuery('convenio_id'))) {
            $this->loadModel('Convenios');
            $convenio = $this->Convenios->get($this->request->getQuery('convenio_id'));
            $diasParaVencimento = $convenio->pymt_date == null ? 0 : $convenio->pymt_date;
            $dataVencimento = date('Y-m-d', strtotime(date('Y-m-d') . ' + ' . $diasParaVencimento . ' days'));
        } else {
            $dataVencimento = date('Y-m-d');
        }

        $fatura_encerramento = $this->Query->insertFaturaEncerramento($conn, $user_id, $mes, $ano, $mesAno, $dataVencimento);
        $conn->driver()->disconnect();
        $conn->driver()->connect();
        if ($fatura_encerramento) {
            $this->loadModel('FaturaEncerramentos');
            $faturaEncerramento = $this->FaturaEncerramentos->find('all')
                ->contain(['Convenios'])
                ->last();

            if (!empty($faturaEncerramento)) {
                $this->loadModel('ContasReceber');
                $conta = $this->ContasReceber->newEntity();
                $conta->data = date('d/m/Y');
                $conta->valor = $faturaEncerramento->total_bruto;
                $conta->idPlanoContas = !empty($faturaEncerramento->convenio->planAcc_id) ? $faturaEncerramento->convenio->planAcc_id : -1;
                $conta->idCliente = -100;
                $conta->contabilidade_id = $faturaEncerramento->unidade_id;
                $conta->tipoPagamento = -10;
                $conta->tipoDocumento = -10;
                $conta->controle_financeiro_id = 1;
                $conta->origem_registro = 2;
                $conta->fatura_id = $faturaEncerramento->id;
                $conta->numerodocumento = 'fat-' . $faturaEncerramento->id;
                $this->ContasReceber->save($conta);
            }
            $this->Flash->success('Faturamento Finalizado com sucesso!');
            $this->redirect(['controller' => 'FaturaEncerramentos', 'action' => 'index']);
        } else {
            $this->Flash->error('Erro ao tentar finalizar faturamento!');
            $this->redirect(['action' => 'listar']);
        }
    }

    public function salvarSelecionadoTemp()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('TempFaturamentos');
        $error = 0;

        if (is_array($this->request->data('id'))) {
            $tempFaturamento = $this->TempFaturamentos->query()->update()
                ->set(['selecionado' => $this->request->data('situacao')])
                ->where(['1=1'])
                ->execute();
            if ($tempFaturamento)
                $error = 0;
            else
                $error += 1;

        } else {
            $tempFaturamento = $this->TempFaturamentos->get($this->request->data('id'));
            if ($tempFaturamento->selecionado == 0) {
                $tempFaturamento->selecionado = 1;
            } else {
                $tempFaturamento->selecionado = 0;
            }
            if ($this->TempFaturamentos->save($tempFaturamento)) {
                $error = 0;
            } else {
                $error += 1;
            }
        }

        $json = json_encode($error);
        $this->response->body($json);
    }

    public function countAtendimentoProcedimentos()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('AtendimentoProcedimentos');

        $query = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos', 'Procedimentos'])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1]);

        if (!empty($this->request->query('situacao_fatura_id'))) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_fatura_id' => $this->request->query('situacao_fatura_id')]);
        }

        if (!empty($this->request->query('data_inicio'))) {
            $data_inicio = $this->Data->DataSQL($this->request->query['data_inicio']);
            $query->andWhere(['Atendimentos.data >= ' => $data_inicio]);
        }
        if (!empty($this->request->query('data_fim'))) {
            $data_fim = $this->Data->DataSQL($this->request->query['data_fim']);
            $query->andWhere(['Atendimentos.data <= ' => $data_fim]);
        }

        if (!empty($this->request->query('unidade_id'))) {
            $query->andWhere(['Atendimentos.unidade_id' => $this->request->query('unidade_id')]);
        }

        if (!empty($this->request->query('origen_id'))) {
            $query->andWhere(['Atendimentos.origen_id' => $this->request->query('origen_id')]);
        }

        if (!empty($this->request->query('convenios'))) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $this->request->query('convenios')]);
        }

        if (!empty($this->request->query('grupos'))) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $this->request->query('grupos')]);
        }

        if (!empty($this->request->query('procedimentos'))) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $this->request->query('procedimentos')]);
        }

        if (!empty($this->request->query('medicos'))) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $this->request->query('medicos')]);
        }

        if (!empty($this->request->query('controle'))) {
            $controle = '%' . $this->request->query('controle') . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }

        $total_equipe = 0;
        foreach($query as $q){
            $total_equipe += $q->total_equipe;
        }

        $query_select = $query->select([
            'total' => $query->func()->sum('total'),
            'count' => $query->func()->sum('quantidade'),
            'valor_caixa' => $query->func()->sum('valor_caixa'),
            'valor_material' => $query->func()->sum('valor_material'),
        ])->toArray();

        $json = [
            'total' => ($query_select[0]->total != null) ? $query_select[0]->total : '0.00',
            'count' => ($query_select[0]->count != null) ? $query_select[0]->count : '0',
            'valor_caixa' => ($query_select[0]->valor_caixa != null) ? $query_select[0]->valor_caixa : '0.00',
            'valor_material' => ($query_select[0]->valor_material != null) ? $query_select[0]->valor_material : '0.00',
            'total_equipe' => $total_equipe
        ];

        $this->response->body(json_encode($json));
    }

    public function countTempFaturamento()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempFaturamentos');

        $query = $this->AtendimentoProcedimentos->find('all')
            ->contain(['Atendimentos', 'Procedimentos'])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1]);

        if (!empty($this->request->query('situacao_fatura_id'))) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_fatura_id' => $this->request->query('situacao_fatura_id')]);
        }

        if (!empty($this->request->query('data_inicio'))) {
            $data_inicio = $this->Data->DataSQL($this->request->query['data_inicio']);
            $query->andWhere(['Atendimentos.data >= ' => $data_inicio]);
        }
        if (!empty($this->request->query('data_fim'))) {
            $data_fim = $this->Data->DataSQL($this->request->query['data_fim']);
            $query->andWhere(['Atendimentos.data <= ' => $data_fim]);
        }

        if (!empty($this->request->query('unidade_id'))) {
            $query->andWhere(['Atendimentos.unidade_id' => $this->request->query('unidade_id')]);
        }

        if (!empty($this->request->query('origen_id'))) {
            $query->andWhere(['Atendimentos.origen_id' => $this->request->query('origen_id')]);
        }

        if (!empty($this->request->query('convenio'))) {
            $query->andWhere(['Atendimentos.convenio_id' => $this->request->query('convenio')]);
        }

        /*if(!empty($this->request->query('grupos'))){
            $query->andWhere(['Procedimentos.grupo_id IN ' => $this->request->query('grupos')]);
        }*/
        if (!empty($this->request->query('procedimentos'))) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $this->request->query('procedimentos')]);
        }

        if (!empty($this->request->query('medicos'))) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $this->request->query('medicos')]);
        }

        if (!empty($this->request->query('controle'))) {
            $controle = '%' . $this->request->query('controle') . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }

        $query_select = $query->select([
            'total' => $query->func()->sum('total'),
            'count' => $query->func()->sum('quantidade')
        ])->toArray();

        $total = $query_select[0]->total;
        $count = $query_select[0]->count;

        $this->response->body(json_encode(['total' => $total, 'count' => $count]));
    }

    public function sumTotalValoresFaturamentosEncerrados($number)
    {
        echo $number;
        exit;
        $this->response->body(json_encode([]));
    }

    //Recebimentos
    public function recListar($faturaEncerramentoId = null)
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('Procedimentos');
        $this->loadModel('AtendimentoProcedimentos');
        $this->loadModel('TempFaturamentos');

        $query = $this->AtendimentoProcedimentos->find('all', [
            'contain' => [
                'Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos', 'MedicoResponsaveis', 'SituacaoRecebimentos'
            ]
        ])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id' => $faturaEncerramentoId])
            ->order(['Clientes.nome' => 'ASC']);

        /*if (!empty($this->request->query('situacao_recebimento_id'))) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_recebimento_id' => $this->request->query('situacao_recebimento_id')]);
        }*/

        if (!empty($this->request->query('situacao_datas'))) {
            $situacao_data = $this->request->query('situacao_datas');
            $collumn = ($situacao_data == 'data') ? 'Atendimentos.' . $situacao_data : 'AtendimentoProcedimentos.' . $situacao_data;

            if (!empty($this->request->query('data_inicio'))) {
                $data_inicio = $this->Data->DataSQL($this->request->query['data_inicio']);
                $query->andWhere([$collumn . ' >= ' => $data_inicio]);
            }
            if (!empty($this->request->query('data_fim'))) {
                $data_fim = $this->Data->DataSQL($this->request->query['data_fim']);
                $query->andWhere([$collumn . ' <= ' => $data_fim]);
            }

        }

        $fill = false;
        if (!empty($this->request->query('unidade_id'))) {
            $query->andWhere(['Atendimentos.unidade_id' => $this->request->query('unidade_id')]);
            $fill = true;
        }

        if (!empty($this->request->query('origen_id'))) {
            $query->andWhere(['Atendimentos.origen_id' => $this->request->query('origen_id')]);
        }

        /*if (!empty($this->request->query('convenios'))) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $this->request->query('convenios')]);
        }*/

        if (!empty($this->request->query('grupos'))) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $this->request->query('grupos')]);
        }

        if (!empty($this->request->query('medicos'))) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $this->request->query('medicos')]);
        }

        if (!empty($this->request->query('controle'))) {
            $controle = '%' . $this->request->query('controle') . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }

        if (!empty($this->request->query('procedimentos'))) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $this->request->query('procedimentos')]);
        }

        /*if(!empty($faturaEncerramentoId) && !$fill){
            $query->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id' => $faturaEncerramentoId]);
        }*/

        /*if(!$fill){
            $query->order(['Atendimentos.data' => 'ASC', 'Clientes.nome' => 'ASC']);
        }*/

        $this->paginate = [
            'limit' => 50,
            'sortWhitelist' => ['atendimento_id', 'Atendimentos.data', 'Atendimentos.cliente_id', 'MedicoResponsaveis.id', 'codigo', 'Procedimentos.id', 'nr_guia', 'autorizacao_senha', 'quantidade', 'dt_recebimento', 'valor_recebido', 'valor_rec_matmed', 'total']
        ];

        $atendimentoProcedimentos = $this->paginate($query);

        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');
        $procedimentos = $this->Procedimentos->find('list', ['limit' => 200])->orderAsc('Procedimentos.nome');
        $grupo_procedimentos = $this->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_recebimentos = $this->AtendimentoProcedimentos->SituacaoRecebimentos->find('list', ['limit' => 200])->orderAsc('SituacaoRecebimentos.nome');
        $this->set(compact('unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'atendimentoProcedimentos', 'situacao_recebimentos', 'tempFaturamento', 'procedimentos', 'faturaEncerramentoId'));
    }

    // Recebimentos
    public function recReceberTodos($faturaid)
    {
        $this->loadModel('SituacaoRecebimentos');

        $column_valor = null;
        if (!empty($this->request->query('column_valor'))) {
            $column_valor = $this->request->query('column_valor');
        }

        if ($this->request->is('post')) {
            $conn = ConnectionManager::get('default');
            $data_recebimento = $this->Data->DataSQL($this->request->data('dt_recebimento'));
            $result = $this->Query->recReceberTodos($faturaid, $data_recebimento, $this->request->data('situacao_recebimento_id'), $column_valor, $conn);
            if ($result) {
                $this->Flash->success('Faturas recebidas com sucesso!');
            } else {
                $this->Flash->error('Erro ao tentar receber faturas!');
            }
        }

        $situacaoRecebimentos = $this->SituacaoRecebimentos->find('list')->where(['SituacaoRecebimentos.situacao_id' => 1])->orderAsc('SituacaoRecebimentos.nome');
        $this->set(compact('situacaoRecebimentos'));
    }


    public function report()
    {
        $this->viewBuilder()->layout('report_2016');
        $array = $query = $this->Faturamento->atendimentoProcedimentos(
            empty($this->request->query('situacao_fatura_id')) ? null : $this->request->query('situacao_fatura_id'),
            empty($this->request->query('data_inicio')) ? null : $this->request->query('data_inicio'),
            empty($this->request->query('data_fim')) ? null : $this->request->query('data_fim'),
            empty($this->request->query('unidade_id')) ? null : $this->request->query('unidade_id'),
            empty($this->request->query('origen_id')) ? null : $this->request->query('origen_id'),
            empty($this->request->query('convenios')) ? null : $this->request->query('convenios'),
            empty($this->request->query('grupos')) ? null : $this->request->query('grupos'),
            empty($this->request->query('procedimentos')) ? null : $this->request->query('procedimentos'),
            empty($this->request->query('medicos')) ? null : $this->request->query('medicos'),
            empty($this->request->query('controle')) ? null : $this->request->query('controle'),
            null
        );
        $json['atendimento'][] = null;
        foreach ($array as $a) {
            $json['atendimento'][] = [
                'atendimento' => $a->atendimento->id,
                'data' => $a->atendimento->data,
                'paciente' => $a->atendimento->cliente->nome,
                'convenio' => $a->atendimento->convenio->nome,
                'medico' => $a->medico_responsavei->nome,
                'matricula' => $a->matricula,
                'codigo' => $a->codigo,
                'procedimento' => $a->procedimento->nome,
                'guia' => $a->nr_guia,
                'autorizacao' => $a->autorizacao_senha,
                'quantidade' => $a->quantidade,
                'valor_fatura' => $a->valor_fatura,
                'valor_caixa' => $a->valor_caixa,
                'material' => $a->material,
                'total' => $a->total
            ];
        }
        $this->Json->create('pre_conferencia_faturamento.json', json_encode($json));
        $this->set('report', 'pre_conferencia_faturamento.mrt');
    }

    public function gestaoFaturas()
    {
        $this->loadModel('AtendimentoProcedimentos');
        $query = $this->Faturamento->filterAtendProc($this->request->query);

        $this->paginate = [
            'limit' => 50
        ];
        $atendimentoProcedimentos = $this->paginate($query);

        $fill = false;
        if (!empty($this->request->query('unidade_id'))) {
            $fill = true;
        }

        $unidades = $this->AtendimentoProcedimentos->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $origens = $this->AtendimentoProcedimentos->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->AtendimentoProcedimentos->Atendimentos->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->AtendimentoProcedimentos->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->AtendimentoProcedimentos->Procedimentos->find('list', ['limit' => 200])->orderAsc('Procedimentos.nome');
        $medicos = $this->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_recebimentos = $this->AtendimentoProcedimentos->SituacaoRecebimentos->find('list', ['limit' => 200])->where(['SituacaoRecebimentos.situacao_id' => 1])->orderAsc('SituacaoRecebimentos.nome');

        $this->set(compact('unidades', 'origens', 'convenios', 'grupo_procedimentos', 'medicos', 'atendimentoProcedimentos', 'situacao_recebimentos', 'procedimentos', 'fill'));
    }

    public function countFilter()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->loadModel('AtendimentoProcedimentos');

        $query = $this->Faturamento->filterAtendProc($this->request->query);

        $query = $this->Faturamento->filterAtendProc($this->request->query);
        $query_select = $query->select([
            'total' => $query->func()->sum('total'),
            'count' => $query->func()->sum('quantidade')
        ])->toArray();

        $total = ($query_select[0]->total != null) ? $query_select[0]->total : '0.00';
        $count = ($query_select[0]->count != null) ? $query_select[0]->count : 0;

        $this->response->body(json_encode(['total' => $total, 'count' => $count]));
    }

    public function reportFatura($id)
    {
        $this->viewBuilder()->setLayout('report_2016');
        $json = $this->Relatorios->fatura($id);
        $this->Json->create('fatura.json', json_encode($json));
        $this->set('report', 'faturamento.mrt');
        $this->render('report');
    }

    public function filterEquipes()
    {
        $query = $this->Faturamento->filterAtendProcToEquipes($this->request->getQuery());
        $total = 0;
        foreach ($query as $q) {
            $total += $q->valor_profissional;
        }
        $json = [
            'total' => $total,
            'contador' => $query->count(),
        ];
        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function annualBilling () {

        $meses = [
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        ];
    
        $anos = [];

        $mesReferencia = '';
        $anoReferencia = '';
    
        for ($i = -3; $i <= 3; $i++) {
            $anos[date('Y') + ($i)] = date('Y') + ($i);
        }

        if (!empty($this->request->query('mes'))) {
            $mesReferencia = $this->request->query('mes');
            $mesReferencia = $meses[$mesReferencia];
        }

        if (!empty($this->request->query('ano'))) {
            $anoReferencia = $this->request->query('ano');
            $anoReferencia = $anos[$anoReferencia];
        }

        $date = new Time();
        $formatedDateMes = intval($date->format('m'));
        $formatedDateAno = $date->format('Y');

        $this->set(compact('meses', 'anos', 'mesReferencia', 'anoReferencia', 'formatedDateMes', 'formatedDateAno'));
        $this->set('_serialize', ['faturamentos']);
        $this->render('faturamento_anual');
    }

    public function getDataForAnnualBillingChart ($date = null)
    {    
        $date = strtotime($date);
        $date = date('Y-m-d', $date);
        $formatedDate = new Time($date);
        $selectedMonth = $formatedDate->format('m');

        $valorNaoFaturado = number_format($this->Faturamento->getValorNaoFaturado(new Time($date)), 2,'.','.');
        $qtNaoFaturado = number_format($this->Faturamento->getQtNaoFaturado(new Time($date)), 2,'.','.');
        $porcentualNaoFaturado = number_format($this->Faturamento->getPorcentualNaoFaturado(new Time($date)), 2,'.','.');

        $this->autoRender = false;
        $this->loadModel('FaturaEncerramentos');

        $ultimo_dia_mes = $this->Data->ultimoDiaMes($selectedMonth);
        //For para pegar os meses e anos
        $meses[] = $this->Data->getLastTwelveMonths($formatedDate);

        $totais = [];
        //Foreach para pegar os valores por mês
        foreach($meses[0] as $key => $mes)
        {   
            $query = $this->FaturaEncerramentos->find('all')
                ->contain(['Convenios'])
                ->where([
                    'FaturaEncerramentos.competencia_ano' => $mes->year,
                    'FaturaEncerramentos.competencia_mes' => $mes->month
                ])
                ->andWhere(['FaturaEncerramentos.situacao_id' => 1])
                ->orderDesc('FaturaEncerramentos.id');

            $faturaEncerramentos = $this->paginate($query);
            $totalRecBankMes = 0;
            $totalPrevistoMes = 0;

            foreach ($faturaEncerramentos as $f) 
            {
                $total_previsto = 0;
                $totalTax = 0;

                //Calculo total previsto
                $this->loadModel('ConvenioImpostosParametro');
                $convenioImpostosParametro = $this->ConvenioImpostosParametro->find()
                    ->where(['id_convenio' => $f->convenio->id]);

                foreach ($convenioImpostosParametro as $cP) 
                {
                    $totalTax += $cP->porcentual;
                }

                $total_previsto = $f->total_bruto - ($f->total_bruto * ($totalTax / 100));
                
                $totalPrevistoMes += $total_previsto;
                $totalRecBankMes += $f->vl_received_bank;
            }

            $totalRecBankMes = number_format($totalRecBankMes, 2,'.','');
            $totalPrevistoMes = number_format($totalPrevistoMes, 2,'.','');

            $totais[0][] = $totalPrevistoMes;
            $totais[1][] = $totalRecBankMes;
        }

        $label[] = [0 => 'Valor Previsto', 1 => 'Valor Rec Banco'];
        $this->response->type('json');
        $json = json_encode(['labels' => $label, 'totais' => $totais, 'meses' => $meses, 'valorNaoFaturado' => $valorNaoFaturado, 'qtNaoFaturado' => $qtNaoFaturado, 'porcentualNaoFaturado' => $porcentualNaoFaturado]);
        $this->response->body($json);
    }
 }