<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GrupoProcedimentos Controller
 *
 * @property \App\Model\Table\GrupoProcedimentosTable $GrupoProcedimentos
 */
class GrupoProcedimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['GrupoProcedimentos.situacao_id = ' => '1']
                    ];
        $grupoProcedimentos = $this->paginate($this->GrupoProcedimentos);

        $this->set(compact('grupoProcedimentos'));
        $this->set('_serialize', ['grupoProcedimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupo Procedimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoProcedimento = $this->GrupoProcedimentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('grupoProcedimento', $grupoProcedimento);
        $this->set('_serialize', ['grupoProcedimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupoProcedimento = $this->GrupoProcedimentos->newEntity();
        if ($this->request->is('post')) {
            $newGrupoProcedimento = $this->request->data;
            $grupoProcedimento = $this->GrupoProcedimentos->patchEntity($grupoProcedimento, $newGrupoProcedimento);
            if ($this->GrupoProcedimentos->save($grupoProcedimento)) {
                $this->Flash->success(__('O grupo procedimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo procedimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrupoProcedimentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->GrupoProcedimentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('grupoProcedimento', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['grupoProcedimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Procedimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoProcedimento = $this->GrupoProcedimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupoProcedimento = $this->GrupoProcedimentos->patchEntity($grupoProcedimento, $this->request->data);
            if ($this->GrupoProcedimentos->save($grupoProcedimento)) {
                $this->Flash->success(__('O grupo procedimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo procedimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrupoProcedimentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->GrupoProcedimentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('grupoProcedimento', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['grupoProcedimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Procedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupoProcedimento = $this->GrupoProcedimentos->get($id);
                $grupoProcedimento->situacao_id = 2;
        if ($this->GrupoProcedimentos->save($grupoProcedimento)) {
            $this->Flash->success(__('O grupo procedimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo procedimento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
