<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinTipoPagamento Controller
 *
 * @property \App\Model\Table\FinTipoPagamentoTable $FinTipoPagamento
 */
class FinTipoPagamentoController extends AppController
{

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Tipo de pagamento');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FinStatus', 'FinTipoDocumento']
        ];
        $finTipoPagamento = $this->paginate($this->FinTipoPagamento);

        $this->set(compact('finTipoPagamento'));
        $this->set('_serialize', ['finTipoPagamento']);

    }

    /**
     * View method
     *
     * @param string|null $id Fin Tipo Pagamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finTipoPagamento = $this->FinTipoPagamento->get($id, [
            'contain' => ['FinStatus', 'TipoDocumentos', 'FinContasPagar']
        ]);

        $this->set('finTipoPagamento', $finTipoPagamento);
        $this->set('_serialize', ['finTipoPagamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finTipoPagamento = $this->FinTipoPagamento->newEntity();
        if ($this->request->is('post')) {
            $finTipoPagamento = $this->FinTipoPagamento->patchEntity($finTipoPagamento, $this->request->data);
            if ($this->FinTipoPagamento->save($finTipoPagamento)) {
                $this->Flash->success(__('O tipo de pagamento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo de pagamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finTipoPagamento'));
        $this->set('_serialize', ['finTipoPagamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Tipo Pagamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finTipoPagamento = $this->FinTipoPagamento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finTipoPagamento = $this->FinTipoPagamento->patchEntity($finTipoPagamento, $this->request->data);
            if ($this->FinTipoPagamento->save($finTipoPagamento)) {
                $this->Flash->success(__('O tipo de pagamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo de pagamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finTipoPagamento'));
        $this->set('_serialize', ['finTipoPagamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Tipo Pagamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finTipoPagamento = $this->FinTipoPagamento->get($id);
                $finTipoPagamento->situacao_id = 2;
        if ($this->FinTipoPagamento->save($finTipoPagamento)) {
            $this->Flash->success(__('O tipo de pagamento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo de pagamento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Tipo Pagamento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinTipoPagamento->find('all')
        ->where(['FinTipoPagamento.descricao LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinTipoPagamento.descricao');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->descricao);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Tipo Pagamento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finTipoPagamento = $this->FinTipoPagamento->get($this->request->data['id']);
            $res = ['nome'=>$finTipoPagamento->nome,'id'=>$finTipoPagamento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
