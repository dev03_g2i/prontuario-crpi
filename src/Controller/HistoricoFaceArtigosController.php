<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HistoricoFaceArtigos Controller
 *
 * @property \App\Model\Table\HistoricoFaceArtigosTable $HistoricoFaceArtigos
 */
class HistoricoFaceArtigosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ClienteHistoricos', 'FaceArtigos', 'SituacaoCadastros', 'Users']
        ];
        $historicoFaceArtigos = $this->paginate($this->HistoricoFaceArtigos);


        $this->set(compact('historicoFaceArtigos'));
        $this->set('_serialize', ['historicoFaceArtigos']);

    }

    /**
     * View method
     *
     * @param string|null $id Historico Face Artigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $historicoFaceArtigo = $this->HistoricoFaceArtigos->get($id, [
            'contain' => ['ClienteHistoricos', 'FaceArtigos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('historicoFaceArtigo', $historicoFaceArtigo);
        $this->set('_serialize', ['historicoFaceArtigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $historicoFaceArtigo = $this->HistoricoFaceArtigos->newEntity();
        if ($this->request->is('post')) {
            $historicoFaceArtigo = $this->HistoricoFaceArtigos->patchEntity($historicoFaceArtigo, $this->request->data);
            if ($this->HistoricoFaceArtigos->save($historicoFaceArtigo)) {
                $this->Flash->success(__('O historico face artigo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O historico face artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('historicoFaceArtigo'));
        $this->set('_serialize', ['historicoFaceArtigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Historico Face Artigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $historicoFaceArtigo = $this->HistoricoFaceArtigos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $historicoFaceArtigo = $this->HistoricoFaceArtigos->patchEntity($historicoFaceArtigo, $this->request->data);
            if ($this->HistoricoFaceArtigos->save($historicoFaceArtigo)) {
                $this->Flash->success(__('O historico face artigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O historico face artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('historicoFaceArtigo'));
        $this->set('_serialize', ['historicoFaceArtigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Historico Face Artigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $historicoFaceArtigo = $this->HistoricoFaceArtigos->get($id);
                $historicoFaceArtigo->situacao_id = 2;
        if ($this->HistoricoFaceArtigos->save($historicoFaceArtigo)) {
            $this->Flash->success(__('O historico face artigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O historico face artigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Historico Face Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->HistoricoFaceArtigos->find('all')
        ->where(['HistoricoFaceArtigos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('HistoricoFaceArtigos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Historico Face Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $historicoFaceArtigo = $this->HistoricoFaceArtigos->get($this->request->data['id']);
            $res = ['nome'=>$historicoFaceArtigo->nome,'id'=>$historicoFaceArtigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
