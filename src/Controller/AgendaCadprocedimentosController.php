<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AgendaCadprocedimentos Controller
 *
 * @property \App\Model\Table\AgendaCadprocedimentosTable $AgendaCadprocedimentos
 */
class AgendaCadprocedimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $agendaCadprocedimentos = $this->paginate($this->AgendaCadprocedimentos);


        $this->set(compact('agendaCadprocedimentos'));
        $this->set('_serialize', ['agendaCadprocedimentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Agenda Cadprocedimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendaCadprocedimento = $this->AgendaCadprocedimentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('agendaCadprocedimento', $agendaCadprocedimento);
        $this->set('_serialize', ['agendaCadprocedimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agendaCadprocedimento = $this->AgendaCadprocedimentos->newEntity();
        if ($this->request->is('post')) {
            $agendaCadprocedimento = $this->AgendaCadprocedimentos->patchEntity($agendaCadprocedimento, $this->request->data);
            if ($this->AgendaCadprocedimentos->save($agendaCadprocedimento)) {
                $this->Flash->success(__('O agenda cadprocedimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O agenda cadprocedimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('agendaCadprocedimento'));
        $this->set('_serialize', ['agendaCadprocedimento']);
    }

    public function orientacao($id)
    {
        $orientacao = $this->AgendaCadprocedimentos->get($id);
        if($this->request->is(['patch', 'post', 'put'])){
            $orientacao = $this->AgendaCadprocedimentos->patchEntity($orientacao, $this->request->data);
            if($this->AgendaCadprocedimentos->save($orientacao)) {
                $this->Flash->success(__('Orientação foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Orientação não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('orientacao'));
    }

    public function listOrientacao()
    {
        $agendaCadproc = null;
        if(!empty($this->request->query('procedimentos'))){
          $procs = explode(",", $this->request->query('procedimentos'));
          $agendaCadproc = $this->AgendaCadprocedimentos->find('all')->where(['AgendaCadprocedimentos.id IN' => $procs]);
        }
        $this->set(compact('agendaCadproc'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda Cadprocedimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendaCadprocedimento = $this->AgendaCadprocedimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendaCadprocedimento = $this->AgendaCadprocedimentos->patchEntity($agendaCadprocedimento, $this->request->data);
            if ($this->AgendaCadprocedimentos->save($agendaCadprocedimento)) {
                $this->Flash->success(__('O agenda cadprocedimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O agenda cadprocedimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('agendaCadprocedimento'));
        $this->set('_serialize', ['agendaCadprocedimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda Cadprocedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agendaCadprocedimento = $this->AgendaCadprocedimentos->get($id);
        $agendaCadprocedimento->situacao_id = 2;
        if ($this->AgendaCadprocedimentos->save($agendaCadprocedimento)) {
            $this->Flash->success(__('O agenda cadprocedimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O agenda cadprocedimento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Agenda Cadprocedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->AgendaCadprocedimentos->find('all')
            ->where(['AgendaCadprocedimentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AgendaCadprocedimentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Agenda Cadprocedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $agendaCadprocedimento = $this->AgendaCadprocedimentos->get($this->request->data['id']);
            $res = ['nome'=>$agendaCadprocedimento->nome,'id'=>$agendaCadprocedimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
