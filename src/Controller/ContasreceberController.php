<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Contasreceber Controller
 *
 * @property \App\Model\Table\ContasreceberTable $Contasreceber
 */
class ContasreceberController extends AppController
{

    public $components = ['Configuracao'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        unset($_SESSION['v_parcelas']);
        $this->loadModel('ContasReceber');
        $query = $this->Contasreceber->find('all')
            ->contain(['ControleFinanceiro', 'Planocontas', 'ClienteClone', 'TipoPagamento', 'TipoDocumento', 'UserReg', 'UserAlt'])
            ->where(['Contasreceber.status_id' => 1]);

        $extern_id = null;
        $atendimento = null;
        if (!empty($this->request->query['id'])) {
            $query->andWhere(['Contasreceber.extern_id' => $this->request->query['id']]);
            $extern_id = $this->request->query['id'];
            $this->loadModel('Atendimentos');
            $atendimento = $this->Atendimentos->get($this->request->query['id'], [
                'contain' => ['Clientes']
            ]);
        }
        $this->paginate = [
            'limit' => 10
        ];
        $contasreceber = $this->paginate($query);
        $this->set(compact('contasreceber', 'extern_id', 'atendimento'));
        $this->set('_serialize', ['contasreceber']);
    }

    public function all()
    {
        unset($_SESSION['v_parcelas']);
        $this->loadModel('ContasReceber');
        $query = $this->Contasreceber->find('all')
            ->contain(['Planocontas', 'ClienteClone', 'TipoPagamento', 'TipoDocumento'])
            ->where(['Contasreceber.status_id = ' => '1']);

        $extern_id = null;
        if (!empty($this->request->query['id'])) {
            $query->andWhere(['Contasreceber.extern_id' => $this->request->query['id']]);
            $extern_id = $this->request->query['id'];
        }
        $cliente_id = null;
        if (!empty($this->request->query['cliente_id'])) {
            $query->andWhere(['Contasreceber.idCliente' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        }
        $this->paginate = [
            'limit' => 10
        ];
        $contasreceber = $this->paginate($query);
        $this->set(compact('contasreceber', 'extern_id', 'cliente_id'));
        $this->set('_serialize', ['contasreceber']);
    }


    public function financeiro()
    {
        unset($_SESSION['v_parcelas']);
        $this->loadModel('Atendimentos');
        $query = $this->Atendimentos->find('all')
            ->contain(['ContasReceber'])
            ->where(['Atendimentos.finalizado' => 0]);

        $extern_id = null;
        if (!empty($this->request->query['id'])) {
            $query->andWhere(['Atendimentos.id' => $this->request->query['id']]);
            $extern_id = $this->request->query['id'];
        }
        $cliente_id = null;
        if (!empty($this->request->query['cliente_id'])) {
            $query->andWhere(['Atendimentos.cliente_id' => $this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        }

        $this->paginate = [
            'limit' => 10
        ];
        $atendimentos = $this->paginate($query);
        $this->set(compact('atendimentos', 'extern_id', 'cliente_id'));
        $this->set('_serialize', ['atendimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Contasreceber id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('ContasReceber');
        $contasreceber = $this->Contasreceber->get($id, [
            'contain' =>
                ['Planocontas', 'ClienteClone', 'Status', 'Contabilidade', 'TipoPagamento', 'TipoDocumento', 'Programacao', 'Deducoes']
        ]);

        $this->set('contasreceber', $contasreceber);
        $this->set('_serialize', ['contasreceber']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->loadModel('ContasReceber');
        $contasreceber = $this->Contasreceber->newEntity();
        if ($this->request->is('post')) {
            $contasreceber = $this->Contasreceber->patchEntity($contasreceber, $this->request->data);
            $contasreceber->extern_id = $this->request->query['extern_id'];
            if ($this->Contasreceber->save($contasreceber)) {
                $this->Flash->success(__('O contasreceber foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contasreceber não foi salvo. Por favor, tente novamente.'));
            }
        }
        $atendimento = null;
        if (!empty($this->request->query['extern_id'])) {
            $this->loadModel('Atendimentos');
            $atendimento = $this->Atendimentos->get($this->request->query['extern_id']);
            //verifico se ja existe o cliente no financeiro
            $this->loadModel('ClienteClone');
            $clone = $this->ClienteClone->findById($atendimento->cliente_id)->first();
            if (empty($clone)) {
                $this->loadModel('Clientes');
                $cliente = $this->Clientes->get($atendimento->cliente_id);
                $clienteclone = $this->ClienteClone->newEntity();
                $clienteclone->id = $cliente->id;
                $clienteclone->nome = $cliente->nome;
                $clienteclone->telefone = $cliente->telefone;
                $clienteclone->celular = $cliente->celular;
                $clienteclone->cep = $cliente->cep;
                $clienteclone->endereco = $cliente->endereco;
                $clienteclone->numero = $cliente->numero;
                $clienteclone->bairro = $cliente->bairro;
                $clienteclone->cidade = $cliente->cidade;
                $clienteclone->uf = $cliente->estado;
                $clienteclone->complemento = $cliente->complemento;
                $clienteclone->cpf = $cliente->cpf;
                $clienteclone->email = $cliente->email;
                $clienteclone->rg = $cliente->rg;
                $clienteclone->datacadastro = $cliente->created;
                $clienteclone->observacao = $cliente->observacao;
                $this->ClienteClone->save($clienteclone);
            }
        }

        $planocontas = $this->Contasreceber->Planocontas->find('list', ['limit' => 200, 'contain' => ['Classificacaocontas']])
            ->where(['Classificacaocontas.tipo' => 1]);
        $clienteClone = $this->Contasreceber->ClienteClone->find('list', ['limit' => 200]);
        $contabilidade = $this->Contasreceber->Contabilidade->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contasreceber->TipoPagamento->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contasreceber->TipoDocumento->find('list', ['limit' => 200]);
        $correcao = $this->Contasreceber->Correcaomonetaria->find('list', ['limit' => 200]);
        $status = $this->Contasreceber->Status->find('list', ['limit' => 200]);

        $this->set(compact('contasreceber', 'planocontas', 'clienteClone', 'atendimento', 'contabilidade', 'tipoPagamento', 'tipoDocumento', 'correcao', 'status'));
        $this->set('_serialize', ['contasreceber']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Contasreceber id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->loadModel('ContasReceber');
        $contasreceber = $this->Contasreceber->get($id, [
            'contain' => ['Deducoes', 'UserReg', 'UserAlt']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $contasreceber = $this->Contasreceber->patchEntity($contasreceber, $this->request->data);
            if ($this->Contasreceber->save($contasreceber)) {
                $this->Flash->success(__('O contasreceber foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contasreceber não foi salvo. Por favor, tente novamente.'));
            }
            $this->autoRender = false;
        }

        $this->loadModel('Atendimentos');
        $atendimentos = null;
        if (!empty($contasreceber->extern_id)) {
            $atendimentos = $this->Atendimentos->get($contasreceber->extern_id);
        }
        $planocontas = $this->Contasreceber->Planocontas->find('list', ['limit' => 200, 'contain' => ['Classificacaocontas']])->where(['Classificacaocontas.tipo' => 1]);
        $clienteClone = $this->Contasreceber->ClienteClone->find('list', ['limit' => 200]);
        $status = $this->Contasreceber->Status->find('list', ['limit' => 200]);
        $contabilidade = $this->Contasreceber->Contabilidade->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contasreceber->TipoPagamento->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contasreceber->TipoDocumento->find('list', ['limit' => 200]);
        $programacao = $this->Contasreceber->Programacao->find('list', ['limit' => 200]);
        $deducoes = $this->Contasreceber->Deducoes->find('list', ['limit' => 200]);
        $correcao = $this->Contasreceber->Correcaomonetaria->find('list', ['limit' => 200]);
        if ($this->Configuracao->showCampoControleFinanceiroContasReceber()) {
            $controles = $this->Contasreceber->ControleFinanceiro->find('list', ['limit' => 200])->where(['ControleFinanceiro.usa_receita' => 1]);
        } else {
            $controles = $this->Configuracao->getControleFinanceiroContasReceberPadrao();
        }
        $this->set(compact('contasreceber', 'planocontas', 'clienteClone', 'status', 'contabilidade', 'tipoPagamento', 'tipoDocumento', 'deducoes', 'correcao', 'atendimentos', 'controles'));
        $this->set('_serialize', ['contasreceber']);

    }

    public function nedit($id = null)
    {
        $this->loadModel('ContasReceber');
        $contasreceber = $this->Contasreceber->get($id, [
            'contain' => ['Deducoes']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $contasreceber = $this->Contasreceber->patchEntity($contasreceber, $this->request->data);
            if ($this->Contasreceber->save($contasreceber)) {
                $this->Flash->success(__('O contasreceber foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O contasreceber não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->loadModel('Atendimentos');
        $atendimentos = null;
        if (!empty($contasreceber->extern_id)) {
            $atendimentos = $this->Atendimentos->get($contasreceber->extern_id);
        }
        $planocontas = $this->Contasreceber->Planocontas->find('list', ['limit' => 200]);
        $clienteClone = $this->Contasreceber->ClienteClone->find('list', ['limit' => 200]);
        $status = $this->Contasreceber->Status->find('list', ['limit' => 200]);
        $contabilidade = $this->Contasreceber->Contabilidade->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contasreceber->TipoPagamento->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contasreceber->TipoDocumento->find('list', ['limit' => 200]);
        $programacao = $this->Contasreceber->Programacao->find('list', ['limit' => 200]);
        $deducoes = $this->Contasreceber->Deducoes->find('list', ['limit' => 200]);
        $correcao = $this->Contasreceber->Correcaomonetaria->find('list', ['limit' => 200]);

        $this->set(compact('contasreceber', 'planocontas', 'clienteClone', 'status', 'contabilidade', 'tipoPagamento', 'tipoDocumento', 'deducoes', 'correcao', 'atendimentos'));
        $this->set('_serialize', ['contasreceber']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Contasreceber id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->loadModel('ContasReceber');
        $this->request->allowMethod(['post', 'delete']);
        $contasreceber = $this->Contasreceber->get($id);
        $contasreceber->situacao_id = 2;
        if ($this->Contasreceber->save($contasreceber)) {
            $this->Flash->success(__('O contasreceber foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O contasreceber não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }


    /* Nome foi mudado para newAdd por causa das permissões de usuarios - Luciano 02/02/2018 08:50
     * public function parcelas(){
        $this->loadModel('ContasReceber');
        $contasreceber = $this->Contasreceber->newEntity();
        if ($this->request->is('post')){
            $aux=0;
            $ap = $this->request->data['vencimento'];
            for($x=1; $x<=$this->request->data['parcelas']; $x++) {
                $contasreceber = $this->Contasreceber->newEntity();
                $contasreceber = $this->Contasreceber->patchEntity($contasreceber, $this->request->data);
                $contasreceber->vencimento =$ap;
                $contasreceber->parcela = $x.'/'.$this->request->data['parcelas'];
                $contasreceber->valorBruto = number_format($this->request->data['valor']/$this->request->data['parcelas'],2,'.','');
                $contasreceber->valor = number_format($this->request->data['valor']/$this->request->data['parcelas'],2,'.','');
                $contasreceber->tipoDocumento = 2 ;
                if($this->Contasreceber->save($contasreceber)){
                    $aux = 1;
                }
                $ap = date("d/m/Y", strtotime("+30 days", strtotime(str_replace('/','-',$ap))));
            }
            if ($aux==1) {
                $_SESSION['v_parcelas'] = 1;
                echo $this->request->query['id'];
            } else {
                echo 'erro';
            }
             $this->autoRender = false;
        }
        $this->loadModel('Atendimentos');
        $atendimento = $this->Atendimentos->get($this->request->query['id']);
        $atendimento->data = $atendimento->data->format('Y-m-d');
        $atendimento->acertado=1;
        $this->Atendimentos->save($atendimento);

        //verifico se ja existe o cliente no financeiro
        $this->loadModel('ClienteClone');
        $clone = $this->ClienteClone->findById($atendimento->cliente_id)->first();
        if (empty($clone->id)) {
            $this->loadModel('Clientes');
            $cliente = $this->Clientes->get($atendimento->cliente_id);
            $clienteclone = $this->ClienteClone->newEntity();
            $clienteclone->id = $cliente->id;
            $clienteclone->nome = $cliente->nome;
            $clienteclone->telefone= $cliente->telefone;
            $clienteclone->celular= $cliente->celular;
            $clienteclone->cep= $cliente->cep;
            $clienteclone->endereco= $cliente->endereco;
            $clienteclone->numero= $cliente->numero;
            $clienteclone->bairro= $cliente->bairro;
            $clienteclone->cidade= $cliente->cidade;
            $clienteclone->uf= $cliente->estado;
            $clienteclone->complemento= $cliente->complemento;
            $clienteclone->cpf= $cliente->cpf;
            $clienteclone->email= $cliente->email;
            $clienteclone->rg= $cliente->rg;
            $clienteclone->datacadastro= $cliente->created;
            $clienteclone->observacao= $cliente->observacao;
            $this->ClienteClone->save($clienteclone);
        }

        $planocontas = $this->Contasreceber->Planocontas->find('list', ['contain'=>['Classificacaocontas']])
            ->where(['Classificacaocontas.tipo'=>1]);

        $status = $this->Contasreceber->Status->find('list', ['limit' => 200]);
        $contabilidade = $this->Contasreceber->Contabilidade->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contasreceber->TipoPagamento->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contasreceber->TipoDocumento->find('list', ['limit' => 200]);
        $correcao = $this->Contasreceber->Correcaomonetaria->find('list', ['limit' => 200]);
        $this->set(compact('contasreceber', 'planocontas', 'atendimento', 'contabilidade','clienteclone', 'tipoPagamento', 'tipoDocumento', 'correcao','status'));
        $this->set('_serialize', ['contasreceber']);
    }*/
    public function newAdd()
    {
        $this->loadModel('ContasReceber');
        $contasreceber = $this->Contasreceber->newEntity();
        if ($this->request->is('post')) {
            $aux = 0;
            $ap = $this->request->data['vencimento'];
            for ($x = 1; $x <= $this->request->data['parcelas']; $x++) {
                $contasreceber = $this->Contasreceber->newEntity();
                $contasreceber = $this->Contasreceber->patchEntity($contasreceber, $this->request->data);
                $contasreceber->vencimento = $ap;
                $contasreceber->parcela = $x . '/' . $this->request->data['parcelas'];
                $contasreceber->valorBruto = number_format($this->request->data['valor'] / $this->request->data['parcelas'], 2, '.', '');
                $contasreceber->valor = number_format($this->request->data['valor'] / $this->request->data['parcelas'], 2, '.', '');
                $contasreceber->tipoDocumento = 2;
                if ($this->Contasreceber->save($contasreceber)) {
                    $aux = 1;
                }
                $ap = date("d/m/Y", strtotime("+30 days", strtotime(str_replace('/', '-', $ap))));
            }
            if ($aux == 1) {
                $_SESSION['v_parcelas'] = 1;
                echo $this->request->query['id'];
            } else {
                echo 'erro';
            }
            $this->autoRender = false;
        }
        $this->loadModel('Atendimentos');
        $atendimento = $this->Atendimentos->get($this->request->query['id']);
        $atendimento->acertado = 1;
        $this->Atendimentos->save($atendimento);

        //verifico se ja existe o cliente no financeiro
        $this->loadModel('ClienteClone');
        $clone = $this->ClienteClone->findById($atendimento->cliente_id)->first();
        if (empty($clone->id)) {
            $this->loadModel('Clientes');
            $cliente = $this->Clientes->get($atendimento->cliente_id);
            $clienteclone = $this->ClienteClone->newEntity();
            $clienteclone->id = $cliente->id;
            $clienteclone->nome = $cliente->nome;
            $clienteclone->telefone = $cliente->telefone;
            $clienteclone->celular = $cliente->celular;
            $clienteclone->cep = $cliente->cep;
            $clienteclone->endereco = $cliente->endereco;
            $clienteclone->numero = $cliente->numero;
            $clienteclone->bairro = $cliente->bairro;
            $clienteclone->cidade = $cliente->cidade;
            $clienteclone->uf = $cliente->estado;
            $clienteclone->complemento = $cliente->complemento;
            $clienteclone->cpf = $cliente->cpf;
            $clienteclone->email = $cliente->email;
            $clienteclone->rg = $cliente->rg;
            $clienteclone->datacadastro = $cliente->created;
            $clienteclone->observacao = $cliente->observacao;
            $this->ClienteClone->save($clienteclone);
        }

        $planocontas = $this->Contasreceber->Planocontas->find('list', ['contain' => ['Classificacaocontas']])
            ->where(['Classificacaocontas.tipo' => 1]);

        $status = $this->Contasreceber->Status->find('list', ['limit' => 200]);
        $contabilidade = $this->Contasreceber->Contabilidade->find('list', ['limit' => 200]);
        $tipoPagamento = $this->Contasreceber->TipoPagamento->find('list', ['limit' => 200]);
        $tipoDocumento = $this->Contasreceber->TipoDocumento->find('list', ['limit' => 200]);
        $correcao = $this->Contasreceber->Correcaomonetaria->find('list', ['limit' => 200]);
        if ($this->Configuracao->showCampoControleFinanceiroContasReceber()) {
            $controles = $this->Contasreceber->ControleFinanceiro->find('list', ['limit' => 200])->where(['ControleFinanceiro.usa_receita' => 1]);
        } else {
            $controles = $this->Configuracao->getControleFinanceiroContasReceberPadrao();
        }
        $this->set(compact('contasreceber', 'planocontas', 'atendimento', 'contabilidade', 'clienteclone', 'tipoPagamento', 'tipoDocumento', 'correcao', 'status', 'controles'));
        $this->set('_serialize', ['contasreceber']);
    }

    /**
     * IMPORTANTE: O PLANO CONTA -10 SIGNIFICA NÃO FOI CONFIGURADO NADA NO CADASTRO DOS TIPOS DE PAGAMENTOS
     * NESTE CASO SERÁ PEGO O VALOR PADRÃO DEFINIDO NA TABELA DE CONFIGURAÇÃO (caixa_idplano_contas), SE FOR DIFERENTE
     * DE -10 ENTÃO BUSCAREI O VALOR NA TABELA DE CONTAS A RECEBER PARA SETAR EM UM CAMPO HIDDEN NA TELA DE new_add.ctp
     * DA CONTROLLER contasreceber. LEMBRANDO QUE ESTA BUSCA SÓ OCORRERÁ SE O CAMPO mostra_caixa_plano DA TABELA DE
     * CONFIGURAÇÕES ESTIVER MARCADO COM 0 (zero)
     */
    public function getPlanoConta()
    {
        $this->loadModel('TipoPagamento');
        $tipoPagamentoId = $this->request->getData('idTipoPagamento');
        $tipoPagamento = $this->TipoPagamento->get($tipoPagamentoId, [
            'contain' => [
                'Planocontas'
            ]
        ]);

        if ($tipoPagamento->id_plano_contas_receita == -10) {
            $idPlanoContas = $this->Configuracao->getIdPlanoContasPadrao();
        } else {
            $idPlanoContas = $tipoPagamento->planoconta->id;
        }

        $this->autoRender = false;
        $this->response->body(json_encode($idPlanoContas));
    }

    public function parcialEdit()
    {
        $this->loadModel('ContasReceber');
        $this->autoRender = false;

        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if (!empty($this->request->data['name']) && !empty($this->request->data['value'])) {
            $name = $this->request->data['name'];
            $contas = $this->Contasreceber->findById($this->request->data['pk'])->first();
            $contas->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->Contasreceber->save($contas)) {
                $res = ['res' => $contas->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

}
