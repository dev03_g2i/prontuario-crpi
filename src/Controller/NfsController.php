<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Nfs Controller
 *
 * @property \App\Model\Table\NfsTable $Nfs
 */
class NfsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($atendimento_id = null)
    {
        $query = $this->Nfs->find('all')
            ->contain(['NfTomadores' => ['Clientes'], 'NfSituacaoNotas'])
            ->where(['Nfs.situacao_id' => 1])->orderDesc('Nfs.id');

        if(!empty($atendimento_id)){
            $query->andWhere(['Nfs.ficha' => $atendimento_id]);
        }

        if(!empty($this->request->getQuery('situacao_nota_id'))){
            $query->andWhere(['Nfs.situacao_nota_id' => $this->request->getQuery('situacao_nota_id')]);
        }
        
        if(!empty($this->request->getQuery('cliente_id'))){
            $query->andWhere(['NfTomadores.cliente_id' => $this->request->getQuery('cliente_id')]);
        }

        $nfs = $this->paginate($query);

        $situacao_nfs = $this->Nfs->NfSituacaoNotas->getList();
        $this->set(compact('nfs', 'situacao_nfs'));
        $this->set('_serialize', ['nfs']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nf = $this->Nfs->get($id, [
            'contain' => ['NfTomadores' => ['Clientes'], 'NfSituacaoNotas']
        ]);

        $this->set('nf', $nf);
        $this->set('_serialize', ['nf']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nf = $this->Nfs->newEntity();
        if ($this->request->is('post')) {
            $nf = $this->Nfs->patchEntity($nf, $this->request->data);
            if ($this->Nfs->save($nf)) {
                $this->Flash->success(__('O nf foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nf'));
        $this->set('_serialize', ['nf']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nf = $this->Nfs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nf = $this->Nfs->patchEntity($nf, $this->request->data);
            if ($this->Nfs->save($nf)) {
                $this->Flash->success(__('O nf foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nf'));
        $this->set('_serialize', ['nf']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nf = $this->Nfs->get($id);
                $nf->situacao_id = 2;
        if ($this->Nfs->save($nf)) {
            $this->Flash->success(__('O nf foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Nfs->find('all')
        ->where(['Nfs.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Nfs.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nf = $this->Nfs->get($this->request->data['id']);
            $res = ['nome'=>$nf->nome,'id'=>$nf->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    /**
     * Verifica se ja existe uma nota fiscal para um determinado atendimento
     *
     * @param Integer $atendimento_id
     * @return void
     */
    public function verificaNfs($atendimento_id)
    {
        $count_nfs = $this->Nfs->find('all')->where(['Nfs.ficha' => $atendimento_id])->limit(1)->count();
        $prestadores = $this->Nfs->NfPrestadores->find('list')->Where(['NfPrestadores.situacao_id' => 1])
            ->andWhere(['NfPrestadores.emissor_atual' => 1]);
        $atendimento = $this->Nfs->Atendimentos->get($atendimento_id);
        
        $json = ['count_nfs' => $count_nfs, 'prestadores' => $prestadores, 'total_liquido' => $atendimento->total_liquido];

        $json_data = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }

    public function saveNfs($atendimento_id)
    {
        $this->loadModel('Atendimentos');
        $this->loadModel('NfTomadores');
        $this->loadModel('NfServicos');

        $atendimento = $this->Atendimentos->get($atendimento_id, [
            'contain' => ['Clientes', 'AtendimentoProcedimentos' => ['Procedimentos']]
        ]);

        $prestador = $this->Nfs->NfPrestadores->get($this->request->getData('prestador'));

        $tomador = $this->NfTomadores->newEntity();
        $tomador->cliente_id = $atendimento->cliente_id;
        if($this->NfTomadores->save($tomador)){
            $nfs = $this->Nfs->newEntity();
            $nfs->prestador_id = $prestador->id;
            $nfs->cnae = $prestador->default_cod_atividade;
            $nfs->descricao_cnae = $prestador->default_nome_atividade;
            $nfs->tributacao_id = $prestador->default_tributacao;
            $nfs->aliquota_atividade = $prestador->default_aliquota_atividade;
            $nfs->tomador_id = $tomador->id;
            $nfs->ficha = $atendimento_id;
            $nfs->valor_total_servicos = $atendimento->total_liquido;
            $this->Nfs->save($nfs);
            if(!empty($atendimento->atendimento_procedimentos)){
                foreach($atendimento->atendimento_procedimentos as $ap){
                    $servico = $this->NfServicos->newEntity();
                    $servico->tributavel = $prestador->default_servicos_tributacao;
                    $servico->nota_fiscal_id = $nfs->id;
                    $servico->descricao = $ap->procedimento->nome;
                    $servico->quantidade = $ap->quantidade;
                    $servico->valor_unitario = $ap->quantidade;
                    $servico->valor_total = $ap->valor_caixa / $ap->quantidade;
                    $this->NfServicos->save($servico);
                }
            }
        }

        $json_data = json_encode($atendimento_id);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }
}
