<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HistoricoItenArtigos Controller
 *
 * @property \App\Model\Table\HistoricoItenArtigosTable $HistoricoItenArtigos
 */
class HistoricoItenArtigosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['AtendimentoitenArtigos', 'SituacaoCadastros', 'Users']
        ];
        $historicoItenArtigos = $this->paginate($this->HistoricoItenArtigos);


        $this->set(compact('historicoItenArtigos'));
        $this->set('_serialize', ['historicoItenArtigos']);

    }

    /**
     * View method
     *
     * @param string|null $id Historico Iten Artigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $historicoItenArtigo = $this->HistoricoItenArtigos->get($id, [
            'contain' => ['AtendimentoitenArtigos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('historicoItenArtigo', $historicoItenArtigo);
        $this->set('_serialize', ['historicoItenArtigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $historicoItenArtigo = $this->HistoricoItenArtigos->newEntity();
        if ($this->request->is('post')) {
            $historicoItenArtigo = $this->HistoricoItenArtigos->patchEntity($historicoItenArtigo, $this->request->data);
            if ($this->HistoricoItenArtigos->save($historicoItenArtigo)) {
                $this->Flash->success(__('O historico iten artigo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O historico iten artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('historicoItenArtigo'));
        $this->set('_serialize', ['historicoItenArtigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Historico Iten Artigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $historicoItenArtigo = $this->HistoricoItenArtigos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $historicoItenArtigo = $this->HistoricoItenArtigos->patchEntity($historicoItenArtigo, $this->request->data);
            if ($this->HistoricoItenArtigos->save($historicoItenArtigo)) {
                $this->Flash->success(__('O historico iten artigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O historico iten artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('historicoItenArtigo'));
        $this->set('_serialize', ['historicoItenArtigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Historico Iten Artigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $historicoItenArtigo = $this->HistoricoItenArtigos->get($id);
                $historicoItenArtigo->situacao_id = 2;
        if ($this->HistoricoItenArtigos->save($historicoItenArtigo)) {
            $this->Flash->success(__('O historico iten artigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O historico iten artigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Historico Iten Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->HistoricoItenArtigos->find('all')
        ->where(['HistoricoItenArtigos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('HistoricoItenArtigos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Historico Iten Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $historicoItenArtigo = $this->HistoricoItenArtigos->get($this->request->data['id']);
            $res = ['nome'=>$historicoItenArtigo->nome,'id'=>$historicoItenArtigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
