<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinGrupos Controller
 *
 * @property \App\Model\Table\FinGruposTable $FinGrupos
 */
class FinGruposController extends AppController
{
    public $components = ['FinGrupoContabilidades'];

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Grupos');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finGrupos = $this->FinGrupos->find()->contain(['FinGrupoContabilidades' => ['FinContabilidades']]);
        $id = !empty($this->request->query(['grupo'])) ? $this->request->query(['grupo']) : null;

        if (isset($id)) $finGrupos = $finGrupos->where(['FinGrupos.id' => $id]);
        
        $finGrupos = $this->paginate($finGrupos);

        $this->set(compact('finGrupos'));
        $this->set('_serialize', ['finGrupos']);
    }

    /**
     * View method
     *
     * @param string|null $id Fin Grupo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finGrupo = $this->FinGrupos->get($id, [
            'contain' => []
        ]);

        $this->set('finGrupo', $finGrupo);
        $this->set('_serialize', ['finGrupo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finGrupo = $this->FinGrupos->newEntity();
        if ($this->request->is('post')) {
            $newGroup = $this->request->data;
            $companies = $newGroup['empresas'];
            unset($newGroup['empresas']);
            $newGroup['situacao'] = 1;
            $finGrupo = $this->FinGrupos->patchEntity($finGrupo, $newGroup);
            if ($this->FinGrupos->save($finGrupo)) {
                if ($this->FinGrupoContabilidades->addCompanies($finGrupo['id'], $companies)) {
                    $this->Flash->success(__('O grupo foi salvo com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->FinGrupos->delete($finGrupo);
                    $this->Flash->error(__('Ocorreu um erro ao cadastrar o grupo de empresas. Por favor, tente novamente.'));    
                }
            } else {
                $this->Flash->error(__('O grupo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finGrupo'));
        $this->set('_serialize', ['finGrupo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Grupo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finGrupo = $this->FinGrupos->get($id, [
            'contain' => []
        ]);
        $contabilidades = $this->FinGrupos->FinGrupoContabilidades->FinContabilidades->find('list');

        $finContabilidades = $this->FinGrupos->FinGrupoContabilidades->find()->select(['fin_contabilidade_id'])->where(['fin_grupo_id' => $finGrupo->id]);
        $idsContabilidades = [];
        foreach($finContabilidades as $finContabilidade)
        {
            $idsContabilidades[$finContabilidade->fin_contabilidade_id] = $finContabilidade->fin_contabilidade_id;
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $newGroup = $this->request->data;
            $companies = $newGroup['empresas'];
            unset($newGroup['empresas']);
            $newGroup['situacao'] = 1;
            $finGrupo = $this->FinGrupos->patchEntity($finGrupo, $newGroup);
            if ($this->FinGrupos->save($finGrupo)) {
                if ($this->FinGrupoContabilidades->editCompanies($finGrupo['id'], $companies)) {
                    $this->Flash->success(__('O grupo foi salvo com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    $this->FinGrupos->delete($finGrupo);
                    $this->Flash->error(__('Ocorreu um erro ao cadastrar o grupo de empresas. Por favor, tente novamente.'));    
                }
            } else {
                $this->Flash->error(__('O grupo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finGrupo', 'contabilidades', 'idsContabilidades'));
        $this->set('_serialize', ['finGrupo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Grupo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finGrupo = $this->FinGrupos->get($id);
        $finGrupo->situacao = 2;
        if ($this->FinGrupos->save($finGrupo)) {
            $this->Flash->success(__('O grupo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Grupo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinGrupos->find('all')
        ->where(['FinGrupos.descricao LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinGrupos.descricao');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->descricao);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Grupo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finGrupo = $this->FinGrupos->get($this->request->data['id']);
            $res = ['nome'=>$finGrupo->nome,'id'=>$finGrupo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
