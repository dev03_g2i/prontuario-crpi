<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinClassificacaoContas Controller
 *
 * @property \App\Model\Table\FinClassificacaoContasTable $FinClassificacaoContas
 */
class FinClassificacaoContasController extends AppController
{

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Classificação de contas');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $description = !empty($this->request->query(['descricao'])) ? $this->request->query(['descricao']) : null;

        $finClassificacaoContas = $this->FinClassificacaoContas->find();
        
        if (isset($description))
            $finClassificacaoContas->where(['FinClassificacaoContas.descricao LIKE' => '%'.$description.'%']);

        $finClassificacaoContas = $this->paginate($finClassificacaoContas);

        $this->set(compact('finClassificacaoContas'));
        $this->set('_serialize', ['finClassificacaoContas']);

    }

    /**
     * View method
     *
     * @param string|null $id Fin Classificacao Conta id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finClassificacaoConta = $this->FinClassificacaoContas->get($id, [
            'contain' => ['FinPlanoContas']
        ]);

        $this->set('finClassificacaoConta', $finClassificacaoConta);
        $this->set('_serialize', ['finClassificacaoConta']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finClassificacaoConta = $this->FinClassificacaoContas->newEntity();
        if ($this->request->is('post')) {
            $finClassificacaoConta = $this->FinClassificacaoContas->patchEntity($finClassificacaoConta, $this->request->data);
            if ($this->FinClassificacaoContas->save($finClassificacaoConta)) {
                $this->Flash->success(__('A classificação de conta foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A classificação de conta não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finClassificacaoConta'));
        $this->set('_serialize', ['finClassificacaoConta']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Classificacao Conta id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finClassificacaoConta = $this->FinClassificacaoContas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finClassificacaoConta = $this->FinClassificacaoContas->patchEntity($finClassificacaoConta, $this->request->data);
            if ($this->FinClassificacaoContas->save($finClassificacaoConta)) {
                $this->Flash->success(__('A classificação de conta foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A classificação de conta não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finClassificacaoConta'));
        $this->set('_serialize', ['finClassificacaoConta']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Classificacao Conta id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finClassificacaoConta = $this->FinClassificacaoContas->get($id);
                $finClassificacaoConta->situacao = 2;
        if ($this->FinClassificacaoContas->save($finClassificacaoConta)) {
            $this->Flash->success(__('A classificação de conta foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! A classificação de conta não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Classificacao Conta id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinClassificacaoContas->find('all')
        ->where(['FinClassificacaoContas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinClassificacaoContas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Classificacao Conta id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finClassificacaoConta = $this->FinClassificacaoContas->get($this->request->data['id']);
            $res = ['nome'=>$finClassificacaoConta->nome,'id'=>$finClassificacaoConta->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
