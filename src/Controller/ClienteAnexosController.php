<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClienteAnexos Controller
 *
 * @property \App\Model\Table\ClienteAnexosTable $ClienteAnexos
 */
class ClienteAnexosController extends AppController
{

    public $components = array('FileUpload');
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clientes', 'TipoAnexos', 'Users', 'SituacaoCadastros']
                        ,'conditions' => ['ClienteAnexos.situacao_id = ' => '1']
                    ];
        $clienteAnexos = $this->paginate($this->ClienteAnexos);

        $this->set(compact('clienteAnexos'));
        $this->set('_serialize', ['clienteAnexos']);
    }

    public function all()
    {
        $query = $this->ClienteAnexos->find('all')
            ->contain(['Clientes', 'TipoAnexos'])
            ->where(['ClienteAnexos.situacao_id = ' => '1']);

        if(!empty($this->request->query['cliente_id'])){
            $query->andWhere(['ClienteAnexos.cliente_id'=>$this->request->query['cliente_id']]);
            $cliente_id = $this->request->query['cliente_id'];
        }else{
            $cliente_id = null;
        }

        $clienteAnexos = $this->paginate($query);

        $this->set(compact('clienteAnexos','cliente_id'));
        $this->set('_serialize', ['clienteAnexos']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente Anexo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteAnexo = $this->ClienteAnexos->get($id, [
            'contain' => ['Clientes', 'TipoAnexos', 'Users', 'SituacaoCadastros','ListaAnexos']
        ]);

        $this->set('clienteAnexo', $clienteAnexo);
        $this->set('_serialize', ['clienteAnexo']);
    }

    public function galeria($id = null)
    {
        $query = $this->ClienteAnexos->find('all')
            ->contain(['Clientes', 'TipoAnexos', 'Users', 'SituacaoCadastros','ListaAnexos'])
            ->where(['ClienteAnexos.situacao_id' => 1])
            ->andWhere(['ClienteAnexos.cliente_id' => $id])
            ->orderDesc('ClienteAnexos.created');

        $clienteAnexoImages = $this->ClienteAnexos->getClienteAnexosPerTipo(1, $id);
        $clienteAnexoExames = $this->ClienteAnexos->getClienteAnexosPerTipo(2, $id);
        $clienteAnexoDocs = $this->ClienteAnexos->getClienteAnexosPerTipo(3, $id);

        $clienteAnexos = $query;
        $checkListaAnexos = $query->count();

        // Contadores
        $countImagens = $query->andWhere(['ClienteAnexos.tipoanexo_id' => 1])->count();
        $countExames = $query->andWhere(['ClienteAnexos.tipoanexo_id' => 2])->count();
        $countDocumentos = $query->andWhere(['ClienteAnexos.tipoanexo_id' => 3])->count();

        $this->set(compact('clienteAnexos', 'checkListaAnexos', 'clienteAnexoImages', 'clienteAnexoExames', 'clienteAnexoDocs', 'countImagens', 'countExames', 'countDocumentos'));
    }

    public function compareImage($id = null)
    {
        $this->viewBuilder()->layout('compare_image');
        $lista_anexos = $this->ClienteAnexos->ListaAnexos->find('all')
            ->where(['ListaAnexos.situacao_id' => 1])
            ->andWhere(['ListaAnexos.anexo_id' => $id]);

        $position = 0;
        if(!empty($this->request->query('position'))){
            $position = $this->request->query('position');
        }

        $this->set(compact('lista_anexos', 'position'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($cliente)
    {
        $clienteAnexo = $this->ClienteAnexos->newEntity();
        if ($this->request->is('post')) {
            $clienteAnexo = $this->ClienteAnexos->patchEntity($clienteAnexo, $this->request->data);
            if ($this->ClienteAnexos->save($clienteAnexo)) {
                $numFile= count(array_filter($_FILES['caminho']['name']));
                $file = $_FILES['caminho'];
                for($i = 0; $i < $numFile; $i++){
                    $this->loadModel('ListaAnexos');
                    $dados = array(
                        "name"      => $file['name'][$i],
                        "type"      => $file['type'][$i],
                        "tmp_name"  => $file['tmp_name'][$i],
                        "error"     => $file['error'][$i],
                        "size"      => $file['size'][$i]
                    );
                    $listaAnexo = $this->ListaAnexos->newEntity();
                    $this->request->data['anexo_id'] = $clienteAnexo->id;
                    $this->request->data['caminho'] = $dados;
                    $this->request->data['descricao_anexo'] = $file['name'][$i];
                    $listaAnexo = $this->ListaAnexos->patchEntity($listaAnexo, $this->request->data);
                    $this->ListaAnexos->save($listaAnexo);
                }
                echo $cliente;
            }else{
                echo -1;
            }
            $this->autoRender = false;
        }
        $clientes = $this->ClienteAnexos->Clientes->find('list', ['limit' => 200]);
        $tipoAnexos = $this->ClienteAnexos->TipoAnexos->find('list', ['limit' => 200]);
        $this->set(compact('clienteAnexo', 'clientes', 'tipoAnexos', 'cliente'));
        $this->set('_serialize', ['clienteAnexo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Anexo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null,$cliente)
    {
        $clienteAnexo = $this->ClienteAnexos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteAnexo = $this->ClienteAnexos->patchEntity($clienteAnexo, $this->request->data);
            if ($this->ClienteAnexos->save($clienteAnexo)) {
                echo $cliente;
            } else {
                echo -1;
            }
            $this->autoRender = false;
        }
        $clientes = $this->ClienteAnexos->Clientes->find('list', ['limit' => 200]);
        $tipoAnexos = $this->ClienteAnexos->TipoAnexos->find('list', ['limit' => 200]);
        $this->set(compact('clienteAnexo', 'clientes', 'tipoAnexos', 'cliente'));
        $this->set('_serialize', ['clienteAnexo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Anexo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $clienteAnexo = $this->ClienteAnexos->get($id);
                $clienteAnexo->situacao_id = 2;
        if ($this->ClienteAnexos->save($clienteAnexo)) {
            $this->Flash->success(__('O cliente anexo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente anexo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }



}
