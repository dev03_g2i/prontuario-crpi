<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoCadastros Controller
 *
 * @property \App\Model\Table\SituacaoCadastrosTable $SituacaoCadastros
 */
class SituacaoCadastrosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $situacaoCadastros = $this->paginate($this->SituacaoCadastros);

        $this->set(compact('situacaoCadastros'));
        $this->set('_serialize', ['situacaoCadastros']);
    }

    /**
     * View method
     *
     * @param string|null $id Situacao Cadastro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoCadastro = $this->SituacaoCadastros->get($id, [
            'contain' => []
        ]);

        $this->set('situacaoCadastro', $situacaoCadastro);
        $this->set('_serialize', ['situacaoCadastro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoCadastro = $this->SituacaoCadastros->newEntity();
        if ($this->request->is('post')) {
            $situacaoCadastro = $this->SituacaoCadastros->patchEntity($situacaoCadastro, $this->request->data);
            if ($this->SituacaoCadastros->save($situacaoCadastro)) {
                $this->Flash->success(__('O situacao cadastro foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao cadastro não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('situacaoCadastro'));
        $this->set('_serialize', ['situacaoCadastro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Cadastro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoCadastro = $this->SituacaoCadastros->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoCadastro = $this->SituacaoCadastros->patchEntity($situacaoCadastro, $this->request->data);
            if ($this->SituacaoCadastros->save($situacaoCadastro)) {
                $this->Flash->success(__('O situacao cadastro foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao cadastro não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->set(compact('situacaoCadastro'));
        $this->set('_serialize', ['situacaoCadastro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Cadastro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoCadastro = $this->SituacaoCadastros->get($id);
                if ($this->SituacaoCadastros->delete($situacaoCadastro)) {
        $this->Flash->success(__('O situacao cadastro foi deletado com sucesso.'));
        } else {
        $this->Flash->error(__('Desculpe! O situacao cadastro não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
