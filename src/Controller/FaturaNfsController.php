<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaNfs Controller
 *
 * @property \App\Model\Table\FaturaNfsTable $FaturaNfs
 */
class FaturaNfsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->FaturaNfs->find('all')
                ->contain(['FaturaEncerramentos' => ['Convenios'], 'SituacaoCadastros', 'Users', 'Unidades'])
                ->where(['FaturaNfs.situacao_id' => 1])
                ->orderAsc('FaturaNfs.data');

        $dados = $query->first();
        $convenio = ($dados->fatura_encerramento->convenio->nome)?$dados->fatura_encerramento->convenio->nome:'';
        $fatura_id = null;
        if(!empty($this->request->query('fatura_id'))){
            $fatura_id = $this->request->query('fatura_id');
            $query->andWhere(['FaturaNfs.fatura_encerramento_id' => $fatura_id]);
        }
        $unidade_id = null;
        if(!empty($this->request->query('unidade_id'))){
            $unidade_id = $this->request->query('unidade_id');
        }

        $faturaNfs = $this->paginate($query);

        $this->set(compact('faturaNfs', 'fatura_id', 'unidade_id', 'convenio'));
        $this->set('_serialize', ['faturaNfs']);

    }

    /**
     * View method
     *
     * @param string|null $id Fatura Nf id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaNf = $this->FaturaNfs->get($id, [
            'contain' => ['FaturaEncerramentos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('faturaNf', $faturaNf);
        $this->set('_serialize', ['faturaNf']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fatura_encerramento_id= null;
        if(!empty($this->request->query('fatura_id'))){
            $fatura_encerramento_id = $this->request->query('fatura_id');
        }
        $unidade_id = null;
        if(!empty($this->request->query('unidade_id'))){
            $unidade_id = $this->request->query('unidade_id');
        }

        $faturaNf = $this->FaturaNfs->newEntity();
        if ($this->request->is('post')) {
            $faturaNf = $this->FaturaNfs->patchEntity($faturaNf, $this->request->data);

            if ($this->FaturaNfs->save($faturaNf)) {
                $this->Flash->success(__('O fatura nf foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura nf não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaNf', 'fatura_encerramento_id', 'unidade_id'));
        $this->set('_serialize', ['faturaNf']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Nf id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaNf = $this->FaturaNfs->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaNf = $this->FaturaNfs->patchEntity($faturaNf, $this->request->data);
            if ($this->FaturaNfs->save($faturaNf)) {
                $this->Flash->success(__('O fatura nf foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura nf não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaNf'));
        $this->set('_serialize', ['faturaNf']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Nf id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faturaNf = $this->FaturaNfs->get($id);
                $faturaNf->situacao_id = 2;
        if ($this->FaturaNfs->save($faturaNf)) {
            $this->Flash->success(__('O fatura nf foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura nf não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fatura Nf id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaturaNfs->find('all')
        ->where(['FaturaNfs.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaNfs.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fatura Nf id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faturaNf = $this->FaturaNfs->get($this->request->data['id']);
            $res = ['nome'=>$faturaNf->nome,'id'=>$faturaNf->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function deleteModal()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $faturaNfs = $this->FaturaNfs->get($this->request->data['id']);
        $faturaNfs->situacao_id = 2;
        if ($this->FaturaNfs->save($faturaNfs)) {
            $res = 0;
        } else {
            $res = 1;
        }

        $json = json_encode(['res' => $res, 'view' => 'index']);
        $this->response->body($json);
    }
}
