<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;
/**
 * FaceArtigos Controller
 *
 * @property \App\Model\Table\FaceArtigosTable $FaceArtigos
 */
class FaceArtigosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $session = new Session();
        if(!empty($session->read('artigos'))){
            $session->delete('artigos');
        }

        $vinculos =[];
        $itens_vinculos = $session->read('retorno_faces');
        if(!empty($itens_vinculos)){
            foreach ($itens_vinculos as $itens_vinculo) {
                $vinculos[]= $itens_vinculo['iten_id'];
            }
        }

        $this->paginate = [
            'contain' => ['FaceItens', 'ProcedimentoGastos', 'Artigos'=>['strategy'=>'select']]
        ];

        $query = [];
        $faceiten_id = null;
        if(!empty($this->request->query['faceiten_id'])){
            $faceiten_id = $this->request->query['faceiten_id'];
            $query['conditions'][]= ['FaceArtigos.face_itens_id'=>$faceiten_id];
        }

        $this->paginate = array_merge($this->paginate,$query);
        $faceArtigos = $this->paginate($this->FaceArtigos);

        $is_historico=null;
        if(!empty($this->request->query['historico'])){
            $is_historico = $this->request->query['historico'];
        }

        $this->set(compact('faceArtigos','faceiten_id','vinculos','is_historico'));
        $this->set('_serialize', ['faceArtigos']);

    }

    function all(){
        $atendimento_iten = $this->request->query['atendimentoiten_id'];
        $this->loadModel('AtendimentoItens');
        $atendimento_itens = $this->AtendimentoItens->get($atendimento_iten, [
            'contain' => [
                'Dentes', 'Regioes',
                'FaceItens'=>['Faces'],
                'AtendimentoProcedimentos'=>['Procedimentos','MedicoResponsaveis']]
        ]);

        $is_prontuario= false;
        if(!empty($this->request->query['pront'])){
            $is_prontuario = true;
        }
        $is_historico=null;
        if(!empty($this->request->query['historico'])){
            $is_historico = $this->request->query['historico'];
        }
        $this->set(compact('atendimento_itens','is_prontuario','is_historico'));
        $this->set('_serialize', ['atendimento_itens']);
    }

    /**
     * View method
     *
     * @param string|null $id Face Artigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faceArtigo = $this->FaceArtigos->get($id, [
            'contain' => ['FaceItens', 'ProcedimentoGastos', 'Artigos'=>['strategy'=>'select'], 'Users', 'SituacaoCadastros']
        ]);

        $this->set('faceArtigo', $faceArtigo);
        $this->set('_serialize', ['faceArtigo']);
    }


    public function vincular(){
        $session = new Session();
        $retorno = empty($session->read('retorno_faces')) ? [] : $session->read('retorno_faces');
        $add = false;
        if(!empty($retorno)){
            foreach ($retorno as $item => $value) {
                if($this->request->data['id']==$value['iten_id']){
                    $add= true;
                    unset($retorno[$item]);
                }
            }
        }

        if(!$add){
            $retorno[] = [
                'iten_id' => $this->request->data['id']
            ];
        }
        $session->delete('retorno_faces');
        $session->write('retorno_faces',$retorno);
        $this->autoRender=false;
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $faceArtigo = $this->FaceArtigos->newEntity();
        if ($this->request->is('post')) {
            $session = new Session();
            $artigos = $session->read('artigos');
            $res = ['msg'=>'Artigos salvos com sucesso!'];

            $retorno = empty($session->read('retorno_faces')) ? [] : $session->read('retorno_faces');
            if(!empty($artigos)) {
                foreach ($artigos as $i => $item) {
                    $faceArtigo = $this->FaceArtigos->newEntity();
                    $this->request->data['procedimento_gasto_id'] = !empty($item['procedimento_gasto_id']) ?$item['procedimento_gasto_id'] :null;
                    $this->request->data['artigo_id'] = $item['artigo_id'];
                    $this->request->data['quantidade'] = $item['quantidade'];
                    $faceArtigo = $this->FaceArtigos->patchEntity($faceArtigo, $this->request->data);
                    if (!$this->FaceArtigos->save($faceArtigo)) {
                        $res[]=['msg'=>$faceArtigo->artigo_id.' não salvo!'];
                    }
                    $retorno[] = [
                        'iten_id' => $faceArtigo->id
                    ];
                }
                $session->delete('retorno_faces');
                $session->write('retorno_faces',$retorno);

                echo json_encode($res);
            }
        }
        $face_itens= null;
        $gastos=[];
        if(!empty($this->request->query['faceiten_id'])){
            $this->loadModel('FaceItens');
            $face_itens = $this->FaceItens->get($this->request->query['faceiten_id'],['contain'=>['AtendimentoItens'=>['AtendimentoProcedimentos']]]);

            $this->loadModel('ProcedimentoGastos');
            $procedimento_gastos = $this->ProcedimentoGastos->find('all')
                ->contain(['Gastos'=>['strategy' => 'select']])
                ->where(['ProcedimentoGastos.procedimento_id'=>$face_itens->atendimento_iten->atendimento_procedimento->procedimento_id]);
            if(!empty($procedimento_gastos)){
                foreach ($procedimento_gastos as $pg) {
                    $gastos[$pg->id] =$pg->gasto->nome;
                }
            }
        }

        $this->set(compact('faceArtigo','gastos','face_itens','procedimento_gastos'));
        $this->set('_serialize', ['faceArtigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Face Artigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faceArtigo = $this->FaceArtigos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faceArtigo = $this->FaceArtigos->patchEntity($faceArtigo, $this->request->data);
            if ($this->FaceArtigos->save($faceArtigo)) {
                $this->Flash->success(__('O face artigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O face artigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faceArtigo'));
        $this->set('_serialize', ['faceArtigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Face Artigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faceArtigo = $this->FaceArtigos->get($id);
                $faceArtigo->situacao_id = 2;
        if ($this->FaceArtigos->save($faceArtigo)) {
            $this->Flash->success(__('O face artigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O face artigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Face Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaceArtigos->find('all')
        ->where(['FaceArtigos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaceArtigos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Face Artigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faceArtigo = $this->FaceArtigos->get($this->request->data['id']);
            $res = ['nome'=>$faceArtigo->nome,'id'=>$faceArtigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function artigos()
    {
        $session = new Session();
        $artigos = empty($session->read('artigos')) ? array() : $session->read('artigos');

        $add = false;

        if (!empty($this->request->query['artigo_id'])) {
            if (!empty($artigos)) {
                foreach ($artigos as $i => $item) {
                    if ($this->request->query['artigo_id'] == $item['artigo_id']) {
                        $add = true;
                    }
                }
            }

            if (!$add) {
                $this->loadModel('EstqArtigo');
                $artigo = $this->EstqArtigo->findById($this->request->query['artigo_id'])->first();
                if(!empty($artigo)){
                    $artigos[] = [
                        'procedimento_gasto_id'=>null,
                        'artigo_id'=>$artigo->id,
                        'artigo_codigo'=>$artigo->codigo_livre,
                        'artigo_nome'=>$artigo->nome,
                        'quantidade'=>!empty($this->request->query['quantidade']) ? $this->request->query['quantidade'] : 0

                    ];
                }
            }
        }

        if (!empty($this->request->query['procedimento_gasto_id'])) {
            $this->loadModel('ProcedimentoGastos');
            $procedimento_gasto = $this->ProcedimentoGastos->findById($this->request->query['procedimento_gasto_id'])->first();

            if(!empty($procedimento_gasto)) {
                $this->loadModel('GastoItens');
                $gasto_itens = $this->GastoItens->find('all')
                    ->contain(['EstqArtigo'])
                    ->where(['GastoItens.gasto_id' => $procedimento_gasto->gasto_id]);

                $ids=[];
                if(!empty($artigos)) {
                    foreach ($artigos as $i => $item) {
                        $ids[] =$item['artigo_id'];
                    }
                }

                if (!empty($gasto_itens)) {
                    foreach ($gasto_itens as $gasto_iten) {
                        if (!in_array($gasto_iten->estq_artigo->id,$ids)) {
                            $artigos[] = [
                                'procedimento_gasto_id' => $procedimento_gasto->id,
                                'artigo_id' => $gasto_iten->estq_artigo->id,
                                'artigo_codigo' => $gasto_iten->estq_artigo->codigo_livre,
                                'artigo_nome' => $gasto_iten->estq_artigo->nome,
                                'quantidade' => !empty($this->request->query['quantidade']) ? $this->request->query['quantidade'] : $gasto_iten->quantidade
                            ];
                        }
                    }
                }
            }
        }
        $session->delete('artigos');
        $session->write('artigos',$artigos);

        $this->set(compact('artigos'));
        $this->set('_serialize', ['artigos']);

    }

    public function dellArtigo()
    {
        $this->viewBuilder()->layout('ajax');
        $session = new Session();
        $artigos = $session->read('artigos');
        unset($artigos[$this->request->query['pos']]);
        $art_novo =[];
        $result = array_merge($artigos,$art_novo);
        $session->delete('artigos');
        $session->write('artigos',$result);
        $this->response->type('json');
        $this->response->body(json_encode($result));
        $this->autoRender=false;
    }

    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name'])){
            $name = $this->request->data['name'];
            $session = new Session();
            $artigos = $session->read('artigos');
            $artigos[$this->request->data['pk']][$name] =  $this->request->data['value'];

            $session->delete('artigos');
            $session->write('artigos',$artigos);
            $this->response->type('json');
            $res = ['res' => $this->request->data['value'],'msg'=>'Dados salvos com sucesso!'];
        }
        $this->response->body(json_encode($res));
    }
}
