<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SolicitanteCbo Controller
 *
 * @property \App\Model\Table\SolicitanteCboTable $SolicitanteCbo
 */
class SolicitanteCboController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    // public function index()
    // {
    //     $this->paginate = [
    //         'contain' => ['SituacaoCadastros']
    //     ];
    //     $solicitanteCbo = $this->paginate($this->SolicitanteCbo);


    //     $this->set(compact('solicitanteCbo'));
    //     $this->set('_serialize', ['solicitanteCbo']);

    // }

    /**
     * View method
     *
     * @param string|null $id Solicitante Cbo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function view($id = null)
    // {
    //     $solicitanteCbo = $this->SolicitanteCbo->get($id, [
    //         'contain' => ['SituacaoCadastros']
    //     ]);

    //     $this->set('solicitanteCbo', $solicitanteCbo);
    //     $this->set('_serialize', ['solicitanteCbo']);
    // }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $solicitanteCbo = $this->SolicitanteCbo->newEntity();
    //     if ($this->request->is('post')) {
    //         $solicitanteCbo = $this->SolicitanteCbo->patchEntity($solicitanteCbo, $this->request->data);
    //         if ($this->SolicitanteCbo->save($solicitanteCbo)) {
    //             $this->Flash->success(__('O solicitante cbo foi salvo com sucesso!'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('O solicitante cbo não foi salvo. Por favor, tente novamente.'));
    //         }
    //     }

    //     $this->set(compact('solicitanteCbo'));
    //     $this->set('_serialize', ['solicitanteCbo']);
    // }

    /**
     * Edit method
     *
     * @param string|null $id Solicitante Cbo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $solicitanteCbo = $this->SolicitanteCbo->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $solicitanteCbo = $this->SolicitanteCbo->patchEntity($solicitanteCbo, $this->request->data);
    //         if ($this->SolicitanteCbo->save($solicitanteCbo)) {
    //             $this->Flash->success(__('O solicitante cbo foi salvo com sucesso.'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('O solicitante cbo não foi salvo. Por favor, tente novamente.'));
    //         }
    //     }

    //     $this->set(compact('solicitanteCbo'));
    //     $this->set('_serialize', ['solicitanteCbo']);
    // }

    /**
     * Delete method
     *
     * @param string|null $id Solicitante Cbo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function delete($id = null)
    // {
    //     $solicitanteCbo = $this->SolicitanteCbo->get($id);
    //             $solicitanteCbo->situacao_id = 2;
    //     if ($this->SolicitanteCbo->save($solicitanteCbo)) {
    //         $this->Flash->success(__('O solicitante cbo foi deletado com sucesso.'));
    //     } else {
    //         $this->Flash->error(__('Desculpe! O solicitante cbo não foi deletado! Tente novamente mais tarde.'));
    //     }
    //             return $this->redirect(['action' => 'index']);
    // }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Solicitante Cbo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->SolicitanteCbo->find('all')
        ->where(['SolicitanteCbo.descricao LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('SolicitanteCbo.descricao');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id2' => $d->id, 'text' => $d->descricao);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Solicitante Cbo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $solicitanteCbo = $this->SolicitanteCbo->get($this->request->data['id']);
            $res = ['nome'=>$solicitanteCbo->descricao,'id'=>$solicitanteCbo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
