<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Banco Controller
 *
 * @property \App\Model\Table\BancoTable $Banco
 */
class BancoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $banco = $this->paginate($this->Banco);


        $this->set(compact('banco'));
        $this->set('_serialize', ['banco']);

    }

    /**
     * View method
     *
     * @param string|null $id Banco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $banco = $this->Banco->get($id, [
            'contain' => ['Contabilidade', 'Movimento']
        ]);

        $this->set('banco', $banco);
        $this->set('_serialize', ['banco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $banco = $this->Banco->newEntity();
        if ($this->request->is('post')) {
            $banco = $this->Banco->patchEntity($banco, $this->request->data);
            if ($this->Banco->save($banco)) {
                $this->Flash->success(__('O banco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O banco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('banco'));
        $this->set('_serialize', ['banco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Banco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $banco = $this->Banco->get($id, [
            'contain' => ['Contabilidade', 'Movimento']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $banco = $this->Banco->patchEntity($banco, $this->request->data);
            if ($this->Banco->save($banco)) {
                $this->Flash->success(__('O banco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O banco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('banco'));
        $this->set('_serialize', ['banco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Banco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $banco = $this->Banco->get($id);
                $banco->situacao_id = 2;
        if ($this->Banco->save($banco)) {
            $this->Flash->success(__('O banco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O banco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Banco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Banco->find('all')
        ->where(['Banco.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Banco.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Banco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $banco = $this->Banco->get($this->request->data['id']);
            $res = ['nome'=>$banco->nome,'id'=>$banco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
