<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Network\Session;
use Cake\Utility\Xml;
use DateTime;

/**
 * Atendimentos Controller
 *
 * @property \App\Model\Table\AtendimentosTable $Atendimentos
 */
class AtendimentosController extends AppController
{

    public $components = ['Data', 'Json', 'Atendimento', 'Configuracao', 'Relatorios', 'Faturamento'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {

        $this->loadModel('Convenios');
        $this->loadModel('TipoAtendimentos');
        $query = $this->Atendimentos->find('all')
            ->leftJoinWith('AtendimentoProcedimentos')
            ->group(['Atendimentos.id'])
            ->contain(['Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos', 'SituacaoCadastros', 'Users', 'AtendimentoProcedimentos'])
            ->where(['Atendimentos.situacao_id ' => '1', 'Atendimentos.tipo ' => '1']);

        if (!empty($this->request->query['clientes'])) {
            $this->loadModel('Clientes');
            $clientes = $this->Clientes->get($this->request->query['clientes']);
            $query->andWhere(['Atendimentos.cliente_id ' => $this->request->query['clientes']]);
        } else {
            $clientes = null;
        }
        if (!empty($this->request->query['data'])) {
            $query->andWhere(['Atendimentos.data ' => $this->Data->DataSQL($this->request->query['data'])]);
        }
        if (!empty($this->request->query['convenio_id'])) {
            $query->andWhere(['Atendimentos.convenio_id ' => $this->request->query['convenio_id']]);
        }
        if (!empty($this->request->query['tipoatendimento_id'])) {
            $query->andWhere(['Atendimentos.tipoatendimento_id ' => $this->request->query['tipoatendimento_id']]);
        }

        if (!empty($this->request->query['acertado'])) {
            $acertato = ($this->request->query['acertado'] == 2) ? 0 : 1;
            $query->andWhere(['Atendimentos.acertado' => $acertato]);
        }
        if (!empty($this->request->query['finalizado'])) {
            if ($this->request->query['finalizado'] == 3) {
                debug($this->request->query['finalizado']);
                $query->andWhere(['Atendimentos.finalizado IN' => [0, 1]]);
            } else {
                $finalizado = ($this->request->query['finalizado'] == 2) ? 0 : 1;
                $query->andWhere(['Atendimentos.finalizado' => $finalizado]);
            }

        } else {
            $query->andWhere(['Atendimentos.finalizado' => 0]);
        }
        $convenio = $this->Convenios->find('list')->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $tipoatendimento = $this->TipoAtendimentos->find('list')->where(['TipoAtendimentos.situacao_id' => 1]);

        if (!empty($this->request->query('numero_atendimento'))) {
            $query->andWhere(['Atendimentos.id' => $this->request->query('numero_atendimento')]);
        }
        if (!empty($this->request->query('exame_integracao'))) {
            $query->andWhere(['AtendimentoProcedimentos.id' => $this->request->query('exame_integracao')]);
        }
        if (empty($this->request->query['sort'])) {
            $query->orderDesc('Atendimentos.id');
        }

        $atendimentos = $this->paginate($query);
        $title = "Atendimentos - Lista";

        $this->set(compact('atendimentos', 'clientes', 'convenio', 'tipoatendimento', 'title'));
        $this->set('_serialize', ['atendimentos']);

    }

    public function clientes()
    {
        $this->loadModel('Convenios');
        $this->loadModel('TipoAtendimentos');
        $query = $this->Atendimentos->find('all')
            ->contain(['Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos', 'SituacaoCadastros', 'Users'])
            ->where(['Atendimentos.situacao_id ' => '1']);
        if (!empty($this->request->query['clientes'])) {
            $this->loadModel('Clientes');
            $clientes = $this->Clientes->get($this->request->query['clientes']);
            $query->andWhere(['Atendimentos.cliente_id ' => $this->request->query['clientes']]);
        } else {
            $clientes = null;
        }
        if (!empty($this->request->query['data'])) {
            $query->andWhere(['Atendimentos.data ' => $this->Data->DataSQL($this->request->query['data'])]);
        }
        if (!empty($this->request->query['convenio_id'])) {
            $query->andWhere(['Atendimentos.convenio_id ' => $this->request->query['convenio_id']]);
        }
        if (!empty($this->request->query['tipoatendimento_id'])) {
            $query->andWhere(['Atendimentos.tipoatendimento_id ' => $this->request->query['tipoatendimento_id']]);
        }

        if (isset($this->request->query['tipo'])) {
            if ($this->request->query['tipo'] != "") {
                $query->andWhere(['Atendimentos.tipo ' => $this->request->query['tipo']]);
            }
        }

        if (isset($this->request->query['acertado'])) {
            if ($this->request->query['acertado'] == 0 || $this->request->query['acertado'] == 1) {
                $query->andWhere(['Atendimentos.acertado ' => $this->request->query['acertado']]);
            }
        }
        $convenio = $this->Convenios->find('list')->where(['Convenios.situacao_id' => 1]);
        $tipoatendimento = $this->TipoAtendimentos->find('list')->where(['TipoAtendimentos.situacao_id' => 1]);

        $atendimentos = $this->paginate($query);
        $this->set(compact('atendimentos', 'clientes', 'convenio', 'tipoatendimento'));
        $this->set('_serialize', ['atendimentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => ['InternacaoCaraterAtendimento', 'InternacaoAcomodacao', 'InternacaoSetor', 'InternacaoLeito', 'InternacaoMotivoAlta', 'Users', 'ConfiguracaoPeriodos', 'Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos', 'AtendimentoProcedimentos' => ['MedicoResponsaveis', 'Procedimentos', 'SituacaoFaturas', 'ControleFinanceiro'], 'Solicitantes'],
        ]);

        $session = new Session();
        $open_fin = $session->read('open_fin');
        $session->delete('open_fin');
        $this->set('atendimento', $atendimento);
        $this->set('open_fin', $open_fin);
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atendimento = $this->Atendimentos->newEntity();
        $this->loadModel('Procedimentos');
        $this->loadModel('MedicoResponsaveis');
        $this->loadModel('ConfiguracaoPeriodos');
        $periodos = $this->ConfiguracaoPeriodos->find('all');
        $agenda = null;
        $error = 0;

        $solicitante_id = null;
        if (!empty($this->request->query['agenda'])) {
            $agenda = $this->Atendimentos->Agendas->get($this->request->query['agenda'], [
                'contain' => ['GrupoAgendas'],
            ]);
            $solicitante_id = $agenda->grupo_agenda->solicitante_id;
        }

        if ($this->request->is('post')) {
            if (!empty($_SESSION['itens'])) {
                $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
                $res = ['res' => '2', 'msg' => 'Erro ao salvar atendimento!'];
                $atendimento->desconto = 0.00;
                $atendimento->total_geral = !empty($this->request->getData('total_liquido')) ? $this->request->getData('total_liquido') : 0.00;

                if (!empty($atendimento->internacao_previa_data)) {
                    $atendimento->internacao_previa_data = $this->Data->DataSQL($atendimento->internacao_previa_data);
                }

                if (!empty($atendimento->internacao_data_hora_entrada)) {
                    $atendimento->internacao_data_hora_entrada = $this->Data->DateTimeSql($atendimento->internacao_data_hora_entrada);
                }

                if ($this->Atendimentos->save($atendimento)) {
                    $error = 0;
                    foreach ($periodos as $p) {
                        if ($atendimento->created->format('H:i:s') >= $p->inicio_atendimento->format('H:i:s')
                        && $atendimento->created->format('H:i:s') <= $p->fim_atendimento->format('H:i:s')
                        ) {
                            $atendimento->configuracao_periodo_id = $p->id;
                            $this->Atendimentos->save($atendimento);
                            break;
                        }
                    }

                    $this->loadModel('AtendimentoProcedimentos');
                    $atendimentoProcedimentos = $this->AtendimentoProcedimentos->newEntities($_SESSION['itens']);
                    foreach ($atendimentoProcedimentos as $AtendProc) {
                        $AtendProc->atendimento_id = $atendimento->id;
                        $AtendProc->solicitante_id = !empty($AtendProc->solicitante_id) ? $AtendProc->solicitante_id : $atendimento->solicitante_id;
                        $AtendProc->prev_entrega_proc = ($this->Configuracao->showPrevEntrega()) ? $this->Atendimento->calculaPrevEntrega($AtendProc->procedimento_id) : null;
                        $AtendProc->situacao_laudo_id = 100;
                        if ($this->AtendimentoProcedimentos->save($AtendProc)) {
                            $error = 0;
                        } else {
                            $error += 1;
                        }
                        $this->Atendimento->gerarXML($AtendProc->id, 'Insert');
                    }
                    unset($_SESSION['itens']);

                    if (!empty($this->request->query['agenda'])) {
                        $agenda->situacao_agenda_id = 12;
                        $agenda->chegada = date('Y-m-d H:i:s');
                        $agenda->convenio_id = $atendimento->convenio_id;
                        if ($this->Atendimentos->Agendas->save($agenda)) {
                            $error = 0;
                            $res = ['res' => 1, 'msg' => $atendimento->id];
                        } else {
                            $error += 1;
                        }
                    }

                    $this->Atendimento->sortProcedimentosPerTotal($atendimento->id);

                } else {
                    $error += 1;
                    $this->Flash->error(__('O Atedimento não foi salvo. Por favor, tente novamente.'));
                }

            } else {
                $this->Flash->error(__('Não é permitido cadastrar atendimento sem procedimentos!'));
                $res = ['res' => '2', 'msg' => 'Não é permitido cadastrar atendimento sem procedimentos!'];
            }

            if (!empty($this->request->getQuery('agenda'))) {
                /* verificação feita apenas para o calendario */
                $res['valor_liquido'] = $this->request->getData('total_liquido') > 0;
                $json = json_encode($res);
                $response = $this->response->withType('json')->withStringBody($json);
                return $response;
            } elseif ($error == 0) {
                $this->Flash->success(__('O Atedimento foi salvo com sucesso!'));
                if ($this->request->getData('total_liquido') > 0) {
                    $session = new Session();
                    $session->write('open_fin', 1);
                }
                return $this->redirect(['action' => 'view', $atendimento->id]);
            }

        } else {
            unset($_SESSION['itens']);
        }

        $procedimentos = $this->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
        $medicos = $this->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200])->orderAsc('TipoAtendimentos.nome');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $acomodacoes = $this->Atendimentos->InternacaoAcomodacao->find('list', ['limit' => 200]);
        $caraterAtendimentos = $this->Atendimentos->InternacaoCaraterAtendimento->find('list', ['limit' => 200]);
        $this->set(compact('atendimento', 'unidades', 'convenios', 'tipoAtendimentos', 'procedimentos', 'medicos', 'agenda', 'origens', 'acomodacoes', 'caraterAtendimentos', 'solicitante_id'));
        $this->set('_serialize', ['atendimento']);
    }

    /* Foi mandado para action add - Luciano 16/11/2017 13:57 */
    /*public function cadastrar()
    {
    $atendimento = $this->Atendimentos->newEntity();
    $this->loadModel('ConfiguracaoPeriodos');
    $this->loadModel('Procedimentos');
    $this->loadModel('MedicoResponsaveis');
    $periodos = $this->ConfiguracaoPeriodos->find('all');
    $agenda = null;
    if (!empty($this->request->query['agenda'])) {
    $agenda = $this->Atendimentos->Agendas->get($this->request->query['agenda']);
    }

    if ($this->request->is('post')) {
    if (!empty($_SESSION['itens'])) {
    $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
    $res = ['res' => '2', 'msg' => 'Erro ao salvar atendimento!'];
    if ($this->Atendimentos->save($atendimento)) {
    foreach ($periodos as $p) {
    if ($atendimento->created->format('H:i:s') >= $p->inicio_atendimento->format('H:i:s')
    && $atendimento->created->format('H:i:s') <= $p->fim_atendimento->format('H:i:s')
    ) {
    $atendimento->configuracao_periodo_id = $p->id;
    $this->Atendimentos->save($atendimento);
    break;
    }
    }
    $this->loadModel('AtendimentoProcedimentos');

    foreach ($_SESSION['itens'] as $i => $item) {
    $AtendProc = $this->AtendimentoProcedimentos->newEntity();
    $AtendProc->procedimento_id = $item['procedimento_id'];
    $AtendProc->atendimento_id = $atendimento->id;
    $AtendProc->valor_base = $item['valor_base'];
    $AtendProc->valor_fatura = $item['valor_faturar'];
    $AtendProc->quantidade = $item['quantidade'];
    $AtendProc->desconto = $this->request->data['desconto'];
    if ($this->request->data['total_geral'] > 0) {
    $AtendProc->porc_desconto = number_format((float)($this->request->data['desconto'] * 100) / $this->request->data['total_geral'], 2, '.', '');
    } else {
    $AtendProc->porc_desconto = 0;
    }
    $AtendProc->valor_caixa = $item['valor_caixa'];
    $AtendProc->vl_caixa_original = $item['vl_caixa_original'];
    $AtendProc->digitado = 'Sim';
    $AtendProc->medico_id = $item['medico_id'];
    $AtendProc->complemento = $item['complemento'];
    $AtendProc->valor_matmed = $item['valor_faturar'];
    $AtendProc->controle = $item['controle'];
    $AtendProc->codigo = $item['codigo'];
    $AtendProc->regra = $item['regra'];
    $this->AtendimentoProcedimentos->save($AtendProc);
    $this->Atendimento->gerarXML($AtendProc->id);
    }
    unset($_SESSION['itens']);

    if (!empty($this->request->query['agenda'])) {
    $agenda->situacao_agenda_id = 12;
    $agenda->chegada = date('Y-m-d H:i:s');
    $this->Atendimentos->Agendas->save($agenda);
    }
    $res = ['res' => 1, 'msg' => $atendimento->id];
    }
    } else {
    $res = ['res' => '2', 'msg' => 'Não é permitido cadastrar atendimento sem procedimentos!'];
    }
    $this->autoRender = false;
    $this->response->type('json');
    $this->response->body(json_encode($res));

    } else {
    unset($_SESSION['itens']);
    }

    $procedimentos = $this->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
    $medicos = $this->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
    $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
    $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
    $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200])->orderAsc('TipoAtendimentos.nome');
    $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');

    $this->set(compact('atendimento', 'unidades', 'convenios', 'tipoAtendimentos', 'procedimentos', 'medicos', 'agenda', 'origens', 'fechar_modal'));
    $this->set('_serialize', ['atendimento']);
    }*/

    /**
     *
     *  retirado em 09-11-2017 - rodrigo - nao usa era oçamento
     *
     * public function nadd()
     * {
     * $atendimento = $this->Atendimentos->newEntity();
     * $this->loadModel('Procedimentos');
     * $this->loadModel('MedicoResponsaveis');
     * if ($this->request->is('post')) {
     * if ($this->request->data['orcamento_id']) {
     * $check = 0;
     * //agora pego o orcamento e gero um Atedimento
     * $orcamento = $this->Atendimentos->get($this->request->data['orcamento_id'],
     * ['contain' => ['AtendimentoProcedimentos' => ['AtendimentoItens' => ['FaceItens']]]]);
     *
     *
     * $valor = 0;
     * $ids_procs = array();
     *
     * foreach ($orcamento->atendimento_procedimentos as $at) {
     * foreach ($at->atendimento_itens as $ate_iten) {
     * if (in_array($ate_iten->id, $this->request->data['ids'])) {
     * $ids_procs[] = $at->id;
     * if (empty($at->valor_base)) {
     * $valor += (float)$at->valor_fatura;
     * } else {
     * $valor += (float)$ate_iten->valor_faturar;
     * }
     *
     * }
     * }
     * }
     * $atendimento->hora = $orcamento->hora;
     * $atendimento->data = date('Y-m-d');
     * $atendimento->observacao = $orcamento->observacao;
     * $atendimento->cliente_id = $orcamento->cliente_id;
     * $atendimento->unidade_id = $orcamento->unidade_id;
     *
     * $liquido = (float)$valor - (float)$orcamento->desconto - (float)$orcamento->total_pagoato;
     *
     * $atendimento->total_liquido = (float)$liquido; //checar valores
     * $atendimento->desconto = $orcamento->desconto; //checar valores
     * $atendimento->total_geral = (float)$valor; //checar valores
     * $atendimento->convenio_id = $orcamento->convenio_id;
     * $atendimento->total_pagoato = $orcamento->total_pagoato; //checar valores
     * $atendimento->tipoatendimento_id = $orcamento->tipoatendimento_id;
     * $atendimento->num_ficha_externa = $orcamento->num_ficha_externa;
     * $atendimento->tipo = 1;
     * $atendimento->id_old = $orcamento->id;
     * $atendimento->acertado = 0;
     * if ($this->Atendimentos->save($atendimento)) {
     *
     * $this->loadModel('AtendimentoProcedimentos');
     * foreach ($orcamento->atendimento_procedimentos as $at) {
     * if (in_array($at->id, $ids_procs)) {
     * $AtendProc = $this->AtendimentoProcedimentos->newEntity();
     * $AtendProc->procedimento_id = $at->procedimento_id;
     * $AtendProc->atendimento_id = $atendimento->id;
     * $AtendProc->valor_base = $at->valor_base; // confirmar valores
     * $count_itens = 0;
     * foreach ($at->atendimento_itens as $ate_iten) {
     * if (in_array($ate_iten->id, $this->request->data['ids'])) {
     * if (!empty($ate_iten->face_itens)) {
     * foreach ($ate_iten->face_itens as $fc) {
     * $count_itens++;
     * }
     * } else {
     * $count_itens++;
     * }
     * }
     * }
     * $AtendProc->valor_fatura = !empty($at->valor_base) ? ($at->valor_base * $count_itens) : $at->valor_fatura; // confirmar valores
     * $AtendProc->quantidade = $at->quantidade;
     * $AtendProc->desconto = $at->desconto;// confirmar valores
     * $AtendProc->porc_desconto = $at->porc_desconto;
     * $AtendProc->valor_caixa = $at->valor_caixa;// confirmar valores
     * $AtendProc->digitado = $at->digitado;
     * $AtendProc->material = $at->material;
     * $AtendProc->num_controle = $at->num_controle;
     * $AtendProc->medico_id = $at->medico_id;
     * $AtendProc->valor_matmed = $at->valor_matmed;
     * $AtendProc->documento_guia = $at->documento_guia;
     * if ($this->AtendimentoProcedimentos->save($AtendProc)) {
     * $this->Atendimento->gerarXML($AtendProc->id);
     * $this->loadModel('AtendimentoItens');
     * foreach ($at->atendimento_itens as $ate_iten) {
     * //copio os atendimento itens para o Atedimento, mas comparando com os ids enviados
     * if (in_array($ate_iten->id, $this->request->data['ids'])) {
     * //aqui seto como realizado no orçamento
     * $atendimento_itens = $this->AtendimentoItens->get($ate_iten->id);
     * $atendimento_itens->status = 1;
     * $this->AtendimentoItens->save($atendimento_itens);
     * //aqui gero outro iten para novo Atedimento
     * $itens = $this->AtendimentoItens->newEntity();
     * $itens->tipo = $ate_iten->tipo;
     * $itens->descricao = $ate_iten->descricao;
     * $itens->valor_faturar = $ate_iten->valor_faturar;
     * $itens->atendproc_id = $AtendProc->id;
     * $itens->status = 1;
     * $itens->id_old = $ate_iten->id;
     * if ($this->AtendimentoItens->save($itens)) {
     * $this->loadModel('FaceItens');
     * foreach ($ate_iten->face_itens as $fc) {
     * $faceitens = $this->FaceItens->newEntity();
     * $faceitens->iten_id = $itens->id;
     * $faceitens->face_id = $fc->face_id;
     * $this->FaceItens->save($faceitens);
     * }
     * } else {
     * $check = 1;
     * }
     *
     * //aqui verifico se todos os itens do orçamento foram enviados para Atedimento e de acordo com o retorno seto o statos do orcamento
     * $restantes = $this->Atendimentos->getItens($orcamento->id);
     * if (!empty($restantes)) {
     * $orcamento->status = $restantes;
     * $this->Atendimentos->save($orcamento);
     * }
     *
     * }
     * }
     * } else {
     * $check = 1;
     * }
     * }
     * }
     * } else {
     * $check = 1;
     * }
     * } else {
     * $check = 1;
     * }
     * if ($check == 0) {
     * echo $atendimento->id;
     * } else {
     * echo 'erro';
     * }
     * $this->autoRender = false;
     * }
     *
     *
     * $procedimentos = $this->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
     * $medicos = $this->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
     * $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome');
     * $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
     * $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200])->orderAsc('TipoAtendimentos.nome');
     * $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
     *
     * $this->set(compact('atendimento', 'unidades', 'convenios', 'tipoAtendimentos', 'procedimentos', 'medicos', 'origens'));
     * $this->set('_serialize', ['atendimento']);
     * }
     */

    /**
     *
     *
     * Edit method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agenda = null;
        if (!empty($this->request->getQuery('agenda'))) {
            $agenda = $this->Atendimentos->Agendas->get($this->request->getQuery('agenda'));
        }
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => ['Users', 'UserEdit', 'ConfiguracaoPeriodos', 'AtendimentoProcedimentos'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);

            if (!empty($atendimento->internacao_previa_data)) {
                $atendimento->internacao_previa_data = $this->Data->DataSQL($atendimento->internacao_previa_data);
            }

            if (!empty($atendimento->internacao_data_hora_entrada)) {
                $atendimento->internacao_data_hora_entrada = $this->Data->DateTimeSql($atendimento->internacao_data_hora_entrada);
            }

            if (!empty($atendimento->internacao_data_hora_saida)) {
                $atendimento->internacao_data_hora_saida = $this->Data->DateTimeSql($atendimento->internacao_data_hora_saida);
            }

            if ($this->Atendimentos->save($atendimento)) {
                /* Pode ter mais de um procedimento cadastrado para um atendimento */
                foreach ($atendimento->atendimento_procedimentos as $atendProc) {
                    $this->Atendimento->gerarXML($atendProc->id, 'Edit');
                }
                $this->Flash->success(__('O atendimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $clientes = $this->Atendimentos->Clientes->find('list')->where(['Clientes.id' => $atendimento->cliente_id]);
        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200]);
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200]);
        $configPeriodos = $this->Atendimentos->ConfiguracaoPeriodos->find('list', ['limit' => 200])->orderAsc('ConfiguracaoPeriodos.id');
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $acomodacoes = $this->Atendimentos->InternacaoAcomodacao->find('list', ['limit' => 200]);
        $caraterAtendimentos = $this->Atendimentos->InternacaoCaraterAtendimento->find('list', ['limit' => 200]);
        $motivosAlta = $this->Atendimentos->InternacaoMotivoAlta->find('list', ['limit' => 200]);
        $internacaoSetores = $this->Atendimentos->InternacaoSetor->find('list', ['limit' => 200]);
        $internacaoLeitos = $this->Atendimentos->InternacaoLeito->find('list', ['limit' => 200]);
        $this->set(compact('atendimento', 'clientes', 'unidades', 'convenios', 'tipoAtendimentos', 'origens', 'configPeriodos', 'agenda', 'acomodacoes', 'caraterAtendimentos', 'motivosAlta', 'internacaoSetores', 'internacaoLeitos'));
        $this->set('_serialize', ['atendimento']);
    }

    public function editar($id = null)
    {
        $atendimento = $this->Atendimentos->get($id, [
            'contain' => ['Users', 'UserEdit'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimento = $this->Atendimentos->patchEntity($atendimento, $this->request->data);
            if ($this->Atendimentos->save($atendimento)) {
                $this->Atendimento->sortProcedimentosPerTotal($atendimento->id);
                $this->Flash->success(__('O atendimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('MedicoResponsaveis');
        $medicos = $this->MedicoResponsaveis->find('list')->orderAsc('MedicoResponsaveis.nome');
        $clientes = $this->Atendimentos->Clientes->find('list')->where(['Clientes.id' => $atendimento->cliente_id]);
        $unidades = $this->Atendimentos->Unidades->find('list', ['limit' => 200]);
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $tipoAtendimentos = $this->Atendimentos->TipoAtendimentos->find('list', ['limit' => 200]);
        $origens = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');

        $this->set(compact('atendimento', 'clientes', 'unidades', 'convenios', 'tipoAtendimentos', 'medicos', 'origens'));
        $this->set('_serialize', ['atendimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $atendimento = $this->Atendimentos->get($id);
        $atendimento->situacao_id = 2;
        if ($this->Atendimentos->save($atendimento)) {
            $atendimento = $this->Atendimentos->get($id, ['contain' => ['ContasReceber']]);
            if (!empty($atendimento->contas_receber)) {
                foreach ($atendimento->contas_receber as $conta) {
                    $this->loadModel('ContasReceber');
                    $receber = $this->ContasReceber->get($conta->id);
                    $receber->status_id = 3;
                    $this->ContasReceber->save($receber);
                }
            }
            $this->Flash->success(__('O atendimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O atendimento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function proc()
    {
        $this->viewBuilder()->layout('ajax');
        if (!isset($_SESSION['itens'])) {
            $_SESSION['itens'] = array();
        }

        $id_procedimento = $this->request->query['procedimento_id'];
        $itens = $_SESSION['itens'];
        $this->set(compact('itens', 'id_procedimento'));
        $this->set('_serialize', ['itens']);
    }

    /* Foi mandado para action edit - Luciano 16/11/2017 14:00 */
    /*public function procedit()
    {
    $this->viewBuilder()->layout('ajax');
    $erro = 'erro';
    $id_procedimento = null;

    $atendimentoProcedimento = $this->AtendimentoProcedimentos->newEntity();
    if($this->request->is('get')){
    $this->loadModel('PrecoProcedimentos');
    $this->loadModel('Procedimentos');
    $this->loadModel('Convenios');
    $this->loadModel('MedicoResponsaveis');
    $this->loadModel('AtendimentoProcedimentos');

    $Preco = $this->PrecoProcedimentos;
    $Procedimento = $this->Procedimentos;
    $Convenios = $this->Convenios;
    $Medicos = $this->MedicoResponsaveis;

    $query = $Preco->find('all')->where([
    'convenio_id =' => $this->request->query['convenio_id'],
    'procedimento_id =' => $this->request->query['procedimento_id'],
    'situacao_id' => 1
    ]);
    $preco_procedimento = $query->first();
    $Medicos = $Medicos->get((int)$this->request->query['medico_id']);
    $atendimentoProcedimento = $this->AtendimentoProcedimentos->patchEntity($atendimentoProcedimento, $this->request->data);
    $quantidade = !empty($this->request->data['quantidade']) ? $this->request->data['quantidade'] : 1;
    $atendimentoProcedimento->medico_id = $Medicos->id;
    $atendimentoProcedimento->procedimento_id = $this->request->query['procedimento_id'];
    $atendimentoProcedimento->atendimento_id = $this->request->query['atendimento_id'];
    $atendimentoProcedimento->valor_base = !empty($preco_procedimento->valor_faturar) ? $preco_procedimento->valor_faturar : 0.00;
    $atendimentoProcedimento->valor_fatura = !empty($preco_procedimento->valor_faturar) ? number_format($preco_procedimento->valor_faturar * $quantidade, 2, '.', '') : 0.00;
    $atendimentoProcedimento->quantidade = $quantidade;
    $atendimentoProcedimento->desconto = 0.00;
    $atendimentoProcedimento->porc_desconto = 0;
    $atendimentoProcedimento->valor_caixa = !empty($preco_procedimento->valor_particular) ? number_format($preco_procedimento->valor_particular * $quantidade, 2, '.', '') : 0.00;
    $atendimentoProcedimento->vl_caixa_original = !empty($preco_procedimento->valor_particular) ? number_format($preco_procedimento->valor_particular * $quantidade, 2, '.', '') : 0.00;
    $atendimentoProcedimento->digitado = 'Sim';
    $atendimentoProcedimento->valor_matmed = !empty($preco_procedimento->valor_faturar) ? $preco_procedimento->valor_faturar : 0.00;
    $atendimentoProcedimento->complemento = $this->request->query['complemento'];
    $atendimentoProcedimento->controle = $this->request->query['controle'];
    $atendimentoProcedimento->codigo = $this->request->query['codigo'];

    if ($this->AtendimentoProcedimentos->save($atendimentoProcedimento)) {
    $this->Atendimento->gerarXML($atendimentoProcedimento->id);
    $erro = $atendimentoProcedimento->atendimento_id;
    }
    }

    echo $erro;
    $this->autoRender = false;
    }*/

    public function removeitem()
    {
        $this->viewBuilder()->layout('ajax');
        unset($_SESSION['itens'][$this->request->query['pos']]);
        /* Ordena os procedimentos de acordo com as regras */
        $procedimentos = $_SESSION['itens'];
        unset($_SESSION['itens']);
        $sort_procedimentos = $this->Atendimento->sortOrdenacao($procedimentos);
        /*****************************************************/
        foreach ($sort_procedimentos as $r => $item) {
            $_SESSION['itens'][] = array(
                'ordenacao' => $item['ordenacao'],
                'procedimento_id' => $item['procedimento_id'],
                'convenio_id' => $item['convenio_id'],
                'medico_id' => $item['medico_id'],
                'medico_nome' => $item['medico_nome'],
                'valor_base' => $item['valor_base'],
                'valor_fatura' => $item['valor_fatura'],
                'valor_caixa' => $item['valor_caixa'],
                'preco_id' => $item['preco_id'],
                'procedimento' => $item['procedimento'],
                'controle' => $item['controle'],
                'complemento' => $item['complemento'],
                'quantidade' => $item['quantidade'],
                'convenio' => $item['convenio'],
                'cobrancas' => $item['cobrancas'],
                'desconto' => $item['desconto'],
                'prev_entrega_proc' => $item['prev_entrega_proc'],
                'codigo' => $item['codigo'],
                'regra' => $item['regra'],
                'filme_reais' => $item['filme_reais'],
                'uco' => $item['uco'],
                'porte' => $item['porte'],
                'filme' => $item['filme'],
            );
        }
        exit;
    }

    public function delledit()
    {
        $this->viewBuilder()->layout('ajax');
        unset($_SESSION['itens']);
        exit;
    }

    public function base()
    {
        $this->autoRender = false;
        $_SESSION['itens'][$_POST['pk']]['valor_base'] = $_POST['value'];
        $this->response->type('json');
        $this->response->body(json_encode(['res' => 1]));
    }

    public function editTable()
    {
        $this->autoRender = false;
        $name = $this->request->data['name'];
        $pk = $this->request->data['pk'];
        $limite_desconto_perc = $this->Auth->user('limite_desconto');
        $error = '';
        $msg = '';

        if ($name == 'valor_base') {
            $_SESSION['itens'][$pk]['valor_fatura'] = $this->request->data['value'];
        }

        if ($name == 'quantidade') {
            $faturar_old = $_SESSION['itens'][$pk]['valor_fatura'] / (($_SESSION['itens'][$pk]['quantidade'] > 0) ? $_SESSION['itens'][$pk]['quantidade'] : 1);
            $caixa_old = $_SESSION['itens'][$pk]['valor_caixa'] / (($_SESSION['itens'][$pk]['quantidade'] > 0) ? $_SESSION['itens'][$pk]['quantidade'] : 1);

            $_SESSION['itens'][$pk]['valor_fatura'] = $faturar_old * $this->request->data['value'];
            $_SESSION['itens'][$pk]['valor_caixa'] = $caixa_old * $this->request->data['value'];
        } elseif ($name == 'desconto') {
            $desconto_maximo = number_format($_SESSION['itens'][$pk]['vl_caixa_original'] * $limite_desconto_perc / 100, 2, '.', '');
            $desconto_aplicado = $this->request->data['value'];
            if ($desconto_maximo >= $desconto_aplicado) {
                $_SESSION['itens'][$pk]['valor_caixa'] = $_SESSION['itens'][$pk]['vl_caixa_original'] - $desconto_aplicado;
            } else {
                $error = 'desconto';
                $msg = 'O desconto aplicado ultrapassa o limite do dado ao usuário.';
            }
        }

        $_SESSION['itens'][$pk][$name] = $this->request->data['value'];
        $this->response->type('json');
        $this->response->body(json_encode(['res' => 1, 'error' => $error, 'msg' => $msg]));
    }

    public function parcial()
    {
        $this->autoRender = false;
        $_SESSION['itens'][$_POST['pk']]['valor_base'] = $_POST['value'];
        $this->response->type('json');
        $this->response->body(json_encode(['res' => 1]));
    }

    public function complemento()
    {
        $this->autoRender = false;
        $_SESSION['itens'][$_POST['pk']]['complemento'] = $_POST['value'];
        $this->response->type('json');
        $this->response->body(json_encode(['res' => 1]));
    }

    public function faturar()
    {
        $this->autoRender = false;
        $_SESSION['itens'][$_POST['pk']]['valor_fatura'] = $_POST['value'];
        $this->response->type('json');
        $this->response->body(json_encode(['res' => 1]));
    }

    public function convenios()
    {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('Clientes');
        $cliente = $this->Clientes;
        $clientes = $cliente->get((int) $this->request->data['id'], [
            'contain' => 'Convenios',
        ]);

        if (!empty($clientes)) {
            $idade = $this->Data->calculaIdade($clientes->nascimento);
            if (!empty($clientes->telefone)) {
                $fone = $clientes->telefone;
            } else {
                $fone = $clientes->celular;
            }
            $convenio = $clientes->convenio_id;
            echo json_encode(['convenio_id' => $convenio, 'idade' => $idade, 'fone' => $fone]);
        } else {
            echo -1;
        }

        exit;
    }

    public function getvalores()
    {
        $this->viewBuilder()->layout('ajax');
        $this->autoRender = false;
        $valor = 0;
        if (!empty($_SESSION['itens'])) {
            foreach ($_SESSION['itens'] as $i => $item) {
                $valor += (float) $item['valor_caixa'];
            }
        }
        echo json_encode(number_format($valor, 2, '.', ''));
    }

    public function itens($procedimento)
    {
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('ItenCobrancas');
        $itens = $this->ItenCobrancas->find('all')
            ->contain(['Dentes', 'Regioes'])
            ->where(['ItenCobrancas.procedimento_id' => $procedimento]);

        $this->set(compact('itens'));
        $this->set('_serialize', ['itens']);
    }

    public function addregiao($id_session, $id_iten)
    {
        $id_session = count($_SESSION['itens']) - 1;
        $this->viewBuilder()->layout('ajax');
        $this->loadModel('ItenCobrancas');
        $ja = false;
        if (!empty($_SESSION['itens'][$id_session]['cobrancas'])) {
            foreach ($_SESSION['itens'][$id_session]['cobrancas'] as $cobranca => $value) {
                if ($id_iten == $value['id']) {
                    $ja = true;
                }
            }
        }
        if (!$ja) {
            $itens = $this->ItenCobrancas->get($id_iten,
                ['contain' => ['Dentes', 'Regioes']]);
            if ($itens->tipo == 'd') {
                $tipo = 'Dente';
                $descri = $itens->dente->descricao;
            } else {
                $descri = $itens->regio->nome;
                $tipo = 'Região';
            }
            $_SESSION['itens'][$id_session]['cobrancas'][$id_iten] =
            array(
                'id' => $itens->id,
                'tipo' => $tipo,
                'type' => $itens->tipo,
                'describle' => $itens->descricao,
                'valor_fatura' => 0,
                'descricao' => $descri,
                'faces' => null,
            );
        }

        $this->autoRender = false;
    }

    public function addlocal($id_session, $id_iten)
    {
        $id_session = count($_SESSION['itens']) - 1;

        $this->viewBuilder()->layout('ajax');
        $this->loadModel('ItenCobrancas');
        $this->loadModel('Faces');
        $faces = $this->Faces->find('all', ['where' => ['Faces.situacao_id' => 1]]);

        $ja = false;
        if (!empty($_SESSION['itens'][$id_session]['cobrancas'])) {
            foreach ($_SESSION['itens'][$id_session]['cobrancas'] as $cobranca => $value) {
                if ($id_iten == $value['id']) {
                    $ja = true;
                }
            }
        }

        if (!$ja) {
            $itens = $this->ItenCobrancas->get($id_iten,
                ['contain' => ['Dentes', 'Regioes']]);
            if ($itens->tipo == 'd') {
                $tipo = 'Dente';
                $descri = $itens->dente->descricao;
            } else {
                $descri = $itens->regio->nome;
                $tipo = 'Região';
            }
            $_SESSION['itens'][$id_session]['cobrancas'][$id_iten] =
            array(
                'id' => $itens->id,
                'tipo' => $tipo,
                'type' => $itens->tipo,
                'describle' => $itens->descricao,
                'descricao' => $descri,
                'valor_fatura' => 0,
                'faces' => null,
            );
        }

        $this->set(compact('faces', 'id_session', 'id_iten'));
        $this->set('_serialize', ['faces']);

    }

    public function addfaces()
    {
        $this->viewBuilder()->layout('ajax');
        $id_session = count($_SESSION['itens']) - 1;
        if (!empty($this->request->data['id_iten'])) {
            $id_iten = $this->request->data['id_iten'];
            $_SESSION['itens'][$id_session]['cobrancas'][$id_iten]['faces'] = $this->request->data['ids'];
        }
        $this->autoRender = false;
    }

    public function cancelar()
    {
        $atendimento = $this->Atendimentos->get($this->request->data['id'],
            ['contain' => ['AtendimentoProcedimentos' => ['AtendimentoItens' => ['FaceItens']]]]);

        foreach ($atendimento->atendimento_procedimentos as $at) {
            $this->loadModel('AtendimentoProcedimentos');
            foreach ($at->atendimento_itens as $ate_iten) {
                $this->loadModel('AtendimentoItens');
                if (!empty($ate_iten->face_itens)) {
                    $this->loadModel('FaceItens');
                    foreach ($ate_iten->face_itens as $item) {
                        $face = $this->FaceItens->get($item->id);
                        $face->situacao_id = 2;
                        $this->FaceItens->save($face);
                    }
                }

                $iten = $this->AtendimentoItens->findById($ate_iten->id_old)->first();
                if (!empty($iten)) {
                    $iten_old = $this->AtendimentoItens->get($ate_iten->id_old);
                    $iten_old->status = 0;
                    $this->AtendimentoItens->save($iten_old);
                }

                $itens = $this->AtendimentoItens->get($ate_iten->id);
                $itens->situacao_id = 2;
                $this->AtendimentoItens->save($itens);
            }

            $adendproc = $this->AtendimentoProcedimentos->get($at->id);
            $adendproc->situacao_id = 2;
            $this->AtendimentoProcedimentos->save($adendproc);
        }

        $atendimento = $this->Atendimentos->get($this->request->data['id']);
        $atendimento->situacao_id = 2;
        $this->Atendimentos->save($atendimento);

        $this->autoRender = false;
    }

    public function precoproc()
    {
        $this->loadModel('PrecoProcedimentos');

        $Preco = $this->PrecoProcedimentos;

        $query = $Preco->find('all')
            ->where(['convenio_id =' => $this->request->query['convenio_id'],
                'procedimento_id =' => $this->request->query['procedimento_id'],
                'situacao_id' => 1]);

        $preco_procedimento = $query->first();

        echo 1;
        $this->autoRender = false;
    }

    public function contrato()
    {
        $atendimento = $this->Atendimentos->get($this->request->query['id']);

        $this->loadModel('Contratos');
        $contrato = $this->Contratos->find('list')
            ->where(['Contratos.situacao_id' => 1])
            ->orderAsc('Contratos.nome');

        $this->set(compact('atendimento', 'contrato'));
        $this->set('_serialize', ['atendimento']);

    }

    public function executores($id)
    {
        $this->viewBuilder()->layout('ajax');
        $atendimento_procedimento = $this->Atendimentos->AtendimentoProcedimentos->get($id);

        $this->set(compact('atendimento_procedimento'));
        $this->set('_serialize', ['atendimento_procedimento']);
    }

    public function respFinanceiro()
    {
        $session = new Session();
        $financeiro_face = [];
        $financeiro_iten = [];
        $itens_vinculos = $session->read('retorno_update');

        if (!empty($itens_vinculos)) {
            foreach ($itens_vinculos as $itens_vinculo) {
                if ($itens_vinculo['origen'] == 'f') {
                    if (!empty($itens_vinculo['face_id'])) {
                        $financeiro_face[] = $itens_vinculo['face_id'];
                    } else {
                        $financeiro_iten[] = $itens_vinculo['iten_id'];
                    }
                }
            }
        }

        $id = $this->request->query['atendimento_procedimento'];
        $atendimento_procedimento = $this->Atendimentos->AtendimentoProcedimentos->get($id, [
            'contain' => ['Procedimentos', 'AtendimentoItens' => ['Dentes', 'Regioes', 'Financeiro', 'User_Financeiro']],
        ]);

        $is_historico = null;
        if (!empty($this->request->query['historico'])) {
            $is_historico = $this->request->query['historico'];
        }

        $this->set(compact('atendimento_procedimento', 'financeiro_face', 'financeiro_iten', 'is_historico'));
        $this->set('_serialize', ['atendimento_procedimento']);
    }

    public function respExecutor()
    {
        $session = new Session();
        $executor_face = [];
        $executor_iten = [];
        $itens_vinculos = $session->read('retorno_update');

        if (!empty($itens_vinculos)) {
            foreach ($itens_vinculos as $itens_vinculo) {
                if ($itens_vinculo['origen'] == 'e') {
                    if (!empty($itens_vinculo['face_id'])) {
                        $executor_face[] = $itens_vinculo['face_id'];
                    } else {
                        $executor_iten[] = $itens_vinculo['iten_id'];
                    }
                }
            }
        }

        $id = $this->request->query['atendimento_procedimento'];
        $atendimento_procedimento = $this->Atendimentos->AtendimentoProcedimentos->get($id, [
            'contain' => ['Procedimentos', 'MedicoResponsaveis', 'AtendimentoItens' => ['Dentes', 'Regioes', 'Executor', 'User_Executor', 'FaceItens' => ['Faces', 'Executor', 'User_Executor']]],
        ]);

        $is_historico = null;
        if (!empty($this->request->query['historico'])) {
            $is_historico = $this->request->query['historico'];
        }

        $this->set(compact('atendimento_procedimento', 'executor_face', 'executor_iten', 'is_historico'));
        $this->set('_serialize', ['atendimento_procedimento']);
    }

    public function parcialEdit()
    {
        $session = new Session();
        $this->autoRender = false;

        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if (!empty($this->request->data['name']) && !empty($this->request->data['value'])) {
            $name = $this->request->data['name'];
            $tipo = $this->request->data['tipo'];

            if ($tipo == 'face') {
                $this->loadModel('FaceItens');
                $itens = 'FaceItens';
                $contain = ['contain' => ['AtendimentoItens' => ['AtendimentoProcedimentos']]];
            } else {
                $this->loadModel('AtendimentoItens');
                $itens = 'AtendimentoItens';
                $contain = ['contain' => ['AtendimentoProcedimentos']];

            }

            $iten = $this->$itens->get($this->request->data['pk'], $contain);
            $iten->$name = $this->request->data['value'];
            if ($name == 'financeiro_id') {
                $iten->data_financeiro = date('Y-m-d H:i:s');
                $iten->user_financeiro = $this->Auth->user('id');
            } else {
                $iten->data_executor = date('Y-m-d H:i:s');
                $iten->user_executor = $this->Auth->user('id');
            }
            $url = $this->request->referer(true);
            $retorno = empty($session->read('retorno_update')) ? [] : $session->read('retorno_update');
            $this->response->type('json');
            if ($this->$itens->save($iten)) {

                if ($url == "/clientes") {
                    if ($tipo == 'face') {
                        $retorno[] = [
                            'referer' => $url,
                            'atendimento_id' => $iten->atendimento_iten->atendimento_procedimento->atendimento_id,
                            'atendimento_procedimento_id' => $iten->atendimento_iten->atendimento_procedimento->id,
                            'iten_id' => $iten->atendimento_iten->id,
                            'face_id' => $iten->id,
                            'tipo' => $tipo,
                            'origen' => $this->request->data['origen'],

                        ];
                    } else {
                        $retorno[] = [
                            'referer' => $url,
                            'atendimento_id' => $iten->atendimento_procedimento->atendimento_id,
                            'atendimento_procedimento_id' => $iten->atendimento_procedimento->id,
                            'iten_id' => $iten->id,
                            'face_id' => null,
                            'tipo' => $tipo,
                            'origen' => $this->request->data['origen'],
                        ];
                    }
                    $session->delete('retorno_update');
                    $session->write('retorno_update', $retorno);
                }

                $res = ['res' => $iten->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

    public function finalizarAtendimento()
    {
        $this->autoRender = false;
        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];
        if (!empty($this->request->data['name'])) {
            $atendimento = $this->Atendimentos->get($this->request->data['pk']);
            $atendimento->finalizado = $this->request->data['value'];
            $this->response->type('json');
            if ($this->Atendimentos->save($atendimento)) {
                $res = ['res' => $atendimento->finalizado, 'msg' => 'Dados salvos com sucesso!'];
            }
            $this->response->body(json_encode($res));
        }
    }

    public function parcialFinalizar()
    {
        $session = new Session();
        $this->autoRender = false;

        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if (!empty($this->request->data['name'])) {
            $name = $this->request->data['name'];
            $tipo = $this->request->data['tipo'];

            if ($tipo == 'face') {
                $this->loadModel('FaceItens');
                $itens = 'FaceItens';
                $contain = ['contain' => ['AtendimentoItens' => ['AtendimentoProcedimentos']]];
            } else {
                $this->loadModel('AtendimentoItens');
                $itens = 'AtendimentoItens';
                $contain = ['contain' => ['AtendimentoProcedimentos']];
            }

            $iten = $this->$itens->get($this->request->data['pk'], $contain);
            $iten->$name = $this->request->data['value'];

            $url = $this->request->referer(true);
            $retorno = empty($session->read('retorno_update')) ? [] : $session->read('retorno_update');

            $this->response->type('json');
            if ($this->$itens->save($iten)) {

                if ($url == "/clientes") {
                    if ($tipo == 'face') {
                        $retorno[] = [
                            'referer' => $url,
                            'atendimento_id' => $iten->atendimento_iten->atendimento_procedimento->atendimento_id,
                            'atendimento_procedimento_id' => $iten->atendimento_iten->atendimento_procedimento->id,
                            'iten_id' => $iten->atendimento_iten->id,
                            'face_id' => $iten->id,
                            'tipo' => $tipo,
                            'origen' => $this->request->data['origen'],
                        ];
                    } else {
                        $retorno[] = [
                            'referer' => $url,
                            'atendimento_id' => $iten->atendimento_procedimento->atendimento_id,
                            'atendimento_procedimento_id' => $iten->atendimento_procedimento->id,
                            'iten_id' => $iten->id,
                            'face_id' => null,
                            'tipo' => $tipo,
                            'origen' => $this->request->data['origen'],
                        ];
                    }
                    $session->write('retorno_update', $retorno);
                }

                $res = ['res' => $iten->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

    public function vincular()
    {
        $session = new Session();
        $retorno = empty($session->read('retorno_update')) ? [] : $session->read('retorno_update');
        $add = false;

        if (!empty($retorno)) {
            foreach ($retorno as $item => $value) {
                if ($this->request->data['tipo'] == 'face' && !empty($value['face_id'])) {
                    if ($this->request->data['id'] == $value['face_id']) {
                        $add = true;
                        unset($retorno[$item]);
                    }
                } else {
                    if ($this->request->data['id'] == $value['iten_id']) {
                        $add = true;
                        unset($retorno[$item]);
                    }
                }
            }
        }

        if ($this->request->data['tipo'] == 'face') {
            $this->loadModel('FaceItens');
            $face_itens = $this->FaceItens->get($this->request->data['id'],
                ['contain' => [
                    'AtendimentoItens' => ['AtendimentoProcedimentos'],
                ]]);
            if (!$add) {
                $retorno[] = [
                    'referer' => null,
                    'atendimento_id' => $face_itens->atendimento_iten->atendimento_procedimento->atendimento_id,
                    'atendimento_procedimento_id' => $face_itens->atendimento_iten->atendimento_procedimento->id,
                    'iten_id' => $face_itens->atendimento_iten->id,
                    'face_id' => $face_itens->id,
                    'tipo' => 'face',
                    'origen' => $this->request->data['origen'],
                ];
            }
        } else {
            $this->loadModel('AtendimentoItens');
            $atendimento_iten = $this->AtendimentoItens->get($this->request->data['id'], ['contain' => [
                'AtendimentoProcedimentos',
            ]]);
            if (!$add) {
                $retorno[] = [
                    'referer' => null,
                    'atendimento_id' => $atendimento_iten->atendimento_procedimento->atendimento_id,
                    'atendimento_procedimento_id' => $atendimento_iten->atendimento_procedimento->id,
                    'iten_id' => $atendimento_iten->id,
                    'face_id' => null,
                    'tipo' => 'dente',
                    'origen' => $this->request->data['origen'],
                ];
            }
        }

        $session->delete('retorno_update');
        $session->write('retorno_update', $retorno);
        $this->autoRender = false;

    }

    /**
     * View method
     *
     * @param string|null $id Atendimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function financeiroExecutor($id = null)
    {

        $atendimentos = $this->Atendimentos->find('all', [
            'contain' => ['Clientes', 'Unidades', 'Convenios', 'TipoAtendimentos', 'AtendimentoProcedimentos' => ['MedicoResponsaveis', 'Procedimentos', 'AtendimentoItens' => ['Dentes', 'Regioes', 'FaceItens' => ['Faces']]]],
        ])
            ->where(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.cliente_id' => $id])
            ->andWhere(['Atendimentos.finalizado' => 0]);
        $is_historico = null;
        if (!empty($this->request->query['historico'])) {
            $is_historico = $this->request->query['historico'];
        }
        $this->set(compact('atendimentos', 'is_historico'));
        $this->set('_serialize', ['atendimentos']);
    }

    public function executar($id)
    {
        $this->viewBuilder()->layout('ajax');
        $atendimento_procedimento = $this->Atendimentos->AtendimentoProcedimentos->get($id);
        $aba = $this->request->query['aba'];
        $is_historico = null;
        if (!empty($this->request->query['historico'])) {
            $is_historico = $this->request->query['historico'];
        }
        $this->set(compact('atendimento_procedimento', 'aba', 'is_historico'));
        $this->set('_serialize', ['atendimento_procedimento']);
    }

    public function checkAgenda()
    {
        $id = $this->request->data['id'];
        $atendimento = $this->Atendimentos->find('all')
            ->where(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.agenda_id' => $id]);

        $total = $atendimento->count();
        $this->loadModel('Agendas');
        $agenda = $this->Agendas->get($id);

        $this->loadModel('ConfiguracaoAgenda');
        $config_agenda = $this->ConfiguracaoAgenda->find()->first();
        $user_logado = $this->Auth->user('id');
        $agenda_reserva_liberacao = $this->Auth->user('agenda_reserva_liberacao');
        if ($config_agenda->reservado == 2 && $agenda->situacao_agenda_id == 1500) { // reservado
            if ($user_logado == $agenda->user_id && $agenda_reserva_liberacao == 2) {
                $regra_edit_reserva = ['valida' => true];
            } else {
                $regra_edit_reserva = ['valida' => false, 'msg' => 'Este horário esta RESERVADO!'];
            }
        } else {
            $regra_edit_reserva = ['valida' => true];
        }

        $atendimento = $atendimento->first();
        $this->response->type('json');
        $this->response->body(json_encode([
            'res' => $total,
            'paciente_id' => $agenda->cliente_id,
            'convenio_id' => $agenda->convenio_id,
            'atendimento_id' => (!empty($atendimento->id) ? $atendimento->id : null),
            'horario_gerado' => $agenda->agenda_horario_id,
            'situacao_agenda' => $agenda->situacao_agenda_id,
            'regra_edit_reserva' => $regra_edit_reserva,
        ]));
        $this->autoRender = false;

    }

    public function teste($id = null)
    {
        //TODO funcao matching
        $categoryId = 98;
        $query = $this->Atendimentos->find()
            ->matching(
                'AtendimentoProcedimentos', function ($q) use ($categoryId) {
                    return $q->where(['AtendimentoProcedimentos.procedimento_id' => $categoryId]);
                }
            );

        $this->set(compact('query'));
        $this->set('_serialize', ['query']);
    }

    public function report($id)
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('Configuracoes');
        $empresa = $this->Configuracoes->find()->first();
        $atendimento = $this->Atendimentos->get(
            $id,
            [
                'contain' => [
                    'AtendimentoProcedimentos' => ['Procedimentos' => ['GrupoProcedimentos'],
                        'MedicoResponsaveis'],
                    'Clientes' => ['EstadoCivis'],
                    'Convenios',
                    'Solicitantes',
                    'Users',
                ],
            ]
        );
        $idade = new Time($atendimento->cliente->nascimento);
        $diff = $idade->diff(new DateTime());
        $anos = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';

        $json['empresa'][] = [
            'nome' => empty($empresa->nome_logo) ? '' : $empresa->nome_logo,
            'telefone' => empty($empresa->telefone) ? '' : $empresa->telefone,
            'cnpj' => empty($empresa->cnpj) ? '' : $empresa->cnpj,
            'celular' => empty($empresa->celular) ? '' : $empresa->celular,
            'cep' => empty($empresa->cep) ? '' : $empresa->cep,
            'fax' => empty($empresa->fax) ? '' : $empresa->fax,
            'endereco' => empty($empresa->endereco) ? '' : $empresa->endereco,

        ];
        $data_entrega = date('Y-m-d H:i:s');
        $entrega = '<ul>';
        $procedimentos = '<ul>';
        $json['procedimento'] = null;
        foreach ($atendimento->atendimento_procedimentos as $a) {
            $data_entrega = empty($a->dt_entrega) ? date('Y-m-d H:i:s') : $a->dt_entrega;
            $entrega .= '<li>Entrega: ' .
            empty($a->dt_entrega) ? '' : $this->Data->data_sms($a->dt_entrega) .
                '</li>';
            $procedimentos .= '<li>' . $a->id . '  ' . $a->procedimento->nome . '</li>';
            $grupo = null;
            $grupo = explode(" ", $a->procedimento->grupo_procedimento->nome);
            $json['procedimento'][] = [
                'medico' => !empty($a->medico_responsavei->nome) ? strtoupper($a->medico_responsavei->nome) : '',
                'quantidade' => $a->quantidade,
                'codigo' => $a->codigo,
                'modelo_url' => '../../img/ficha/' . $a->procedimento->ficha_atendimento_impressa_url_modelo,
                'prcedimento' => !empty($a->procedimento->nome) ? strtoupper($a->procedimento->nome) : '',
                'atendimento_procedimento_id' => $a->id,
                'chegada' => !empty($atendimento->created) ? $atendimento->created : date('Y-m-d H:i:s'),
                'valor_caixa' => $a->valor_caixa,
                'valor_fatura' => $a->valor_fatura,
                'grupo_id' => $a->procedimento->grupo_id,
                'grupo_nome' => $grupo[0],
                'entrega' => !empty($a->dt_entrega) ? $a->dt_entrega : date('Y-m-d H:i:s'),
                'complemento' => !empty($a->complemento) ? strtoupper($a->complemento) : '',
            ];
        }
        $procedimentos .= '</ul>';
        $entrega .= '</ul>';
        $json['cliente'][] = [
            'id' => $atendimento->cliente->id,
            'cliente' => !empty($atendimento->cliente->nome) ? strtoupper($atendimento->cliente->nome) : '',
            'prontuario' => $atendimento->cliente->id,
            'nascimento' =>
            empty($atendimento->cliente->nascimento) ? '' : $atendimento->cliente->nascimento->format('d/m/Y'),
            'idade' => $anos,
            'sexo' => $atendimento->cliente->sexo,
            'cor' => $atendimento->cliente->cor,
            'cpf' => $atendimento->cliente->cpf,
            'solicitante' => !empty($atendimento->solicitante->nome) ? strtoupper($atendimento->solicitante->nome) : '',
            'profissao' => !empty($atendimento->cliente->profissao) ? strtoupper($atendimento->cliente->profissao) : '',
            'telefone' => $atendimento->cliente->telefone . ' / ' . $atendimento->cliente->celular,
            'convenio' => !empty($atendimento->convenio->nome) ? strtoupper($atendimento->convenio->nome) : '',
            'estado_civil' => empty($atendimento->cliente->estadocivil_id) ? '' : strtoupper($atendimento->cliente->estado_civi->nome),
            'atendimento' => $atendimento->id,
            'atendente' => !empty($atendimento->user->nome) ? strtoupper($atendimento->user->nome) : '',
            'obs' => !empty($atendimento->cliente->recepcao) ? strtoupper($atendimento->cliente->recepcao) : '',
            'all_procedimento' => $procedimentos,
            'all_entrega' => $entrega,
            'data_entrega' => $data_entrega,
            'total_pago' => $atendimento->total_pagoato,
        ];
        $this->set('atendimento', $atendimento);
        $this->Json->create('atendimento.json', json_encode($json));
        $this->set('report', $this->Configuracao->getMrtFichaAtendimento());
    }

    public function importXml()
    {
        $atendProc = $this->Atendimentos->AtendimentoProcedimentos->find('all')->where(['AtendimentoProcedimentos.situacao_id' => 1]);
        $total = $atendProc->count();
        echo 'Total de atendimentos: ' . $total . '<br>';
        foreach ($atendProc as $ap) {
            echo $ap->id . '.xml criado com sucesso! <br>';
            $this->Atendimento->gerarXML($ap->id, 'Insert');
        }

        $this->autoRender = false;
    }

    public function etiqueta($id)
    {
        $this->viewBuilder()->layout('report_2016');
        $json = $this->Atendimento->etiqueta($id);
        $this->Json->create('etiqueta.json', json_encode($json));
        $this->set('report', 'etiqueta.mrt');
    }

    /** função checa a configuracao de imprimir ficha do atendimento
     * @param $atendimento_id
     * @return static
     */
    public function checkConfigFicha($atendimento_id)
    {
        $this->loadModel('Configuracoes');
        $config = $this->Configuracoes->find()->first();
        $atendimento = $this->Atendimentos->get($atendimento_id);

        $res = [];
        if ($config->imprimir_ficha_semreceber == 0 && $atendimento->total_areceber > 0) {
            $res = ['error' => 1, 'msg' => 'Ficha com valor a receber, Impressão permitida apenas após o recebimento!'];
        }

        $json = json_encode($res);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function relatorios()
    {
        $medQuery = $this->request->query(['medicos']) != null ? $this->request->query(['medicos']) : null;
        $conQuery = $this->request->query(['convenios']) != null ? $this->request->query(['convenios']) : null;
        $groupQuery = $this->request->query(['grupos']) != null ? $this->request->query(['grupos']) : null;
        $procQuery = $this->request->query(['procedimentos']) != null ? $this->request->query(['procedimentos']) : null;
        $inicioQuery = $this->request->query(['inicio']) != null ? $this->Data->DataSQL($this->request->query(['inicio'])) : null;
        $fimQuery = $this->request->query(['fim']) != null ? $this->Data->DataSQL($this->request->query(['fim'])) : null;
        $uniQuery = $this->request->query(['unidade_id']) != null ? $this->request->query(['unidade_id']) : null;
        $controlQuery = $this->request->query(['controle']) != null ? $this->request->query(['controle']) : null;
        $orderQuery = $this->request->query(['ordenar_por']) != null ? $this->request->query(['ordenar_por']) : null;

        $unidades = $this->Atendimentos->Origens->find('list', ['limit' => 200])->orderAsc('Origens.nome');
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $grupo_procedimentos = $this->Atendimentos->AtendimentoProcedimentos->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Atendimentos->AtendimentoProcedimentos->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
        $medicos = $this->Atendimentos->AtendimentoProcedimentos->MedicoResponsaveis->find('list', ['limit' => 200])->orderAsc('MedicoResponsaveis.nome');
        $situacao_faturas = $this->Atendimentos->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->where(['SituacaoFaturas.situacao_id' => 1])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorios = [1 => 'Grupo Procedimentos', 2 => 'Por Usuário', 3 => 'Data Entrega Resultado', 4 => 'Data Entrega x Medico'];

        $atendimentos = $this->Atendimentos->AtendimentoProcedimentos->find()
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis'])
            ->select(['id' => 'AtendimentoProcedimentos.atendimento_id', 'grupo' => 'GrupoProcedimentos.nome', 'exame' => 'Atendimentos.nome', 'paciente' => 'Clientes.nome', 'convenio' => 'Convenios.nome', 'exame' => 'Procedimentos.nome',
                'res_nome' => 'MedicoResponsaveis.nome', 'entrega' => 'DATE_FORMAT(Atendimentos.data, "%d/%m/%Y")'])
            ->where(['Atendimentos.data >=' => $inicioQuery, 'Atendimentos.data <=' => $fimQuery]);

        if (isset($medQuery)) {
            $atendimentos = $atendimentos->where(['MedicoResponsaveis.id IN ' => $medQuery]);
        }

        if (isset($conQuery)) {
            $atendimentos = $atendimentos->where(['Convenios.id IN ' => $conQuery]);
        }

        if (isset($groupQuery)) {
            $atendimentos = $atendimentos->where(['GrupoProcedimentos.id IN ' => $groupQuery]);
        }

        if (isset($procQuery)) {
            $atendimentos = $atendimentos->where(['Procedimentos.id IN ' => $procQuery]);
        }

        if (isset($controlQuery)) {
            $atendimentos = $atendimentos->where(['Atendimentos.id IN ' => $controlQuery]);
        }

        if (isset($uniQuery)) {
            $atendimentos = $atendimentos->where(['Atendimentos.origen_id' => $uniQuery]);
        }

        if (isset($orderQuery)) {
            $atendimentos = $atendimentos->order([$orderQuery]);
        }

        $atendimentos = $this->paginate($atendimentos);
        $this->set(compact('atendimentos', 'unidades', 'convenios', 'grupo_procedimentos', 'medicos', 'atendimentoProcedimentos', 'situacao_faturas', 'procedimentos', 'tiposRelatorios'));
        $this->set('_serialize', ['atendimentos']);
    }

    /**
     * 1ª Documentação - 12/03/2021, por Fernanda Taso
     * Cópia da tela de Relatórios de Atendimentos, com menos filtros e específico para o convênio SUS
     */
    public function relatorioSus() {
        $conQuery = $this->request->query(['convenios']) != null ? $this->request->query(['convenios']) : null;
        $groupQuery = $this->request->query(['grupos']) != null ? $this->request->query(['grupos']) : null;
        $procQuery = $this->request->query(['procedimentos']) != null ? $this->request->query(['procedimentos']) : null;
        $inicioQuery = $this->request->query(['inicio']) != null ? $this->Data->DataSQL($this->request->query(['inicio'])) : null;
        $fimQuery = $this->request->query(['fim']) != null ? $this->Data->DataSQL($this->request->query(['fim'])) : null;
        
        $convenios = $this->Atendimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1, 'Convenios.id' => 29])->orderAsc('Convenios.nome')->toArray(); //Traz apenas o SUS para visualização
        $grupo_procedimentos = $this->Atendimentos->AtendimentoProcedimentos->Procedimentos->GrupoProcedimentos->find('list', ['limit' => 200])->orderAsc('GrupoProcedimentos.nome');
        $procedimentos = $this->Atendimentos->AtendimentoProcedimentos->Procedimentos->find('list')->orderAsc('Procedimentos.nome');
        $situacao_faturas = $this->Atendimentos->AtendimentoProcedimentos->SituacaoFaturas->find('list', ['limit' => 200])->where(['SituacaoFaturas.situacao_id' => 1])->orderAsc('SituacaoFaturas.nome');
        $tiposRelatorios = [200 => 'Relatório SUS'];

        $atendimentos = $this->Atendimentos->AtendimentoProcedimentos->find()
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis'])
            ->select(['id' => 'AtendimentoProcedimentos.atendimento_id', 'grupo' => 'GrupoProcedimentos.nome', 'exame' => 'Atendimentos.nome', 'paciente' => 'Clientes.nome', 'convenio' => 'Convenios.nome', 'exame' => 'Procedimentos.nome',
                'res_nome' => 'MedicoResponsaveis.nome', 'entrega' => 'DATE_FORMAT(Atendimentos.data, "%d/%m/%Y")'])
            ->where(['Atendimentos.data >=' => $inicioQuery, 'Atendimentos.data <=' => $fimQuery]);

        if (isset($conQuery)) {
            $atendimentos = $atendimentos->where(['Convenios.id' => $conQuery]);
        }

        if (isset($groupQuery)) {
            $atendimentos = $atendimentos->where(['GrupoProcedimentos.id IN ' => $groupQuery]);
        }

        if (isset($procQuery)) {
            $atendimentos = $atendimentos->where(['Procedimentos.id IN ' => $procQuery]);
        }

        $atendimentos = $this->paginate($atendimentos);
        $this->set(compact('atendimentos', 'convenios', 'grupo_procedimentos', 'procedimentos', 'tiposRelatorios'));
        $this->set('_serialize', ['atendimentos']);
    }

    //Relatórios Atendimentos
    public function callsReports()
    {
        $this->viewBuilder()->layout('report_2016');
        $inicioQuery = $this->request->query(['inicio']) != null ? $this->Data->DataSQL($this->request->query(['inicio'])) : null;
        $fimQuery = $this->request->query(['fim']) != null ? $this->Data->DataSQL($this->request->query(['fim'])) : null;
        $type = $this->request->query(['relatorio']) != null ? $this->request->query(['relatorio']) : null;
        $unidadeQuery = $this->request->query(['unidade_id']) != null ? $this->request->query(['unidade_id']) : null;
        $controleQuery = $this->request->query(['controle']) != null ? $this->request->query(['controle']) : null;
        $orderQuery = $this->request->query(['ordenar_por']) != null ? $this->request->query(['ordenar_por']) : null;
        $medicoQuery = $this->request->query(['medicos']) != null ? $this->request->query(['medicos']) : null;
        $convenioQuery = $this->request->query(['convenios']) != null ? $this->request->query(['convenios']) : null;
        $grupoQuery = $this->request->query(['grupos']) != null ? $this->request->query(['grupos']) : null;
        $procedimentoQuery = $this->request->query(['procedimentos']) != null ? $this->request->query(['procedimentos']) : null;         
   
        switch($type) {
            case 1:
                $dataQuery = $this->groupCallsReport($inicioQuery, $fimQuery,$unidadeQuery,$controleQuery,$orderQuery,$medicoQuery,$convenioQuery,$grupoQuery, $procedimentoQuery);
                break;
            case 2:
                $dataQuery = $this->userCallsReport($inicioQuery, $fimQuery,$unidadeQuery,$controleQuery,$orderQuery,$medicoQuery,$convenioQuery,$grupoQuery, $procedimentoQuery);
                break;
            case 3:
                $dataQuery = $this->deliveryDateResultCallsReport($inicioQuery, $fimQuery,$unidadeQuery,$controleQuery,$orderQuery,$medicoQuery,$convenioQuery,$grupoQuery, $procedimentoQuery);
                break;
            case 4:
                $dataQuery = $this->deliveryDateDoctorCallsReport($inicioQuery, $fimQuery,$unidadeQuery,$controleQuery,$orderQuery,$medicoQuery,$convenioQuery,$grupoQuery, $procedimentoQuery);
                break;
            case 200: //Relatório SUS
                $dataQuery = $this->groupCallsReport($inicioQuery, $fimQuery,$unidadeQuery,$controleQuery,$orderQuery,$medicoQuery,$convenioQuery,$grupoQuery, $procedimentoQuery, 200);
            break;
        }

        if(empty($dataQuery[1]['dados'])){
            $this->Flash->error('Atenção, não possuem dados com esses filtros!');
        }
        
        $this->Json->create($dataQuery[0], json_encode($dataQuery[1]));
        $this->set('report', $dataQuery[2]);
        $this->render('report');
    }
    
    private function groupCallsReport($inicioQuery = null, $fimQuery = null,$unidadeQuery = null,$controleQuery = null,
    $orderQuery = null,$medicoQuery = null,$convenioQuery = null,$grupoQuery = null, $procedimentoQuery = null, $tipo = null)
    {
        $atendimentos = $this->Atendimentos->AtendimentoProcedimentos->find()
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis'])
            ->select([
                'id' => 'AtendimentoProcedimentos.id',
                'atendimento_id' => 'AtendimentoProcedimentos.atendimento_id',
                'grupo' => 'GrupoProcedimentos.nome',
                'exame' => 'Atendimentos.nome',
                'prontuario' => 'Atendimentos.cliente_id',
                'paciente' => 'Clientes.nome',
                'sexo' => 'Clientes.sexo',
                'nascimento' => 'Clientes.nascimento',
				'telefone' => 'Clientes.celular',
				'carteirinha' => 'Clientes.matricula',
                'cep' => 'Clientes.cep',
                'endereco' => 'Clientes.endereco',
                'numero' => 'Clientes.numero',
                'bairro' => 'Clientes.bairro',
                'complemento' => 'Clientes.complemento',
                'cidade' => 'Clientes.cidade',
                'estado' => 'Clientes.estado',
                'convenio' => 'Convenios.nome', 
                'exame' => 'Procedimentos.nome',
                'res_nome' => 'MedicoResponsaveis.nome',
                'entrega' => 'DATE_FORMAT(Atendimentos.data, "%d/%m/%Y")',
                'hora' => 'Atendimentos.hora',
                'chave_sus' => 'AtendimentoProcedimentos.chave_sus'
            ])
            ->where(['Atendimentos.data >=' => $inicioQuery, 'Atendimentos.data <=' => $fimQuery]);
       
        if (isset($medicoQuery)) {
             $atendimentos->andWhere(['MedicoResponsaveis.id IN ' => $medicoQuery]);
        }
        if (isset($unidadeQuery)) {
             $atendimentos->andWhere(['Atendimentos.unidade_id' => $unidadeQuery]);
        }
        if (isset($controleQuery)) {
             $atendimentos->andWhere(['AtendimentoProcedimentos.controle_financeiro_id IN ' => $controleQuery]);
        }
        if (isset($convenioQuery) && $tipo != 200) {
             $atendimentos->andWhere(['Convenios.id IN ' => $convenioQuery]);
        }
        if (isset($grupoQuery)) {
             $atendimentos->andWhere(['GrupoProcedimentos.id IN' => $grupoQuery]);
        }
        if (isset($procedimentoQuery)) {
             $atendimentos->andWhere(['Procedimentos.id IN ' => $procedimentoQuery]);
        }
        if (isset($orderQuery)) {
             $atendimentos->order([$orderQuery]);
        }

        if ($tipo == 200)
            $atendimentos->andWhere(['Convenios.id' => 29]);

        $json['dados'] = null;
        $atendimentos->orderAsc('Atendimentos.id');

        foreach ($atendimentos as $a) {
            $json['dados'][] = [
                'id' => $a->id,
                'atendimento_id' => $a->atendimento_id,
                'grupo' => $a->grupo,
                'exame' => $a->exame,
                'prontuario' => $a->prontuario,
                'paciente' => $a->paciente,
				'telefone' => $a->telefone,
				'carteirinha' => $a->carteirinha,
                'sexo' => $a->sexo,
                'nascimento' => $this->Data->data_sms($a->nascimento),
                'endereco' => $a->cep . ' - ' . $a->endereco . ', ' . $a->numero . '. ' . $a->bairro . ' ' . $a->complemento . ' - ' . $a->cidade . '/' . $a->estado,
                'convenio' => $a->convenio,
                'exame' => $a->exame,
                'res_nome' => $a->res_nome,
                'entrega' => $a->entrega,
                'hora' => date_format($a->hora, 'H:i'),
                'chave_sus' => !empty($a->chave_sus) ? $a->chave_sus : '-'
            ];
        }

        $json['alias'] = [
            'inicio' => $this->Data->data_sms($inicioQuery),
            'fim' => $this->Data->data_sms($fimQuery)
        ];
        if($tipo == 200)
            return ['atendimento_grupo_procedimentos.json', $json, 'atendimento_relatorio_sus.mrt'];
        return ['atendimento_grupo_procedimentos.json', $json, 'atendimento_grupo_procedimentos.mrt'];
        
    }

    private function userCallsReport($inicioQuery = null, $fimQuery = null,$unidadeQuery = null,$controleQuery = null,
    $orderQuery = null,$medicoQuery = null,$convenioQuery = null,$grupoQuery = null, $procedimentoQuery = null)
    {
        $atendimentos = $this->Atendimentos->AtendimentoProcedimentos->find()
            ->contain(['Atendimentos' => ['Clientes', 'Convenios', 'Users'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis'])
            ->select(['id' => 'AtendimentoProcedimentos.atendimento_id', 'usuario' => 'Users.nome', 'exame' => 'Atendimentos.nome',
                'paciente' => 'Clientes.nome', 'convenio' => 'Convenios.nome', 'exame' => 'Procedimentos.nome',
                'res_nome' => 'MedicoResponsaveis.nome', 'entrega' => 'DATE_FORMAT(Atendimentos.data, "%d/%m/%Y")',
                'hora' => 'Atendimentos.hora', 'valor_pago' => 'Atendimentos.total_geral - Atendimentos.total_pagoato'])
            ->where(['Atendimentos.data >=' => $inicioQuery, 'Atendimentos.data <=' => $fimQuery]);
            
            if (isset($medicoQuery)) {
                $atendimentoProcedimentos = $atendimentoProcedimentos->where(['MedicoResponsaveis.id IN ' => $medicoQuery]);
            }
            if (isset($unidadeQuery)) {
                $atendimentos->where(['Atendimentos.unidade_id' => $unidadeQuery]);
            }
            if (isset($controleQuery)) {
                $atendimentos->where(['AtendimentoProcedimentos.controle_financeiro_id IN ' => $controleQuery]);
            }
            if (isset($convenioQuery)) {
                $atendimentos->where(['Convenios.id IN ' => $convenioQuery]);
            }
            if (isset($grupoQuery)) {
                $atendimentos->where(['GrupoProcedimentos.id IN ' => $grupoQuery]);
            }
            if (isset($procedimentoQuery)) {
                $atendimentos->where(['Procedimentos.id IN ' => $procedimentoQuery]);
            }
            if (isset($orderQuery)) {
                $atendimentos->order([$orderQuery]);
            }



        $json['dados'] = null;
        $atendimentos->orderAsc('Atendimentos.id');
        foreach ($atendimentos as $a) {
            $json['dados'][] = [
                'id' => $a->id,
                'exame' => $a->exame,
                'usuario' => $a->usuario,
                'paciente' => $a->paciente,
                'convenio' => $a->convenio,
                'exame' => $a->exame,
                'res_nome' => $a->res_nome,
                'entrega' => $a->entrega,
                'atendimento' => $a->atendimento,
                'hora' => date_format($a->hora, 'H:i'),
                'valor_pago' => $a->valor_pago
            ];
        }

        return ['atendimento_usuario_procedimentos.json', $json, 'atendimento_usuario_procedimentos.mrt'];
    }

    private function deliveryDateResultCallsReport($inicioQuery = null, $fimQuery = null,$unidadeQuery = null,$controleQuery = null,
    $orderQuery = null,$medicoQuery = null,$convenioQuery = null,$grupoQuery = null, $procedimentoQuery = null)
    {
        $atendimentos = $this->Atendimentos->AtendimentoProcedimentos->find()
            ->contain(['Atendimentos' => ['Clientes', 'Convenios', 'Users'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'SituacaoLaudos'])
            ->select(['id' => 'AtendimentoProcedimentos.atendimento_id', 'grupo' => 'GrupoProcedimentos.nome', 'usuario' => 'Users.nome', 'exame' => 'Atendimentos.nome',
                'paciente' => 'Clientes.nome', 'convenio' => 'Convenios.nome', 'exame' => 'Procedimentos.nome',
                'res_nome' => 'MedicoResponsaveis.nome', 'entrega' => 'DATE_FORMAT(Atendimentos.data, "%d/%m/%Y")',
                'hora' => 'Atendimentos.hora', 'atendimento' => 'DATE_FORMAT(Atendimentos.created, "%d/%m/%Y")',
                'situacao_laudo' => 'SituacaoLaudos.nome'])
            ->where(['Atendimentos.data >=' => $inicioQuery, 'Atendimentos.data <=' => $fimQuery]);

            if (isset($medicoQuery)) {
                $atendimentos->where(['MedicoResponsaveis.id IN ' => $medicoQuery]);
            }
            if (isset($unidadeQuery)) {
                $atendimentos->where(['Atendimentos.unidade_id' => $unidadeQuery]);
            }
            if (isset($controleQuery)) {
                $atendimentos->where(['AtendimentoProcedimentos.controle_financeiro_id IN ' => $controleQuery]);
            }
            if (isset($convenioQuery)) {
                $atendimentos->where(['Convenios.id IN ' => $convenioQuery]);
            }
            if (isset($grupoQuery)) {
                $atendimentos->where(['GrupoProcedimentos.id IN ' => $grupoQuery]);
            }
            if (isset($procedimentoQuery)) {
                $atendimentos->where(['Procedimentos.id IN ' => $procedimentoQuery]);
            }
            if (isset($orderQuery)) {
                $atendimentos->order([$orderQuery]);
            }

        $json['dados'] = null;
        $atendimentos->orderAsc('Atendimentos.id');
        foreach ($atendimentos as $a) {
            $json['dados'][] = [
                'id' => $a->id,
                'grupo' => $a->grupo,
                'exame' => $a->exame,
                'usuario' => $a->usuario,
                'paciente' => $a->paciente,
                'convenio' => $a->convenio,
                'exame' => $a->exame,
                'res_nome' => $a->res_nome,
                'data_entrega' => $a->entrega,
                'data_atendimento' => $a->atendimento,
                'situacao_laudo' => $a->situacao_laudo,
                'hora' => date_format($a->hora, 'H:i')
            ];
        }

        return ['atendimento_data_entrega.json', $json, 'atendimento_data_entrega.mrt'];
    }

    private function deliveryDateDoctorCallsReport($inicioQuery = null, $fimQuery = null,$unidadeQuery = null,$controleQuery = null,
    $orderQuery = null,$medicoQuery = null,$convenioQuery = null,$grupoQuery = null, $procedimentoQuery = null)
    {
        $atendimentos = $this->Atendimentos->AtendimentoProcedimentos->find()
            ->contain(['Atendimentos' => ['Clientes', 'Convenios', 'Users'], 'Procedimentos' => ['GrupoProcedimentos'], 'MedicoResponsaveis', 'SituacaoLaudos'])
            ->select(['id' => 'AtendimentoProcedimentos.atendimento_id', 'grupo' => 'GrupoProcedimentos.nome', 'usuario' => 'Users.nome', 'exame' => 'Atendimentos.nome',
                'paciente' => 'Clientes.nome', 'convenio' => 'Convenios.nome', 'exame' => 'Procedimentos.nome',
                'res_nome' => 'MedicoResponsaveis.nome', 'entrega' => 'DATE_FORMAT(Atendimentos.data, "%d/%m/%Y")',
                'hora' => 'Atendimentos.hora', 'atendimento' => 'DATE_FORMAT(Atendimentos.created, "%d/%m/%Y")',
                'situacao_laudo' => 'SituacaoLaudos.nome'])
            ->where(['Atendimentos.data >=' => $inicioQuery, 'Atendimentos.data <=' => $fimQuery]);
            
            if (isset($medicoQuery)) {
                $atendimentoProcedimentos = $atendimentoProcedimentos->where(['MedicoResponsaveis.id IN ' => $medicoQuery]);
            }
            if (isset($unidadeQuery)) {
                $atendimentos->where(['Atendimentos.unidade_id' => $unidadeQuery]);
            }
            if (isset($controleQuery)) {
                $atendimentos->where(['AtendimentoProcedimentos.controle_financeiro_id IN ' => $controleQuery]);
            }
            if (isset($convenioQuery)) {
                $atendimentos->where(['Convenios.id IN ' => $convenioQuery]);
            }
            if (isset($grupoQuery)) {
                $atendimentos->where(['GrupoProcedimentos.id IN ' => $grupoQuery]);
            }
            if (isset($procedimentoQuery)) {
                $atendimentos->where(['Procedimentos.id IN ' => $procedimentoQuery]);
            }
            if (isset($orderQuery)) {
                $atendimentos->order([$orderQuery]);
            }

        $json['dados'] = null;
        $atendimentos->orderAsc('Atendimentos.id');
        foreach ($atendimentos as $a) {
            $json['dados'][] = [
                'id' => $a->id,
                'grupo' => $a->grupo,
                'exame' => $a->exame,
                'usuario' => $a->usuario,
                'paciente' => $a->paciente,
                'convenio' => $a->convenio,
                'exame' => $a->exame,
                'res_nome' => $a->res_nome,
                'data_entrega' => $a->entrega,
                'data_atendimento' => $a->atendimento,
                'situacao_laudo' => $a->situacao_laudo,
                'hora' => date_format($a->hora, 'H:i')
            ];
        }

        return ['atendimento_data_entrega_medicos.json', $json, 'atendimento_data_entrega_medicos.mrt'];
    }

    public function cancelaAtendimento()
    {
        $error = 0;
        $atendimento = $this->Atendimentos->get($this->request->getData('atendimento_id'), [
            'contain' => ['AtendimentoProcedimentos']
        ]);
        $atendimento->situacao_id = 2;
        $atendimento->excluido_justificativa = $this->request->getData('justificativa');
        if($this->Atendimentos->save($atendimento)) {
            if(!empty($atendimento->atendimento_procedimentos)) {
                $query = $this->Atendimentos->AtendimentoProcedimentos->query()->update()
                    ->set(['situacao_id' => 2])
                    ->where(['atendimento_id' => $this->request->getData('atendimento_id')])
                    ->execute();

                if($query)
                    $error = 0;
            
                foreach($atendimento->atendimento_procedimentos as $ap){
                    $this->Atendimento->gerarXML($ap->id, 'Delete');
                }
            }
        }

        $json = json_encode($error);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }
}