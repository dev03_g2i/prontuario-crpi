<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConvenioRegracalculo Controller
 *
 * @property \App\Model\Table\ConvenioRegracalculoTable $ConvenioRegracalculo
 */
class ConvenioRegracalculoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $convenioRegracalculo = $this->paginate($this->ConvenioRegracalculo);


        $this->set(compact('convenioRegracalculo'));
        $this->set('_serialize', ['convenioRegracalculo']);

    }

    /**
     * View method
     *
     * @param string|null $id Convenio Regracalculo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convenioRegracalculo = $this->ConvenioRegracalculo->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('convenioRegracalculo', $convenioRegracalculo);
        $this->set('_serialize', ['convenioRegracalculo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convenioRegracalculo = $this->ConvenioRegracalculo->newEntity();
        if ($this->request->is('post')) {
            $convenioRegracalculo = $this->ConvenioRegracalculo->patchEntity($convenioRegracalculo, $this->request->data);
            if ($this->ConvenioRegracalculo->save($convenioRegracalculo)) {
                $this->Flash->success(__('O convenio regracalculo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O convenio regracalculo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('convenioRegracalculo'));
        $this->set('_serialize', ['convenioRegracalculo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Convenio Regracalculo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convenioRegracalculo = $this->ConvenioRegracalculo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convenioRegracalculo = $this->ConvenioRegracalculo->patchEntity($convenioRegracalculo, $this->request->data);
            if ($this->ConvenioRegracalculo->save($convenioRegracalculo)) {
                $this->Flash->success(__('O convenio regracalculo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O convenio regracalculo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('convenioRegracalculo'));
        $this->set('_serialize', ['convenioRegracalculo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convenio Regracalculo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $convenioRegracalculo = $this->ConvenioRegracalculo->get($id);
                $convenioRegracalculo->situacao_id = 2;
        if ($this->ConvenioRegracalculo->save($convenioRegracalculo)) {
            $this->Flash->success(__('O convenio regracalculo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O convenio regracalculo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Convenio Regracalculo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ConvenioRegracalculo->find('all')
        ->where(['ConvenioRegracalculo.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ConvenioRegracalculo.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Convenio Regracalculo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $convenioRegracalculo = $this->ConvenioRegracalculo->get($this->request->data['id']);
            $res = ['nome'=>$convenioRegracalculo->nome,'id'=>$convenioRegracalculo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
