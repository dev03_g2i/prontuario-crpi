<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ConvenioImpostosParametro Controller
 *
 * @property \App\Model\Table\ConvenioImpostosParametroTable $ConvenioImpostosParametro
 */
class ConvenioImpostosParametroController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($convenioId = null)
    {      
        if (!$convenioId) return $this->redirect(['controller' => 'Agendas', 'action' => 'index']);

        $this->loadModel('Convenios');
        $convenio = $this->Convenios->get($convenioId);
        $listConvenioImpostosParametros = $this->ConvenioImpostosParametro->find('all')
                                            ->contain(['Convenios', 'ConvenioTipoimposto'])
                                            ->where(['ConvenioImpostosParametro.situacao_id' => 1])
                                            ->andWhere(['ConvenioImpostosParametro.id_convenio' => $convenioId]);

        $convenioImpostosParametro = $this->paginate($listConvenioImpostosParametros);
        
        $this->set(compact('convenioImpostosParametro', 'convenio', 'listConvenioImpostosParametros'));
        $this->set('_serialize', ['convenioImpostosParametro']);

    }

    /**
     * View method
     *
     * @param string|null $id Convenio Impostos Parametro id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convenioImpostosParametro = $this->ConvenioImpostosParametro->get($id, [
            'contain' => []
        ]);

        $this->set('convenioImpostosParametro', $convenioImpostosParametro);
        $this->set('_serialize', ['convenioImpostosParametro']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($convenioId = null)
    {   
        if (!$convenioId) return $this->redirect(['controller' => 'Agendas', 'action' => 'index']);
        
        $this->loadModel('Convenios');
        $this->loadModel('ConvenioTipoimposto');
        $convenio = $this->Convenios->get($convenioId);
        $impostos = $this->ConvenioTipoimposto->find('list')->orderAsc('ConvenioTipoimposto.descricao');

        $convenioImpostosParametro = $this->ConvenioImpostosParametro->newEntity();
        if ($this->request->is('post')) {

            $newConvenioImposto = $this->request->data;
            $newConvenioImposto['id_convenio'] = $convenioId;
            $newConvenioImposto['convenio_id'] = $convenioId;
            $newConvenioImposto['user_created'] = $this->Auth->user('id');

            $convenioImpostosParametro = $this->ConvenioImpostosParametro->patchEntity($convenioImpostosParametro, $newConvenioImposto);
            if ($this->ConvenioImpostosParametro->save($convenioImpostosParametro)) {
                $this->Flash->success(__('O convenio impostos parametro foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index', $convenioId]);
            } else {
                $this->Flash->error(__('O convenio impostos parametro não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('convenioImpostosParametro', 'convenio', 'impostos'));
        $this->set('_serialize', ['convenioImpostosParametro']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Convenio Impostos Parametro id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null, $convenioId = null)
    {   
        if (!$convenioId || !$id) return $this->redirect(['controller' => 'Agendas', 'action' => 'index']);

        $this->loadModel('Convenios');
        $this->loadModel('ConvenioTipoimposto');
        $convenio = $this->Convenios->get($convenioId);
        $impostos = $this->ConvenioTipoimposto->find('list')->orderAsc('ConvenioTipoimposto.descricao');

        $convenioImpostosParametro = $this->ConvenioImpostosParametro->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $newConvenioImposto = $this->request->data;
            $newConvenioImposto['id_convenio'] = $convenioId;
            $newConvenioImposto['user_updated'] = $this->Auth->user('id');

            $convenioImpostosParametro = $this->ConvenioImpostosParametro->patchEntity($convenioImpostosParametro, $newConvenioImposto);
            if ($this->ConvenioImpostosParametro->save($convenioImpostosParametro)) {
                $this->Flash->success(__('O convenio impostos parametro foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index', $convenioId]);
            } else {
                $this->Flash->error(__('O convenio impostos parametro não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('convenioImpostosParametro', 'convenio', 'impostos'));
        $this->set('_serialize', ['convenioImpostosParametro']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convenio Impostos Parametro id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null, $convenioId = null)
    {   
        if (!$convenioId || !$id) return $this->redirect(['controller' => 'Agendas', 'action' => 'index']);

        $convenioImpostosParametro = $this->ConvenioImpostosParametro->get($id);
        $convenioImpostosParametro->situacao_id = 2;
        if ($this->ConvenioImpostosParametro->save($convenioImpostosParametro)) {
            $this->Flash->success(__('O convenio impostos parametro foi deletado com sucesso.'));
            return $this->redirect(['action' => 'index', $convenioId]);
        } else {
            $this->Flash->error(__('Desculpe! O convenio impostos parametro não foi deletado! Tente novamente mais tarde.'));
        }
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Convenio Impostos Parametro id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ConvenioImpostosParametro->find('all')
        ->where(['ConvenioImpostosParametro.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ConvenioImpostosParametro.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Convenio Impostos Parametro id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $convenioImpostosParametro = $this->ConvenioImpostosParametro->get($this->request->data['id']);
            $res = ['nome'=>$convenioImpostosParametro->nome,'id'=>$convenioImpostosParametro->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
