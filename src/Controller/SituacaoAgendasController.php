<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoAgendas Controller
 *
 * @property \App\Model\Table\SituacaoAgendasTable $SituacaoAgendas
 */
class SituacaoAgendasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['SituacaoAgendas.situacao_id = ' => '1']
                    ];
        $situacaoAgendas = $this->paginate($this->SituacaoAgendas);

        $this->set(compact('situacaoAgendas'));
        $this->set('_serialize', ['situacaoAgendas']);
    }

    /**
     * View method
     *
     * @param string|null $id Situacao Agenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoAgenda = $this->SituacaoAgendas->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('situacaoAgenda', $situacaoAgenda);
        $this->set('_serialize', ['situacaoAgenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoAgenda = $this->SituacaoAgendas->newEntity();
        if ($this->request->is('post')) {
            $situacaoAgenda = $this->SituacaoAgendas->patchEntity($situacaoAgenda, $this->request->data);
            if ($this->SituacaoAgendas->save($situacaoAgenda)) {
                $this->Flash->success(__('O situacao agenda foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao agenda não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->SituacaoAgendas->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->SituacaoAgendas->Users->find('list', ['limit' => 200]);
        $this->set(compact('situacaoAgenda', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['situacaoAgenda']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Agenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoAgenda = $this->SituacaoAgendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoAgenda = $this->SituacaoAgendas->patchEntity($situacaoAgenda, $this->request->data);
            if ($this->SituacaoAgendas->save($situacaoAgenda)) {
                $this->Flash->success(__('O situacao agenda foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao agenda não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->SituacaoAgendas->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->SituacaoAgendas->Users->find('list', ['limit' => 200]);
        $this->set(compact('situacaoAgenda', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['situacaoAgenda']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoAgenda = $this->SituacaoAgendas->get($id);
                $situacaoAgenda->situacao_id = 2;
        if ($this->SituacaoAgendas->save($situacaoAgenda)) {
            $this->Flash->success(__('O situacao agenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao agenda não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
