<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProfissionalCreditos Controller
 *
 * @property \App\Model\Table\ProfissionalCreditosTable $ProfissionalCreditos
 */
class ProfissionalCreditosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['MedicoResponsaveis', 'SituacaoCadastros', 'Users']
        ];
        $query =[];
        $profissional_id =null;
        if(!empty($this->request->query['profissional_id'])){
            $profissional_id = $this->request->query['profissional_id'];
            $query['conditions'][] = ['ProfissionalCreditos.profissional_id'=>$profissional_id];
        }
        $this->paginate = array_merge($this->paginate,$query);

        $profissionalCreditos = $this->paginate($this->ProfissionalCreditos);


        $this->set(compact('profissionalCreditos','profissional_id'));
        $this->set('_serialize', ['profissionalCreditos']);

    }

    /**
     * View method
     *
     * @param string|null $id Profissional Credito id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $profissionalCredito = $this->ProfissionalCreditos->get($id, [
            'contain' => ['MedicoResponsaveis', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('profissionalCredito', $profissionalCredito);
        $this->set('_serialize', ['profissionalCredito']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $profissionalCredito = $this->ProfissionalCreditos->newEntity();
        if ($this->request->is('post')) {
            $profissionalCredito = $this->ProfissionalCreditos->patchEntity($profissionalCredito, $this->request->data);
            if ($this->ProfissionalCreditos->save($profissionalCredito)) {
                $this->Flash->success(__('O profissional credito foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O profissional credito não foi salvo. Por favor, tente novamente.'));
            }
        }

        $profissional_id =null;
        if(!empty($this->request->query['profissional_id'])){
            $profissional_id = $this->request->query['profissional_id'];
        }

        $this->set(compact('profissionalCredito','profissional_id'));
        $this->set('_serialize', ['profissionalCredito']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Profissional Credito id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $profissionalCredito = $this->ProfissionalCreditos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $profissionalCredito = $this->ProfissionalCreditos->patchEntity($profissionalCredito, $this->request->data);
            if ($this->ProfissionalCreditos->save($profissionalCredito)) {
                $this->Flash->success(__('O profissional credito foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O profissional credito não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('profissionalCredito'));
        $this->set('_serialize', ['profissionalCredito']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Profissional Credito id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $profissionalCredito = $this->ProfissionalCreditos->get($id);
                $profissionalCredito->situacao_id = 2;
        if ($this->ProfissionalCreditos->save($profissionalCredito)) {
            $this->Flash->success(__('O profissional credito foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O profissional credito não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Profissional Credito id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ProfissionalCreditos->find('all')
        ->where(['ProfissionalCreditos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ProfissionalCreditos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Profissional Credito id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $profissionalCredito = $this->ProfissionalCreditos->get($this->request->data['id']);
            $res = ['nome'=>$profissionalCredito->nome,'id'=>$profissionalCredito->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
