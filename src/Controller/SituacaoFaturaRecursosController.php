<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoFaturaRecursos Controller
 *
 * @property \App\Model\Table\SituacaoFaturaRecursosTable $SituacaoFaturaRecursos
 */
class SituacaoFaturaRecursosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
        ];
        $situacaoFaturaRecursos = $this->paginate($this->SituacaoFaturaRecursos);


        $this->set(compact('situacaoFaturaRecursos'));
        $this->set('_serialize', ['situacaoFaturaRecursos']);

    }

    /**
     * View method
     *
     * @param string|null $id Situacao Fatura Recurso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->get($id, [
            'contain' => ['Users', 'SituacaoCadastros', 'FaturaRecursos']
        ]);

        $this->set('situacaoFaturaRecurso', $situacaoFaturaRecurso);
        $this->set('_serialize', ['situacaoFaturaRecurso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->newEntity();
        if ($this->request->is('post')) {
            $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->patchEntity($situacaoFaturaRecurso, $this->request->data);
            if ($this->SituacaoFaturaRecursos->save($situacaoFaturaRecurso)) {
                $this->Flash->success(__('O situacao fatura recurso foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao fatura recurso não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoFaturaRecurso'));
        $this->set('_serialize', ['situacaoFaturaRecurso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Fatura Recurso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->patchEntity($situacaoFaturaRecurso, $this->request->data);
            if ($this->SituacaoFaturaRecursos->save($situacaoFaturaRecurso)) {
                $this->Flash->success(__('O situacao fatura recurso foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao fatura recurso não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoFaturaRecurso'));
        $this->set('_serialize', ['situacaoFaturaRecurso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Fatura Recurso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->get($id);
                $situacaoFaturaRecurso->situacao_id = 2;
        if ($this->SituacaoFaturaRecursos->save($situacaoFaturaRecurso)) {
            $this->Flash->success(__('O situacao fatura recurso foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao fatura recurso não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Situacao Fatura Recurso id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->SituacaoFaturaRecursos->find('all')
        ->where(['SituacaoFaturaRecursos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('SituacaoFaturaRecursos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Situacao Fatura Recurso id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $situacaoFaturaRecurso = $this->SituacaoFaturaRecursos->get($this->request->data['id']);
            $res = ['nome'=>$situacaoFaturaRecurso->nome,'id'=>$situacaoFaturaRecurso->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
