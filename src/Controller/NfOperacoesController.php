<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NfOperacoes Controller
 *
 * @property \App\Model\Table\NfOperacoesTable $NfOperacoes
 */
class NfOperacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $nfOperacoes = $this->paginate($this->NfOperacoes);


        $this->set(compact('nfOperacoes'));
        $this->set('_serialize', ['nfOperacoes']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf Operaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nfOperaco = $this->NfOperacoes->get($id, [
            'contain' => []
        ]);

        $this->set('nfOperaco', $nfOperaco);
        $this->set('_serialize', ['nfOperaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nfOperaco = $this->NfOperacoes->newEntity();
        if ($this->request->is('post')) {
            $nfOperaco = $this->NfOperacoes->patchEntity($nfOperaco, $this->request->data);
            if ($this->NfOperacoes->save($nfOperaco)) {
                $this->Flash->success(__('O nf operaco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf operaco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfOperaco'));
        $this->set('_serialize', ['nfOperaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf Operaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nfOperaco = $this->NfOperacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nfOperaco = $this->NfOperacoes->patchEntity($nfOperaco, $this->request->data);
            if ($this->NfOperacoes->save($nfOperaco)) {
                $this->Flash->success(__('O nf operaco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf operaco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfOperaco'));
        $this->set('_serialize', ['nfOperaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf Operaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nfOperaco = $this->NfOperacoes->get($id);
                $nfOperaco->situacao_id = 2;
        if ($this->NfOperacoes->save($nfOperaco)) {
            $this->Flash->success(__('O nf operaco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf operaco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf Operaco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->NfOperacoes->find('all')
        ->where(['NfOperacoes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('NfOperacoes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf Operaco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nfOperaco = $this->NfOperacoes->get($this->request->data['id']);
            $res = ['nome'=>$nfOperaco->nome,'id'=>$nfOperaco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
