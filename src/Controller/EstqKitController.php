<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EstqKit Controller
 *
 * @property \App\Model\Table\EstqKitTable $EstqKit
 */
class EstqKitController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->EstqKit->find('all')
            ->contain(['Procedimentos', 'UserReg', 'UserAlt', 'SituacaoCadastros'])
            ->where(['EstqKit.situacao_id' => 1]);
        $estqKit = $this->paginate($query);

        $procedimentos = $this->EstqKit->Procedimentos->getList();
        $this->set(compact('estqKit', 'procedimentos'));
        $this->set('_serialize', ['estqKit']);

    }

    /**
     * View method
     *
     * @param string|null $id Estq Kit id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estqKit = $this->EstqKit->get($id, [
            'contain' => ['Procedimentos', 'UserReg', 'UserAlt', 'SituacaoCadastros']
        ]);

        $this->set('estqKit', $estqKit);
        $this->set('_serialize', ['estqKit']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $estqKit = $this->EstqKit->newEntity();
        if ($this->request->is('post')) {
            $estqKit = $this->EstqKit->patchEntity($estqKit, $this->request->data);
            if ($this->EstqKit->save($estqKit)) {
                $this->Flash->success(__('O estq kit foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq kit não foi salvo. Por favor, tente novamente.'));
            }
        }

        $procedimentos = $this->EstqKit->Procedimentos->getList();
        $this->set(compact('estqKit', 'procedimentos'));
        $this->set('_serialize', ['estqKit']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Estq Kit id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $estqKit = $this->EstqKit->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estqKit = $this->EstqKit->patchEntity($estqKit, $this->request->data);
            if ($this->EstqKit->save($estqKit)) {
                $this->Flash->success(__('O estq kit foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq kit não foi salvo. Por favor, tente novamente.'));
            }
        }

        $procedimentos = $this->EstqKit->Procedimentos->getList();
        $this->set(compact('estqKit', 'procedimentos'));
        $this->set('_serialize', ['estqKit']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estq Kit id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $estqKit = $this->EstqKit->get($id);
                $estqKit->situacao_id = 2;
        if ($this->EstqKit->save($estqKit)) {
            $this->Flash->success(__('O estq kit foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O estq kit não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Estq Kit id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->EstqKit->find('all')
        ->where(['EstqKit.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('EstqKit.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Estq Kit id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $estqKit = $this->EstqKit->get($this->request->data['id']);
            $res = ['nome'=>$estqKit->nome,'id'=>$estqKit->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function verificaProcKit(){
        $this->autoRender=false;
        $this->response->type('json');

        $checkEstqKit = $this->EstqKit->find('all')->where(['EstqKit.situacao_id' => 1])
            ->andWhere(['EstqKit.procedimento_id' => $this->request->data('proc_id')])->count();

        $query = $this->EstqKit->getList();

        if($checkEstqKit == 0){
            $query = $query->andWhere(['EstqKit.procedimento_id <> ' => $this->request->data('proc_id')]);
        }

        $this->response->body(json_encode($query));
    }
}
