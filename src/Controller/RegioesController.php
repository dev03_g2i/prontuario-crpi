<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Regioes Controller
 *
 * @property \App\Model\Table\RegioesTable $Regioes
 */
class RegioesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros']
                        ,'conditions' => ['Regioes.situacao_id = ' => '1']
                    ];
        $regioes = $this->paginate($this->Regioes);

        $this->set(compact('regioes'));
        $this->set('_serialize', ['regioes']);
    }

    /**
     * View method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $regio = $this->Regioes->get($id, [
            'contain' => ['SituacaoCadastros']
        ]);

        $this->set('regio', $regio);
        $this->set('_serialize', ['regio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $regio = $this->Regioes->newEntity();
        if ($this->request->is('post')) {
            $regio = $this->Regioes->patchEntity($regio, $this->request->data);
            if ($this->Regioes->save($regio)) {
                $this->Flash->success(__('O regio foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O regio não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Regioes->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('regio', 'situacaoCadastros'));
        $this->set('_serialize', ['regio']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $regio = $this->Regioes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $regio = $this->Regioes->patchEntity($regio, $this->request->data);
            if ($this->Regioes->save($regio)) {
                $this->Flash->success(__('O regio foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O regio não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Regioes->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('regio', 'situacaoCadastros'));
        $this->set('_serialize', ['regio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Regio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $regio = $this->Regioes->get($id);
                $regio->situacao_id = 2;
        if ($this->Regioes->save($regio)) {
            $this->Flash->success(__('O regio foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O regio não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
