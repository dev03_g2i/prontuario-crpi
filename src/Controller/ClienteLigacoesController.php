<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ClienteLigacoes Controller
 *
 * @property \App\Model\Table\ClienteLigacoesTable $ClienteLigacoes
 */
class ClienteLigacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clientes', 'ParentClienteLigacoes', 'GrauParentescos', 'SituacaoCadastros', 'Users']
        ];

        $query = [];
        $cliente_id = null;
        if(!empty($this->request->query['cliente_id'])){
            $cliente_id = $this->request->query['cliente_id'];
            $query['conditions'][]= ['ClienteLigacoes.cliente_id'=>$this->request->query['cliente_id']];
        }
        $this->paginate = array_merge($this->paginate,$query);
        $clienteLigacoes = $this->paginate($this->ClienteLigacoes);


        $this->set(compact('clienteLigacoes','cliente_id'));
        $this->set('_serialize', ['clienteLigacoes']);

    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function all()
    {
        $this->paginate = [
            'contain' => ['Clientes', 'ParentClienteLigacoes', 'GrauParentescos', 'SituacaoCadastros', 'Users']
        ];

        $query = [];
        $cliente_id = null;
        if(!empty($this->request->query['cliente_id'])){
            $cliente_id = $this->request->query['cliente_id'];
            $query['conditions'][]= ['ClienteLigacoes.cliente_id'=>$this->request->query['cliente_id']];
        }
        $this->paginate = array_merge($this->paginate,$query);
        $clienteLigacoes = $this->paginate($this->ClienteLigacoes);


        $this->set(compact('clienteLigacoes','cliente_id'));
        $this->set('_serialize', ['clienteLigacoes']);

    }

    /**
     * View method
     *
     * @param string|null $id Cliente Ligaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $clienteLigaco = $this->ClienteLigacoes->get($id, [
            'contain' => ['Clientes', 'ParentClienteLigacoes', 'GrauParentescos', 'SituacaoCadastros', 'Users', 'ChildClienteLigacoes']
        ]);

        $this->set('clienteLigaco', $clienteLigaco);
        $this->set('_serialize', ['clienteLigaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $clienteLigaco = $this->ClienteLigacoes->newEntity();
        if ($this->request->is('post')) {
            $clienteLigaco = $this->ClienteLigacoes->patchEntity($clienteLigaco, $this->request->data);
            if ($this->ClienteLigacoes->save($clienteLigaco)) {
                $cliente = $this->ClienteLigacoes->Clientes->get($clienteLigaco->cliente_id);
                $this->loadModel('LinkLigacoes');
                $count_link = $this->LinkLigacoes->find('all')->where(['LinkLigacoes.in_id'=>$clienteLigaco->grau_id])->count();
                $out = null;

                if($count_link>1){
                    $link = $this->LinkLigacoes->find('all')->where(['LinkLigacoes.in_id'=>$clienteLigaco->grau_id]);
                    foreach ($link as $l) {
                        if($l->param==$cliente->sexo){
                            $out = $l->out_id;
                        }
                    }
                }else{
                    $link = $this->LinkLigacoes->find('all')->where(['LinkLigacoes.in_id'=>$clienteLigaco->grau_id])->first();
                    $out = $link->out_id;
                }


                    $clienteOut = $this->ClienteLigacoes->newEntity();
                    $clienteOut->cliente_id = $clienteLigaco->parent_id;
                    $clienteOut->parent_id = $clienteLigaco->cliente_id;
                    $clienteOut->grau_id = empty($out) ? $clienteLigaco->cliente_id : $out;
                    $this->ClienteLigacoes->save($clienteOut);

                $this->Flash->success(__('O cliente ligaco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente ligaco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $cliente_id = null;
        if(!empty($this->request->query['cliente_id'])){
            $cliente_id = $this->request->query['cliente_id'];
        }
        $grau = $this->ClienteLigacoes->GrauParentescos->find('list')->orderAsc('GrauParentescos.nome');

        $this->set(compact('clienteLigaco','grau','cliente_id'));
        $this->set('_serialize', ['clienteLigaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function nadd()
    {
        $clienteLigaco = $this->ClienteLigacoes->newEntity();
        if ($this->request->is('post')) {
            $clienteLigaco = $this->ClienteLigacoes->patchEntity($clienteLigaco, $this->request->data);
            if ($this->ClienteLigacoes->save($clienteLigaco)) {
                $cliente = $this->ClienteLigacoes->Clientes->get($clienteLigaco->cliente_id);
                $this->loadModel('LinkLigacoes');
                $count_link = $this->LinkLigacoes->find('all')->where(['LinkLigacoes.in_id'=>$clienteLigaco->grau_id])->count();
                $out = null;

                if($count_link>1){
                    $link = $this->LinkLigacoes->find('all')->where(['LinkLigacoes.in_id'=>$clienteLigaco->grau_id]);
                    foreach ($link as $l) {
                        if($l->param==$cliente->sexo){
                            $out = $l->out_id;
                        }
                    }
                }else{
                    $link = $this->LinkLigacoes->find('all')->where(['LinkLigacoes.in_id'=>$clienteLigaco->grau_id])->first();
                    $out = $link->out_id;
                }


                    $clienteOut = $this->ClienteLigacoes->newEntity();
                    $clienteOut->cliente_id = $clienteLigaco->parent_id;
                    $clienteOut->parent_id = $clienteLigaco->cliente_id;
                    $clienteOut->grau_id = empty($out) ? $clienteLigaco->cliente_id : $out;
                    $this->ClienteLigacoes->save($clienteOut);

                $this->Flash->success(__('O cliente ligaco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente ligaco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $cliente_id = null;
        if(!empty($this->request->query['cliente_id'])){
            $cliente_id = $this->request->query['cliente_id'];
        }
        $grau = $this->ClienteLigacoes->GrauParentescos->find('list')->orderAsc('GrauParentescos.nome');

        $this->set(compact('clienteLigaco','grau','cliente_id'));
        $this->set('_serialize', ['clienteLigaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente Ligaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $clienteLigaco = $this->ClienteLigacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $clienteLigaco = $this->ClienteLigacoes->patchEntity($clienteLigaco, $this->request->data);
            if ($this->ClienteLigacoes->save($clienteLigaco)) {
                $this->Flash->success(__('O cliente ligaco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O cliente ligaco não foi salvo. Por favor, tente novamente.'));
            }
        }
        $grau = $this->ClienteLigacoes->GrauParentescos->find('list')->orderAsc('GrauParentescos.nome');

        $this->set(compact('clienteLigaco','grau'));
        $this->set('_serialize', ['clienteLigaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente Ligaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->request->allowMethod(['post', 'delete']);
        $clienteLigaco = $this->ClienteLigacoes->get($id);
                $clienteLigaco->situacao_id = 2;

        if ($this->ClienteLigacoes->save($clienteLigaco)) {

            $vinculo = $this->ClienteLigacoes->find('all')
                ->where(['ClienteLigacoes.parent_id'=> $clienteLigaco->cliente_id])
                ->andWhere(['ClienteLigacoes.cliente_id'=> $clienteLigaco->parent_id]);

            if(!empty($vinculo)){
                foreach ($vinculo as $v) {
                    $Ligaco = $this->ClienteLigacoes->get($v->id);
                    $Ligaco->situacao_id = 2;
                    $this->ClienteLigacoes->save($Ligaco);
                }
            }
            $res = ['msg'=>'Deletado com sucesso'];
        } else {
            $res = ['msg'=>'Erro ao deletar'];
        }
        $this->response->body(json_encode($res));
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Cliente Ligaco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ClienteLigacoes->find('all')
        ->where(['ClienteLigacoes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ClienteLigacoes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Cliente Ligaco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $clienteLigaco = $this->ClienteLigacoes->get($this->request->data['id']);
            $res = ['nome'=>$clienteLigaco->nome,'id'=>$clienteLigaco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
