<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
/**
 * ListaAnexos Controller
 *
 * @property \App\Model\Table\ListaAnexosTable $ListaAnexos
 */
class ListaAnexosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ListaAnexos->find('all')
            ->contain(['ClienteAnexos', 'SituacaoCadastros', 'Users'])
            ->where(['ListaAnexos.situacao_id = ' => '1']);

        $cliente_anexo = null;

        if(!empty($this->request->query['cliente_anexo'])){
            $this->loadModel('ClienteAnexos');
            $clienteAnexo = $this->ClienteAnexos->findById($this->request->query['cliente_anexo'])->first();
            if(!empty($clienteAnexo)) {
                $cliente_anexo = $clienteAnexo;
                $query->andWhere(['anexo_id'=>$clienteAnexo->id]);
            }
        }

        $listaAnexos = $this->paginate($query);

        $this->set(compact('listaAnexos','cliente_anexo'));
        $this->set('_serialize', ['listaAnexos']);
    }

    /**
     * View method
     *
     * @param string|null $id Lista Anexo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $listaAnexo = $this->ListaAnexos->get($id, [
            'contain' => ['ClienteAnexos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('listaAnexo', $listaAnexo);
        $this->set('_serialize', ['listaAnexo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($anexo_id = null)
    {
        $listaAnexo = $this->ListaAnexos->newEntity();
        if ($this->request->is('post')) {
            $numFile= count(array_filter($_FILES['caminho']['name']));
            if($numFile>0) {
                $file = $_FILES['caminho'];
                $aux = 0;
                for ($i = 0; $i < $numFile; $i++) {
                    $listaAnexo = $this->ListaAnexos->newEntity();
                    $dados = array(
                        "name" => $file['name'][$i],
                        "type" => $file['type'][$i],
                        "tmp_name" => $file['tmp_name'][$i],
                        "error" => $file['error'][$i],
                        "size" => $file['size'][$i]
                    );

                    $this->request->data['caminho'] = $dados;
                    $this->request->data['descricao_anexo'] = $file['name'][$i];
                    $listaAnexo = $this->ListaAnexos->patchEntity($listaAnexo, $this->request->data);
                    if ($this->ListaAnexos->save($listaAnexo)) {
                        $aux = 1;
                    }
                }
                if ($aux == 1) {
                    if(!empty($anexo_id)){
                        echo json_encode(array('cliente_anexo'=>$anexo_id));
                        $this->autoRender=false;
                    }else {
                        $this->Flash->success(__('O lista anexo foi salvo com sucesso!'));
                        return $this->redirect(['action' => 'index']);
                    }
                } else {
                    $this->Flash->error(__('O lista anexo não foi salvo. Por favor, tente novamente.'));
                }
            }else{
                $this->Flash->error(__('Selecione um arquivo.'));
            }
        }

        $clienteAnexos = $this->ListaAnexos->ClienteAnexos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ListaAnexos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ListaAnexos->Users->find('list', ['limit' => 200]);
        $this->set(compact('listaAnexo', 'clienteAnexos', 'situacaoCadastros', 'users','anexo_id'));
        $this->set('_serialize', ['listaAnexo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lista Anexo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $listaAnexo = $this->ListaAnexos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $listaAnexo = $this->ListaAnexos->patchEntity($listaAnexo, $this->request->data);
            if ($this->ListaAnexos->save($listaAnexo)) {
                echo json_encode(array('cliente_anexo'=>$listaAnexo->anexo_id));
                $this->autoRender=false;
            } else {
                $this->Flash->error(__('O lista anexo não foi salvo. Por favor, tente novamente.'));
            }
        }
        $clienteAnexos = $this->ListaAnexos->ClienteAnexos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ListaAnexos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ListaAnexos->Users->find('list', ['limit' => 200]);
        $this->set(compact('listaAnexo', 'clienteAnexos', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['listaAnexo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lista Anexo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $listaAnexo = $this->ListaAnexos->get($id);
                $listaAnexo->situacao_id = 2;
        if ($this->ListaAnexos->save($listaAnexo)) {
            $this->Flash->success(__('O lista anexo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O lista anexo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function deleteModal()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $listaAnexo = $this->ListaAnexos->get($this->request->data['id']);
        $listaAnexo->situacao_id = 2;
        if ($this->ListaAnexos->save($listaAnexo)) {
            $res = 0;
        } else {
            $res = 1;
        }

        $json = json_encode(['res' => $res]);
        $this->response->body($json);
    }

    public function baixar($id){
        $data = $this->ListaAnexos->findById($id)->first();

        if (empty($data)) {
            throw new NotFoundException();
        }

        $caminho = WWW_ROOT.'files/listaanexos/caminho/'.$data->caminho_dir.'/'.$data->caminho;

        $this->response->file(
            $caminho,
            ['download'=>true,'name'=>$data->caminho]
        );
        return $this->response;
    }
}
