<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ListaDocumentos Controller
 *
 * @property \App\Model\Table\ListaDocumentosTable $ListaDocumentos
 */
class ListaDocumentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ListaDocumentos->find('all')
            ->contain(['ClienteDocumentos', 'SituacaoCadastros', 'Users'])
            ->where(['ListaDocumentos.situacao_id = ' => '1']);

        $cliente_documento= null;

        if(!empty($this->request->query['cliente_documento'])){
            $this->loadModel('ClienteDocumentos');
            $clienteDocumento = $this->ClienteDocumentos->findById($this->request->query['cliente_documento'])->first();
            if(!empty($clienteDocumento)) {
                $cliente_documento = $clienteDocumento;
                $query->andWhere(['documento_id'=>$clienteDocumento->id]);
            }
        }
        $listaDocumentos = $this->paginate($query);

        $this->set(compact('listaDocumentos','cliente_documento'));
        $this->set('_serialize', ['listaDocumentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Lista Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $listaDocumento = $this->ListaDocumentos->get($id, [
            'contain' => ['ClienteDocumentos', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('listaDocumento', $listaDocumento);
        $this->set('_serialize', ['listaDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($documento_id = null)
    {
        $listaDocumento = $this->ListaDocumentos->newEntity();
        if ($this->request->is('post')) {
            $numFile= count(array_filter($_FILES['caminho']['name']));
            if($numFile>0) {
                $file = $_FILES['caminho'];
                $aux = 0;
                for ($i = 0; $i < $numFile; $i++) {
                    $listaDocumento = $this->ListaDocumentos->newEntity();
                    $dados = array(
                        "name" => $file['name'][$i],
                        "type" => $file['type'][$i],
                        "tmp_name" => $file['tmp_name'][$i],
                        "error" => $file['error'][$i],
                        "size" => $file['size'][$i]
                    );

                    $this->request->data['caminho'] = $dados;
                    $listaDocumento = $this->ListaDocumentos->patchEntity($listaDocumento, $this->request->data);
                    if ($this->ListaDocumentos->save($listaDocumento)) {
                        $aux = 1;
                    }

                }
                if ($aux == 1) {
                    if(!empty($documento_id)){
                        echo json_encode(array('cliente_documento'=>$documento_id));
                        $this->autoRender=false;
                    }else {
                        $this->Flash->success(__('O documento foi salvo com sucesso!'));
                        return $this->redirect(['action' => 'index']);
                    }
                } else {
                    $this->Flash->error(__('O documento não foi salvo. Por favor, tente novamente.'));
                }
            }else{
                $this->Flash->error(__('Selecione um arquivo.'));
            }
        }
        $clienteDocumentos = $this->ListaDocumentos->ClienteDocumentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ListaDocumentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ListaDocumentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('listaDocumento', 'clienteDocumentos', 'situacaoCadastros', 'users','documento_id'));
        $this->set('_serialize', ['listaDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lista Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $listaDocumento = $this->ListaDocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $listaDocumento = $this->ListaDocumentos->patchEntity($listaDocumento, $this->request->data);
            if ($this->ListaDocumentos->save($listaDocumento)) {
                $this->Flash->success(__('O lista documento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O lista documento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $clienteDocumentos = $this->ListaDocumentos->ClienteDocumentos->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->ListaDocumentos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->ListaDocumentos->Users->find('list', ['limit' => 200]);
        $this->set(compact('listaDocumento', 'clienteDocumentos', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['listaDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lista Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $listaDocumento = $this->ListaDocumentos->get($id);
                $listaDocumento->situacao_id = 2;
        if ($this->ListaDocumentos->save($listaDocumento)) {
            $this->Flash->success(__('O lista documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O lista documento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
