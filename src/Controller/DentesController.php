<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dentes Controller
 *
 * @property \App\Model\Table\DentesTable $Dentes
 */
class DentesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
                        ,'conditions' => ['Dentes.situacao_id = ' => '1']
                    ];
        $dentes = $this->paginate($this->Dentes);

        $this->set(compact('dentes'));
        $this->set('_serialize', ['dentes']);
    }

    /**
     * View method
     *
     * @param string|null $id Dente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dente = $this->Dentes->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('dente', $dente);
        $this->set('_serialize', ['dente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dente = $this->Dentes->newEntity();
        if ($this->request->is('post')) {
            $dente = $this->Dentes->patchEntity($dente, $this->request->data);
            if ($this->Dentes->save($dente)) {
                $this->Flash->success(__('O dente foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O dente não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->Dentes->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Dentes->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('dente', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['dente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Dente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dente = $this->Dentes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dente = $this->Dentes->patchEntity($dente, $this->request->data);
            if ($this->Dentes->save($dente)) {
                $this->Flash->success(__('O dente foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O dente não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->Dentes->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Dentes->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('dente', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['dente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Dente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dente = $this->Dentes->get($id);
                $dente->situacao_id = 2;
        if ($this->Dentes->save($dente)) {
            $this->Flash->success(__('O dente foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O dente não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
