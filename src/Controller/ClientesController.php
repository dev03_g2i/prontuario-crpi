<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\Network\Session;
use Cake\Routing\Router;
use Symfony\Component\Config\Definition\Exception\Exception;
use DateTime;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 */
class ClientesController extends AppController
{

    public $components = array('Data', 'Json', 'Cliente', 'Configuracao');

    public $paginate = ['order' => ['Clientes.nome' => 'asc']];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $session = new Session();

        $query = $this->Clientes->find('all')
            ->contain(['MedicoResponsaveis', 'Convenios', 'EstadoCivis'])
            ->where(['Clientes.situacao_id = ' => '1']);

        if (!empty($this->request->query['nome'])) {
            $query->andWhere(['Clientes.nome LIKE' => '%' . $this->request->query['nome'] . '%']);
        }

        if (!empty($this->request->query['id'])) {
            $query->andWhere(['Clientes.id ' => $this->request->query['id']]);
        }
        if (!empty($this->request->query['arquivo'])) {
            $query->andWhere(['Clientes.arquivo ' => $this->request->query['arquivo']]);
        }
        if (isset($this->request->query['digital'])) {
            if ($this->request->query['digital'] != "") {
                $query->andWhere(['Clientes.digital' => $this->request->query['digital']]);
            }
        }
        if (!empty($session->read('open.cliente'))) {
            $query->andWhere(['Clientes.nome LIKE' => '%' . $session->read('open.cliente') . '%']);
        }

        if (!empty($this->request->query['cpf'])) {
            $query->andWhere(['Clientes.cpf ' => $this->request->query['cpf']]);
        }
        $query->andWhere(['Clientes.id <>' => '-1']);


        $clientes = $this->paginate($query);

        $open = 0;
        $pront = null;
        if (!empty($session->read('open.pront'))) {
            $open = 1;
            $pront = $session->read('open.pront');
        }

        $resp = null;
        if (!empty($session->read('open.resp'))) {
            $open = 1;
            $resp = $session->read('open.resp');
        }

        $title = "Pacientes - Lista";


        $this->set(compact('clientes', 'open', 'pront', 'resp', 'title'));
        $this->set('_serialize', ['clientes']);

        $session->delete('open.cliente');
        $session->delete('open.pront');
        $session->delete('open.resp');
    }

    public function openpront()
    {
        $session = new Session();
        if (!empty($this->request->query['pront'])) {
            $session->write('open.pront', $this->request->query['pront']);
        }

        if (!empty($this->request->query['resp'])) {
            $session->write('open.resp', $this->request->query['resp']);
        }
        if (!empty($this->request->query['nome'])) {
            $session->write('open.cliente', $this->request->query['nome']);
        }

        if (!empty($this->request->query['agenda_id'])) {
            $this->loadModel('Agendas');
            $agenda = $this->Agendas->get($this->request->query['agenda_id'], ['contain' => ['Clientes']]);
            $session->write('open.cliente', $agenda->cliente->nome);
            $session->write('open.pront', $agenda->cliente_id);

        }
        $this->redirect(['controller' => 'clientes', 'action' => 'index']);
    }

    public function lista()
    {
        $query = $this->Clientes->find('all')
            ->contain(['MedicoResponsaveis', 'Convenios', 'EstadoCivis'])
            ->where(['Clientes.situacao_id = ' => '1'])
            ->andWhere(['EXTRACT(MONTH FROM nascimento) = ' => date('m')]);
        $tipo = 'do mês';
        if (!empty($this->request->query['tipo'])) {

            if ($this->request->query['tipo'] == 1) {
                $query->andWhere(['EXTRACT(DAY FROM nascimento) = ' => date('d')]);
                $tipo = 'do dia';
            }
            if ($this->request->query['tipo'] == 2) {
                $query->andWhere(function ($exp, $q) {
                    $semana = $this->Data->semana();
                    return $exp->between('EXTRACT(DAY FROM nascimento)', $semana->domingo, $semana->sabado);
                });

                $tipo = 'da semana';

            }
        }

        $query->orderAsc('Clientes.nome');
        $clientes = $this->paginate($query);

        $this->set(compact('clientes', 'tipo'));
        $this->set('_serialize', ['clientes']);
    }

    /**
     * View method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['MedicoResponsaveis', 'Convenios', 'EstadoCivis', 'SituacaoCadastros', 'Users', 'Indicacao', 'IndicacaoItens', 'IndicacaoClientes']
        ]);

        $this->set('cliente', $cliente);
        $this->set('_serialize', ['cliente']);
    }

    public function nview($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => ['MedicoResponsaveis', 'Convenios', 'EstadoCivis', 'SituacaoCadastros', 'Users', 'UserEdit', 'Indicacao', 'IndicacaoItens', 'IndicacaoClientes', 'Religiao']
        ]);
        $title = "Pacientes - Detalhes";

        $this->set(compact('cliente', 'title'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $cliente = $this->Clientes->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['password'] = 123;
            $this->request->data['nome'] = trim($this->request->data['nome']);
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {

                if (!empty($this->request->query['agenda'])) {
                    $this->loadModel('Agendas');
                    $agenda = $this->Agendas->get($this->request->query['agenda']);
                    $agenda->cliente_id = $cliente->id;
                    $this->Agendas->save($agenda);
                }

                $this->loadModel('ClienteClone');
                $clienteclone = $this->ClienteClone->newEntity();
                $clienteclone->id = $cliente->id;
                $clienteclone->nome = $cliente->nome;
                $clienteclone->telefone = $cliente->telefone;
                $clienteclone->celular = $cliente->celular;
                $clienteclone->cep = $cliente->cep;
                $clienteclone->endereco = $cliente->endereco;
                $clienteclone->numero = $cliente->numero;
                $clienteclone->bairro = $cliente->bairro;
                $clienteclone->cidade = $cliente->cidade;
                $clienteclone->uf = $cliente->estado;
                $clienteclone->complemento = $cliente->complemento;
                $clienteclone->cpf = $cliente->cpf;
                $clienteclone->email = $cliente->email;
                $clienteclone->rg = $cliente->rg;
                $clienteclone->datacadastro = $cliente->created;
                $clienteclone->observacao = $cliente->observacao;
                $this->ClienteClone->save($clienteclone);
                $this->Flash->success(__('O cliente foi salvo com sucesso!'));
                return $this->redirect(['action' => 'nview', $cliente->id]);
                //return $this->redirect(['action' => 'edit', $cliente->id]);
            } else {
                $this->Flash->error(__('O cliente não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('Configuracoes');
        $configuracoes = $this->Configuracoes->find()->first();
        $paciente_matrvalidade = $configuracoes->paciente_matrvalidade;

        $medicoResponsaveis = $this->Clientes->MedicoResponsaveis->find('list');
        $convenios = $this->Clientes->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $estadoCivis = $this->Clientes->EstadoCivis->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Clientes->SituacaoCadastros->find('list', ['limit' => 200]);
        $indicacao = $this->Clientes->Indicacao->find('list', ['limit' => 200]);
        $users = $this->Clientes->Users->find('list', ['limit' => 200]);
        $religioes = $this->Clientes->Religiao->find('list');
        $title = "Pacientes - Cadastrar";

        $this->set(compact('cliente', 'medicoResponsaveis', 'convenios', 'estadoCivis', 'situacaoCadastros', 'users', 'indicacao', 'title', 'paciente_matrvalidade', 'religioes'));
        $this->set('_serialize', ['cliente']);
    }

    public function nadd()
    {
        $cliente = $this->Clientes->newEntity();
        $medicoResponsaveis = $this->Clientes->MedicoResponsaveis->find('list', ['limit' => 200]);
        $convenios = $this->Clientes->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $estadoCivis = $this->Clientes->EstadoCivis->find('list', ['limit' => 200]);
        $indicacao = $this->Clientes->Indicacao->find('list', ['limit' => 200]);
        $this->set(compact('cliente', 'medicoResponsaveis', 'convenios', 'estadoCivis', 'indicacao', 'fechar_modal'));
        $this->set('_serialize', ['cliente']);
    }

    public function naddSave()
    {
        $this->viewBuilder()->setLayout('ajax');
        $cliente = $this->Clientes->newEntity();
        $txt = -1;
        if ($this->request->is('post')) {
            $this->request->data['password'] = 123;
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {

                if (!empty($this->request->query['agenda'])) {
                    $this->loadModel('Agendas');
                    $agenda = $this->Agendas->get($this->request->query['agenda']);
                    $agenda->cliente_id = $cliente->id;
                    $this->Agendas->save($agenda);
                }

                $this->loadModel('ClienteClone');
                $clienteclone = $this->ClienteClone->newEntity();
                $clienteclone->id = $cliente->id;
                $clienteclone->nome = $cliente->nome;
                $clienteclone->telefone = $cliente->telefone;
                $clienteclone->celular = $cliente->celular;
                $clienteclone->cep = $cliente->cep;
                $clienteclone->endereco = $cliente->endereco;
                $clienteclone->numero = $cliente->numero;
                $clienteclone->bairro = $cliente->bairro;
                $clienteclone->cidade = $cliente->cidade;
                $clienteclone->uf = $cliente->estado;
                $clienteclone->complemento = $cliente->complemento;
                $clienteclone->cpf = $cliente->cpf;
                $clienteclone->email = $cliente->email;
                $clienteclone->rg = $cliente->rg;
                $clienteclone->datacadastro = $cliente->created;
                $clienteclone->observacao = $cliente->observacao;
                $this->ClienteClone->save($clienteclone);
            }
            $txt = ['id' => $cliente->id, 'text' => $cliente->nome];
        }
        $medicoResponsaveis = $this->Clientes->MedicoResponsaveis->find('list', ['limit' => 200]);
        $convenios = $this->Clientes->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $estadoCivis = $this->Clientes->EstadoCivis->find('list', ['limit' => 200]);
        $indicacao = $this->Clientes->Indicacao->find('list', ['limit' => 200]);
        $this->set(compact('cliente', 'medicoResponsaveis', 'convenios', 'estadoCivis', 'indicacao', 'fechar_modal'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cliente = $this->Clientes->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->request->data['login'] = $id;
            $this->request->data['nome'] = trim($this->request->data['nome']);
            $cliente = $this->Clientes->patchEntity($cliente, $this->request->data);
            if ($this->Clientes->save($cliente)) {

                $this->loadModel('ClienteClone');
                $clone = $this->ClienteClone->findById($id)->first();
                if (!empty($clone)) {
                    $clienteclone = $this->ClienteClone->get($id);
                } else {
                    $clienteclone = $this->ClienteClone->newEntity();
                }
                $clienteclone->id = $cliente->id;
                $clienteclone->nome = $cliente->nome;
                $clienteclone->telefone = $cliente->telefone;
                $clienteclone->celular = $cliente->celular;
                $clienteclone->cep = $cliente->cep;
                $clienteclone->endereco = $cliente->endereco;
                $clienteclone->numero = $cliente->numero;
                $clienteclone->bairro = $cliente->bairro;
                $clienteclone->cidade = $cliente->cidade;
                $clienteclone->uf = $cliente->estado;
                $clienteclone->complemento = $cliente->complemento;
                $clienteclone->cpf = $cliente->cpf;
                $clienteclone->email = $cliente->email;
                $clienteclone->rg = $cliente->rg;
                $clienteclone->datacadastro = $cliente->created;
                $clienteclone->observacao = $cliente->observacao;
                $this->ClienteClone->save($clienteclone);

                $this->Flash->success(__('O cliente foi salvo com sucesso.'));
                return $this->redirect(['action' => 'nview', $cliente->id]);
            } else {
                $this->Flash->error(__('O cliente não foi salvo. Por favor, tente novamente.'));
            }
        }
        $medicoResponsaveis = $this->Clientes->MedicoResponsaveis->find('list', ['limit' => 200]);
        $convenios = $this->Clientes->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $estadoCivis = $this->Clientes->EstadoCivis->find('list', ['limit' => 200]);
        $indicacao = $this->Clientes->Indicacao->find('list', ['limit' => 200]);
        $religioes = $this->Clientes->Religiao->find('list');
        $this->set(compact('cliente', 'medicoResponsaveis', 'convenios', 'estadoCivis', 'indicacao', 'fechar_modal', 'religioes'));
        $this->set('_serialize', ['cliente']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cliente id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cliente = $this->Clientes->get($id);
        $cliente->situacao_id = 2;
        if ($this->Clientes->save($cliente)) {
            $this->loadModel('ClienteClone');
            $clone = $this->ClienteClone->findById($id)->first();
            if (!empty($clone)) {
                $clienteclone = $this->ClienteClone->get($id);
                $clienteclone->status = 3;
                $this->ClienteClone->save($clienteclone);
            }

            $this->Flash->success(__('O cliente foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O cliente não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function fill()
    {
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $Clientes = $this->Clientes;

        $query = $Clientes->find('all')
            ->where(['Clientes.situacao_id =' => 1,
                'Clientes.nome LIKE ' => $termo . '%'])
            ->orWhere(['Clientes.cpf LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Clientes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome . ' - ' . $d->nascimento . ' - ' . $d->cpf);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function fillProntuario()
    {
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $Clientes = $this->Clientes;

        $query = $Clientes->find('all')
            ->where(['Clientes.situacao_id =' => 1])
            ->andWhere(['Clientes.nome LIKE ' => '%' . $termo . '%'])
            ->orWhere(['Clientes.id LIKE' => $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Clientes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->id . ' - ' . $d->nome);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function findfull()
    {
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $Clientes = $this->Clientes;

        $query = $Clientes->find('all')
            ->where(['Clientes.situacao_id =' => 1,
                'Clientes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Clientes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->nome, 'text' => $d->nome);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function isAuthorized($user)
    {
        $action = $this->request->params['action'];
        if (in_array($action, ['delete'])) {
            if ($user['grupo_id'] != 1) {
                return false;
            }
        }
        return parent::isAuthorized($user);

    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Contrato id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $cliente = $this->Clientes->get($this->request->data['id']);
            $res = ['nome' => $cliente->nome, 'id' => $cliente->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    public function prontuario($id)
    {
        $this->viewBuilder()->layout('ajax');
        $cliente = $this->Clientes->get($id, [
            'contain' => ['Convenios', 'Religiao', 'ClienteResponsaveis' => ['GrauParentescos', 'conditions' => ['grau_id in' => ['1', '2']]], 'Indicacao']
        ]);

        $agenda = null;
        if (!empty($this->request->query['agenda'])) {
            $agenda = $this->request->query['agenda'];
        }
        $idade = new Time($cliente->nascimento);
        $diff = $idade->diff(new \DateTime());
        $anos = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';
        $aniversario = $this->Cliente->aniversariante($cliente->nascimento);

        // Contadores
        $countClienteDocumentos = $this->Clientes->ClienteDocumentos->find('all')->where(['ClienteDocumentos.situacao_id' => 1])->andWhere(['ClienteDocumentos.cliente_id' => $id])->count();
        $countClienteAnexos = $this->Clientes->ClienteAnexos->find('all')->where(['ClienteAnexos.situacao_id' => 1])->andWhere(['ClienteAnexos.cliente_id' => $id])->count();

        $this->set(compact('cliente', 'anos', 'agenda', 'fechar_modal', 'countClienteDocumentos', 'countClienteAnexos', 'aniversario'));
        $this->set('_serialize', ['cliente']);

    }

    public function parcialEdit()
    {
        $this->autoRender = false;
        $res = ['res' => 'erro', 'msg' => "Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];
        if (!empty($this->request->data['name']) && !empty($this->request->data['value'])) {
            $name = $this->request->data['name'];
            $cliente = $this->Clientes->get($this->request->data['pk']);
            $this->response->type('json');
            if ($this->Clientes->save($cliente)) {
                $res = ['res' => $cliente->$name, 'msg' => 'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

    public function report($id)
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('ClienteHistoricos');
        $prontuario = $this->ClienteHistoricos->find('all')
            ->contain([
                'TipoHistorias',
                'MedicoResponsaveis'
            ])
            ->where(['ClienteHistoricos.cliente_id' => $id])
            ->order(['ClienteHistoricos.id' => 'DESC']);
        $cliente = $this->Clientes->get($id, [
            'contain' => ['ClienteResponsaveis' => ['GrauParentescos', 'conditions' => ['grau_id in' => ['1', '2']]]]
        ]);

        $json['prontuario'] = null;
        foreach ($prontuario as $p) {
            $data = new Time($p->created);
            $data = $data->format('d/m/Y');
            $json['prontuario'][] = [
                'data' => $data,
                'procedimento' => $p->tipo_historia->nome,
                'medico' => $p->medico_responsavei->nome,
                'evolucao' => $p->descricao,
            ];
        }
        $idade = new Time($cliente->nascimento);
        $diff = $idade->diff(new DateTime());
        $anos = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';
        $json['cliente'][] = [
            'prontuario' => $cliente->id,
            'nome' => $cliente->nome,
            'sexo' => $cliente->sexo,
            'idade' => $anos,
            'profissao' => $cliente->profissao,
            'mae_nome' => empty($cliente->nome_mae) ? '' : $cliente->nome_mae,
            'mae_profissao' => empty($cliente->mae_profissao) ? '' : $cliente->mae_profissao,
            'mae_idade' => empty($cliente->mae_idade) ? '' : $cliente->mae_idade,
            'pai_nome' => empty($cliente->nome_pai) ? '' : $cliente->nome_pai,
            'pai_profissao' => empty($cliente->pai_profissao) ? '' : $cliente->pai_profissao,
            'pai_idade' => empty($cliente->pai_idade) ? '' : $cliente->pai_idade
        ];
        $this->Json->create('prontuario.json', json_encode($json));
        $this->set('report', 'prontuario.mrt');
    }

    /**
     * This function is responsible to created a json for whit data of Cliente
     * Esta função é responsavel por criar um Json do cadatro de Clientes.
     */
    public function reportCadastro($id = null)
    {
        $this->viewBuilder()->layout('report_2016');
        $this->loadModel('Clientes');
        $cliente = $this->Clientes->get($id == null ? $this->request->data('id') : $id, ['contain' => ['MedicoResponsaveis', 'Convenios', 'Indicacao']]);
        $this->set('cliente', $cliente);
        $idade = new Time($cliente->nascimento);
        $diff = $idade->diff(new DateTime());
        $anos = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';
        $dataDeNascimento = date_format($cliente->nascimento, 'd/m/Y');

        $json['cliente'][] = [
            'id' => $cliente->id,
            'nome' => $cliente->nome,
            'nascimento' => $dataDeNascimento,
            'dia_nascimento' => date_format($cliente->nascimento, 'd'),
            'mes_nascimento' => date_format($cliente->nascimento, 'm'),
            'ano_nascimento' => date_format($cliente->nascimento, 'Y'),
            'arquvio' => $cliente->arquivo,
            'sexo' => $cliente->sexo,
            'cpf' => $cliente->cpf,
            'rg' => $cliente->rg,
            'cid' => $cliente->cid,
            'celular' => $cliente->celular,
            'telefone' => $cliente->telefone,
            'comercial' => $cliente->comercial,
            'cep' => $cliente->cep,
            'endereco' => $cliente->endereco,
            'numero' => $cliente->numero,
            'bairro' => $cliente->bairro,
            'estado' => $cliente->estado,
            'cidade' => $cliente->cidade,
            'email' => $cliente->email,
            'complemento' => $cliente->complemento,
            'carteira' => $cliente->matricula,
            'validade_carteira' => $cliente->validade,
            'convenio' => $cliente->has('convenio') ? $cliente->convenio->nome : 'Não Cadastrado',
            'medico' => $cliente->medico_responsavei->nome,
            'nome_pai' => $cliente->nome_pai,
            'profissao_pai' => $cliente->pai_profissao,
            'idade_pai' => $cliente->pai_idade,
            'nome_mae' => $cliente->nome_mae,
            'profissao_mae' => $cliente->mae_profissao,
            'idade_mae' => $cliente->mae_idade,
            'profissao' => $cliente->profissao,
            'estado_civil' => $cliente->estadocivi,
            'idade' => $anos,
            'indicacao' => (empty($cliente->indicacao->nome)) ? '' : $cliente->indicacao->nome
        ];

        $this->Json->create('cliente.json', json_encode($json));
        $this->set('report', $this->Configuracao->getMrtFichaPaciente());
    }

    public function validaCpf()
    {
        $this->autoRender = false;
        $this->viewBuilder()->layout('ajax');
        $this->response->type('json');
        $validation = null;

        $this->loadModel('Configuracoes');
        $this->loadModel('Clientes');

        if ($this->Configuracao->getConfigRestringeCpf() == 1) {

            $verificaCpf = $this->Clientes->find('all')
                ->where(['Clientes.cpf' => $this->request->getData('cpf')])
                ->orWhere(['Clientes.cpf' => $this->Cliente->limpaCpf($this->request->getData('cpf'))])
                ->count();

            if ($verificaCpf > 0) {
                if (!empty($this->request->getData('cpf'))) {
                    if ($this->Configuracao->getConfigDuplicaCpf() != $this->request->getData('cpf')) {
                        $validation = ['res' => 2, 'msg' => 'Este CPF já existe na base de dados!'];
                    }
                }
            }

        }

        $this->response->body(json_encode($validation));
    }

    public function verificaCarteira()
    {
        $json = ['erro' => 0];
        if (!empty($this->request->getData('convenio_id'))) {
            $this->loadModel('Convenios');
            $convenio = $this->Convenios->get($this->request->getData('convenio_id'));
        }
        if (!empty($this->request->getData('cliente_id'))) {
            if ($convenio->obrigatorio_carteira_paciente == 1) { // Obrigatório
                $cliente = $this->Clientes->get($this->request->getData('cliente_id'));
                if (empty($cliente->matricula)) {
                    $json = ['erro' => 1, 'msg' => 'A Configuração do Convênio exige o preenchimento da Matricula/Carteirinha no cadastro deste paciente.'];
                }
            }
        }

        $json = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

}
