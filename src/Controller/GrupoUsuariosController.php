<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GrupoUsuarios Controller
 *
 * @property \App\Model\Table\GrupoUsuariosTable $GrupoUsuarios
 */
class GrupoUsuariosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros']
                        ,'conditions' => ['GrupoUsuarios.situacao_id = ' => '1']
                    ];
        $grupoUsuarios = $this->paginate($this->GrupoUsuarios);

        $this->set(compact('grupoUsuarios'));
        $this->set('_serialize', ['grupoUsuarios']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupo Usuario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoUsuario = $this->GrupoUsuarios->get($id, [
            'contain' => ['SituacaoCadastros']
        ]);

        $this->set('grupoUsuario', $grupoUsuario);
        $this->set('_serialize', ['grupoUsuario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupoUsuario = $this->GrupoUsuarios->newEntity();
        if ($this->request->is('post')) {
            $grupoUsuario = $this->GrupoUsuarios->patchEntity($grupoUsuario, $this->request->data);
            if ($this->GrupoUsuarios->save($grupoUsuario)) {
                $this->loadModel('Controladores');
                $controladores = $this->Controladores->find('all')->where(['Controladores.situacao_id'=>1]);
                if(!empty($controladores)) {
                    foreach ($controladores as $controlador) {
                        $this->loadModel('GrupoControladores');
                        $GrupoControlador = $this->GrupoControladores->newEntity();
                        $GrupoControlador->grupo_id = $grupoUsuario->id;
                        $GrupoControlador->controlador_id = $controlador->id;
                        if($this->GrupoControladores->save($GrupoControlador)){
                            $this->loadModel('Acoes');
                            $acoes = $this->Acoes->find('all')->where(['Acoes.situacao_id'=>1]);
                            if(!empty($acoes)){
                                foreach ($acoes as $acao){
                                    $this->loadModel('GrupoControladorAcoes');
                                    $grupoControladorAco = $this->GrupoControladorAcoes->newEntity();
                                    $grupoControladorAco->grupo_controlador_id = $GrupoControlador->id;
                                    $grupoControladorAco->acao_id = $acao->id;
                                    $grupoControladorAco->acesso = 1;
                                    $this->GrupoControladorAcoes->save($grupoControladorAco);
                                }
                            }
                        }
                    }
                }
                $this->Flash->success(__('O grupo usuario foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo usuario não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrupoUsuarios->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('grupoUsuario', 'situacaoCadastros'));
        $this->set('_serialize', ['grupoUsuario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Usuario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoUsuario = $this->GrupoUsuarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupoUsuario = $this->GrupoUsuarios->patchEntity($grupoUsuario, $this->request->data);
            if ($this->GrupoUsuarios->save($grupoUsuario)) {
                $this->Flash->success(__('O grupo usuario foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo usuario não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrupoUsuarios->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('grupoUsuario', 'situacaoCadastros'));
        $this->set('_serialize', ['grupoUsuario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Usuario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupoUsuario = $this->GrupoUsuarios->get($id);
                $grupoUsuario->situacao_id = 2;
        if ($this->GrupoUsuarios->save($grupoUsuario)) {
            $this->Flash->success(__('O grupo usuario foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo usuario não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Action Permisso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->GrupoUsuarios->find('all')
            ->where(['GrupoUsuarios.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('GrupoUsuarios.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Action Permisso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $grupoUser = $this->GrupoUsuarios->get($this->request->data['id']);
            $res = ['nome'=>$grupoUser->nome,'id'=>$grupoUser->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
