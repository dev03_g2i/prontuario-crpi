<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TipoHistoriaModelos Controller
 *
 * @property \App\Model\Table\TipoHistoriaModelosTable $TipoHistoriaModelos
 */
class TipoHistoriaModelosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->TipoHistoriaModelos->find('all')
                ->contain(['TipoHistorias', 'SituacaoCadastros', 'Users'])
                ->where(['TipoHistoriaModelos.situacao_id' => 1])
                ->andWhere(['TipoHistoriaModelos.tipo_conteudo <> ' => 2])
                ->orderAsc('TipoHistoriaModelos.nome');

        if(!empty($this->request->query('nome'))){
            $query->andWhere(['TipoHistoriaModelos.nome LIKE ' => '%'.$this->request->query('nome').'%']);
        }

        $tipoHistoriaModelos = $this->paginate($query);

        $tipoHistorias = $this->TipoHistoriaModelos->TipoHistorias->find('list', ['limit' => 200])->where(['TipoHistorias.situacao_id' => 1])->orderAsc('TipoHistorias.nome');

        $this->set(compact('tipoHistoriaModelos', 'tipoHistorias'));
        $this->set('_serialize', ['tipoHistoriaModelos']);

    }

    /**
     * View method
     *
     * @param string|null $id Tipo Historia Modelo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tipoHistoriaModelo = $this->TipoHistoriaModelos->get($id, [
            'contain' => ['TipoHistorias', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('tipoHistoriaModelo', $tipoHistoriaModelo);
        $this->set('_serialize', ['tipoHistoriaModelo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($tipo_historia_id = null)
    {
        $tipoHistoriaModelo = $this->TipoHistoriaModelos->newEntity();
        if ($this->request->is('post')) {
            $tipoHistoriaModelo = $this->TipoHistoriaModelos->patchEntity($tipoHistoriaModelo, $this->request->data);
            if ($this->TipoHistoriaModelos->save($tipoHistoriaModelo)) {
                $this->Flash->success(__('O tipo historia modelo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo historia modelo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->loadModel('MedicoResponsaveis');
        $medico_responsaveis = $this->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'])->orderAsc('MedicoResponsaveis.nome');
        $tipoHistorias = $this->TipoHistoriaModelos->TipoHistorias->find('list', ['limit' => 200])->where(['TipoHistorias.situacao_id' => 1])->orderAsc('TipoHistorias.nome');
        $this->set(compact('tipoHistoriaModelo', 'tipoHistorias', 'medico_responsaveis'));
        $this->set('_serialize', ['tipoHistoriaModelo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tipo Historia Modelo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tipoHistoriaModelo = $this->TipoHistoriaModelos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tipoHistoriaModelo = $this->TipoHistoriaModelos->patchEntity($tipoHistoriaModelo, $this->request->data);
            if ($this->TipoHistoriaModelos->save($tipoHistoriaModelo)) {
                $this->Flash->success(__('O tipo historia modelo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tipo historia modelo não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('MedicoResponsaveis');
        $medico_responsaveis = $this->MedicoResponsaveis->find('list')->where(['MedicoResponsaveis.situacao_id'])->orderAsc('MedicoResponsaveis.nome');
        $tipoHistorias = $this->TipoHistoriaModelos->TipoHistorias->find('list', ['limit' => 200])->where(['TipoHistorias.situacao_id' => 1])->orderAsc('TipoHistorias.nome');
        $this->set(compact('tipoHistoriaModelo', 'tipoHistorias', 'medico_responsaveis'));
        $this->set('_serialize', ['tipoHistoriaModelo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tipo Historia Modelo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $tipoHistoriaModelo = $this->TipoHistoriaModelos->get($id);
                $tipoHistoriaModelo->situacao_id = 2;
        if ($this->TipoHistoriaModelos->save($tipoHistoriaModelo)) {
            $this->Flash->success(__('O tipo historia modelo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tipo historia modelo não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Tipo Historia Modelo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->TipoHistoriaModelos->find('all')
        ->where(['TipoHistoriaModelos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TipoHistoriaModelos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    public function getModelo()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $codigo_medico = $this->Auth->user('codigo_medico');
        if($this->request->data('column') == 'nome'){
            $modelo = $this->TipoHistoriaModelos->find('all')
                    ->where(['TipoHistoriaModelos.tipo_historia_id' => $this->request->data('id')])
                    ->orderAsc('TipoHistoriaModelos.nome');

            /*
              Se o usuário logado estiver um codigo do médico e este código estiver vinculado ao(s) modelo(s),
              traz os modelos deste médico e os publicos = null.
              proprietario = codigo_medico
           */
            if(!empty($codigo_medico)){
                $modelo->andWhere([
                    'or' => [
                    'TipoHistoriaModelos.proprietario' => $codigo_medico,
                    'TipoHistoriaModelos.proprietario IS' => null]
                ]);
            }

        }else if($this->request->data('column') == 'modelo'){
            $modelo = $this->TipoHistoriaModelos->get($this->request->data('id'));
        }

        $this->response->body(json_encode($modelo));
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Tipo Historia Modelo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $tipoHistoriaModelo = $this->TipoHistoriaModelos->get($this->request->data['id']);
            $res = ['nome'=>$tipoHistoriaModelo->nome,'id'=>$tipoHistoriaModelo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
