<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Mastologias Controller
 *
 * @property \App\Model\Table\MastologiasTable $Mastologias
 */
class MastologiasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ClienteHistoricos']
        ];
        $mastologias = $this->paginate($this->Mastologias);


        $this->set(compact('mastologias'));
        $this->set('_serialize', ['mastologias']);

    }

    /**
     * View method
     *
     * @param string|null $id Mastologia id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mastologia = $this->Mastologias->get($id, [
            'contain' => ['ClienteHistoricos']
        ]);

        $this->set('mastologia', $mastologia);
        $this->set('_serialize', ['mastologia']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mastologia = $this->Mastologias->newEntity();
        if ($this->request->is('post')) {
            $mastologia = $this->Mastologias->patchEntity($mastologia, $this->request->data);
            if ($this->Mastologias->save($mastologia)) {
                $this->Flash->success(__('O mastologia foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O mastologia não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('mastologia'));
        $this->set('_serialize', ['mastologia']);
    }

    public function nadd()
    {
        $mastologia = $this->Mastologias->newEntity();
        if ($this->request->is('post')) {
            $mastologia = $this->Mastologias->patchEntity($mastologia, $this->request->data);

            $this->autoRender=false;
            $this->response->type('json');

            $this->loadModel('ClienteHistoricos');
            $clienteHistorico = $this->ClienteHistoricos->newEntity();
            $clienteHistorico->cliente_id = $this->request->query('cliente_id');
            $clienteHistorico->tipohistoria_id = $this->request->query('tipohistoria_id');
            $clienteHistorico->descricao = $this->request->query('descricao');
            $clienteHistorico->medico_id = $this->request->query('medico_id');

            if($this->ClienteHistoricos->save($clienteHistorico)){
                $mastologia->cliente_historico_id = $clienteHistorico->id;
                if ($this->Mastologias->save($mastologia)) {
                    $clienteHistorico->mastologia_id = $mastologia->id;
                    if($this->ClienteHistoricos->save($clienteHistorico)){
                        $res = ['res' => 0, 'msg' => 'O mastologia foi salvo com sucesso!'];
                    }else {
                        $res = ['res' => 1, 'msg' => 'O mastologia não foi salvo. Por favor, tente novamente.'];
                    }
                }
            }

            $this->response->body(json_encode($res));
        }


        $this->set(compact('mastologia'));
        $this->set('_serialize', ['mastologia']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mastologia id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mastologia = $this->Mastologias->get($id, [
            'contain' => ['ClienteHistoricos' => ['Clientes']]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mastologia = $this->Mastologias->patchEntity($mastologia, $this->request->data);
            if ($this->Mastologias->save($mastologia)) {
                $this->Flash->success(__('O mastologia foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O mastologia não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('mastologia'));
        $this->set('_serialize', ['mastologia']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mastologia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mastologia = $this->Mastologias->get($id);
        $mastologia->situacao_id = 2;
        if ($this->Mastologias->save($mastologia)) {
            $this->Flash->success(__('O mastologia foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O mastologia não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Mastologia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->Mastologias->find('all')
            ->where(['Mastologias.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Mastologias.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Mastologia id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $mastologia = $this->Mastologias->get($this->request->data['id']);
            $res = ['nome'=>$mastologia->nome,'id'=>$mastologia->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
