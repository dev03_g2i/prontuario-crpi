<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinContasReceber Controller
 *
 * @property \App\Model\Table\FinContasReceberTable $FinContasReceber
 */
class FinContasReceberController extends AppController
{
    //Componentes externos para serem usados.
    public $components = ['Date', 'Data', 'Model', 'ContaReceber'];
    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Contas a receber');
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FinPlanoContas', 'Clientes', 'FinContabilidades', 'FinContasReceberPlanejamentos', 'FinTipoDocumento']
        ];
        
        $inicio = !empty($this->request->query(['inicio'])) ? $this->Date->formatDateForSql($this->request->query(['inicio'])) : null;
        $fim = !empty($this->request->query(['fim'])) ? $this->Date->formatDateForSql($this->request->query(['fim'])) : null;
        $planocontas = !empty($this->request->query(['planoconta'])) ? $this->request->query(['planoconta']) : null;
        $clientes = !empty($this->request->query(['cliente'])) ? $this->request->query(['cliente']) : null;
        $contabilidades = !empty($this->request->query(['contabilidade'])) ? $this->request->query(['contabilidade']) : null;
        $situacoes = !empty($this->request->query(['situacao'])) ? $this->request->query(['situacao']) : null;
        $status_contas = !empty($this->request->query(['status_conta'])) ? $this->request->query(['status_conta']) : null;

        $finContasReceber = $this->FinContasReceber->find('all')
        ->contain(['FinPlanoContas', 'Clientes', 'FinContabilidades', 'FinContasReceberPlanejamentos', 'FinTipoDocumento']);

        if (isset($inicio) && isset($fim)) {
            $finContasReceber = $finContasReceber->andWhere(['FinContasReceber.vencimento >=' => $inicio, 'FinContasReceber.vencimento <=' => $fim]);
        } else if (isset($inicio)) {
            $finContasReceber = $finContasReceber->andWhere(['FinContasReceber.vencimento >=' => $inicio]);
        } else if (isset($fim)) {
            $finContasReceber = $finContasReceber->andWhere(['FinContasReceber.vencimento <=' => $fim]);
        }

        if (isset($planocontas)) {
            $string = '';
            $last_key = key(array_slice($planocontas, -1, 1, TRUE));
            foreach($planocontas as $key => $planoconta)
            {
                if ($key === 0){
                    $string = '('.$planoconta;
                } else {
                    $string = $string.$planoconta;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }

            $finContasReceber = $finContasReceber->andWhere('(FinContasReceber.fin_plano_conta_id IN '.$string.')');
        }

        if (isset($clientes)){
            $string = '';
            $last_key = key(array_slice($clientes, -1, 1, TRUE));
            foreach($clientes as $key => $fornecedor)
            {
                if ($key === 0){
                    $string = '('.$fornecedor;
                } else {
                    $string = $string.$fornecedor;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $finContasReceber = $finContasReceber->andWhere('(FinContasReceber.cliente_id IN '.$string.')');
        }

        if (isset($contabilidades)){
            $string = '';
            $last_key = key(array_slice($contabilidades, -1, 1, TRUE));
            foreach($contabilidades as $key => $contabilidade)
            {
                if ($key === 0){
                    $string = '('.$contabilidade;
                } else {
                    $string = $string.$contabilidade;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $finContasReceber = $finContasReceber->andWhere('(FinContasReceber.fin_contabilidade_id IN '.$string.')');
        }

        if (isset($situacoes)){
            $string = '';
            foreach($situacoes as $key => $situacao)
            {   
                if ($situacao === '2')
                    $situacao = 'NULL';
                else
                    $situacao = 'NOT NULL and FinContasReceber.data_pagamento != "0000-00-00"';
                
                if ($key == 0)
                    $string = $string.'FinContasReceber.data_pagamento IS '.$situacao;
                else
                    $string = $string.' OR FinContasReceber.data_pagamento IS '.$situacao;
            }
            $finContasReceber = $finContasReceber->andWhere('('.$string.')');
        }

        if (isset($status_conta)){
            $string = '';
            $last_key = key(array_slice($status_contas, -1, 1, TRUE));
            foreach($status_contas as $key => $status)
            {
                if ($key === 0){
                    $string = '('.$status;
                } else {
                    $string = $string.$status;
                }

                if ($key < $last_key){
                    $string = $string.', ';
                } else {
                    $string = $string.')';
                }
            }
            $finContasReceber = $finContasReceber->andWhere('(FinContasReceber.situacao IN '.$string.')');
        }

        $finContasReceber = $this->paginate($finContasReceber);


        $this->set(compact('finContasReceber'));
        $this->set('_serialize', ['finContasReceber']);
    }

    public function quitacao()
    {
        $selectData = $this->getDataForSelects();
        $banco_movimentos = $selectData[0];
        $formas_pagamento = $selectData[1];

        $contasreceber = $this->FinContasReceber->find('all')
            ->contain(['FinPlanoContas', 'Clientes', 'FinContabilidades', 'FinContasReceberPlanejamentos', 'FinTipoDocumento'])->where(['FinContasReceber.data_pagamento IS NULL']);

        $contasreceber = $this->ContaReceber->filtroContasReceber($contasreceber, $this->request->getQuery());
        
        $contasreceber->order(['FinContasReceber.vencimento ASC']);
        
        $totalPeriodo = clone($contasreceber);
        $totalPeriodo = $totalPeriodo->select(['soma' =>'SUM(FinContasReceber.valor)'])->first();
        $totalPeriodo = $totalPeriodo->soma;

        $this->set(compact('contasreceber', 'planocontas', 'clientes', 'contabilidades', 'banco_movimentos', 'formas_pagamento', 'totalPeriodo'));
        $this->set('_serialize', ['contasreceber']);
    }

    //Quitando a conta a receber
    public function finishAccount()
    {
        $form = [];
        $form[] = !empty($this->request->query(['bancoMovimento'])) ? $this->request->query(['bancoMovimento']) : null;
        $form[] = !empty($this->request->query(['documento'])) ? $this->request->query(['documento']) : null;
        $form[] = !empty($this->request->query(['formaPagamento'])) ? $this->request->query(['formaPagamento']) : null;
        $form[] = !empty($this->request->query(['ids'])) ? $this->request->query(['ids']) : null;
        
        foreach($form as $data)
        {
            if (!isset($data)) return null;
        }

        $result = $this->Model->getModelData('FinMovimentos', 'finishAccounts', $form, 'receber');

        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }


    /**
     * View method
     *
     * @param string|null $id Fin Contas Receber id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finContasReceber = $this->FinContasReceber->get($id, [
            'contain' => ['FinPlanoContas', 'Clientes', 'FinContabilidades', 'FinContasReceberPlanejamentos', 'FinAbatimentosContasReceber']
        ]);

        $this->set('finContasReceber', $finContasReceber);
        $this->set('_serialize', ['finContasReceber']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finContasReceber = $this->FinContasReceber->newEntity();
        if ($this->request->is('post')) {
            $newConta = $this->request->data;

            $finContasReceber = $this->FinContasReceber->patchEntity($finContasReceber, $newConta);

            if ($this->FinContasReceber->save($finContasReceber)) {
                $this->Flash->success(__('O fin contas receber foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin contas receber não foi salvo. Por favor, tente novamente.'));
            }
        }

        $finContasReceber->multa = $finContasReceber->multa != 0 ? $finContasReceber->multa : 0;
        $finContasReceber->juros = $finContasReceber->juros != 0 ? $finContasReceber->juros : 0;
        $finContasReceber->desconto = $finContasReceber->desconto != 0 ? $finContasReceber->desconto : 0;
        $finContasReceber->valor_bruto = $finContasReceber->valor_bruto != 0 ? $finContasReceber->valor_bruto : 0;
        $finContasReceber->valor = $finContasReceber->valor != 0 ? $finContasReceber->valor : 0;

        $this->set(compact('finContasReceber'));
        $this->set('_serialize', ['finContasReceber']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Contas Receber id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finContasReceber = $this->FinContasReceber->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finContasReceber = $this->FinContasReceber->patchEntity($finContasReceber, $this->request->data);
            if ($this->FinContasReceber->save($finContasReceber)) {
                $this->Flash->success(__('O fin contas receber foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin contas receber não foi salvo. Por favor, tente novamente.'));
            }
        }

        $finContasReceber->multa = $finContasReceber->multa != 0 ? $finContasReceber->multa : 0;
        $finContasReceber->juros = $finContasReceber->juros != 0 ? $finContasReceber->juros : 0;
        $finContasReceber->desconto = $finContasReceber->desconto != 0 ? $finContasReceber->desconto : 0;
        $finContasReceber->valor_bruto = $finContasReceber->valor_bruto != 0 ? $finContasReceber->valor_bruto : 0;
        $finContasReceber->valor = $finContasReceber->valor != 0 ? $finContasReceber->valor : 0;

        $this->set(compact('finContasReceber'));
        $this->set('_serialize', ['finContasReceber']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Contas Receber id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->render(false);
        $conta = $this->FinContasReceber->get($id);
        $conta->situacao = 2;
        if ($this->FinContasReceber->save($conta)) {
            $this->Flash->success(__('A conta foi deletada com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! A conta não foi deletada! Tente novamente mais tarde.'));
        }
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Contas Receber id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    //Função para pegar os dados dos selects
    private function getDataForSelects()
    {
        // $planocontas = $this->FinContasReceber->FinPlanoContas->find('list', ['limit' => 500])->toArray();
        // $clientes = $this->FinContasReceber->Clientes->find('list', ['limit' => 200])->toArray();
        // $contabilidades = $this->FinContasReceber->FinContabilidades->find('list', ['limit' => 200])->toArray();
        $movesBank = $this->Model->getModelData('FinBancoMovimentos', 'getOpenedMovesBankName')->order(['FinBancos.nome', 'FinBancoMovimentos.id']);
        $formas_pagamento = $this->Model->getModelData('FinTipoPagamento', 'getList');

        return [$movesBank, $formas_pagamento];
    }

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinContasReceber->find('all')
        ->where(['FinContasReceber.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinContasReceber.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Contas Receber id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finContasReceber = $this->FinContasReceber->get($this->request->data['id']);
            $res = ['nome'=>$finContasReceber->nome,'id'=>$finContasReceber->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
