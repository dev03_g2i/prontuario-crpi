<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinStatus Controller
 *
 * @property \App\Model\Table\FinStatusTable $FinStatus
 */
class FinStatusController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finStatus = $this->paginate($this->FinStatus);


        $this->set(compact('finStatus'));
        $this->set('_serialize', ['finStatus']);

    }

    /**
     * View method
     *
     * @param string|null $id Fin Status id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finStatus = $this->FinStatus->get($id, [
            'contain' => []
        ]);

        $this->set('finStatus', $finStatus);
        $this->set('_serialize', ['finStatus']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finStatus = $this->FinStatus->newEntity();
        if ($this->request->is('post')) {
            $finStatus = $this->FinStatus->patchEntity($finStatus, $this->request->data);
            if ($this->FinStatus->save($finStatus)) {
                $this->Flash->success(__('O fin status foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin status não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finStatus'));
        $this->set('_serialize', ['finStatus']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Status id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finStatus = $this->FinStatus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finStatus = $this->FinStatus->patchEntity($finStatus, $this->request->data);
            if ($this->FinStatus->save($finStatus)) {
                $this->Flash->success(__('O fin status foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin status não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finStatus'));
        $this->set('_serialize', ['finStatus']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Status id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finStatus = $this->FinStatus->get($id);
                $finStatus->situacao_id = 2;
        if ($this->FinStatus->save($finStatus)) {
            $this->Flash->success(__('O fin status foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fin status não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Status id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinStatus->find('all')
        ->where(['FinStatus.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinStatus.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Status id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finStatus = $this->FinStatus->get($this->request->data['id']);
            $res = ['nome'=>$finStatus->nome,'id'=>$finStatus->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
