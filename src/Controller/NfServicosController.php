<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NfServicos Controller
 *
 * @property \App\Model\Table\NfServicosTable $NfServicos
 */
class NfServicosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['NotaFiscals']
        ];
        $nfServicos = $this->paginate($this->NfServicos);


        $this->set(compact('nfServicos'));
        $this->set('_serialize', ['nfServicos']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf Servico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nfServico = $this->NfServicos->get($id, [
            'contain' => ['NotaFiscals']
        ]);

        $this->set('nfServico', $nfServico);
        $this->set('_serialize', ['nfServico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nfServico = $this->NfServicos->newEntity();
        if ($this->request->is('post')) {
            $nfServico = $this->NfServicos->patchEntity($nfServico, $this->request->data);
            if ($this->NfServicos->save($nfServico)) {
                $this->Flash->success(__('O nf servico foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf servico não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfServico'));
        $this->set('_serialize', ['nfServico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf Servico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nfServico = $this->NfServicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nfServico = $this->NfServicos->patchEntity($nfServico, $this->request->data);
            if ($this->NfServicos->save($nfServico)) {
                $this->Flash->success(__('O nf servico foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf servico não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfServico'));
        $this->set('_serialize', ['nfServico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf Servico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nfServico = $this->NfServicos->get($id);
                $nfServico->situacao_id = 2;
        if ($this->NfServicos->save($nfServico)) {
            $this->Flash->success(__('O nf servico foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf servico não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf Servico id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->NfServicos->find('all')
        ->where(['NfServicos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('NfServicos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf Servico id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nfServico = $this->NfServicos->get($this->request->data['id']);
            $res = ['nome'=>$nfServico->nome,'id'=>$nfServico->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
