<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ItenCobrancas Controller
 *
 * @property \App\Model\Table\ItenCobrancasTable $ItenCobrancas
 */
class ItenCobrancasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->viewBuilder('ajax');
        $query = $this->ItenCobrancas->find('all')
            ->contain(['SituacaoCadastros', 'Procedimentos','Dentes','Faces','Regioes'])
            ->where(['ItenCobrancas.situacao_id = ' => '1']);
        $procedimentos = null;
        if (!empty($this->request->query['procedimento'])) {
            $this->loadModel('Procedimentos');
            $procedimentos = $this->Procedimentos->get($this->request->query['procedimento']);
            $query->andWhere(['ItenCobrancas.procedimento_id'=>$this->request->query['procedimento']]);

        }

        $itenCobrancas = $this->paginate($query);

        $this->set(compact('itenCobrancas', 'procedimentos'));
        $this->set('_serialize', ['itenCobrancas']);
    }

    /**
     * View method
     *
     * @param string|null $id Iten Cobranca id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $itenCobranca = $this->ItenCobrancas->get($id, [
            'contain' => ['SituacaoCadastros', 'Procedimentos']
        ]);

        $this->set('itenCobranca', $itenCobranca);
        $this->set('_serialize', ['itenCobranca']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder('ajax');
        $itenCobranca = $this->ItenCobrancas->newEntity();
        if ($this->request->is('post')) {
            $aux = 0;
            if (!empty($this->request->data['tipo']) && $this->request->data['tipo'] =='d') {
                if (!empty($this->request->data['dentes'])) {
                    foreach ($this->request->data['dentes'] as $d) {
                        $itenCobranca = $this->ItenCobrancas->newEntity();
                            $itenCobranca->procedimento_id = $this->request->data['procedimento_id'];
                            $itenCobranca->descricao = $d;
                            $itenCobranca->tipo = $this->request->data['tipo'];
                            if ($this->ItenCobrancas->save($itenCobranca)) {
                                $aux = 1;
                            }
                    }
                }
            }else  if (!empty($this->request->data['tipo']) && $this->request->data['tipo'] =='r') {
                if (!empty($this->request->data['regioes'])) {
                    foreach ($this->request->data['regioes'] as $d) {
                        $itenCobranca = $this->ItenCobrancas->newEntity();
                        $itenCobranca->procedimento_id = $this->request->data['procedimento_id'];
                        $itenCobranca->descricao = $d;
                        $itenCobranca->tipo = $this->request->data['tipo'];
                        if ($this->ItenCobrancas->save($itenCobranca)) {
                            $aux = 1;
                        }
                    }
                }
            }
            $n="";
            if (!empty($this->request->query['n'])) {
                $n  =1;
            }
            if ($aux == 1) {
                $res = array('procedimento'=>$itenCobranca->procedimento_id,
                            'novo'=>$n);
                echo json_encode($res);
            } else {
                echo 'erro';
            }
            $this->autoRender = false;
        }
        $procedimento = null;
        if (!empty($this->request->query['procedimento'])) {
            $this->loadModel('Procedimentos');
            $procedimento = $this->Procedimentos->get($this->request->query['procedimento']);
        }

        $this->loadModel('Dentes');
        $this->loadModel('Regioes');
        $dentes = $this->Dentes->find('all')->where(['Dentes.situacao_id' => 1]);
        $regioes = $this->Regioes->find('all')->where(['Regioes.situacao_id' => 1]);
        $procedimentos = $this->ItenCobrancas->Procedimentos->find('list', ['limit' => 200]);
        $this->set(compact('itenCobranca', 'procedimentos', 'procedimento', 'dentes', 'regioes'));
        $this->set('_serialize', ['itenCobranca']);
    }

    public function faces()
    {
        $this->viewBuilder('ajax');
        $itenCobranca = $this->ItenCobrancas->newEntity();
        if ($this->request->is('post')) {
            $aux = 0;
            $n="";
            if (!empty($this->request->query['n'])) {
                $n  =1;
            }
            if (!empty($this->request->data['tipo']) && $this->request->data['tipo'] =='f') {
                if (!empty($this->request->data['faces'])) {
                    foreach ($this->request->data['faces'] as $d) {
                        $itenCobranca = $this->ItenCobrancas->newEntity();
                        $itenCobranca->procedimento_id = $this->request->data['procedimento_id'];
                        $itenCobranca->descricao = $d;
                        $itenCobranca->tipo = $this->request->data['tipo'];
                        if ($this->ItenCobrancas->save($itenCobranca)) {
                            $aux = 1;
                        }
                    }
                }

                if ($aux == 1) {
                    $res = array('procedimento'=>$itenCobranca->procedimento_id,
                        'novo'=>$n);
                    echo json_encode($res);
                } else {
                    echo 'erro';
                }
            }else{
                $res = array('procedimento'=>$this->request->data['procedimento_id'],
                    'novo'=>$n);
                echo json_encode($res);
            }
            $this->autoRender = false;
        }
        $procedimento = null;
        if (!empty($this->request->query['procedimento'])) {
            $this->loadModel('Procedimentos');
            $procedimento = $this->Procedimentos->get($this->request->query['procedimento']);
        }

        $this->loadModel('Faces');
        $faces = $this->Faces->find('all')->where(['Faces.situacao_id' => 1]);
        $procedimentos = $this->ItenCobrancas->Procedimentos->find('list', ['limit' => 200]);
        $this->set(compact('itenCobranca', 'procedimentos', 'procedimento', 'faces'));
        $this->set('_serialize', ['itenCobranca']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Iten Cobranca id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $ids = array();
        $tipo =null;
        if(empty($id)){
            $itenCobranca = $this->ItenCobrancas->newEntity();

            $itens = $this->ItenCobrancas->find('all')
                ->contain([])
                ->where(['ItenCobrancas.situacao_id'=>1,
                    'ItenCobrancas.procedimento_id'=>$this->request->query['procedimento'],
                    'ItenCobrancas.tipo IN'=>['d','r']]);
            foreach ($itens as $iten) {
                $ids[] = $iten->tipo.$iten->descricao;
                if($iten->tipo=='d' ){
                    $tipo = 'd';
                }else{
                    $tipo = 'r';
                }
            }
        }else {
            $itenCobranca = $this->ItenCobrancas->get($id,[
                'contain' => []
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($itens)) {
                foreach ($itens as $iten) {
                    $itens_delete = $this->ItenCobrancas->get($iten->id);
                    $this->ItenCobrancas->delete($itens_delete);
                }
            }
            $aux = 0;
            if (!empty($this->request->data['tipo']) && $this->request->data['tipo'] =='d') {
                if (!empty($this->request->data['dentes'])) {
                    foreach ($this->request->data['dentes'] as $d) {
                        $itenCobranca = $this->ItenCobrancas->newEntity();
                        $itenCobranca->procedimento_id = $this->request->query['procedimento'];
                        $itenCobranca->descricao = $d;
                        $itenCobranca->tipo = $this->request->data['tipo'];
                        if ($this->ItenCobrancas->save($itenCobranca)) {
                            $aux = 1;
                        }
                    }
                }
            }else  if (!empty($this->request->data['tipo']) && $this->request->data['tipo'] =='r') {
                if (!empty($this->request->data['regioes'])) {
                    foreach ($this->request->data['regioes'] as $d) {
                        $itenCobranca = $this->ItenCobrancas->newEntity();
                        $itenCobranca->procedimento_id = $this->request->query['procedimento'];
                        $itenCobranca->descricao = $d;
                        $itenCobranca->tipo = $this->request->data['tipo'];
                        if ($this->ItenCobrancas->save($itenCobranca)) {
                            $aux = 1;
                        }
                    }
                }
            }
            if ($aux == 1) {
                echo $itenCobranca->procedimento_id;
            } else {
                echo 'erro';
            }
            $this->autoRender = false;

        }
        $this->loadModel('Dentes');
        $this->loadModel('Regioes');
        $dentes = $this->Dentes->find('all')->where(['Dentes.situacao_id' => 1]);
        $regioes = $this->Regioes->find('all')->where(['Regioes.situacao_id' => 1]);
        $this->set(compact('itenCobranca', 'ids', 'dentes','regioes','tipo'));
        $this->set('_serialize', ['itenCobranca']);
    }

    public function editfaces($id = null)
    {
        $ids = array();
        $tipo =null;
        if(empty($id)){
            $itenCobranca = $this->ItenCobrancas->newEntity();
            $itens = $this->ItenCobrancas->find('all')
                ->contain([])
                ->where(['ItenCobrancas.situacao_id'=>1,
                    'ItenCobrancas.procedimento_id'=>$this->request->query['procedimento'],
                    'ItenCobrancas.tipo IN'=>['f']]);

            foreach ($itens as $iten) {
                $ids[] = $iten->tipo.$iten->descricao;
                $tipo='f';
            }

        }else {
            $itenCobranca = $this->ItenCobrancas->get($id,[
                'contain' => []
            ]);
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            if(!empty($itens)) {
                foreach ($itens as $iten) {
                    $itens_delete = $this->ItenCobrancas->get($iten->id);
                    $this->ItenCobrancas->delete($itens_delete);
                }
            }
            $aux = 0;
            if (!empty($this->request->data['tipo']) && $this->request->data['tipo'] =='f') {
                if (!empty($this->request->data['faces'])) {
                    foreach ($this->request->data['faces'] as $d) {
                        $itenCobranca = $this->ItenCobrancas->newEntity();
                        $itenCobranca->procedimento_id = $this->request->query['procedimento'];
                        $itenCobranca->descricao = $d;
                        $itenCobranca->tipo = $this->request->data['tipo'];
                        if ($this->ItenCobrancas->save($itenCobranca)) {
                            $aux = 1;
                        }
                    }
                }
                $n="";
                if (!empty($this->request->query['n'])) {
                    $n  =1;
                }
                if ($aux == 1) {
                    $res = array('procedimento'=>$itenCobranca->procedimento_id,
                        'novo'=>$n);
                    echo json_encode($res);
                } else {
                    echo 'erro';
                }
            }else{
                echo $this->request->data['procedimento_id'];
            }
            $this->autoRender = false;

        }
        $this->loadModel('Faces');
        $faces = $this->Faces->find('all')->where(['Faces.situacao_id' => 1]);
        $this->set(compact('itenCobranca', 'ids','faces','tipo'));
        $this->set('_serialize', ['itenCobranca']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Iten Cobranca id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $itenCobranca = $this->ItenCobrancas->get($id);
        $itenCobranca->situacao_id = 2;
        if ($this->ItenCobrancas->save($itenCobranca)) {
            $this->Flash->success(__('O iten cobranca foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O iten cobranca não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
