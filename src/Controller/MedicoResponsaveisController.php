<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\File;

/**
 * MedicoResponsaveis Controller
 *
 * @property \App\Model\Table\MedicoResponsaveisTable $MedicoResponsaveis
 */
class MedicoResponsaveisController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['MedicoResponsaveis.situacao_id = ' => '1']
                    ];
        $medicoResponsaveis = $this->paginate($this->MedicoResponsaveis);

        $this->set(compact('medicoResponsaveis'));
        $this->set('_serialize', ['medicoResponsaveis']);
    }

    /**
     * View method
     *
     * @param string|null $id Medico Responsavei id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $medicoResponsavei = $this->MedicoResponsaveis->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('medicoResponsavei', $medicoResponsavei);
        $this->set('_serialize', ['medicoResponsavei']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $medicoResponsavei = $this->MedicoResponsaveis->newEntity();
        if ($this->request->is('post')) {
            $medicoResponsavei = $this->MedicoResponsaveis->patchEntity($medicoResponsavei, $this->request->data);

            if ($this->MedicoResponsaveis->save($medicoResponsavei)) {
                if($medicoResponsavei->cadastra_agenda){
                    $this->loadModel('GrupoAgendas');
                    $grupoAgenda = $this->GrupoAgendas->newEntity();
                    $grupoAgenda->nome = $medicoResponsavei->nome;
                    $grupoAgenda->medico_id = $medicoResponsavei->id;
                    $grupoAgenda->intervalo = '00:30';
                    $this->GrupoAgendas->save($grupoAgenda);
                }
                $this->Flash->success(__('O medico responsavei foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O medico responsavei não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->MedicoResponsaveis->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->MedicoResponsaveis->Users->find('list', ['limit' => 200]);
        $this->set(compact('medicoResponsavei', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['medicoResponsavei']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Medico Responsavei id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {        
        $medicoResponsavei = $this->MedicoResponsaveis->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $medicoResponsavei = $this->MedicoResponsaveis->patchEntity($medicoResponsavei, $this->request->getData());
            
            if($this->request->data['caminho']['error'] == 0){
                $medicoResponsavei->caminho = $this->request->data['caminho'];
            }else{
                $medicoResponsavei->caminho = '';
            }

            if ($this->MedicoResponsaveis->save($medicoResponsavei)) {
                $this->Flash->success(__('O medico responsavei foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O medico responsavei não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->MedicoResponsaveis->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->MedicoResponsaveis->Users->find('list', ['limit' => 200]);
        $this->set(compact('medicoResponsavei', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['medicoResponsavei']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Medico Responsavei id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $medicoResponsavei = $this->MedicoResponsaveis->get($id);
                $medicoResponsavei->situacao_id = 2;
        if ($this->MedicoResponsaveis->save($medicoResponsavei)) {
            $this->Flash->success(__('O medico responsavei foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O medico responsavei não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
    public function deleteImg($id)
    {
        $medico_responsaveis = $this->MedicoResponsaveis->get($id);
        $caminho = WWW_ROOT . 'files/medicoresponsaveis/caminho/' . $medico_responsaveis->caminho_dir . '/' . $medico_responsaveis->caminho;

        $file = new File($caminho);
        if ($file->exists()) {
            if ($file->delete()) {
                $medico_responsaveis->caminho = '';
                $medico_responsaveis->caminho_dir = '';
                $this->MedicoResponsaveis->save($medico_responsaveis);
            }
            $file->close();
        }
    }

    public function fill()
    {
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->MedicoResponsaveis->find('all')
            ->where(['MedicoResponsaveis.situacao_id =' => 1,
                'MedicoResponsaveis.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();

        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function getregistro(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['res'=>'-1','msg'=>'Erro ao buscar registro'];
        if(!empty($this->request->query['id'])) {
            $medico = $this->MedicoResponsaveis->get($this->request->query['id']);
            $res = ['res'=>$medico];
        }
        $this->response->body(json_encode($res));


    }

    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $medicos = $this->MedicoResponsaveis->get($this->request->data['id']);
            $res = ['nome'=>$medicos->nome,'id'=>$medicos->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }


}
