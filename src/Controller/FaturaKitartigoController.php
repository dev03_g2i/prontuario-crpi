<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaKitartigo Controller
 *
 * @property \App\Model\Table\FaturaKitartigoTable $FaturaKitartigo
 */
class FaturaKitartigoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->FaturaKitartigo->find('all')
                ->contain(['FaturaKit', 'EstqArtigos', 'Users', 'SituacaoCadastros'])
                ->where(['FaturaKitartigo.situacao_id' => 1]);

        $fatura_kit_id = null;
        if(!empty($this->request->query('fatura_kit_id'))){
            $fatura_kit_id = $this->request->query('fatura_kit_id');
            $query->andWhere(['FaturaKitartigo.fatura_kit_id' => $fatura_kit_id]);
        }

        $faturaKitartigo = $this->paginate($query);

        $this->set(compact('faturaKitartigo', 'fatura_kit_id'));
        $this->set('_serialize', ['faturaKitartigo']);

    }

    /**
     * View method
     *
     * @param string|null $id Fatura Kitartigo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaKitartigo = $this->FaturaKitartigo->get($id, [
            'contain' => ['FaturaKit', 'EstqArtigos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('faturaKitartigo', $faturaKitartigo);
        $this->set('_serialize', ['faturaKitartigo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fatura_kit_id = null;
        if(!empty($this->request->query('fatura_kit_id'))){
            $fatura_kit_id = $this->request->query('fatura_kit_id');
        }

        $faturaKitartigo = $this->FaturaKitartigo->newEntity();
        if ($this->request->is('post')) {
            $faturaKitartigo = $this->FaturaKitartigo->patchEntity($faturaKitartigo, $this->request->data);
            if ($this->FaturaKitartigo->save($faturaKitartigo)) {
                $this->Flash->success(__('O fatura kitartigo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura kitartigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaKitartigo', 'fatura_kit_id'));
        $this->set('_serialize', ['faturaKitartigo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Kitartigo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaKitartigo = $this->FaturaKitartigo->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaKitartigo = $this->FaturaKitartigo->patchEntity($faturaKitartigo, $this->request->data);
            if ($this->FaturaKitartigo->save($faturaKitartigo)) {
                $this->Flash->success(__('O fatura kitartigo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura kitartigo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('faturaKitartigo'));
        $this->set('_serialize', ['faturaKitartigo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Kitartigo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faturaKitartigo = $this->FaturaKitartigo->get($id);
                $faturaKitartigo->situacao_id = 2;
        if ($this->FaturaKitartigo->save($faturaKitartigo)) {
            $this->Flash->success(__('O fatura kitartigo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura kitartigo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fatura Kitartigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaturaKitartigo->find('all')
        ->where(['FaturaKitartigo.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaKitartigo.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fatura Kitartigo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faturaKitartigo = $this->FaturaKitartigo->get($this->request->data['id']);
            $res = ['nome'=>$faturaKitartigo->nome,'id'=>$faturaKitartigo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    /**
     * função essencial para atualizar campos com plugin editable
     *
     * @return void
     */
    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name']) && !empty($this->request->data['value'])){
            $name = $this->request->data['name'];
            $faturaKitarrigo = $this->FaturaKitartigo->get($this->request->data['pk']);
            $faturaKitarrigo->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->FaturaKitartigo->save($faturaKitarrigo)) {
                $res = ['res' => $faturaKitarrigo->$name,'msg'=>'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }

}
