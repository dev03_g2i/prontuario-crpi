<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GrupoControladores Controller
 *
 * @property \App\Model\Table\GrupoControladoresTable $GrupoControladores
 */
class GrupoControladoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['GrupoUsuarios', 'Controladores','GrupoControladorAcoes'=>['Acoes']]
        ];

        $where =[];
        $grupo_id = null;
        if(!empty($this->request->query['grupo_id'])){
            $grupo_id = $this->request->query['grupo_id'];
            $where['conditions'][] = ['GrupoControladores.grupo_id'=>$grupo_id];
        }

        $this->paginate = array_merge($where,$this->paginate);

        $grupoControladores = $this->paginate($this->GrupoControladores);

        $this->set(compact('grupoControladores','grupo_id'));
        $this->set('_serialize', ['grupoControladores']);

    }

    /**
     * View method
     *
     * @param string|null $id Grupo Controladore id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoControladore = $this->GrupoControladores->get($id, [
            'contain' => ['GrupoUsuarios', 'Controladores', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('grupoControladore', $grupoControladore);
        $this->set('_serialize', ['grupoControladore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupoControladore = $this->GrupoControladores->newEntity();
        if ($this->request->is('post')) {
            $grupoControladore = $this->GrupoControladores->patchEntity($grupoControladore, $this->request->data);
            if ($this->GrupoControladores->save($grupoControladore)) {
                $this->Flash->success(__('O grupo controladore foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo controladore não foi salvo. Por favor, tente novamente.'));
            }
        }
        $grupo_id = null;
        if(!empty($this->request->query['grupo_id'])){
            $grupo_id = $this->request->query['grupo_id'];
        }
        $this->set(compact('grupoControladore','grupo_id'));
        $this->set('_serialize', ['grupoControladore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Controladore id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoControladore = $this->GrupoControladores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupoControladore = $this->GrupoControladores->patchEntity($grupoControladore, $this->request->data);
            if ($this->GrupoControladores->save($grupoControladore)) {
                $this->Flash->success(__('O grupo controladore foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo controladore não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('grupoControladore'));
        $this->set('_serialize', ['grupoControladore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Controladore id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupoControladore = $this->GrupoControladores->get($id);
                $grupoControladore->situacao_id = 2;
        if ($this->GrupoControladores->save($grupoControladore)) {
            $this->Flash->success(__('O grupo controladore foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo controladore não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Grupo Controladore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->GrupoControladores->find('all')
        ->where(['GrupoControladores.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('GrupoControladores.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Grupo Controladore id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $grupoControladore = $this->GrupoControladores->get($this->request->data['id']);
            $res = ['nome'=>$grupoControladore->nome,'id'=>$grupoControladore->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
