<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaRecursos Controller
 *
 * @property \App\Model\Table\FaturaRecursosTable $FaturaRecursos
 */
class FaturaRecursosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($id = null)
    {
        $query = $this->FaturaRecursos->find('all')
            ->contain(['SituacaoFaturaRecursos'])
            ->where(['FaturaRecursos.situacao_id' => 1]);

        $atendProc_id = null;
        if(!empty($id)){
          $query->andWhere(['FaturaRecursos.atendimento_procedimento_id' => $id]);
          $atendProc_id = $id;
        }
        $faturaRecursos = $this->paginate($query);

        $sitFaturaRecursos = $this->FaturaRecursos->SituacaoFaturaRecursos->find('list')->where(['SituacaoFaturaRecursos.situacao_id' => 1])->orderAsc('SituacaoFaturaRecursos.nome');
        $this->set(compact('faturaRecursos', 'atendProc_id', 'sitFaturaRecursos'));
        $this->set('_serialize', ['faturaRecursos']);
    }

    /**
     * View method
     *
     * @param string|null $id Fatura Recurso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaRecurso = $this->FaturaRecursos->get($id, [
            'contain' => ['SituacaoFaturaRecursos', 'AtendimentoProcedimentos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('faturaRecurso', $faturaRecurso);
        $this->set('_serialize', ['faturaRecurso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id)
    {
        $faturaRecurso = $this->FaturaRecursos->newEntity();
        if ($this->request->is('post')) {
            $faturaRecurso = $this->FaturaRecursos->patchEntity($faturaRecurso, $this->request->data);
            $faturaRecurso->atendimento_procedimento_id = $id;
            if ($this->FaturaRecursos->save($faturaRecurso)) {
                $this->Flash->success(__('O fatura recurso foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura recurso não foi salvo. Por favor, tente novamente.'));
            }
        }

        $sitFaturaRecursos = $this->FaturaRecursos->SituacaoFaturaRecursos->find('list')->where(['SituacaoFaturaRecursos.situacao_id' => 1])->orderAsc('SituacaoFaturaRecursos.nome');
        $this->set(compact('faturaRecurso', 'sitFaturaRecursos'));
        $this->set('_serialize', ['faturaRecurso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Recurso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaRecurso = $this->FaturaRecursos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $faturaRecurso = $this->FaturaRecursos->patchEntity($faturaRecurso, $this->request->data);
            if ($this->FaturaRecursos->save($faturaRecurso)) {
                $this->Flash->success(__('O fatura recurso foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura recurso não foi salvo. Por favor, tente novamente.'));
            }
        }

        $sitFaturaRecursos = $this->FaturaRecursos->SituacaoFaturaRecursos->find('list')->where(['SituacaoFaturaRecursos.situacao_id' => 1])->orderAsc('SituacaoFaturaRecursos.nome');
        $this->set(compact('faturaRecurso', 'sitFaturaRecursos'));
        $this->set('_serialize', ['faturaRecurso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Recurso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $faturaRecurso = $this->FaturaRecursos->get($id);
                $faturaRecurso->situacao_id = 2;
        if ($this->FaturaRecursos->save($faturaRecurso)) {
            $this->Flash->success(__('O fatura recurso foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura recurso não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fatura Recurso id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaturaRecursos->find('all')
        ->where(['FaturaRecursos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaRecursos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fatura Recurso id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faturaRecurso = $this->FaturaRecursos->get($this->request->data['id']);
            $res = ['nome'=>$faturaRecurso->nome,'id'=>$faturaRecurso->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
