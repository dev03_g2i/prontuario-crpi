<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * LinkLigacoes Controller
 *
 * @property \App\Model\Table\LinkLigacoesTable $LinkLigacoes
 */
class LinkLigacoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users','GrauIn','GrauOut']
        ];
        $linkLigacoes = $this->paginate($this->LinkLigacoes);


        $this->set(compact('linkLigacoes'));
        $this->set('_serialize', ['linkLigacoes']);

    }

    /**
     * View method
     *
     * @param string|null $id Link Ligaco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $linkLigaco = $this->LinkLigacoes->get($id, [
            'contain' => ['SituacaoCadastros', 'Users','GrauIn','GrauOut']
        ]);

        $this->set('linkLigaco', $linkLigaco);
        $this->set('_serialize', ['linkLigaco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $linkLigaco = $this->LinkLigacoes->newEntity();
        if ($this->request->is('post')) {
            $linkLigaco = $this->LinkLigacoes->patchEntity($linkLigaco, $this->request->data);
            if ($this->LinkLigacoes->save($linkLigaco)) {
                $this->Flash->success(__('O link ligaco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O link ligaco não foi salvo. Por favor, tente novamente.'));
            }
        }
        $grau = $this->LinkLigacoes->GrauIn->find('list')->orderAsc('GrauIn.nome');
        $this->set(compact('linkLigaco','grau'));
        $this->set('_serialize', ['linkLigaco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Link Ligaco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $linkLigaco = $this->LinkLigacoes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $linkLigaco = $this->LinkLigacoes->patchEntity($linkLigaco, $this->request->data);
            if ($this->LinkLigacoes->save($linkLigaco)) {
                $this->Flash->success(__('O link ligaco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O link ligaco não foi salvo. Por favor, tente novamente.'));
            }
        }
        $grau = $this->LinkLigacoes->GrauIn->find('list')->orderAsc('GrauIn.nome');
        $this->set(compact('linkLigaco','grau'));
        $this->set('_serialize', ['linkLigaco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Link Ligaco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $linkLigaco = $this->LinkLigacoes->get($id);
                $linkLigaco->situacao_id = 2;
        if ($this->LinkLigacoes->save($linkLigaco)) {
            $this->Flash->success(__('O link ligaco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O link ligaco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Link Ligaco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->LinkLigacoes->find('all')
        ->where(['LinkLigacoes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('LinkLigacoes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Link Ligaco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $linkLigaco = $this->LinkLigacoes->get($this->request->data['id']);
            $res = ['nome'=>$linkLigaco->nome,'id'=>$linkLigaco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
