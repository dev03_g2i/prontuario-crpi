<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FinTipoDocumento Controller
 *
 * @property \App\Model\Table\FinTipoDocumentoTable $FinTipoDocumento
 */
class FinTipoDocumentoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FinStatus']
        ];
        $finTipoDocumento = $this->paginate($this->FinTipoDocumento);


        $this->set(compact('finTipoDocumento'));
        $this->set('_serialize', ['finTipoDocumento']);

    }

    /**
     * View method
     *
     * @param string|null $id Fin Tipo Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finTipoDocumento = $this->FinTipoDocumento->get($id, [
            'contain' => ['FinStatus', 'FinContasPagar']
        ]);

        $this->set('finTipoDocumento', $finTipoDocumento);
        $this->set('_serialize', ['finTipoDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finTipoDocumento = $this->FinTipoDocumento->newEntity();
        if ($this->request->is('post')) {
            $finTipoDocumento = $this->FinTipoDocumento->patchEntity($finTipoDocumento, $this->request->data);
            if ($this->FinTipoDocumento->save($finTipoDocumento)) {
                $this->Flash->success(__('O fin tipo documento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin tipo documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finTipoDocumento'));
        $this->set('_serialize', ['finTipoDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Tipo Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finTipoDocumento = $this->FinTipoDocumento->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $finTipoDocumento = $this->FinTipoDocumento->patchEntity($finTipoDocumento, $this->request->data);
            if ($this->FinTipoDocumento->save($finTipoDocumento)) {
                $this->Flash->success(__('O fin tipo documento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O fin tipo documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('finTipoDocumento'));
        $this->set('_serialize', ['finTipoDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Tipo Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finTipoDocumento = $this->FinTipoDocumento->get($id);
                $finTipoDocumento->situacao_id = 2;
        if ($this->FinTipoDocumento->save($finTipoDocumento)) {
            $this->Flash->success(__('O fin tipo documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fin tipo documento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fin Tipo Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FinTipoDocumento->find('all')
        ->where(['FinTipoDocumento.descricao LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinTipoDocumento.descricao');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->descricao);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fin Tipo Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $finTipoDocumento = $this->FinTipoDocumento->get($this->request->data['id']);
            $res = ['nome'=>$finTipoDocumento->nome,'id'=>$finTipoDocumento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
