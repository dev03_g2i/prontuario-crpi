<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoFaturaencerramentos Controller
 *
 * @property \App\Model\Table\SituacaoFaturaencerramentosTable $SituacaoFaturaencerramentos
 */
class SituacaoFaturaencerramentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $situacaoFaturaencerramentos = $this->paginate($this->SituacaoFaturaencerramentos);


        $this->set(compact('situacaoFaturaencerramentos'));
        $this->set('_serialize', ['situacaoFaturaencerramentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Situacao Faturaencerramento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('situacaoFaturaencerramento', $situacaoFaturaencerramento);
        $this->set('_serialize', ['situacaoFaturaencerramento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->newEntity();
        if ($this->request->is('post')) {
            $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->patchEntity($situacaoFaturaencerramento, $this->request->data);
            if ($this->SituacaoFaturaencerramentos->save($situacaoFaturaencerramento)) {
                $this->Flash->success(__('O situacao faturaencerramento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao faturaencerramento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoFaturaencerramento'));
        $this->set('_serialize', ['situacaoFaturaencerramento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Faturaencerramento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->patchEntity($situacaoFaturaencerramento, $this->request->data);
            if ($this->SituacaoFaturaencerramentos->save($situacaoFaturaencerramento)) {
                $this->Flash->success(__('O situacao faturaencerramento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao faturaencerramento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoFaturaencerramento'));
        $this->set('_serialize', ['situacaoFaturaencerramento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Faturaencerramento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->get($id);
                $situacaoFaturaencerramento->situacao_id = 2;
        if ($this->SituacaoFaturaencerramentos->save($situacaoFaturaencerramento)) {
            $this->Flash->success(__('O situacao faturaencerramento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao faturaencerramento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Situacao Faturaencerramento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->SituacaoFaturaencerramentos->find('all')
        ->where(['SituacaoFaturaencerramentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('SituacaoFaturaencerramentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Situacao Faturaencerramento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $situacaoFaturaencerramento = $this->SituacaoFaturaencerramentos->get($this->request->data['id']);
            $res = ['nome'=>$situacaoFaturaencerramento->nome,'id'=>$situacaoFaturaencerramento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
