<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Network\Session;


/**
 * Atendimentos Controller
 *
 * @property \App\Model\Table\AtendimentosTable $Atendimentos
 */
class CepController extends AppController
{
    function busca(){
        $cep = $this->request->query['cep'];
        //$reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);
        $reg = json_decode(@file_get_contents("https://viacep.com.br/ws/$cep/json/"));
        if($reg){
            $dados['endereco']     = (string) $reg->logradouro;
            $dados['logradouro']     =(string) $reg->logradouro;
            $dados['bairro']  = (string) $reg->bairro;
            $dados['cidade']  = (string) $reg->localidade;
            $dados['uf']  = (string) $reg->uf;
        }else {
            $dados['endereco']     = null;
            $dados['logradouro']     = null;
            $dados['bairro']  = null;
            $dados['cidade']  = null;
            $dados['uf']  = null;
        }

        $this->autoRender=false;
        echo json_encode($dados);
    }

    public function beforeFilter(\Cake\Event\Event $event) {
        $this->Auth->allow(['busca']);
    }
}