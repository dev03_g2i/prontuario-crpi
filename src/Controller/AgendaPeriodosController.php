<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AgendaPeriodos Controller
 *
 * @property \App\Model\Table\AgendaPeriodosTable $AgendaPeriodos
 */
class AgendaPeriodosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $grupo_agenda_id = null;
        if(!empty($this->request->query('grupo_agenda_id'))){
            $grupo_agenda_id = $this->request->query('grupo_agenda_id');
        }

        $query = $this->AgendaPeriodos->find('all')
            ->contain(['GrupoAgendas', 'TipoAgendas'])
            ->where(['AgendaPeriodos.grupo_agenda_id' => $grupo_agenda_id])
            ->andWhere(['AgendaPeriodos.situacao_id' => 1])
            ->order(['AgendaPeriodos.dia_semana' => 'asc', 'AgendaPeriodos.inicio' => 'asc']);

        if(strval($this->request->getQuery('AgendaPeriodos__dia_semana')) === '0')
        {
            $query->andWhere(['AgendaPeriodos.dia_semana' => 0]);
        }

        $agendaPeriodos = $this->paginate($query);

        $this->set(compact('agendaPeriodos', 'grupo_agenda_id'));
        $this->set('_serialize', ['agendaPeriodos']);

    }

    /**
     * View method
     *
     * @param string|null $id Agenda Periodo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendaPeriodo = $this->AgendaPeriodos->get($id, [
            'contain' => ['GrupoAgendas']
        ]);

        $this->set('agendaPeriodo', $agendaPeriodo);
        $this->set('_serialize', ['agendaPeriodo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($grupo_agenda_id)
    {
        $agendaPeriodo = $this->AgendaPeriodos->newEntity();
        if ($this->request->is('post')) {
            $agendaPeriodo = $this->AgendaPeriodos->patchEntity($agendaPeriodo, $this->request->data);

            $this->autoRender=false;
            $this->response->type('json');

            $dia_semana = $this->request->getData('dia_semana');
            $periodo = $this->request->getData('periodo');
            $tipo_agenda = $this->request->getData('tipo_agenda_id');
            $inicio = $this->request->getData('inicio');
            $check = $this->AgendaPeriodos->checkPeriodos($dia_semana, $periodo, $grupo_agenda_id, $tipo_agenda, $inicio, null);

            if($check > 0){
                $json = json_encode(['res' => $check, 'msg' => 'Já existe um período cadastrado, por favor informe outro para continuar!']);
            }else {

                if(!empty($grupo_agenda_id)){
                    $agendaPeriodo->grupo_agenda_id = $grupo_agenda_id;
                }
                if ($this->AgendaPeriodos->save($agendaPeriodo)) {
                    $json = json_encode(['res' => 0, 'msg' => 'O agenda periodo foi salvo com sucesso!']);
                } else {
                    $json = json_encode(['res' => 1, 'msg' => 'O agenda periodo não foi salvo. Por favor, tente novamente.']);
                }
            }
            $this->response->body($json);
        }

        $tipo_agendas = $this->AgendaPeriodos->TipoAgendas->getList();
        $this->set(compact('agendaPeriodo', 'grupo_agenda_id', 'tipo_agendas'));
        $this->set('_serialize', ['agendaPeriodo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda Periodo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendaPeriodo = $this->AgendaPeriodos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendaPeriodo = $this->AgendaPeriodos->patchEntity($agendaPeriodo, $this->request->data);

            $this->autoRender=false;
            $this->response->type('json');

            $dia_semana = $this->request->getData('dia_semana');
            $periodo = $this->request->getData('periodo');
            $grupo_agenda = $this->request->getQuery('grupo_agenda_id');
            $tipo_agenda = $this->request->getData('tipo_agenda_id');
            $inicio = $this->request->getData('inicio');
            $check = $this->AgendaPeriodos->checkPeriodos($dia_semana, $periodo, $grupo_agenda, $tipo_agenda, $inicio, $id);

            if($check > 0){
                $json = json_encode(['res' => $check, 'msg' => 'Já existe um período cadastrado, por favor informe outro para continuar!']);
            }else {
                if ($this->AgendaPeriodos->save($agendaPeriodo)) {
                    $json = json_encode(['res' => 0, 'msg' => 'O agenda periodo foi salvo com sucesso!']);
                } else {
                    $json = json_encode(['res' => 1, 'msg' => 'O agenda periodo não foi salvo. Por favor, tente novamente.']);
                }
            }
            $this->response->body($json);
        }
        $tipo_agendas = $this->AgendaPeriodos->TipoAgendas->getList();
        $this->set(compact('agendaPeriodo', 'tipo_agendas'));
        $this->set('_serialize', ['agendaPeriodo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda Periodo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $agendaPeriodo = $this->AgendaPeriodos->get($id);
        $agendaPeriodo->situacao_id = 2;
        if ($this->AgendaPeriodos->save($agendaPeriodo)) {
            $this->Flash->success(__('O agenda periodo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O agenda periodo não foi deletado! Tente novamente mais tarde.'));
        }

    }

    //TODO deletar registro dentro dos modais
    public function deleteModal()
    {
        $this->autoRender=false;
        $this->response->type('json');

        //$this->request->allowMethod(['post', 'delete']);
        $agendaPeriodo = $this->AgendaPeriodos->get($this->request->data['id']);
        $agendaPeriodo->situacao_id = 2;
        if ($this->AgendaPeriodos->save($agendaPeriodo)) {
            $res = 0;
        } else {
            $res = 1;
        }

        $json = json_encode(['res' => $res, 'view' => 'index']);
        $this->response->body($json);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Agenda Periodo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->AgendaPeriodos->find('all')
            ->where(['AgendaPeriodos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AgendaPeriodos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Agenda Periodo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $agendaPeriodo = $this->AgendaPeriodos->get($this->request->data['id']);
            $res = ['nome'=>$agendaPeriodo->nome,'id'=>$agendaPeriodo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

}
