<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AtendimentoProcedimentoEquipes Controller
 *
 * @property \App\Model\Table\AtendimentoProcedimentoEquipesTable $AtendimentoProcedimentoEquipes
 */
class AtendimentoProcedimentoEquipesController extends AppController
{

    public $components = array('Atendimento');

    /**
     * Index method
     * @param $id Integer - atendimento_id
     * @return \Cake\Network\Response|null
     */
    public function index($id)
    {
        
        $query = $this->AtendimentoProcedimentoEquipes->findAllEquipes();
        
        $dados_atendimento = null;
        if(!empty($id)){
            $dados_atendimento = $this->AtendimentoProcedimentoEquipes->dadosAtendimento($id);
            $query->andWhere(['AtendimentoProcedimentoEquipes.atendimento_procedimento_id' => $id]);
        }

        $atendimentoProcedimentoEquipes = $this->paginate($query);

        $atendProcedimentos  = $this->Atendimento->procedimentos($dados_atendimento->atendimento_id);
        $this->set(compact('atendimentoProcedimentoEquipes', 'dados_atendimento', 'atendProcedimentos'));
        $this->set('_serialize', ['atendimentoProcedimentoEquipes']);
    }

    /**
     * View method
     *
     * @param string|null $id Atendimento Procedimento Equipe id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->get($id, [
            'contain' => ['AtendimentoProcedimentos', 'MedicoResponsaveis', 'TipoEquipes', 'TipoEquipegrau', 'ConfiguracaoApuracao', 'SituacaoCadastros']
        ]);

        $this->set('atendimentoProcedimentoEquipe', $atendimentoProcedimentoEquipe);
        $this->set('_serialize', ['atendimentoProcedimentoEquipe']);
    }

    /**
     * Add method
     * @param $id Integer - atendimento_id
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($id = null)
    {
        $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->newEntity();
        if ($this->request->is('post')) {            

            $atendimentoProcedimentoEquipe->atendimento_procedimento_id = $this->request->getData('atendimento_procedimento_id');

            $query = $this->AtendimentoProcedimentoEquipes->findALlEquipes();
            $total = $query->count();
            $atendimentoProcedimentoEquipe->ordem = $total+1;
            $newAtendimentoEquipe = $this->request->data;

            $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->patchEntity($atendimentoProcedimentoEquipe, $newAtendimentoEquipe);
            if ($this->AtendimentoProcedimentoEquipes->save($atendimentoProcedimentoEquipe)) {
                $this->Flash->success(__('Equipe cadastrada sucesso!'));
                return $this->redirect(['action' => 'index', $this->request->getData('atendimento_procedimento_id')]);
            } else {
                $this->Flash->error(__('Não foi possivel realizar o cadastro. Por favor, tente novamente.'));
            }
        }

        $profissionais = $this->AtendimentoProcedimentoEquipes->AtendimentoProcedimentos->MedicoResponsaveis->getList();
        $tipo_equipes = $this->AtendimentoProcedimentoEquipes->TipoEquipes->getList();
        $tipo_equipegrau = $this->AtendimentoProcedimentoEquipes->TipoEquipegrau->getList();
        $configuracao_apuracao = $this->AtendimentoProcedimentoEquipes->ConfiguracaoApuracao->getList();

        $dados_atendimento = null;
        $atendProcedimentos = null;
        if(!empty($id)){
            $dados_atendimento = $this->AtendimentoProcedimentoEquipes->dadosAtendimento($id);
            $atendProcedimentos  = $this->Atendimento->procedimentos($dados_atendimento->atendimento_id);
        }

        $this->set(compact('atendimentoProcedimentoEquipe', 'profissionais', 'tipo_equipes', 'tipo_equipegrau', 'configuracao_apuracao', 'atendProcedimentos', 'dados_atendimento'));
        $this->set('_serialize', ['atendimentoProcedimentoEquipe']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento Procedimento Equipe id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->patchEntity($atendimentoProcedimentoEquipe, $this->request->data);
            if ($this->AtendimentoProcedimentoEquipes->save($atendimentoProcedimentoEquipe)) {
                $this->Flash->success(__('O atendimento procedimento equipe foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento procedimento equipe não foi salvo. Por favor, tente novamente.'));
            }
        }
        $profissionais = $this->AtendimentoProcedimentoEquipes->AtendimentoProcedimentos->MedicoResponsaveis->getList();
        $tipo_equipes = $this->AtendimentoProcedimentoEquipes->TipoEquipes->getList();
        $tipo_equipegrau = $this->AtendimentoProcedimentoEquipes->TipoEquipegrau->getList();
        $configuracao_apuracao = $this->AtendimentoProcedimentoEquipes->ConfiguracaoApuracao->getList();
        $dados_atendimento = null;
        $atendProcedimentos = null;
        if(!empty($id)){
            $dados_atendimento = $this->AtendimentoProcedimentoEquipes->dadosAtendimento($atendimentoProcedimentoEquipe->atendimento_procedimento_id);
            $atendProcedimentos  = $this->Atendimento->procedimentos($dados_atendimento->atendimento_id);
        }

        $this->set(compact('atendimentoProcedimentoEquipe', 'profissionais', 'tipo_equipes', 'tipo_equipegrau', 'configuracao_apuracao', 'atendProcedimentos', 'dados_atendimento'));
        $this->set('_serialize', ['atendimentoProcedimentoEquipe']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento Procedimento Equipe id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->get($id);
                $atendimentoProcedimentoEquipe->situacao_id = 2;
        if ($this->AtendimentoProcedimentoEquipes->save($atendimentoProcedimentoEquipe)) {
            $this->Flash->success(__('Equipe foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! Ouve um problema ao tentar deletar o registro! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index', $atendimentoProcedimentoEquipe->atendimento_procedimento_id]);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Atendimento Procedimento Equipe id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AtendimentoProcedimentoEquipes->find('all')
        ->where(['AtendimentoProcedimentoEquipes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AtendimentoProcedimentoEquipes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Atendimento Procedimento Equipe id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $atendimentoProcedimentoEquipe = $this->AtendimentoProcedimentoEquipes->get($this->request->data['id']);
            $res = ['nome'=>$atendimentoProcedimentoEquipe->nome,'id'=>$atendimentoProcedimentoEquipe->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
