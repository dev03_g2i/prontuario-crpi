<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PrecoProcedimentos Controller
 *
 * @property \App\Model\Table\PrecoProcedimentosTable $PrecoProcedimentos
 */
class PrecoProcedimentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->PrecoProcedimentos->find('all')
            ->contain(['Convenios', 'Procedimentos', 'UserReg', 'UserAlt', 'SituacaoCadastros'])
            ->where(['PrecoProcedimentos.situacao_id' => 1]);

        if(!empty($this->request->query['procedimentos'])){
            $query->andWhere(['PrecoProcedimentos.procedimento_id'=>$this->request->query['procedimentos']]);
        }
        if(!empty($this->request->query['convenio'])){
            $query->andWhere(['PrecoProcedimentos.convenio_id'=>$this->request->query['convenio']]);
        }
        $precoProcedimentos = $this->paginate($query);

        $this->loadModel('Convenios');
        $convenios = $this->Convenios->find('list')->where(['Convenios.situacao_id'=>1]);
        $this->set(compact('precoProcedimentos','convenios'));
        $this->set('_serialize', ['precoProcedimentos']);
    }

    public function all()
    {
        $query = $this->PrecoProcedimentos->find('all')
            ->contain(['Convenios', 'Procedimentos', 'UserReg', 'UserAlt', 'SituacaoCadastros', 'ConvenioRegracalculo'])
            ->where(['PrecoProcedimentos.situacao_id' => 1])
            ->orderAsc('Procedimentos.nome');

        $dados_convenio = null;
        $convenio_id = null;
        if(!empty($this->request->getQuery('convenio'))){
            $convenio_id = $this->request->getQuery('convenio');
            $query->andWhere(['PrecoProcedimentos.convenio_id' => $convenio_id]);
            $dados_convenio = $this->PrecoProcedimentos->Convenios->get($this->request->getQuery('convenio'));
        }
        $procedimento_id = null;
        if(!empty($this->request->getQuery('procedimento'))){
            $procedimento_id = $this->request->getQuery('procedimento');
            $query->andWhere(['PrecoProcedimentos.procedimento_id' => $procedimento_id]);
        }
        if(!empty($this->request->getQuery('grupo_procedimento'))){
            $query->andWhere(['Procedimentos.grupo_id' => $this->request->getQuery('grupo_procedimento')]);
        }

        $precoProcedimentos = $this->paginate($query);

        $this->loadModel('ConvenioRegracalculo');
        $regra_calculo = $this->ConvenioRegracalculo->dataSource();

        $grupo_procedimentos = $this->PrecoProcedimentos->Procedimentos->GrupoProcedimentos->getList();
        $procedimentos = $this->PrecoProcedimentos->getProcedimentosToConvenio($convenio_id);

        $this->set(compact('precoProcedimentos', 'grupo_procedimentos', 'procedimento_id', 'convenio_id', 'dados_convenio', 'regra_calculo', 'procedimentos'));
        $this->set('_serialize', ['precoProcedimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Preco Procedimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $precoProcedimento = $this->PrecoProcedimentos->get($id, [
            'contain' => ['Convenios', 'Procedimentos', 'Users', 'SituacaoCadastros']
        ]);

        $this->set('precoProcedimento', $precoProcedimento);
        $this->set('_serialize', ['precoProcedimento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $precoProcedimento = $this->PrecoProcedimentos->newEntity();
        if ($this->request->is('post')) {
            $precoProcedimento = $this->PrecoProcedimentos->patchEntity($precoProcedimento, $this->request->data);

            $check = $this->PrecoProcedimentos->verificaProcedimento($this->request->getQuery('convenio_id'), $this->request->getData('procedimento_id'));
            if($check > 0){
                $json = ['res' => $check, 'msg' => 'Já existe um Procedimento para este Convênio!'];
            }else {
                if ($this->PrecoProcedimentos->save($precoProcedimento)) {
                    $json = ['res' => $check, 'msg' => 'O preco procedimento foi salvo com sucesso!'];
                } else {
                    $json = ['res' => $check, 'msg' => 'O preco procedimento não foi salvo. Por favor, tente novamente.'];
                }
            }
            $json_data = json_encode($json);
            $response = $this->response->withType('json')->withStringBody($json_data);
            return $response;
        }
        $convenios = $this->PrecoProcedimentos->Convenios->find('list', ['limit' => 200])->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $Procedimentos = null;
        if(!empty($this->request->query['procedimento'])){
            $Procedimentos = $this->PrecoProcedimentos->Procedimentos->get($this->request->query['procedimento']);
        }
        if(!empty($this->request->query('convenio_id'))){
            $convenio_id = $this->request->query('convenio_id');
        }else{
            $convenio_id = null;
        }
        $procedimentos = $this->PrecoProcedimentos->Procedimentos->find('list')->where(['Procedimentos.situacao_id' => 1])->orderAsc('Procedimentos.nome');
        $regra_calculo = $this->PrecoProcedimentos->ConvenioRegracalculo->getList();

        $this->set(compact('precoProcedimento', 'convenios', 'procedimentos','Procedimentos', 'convenio_id', 'regra_calculo'));
        $this->set('_serialize', ['precoProcedimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Preco Procedimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $precoProcedimento = $this->PrecoProcedimentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $precoProcedimento = $this->PrecoProcedimentos->patchEntity($precoProcedimento, $this->request->data);
            if ($this->PrecoProcedimentos->save($precoProcedimento)) {
                $this->Flash->success(__('O preco procedimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__($this->PrecoProcedimentos->error));
                //$this->Flash->error(__('O preco procedimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $convenios = $this->PrecoProcedimentos->Convenios->find('list', ['limit' => 200]);
        $procedimentos = $this->PrecoProcedimentos->Procedimentos->find('list', ['limit' => 200]);
        $this->set(compact('precoProcedimento', 'convenios', 'procedimentos'));
        $this->set('_serialize', ['precoProcedimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Preco Procedimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $precoProcedimento = $this->PrecoProcedimentos->get($id);
        $precoProcedimento->situacao_id = 2;
        if ($this->PrecoProcedimentos->save($precoProcedimento)) {
            $this->Flash->success(__('O preco procedimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O preco procedimento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }
    public function deleteModal()
    {
        $precoProcedimento = $this->PrecoProcedimentos->get($this->request->getData('id'));
        $precoProcedimento->situacao_id = 2;
        if ($this->PrecoProcedimentos->save($precoProcedimento)) {
            $json = ['res' => 0];
        } else {
            $json = ['res' => 1];
        }
        $json_data = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }

    public function getPrecoProc(){
        $preco_procedimento = $this->PrecoProcedimentos->find('all')
            ->select(['PrecoProcedimentos.codigo',  'regra' => 'PrecoProcedimentos.convenio_regracalculo_id', 'PrecoProcedimentos.valor_faturar', 'PrecoProcedimentos.valor_particular', 'Procedimentos.dias_entrega', 'PrecoProcedimentos.filme', 'PrecoProcedimentos.uco', 'PrecoProcedimentos.porte', 'PrecoProcedimentos.filme_reais'])
            ->contain(['Procedimentos'])
            ->where([
                'PrecoProcedimentos.convenio_id =' => $this->request->getData('convenio_id'),
                'PrecoProcedimentos.procedimento_id =' => $this->request->getData('procedimento_id'),
                'PrecoProcedimentos.situacao_id' => 1
            ])->first();

        $json = json_encode($preco_procedimento);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function parcialEdit(){
        $this->autoRender=false;
        $res = ['res'=>'erro','msg'=>"Erro ao salvar! \nPor favor! \nVerifique se todos os dados estão corretos!"];

        if(!empty($this->request->data['name']) && !empty($this->request->data['value'])){
            $name = $this->request->data['name'];
            $precoProc = $this->PrecoProcedimentos->get($this->request->data['pk']);
            $precoProc->$name = $this->request->data['value'];
            $this->response->type('json');
            if ($this->PrecoProcedimentos->save($precoProc)) {
                $res = ['res' => $precoProc->$name,'msg'=>'Dados salvos com sucesso!'];
            }
        }
        $this->response->body(json_encode($res));
    }


    public function vlProcedimentos()
    {

        $fill = false;
        if(!empty($this->request->getQuery())){
            $fill = true;
            $query = $this->PrecoProcedimentos->find('all')
                ->contain(['Procedimentos', 'Convenios'])
                ->where(['PrecoProcedimentos.situacao_id' => 1]);
        }else{
            $query = [];
        }

        if(!empty($this->request->getQuery('convenio'))){
            $query->andWhere(['PrecoProcedimentos.convenio_id IN' => $this->request->getQuery('convenio')]);
        }
        if(!empty($this->request->getQuery('procedimentos'))){
            $query->andWhere(['PrecoProcedimentos.procedimento_id IN ' => $this->request->getQuery('procedimentos')]);
        }

        $total = 0;
        if($fill){
            $total = $query->sumOf('valor_particular');
        }

        $precoProcedimentos = $query;
        $convenios = $this->PrecoProcedimentos->Convenios->getList();
        $this->set(compact('convenios', 'precoProcedimentos', 'fill', 'total'));
    }

    /** Ajax dos procedimentos 'cadastrados' no preco_procedimentos de um determinado convenio
     * @return static
     */
    public function procedimentosInConvenio()
    {
        $convenio = $this->request->getData('convenio');
        $procedimentos = $this->PrecoProcedimentos->getProcedimentosToConvenio($convenio);
        $json_data = json_encode($procedimentos);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }
}
