<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AtendimentoDocumentos Controller
 *
 * @property \App\Model\Table\AtendimentoDocumentosTable $AtendimentoDocumentos
 */
class AtendimentoDocumentosController extends AppController
{
    public $components = ['Atendimento', 'Data'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($atendimento_id = null)
    {
        $this->paginate = [
            'contain' => ['AtendimentoModeloDocumentos', 'ClienteResponsaveis', 'Atendimentos', 'SituacaoCadastros', 'Users']
        ];
        $atendimentoDocumentos = $this->paginate($this->AtendimentoDocumentos);

        $this->set(compact('atendimentoDocumentos', 'atendimento_id', 'documentos'));
        $this->set('_serialize', ['atendimentoDocumentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Atendimento Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimentoDocumento = $this->AtendimentoDocumentos->get($id, [
            'contain' => ['AtendimentoModeloDocumentos', 'Atendimentos', 'Users']
        ]);

        $this->set('atendimentoDocumento', $atendimentoDocumento);
        $this->set('_serialize', ['atendimentoDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($atendimento_id = null)
    {
        $atendimentoDocumento = $this->AtendimentoDocumentos->newEntity();
        if ($this->request->is('post')) {
            $atendimentoDocumento = $this->AtendimentoDocumentos->patchEntity($atendimentoDocumento, $this->request->data);
            $data_emissao = $this->Data->DataSQL($this->request->getData('data_emissao'));
            $replace_texto = $this->Atendimento->gerarModelo($this->request->getData('atendimento_modelo_documento_id'), $atendimento_id, $data_emissao);
            $atendimentoDocumento->texto_documento = $replace_texto;
            $atendimentoDocumento->atendimento_id = $atendimento_id;
            if ($this->AtendimentoDocumentos->save($atendimentoDocumento)) {
                $this->Flash->success(__('O atendimento documento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'edit', $atendimentoDocumento->id]);
            } else {
                $this->Flash->error(__('O atendimento documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $documentos = $this->AtendimentoDocumentos->AtendimentoModeloDocumentos->getList();
        $responsaveis = $this->AtendimentoDocumentos->ClienteResponsaveis->getList();
        $this->set(compact('atendimentoDocumento', 'documentos', 'responsaveis'));
        $this->set('_serialize', ['atendimentoDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendimentoDocumento = $this->AtendimentoDocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoDocumento = $this->AtendimentoDocumentos->patchEntity($atendimentoDocumento, $this->request->data);            
            if ($this->AtendimentoDocumentos->save($atendimentoDocumento)) {
                $this->Flash->success(__('O atendimento documento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $dados_paciente = $this->AtendimentoDocumentos->Atendimentos->get($atendimentoDocumento->atendimento_id, [
            'contain' => ['Clientes']
        ]);
        $documentos = $this->AtendimentoDocumentos->AtendimentoModeloDocumentos->getList();
        $responsaveis = $this->AtendimentoDocumentos->ClienteResponsaveis->getList();
        $this->set(compact('atendimentoDocumento', 'documentos', 'responsaveis', 'dados_paciente'));
        $this->set('_serialize', ['atendimentoDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $atendimentoDocumento = $this->AtendimentoDocumentos->get($id);
                $atendimentoDocumento->situacao_id = 2;
        if ($this->AtendimentoDocumentos->save($atendimentoDocumento)) {
            $this->Flash->success(__('O atendimento documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O atendimento documento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function deleteModal()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $delete = $this->AtendimentoDocumentos->get($this->request->data['id']);
        $delete->situacao_id = 2;
        $res = ($this->AtendimentoDocumentos->save($delete)) ? 0 : 1;

        $json = json_encode(['res' => $res, 'view' => 'index']);
        $this->response->body($json);
    }


    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Atendimento Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AtendimentoDocumentos->find('all')
        ->where(['AtendimentoDocumentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AtendimentoDocumentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Atendimento Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $atendimentoDocumento = $this->AtendimentoDocumentos->get($this->request->data['id']);
            $res = ['nome'=>$atendimentoDocumento->nome,'id'=>$atendimentoDocumento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
