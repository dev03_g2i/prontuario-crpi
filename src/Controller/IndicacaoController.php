<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Indicacao Controller
 *
 * @property \App\Model\Table\IndicacaoTable $Indicacao
 */
class IndicacaoController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->Indicacao->find('all')
                 ->contain(['SituacaoCadastros', 'Users'])
                 ->where(['Indicacao.situacao_id = ' => '1']);

        if (!empty($this->request->query['nome'])) {
            $query->andWhere(['Indicacao.nome LIKE' => '%' . $this->request->query['nome'] . '%']);
        }
        $indicacao = $this->paginate($query);

        $this->set(compact('indicacao'));
        $this->set('_serialize', ['indicacao']);
    }

    /**
     * View method
     *
     * @param string|null $id Indicacao id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $indicacao = $this->Indicacao->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('indicacao', $indicacao);
        $this->set('_serialize', ['indicacao']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $indicacao = $this->Indicacao->newEntity();
        if ($this->request->is('post')) {
            $indicacao = $this->Indicacao->patchEntity($indicacao, $this->request->data);
            if ($this->Indicacao->save($indicacao)) {
                $this->Flash->success(__('O indicacao foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O indicacao não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Indicacao->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Indicacao->Users->find('list', ['limit' => 200]);
        $this->set(compact('indicacao', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['indicacao']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Indicacao id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $indicacao = $this->Indicacao->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $indicacao = $this->Indicacao->patchEntity($indicacao, $this->request->data);
            if ($this->Indicacao->save($indicacao)) {
                $this->Flash->success(__('O indicacao foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O indicacao não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->Indicacao->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Indicacao->Users->find('list', ['limit' => 200]);
        $this->set(compact('indicacao', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['indicacao']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Indicacao id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $indicacao = $this->Indicacao->get($id);
                $indicacao->situacao_id = 2;
        if ($this->Indicacao->save($indicacao)) {
            $this->Flash->success(__('O indicacao foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O indicacao não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $query = $this->Indicacao->find('all')
            ->where(['Indicacao.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Indicacao.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Indicacao Iten id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
            $indicacao = $this->Indicacao->get($this->request->data['id']);
            $res = ['nome'=>$indicacao->nome,'id'=>$indicacao->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
