<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * GrupoAgendas Controller
 *
 * @property \App\Model\Table\GrupoAgendasTable $GrupoAgendas
 */
class GrupoAgendasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->GrupoAgendas->find('all')
                 ->contain(['SituacaoCadastros', 'Users', 'MedicoResponsaveis', 'Solicitantes'])
                 ->where(['GrupoAgendas.situacao_id = ' => '1'])->orderAsc('GrupoAgendas.nome');


        $grupoAgendas = $this->paginate($query);

        $this->set(compact('grupoAgendas'));
        $this->set('_serialize', ['grupoAgendas']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupo Agenda id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoAgenda = $this->GrupoAgendas->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'MedicoResponsaveis']
        ]);

        $this->set('grupoAgenda', $grupoAgenda);
        $this->set('_serialize', ['grupoAgenda']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupoAgenda = $this->GrupoAgendas->newEntity();
        if ($this->request->is('post')) {
            $grupoAgenda = $this->GrupoAgendas->patchEntity($grupoAgenda, $this->request->data);
            if ($this->GrupoAgendas->save($grupoAgenda)) {
                $this->Flash->success(__('O grupo agenda foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo agenda não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrupoAgendas->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->GrupoAgendas->Users->find('list', ['limit' => 200]);
        $medicoResponsaveis = $this->GrupoAgendas->MedicoResponsaveis->find('list', ['limit' => 200]);
        $this->set(compact('grupoAgenda', 'situacaoCadastros', 'users', 'medicoResponsaveis'));
        $this->set('_serialize', ['grupoAgenda']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Agenda id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoAgenda = $this->GrupoAgendas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupoAgenda = $this->GrupoAgendas->patchEntity($grupoAgenda, $this->request->data);
            if ($this->GrupoAgendas->save($grupoAgenda)) {
                $this->Flash->success(__('O grupo agenda foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo agenda não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->GrupoAgendas->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->GrupoAgendas->Users->find('list', ['limit' => 200]);
        $medicoResponsaveis = $this->GrupoAgendas->MedicoResponsaveis->find('list', ['limit' => 200]);
        $this->set(compact('grupoAgenda', 'situacaoCadastros', 'users', 'medicoResponsaveis'));
        $this->set('_serialize', ['grupoAgenda']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Agenda id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupoAgenda = $this->GrupoAgendas->get($id);
                $grupoAgenda->situacao_id = 2;
        if ($this->GrupoAgendas->save($grupoAgenda)) {
            $this->Flash->success(__('O grupo agenda foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo agenda não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    public function getHorarios($id)
    {
        $query = $this->GrupoAgendas->get($id);
        if (!empty($query->inicio)) {
            $hora_inicio = new Time($query->inicio);
            $hora_inicio = $hora_inicio->format('H:i');
        }
        if (!empty($query->fim)) {
            $hora_fim = new Time($query->fim);
            $hora_fim = $hora_fim->format('H:i');
        }

        if (!empty($query->intervalo)) {
            $intervalo = new Time($query->intervalo);
            $intervalo = $intervalo->format('H:i');
        }
        $dados = ['inicio' => $hora_inicio, 'fim' => $hora_fim, 'intervalo' => $intervalo];
        $json = json_encode($dados);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }

    public function getParticularidades($id = null)
    {
        $grupo = !empty($this->request->query(['grupo'])) ? $this->request->query(['grupo']) : null;

        $grupos = $this->GrupoAgendas->find('all')
            ->where(['id IN ' => explode(',', $grupo)])->toArray();

        if ($this->request->is('put')) {
            $newGrupoAgenda = $this->request->data;
            $grupoAgenda = $this->GrupoAgendas->get($newGrupoAgenda['id']);
            $grupoAgenda = $this->GrupoAgendas->patchEntity($grupoAgenda, $newGrupoAgenda);
            $grupoAgenda->particularidades = $newGrupoAgenda['particularidades'];
            if ($this->GrupoAgendas->save($grupoAgenda)) {
                $this->Flash->success(__('A particularidade foi editada com sucesso.'));
            } else {
                $this->Flash->error(__('Desculpe! Tente novamente mais tarde.'));
            }
        }

        $this->set(compact('grupos'));
        $this->set('_serialize', ['grupos']);
    }
}
