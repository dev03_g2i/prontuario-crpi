<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Tecnicos Controller
 *
 * @property \App\Model\Table\TecnicosTable $Tecnicos
 */
class TecnicosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
        ];
        $tecnicos = $this->paginate($this->Tecnicos);


        $this->set(compact('tecnicos'));
        $this->set('_serialize', ['tecnicos']);

    }

    /**
     * View method
     *
     * @param string|null $id Tecnico id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tecnico = $this->Tecnicos->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('tecnico', $tecnico);
        $this->set('_serialize', ['tecnico']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tecnico = $this->Tecnicos->newEntity();
        if ($this->request->is('post')) {
            $tecnico = $this->Tecnicos->patchEntity($tecnico, $this->request->data);
            if ($this->Tecnicos->save($tecnico)) {
                $this->Flash->success(__('O tecnico foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tecnico não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tecnico'));
        $this->set('_serialize', ['tecnico']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Tecnico id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tecnico = $this->Tecnicos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tecnico = $this->Tecnicos->patchEntity($tecnico, $this->request->data);
            if ($this->Tecnicos->save($tecnico)) {
                $this->Flash->success(__('O tecnico foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O tecnico não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tecnico'));
        $this->set('_serialize', ['tecnico']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Tecnico id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tecnico = $this->Tecnicos->get($id);
                $tecnico->situacao_id = 2;
        if ($this->Tecnicos->save($tecnico)) {
            $this->Flash->success(__('O tecnico foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O tecnico não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Tecnico id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Tecnicos->find('all')
        ->where(['Tecnicos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Tecnicos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Tecnico id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $tecnico = $this->Tecnicos->get($this->request->data['id']);
            $res = ['nome'=>$tecnico->nome,'id'=>$tecnico->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
