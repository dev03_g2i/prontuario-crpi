<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * FinMovimentos Controller
 *
 * @property \App\Model\Table\FinMovimentosTable $FinMovimentos
 */
class FinMovimentosController extends AppController
{
    //Componentes externos para serem usados.
    public $components = ['Date', 'Data', 'Movimentos', 'RelatoriosFinanceiro'];

    public function __construct($request = null, $response = null, $name = null, $eventManager = null, $components = null)
    {
        parent::__construct($request, $response, $name, $eventManager, $components);
        $this->set('title', 'Movimentos');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $finMovimentos = $this->FinMovimentos->find()->contain(['FinBancoMovimentos', 'FinPlanoContas', 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes']);

        $now = new Time();
        $initialDate = $now->format('d/m/Y');
        $initialDate = '01/' . $now->format('m/Y');
        $lastDay = $this->Date->getLastDayOfMonth($now);
        $finalDate = $lastDay . '/' . $now->format('m') . '/' . $now->format('Y');
        $bancos = $this->FinMovimentos->FinBancos->find('list');

        $resultArray = $this->Movimentos->filterMovesIndex($this->request->query(), $finMovimentos);

        $finMovimentos = $resultArray[0];
        $saldoInicial = $resultArray[1];
        $saldoFinal = $resultArray[2];
        $totalCredito = $resultArray[3];
        $totalDebito = $resultArray[4];
        $tiposRelatorios = $this->RelatoriosFinanceiro->getRelatoriosMovimentos();

        $this->set(compact('finMovimentos', 'bancos', 'initialDate', 'finalDate', 'empresas', 'saldoInicial', 'totalCredito', 'totalDebito', 'saldoFinal', 'tiposRelatorios'));
        $this->set('_serialize', ['finMovimentos']);
    }

    public function getMovesForBank()
    {
        $idBank = !empty($this->request->query(['banco'])) ? intval($this->request->query(['banco'])) : null;
        $situation = !empty($this->request->query(['situacao'])) ? intval($this->request->query(['situacao'])) : null;
        $movesDate = $this->FinMovimentos->FinBancoMovimentos->find()
            ->where(['FinBancoMovimentos.fin_banco_id' => $idBank])
            ->order('FinBancoMovimentos.ano DESC')
            ->order('FinBancoMovimentos.mes DESC');

        if ($situation === 2) {
            $movesDate->andWhere(['FinBancoMovimentos.status LIKE' => 'Encerrado']);
        } else if ($situation === 1) {
            $movesDate->andWhere(['FinBancoMovimentos.status LIKE' => 'Aberto']);
        }

        $this->set(compact('movesDate'));
        $this->set('_serialize', ['movesDate']);
    }

    public function geral()
    {
        if (!empty($this->request->query(['inicio'])) && !empty($this->request->query(['fim']))) {
            $finMovimentos = $this->FinMovimentos->find()->contain(['FinBancoMovimentos', 'FinPlanoContas' => ['FinClassificacaoContas'], 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes']);
            
            $finMovimentosTransfs = $this->FinMovimentos->find()->contain(['FinBancoMovimentos', 'FinPlanoContas' => ['FinClassificacaoContas'], 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes'])
            ->where(['FinClassificacaoContas.operacao_transferencia = ' => 1]);
            
            $result = $this->Movimentos->filterMovesGeneral($this->request->query(), $finMovimentos, $finMovimentosTransfs);
            $finMovimentos = $result[0];
            $saldoInicial = $result[1];
            $transfers = $result[2];

            $transfers = $transfers->where(['FinMovimentos.data >=' => $this->Date->formatDateForSql($this->request->query(['inicio'])), 'FinMovimentos.data <=' => $this->Date->formatDateForSql($this->request->query(['fim']))]);
            $transfers = $transfers->select(['transferencia_d' => 'TRUNCATE(SUM(FinMovimentos.debito), 2)', 'transferencia_c' => 'TRUNCATE(SUM(FinMovimentos.credito), 2)'])->first();

            $movimentosParaSaldos = clone ($finMovimentos);
            $totalDebito = $movimentosParaSaldos->select(['total_debito' => 'SUM(FinMovimentos.debito)'])->first();
            $totalDebito = $totalDebito->total_debito;

            $totalTransfD = $transfers->transferencia_d;
            $totalTransfC = $transfers->transferencia_c;

            $movimentosParaSaldos = clone ($finMovimentos);
            $totalCredito = $movimentosParaSaldos->select(['total_credito' => 'SUM(FinMovimentos.credito)'])->first();
            $totalCredito = $totalCredito->total_credito;

            $movimentosParaSaldos = clone ($finMovimentos);
            $saldoFinal = $movimentosParaSaldos->select(['saldo_final' => 'SUM(FinMovimentos.credito) - SUM(FinMovimentos.debito)'])->first();
            $saldoFinal = $saldoFinal->saldo_final + $saldoInicial;

            $finMovimentos = $this->paginate($finMovimentos);

            $finMovimentos = $this->getFinalBalance($finMovimentos, 0);
        }

        //Formatando data pra colocar nos campos do datepicker
        $initialDate = date('01/m/Y');
        $finalDate = date('t/m/Y');
        $bancos = $this->FinMovimentos->FinBancos->find('list');
        $contabilidades = $this->FinMovimentos->FinContabilidades->find('list')->toArray();
        $tiposRelatorios = $this->RelatoriosFinanceiro->getRelatoriosMovimentosGeral();

        $this->set(compact('finMovimentos', 'initialDate', 'finalDate', 'bancos', 'totalCredito', 'totalDebito', 'saldoInicial', 'saldoFinal', 'contabilidades', 'totalTransfD', 'totalTransfC', 'tiposRelatorios'));
        $this->set('_serialize', ['finMovimentos']);
    }

    /**
     * View method
     *
     * @param string|null $id Fin Movimento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $finMovimento = $this->FinMovimentos->get($id, [
            'contain' => ['FinPlanoContas', 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'FinBancoMovimentos'],
        ]);

        $this->set('finMovimento', $finMovimento);
        $this->set('_serialize', ['finMovimento']);
    }

    public function transferenciaModal($id = null)
    {
        $movimento = $this->FinMovimentos->newEntity();
        if ($this->request->is('post')) {

            $newMovimento = $this->request->data;
            $movimentos = [];

            $movimentoCredito['credito'] = $newMovimento['valor'];
            $movimentoCredito['fin_banco_movimento_id'] = $newMovimento['banco_movimento_destino'];
            $movimentoCredito['fin_contabilidade_id'] = $newMovimento['contabilidade_id'];
            $movimentoCredito['documento'] = $newMovimento['documento'];
            $movimentoCredito['data'] = $newMovimento['data'];
            $movimentoCredito['complemento'] = $newMovimento['complemento'];
            $movimentoCredito['situacao'] = 1;
            $movimentoCredito['categoria'] = 'Aberto';

            $movimentoDebito['debito'] = $newMovimento['valor'];
            $movimentoDebito['fin_banco_movimento_id'] = $newMovimento['banco_movimento_origem'];
            $movimentoDebito['fin_contabilidade_id'] = $newMovimento['contabilidade_id'];
            $movimentoDebito['documento'] = $newMovimento['documento'];
            $movimentoDebito['data'] = $newMovimento['data'];
            $movimentoDebito['complemento'] = $newMovimento['complemento'];
            $movimentoDebito['situacao'] = 1;
            $movimentoDebito['categoria'] = 'Aberto';

            $movimentos[] = $movimentoCredito;
            $movimentos[] = $movimentoDebito;

            foreach ($movimentos as $key => $finMovimento) {
                $bancoMovimento = $this->FinMovimentos->FinBancoMovimentos->get($finMovimento['fin_banco_movimento_id']);
                $banco = $bancoMovimento->fin_banco_id;
                $finMovimento['fin_banco_id'] = $banco;

                $movimento = $this->FinMovimentos->newEntity();
                $movimento = $this->FinMovimentos->patchEntity($movimento, $finMovimento);

                if ($this->FinMovimentos->save($movimento)) {
                    $transferenciasFallback[] = $movimento;
                } else {
                    if (isset($transferenciasFallback) && !empty($transferenciasFallback)) 
                        $this->fallbackTransferencias($transferenciasFallback);

                    return $this->Flash->error(__('Os movimentos não foram salvos. Por favor, tente novamente.'));
                }
            }
            
            $this->Flash->success(__('Os movimentos foram saldos com sucesso!'));
            return $this->redirect(['action' => 'index']);
        }

        $banco_movimentos = $this->FinMovimentos->FinBancoMovimentos->getOpenedMovesBankName()->order(['FinBancos.nome', 'FinBancoMovimentos.id']);
        $contabilidades = $this->FinMovimentos->FinContabilidades->find('list')->toArray();
        $movimentoAtual = date('m/Y');

        $this->set(compact('movimento', 'banco_movimentos', 'contabilidades'));
        $this->set('_serialize', ['movimento']);
        $this->render('modals/transferencia_modal');
    }

    public function controle()
    {
        $direction = !empty($this->request->query(['direction'])) ? $this->request->query(['direction']) : null;
        $sort = !empty($this->request->query(['sort'])) ? $this->request->query(['sort']) : null;
        $month = !empty($this->request->query(['month'])) ? $this->request->query(['month']) : null;
        $year = !empty($this->request->query(['year'])) ? $this->request->query(['year']) : null;
        $bank = !empty($this->request->query(['bank'])) ? $this->request->query(['bank']) : null;
        $situation = !empty($this->request->query(['situation'])) ? $this->request->query(['situation']) : null;

        $bankMoves = $this->FinMovimentos->FinBancoMovimentos->find();
        $banks = $this->FinMovimentos->FinBancos->find('list');

        if(isset($month)) $bankMoves = $bankMoves->where(['FinBancoMovimentos.mes' => $month]);

        if(isset($year)) $bankMoves = $bankMoves->where(['FinBancoMovimentos.ano' => $year]);

        if(isset($bank)) $bankMoves = $bankMoves->where(['FinBancoMovimentos.fin_banco_id' => $bank]);

        if(isset($situation)) {
            if ($situation === 1)
                $situation = 'Aberto';
            else if ($situation === 2)
                $situation = 'Encerrado';

            $bankMoves = $bankMoves->where(['FinBancoMovimentos.status' => $situation]);
        }

        $bankMoves = $bankMoves->order(['status' => 'ASC', 'ano' => 'DESC', 'mes' => 'DESC']);
        
        $this->set(compact('bankMoves', 'banks'));
        $this->set('_serialize', ['bankMoves']);
    }

    //Função para carregar o modal de editar movimentação
    public function editMovesBankModal()
    {
        $Users = TableRegistry::get('Users');

        $id = !empty($this->request->query(['bankMoveId'])) ? $this->request->query(['bankMoveId']) : null;

        $bankMove = $this->FinMovimentos->FinBancoMovimentos->get($id);

        $endUser = $Users->find()->where(['Users.id' => $bankMove->por])->first();
        
        $this->set(compact('bankMove', 'endUser'));
        $this->set('_serialize', ['bankMove']);
        $this->render('modals/editar_movimentacao_modal');
    }

    //Função para carregar o modal de finalizar movimentação
    public function finishMovesBankModal()
    {
        $id = !empty($this->request->query(['bankMoveId'])) ? $this->request->query(['bankMoveId']) : null;

        $bankMove = $this->FinMovimentos->FinBancoMovimentos->get($id);

        $this->set(compact('bankMove'));
        $this->set('_serialize', ['bankMove']);
        $this->render('modals/finalizar_movimentacao_modal');
    }
    
    //Função para finalizar movimentação e iniciar a próxima
    public function finishMovesBank()
    {
        $bankMove = $this->FinMovimentos->FinBancoMovimentos->get($this->request->query(['id']));
        $uid = $this->Auth->user('id');
        if ($bankMove->status === 'Encerrado') return null;

        $patchBankMove['id'] = $this->request->query(['id']);
        $patchBankMove['saldoFinal'] = $bankMove->calculoSaldoFinal;
        $patchBankMove['status'] = 'Encerrado';
        $patchBankMove['data'] = $bankMove['data'];
        $patchBankMove['encerradoDt'] = new Time();
        $patchBankMove['por'] = $uid;

        $bankMove = $this->FinMovimentos->FinBancoMovimentos->patchEntity($bankMove, $patchBankMove);

        if ($this->FinMovimentos->FinBancoMovimentos->save($bankMove)){

            $this->Flash->success(__('A movimentação foi finalizada com sucesso!'));

            $month = intval($bankMove['mes']);
            $year = intval($bankMove['ano']);
            if ($month < 9){
                $month = '0'.++$month;
            } else if($month === 12) {
                $month = '01';
                $year = (string)++$year;
            }

            $newBankMove['saldoInicial'] = $bankMove['saldoFinal'];
            $newBankMove['fin_banco_id'] = $bankMove['fin_banco_id'];
            $newBankMove['mes'] = $month;
            $newBankMove['ano'] = $year;
            $newBankMove['status'] = 1;
            $newBankMove['data_movimento'] = $newBankMove['mes'].'/'.$newBankMove['ano'];
            $newBankMove['data'] = new Time();

            $bankMove = $this->FinMovimentos->FinBancoMovimentos->newEntity();
            $bankMove = $this->FinMovimentos->FinBancoMovimentos->patchEntity($bankMove, $newBankMove);
            if($this->FinMovimentos->FinBancoMovimentos->save($bankMove)) {
                $this->Flash->success(__('Uma nova movimentação foi criada!'));
            } else {
                return null;
            }
        } else {
            return null;
        }
        $this->set(compact('bankMove'));
        $this->set('_serialize', ['bankMove']);
    }

    //Função para fazer o fallback da conta e do movimento em caso de erro
    private function fallbackTransferencias($transferenciasFallback)
    {
        foreach ($transferenciasFallback as $fallBack) {
            $movimento = $this->get($fallback->id);
            $this->delete($movimento);
        }
        return;
    }

    public function getOpenedMovesBankExceptThese()
    {
        $ids = !empty($this->request->query(['movesBankIds'])) ? $this->request->query(['movesBankIds']) : [];
        $banco_movimentos = $this->FinMovimentos->FinBancoMovimentos->getOpenedMovesBankName()->order(['FinBancos.nome', 'FinBancoMovimentos.id']);

        if (!empty($ids)) {
            $banco_movimentos = $banco_movimentos->where(['FinBancoMovimentos.id NOT IN ' => $ids]);
        }

        $this->set(compact('banco_movimentos'));
        $this->set('_serialize', ['banco_movimentos']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $finMovimento = $this->FinMovimentos->newEntity();
        if ($this->request->is('post')) {
            $newMovimento = $this->request->data;
            $bancoMovimento = $this->FinMovimentos->FinBancoMovimentos->get($newMovimento['fin_banco_movimento_id']);
            if ($newMovimento['tipo'] === '0') {
                unset($newMovimento['fin_fornecedor_id']);
                $newMovimento['debito'] = '0';
                $newMovimento['credito'] = $newMovimento['valor'];
            } else if ($newMovimento['tipo'] === '1') {
                unset($newMovimento['cliente_id']);
                $newMovimento['credito'] = '0';
                $newMovimento['debito'] = $newMovimento['valor'];
            }
            $newMovimento['situacao'] = 1;
            $newMovimento['categoria'] = 'Aberto';
            $newMovimento['fin_banco_id'] = $bancoMovimento->fin_banco_id;

            $finMovimento = $this->FinMovimentos->patchEntity($finMovimento, $newMovimento);

            if ($this->FinMovimentos->save($finMovimento)) {
                $this->Flash->success(__('O movimento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'add']);
            } else {
                $this->Flash->error(__('O movimento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $bancos = $this->FinMovimentos->FinBancos->find('list')->toArray();
        $contabilidades = $this->FinMovimentos->FinContabilidades->find('list')->toArray();
        $now = new Time();
        $bancoMovimentos = $this->FinMovimentos->FinBancos->FinBancoMovimentos->find()->where(['FinBancoMovimentos.status' => 'Aberto']);
        $movimentoAtual = $now->format('m/Y');

        foreach ($bancos as $key => $banco) {
            $bancos[$key] = $banco . ' - ' . $movimentoAtual;
        }

        $this->set(compact('finMovimento', 'bancos', 'contabilidades', 'bancoMovimentos'));
        $this->set('_serialize', ['finMovimento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fin Movimento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $finMovimento = $this->FinMovimentos->get($id, [
            'contain' => ['FinBancos', 'FinBancoMovimentos'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newMovimento = $this->request->data;

            $newMovimento = $this->request->data;
            $bancoMovimento = $this->FinMovimentos->FinBancoMovimentos->get($newMovimento['fin_banco_movimento_id']);
            if ($newMovimento['tipo'] === '0') {
                unset($newMovimento['fin_fornecedor_id']);
                $newMovimento['debito'] = '0';
                $newMovimento['credito'] = $newMovimento['valor'];
            } else if ($newMovimento['tipo'] === '1') {
                unset($newMovimento['cliente_id']);
                $newMovimento['credito'] = '0';
                $newMovimento['debito'] = $newMovimento['valor'];
            }
            $newMovimento['situacao'] = 1;
            $newMovimento['categoria'] = 'Aberto';
            $newMovimento['fin_banco_id'] = $bancoMovimento->fin_banco_id;

            $finMovimento = $this->FinMovimentos->patchEntity($finMovimento, $newMovimento);
            if ($this->FinMovimentos->save($finMovimento)) {
                $this->Flash->success(__('O movimento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O movimento não foi salvo. Por favor, tente novamente.'));
            }
        }
        $bancos = $this->FinMovimentos->FinBancos->find('list')->toArray();
        $contabilidades = $this->FinMovimentos->FinContabilidades->find('list')->toArray();
        $now = new Time();
        $movimentoAtual = $now->format('m/Y');
        $bancoMovimentos = $this->FinMovimentos->FinBancos->FinBancoMovimentos->find()->where(['FinBancoMovimentos.status' => 'Aberto']);
        
        foreach ($bancos as $key => $banco) {
            $bancos[$key] = $banco . ' - ' . $movimentoAtual;
        }

        $this->set(compact('finMovimento', 'bancoMovimentos', 'bancos', 'contabilidades'));
        $this->set('_serialize', ['finMovimento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fin Movimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $finMovimento = $this->FinMovimentos->get($id);
        $finMovimento->situacao = 2;
        if ($this->FinMovimentos->save($finMovimento)) {
            $this->Flash->success(__('O movimento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O movimento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    /**
     * Método busca por ajax nos select2
     *
     * @param string|null $param Fin Movimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */

    public function fill()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo)) {
            $termo = '';
        }

        if (!isset($size) || $size < 1) {
            $size = 10;
        }

        $query = $this->FinMovimentos->find('all')
            ->where(['FinMovimentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FinMovimentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int) $cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
     * Método de pesquisa de valores no editar
     *
     * @param string|null $id Fin Movimento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');
        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $finMovimento = $this->FinMovimentos->get($this->request->data['id']);
            $res = ['nome' => $finMovimento->nome, 'id' => $finMovimento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }

    private function workWithFinMovesData($moves)
    {
        $lastBalance = $this->FinMovimentos->FinBancoMovimentos->getLastMonthFinalBalance();

        foreach ($moves as $key => $move) {
            if ($key === 0) {
                $move->saldo = ($move->credito - $move->debito) + $lastBalance;
            } else {
                $move->saldo = ($move->credito - $move->debito) + $moves[$key - 1]->saldo;
            }

        }
        return $moves;
    }

    private function getBalances($date = null)
    {
        $now = new Time();
        $initialDate = $now->format('m/Y');
    }

    private function getFinalBalance($finMovimentos, $saldoInicial)
    {
        foreach ($finMovimentos as $key => $finMovimento) {
            $finMovimento->saldo = $finMovimento->credito - $finMovimento->debito;
            if ($key === 0) {
                $finMovimento->saldo = $saldoInicial + $finMovimento->saldo;
            } else {
                $finMovimento->saldo = $saldoAnterior + $finMovimento->saldo;
            }

            $saldoAnterior = $finMovimento->saldo;
        }

        return $finMovimentos;
    }
}
