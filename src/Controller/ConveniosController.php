<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Convenios Controller
 *
 * @property \App\Model\Table\ConveniosTable $Convenios
 * @property \App\Model\Table\BancoTable $Banco
 */
class ConveniosController extends AppController
{
    public $components = ['Conv', 'Data'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['TipoConvenios', 'SituacaoCadastros', 'Users', 'PrecoProcedimentos']
            , 'conditions' => ['Convenios.situacao_id = ' => '1']
        ];
        $convenios = $this->paginate($this->Convenios);

        $this->set(compact('convenios'));
        $this->set('_serialize', ['convenios']);
    }

    /**
     * View method
     *
     * @param string|null $id Convenio id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $convenio = $this->Convenios->get($id, [
            'contain' => ['TipoConvenios', 'SituacaoCadastros', 'Users', 'Atendimentos', 'PrecoProcedimentos']
        ]);

        $this->set('convenio', $convenio);
        $this->set('_serialize', ['convenio']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $convenio = $this->Convenios->newEntity();
        if ($this->request->is('post')) {
            $convenio = $this->Convenios->patchEntity($convenio, $this->request->data);
            if ($this->Convenios->save($convenio)) {
                $this->Flash->success(__('O convenio foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O convenio não foi salvo. Por favor, tente novamente.'));
            }
        }
        $this->loadModel('Planocontas');
        $planAcc = $this->Planocontas->find('list', ['limit' => 200])->contain(['Classificacaocontas'])->orderAsc('Planocontas.nome')->where(['Classificacaocontas.tipo' => 1]);
        
        $tipoCodClinica = $this->Conv->getListaTipoCodigo();
        $tipoConvenios = $this->Convenios->TipoConvenios->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Convenios->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Convenios->Users->find('list', ['limit' => 200]);
        $this->set(compact('convenio', 'tipoConvenios', 'situacaoCadastros', 'users', 'tipoCodClinica', 'planAcc'));
        $this->set('_serialize', ['convenio']);
    }

    /** new-add method - Recalcula faturas
     * @param $convenio_id
     */
    public function newAdd($convenio_id)
    {
        $convenio = $this->Convenios->get($convenio_id);
        if ($this->request->is('post')) {

            $query = $this->Convenios->PrecoProcedimentos->find('all')
                ->contain(['Procedimentos'])
                ->where(['PrecoProcedimentos.situacao_id' => 1])
                ->andWhere(['PrecoProcedimentos.convenio_id' => $convenio_id]);

            if(!empty($this->request->getData('grupo_procedimentos'))){
                $query->andWhere(['Procedimentos.grupo_id' => $this->request->getData('grupo_procedimentos')]);
            }
            $data_inicio = date('Y-m-d');
            if(!empty($this->request->getData('data_inicio'))){
                $data_inicio = $this->Data->DataSQL($this->request->getData('data_inicio'));
            }
            $data_fim = date('Y-m-d');
            if(!empty($this->request->getData('data_fim'))){
                $data_fim = $this->Data->DataSQL($this->request->getData('data_fim'));
            }

            foreach ($query as $q){
                $this->Convenios->Atendimentos->AtendimentoProcedimentos->recalculaValoresPerPeriodos($q, $data_inicio, $data_fim);
            }

            $faturaRecalcular = $this->Convenios->FaturaRecalcular->newEntity();
            $faturaRecalcular->convenio_id = $convenio_id;
            $faturaRecalcular->inicio = $this->request->getData('data_inicio');
            $faturaRecalcular->fim = $this->request->getData('data_fim');
            if ($this->Convenios->FaturaRecalcular->save($faturaRecalcular)) {
                $this->Flash->success(__('O fatura recalcular foi salvo com sucesso!'));
                return $this->redirect(['controller' => 'FaturaRecalcular', 'action' => 'index']);
            } else {
                $this->Flash->error(__('O fatura recalcular não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->loadModel('GrupoProcedimentos');
        $this->loadModel('Unidades');
        $grupo_procedimentos = $this->GrupoProcedimentos->getList();
        $unidades = $this->Unidades->getList();

        $this->set(compact( 'grupo_procedimentos', 'unidades', 'faturaRecalcular', 'convenio'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Convenio id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $convenio = $this->Convenios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $convenio = $this->Convenios->patchEntity($convenio, $this->request->data);
            if ($this->Convenios->save($convenio)) {
                $this->Flash->success(__('O convenio foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O convenio não foi salvo. Por favor, tente novamente.'));
            }
        }
        
        $this->loadModel('Planocontas');
        $planAcc = $this->Planocontas->find('list', ['limit' => 200])->contain(['Classificacaocontas'])->orderAsc('Planocontas.nome')->where(['Classificacaocontas.tipo' => 1]);

        $tipoCodClinica = $this->Conv->getListaTipoCodigo();
        $tipoConvenios = $this->Convenios->TipoConvenios->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->Convenios->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->Convenios->Users->find('list', ['limit' => 200]);
        $this->set(compact('convenio', 'tipoConvenios', 'situacaoCadastros', 'users', 'tipoCodClinica', 'planAcc'));
        $this->set('_serialize', ['convenio']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Convenio id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $convenio = $this->Convenios->get($id);
        $convenio->situacao_id = 2;
        if ($this->Convenios->save($convenio)) {
            $this->Flash->success(__('O convenio foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O convenio não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function fill()
    {
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];

        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
            $termo = '';
        if (!isset($size) || $size < 1)
            $size = 10;

        $Convenios = $this->Convenios;
        
        $query = $Convenios->find('all')
            ->where(['Convenios.situacao_id =' => 1,
                'Convenios.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Convenios.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
            $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        echo json_encode($ret);

        $this->autoRender = false;
    }

    public function getedit()
    {
        $this->autoRender = false;
        $this->response->type('json');

        $res = ['nome' => 'selecione', 'id' => null];
        if (!empty($this->request->data['id'])) {
            $convenio = $this->Convenios->get($this->request->data['id']);
            $res = ['nome' => $convenio->nome, 'id' => $convenio->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
