<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AtendimentoModeloDocumentos Controller
 *
 * @property \App\Model\Table\AtendimentoModeloDocumentosTable $AtendimentoModeloDocumentos
 */
class AtendimentoModeloDocumentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $atendimentoModeloDocumentos = $this->paginate($this->AtendimentoModeloDocumentos);


        $this->set(compact('atendimentoModeloDocumentos'));
        $this->set('_serialize', ['atendimentoModeloDocumentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Atendimento Modelo Documento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'AtendimentoDocumentos']
        ]);

        $this->set('atendimentoModeloDocumento', $atendimentoModeloDocumento);
        $this->set('_serialize', ['atendimentoModeloDocumento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->newEntity();
        if ($this->request->is('post')) {
            $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->patchEntity($atendimentoModeloDocumento, $this->request->data);
            if ($this->AtendimentoModeloDocumentos->save($atendimentoModeloDocumento)) {
                $this->Flash->success(__('O atendimento modelo documento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento modelo documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('atendimentoModeloDocumento'));
        $this->set('_serialize', ['atendimentoModeloDocumento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Atendimento Modelo Documento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->patchEntity($atendimentoModeloDocumento, $this->request->data);
            if ($this->AtendimentoModeloDocumentos->save($atendimentoModeloDocumento)) {
                $this->Flash->success(__('O atendimento modelo documento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O atendimento modelo documento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('atendimentoModeloDocumento'));
        $this->set('_serialize', ['atendimentoModeloDocumento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Atendimento Modelo Documento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->get($id);
                $atendimentoModeloDocumento->situacao_id = 2;
        if ($this->AtendimentoModeloDocumentos->save($atendimentoModeloDocumento)) {
            $this->Flash->success(__('O atendimento modelo documento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O atendimento modelo documento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Atendimento Modelo Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AtendimentoModeloDocumentos->find('all')
        ->where(['AtendimentoModeloDocumentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AtendimentoModeloDocumentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Atendimento Modelo Documento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $atendimentoModeloDocumento = $this->AtendimentoModeloDocumentos->get($this->request->data['id']);
            $res = ['nome'=>$atendimentoModeloDocumento->nome,'id'=>$atendimentoModeloDocumento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
