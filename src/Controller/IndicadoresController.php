<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use DateTime;
/**
 * Agendas Controller
 *
 * @property \App\Model\Table\AgendasTable $Agendas
 */
class IndicadoresController extends AppController
{

    public $components = array('Data', 'Indicador');

    public function index(){

        $codigo_medico = $this->Auth->user('codigo_agenda');
        $data = new Time(date('Y-m-d'));
        $dat = new Time(date('Y-m-').'01');
        $first = $dat;
        $this->loadModel('SituacaoAgendas');
        $this->loadModel('Agendas');

        $situacaoAgendas = $this->SituacaoAgendas->find('all')
            ->where(['SituacaoAgendas.situacao_id'=>1]);

        $agendas =  array();

        $data_semana = $this->Data->data_semana();
        foreach ($situacaoAgendas as $situacaoAgenda) {
            $agendas[$situacaoAgenda->nome]['mes'] = $this->Agendas->getCountAgendasMedico($codigo_medico, $situacaoAgenda->id,$first->format('Y-m-d H:i:s'))->total;
            $agendas[$situacaoAgenda->nome]['semana'] = $this->Agendas->getCountAgendasMedico($codigo_medico, $situacaoAgenda->id,$data_semana->domingo.' 06:00:00',$data_semana->sabado.' 20:00:00')->total;
            $agendas[$situacaoAgenda->nome]['dia'] = $this->Agendas->getCountAgendasMedico($codigo_medico, $situacaoAgenda->id,date('Y-m-d').' 06:00:00',date('Y-m-d').' 23:59:00')->total;
            $agendas[$situacaoAgenda->nome]['cor'] = $situacaoAgenda->cor;
        }
        $this->set(compact(['agendas']));
    }

    public function princ(){

    }
    public function all(){

        $this->loadModel('Retornos');
        $this->loadModel('Clientes');

        $expirados = $this->Retornos->find()
            ->select(['total'=>'count(Retornos.id)'])
            ->where(['Retornos.previsao <'=>date('Y-m-d')])
            ->andWhere(['Retornos.situacao_id'=>1])
            ->andWhere(['Retornos.situacao_retorno_id'=>1])
            ->first();

        $reprogramados= $this->Retornos->find()
            ->select(['total'=>'count(Retornos.id)'])
            ->where(['Retornos.situacao_id'=>1])
            ->andWhere(['Retornos.situacao_retorno_id'=>3])
            ->first();

        $programado = $this->Retornos->find()
            ->select(['total'=>'count(Retornos.id)'])
            ->where(['Retornos.situacao_id'=>1])
            ->andWhere(['Retornos.previsao >='=>date('Y-m-d')])
            ->andWhere(['Retornos.situacao_retorno_id'=>1])
            ->first();

        $clientes = $this->Clientes->find()
            ->select(['total'=>'count(Clientes.id)'])
            ->where(['Clientes.situacao_id'=>1])
            ->first();


        $data = new Time(date('Y-m-d'));
        $dat = new Time(date('Y-m-').'01');
        $first = $dat;
        $this->loadModel('Atendimentos');
        $atendimentos = $this->Atendimentos->find('all',[
            'contain' => ['AtendimentoProcedimentos' => [ 'AtendimentoItens']]
        ])
            ->where(['Atendimentos.data >= '=>$first->format('Y-m-d')])
            ->andWhere(['Atendimentos.tipo '=>0])
            ->andWhere(['Atendimentos.situacao_id'=>1]);

        $tratamentos= $this->Atendimentos->find()
            ->select(['total'=>'count(Atendimentos.id)'])
            ->where(['Atendimentos.data >= '=>$first->format('Y-m-d')])
            ->andWhere(['Atendimentos.tipo '=>1])
            ->andWhere(['Atendimentos.situacao_id '=>1])
            ->first();

        $this->loadModel('Procedimentos');
        $procedimentos = $this->Procedimentos->find('all')
                ->where(['Procedimentos.situacao_id' => 1])
                ->count();

        $parcial=0;
        $aprovado=0;
        $reprovado=0;
        foreach ($atendimentos as $atendimento) {
            $aprov = 0;
            $reprov = 0;
            if (!empty($atendimento->atendimento_procedimentos)) {
                foreach ($atendimento->atendimento_procedimentos as $anted) {
                    foreach ($anted->atendimento_itens as $iten_cobranca) {
                        ($iten_cobranca->status == 0) ? $reprov++ : $aprov++;
                    }
                }
                if ($reprov > 0 && $aprov > 0) {
                    $parcial++;
                } else if ($aprov > 0 && $reprov <= 0) {
                    $aprovado++;
                } else {
                    $reprovado++;
                }
            }
        }

        $this->loadModel('SituacaoAgendas');
        $this->loadModel('Agendas');

        $situacaoAgendas = $this->SituacaoAgendas->find('all')
            ->where(['SituacaoAgendas.situacao_id'=>1]);

        $agendas =  array();

        $data_semana = $this->Data->data_semana();
        foreach ($situacaoAgendas as $situacaoAgenda) {
            $agendas[$situacaoAgenda->nome]['mes'] = $this->Agendas->getCountAgendas($situacaoAgenda->id,$first->format('Y-m-d H:i:s'))->total;
            $agendas[$situacaoAgenda->nome]['semana'] = $this->Agendas->getCountAgendas($situacaoAgenda->id,$data_semana->domingo.' 06:00:00',$data_semana->sabado.' 20:00:00')->total;
            $agendas[$situacaoAgenda->nome]['dia'] = $this->Agendas->getCountAgendas($situacaoAgenda->id,date('Y-m-d').' 06:00:00',date('Y-m-d').' 23:59:00')->total;
            $agendas[$situacaoAgenda->nome]['cor'] = $situacaoAgenda->cor;
        }

        $title = "Indicadores";
        $this->set(compact('dia', 'mes', 'semana','expirados','reprogramados','programado','clientes','parcial','reprovado','tratamentos','agendas','title', 'procedimentos'));
    }

    public function pacientes()
    {
        $this->loadModel('Clientes');
        $clientes = $this->Clientes->find('all')
            ->where(['Clientes.situacao_id' => 1])
            ->andWhere(['MONTH(nascimento)' => date('m')])
            ->extract('nascimento');

        $qtTotalPacientes = $this->Clientes->find('all')
            ->select(['qtTotal' => 'count(*)'])
            ->where(['Clientes.situacao_id' => 1])
            ->first();

        $qtTotalPacientes = $qtTotalPacientes->qtTotal;

        $actualDate = new Time();
        $lastMonthDate = (new Time())->subMonth(1);

        $stringMesAnoAtual = $this->Data->mesAtual($actualDate);
        $stringMesAnteriorAnoAtual = $this->Data->mesAtual($lastMonthDate);

        $qtTotalPacientesNovosAtual = $this->Clientes->find('all')
            ->select(['qtTotal' => 'count(*)'])
            ->where(['Clientes.situacao_id' => 1])
            ->andWhere(['MONTH(Clientes.created)' => $actualDate->format('m'),
            'YEAR(Clientes.created)' => $actualDate->format('Y')])
            ->first();

        $qtTotalPacientesNovosAtual = $qtTotalPacientesNovosAtual->qtTotal;

        $qtTotalPacientesNovosAnterior = $this->Clientes->find('all')
            ->select(['qtTotal' => 'count(*)'])
            ->where(['Clientes.situacao_id' => 1])
            ->andWhere(['MONTH(Clientes.created)' => $lastMonthDate->format('m'),
            'YEAR(Clientes.created)' => $lastMonthDate->format('Y')])
            ->first();

        $qtTotalPacientesNovosAnterior = $qtTotalPacientesNovosAnterior->qtTotal;

        $dia = 0;
        $semana = 0;
        $mes = 0;
        $dados_semana = $this->Data->semana();
        if(!empty($clientes)) {
            foreach ($clientes as $nascimento) {
                if (!empty($nascimento)) {
                    $nasc = new Time($nascimento);
                    $day = $nasc->format('d');
                    if ($day == date('d')) {
                        $dia++;
                    }
                    if ($day >= $dados_semana->domingo && $day <= $dados_semana->sabado) {
                        $semana++;
                    }
                }
                $mes++;
            }
        }
        $this->set(compact('dia', 'semana', 'mes', 'qtTotalPacientes', 'stringMesAnoAtual', 'stringMesAnteriorAnoAtual', 'qtTotalPacientesNovosAtual', 'qtTotalPacientesNovosAnterior'));
    }

    public function atendGeral()
    {
        $meses = $this->Data->meses();
        $anos = $this->Data->years();
        $this->set(compact('meses', 'anos'));
    }

    public function chartLineProcedimento()
    {
        if($this->request->is('post')){

            $mes = (!empty($this->request->getData('mes'))) ? $this->request->getData('mes') : date('m');
            $ano = (!empty($this->request->getData('ano'))) ? $this->request->getData('ano') : date('Y');

            $res = [
                'dias' => $this->Data->getDaysMonth($ano, $mes),
                'procedimentos' => [
                    'dataSets' => $this->Indicador->getProcedimentosPerGrupo($mes, $ano)
                ]
            ];

            $json = json_encode($res);
            $response = $this->response->withType('json')->withStringBody($json);
            return $response;
        }
    }

    public function chartLinePaciente()
    {
        if($this->request->is('post')){

            $mes = (!empty($this->request->getData('mes'))) ? $this->request->getData('mes') : date('m');
            $ano = (!empty($this->request->getData('ano'))) ? $this->request->getData('ano') : date('Y');

            $res = [
                'dias' => $this->Data->getDaysMonth($ano, $mes),
                'pacientes' => [
                    'dataSets' => $this->Indicador->getAtendimentosPerGrupo($mes, $ano)
                ]
            ];

            $json = json_encode($res);
            $response = $this->response->withType('json')->withStringBody($json);
            return $response;
        }
    }

    public function grafico(){

        $codigo_agenda = ($this->request->getData('controller') == 'index') ? $this->Auth->user('codigo_agenda') : null;
        $codigo_medico = ($this->request->getData('controller') == 'index') ? $this->Auth->user('codigo_medico') : null;

        $now = new Time();
        $mes1 = $now->format('m');
        $mes2 = $now->subMonth(1)->format('m');
        $mes3 = $now->subMonth(1)->format('m');

        // dados para os agendamentos
        $agendamento = [
            'ultimos_meses' => [
                $this->Indicador->searchAgendamentoPerMonth($mes3, $codigo_agenda),
                $this->Indicador->searchAgendamentoPerMonth($mes2, $codigo_agenda),
                $this->Indicador->searchAgendamentoPerMonth($mes1, $codigo_agenda)
            ]];
        /************************************************************************/

        // dados para os atendimentos
        $this->loadModel('GrupoProcedimentos');
        $grupoProcedimentos = $this->GrupoProcedimentos->find('all')->where(['GrupoProcedimentos.situacao_id' => 1]);
        $grupoProcedimento = [];
        $a_mes1 = [];
        $a_mes2 = [];
        $a_mes3 = [];
        foreach ($grupoProcedimentos as $gp){
            $grupoProcedimento[] = $gp->nome;
            $a_mes1[] = $this->Indicador->searchAtendimentoPerMonth($mes1, $codigo_medico, $gp->id);// mes atual
            $a_mes2[] = $this->Indicador->searchAtendimentoPerMonth($mes2, $codigo_medico, $gp->id);// mes passado
            $a_mes3[] = $this->Indicador->searchAtendimentoPerMonth($mes3, $codigo_medico, $gp->id);// mes retrasado
        }
        /***************************************************************************/

        // dados para os valores
        $grupoProcedimentos = $this->GrupoProcedimentos->find('all')->where(['GrupoProcedimentos.situacao_id' => 1]);
        $v_a_mes1 = [];
        $v_a_mes1 = [];
        $v_a_mes3 = [];
        foreach ($grupoProcedimentos as $gp){
            $v_a_mes1[] = $this->Indicador->searchValoresPerMonth($mes1, null, $gp->id);// mes atual
            $v_a_mes1[] = $this->Indicador->searchValoresPerMonth($mes2, null, $gp->id);// mes passado
            $v_a_mes3[] = $this->Indicador->searchValoresPerMonth($mes3, null, $gp->id);// mes retrasado
        }
        /***************************/

        $dados = [
            'agendamento' => $agendamento,
            'atendimento' => [
                'mes_atual' => $a_mes1,
                'mes_passado' => $a_mes2,
                'mes_retrasado' => $a_mes3
            ],
            'valores' => [
                'mes_atual' => $v_a_mes1,
                'mes_passado' => $v_a_mes1,
                'mes_retrasado' => $v_a_mes3
            ],
            'label_meses' => $this->Data->ultimosMeses(),
            'grupos' => $grupoProcedimento
        ];

        $json = json_encode($dados);
        $response = $this->response->withType('json')->withStringBody($json);
        return $response;
    }
}