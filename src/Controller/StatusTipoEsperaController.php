<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StatusTipoEspera Controller
 *
 * @property \App\Model\Table\StatusTipoEsperaTable $StatusTipoEspera
 */
class StatusTipoEsperaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $statusTipoEspera = $this->paginate($this->StatusTipoEspera);


        $this->set(compact('statusTipoEspera'));
        $this->set('_serialize', ['statusTipoEspera']);

    }

    /**
     * View method
     *
     * @param string|null $id Status Tipo Espera id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $statusTipoEspera = $this->StatusTipoEspera->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('statusTipoEspera', $statusTipoEspera);
        $this->set('_serialize', ['statusTipoEspera']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $statusTipoEspera = $this->StatusTipoEspera->newEntity();
        if ($this->request->is('post')) {
            $statusTipoEspera = $this->StatusTipoEspera->patchEntity($statusTipoEspera, $this->request->data);
            if ($this->StatusTipoEspera->save($statusTipoEspera)) {
                $this->Flash->success(__('O status tipo espera foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O status tipo espera não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('statusTipoEspera'));
        $this->set('_serialize', ['statusTipoEspera']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Status Tipo Espera id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $statusTipoEspera = $this->StatusTipoEspera->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $statusTipoEspera = $this->StatusTipoEspera->patchEntity($statusTipoEspera, $this->request->data);
            if ($this->StatusTipoEspera->save($statusTipoEspera)) {
                $this->Flash->success(__('O status tipo espera foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O status tipo espera não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('statusTipoEspera'));
        $this->set('_serialize', ['statusTipoEspera']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Status Tipo Espera id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $statusTipoEspera = $this->StatusTipoEspera->get($id);
                $statusTipoEspera->situacao_id = 2;
        if ($this->StatusTipoEspera->save($statusTipoEspera)) {
            $this->Flash->success(__('O status tipo espera foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O status tipo espera não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Status Tipo Espera id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->StatusTipoEspera->find('all')
        ->where(['StatusTipoEspera.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('StatusTipoEspera.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Status Tipo Espera id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $statusTipoEspera = $this->StatusTipoEspera->get($this->request->data['id']);
            $res = ['nome'=>$statusTipoEspera->nome,'id'=>$statusTipoEspera->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
