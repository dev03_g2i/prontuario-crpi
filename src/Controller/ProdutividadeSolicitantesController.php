<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProdutividadeSolicitantes Controller
 *
 * @property \App\Model\Table\ProdutividadeSolicitantesTable $ProdutividadeSolicitantes
 */
class ProdutividadeSolicitantesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->ProdutividadeSolicitantes->find('all')
            ->contain(['Convenios', 'Solicitantes', 'Users', 'SituacaoCadastros','Procedimentos'])
            ->where(['ProdutividadeSolicitantes.situacao_id' => 1]);
        $solicitante_id = null;
        if(!empty($this->request->getQuery(['solicitante_id']))){
            $solicitante_id = $this->request->getQuery(['solicitante_id']);
            $query->andWhere(['ProdutividadeSolicitantes.solicitante_id' => $solicitante_id]);
        }
        if(!empty($this->request->getQuery('procedimento_id'))){
            $query->andWhere(['ProdutividadeProfissionais.procedimento_id' => $this->request->getQuery('procedimento_id')]);
        }

        $produtividadeSolicitantes = $this->paginate($query);
        $convenios = $this->ProdutividadeSolicitantes->Convenios->find('list')
            ->where(['Convenios.situacao_id' => 1])->orderAsc('Convenios.nome');
        $this->set(compact('produtividadeSolicitantes','solicitante_id','convenios'));
        $this->set('_serialize', ['produtividadeSolicitantes']);
    }

    /**
     * View method
     *
     * @param string|null $id Produtividade Solicitante id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $produtividadeSolicitante = $this->ProdutividadeSolicitantes->get($id, [
            'contain' => ['Convenios', 'Solicitantes', 'Users']
        ]);

        $this->set('produtividadeSolicitante', $produtividadeSolicitante);
        $this->set('_serialize', ['produtividadeSolicitante']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($solicitante_id = null)
    {
        $produtividadeSolicitante = $this->ProdutividadeSolicitantes->newEntity();
        if ($this->request->is('post')) {
            $produtividadeSolicitante = $this->ProdutividadeSolicitantes->patchEntity($produtividadeSolicitante, $this->request->getData());
            $this->autoRender=false;
            $this->response->type('json');

            $check = $this->ProdutividadeSolicitantes->find('all')
                ->where(['ProdutividadeSolicitantes.situacao_id' => 1])
                ->andWhere(['ProdutividadeSolicitantes.convenio_id' => $this->request->getData('convenio_id')])
                ->andWhere(['ProdutividadeSolicitantes.solicitante_id' => $solicitante_id])
                ->andWhere(['ProdutividadeSolicitantes.procedimento_id' => $this->request->getData('procedimento_id')])
                ->count();

            if($check > 0){
                $json = json_encode(['res' => $check, 'msg' => 'Já existe uma produtividade cadastrada, por favor informe outro para continuar!']);
            }else {
                if(!empty($solicitante_id)){
                    $produtividadeSolicitante->solicitante_id = $solicitante_id;
                }
                if ($this->ProdutividadeSolicitantes->save($produtividadeSolicitante)) {
                    $json = $json = json_encode(['res' => 0, 'msg' => 'A produtividade do solicitante foi salvo com sucesso!']);
                } else {
                    $json = $json = json_encode(['res' => 1, 'msg' => 'O produtividade do solicitante não foi salvo. Por favor, tente novamente.']);
                }
            }
            $this->response->body($json);
            $convenios = $this->ProdutividadeSolicitantes->Convenios->find('list', ['limit' => 200])->orderAsc('Convenios.nome');

        }

        $this->set(compact('produtividadeSolicitante','convenios'));
        $this->set('_serialize', ['produtividadeSolicitante']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Produtividade Solicitante id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $produtividadeSolicitante = $this->ProdutividadeSolicitantes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $produtividadeSolicitante = $this->ProdutividadeSolicitantes->patchEntity($produtividadeSolicitante, $this->request->data);
            if ($this->ProdutividadeSolicitantes->save($produtividadeSolicitante)) {
                $this->Flash->success(__('O produtividade solicitante foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O produtividade solicitante não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('produtividadeSolicitante'));
        $this->set('_serialize', ['produtividadeSolicitante']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Produtividade Solicitante id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $produtividadeSolicitante = $this->ProdutividadeSolicitantes->get($id);
                $produtividadeSolicitante->situacao_id = 2;
        if ($this->ProdutividadeSolicitantes->save($produtividadeSolicitante)) {
            $this->Flash->success(__('O produtividade solicitante foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O produtividade solicitante não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Produtividade Solicitante id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ProdutividadeSolicitantes->find('all')
        ->where(['ProdutividadeSolicitantes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ProdutividadeSolicitantes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Produtividade Solicitante id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $produtividadeSolicitante = $this->ProdutividadeSolicitantes->get($this->request->data['id']);
            $res = ['nome'=>$produtividadeSolicitante->nome,'id'=>$produtividadeSolicitante->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
