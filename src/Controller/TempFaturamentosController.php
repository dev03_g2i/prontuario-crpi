<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TempFaturamentos Controller
 *
 * @property \App\Model\Table\TempFaturamentosTable $TempFaturamentos
 */
class TempFaturamentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Atendimentos', 'Users']
        ];
        $tempFaturamentos = $this->paginate($this->TempFaturamentos);


        $this->set(compact('tempFaturamentos'));
        $this->set('_serialize', ['tempFaturamentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Temp Faturamento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tempFaturamento = $this->TempFaturamentos->get($id, [
            'contain' => ['Atendimentos', 'Users']
        ]);

        $this->set('tempFaturamento', $tempFaturamento);
        $this->set('_serialize', ['tempFaturamento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tempFaturamento = $this->TempFaturamentos->newEntity();
        if ($this->request->is('post')) {
            $tempFaturamento = $this->TempFaturamentos->patchEntity($tempFaturamento, $this->request->data);
            if ($this->TempFaturamentos->save($tempFaturamento)) {
                $this->Flash->success(__('O temp faturamento foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O temp faturamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tempFaturamento'));
        $this->set('_serialize', ['tempFaturamento']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Temp Faturamento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tempFaturamento = $this->TempFaturamentos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tempFaturamento = $this->TempFaturamentos->patchEntity($tempFaturamento, $this->request->data);
            if ($this->TempFaturamentos->save($tempFaturamento)) {
                $this->Flash->success(__('O temp faturamento foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O temp faturamento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('tempFaturamento'));
        $this->set('_serialize', ['tempFaturamento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Temp Faturamento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tempFaturamento = $this->TempFaturamentos->get($id);
                $tempFaturamento->situacao_id = 2;
        if ($this->TempFaturamentos->save($tempFaturamento)) {
            $this->Flash->success(__('O temp faturamento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O temp faturamento não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Temp Faturamento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->TempFaturamentos->find('all')
        ->where(['TempFaturamentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('TempFaturamentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Temp Faturamento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $tempFaturamento = $this->TempFaturamentos->get($this->request->data['id']);
            $res = ['nome'=>$tempFaturamento->nome,'id'=>$tempFaturamento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
