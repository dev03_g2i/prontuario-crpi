<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProcedimentoCaddetalhes Controller
 *
 * @property \App\Model\Table\ProcedimentoCaddetalhesTable $ProcedimentoCaddetalhes
 */
class ProcedimentoCaddetalhesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $procedimentoCaddetalhes = $this->paginate($this->ProcedimentoCaddetalhes);


        $this->set(compact('procedimentoCaddetalhes'));
        $this->set('_serialize', ['procedimentoCaddetalhes']);

    }

    /**
     * View method
     *
     * @param string|null $id Procedimento Caddetalhe id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->get($id, [
            'contain' => ['SituacaoCadastros', 'Users']
        ]);

        $this->set('procedimentoCaddetalhe', $procedimentoCaddetalhe);
        $this->set('_serialize', ['procedimentoCaddetalhe']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->newEntity();
        if ($this->request->is('post')) {
            $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->patchEntity($procedimentoCaddetalhe, $this->request->data);
            if ($this->ProcedimentoCaddetalhes->save($procedimentoCaddetalhe)) {
                $this->Flash->success(__('O procedimento caddetalhe foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O procedimento caddetalhe não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('procedimentoCaddetalhe'));
        $this->set('_serialize', ['procedimentoCaddetalhe']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Procedimento Caddetalhe id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->patchEntity($procedimentoCaddetalhe, $this->request->data);
            if ($this->ProcedimentoCaddetalhes->save($procedimentoCaddetalhe)) {
                $this->Flash->success(__('O procedimento caddetalhe foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O procedimento caddetalhe não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('procedimentoCaddetalhe'));
        $this->set('_serialize', ['procedimentoCaddetalhe']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Procedimento Caddetalhe id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->get($id);
                $procedimentoCaddetalhe->situacao_id = 2;
        if ($this->ProcedimentoCaddetalhes->save($procedimentoCaddetalhe)) {
            $this->Flash->success(__('O procedimento caddetalhe foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O procedimento caddetalhe não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Procedimento Caddetalhe id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->ProcedimentoCaddetalhes->find('all')
        ->where(['ProcedimentoCaddetalhes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('ProcedimentoCaddetalhes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Procedimento Caddetalhe id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $procedimentoCaddetalhe = $this->ProcedimentoCaddetalhes->get($this->request->data['id']);
            $res = ['nome'=>$procedimentoCaddetalhe->nome,'id'=>$procedimentoCaddetalhe->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
