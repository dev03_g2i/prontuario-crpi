<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * AgendaFilaespera Controller
 *
 * @property \App\Model\Table\AgendaFilaesperaTable $AgendaFilaespera
 */
class AgendaFilaesperaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = $this->AgendaFilaespera->find('all')
            ->contain(['Convenios', 'TipoEsperaagenda', 'GrupoAgendas', 'SituacaoCadastros', 'Users'])
            ->where(['AgendaFilaespera.situacao_id' => 1]);

        $grupo = null;
        if(!empty($this->request->getQuery('grupo'))){
            $grupo = explode(',', $this->request->getQuery('grupo'));
            $query->andWhere(['AgendaFilaespera.grupo_id IN' => $grupo]);
        }

        $agendaFilaespera = $this->paginate($query);

        $tipo_espera = $this->AgendaFilaespera->TipoEsperaagenda->getList();
        $this->set(compact('agendaFilaespera', 'tipo_espera', 'grupo_id'));
        $this->set('_serialize', ['agendaFilaespera']);

    }

    /**
     * View method
     *
     * @param string|null $id Agenda Filaespera id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $agendaFilaespera = $this->AgendaFilaespera->get($id, [
            'contain' => ['Convenios', 'TipoEsperaagenda', 'GrupoAgendas', 'SituacaoCadastros', 'Users']
        ]);

        $this->set('agendaFilaespera', $agendaFilaespera);
        $this->set('_serialize', ['agendaFilaespera']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $agendaFilaespera = $this->AgendaFilaespera->newEntity();
        if ($this->request->is('post')) {
            $agendaFilaespera = $this->AgendaFilaespera->patchEntity($agendaFilaespera, $this->request->data);
            if ($this->AgendaFilaespera->save($agendaFilaespera)) {
                $this->Flash->success(__('O agenda filaespera foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index', $grupo_id]);
            } else {
                $this->Flash->error(__('O agenda filaespera não foi salvo. Por favor, tente novamente.'));
            }
        }

        $tipo_espera = $this->AgendaFilaespera->TipoEsperaagenda->getList();
        $grupos = $this->AgendaFilaespera->GrupoAgendas->getList();
        $this->set(compact('agendaFilaespera', 'tipo_espera', 'grupos', 'grupo_id'));
        $this->set('_serialize', ['agendaFilaespera']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Agenda Filaespera id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $agendaFilaespera = $this->AgendaFilaespera->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $agendaFilaespera = $this->AgendaFilaespera->patchEntity($agendaFilaespera, $this->request->data);
            if ($this->AgendaFilaespera->save($agendaFilaespera)) {
                $this->Flash->success(__('O agenda filaespera foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O agenda filaespera não foi salvo. Por favor, tente novamente.'));
            }
        }

        $tipo_espera = $this->AgendaFilaespera->TipoEsperaagenda->getList();
        $grupos = $this->AgendaFilaespera->GrupoAgendas->getList();
        $this->set(compact('agendaFilaespera', 'tipo_espera', 'grupos'));
        $this->set('_serialize', ['agendaFilaespera']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Agenda Filaespera id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $agendaFilaespera = $this->AgendaFilaespera->get($id);
                $agendaFilaespera->situacao_id = 2;
        if ($this->AgendaFilaespera->save($agendaFilaespera)) {
            $this->Flash->success(__('O agenda filaespera foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O agenda filaespera não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete Modal
     *
     * @return void
     */
    public function deleteModal()
    {
        $AgendaFilaespera = $this->AgendaFilaespera->get($this->request->getData('id'));
        $AgendaFilaespera->situacao_id = 2;
        if ($this->AgendaFilaespera->save($AgendaFilaespera)) {
            $json = ['res' => 0];
        } else {
            $json = ['res' => 1];
        }
        $json_data = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }


    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Agenda Filaespera id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->AgendaFilaespera->find('all')
        ->where(['AgendaFilaespera.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('AgendaFilaespera.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Agenda Filaespera id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $agendaFilaespera = $this->AgendaFilaespera->get($this->request->data['id']);
            $res = ['nome'=>$agendaFilaespera->nome,'id'=>$agendaFilaespera->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
