<?php
namespace App\Controller;

use Cake\I18n\Time;
use App\Controller\AppController;

/**
 * EstqSaida Controller
 *
 * @property \App\Model\Table\EstqSaidaTable $EstqSaida
 */
class EstqSaidaController extends AppController
{
    public $components = array('Atendimento', 'Matmed');
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($atendimento_id = null)
    {
        $query = $this->EstqSaida->find('all')
                    ->contain(['EstqArtigos'])
                    ->where(['EstqSaida.situacao_id' => 1]);

        if(!empty($atendimento_id)){
            $query->andWhere(['EstqSaida.atendimento_id' => $atendimento_id]);
        }

        $estqSaida = $this->paginate($query);

        $dadosPaciente = $this->EstqSaida->AtendimentoProcedimentos->find('all')
                ->contain(['Atendimentos' => ['Convenios', 'Clientes']])
                ->where(['AtendimentoProcedimentos.atendimento_id' => $atendimento_id])->first();

        $atendProcedimentos  = $this->Atendimento->procedimentos($dadosPaciente->atendimento_id);
        
        $this->loadModel('EstqKit');
        $estq_kit = $this->EstqKit->getList();
        $this->set(compact('estqSaida', 'dadosPaciente', 'atendProcedimentos', 'estq_kit'));
        $this->set('_serialize', ['estqSaida']);

    }

    /**'
     * View method
     *
     * @param string|null $id Estq Saida id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $estqSaida = $this->EstqSaida->get($id, [
            'contain' => ['EstqTipoMovimento', 'AtendimentoProcedimentos', 'SituacaoCadastros']
        ]);

        $this->set('estqSaida', $estqSaida);
        $this->set('_serialize', ['estqSaida']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $estqSaida = $this->EstqSaida->newEntity();
        if ($this->request->is('post')) {
            $estqSaida = $this->EstqSaida->patchEntity($estqSaida, $this->request->data);
            if ($this->EstqSaida->save($estqSaida)) {
                $this->Flash->success(__('O estq saida foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq saida não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('estqSaida'));
        $this->set('_serialize', ['estqSaida']);
    }

    public function addArtigo()
    {
        $dados = null;
        if(!empty($this->request->query('atendProc_id'))){
            $dados = $this->EstqSaida->AtendimentoProcedimentos->get($this->request->query('atendProc_id'), [
                'contain' => ['Procedimentos', 'Atendimentos' => ['Clientes']]
            ]);
        }

        $artigos = $this->EstqSaida->EstqArtigos->find('list')->where(['EstqArtigos.situacao_id' => 1])->orderAsc('EstqArtigos.nome');
        $this->set(compact('artigos', 'dados'));
    }

    public function newArtigo()
    {
        $this->autoRender=false;
        $this->response->type('json');

        $EstqSaida = $this->EstqSaida->newEntity();
        if ($this->request->is('post')) {
            $EstqSaida = $this->EstqSaida->patchEntity($EstqSaida, $this->request->data);
            $dados = $this->Matmed->precoArtigo($this->request->getData('artigo_id'));
            $EstqSaida->codigo_tiss = ($dados)? $dados->codigo : null;
            $EstqSaida->vl_venda = ($dados) ? $dados->valor : null;
            $EstqSaida->total = ($dados) ? $this->request->data('quantidade') * $dados->valor : '';
            $EstqSaida->codigo_convenio = ($dados) ? $dados->codigo : null;

            $artigo = $this->EstqSaida->EstqArtigos->get($this->request->getData('artigo_id'));
            $EstqSaida->codigo_tiss = $artigo->codigo_tiss;
            $EstqSaida->codigo_tuss = $artigo->codigo_tuss;
            if ($this->EstqSaida->save($EstqSaida)) {
                $dados = $this->EstqSaida->get($EstqSaida->id, ['contain' => ['EstqArtigos']]);
                $date = new Time($dados->data);
                $newDate = $date->format('d/m/Y');
                $res = ['id' => $dados->id, 'data' => $newDate, 'codigo' => $dados->estq_artigo->codigo_livre, 'artigo' => $dados->estq_artigo->nome, 'qtd' => $dados->quantidade];
            }else {
                $res = 'error';
            }
        }

        $this->response->body(json_encode($res));
    }

    public function addKitMatmed(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = [];

        $atendProc = $this->EstqSaida->AtendimentoProcedimentos->get($this->request->data('atend_proc_id'), [
            'contain' => ['Atendimentos']
        ]);
        $faturaKitartigo = $this->EstqSaida->EstqArtigos->EstqKitArtigo->find('all')->where(['EstqKitArtigo.fatura_kit_id' => $this->request->data('fatura_kit_id')]);
        $countFaturaKitartigo = $faturaKitartigo->count();
        if($countFaturaKitartigo > 0){
            foreach ($faturaKitartigo as $fka) {
                $dados = $this->Matmed->precoArtigo($fka->artigo_id);

                $faturaMatmed = $this->EstqSaida->newEntity();
                $faturaMatmed->data = $atendProc->atendimento->data;
                $faturaMatmed->artigo_id = $fka->artigo_id;
                $faturaMatmed->quantidade = $fka->quantidade;
                $faturaMatmed->atendimento_procedimento_id = $atendProc->id;
                $faturaMatmed->atendimento_id = $atendProc->atendimento_id;
                $faturaMatmed->vl_venda = $dados->valor;
                $faturaMatmed->total = $fka->quantidade * $dados->valor;
                $faturaMatmed->codigo_convenio = ($dados) ? $dados->codigo : null;
                $artigo = $this->EstqSaida->EstqArtigos->get($fka->artigo_id);
                $faturaMatmed->codigo_tiss = $artigo->codigo_tiss;
                $faturaMatmed->codigo_tuss = $artigo->codigo_tuss;

                if($this->EstqSaida->save($faturaMatmed)){
                    $res = ['res' => 0, 'msg' => 'O kit/Pacote foi adicionado com sucesso!'];
                }else {
                    $res = ['res' => 1, 'msg' => 'Não foi possivel adicionar o Kit/Pacote, por favor tente mais tarde!'];
                }
            }
            //$this->Matmed->atualizaValorMaterial($atendProc->id);
        }else {
            $res = ['res' => 1, 'msg' => 'Não foi encontrado artigos cadastrados para este Kit/Pacote!'];
        }

        $this->response->body(json_encode($res));
    }

    /**
     * Edit method
     *
     * @param string|null $id Estq Saida id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $estqSaida = $this->EstqSaida->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $estqSaida = $this->EstqSaida->patchEntity($estqSaida, $this->request->data);
            if ($this->EstqSaida->save($estqSaida)) {
                $this->Flash->success(__('O estq saida foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O estq saida não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('estqSaida'));
        $this->set('_serialize', ['estqSaida']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Estq Saida id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $estqSaida = $this->EstqSaida->get($id);
                $estqSaida->situacao_id = 2;
        if ($this->EstqSaida->save($estqSaida)) {
            $this->Flash->success(__('O estq saida foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O estq saida não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete Modal
     *
     * @return void
     */
    public function deleteModal()
    {
        $estqSaida = $this->EstqSaida->get($this->request->getData('id'));
        $estqSaida->situacao_id = 2;
        if ($this->EstqSaida->save($estqSaida)) {
            $json = ['res' => 0];
        } else {
            $json = ['res' => 1];
        }
        $json_data = json_encode($json);
        $response = $this->response->withType('json')->withStringBody($json_data);
        return $response;
    }

    public function filterTotais(){
        $this->autoRender=false;
        $this->response->type('json');

        $query = $this->EstqSaida->find()
                        ->where(['EstqSaida.situacao_id' => 1])
                        ->andWhere(['EstqSaida.atendimento_id' => $this->request->data('atendimento_id')]);

        $query_select = $query->select([
            'total' => $query->func()->sum('EstqSaida.total')
        ])->toArray();

        $res = ['total' => ($query_select[0]->total)?$query_select[0]->total: 0];
        $this->response->body(json_encode($res));
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Estq Saida id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->EstqSaida->find('all')
        ->where(['EstqSaida.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('EstqSaida.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Estq Saida id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $estqSaida = $this->EstqSaida->get($this->request->data['id']);
            $res = ['nome'=>$estqSaida->nome,'id'=>$estqSaida->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
