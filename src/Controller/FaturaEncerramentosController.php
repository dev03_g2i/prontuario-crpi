<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FaturaEncerramentos Controller
 *
 * @property \App\Model\Table\FaturaEncerramentosTable $FaturaEncerramentos
 */
class FaturaEncerramentosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Convenios', 'SituacaoFaturaencerramentos', 'Unidades'],
            'order' => ['FaturaEncerramentos.created DESC']
        ];
        $faturaEncerramentos = $this->paginate($this->FaturaEncerramentos);

        $this->set(compact('faturaEncerramentos'));
        $this->set('_serialize', ['faturaEncerramentos']);

    }

    /**
     * View method
     *
     * @param string|null $id Fatura Encerramento id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $faturaEncerramento = $this->FaturaEncerramentos->get($id, [
            'contain' => ['Convenios']
        ]);

        $this->set('faturaEncerramento', $faturaEncerramento);
        $this->set('_serialize', ['faturaEncerramento']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $faturaEncerramento = $this->FaturaEncerramentos->newEntity();
    //     if ($this->request->is('post')) {
    //         $faturaEncerramento = $this->FaturaEncerramentos->patchEntity($faturaEncerramento, $this->request->data);
    //         if ($this->FaturaEncerramentos->save($faturaEncerramento)) {
    //             $this->Flash->success(__('O fatura encerramento foi salvo com sucesso!'));
    //             return $this->redirect(['controller' => 'Faturamentos', 'action' => 'all']);
    //         } else {
    //             $this->Flash->error(__('O fatura encerramento não foi salvo. Por favor, tente novamente.'));
    //         }
    //     }

    //     $this->set(compact('faturaEncerramento'));
    //     $this->set('_serialize', ['faturaEncerramento']);
    // }

    /**
     * Edit method
     *
     * @param string|null $id Fatura Encerramento id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $faturaEncerramento = $this->FaturaEncerramentos->get($id, [
            'contain' => ['Convenios', 'Users', 'SituacaoFaturaencerramentos', 'UserEdit'],
            'conditions' => ['FaturaEncerramentos.situacao_id' => 1]
        ]);

        if ($faturaEncerramento->competencia_mes < 10) {
            $faturaEncerramento->competencia_mes = $faturaEncerramento->competencia_mes[1];
        }

        $meses = [
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        ];

        $anos = [];

        for ($i = -4; $i <= 4; $i++) {
            $anos[date('Y') + ($i)] = date('Y') + ($i);
        }
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newFaturaEncerramento = $this->request->data;
            $newFaturaEncerramento['user_update'] = $this->Auth->user('id');
            $newFaturaEncerramento['data_quitacao'] = date('Y-m-d');

            // Ano e mes da competencia, pegando a string dos valores e o text de cada input
            // para a posição de acordo com o valor no input.
            // O código para incrementar o valor da competência está no script ao final do HTML
            $newFaturaEncerramento['competencia_mes'] = strval($newFaturaEncerramento['competencia_mes']);
            $newFaturaEncerramento['competencia_mes'] = $newFaturaEncerramento['competencia_mes'] < 10 ? 
            '0'.$newFaturaEncerramento['competencia_mes'] : $newFaturaEncerramento['competencia_mes'];
            $newFaturaEncerramento['competencia_ano'] = strval($anos[$newFaturaEncerramento['competencia_ano']]);

            $faturaEncerramento = $this->FaturaEncerramentos->patchEntity($faturaEncerramento, $newFaturaEncerramento);
            if ($this->FaturaEncerramentos->save($faturaEncerramento)) {
                $this->Flash->success(__('O fatura encerramento foi salvo com sucesso.'));
                return $this->redirect(['controller' => 'Faturamentos', 'action' => 'all']);
            } else {
                $this->Flash->error(__('O fatura encerramento não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->loadModel('Unidades');
        $branches = $this->Unidades->find('list', ['limit' => 200])->orderAsc('Unidades.nome')->where(['situacao_id' => 1]);

        $situacaoFaturaencerramento = $this->FaturaEncerramentos->SituacaoFaturaencerramentos->find('list', ['limit' => 200])->where(['SituacaoFaturaencerramentos.situacao_id' => 1])->orderAsc('nome');
        $this->set(compact('faturaEncerramento', 'situacaoFaturaencerramento', 'branches', 'meses', 'anos'));
        $this->set('_serialize', ['faturaEncerramento']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fatura Encerramento id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $faturaEncerramento = $this->FaturaEncerramentos->get($id);
                $faturaEncerramento->situacao_id = 2;
        if ($this->FaturaEncerramentos->save($faturaEncerramento)) {
            $this->Flash->success(__('O fatura encerramento foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O fatura encerramento não foi deletado! Tente novamente mais tarde.'));
        }
        return $this->redirect(['controller' => 'Faturamentos', 'action' => 'all']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Fatura Encerramento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->FaturaEncerramentos->find('all')
        ->where(['FaturaEncerramentos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('FaturaEncerramentos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Fatura Encerramento id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $faturaEncerramento = $this->FaturaEncerramentos->get($this->request->data['id']);
            $res = ['nome'=>$faturaEncerramento->nome,'id'=>$faturaEncerramento->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
