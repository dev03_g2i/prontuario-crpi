<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoLaudos Controller
 *
 * @property \App\Model\Table\SituacaoLaudosTable $SituacaoLaudos
 */
class SituacaoLaudosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Situacaos', 'Users']
        ];
        $situacaoLaudos = $this->paginate($this->SituacaoLaudos);


        $this->set(compact('situacaoLaudos'));
        $this->set('_serialize', ['situacaoLaudos']);

    }

    /**
     * View method
     *
     * @param string|null $id Situacao Laudo id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoLaudo = $this->SituacaoLaudos->get($id, [
            'contain' => ['Situacaos', 'Users', 'AtendimentoProcedimentos']
        ]);

        $this->set('situacaoLaudo', $situacaoLaudo);
        $this->set('_serialize', ['situacaoLaudo']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoLaudo = $this->SituacaoLaudos->newEntity();
        if ($this->request->is('post')) {
            $situacaoLaudo = $this->SituacaoLaudos->patchEntity($situacaoLaudo, $this->request->data);
            if ($this->SituacaoLaudos->save($situacaoLaudo)) {
                $this->Flash->success(__('O situacao laudo foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao laudo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoLaudo'));
        $this->set('_serialize', ['situacaoLaudo']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Laudo id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoLaudo = $this->SituacaoLaudos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoLaudo = $this->SituacaoLaudos->patchEntity($situacaoLaudo, $this->request->data);
            if ($this->SituacaoLaudos->save($situacaoLaudo)) {
                $this->Flash->success(__('O situacao laudo foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao laudo não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('situacaoLaudo'));
        $this->set('_serialize', ['situacaoLaudo']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Laudo id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoLaudo = $this->SituacaoLaudos->get($id);
                $situacaoLaudo->situacao_id = 2;
        if ($this->SituacaoLaudos->save($situacaoLaudo)) {
            $this->Flash->success(__('O situacao laudo foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao laudo não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Situacao Laudo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->SituacaoLaudos->find('all')
        ->where(['SituacaoLaudos.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('SituacaoLaudos.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Situacao Laudo id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $situacaoLaudo = $this->SituacaoLaudos->get($this->request->data['id']);
            $res = ['nome'=>$situacaoLaudo->nome,'id'=>$situacaoLaudo->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
