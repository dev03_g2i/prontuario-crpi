<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * GrupoRetornos Controller
 *
 * @property \App\Model\Table\GrupoRetornosTable $GrupoRetornos
 */
class GrupoRetornosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'SituacaoCadastros']
                        ,'conditions' => ['GrupoRetornos.situacao_id = ' => '1']
                    ];
        $grupoRetornos = $this->paginate($this->GrupoRetornos);

        $this->set(compact('grupoRetornos'));
        $this->set('_serialize', ['grupoRetornos']);
    }

    /**
     * View method
     *
     * @param string|null $id Grupo Retorno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $grupoRetorno = $this->GrupoRetornos->get($id, [
            'contain' => ['Users', 'SituacaoCadastros']
        ]);

        $this->set('grupoRetorno', $grupoRetorno);
        $this->set('_serialize', ['grupoRetorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $grupoRetorno = $this->GrupoRetornos->newEntity();
        if ($this->request->is('post')) {
            $grupoRetorno = $this->GrupoRetornos->patchEntity($grupoRetorno, $this->request->data);
            if ($this->GrupoRetornos->save($grupoRetorno)) {
                $this->Flash->success(__('O grupo retorno foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->GrupoRetornos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->GrupoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('grupoRetorno', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['grupoRetorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Grupo Retorno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $grupoRetorno = $this->GrupoRetornos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $grupoRetorno = $this->GrupoRetornos->patchEntity($grupoRetorno, $this->request->data);
            if ($this->GrupoRetornos->save($grupoRetorno)) {
                $this->Flash->success(__('O grupo retorno foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O grupo retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $users = $this->GrupoRetornos->Users->find('list', ['limit' => 200]);
        $situacaoCadastros = $this->GrupoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $this->set(compact('grupoRetorno', 'users', 'situacaoCadastros'));
        $this->set('_serialize', ['grupoRetorno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Grupo Retorno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $grupoRetorno = $this->GrupoRetornos->get($id);
                $grupoRetorno->situacao_id = 2;
        if ($this->GrupoRetornos->save($grupoRetorno)) {
            $this->Flash->success(__('O grupo retorno foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O grupo retorno não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
