<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SituacaoRetornos Controller
 *
 * @property \App\Model\Table\SituacaoRetornosTable $SituacaoRetornos
 */
class SituacaoRetornosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
                        ,'conditions' => ['SituacaoRetornos.situacao_id = ' => '1']
                    ];
        $situacaoRetornos = $this->paginate($this->SituacaoRetornos);

        $this->set(compact('situacaoRetornos'));
        $this->set('_serialize', ['situacaoRetornos']);
    }

    /**
     * View method
     *
     * @param string|null $id Situacao Retorno id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $situacaoRetorno = $this->SituacaoRetornos->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'Retornos']
        ]);

        $this->set('situacaoRetorno', $situacaoRetorno);
        $this->set('_serialize', ['situacaoRetorno']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $situacaoRetorno = $this->SituacaoRetornos->newEntity();
        if ($this->request->is('post')) {
            $situacaoRetorno = $this->SituacaoRetornos->patchEntity($situacaoRetorno, $this->request->data);
            if ($this->SituacaoRetornos->save($situacaoRetorno)) {
                $this->Flash->success(__('O situacao retorno foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->SituacaoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->SituacaoRetornos->Users->find('list', ['limit' => 200]);
        $this->set(compact('situacaoRetorno', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['situacaoRetorno']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Situacao Retorno id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $situacaoRetorno = $this->SituacaoRetornos->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $situacaoRetorno = $this->SituacaoRetornos->patchEntity($situacaoRetorno, $this->request->data);
            if ($this->SituacaoRetornos->save($situacaoRetorno)) {
                $this->Flash->success(__('O situacao retorno foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O situacao retorno não foi salvo. Por favor, tente novamente.'));
            }
        }
        $situacaoCadastros = $this->SituacaoRetornos->SituacaoCadastros->find('list', ['limit' => 200]);
        $users = $this->SituacaoRetornos->Users->find('list', ['limit' => 200]);
        $this->set(compact('situacaoRetorno', 'situacaoCadastros', 'users'));
        $this->set('_serialize', ['situacaoRetorno']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Situacao Retorno id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $situacaoRetorno = $this->SituacaoRetornos->get($id);
                $situacaoRetorno->situacao_id = 2;
        if ($this->SituacaoRetornos->save($situacaoRetorno)) {
            $this->Flash->success(__('O situacao retorno foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O situacao retorno não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }
}
