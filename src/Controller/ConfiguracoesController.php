<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Configuracoes Controller
 *
 * @property \App\Model\Table\ConfiguracoesTable $Configuracoes
 */
class ConfiguracoesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $configuracoes = $this->paginate($this->Configuracoes);


        $this->set(compact('configuracoes'));
        $this->set('_serialize', ['configuracoes']);

    }

    /**
     * View method
     *
     * @param string|null $id Configuraco id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $configuraco = $this->Configuracoes->get($id, [
            'contain' => []
        ]);

        $this->set('configuraco', $configuraco);
        $this->set('_serialize', ['configuraco']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $configuraco = $this->Configuracoes->newEntity();
        if ($this->request->is('post')) {
            $configuraco = $this->Configuracoes->patchEntity($configuraco, $this->request->data);
            if ($this->Configuracoes->save($configuraco)) {
                $this->Flash->success(__('O configuraco foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O configuraco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('configuraco'));
        $this->set('_serialize', ['configuraco']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Configuraco id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit()
    {
        $configuraco = $this->Configuracoes->find('all')->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $configuraco = $this->Configuracoes->patchEntity($configuraco, $this->request->data);
            if ($this->Configuracoes->save($configuraco)) {
                $this->Flash->success(__('O configuraco foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O configuraco não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('configuraco'));
        $this->set('_serialize', ['configuraco']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Configuraco id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $configuraco = $this->Configuracoes->get($id);
                $configuraco->situacao_id = 2;
        if ($this->Configuracoes->save($configuraco)) {
            $this->Flash->success(__('O configuraco foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O configuraco não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Configuraco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->Configuracoes->find('all')
        ->where(['Configuracoes.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('Configuracoes.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Configuraco id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $configuraco = $this->Configuracoes->get($this->request->data['id']);
            $res = ['nome'=>$configuraco->nome,'id'=>$configuraco->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
