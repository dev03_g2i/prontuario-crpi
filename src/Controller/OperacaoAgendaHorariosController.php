<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OperacaoAgendaHorarios Controller
 *
 * @property \App\Model\Table\OperacaoAgendaHorariosTable $OperacaoAgendaHorarios
 */
class OperacaoAgendaHorariosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['SituacaoCadastros', 'Users']
        ];
        $operacaoAgendaHorarios = $this->paginate($this->OperacaoAgendaHorarios);


        $this->set(compact('operacaoAgendaHorarios'));
        $this->set('_serialize', ['operacaoAgendaHorarios']);

    }

    /**
     * View method
     *
     * @param string|null $id Operacao Agenda Horario id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->get($id, [
            'contain' => ['SituacaoCadastros', 'Users', 'GrupoAgendaHorarios']
        ]);

        $this->set('operacaoAgendaHorario', $operacaoAgendaHorario);
        $this->set('_serialize', ['operacaoAgendaHorario']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->newEntity();
        if ($this->request->is('post')) {
            $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->patchEntity($operacaoAgendaHorario, $this->request->data);
            if ($this->OperacaoAgendaHorarios->save($operacaoAgendaHorario)) {
                $this->Flash->success(__('O operacao agenda horario foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O operacao agenda horario não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('operacaoAgendaHorario'));
        $this->set('_serialize', ['operacaoAgendaHorario']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Operacao Agenda Horario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->patchEntity($operacaoAgendaHorario, $this->request->data);
            if ($this->OperacaoAgendaHorarios->save($operacaoAgendaHorario)) {
                $this->Flash->success(__('O operacao agenda horario foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O operacao agenda horario não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('operacaoAgendaHorario'));
        $this->set('_serialize', ['operacaoAgendaHorario']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Operacao Agenda Horario id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->get($id);
                $operacaoAgendaHorario->situacao_id = 2;
        if ($this->OperacaoAgendaHorarios->save($operacaoAgendaHorario)) {
            $this->Flash->success(__('O operacao agenda horario foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O operacao agenda horario não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Operacao Agenda Horario id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->OperacaoAgendaHorarios->find('all')
        ->where(['OperacaoAgendaHorarios.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('OperacaoAgendaHorarios.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Operacao Agenda Horario id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $operacaoAgendaHorario = $this->OperacaoAgendaHorarios->get($this->request->data['id']);
            $res = ['nome'=>$operacaoAgendaHorario->nome,'id'=>$operacaoAgendaHorario->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
