<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * NfSituacaoNotas Controller
 *
 * @property \App\Model\Table\NfSituacaoNotasTable $NfSituacaoNotas
 */
class NfSituacaoNotasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $nfSituacaoNotas = $this->paginate($this->NfSituacaoNotas);


        $this->set(compact('nfSituacaoNotas'));
        $this->set('_serialize', ['nfSituacaoNotas']);

    }

    /**
     * View method
     *
     * @param string|null $id Nf Situacao Nota id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $nfSituacaoNota = $this->NfSituacaoNotas->get($id, [
            'contain' => []
        ]);

        $this->set('nfSituacaoNota', $nfSituacaoNota);
        $this->set('_serialize', ['nfSituacaoNota']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $nfSituacaoNota = $this->NfSituacaoNotas->newEntity();
        if ($this->request->is('post')) {
            $nfSituacaoNota = $this->NfSituacaoNotas->patchEntity($nfSituacaoNota, $this->request->data);
            if ($this->NfSituacaoNotas->save($nfSituacaoNota)) {
                $this->Flash->success(__('O nf situacao nota foi salvo com sucesso!'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf situacao nota não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfSituacaoNota'));
        $this->set('_serialize', ['nfSituacaoNota']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Nf Situacao Nota id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $nfSituacaoNota = $this->NfSituacaoNotas->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $nfSituacaoNota = $this->NfSituacaoNotas->patchEntity($nfSituacaoNota, $this->request->data);
            if ($this->NfSituacaoNotas->save($nfSituacaoNota)) {
                $this->Flash->success(__('O nf situacao nota foi salvo com sucesso.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('O nf situacao nota não foi salvo. Por favor, tente novamente.'));
            }
        }

        $this->set(compact('nfSituacaoNota'));
        $this->set('_serialize', ['nfSituacaoNota']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Nf Situacao Nota id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $nfSituacaoNota = $this->NfSituacaoNotas->get($id);
                $nfSituacaoNota->situacao_id = 2;
        if ($this->NfSituacaoNotas->save($nfSituacaoNota)) {
            $this->Flash->success(__('O nf situacao nota foi deletado com sucesso.'));
        } else {
            $this->Flash->error(__('Desculpe! O nf situacao nota não foi deletado! Tente novamente mais tarde.'));
        }
                return $this->redirect(['action' => 'index']);
    }

    /**
        * Método busca por ajax nos select2
        *
        * @param string|null $param Nf Situacao Nota id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */

    public function fill()
    {
        $this->autoRender=false;
        $this->response->type('json');
        $this->viewBuilder()->layout('ajax');

        $termo = $this->request->data['termo'];
        $size = $this->request->data['size'];
        $page = (!isset($this->request->data['page']) || $this->request->data['page'] < 1) ? 1 : $this->request->data['page'];

        if (!isset($termo))
        $termo = '';
        if (!isset($size) || $size < 1)
        $size = 10;

        $query = $this->NfSituacaoNotas->find('all')
        ->where(['NfSituacaoNotas.nome LIKE ' => '%' . $termo . '%']);

        $cont = $query->count();
        $query->orderAsc('NfSituacaoNotas.nome');
        $ret["more"] = (($size * ($page - 1)) >= (int)$cont) ? false : true;
        $ret["total"] = $cont;
        $ret["dados"] = array();

        $query->limit($size);
        $query->offset($size * ($page - 1));

        foreach ($query as $d) {
        $ret["dados"][] = array('id' => $d->id, 'text' => $d->nome);
        }
        $json = json_encode($ret);
        $this->response->body($json);
    }

    /**
        * Método de pesquisa de valores no editar
        *
        * @param string|null $id Nf Situacao Nota id.
        * @return \Cake\Network\Response|null Redirects to index.
        * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
    public function getedit(){
        $this->autoRender=false;
        $this->response->type('json');
        $res = ['nome'=>'selecione','id'=>null];
        if(!empty($this->request->data['id'])){
        $nfSituacaoNota = $this->NfSituacaoNotas->get($this->request->data['id']);
            $res = ['nome'=>$nfSituacaoNota->nome,'id'=>$nfSituacaoNota->id];
        }
        $json = json_encode($res);
        $this->response->body($json);
    }
}
