<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\I18n\Time;
include_once("PHP/autoload.php");
class SmsComponent extends Component
{
    private $smsFacade;

    public function __construct(){
        $this->smsFacade = new \SmsFacade("grupo.g2i", "UBR6zfTZTc");
    }

    public function data($data){
        $data = new Time($data);
        $now = $data->format('Y-m-d H:i:s');
        return date('Y-m-d\TH:i:s', strtotime($now));
    }

    public function dataadd($data,$tempo){
        $data = new Time($data);
        $data->addMinute($tempo);
        $now = $data->format('Y-m-d H:i:s');
        return $now;
    }
    public function phone($phone){
        return '55'.str_replace('-','',str_replace(')','',str_replace('(','',$phone)));
    }

    function enviar($dados = array()){
        $sms = new \Sms();
        $sms->setTo($this->phone($dados['numero']));
        $sms->setMsg($dados['mensagem']);
        $sms->setSchedule($this->data($dados['data']));
        $sms->setId($dados['id']);
        $aggregateId=$dados['agregado'];
        try{
           $response =$this->smsFacade->send($sms, $aggregateId);
            $result = array(
                'status'=>$response->getStatusCode(),
                'StatusDescription'=>$response->getStatusDescription(),
                'DetailCode'=>$response->getDetailCode(),
                'DetailDescription'=>$response->getDetailDescription()
            );
            return $result;
        }
        catch(\Exception $ex ){
           return $ex->getMessage();
        }
    }


    function getMensagens($id){
        $response = $this->smsFacade->getStatus($id);

        $result = array(
            'status'=>$response->getStatusCode(),
            'StatusDescription'=>$response->getStatusDescription(),
            'DetailCode'=>$response->getDetailCode(),
            'DetailDescription'=>$response->getDetailDescription(),
            'id'=>$response->getId(),
            'recebido'=>$response->getReceived()
        );
        return $result;
    }


    function multiplas($dados){
        $list=array();
        $aggregateId = null;
        foreach ($dados as $dado) {
            $sms = new \Sms();
            $sms->setTo($this->phone($dado['numero']));
            $sms->setMsg($dado['mensagem']);
            $sms->setSchedule($this->data($dado['data']));
            $sms->setId($dado['id']);
            $list[] = $sms;
            $aggregateId=$dado['agregado'];
        }

        if(!empty($list)){
            try{
                $responses = $this->smsFacade->sendMultiple($list, $aggregateId);
                $result = array();
                foreach ($responses as $response) {
                    $result[] = array(
                        'status'=>$response->getStatusCode(),
                        'StatusDescription'=>$response->getStatusDescription(),
                        'DetailCode'=>$response->getDetailCode(),
                        'DetailDescription'=>$response->getDetailDescription(),
                        'id'=>$response->getId(),
                        'recebido'=>$response->getReceived()
                    );
                }

                return $result;
            }
            catch(\Exception $ex ){
                return $ex->getMessage();
            }
        }
    }

    function cancelar($id){
        try{
            $response = $this->smsFacade->cancel($id);
            $result[] = array(
                'status'=>$response->getStatusCode(),
                'StatusDescription'=>$response->getStatusDescription(),
                'DetailCode'=>$response->getDetailCode(),
                'DetailDescription'=>$response->getDetailDescription()
            );
            return $result;
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }


    function getRecebidos(){
        $result = array();
        try{
            $response = $this->smsFacade->listMessagesReceived();
            if ($response->hasMessages()) {
                $messages = $response->getReceivedMessages();
                foreach ($messages as $smsReceived) {
                    $result[] = array(
                        'celular'=>$smsReceived->getMobile(),
                        'data'=> $smsReceived->getDateReceived(),
                        'mensagem'=> $smsReceived->getBody(),
                        'id'=>$smsReceived->getSmsOriginId()
                    );
                }
            }
            return $result;
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }


    function fillRcebidos($dados){
        $result = array();
        try{
            $startPeriod=$this->data($dados['inicio']);
            $endPeriod=$this->data($dados['fim']);
            $mobile=$this->phone($dados['numero']);
            $smsId=$dados['id'];
            $response = $this->smsFacade->searchMessagesReceived($startPeriod, $endPeriod, $mobile, $smsId);
            if ($response->hasMessages()) {
                $messages = $response->getReceivedMessages();
                foreach ($messages as $smsReceived) {
                    $result = array(
                        'celular'=>$smsReceived->getMobile(),
                        'data'=> $smsReceived->getDateReceived(),
                        'mensagem'=> $smsReceived->getBody(),
                        'id'=>$smsReceived->getSmsOriginId()
                    );
                }
            }
            return $result;
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }

    function DateTimeSql($data){
        $dat = explode('/',$data);
        $dia = $dat[0];
        $mes = $dat[1];
        $ano_hora = explode(' ',$dat[2]);
        $ano = $ano_hora[0];
        $hora = $ano_hora[1];
        return $ano.'-'.$mes.'-'.$dia.' '.$hora;
    }

    function verificaSms($sms, $resposta, $cliente)
    {
        $icon = [];
        if($sms != '' && $cliente != '-1'){
            if($sms === '000'){// Enviado
                $icon = ['icon' => "<i class='fa fa-paper-plane'></i>", 'msg' => 'Enviado', 'tipo' => 'enviado'];
            }else if($sms === '120'){// Recebida
                $icon = ['icon' => "<i class='fa fa-flag'></i>", 'msg' => 'Recebida', 'tipo' => 'recebida'];
            }else if($sms === '1000'){// Respondido
                $icon = ['icon' => "<i class='fa fa-whatsapp'></i>", 'msg' => 'Respondido', 'tipo' => 'respondido'];
                if($resposta === '1'){ //Confirmado
                    $icon = ['icon' => "<i class='fa fa-thumbs-o-up'></i>", 'msg' => 'Confirmado', 'tipo' => 'confirmado'];
                }
            } else if($sms === '0'){//Agendamento não enviado nem pro sistema WEB
                $icon = ['icon' => "<i class='fa fa-times'></i>", 'msg' => 'Não Enviado', 'tipo' => 'Não Enviado Sistema SMS'];
            } else if($sms === '-10'){//Agedamento já foi varrido pelo sistema WEB
                $icon = ['icon' => "<i class='fa fa-cloud-upload'></i>", 'msg' => 'Enviado Sistema SMS', 'tipo' => 'Enviado Sistema SMS'];
            }
        }
        return $icon;
    }

}