<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class AgendaComponent extends Component
{
    public $components = ['Data'];

    public function connAgenda()
    {
        $agenda = TableRegistry::get('Agendas');
        return $agenda;
    }

   /**
    * Função que gera querys sql em string
    *
    * @param array $data
    * @param Integer $day
    * @return void
    */
    public function geraQuery($data = array(), $day)
    {
        $inicio = new Time($this->Data->DataSQL($data['data_inicio']));
        $fim = new Time($this->Data->DataSQL($data['data_fim']));
        $intervalo = new Time($data['intervalo']);
        $operacao = $data['operacao_agenda_horario_id'];
        $grupo_id = $data['grupo_id'];
        $user_id = $_SESSION['Auth']['User']['id'];
        $created = date('Y-m-d H:i:s');
        $tipo_id = $data['tipo_id'];
        $nome_provisorio = $data['nome_provisorio'];
        $data_hora_inicio = $inicio->format('Y-m-d').' '.$data['hora_inicio'].':00';
        $insert = "INSERT INTO agendas (cliente_id, convenio_id, grupo_id, tipo_id, situacao_agenda_id, agenda_horario_id, inicio, fim, created, modified, user_id, situacao_id, dia_semana, nome_provisorio) VALUES ";
        $values = "";
        $inInicio = "";
        $days = "";

        while($inicio <= $fim){
            $hora_inicio = new Time($data['hora_inicio']);
            $hora_fim = new Time($data['hora_fim']);
            while($hora_inicio <= $hora_fim){
                $data_inicio = $inicio->format('d/m/Y').' '.$hora_inicio->format('H:i');
                // Convert inicio in Datetime sql
                $dt_inicio_sql = $inicio->format('Y-m-d').' '.$hora_inicio->format('H:i').':00';
                // data fim e Incrementador intervalo
                $data_fim_sql = $inicio->format('Y-m-d').' '.$hora_inicio->modify('+'.$intervalo->format('i').' minute')->format('H:i').':00';

                if($operacao == 1 || $operacao == 2){ // Gerar Horarios e Bloquear'
                        $situacao = 5;
                    if($operacao == 2)
                        $situacao = 1000;
                    // Verifica se existe agendamento e se entre o periodo selecionado existe o dia da semana selecionado
                    if($this->connAgenda()->checkAgendamento($grupo_id, $dt_inicio_sql, null, false) && $this->connAgenda()->checkDiaSemana($dt_inicio_sql, $day)){
                        $values .= "(-1, -1, $grupo_id, $tipo_id, $situacao, -1, '$dt_inicio_sql', '$data_fim_sql', '$created', '$created', $user_id, 1, $day, '$nome_provisorio'),";
                    }
                }
                $inInicio .= "'$dt_inicio_sql',";
            }
            $inicio->modify('+1 days');// incrementador de dias
        }

        $days = implode(',', $data['dias_semana']);
        $inInicio = substr($inInicio, 0, -1);
        switch ($operacao){
            case 1:// Agendar
                $sql = (!empty($values))?$insert.$values:' ';
                break;
            case 2:// Bloquear
                $sql = "UPDATE agendas SET situacao_agenda_id = 1000, nome_provisorio = '$nome_provisorio', modified = '$created' WHERE grupo_id = $grupo_id AND cliente_id = -1 AND convenio_id = -1 AND inicio IN($inInicio) AND dia_semana IN ($days) AND agenda_horario_id IS NOT NULL; \n";
                $sql .= (!empty($values)) ? $insert.$values : '';
                break;
            case 3:// Desbloqeuar
                $sql = "UPDATE agendas SET situacao_agenda_id = 5, modified = '$created', nome_provisorio = NULL WHERE grupo_id = $grupo_id AND situacao_agenda_id = 1000 AND inicio IN($inInicio) AND dia_semana IN ($days); ";
                break;
            case 4:// Excluir
                $sql = "UPDATE agendas SET situacao_id = 2, modified = '$created' WHERE grupo_id = $grupo_id AND cliente_id = -1 AND inicio IN($inInicio) AND dia_semana IN ($days) AND agenda_horario_id IS NOT NULL; ";
                break;
            default: $sql = '';
        }

        return substr($sql, 0, -1);
    }

    public function getTiposRelatoriosAgenda()
    {
        return [
          1 => 'Agendamentos c/ Dados de Paciente',
          2 => 'Agendamentos s/ Dados de Paciente'
        ];
    }

    /**
     * Monta grade virtual de horarios, para o calendario
     *
     * @param String $data
     * @param array|Integer $grupo_id
     * @return array
     */
    function mountVirtualGride($data, $grupos) 
    {
        //$agendaPeriodos = TableRegistry::get('AgendaPeriodos');
        if(is_array($grupos))
            $in = 'IN';
        else
            $in = '';

        $dates_week = $this->Data->getDateWeek($data);
        $grid = [];
        foreach($dates_week as $dw)
        {
            $now = new Time($dw);
            $agendaPeriodo = $this->connAgenda()->GrupoAgendas->AgendaPeriodos->find('all')
                    ->contain(['GrupoAgendas', 'TipoAgendas'])
                    ->where(['AgendaPeriodos.grupo_agenda_id '.$in => $grupos])
                    ->andWhere(['AgendaPeriodos.situacao_id' => 1])
                    ->andWhere(['AgendaPeriodos.dia_semana' => $now->format('w')]);
    
            foreach($agendaPeriodo as $ap)
            {
                $inicio = new Time($ap->inicio);
                $fim = new Time($ap->fim);
                $intervalo = ($ap->has('tipo_agenda') && !empty($ap->intervalo)) ? $ap->intervalo : $ap->grupo_agenda->intervalo;
                $intervalo = new Time($intervalo);
                while($inicio < $fim)
                {
                    $start = $now->format('Y-m-d').' '.$inicio->format('H:i').':00';
                    $end = $now->format('Y-m-d').' '.$inicio->modify('+'.$intervalo->format('H').' hours'. '+'.$intervalo->format('i').' minute')->format('H:i').':00';
                    if($ap->has('tipo_agenda'))
                    {
                        $grid[] = [
                            'title' => $ap->tipo_agenda->nome,                        
                            'start' => $this->Data->data_iso($start),
                            'end' => $this->Data->data_iso($end),
                            'textColor' => $ap->tipo_agenda->cor,
                            'event_empty' => true,
                            'tipo_agenda' => $ap->tipo_agenda->id,
                            'backgroundColor' => '#fff8c4',
                            'borderColor' => '#ccc',
                        ];
                    }
                    
                }
            }
        }

        return $grid;
    }
    
}