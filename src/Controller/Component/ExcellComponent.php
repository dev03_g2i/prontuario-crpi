<?php
/**
 * Created by PhpStorm.
 * User: Tiago Farias
 * Date: 26/10/2016
 * Time: 09:20
 */
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
include  'PHPExcell/Classes/PHPExcel.php';
class ExcellComponent extends Component
{
    public function path(){
        $path = dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."planilhas". DS ."excell";
        $dir = new Folder($path);
        if(!$dir->path){
            $dir->create($path,0755);
        }
        return $dir->path;
    }

    public function create($file_name,$header,$data){
        $path = $this->path();
        $file = new File($path.DS.$file_name);
        if(!$file->exists()){
            $file->create();
        }

        $excell = new \PHPExcel();

        $excell->setActiveSheetIndex(0);
        $a=1;
        $alf=65;
        foreach ($header as $h) {
            $colun = chr($alf).$a;
            $excell->getActiveSheet()->getStyle($colun)->getFont()->setBold(true);
            $excell->getActiveSheet()->setCellValue($colun,$h);
            $alf++;
        }

        $x=2;
        $y=0;
        foreach ($data as $d){
            foreach ($d as $item) {
                echo '<pre>';
                echo $y;
                echo '</pre>';
                $excell->getActiveSheet()->setCellValueByColumnAndRow($y,$x,$item);
                $y++;
            }
            $x++;
            $y=0;
        }
        $excell->getActiveSheet()->setTitle($file_name);
        $objWriter = \PHPExcel_IOFactory::createWriter($excell, 'Excel2007');
        $objWriter->save($path.DS.$file_name.'.xlsx');

        return dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."planilhas";
    }
}
