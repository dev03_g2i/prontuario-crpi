<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\I18n\Time;
use DateTime;

class DataComponent extends Component {

    function getAmount($money)
    {
        try{
            $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
            $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

            $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

            $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
            $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

            return (float) str_replace(',', '.', $removedThousendSeparator);
        }catch (Exception $e){
            return null;
        }
    }

    function getMinutos($intervalo){

        $time = new Time($intervalo);

        $hora = $time->format('H');
        $minuto = $time->format('i');

        $result = ($hora*60)+$minuto;

        return $result;
    }
    function DataSQL($data) {
        try {
            if ($data != "") {
                list($d, $m, $y) = preg_split('/\//', $data);
                return sprintf('%04d-%02d-%02d', $y, $m, $d);
            }
        }catch (Exception $e){
            return null;
        }
    }

    function DateTimeSql($data){
        $dt = new DateTime();
        $now = $dt->createFromFormat('d/m/Y H:i', $data);
        return $now->format('Y-m-d H:i:s');
    }

    public function data_iso($data){
        $data = new Time($data);
        $now = $data->format('Y-m-d H:i:s');
        return date('Y-m-d\TH:i:s', strtotime($now));
    }

    public function data_iso2($data){
        $data = new Time($data);
        $now = $data->format('Y-m-d h:i:s');
        return date('Y-m-d\Th:i:s', strtotime($now));
    }
    
    public function data_sms($data){
        $data = new Time($data);
        $now = $data->format('d/m/Y');
        return $now;
    }
    public function hora_sms($data){
        $data = new Time($data);
        $now = $data->format('H:i');
        return $now;
    }

    public function date_time($data){
        $data = new Time($data);
        $now = $data->format('d/m/Y H:i');
        return $now;
    }

    public function dateTimestamp($data){
        $new_date = explode(' ', $data);
        $data = new Time($data);
        if(array_key_exists(1, $new_date)){
            $now = $data->format('Y-m-d H:i:s');
            return date('YmdHis', strtotime($now));
        }else {
            $now = $data->format('Y-m-d');
            return date('Ymd', strtotime($now));
        }
    }

    public function semana(){
        $time = date('Y-m-d',strtotime('monday this week'));
        $data = new Time($time);
        $domingo = $data->subDay(1);

        $prox = date('Y-m-d',strtotime('monday next week'));
        $next_monday = new Time($prox);
        $sabado = $next_monday->subDay(2);

        return (object)['domingo'=>$domingo->format('d'),'sabado'=>$sabado->format('d')];
    }

    public function data_semana(){
        $time = date('Y-m-d',strtotime('monday this week'));
        $data = new Time($time);
        $domingo = $data->subDay(1);

        $prox = date('Y-m-d',strtotime('monday next week'));
        $next_monday = new Time($prox);
        $sabado = $next_monday->subDay(2);

        return (object)['domingo'=>$domingo->format('Y-m-d'),'sabado'=>$sabado->format('Y-m-d')];
    }

    public function meses(){
        $meses = array(
            '01' => "Janeiro",
            '02' => "Fevereiro",
            '03' => "Março",
            '04' => "Abril",
            '05' => "Maio",
            '06' => "Junho",
            '07' => "Julho",
            '08' => "Agosto",
            '09' => "Setembro",
            '10' => "Outubro",
            '11' => "Novembro",
            '12' => "Dezembro"
        );
        return $meses;
    }

    public function mesAtual($date = null) {
        if ($date && $date != null && $date != '') {
            $date = new Time($date);
            $mes = $date->format('m');
            $ano = $date->format('Y');

            foreach ($this->meses() as $key => $value){
                if($key == $mes){
                    $mes = $value.'-'.$ano;
                }
            }
        } else {
            $mes = '';
        }
        return $mes;
    }

    public function NomeMes($date = null) {
        if ($date && $date != null && $date != '') {
            $date = new Time($date);
            $mes = $date->format('m');

            foreach ($this->meses() as $key => $value){
                if($key == $mes){
                    $mes = $value;
                }
            }
        } else {
            $mes = '';
        }
        return $mes;
    }


    public function ultimosMeses($date = null) {

        if (!$date || $date == null || $date == '') {
            $date = new Time();
            $mes1 = $date->format('m');
            $mes2 = $date->subMonth(1)->format('m');
            $mes3 = $date->subMonth(1)->format('m');
            $ultimosMeses = [];

            foreach ($this->meses() as $key => $value){
                if($key == $mes1 || $key == $mes2 || $key == $mes3){
                    $ultimosMeses[] = $value;
                }
            }
            
        } else {
            $date = new Time($date);
            $mes1 = $date->format('m');
            $ano[1] = $date->format('Y');
            $mes2 = $date->subMonth(1)->format('m');
            $ano[2] = $date->format('Y');
            $mes3 = $date->subMonth(1)->format('m');
            $ano[3] = $date->format('Y');
            $ultimosMeses = [];
            $i = 1;

            foreach ($this->meses() as $key => $value){
                if($key == $mes1 || $key == $mes2 || $key == $mes3){
                    $ultimosMeses[] = $value.'-'.$ano[$i];
                    $i++;
                }
            }
            
        }
        return $ultimosMeses;
    }

    public function getLastTwelveMonths($date = null)
    {   
        $j = 1;
        for($i = 11; $i >= 0; $i--)
        {
            $meses[$i] = new \StdClass();
            if ($i == 11) {
                $meses[$i]->year = $date->format('Y');
                $meses[$i]->month = $date->format('m');
            } else {
                $meses[$i]->month = $date->subMonth(1)->format('m');
                $meses[$i]->year = $date->format('Y');
            }
        }

        foreach ($this->meses() as $key => $value)
        {
            foreach($meses as $mes){
                if($key == $mes->month)
                {
                    $mes->label = $value.'-'.$mes->year;
                    $j++;
                }
            }
        }
        
        return $meses;
    }

    public function idade($nascimento) {
        $data = new DateTime();
        $data = $data->createFromFormat('d/m/Y', $nascimento);
        $data_atual = new DateTime();
        $diff = $data->diff($data_atual);
        return $diff->format('%y');
    }
    public function calculaIdade($nascimento){
        $idade = new Time($nascimento);
        $diff = $idade->diff(new DateTime());
        return $diff->format("%y");
    }

    public function formatDatetime($datetimepicker)
    {
        $data = new DateTime();
        $data = $data->createFromFormat('d/m/Y H:i', $datetimepicker);
        return $data;
    }

    public function ultimoDiaMes($mes_atual)
    {
        return date("t", mktime(0,0,0,$mes_atual,'01',date("Y")));
    }

    /** Traz os dias do mes
     * @param $year
     * @param $month
     * @return array
     */
    public function getDaysMonth($year, $month){

        $days = [];
        $num_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for($i=0;$i<$num_of_days;$i++){
            $days[] = $i+1;
        }
        return $days;
    }

    public function years()
    {
        $rangeYear = range(2000, date('Y'));
        $years =[];
        foreach ($rangeYear as $k => $v){
            $years[$v] = $v;
        }
        return $years;
    }

    /**
     * Convert date 1111-11-11 to 11/11/1111
     *
     * @param String|Objet $data
     * @return void
     */
    function DateBr($data){
        $inicio = new DateTime($data);
        return $inicio->format('d/m/Y');
    }

    /**
     * traz todas as datas da semana de acordo com uma data
     *
     * @param String $data
     * @return array
     */
    public function getDateWeek($data)
    {
        $ts = strtotime($data);
        $year = date('o', $ts);
        $week = date('W', $ts);
        $dates = [];
        for($i = 0; $i <= 6; $i++) {
            $ts = strtotime($year.'W'.$week.$i);
            $dates[] = date("Y-m-d", $ts);
        }
        return $dates;
    }

}