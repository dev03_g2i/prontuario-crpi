<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

class JsonComponent extends Component {
    const DIRECTORY_FILES_UPLOAD = 'report/';
    public function path(){
        $path = dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."reports". DS ."json";
        $dir = new Folder($path);
        if(!$dir->path){
            $dir->create($path,0755);
        }
        return $dir->path;
    }

    public function create($file_name,$data){
        $path = $this->path();
        $file = new File($path.DS.$file_name);
        if(!$file->exists()){
            $file->create();
        }

        if($file->writable()){
            $file->write($data);
            $file->close();
        }

        return dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."reports";
    }
}