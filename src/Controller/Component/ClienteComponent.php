<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ClienteComponent extends Component
{
    public function limpaCpf($cpf)
    {
        if (!empty($cpf)) {
            $cpf = trim($cpf);
            $cpf = str_replace('.', '', $cpf);
            $cpf = str_replace('-', '', $cpf);
            return $cpf;
        }
    }

    public function aniversariante($dataNascimento)
    {
        $dataNascimento = date_format($dataNascimento, 'd/m');
        $dataAtual = date('d/m');

        if ($dataNascimento == $dataAtual) {
            return true;
        } else {
            return false;
        }
    }

}