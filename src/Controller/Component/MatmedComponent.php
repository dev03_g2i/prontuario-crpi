<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class MatmedComponent extends Component
{
    /* renomear precoArtigoConvenio */
    public function precoArtigo($artigo_id)
    {
        $FaturaPrecoartigo = TableRegistry::get('FaturaPrecoartigo');
        $dados = $FaturaPrecoartigo->find('all')->where(['FaturaPrecoartigo.artigo_id' => $artigo_id])->first();
        if(!empty($dados)){
            return $dados;
        }else {
            return null;
        }
    }
    public function atualizaValorMaterial($atendProc_id)
    {
        $FaturaMatmed = TableRegistry::get('FaturaMatmed');

        $query = $FaturaMatmed->find();
        $query_select = $query->select(['total' => $query->func()->sum('FaturaMatmed.total')])
            ->where(['FaturaMatmed.atendimento_procedimento_id' => $atendProc_id])->toArray();

        $atendProc = $FaturaMatmed->AtendimentoProcedimentos->get($atendProc_id);
        $atendProc->valor_material = $query_select[0]->total;
        $FaturaMatmed->AtendimentoProcedimentos->save($atendProc);
    }
}