<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class ConfiguracaoComponent extends Component
{
    public $components = array('Data');

    public function configuracoes()
    {
        $configuracoes = TableRegistry::get('Configuracoes');
        return $configuracoes->find()->first();
    }

    public function configuracoesFaturamento()
    {
        $configuracaoFaturamento = TableRegistry::get('ConfiguracaoFaturamentos');
        return $configuracaoFaturamento->find()->first();
    }

    public function configuracoesCadPaciente()
    {
        $configuracaoCadPaciente = TableRegistry::get('ConfiguracaoCadpaciente');
        return $configuracaoCadPaciente->find()->first();
    }

    public function getMrtFichaAtendimento()
    {
        return $this->configuracoes()->ficha_impressa_atendimento;
    }

    public function showPlanoContas()
    {
        return $this->configuracoes()->mostra_caixa_plano;
    }

    public function getIdPlanoContasPadrao()
    {
        return $this->configuracoes()->caixa_idplano_contas;
    }


// Configuração para mostrar previsão de entrega dos atendimentos
    public
    function showPrevEntrega()
    {
        if ($this->configuracoes()->atendimento_dt_entrega == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @see Configuração para verificar se vai mostrar o campo de controle financeiro no cadastro de Atendimento Financeiro.
     */
    public
    function showCampoControleFinanceiro()
    {
        if ($this->configuracoes()->controle_finsetor_atendproc == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @see Método para retornar o valor padrão pro campo controle financeiro para atendimento procedimento.
     */
    public
    function getControleFinanceiroAtendProcPadrao()
    {
        return $this->configuracoes()->controle_finsetor_atendproc_padrao;
    }

    /**
     * @see Configuração para verificar se vai mostrar o campo de controle financeiro no cadastro de contas a receber.
     */
    public
    function showCampoControleFinanceiroContasReceber()
    {
        if ($this->configuracoes()->controle_financsetor_receita == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @see Método para retornar o valor padrão pro campo controle financeiro das contas a receber.
     */
    public
    function getControleFinanceiroContasReceberPadrao()
    {
        return $this->configuracoes()->controle_financsetor_receita_padrao;
    }

    /**
     * Verficia a permissão de visualização do prontuario.
     * @class ClienteHistoricos
     *
     * @return void
     */
    public function prontuarioPermissaoVisualizacao()
    {
        return $this->configuracoes()->prontuario_permissao_visualizacao;
    }

    /**
     * @see Método que retorna a configuração do campo site identifier para o XML de integração
     * Se for 1 vai pegar o número da ficha (Conforme era antes)
     * Se for 0 vai pegar o valor do campo integracao_mv_site_identifier_valor
     * @return mixed
     */
    public function getConfigMvSiteIdentifier()
    {
        return $this->configuracoes()->integracao_mv_site_identifier;
    }

    public function getValorMvSiteIdentifier()
    {
        return $this->configuracoes()->integracao_mv_site_identifier_valor;
    }

    /**
     * @see Método que retorna a configuração do campo last_name dentro do referring_physician_details
     * Se for 1 vai pegar o id do solicitante da tabela de atendimento procedimento
     * Se for 0 vai pegar o id do solicitante da tabela de atendimento (Valor Default)
     * IMPORTANTE estar habilitado solicitante atendimento procedimento - configuração
     * @return mixed
     */
    public function getConfigMvSolicitante()
    {
        return $this->configuracoes()->integracao_mv_solicitante;
    }


    public function getMrtFichaPaciente()
    {
        return $this->configuracoes()->repor_ficha_paciente;
    }

    public function mostraOpcoesEquipes()
    {
        if ($this->configuracoesFaturamento()->tela_fatura_equipe == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getConfigRestringeCpf()
    {
        return $this->configuracoesCadPaciente()->restringe_cpf;
    }

        public function getConfigDuplicaCpf()
    {
        return $this->configuracoesCadPaciente()->duplica_cpf;
    }
}