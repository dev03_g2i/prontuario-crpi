<?php
/**
 * Created by PhpStorm.
 * User: Vagner
 * Date: 02/02/2017
 * Time: 13:45
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use DateTime;

class XmlComponent extends Component
{
    public $components = ['Data'];

    public function getDadosComuns()
    {
        return array(
            'sequencialTransacao' => 1 #random
        );
    }

    /**
     * Suporta 3.02.00
     * @return string
     */
    public
    function getCabecalho30200($convenio, $randomNumber)
    {
        $tipoTransaaco = 'ENVIO_LOTE_GUIAS';
        $dataRegistroTransacao = date('Y-m-d');
        $horaRegistroTransacao = date('H:s:i');
        $tipoCodigo = $convenio->tiss_tipocodigo;
        $codcliconvenio = $convenio->tiss_codcliconvenio;
        $registroANS = !empty($convenio->tiss_registroans) ? str_replace('-', '', $convenio->tiss_registroans) : '000000';
        $padrao = '3.02.00';
        return
            "
              <ans:mensagemTISS xmlns:ans='http://www.ans.gov.br/padroes/tiss/schemas'>
                <ans:cabecalho>
                    <ans:identificacaoTransacao>
                        <ans:tipoTransacao>" . $tipoTransaaco . "</ans:tipoTransacao>
                        <ans:sequencialTransacao>" . $randomNumber . "</ans:sequencialTransacao>
                        <ans:dataRegistroTransacao>" . $dataRegistroTransacao . "</ans:dataRegistroTransacao>
                        <ans:horaRegistroTransacao>" . $horaRegistroTransacao . "</ans:horaRegistroTransacao>
                    </ans:identificacaoTransacao>
                    <ans:origem>
                        <ans:identificacaoPrestador>
                            <ans:$tipoCodigo>" . $codcliconvenio . "</ans:$tipoCodigo>
                        </ans:identificacaoPrestador>
                    </ans:origem>
                    <ans:destino>
                        <ans:registroANS>" . $registroANS . "</ans:registroANS>
                    </ans:destino>
                    <ans:versaoPadrao>" . $padrao . "</ans:versaoPadrao>
                </ans:cabecalho>  
            ";
    }

    /**
     * Suporta 3.03.01
     * @return string
     */
    public
    function getCabecalho30301($convenio, $randomNumber)
    {
        $tipoTransaaco = 'ENVIO_LOTE_GUIAS';
        $dataRegistroTransacao = date('Y-m-d');
        $horaRegistroTransacao = date('H:s:i');
        $tipoCodigo = $convenio->tiss_tipocodigo;
        $codcliconvenio = $convenio->tiss_codcliconvenio;
        $registroANS = !empty($convenio->tiss_registroans) ? str_replace('-', '', $convenio->tiss_registroans) : '000000';
        $padrao = '3.03.01';
        return
            "
              <ans:mensagemTISS xmlns:ans='http://www.ans.gov.br/padroes/tiss/schemas'>
                <ans:cabecalho>
                    <ans:identificacaoTransacao>
                        <ans:tipoTransacao>" . $tipoTransaaco . "</ans:tipoTransacao>
                        <ans:sequencialTransacao>" . $randomNumber . "</ans:sequencialTransacao>
                        <ans:dataRegistroTransacao>" . $dataRegistroTransacao . "</ans:dataRegistroTransacao>
                        <ans:horaRegistroTransacao>" . $horaRegistroTransacao . "</ans:horaRegistroTransacao>
                    </ans:identificacaoTransacao>
                    <ans:origem>
                        <ans:identificacaoPrestador>
                            <ans:$tipoCodigo>" . $codcliconvenio . "</ans:$tipoCodigo>
                        </ans:identificacaoPrestador>
                    </ans:origem>
                    <ans:destino>
                        <ans:registroANS>" . $registroANS . "</ans:registroANS>
                    </ans:destino>
                    <ans:Padrao>" . $padrao . "</ans:Padrao>
                </ans:cabecalho>  
            ";
    }

    /**
     * Suporta 3.03.02
     * @return string
     */
    public
    function getCabecalho30302($convenio, $randomNumber)
    {
        $tipoTransaaco = 'ENVIO_LOTE_GUIAS';
        $dataRegistroTransacao = date('Y-m-d');
        $horaRegistroTransacao = date('H:s:i');
        $tipoCodigo = $convenio->tiss_tipocodigo;
        $codcliconvenio = $convenio->tiss_codcliconvenio;
        $registroANS = !empty($convenio->tiss_registroans) ? str_replace('-', '', $convenio->tiss_registroans) : '000000';
        $padrao = '3.03.02';
        return
            "
              <ans:mensagemTISS xmlns:ans='http://www.ans.gov.br/padroes/tiss/schemas'>
                <ans:cabecalho>
                    <ans:identificacaoTransacao>
                        <ans:tipoTransacao>" . $tipoTransaaco . "</ans:tipoTransacao>
                        <ans:sequencialTransacao>" . $randomNumber . "</ans:sequencialTransacao>
                        <ans:dataRegistroTransacao>" . $dataRegistroTransacao . "</ans:dataRegistroTransacao>
                        <ans:horaRegistroTransacao>" . $horaRegistroTransacao . "</ans:horaRegistroTransacao>
                    </ans:identificacaoTransacao>
                    <ans:origem>
                        <ans:identificacaoPrestador>
                            <ans:$tipoCodigo>" . $codcliconvenio . "</ans:$tipoCodigo>
                        </ans:identificacaoPrestador>
                    </ans:origem>
                    <ans:destino>
                        <ans:registroANS>" . $registroANS . "</ans:registroANS>
                    </ans:destino>
                    <ans:Padrao>" . $padrao . "</ans:Padrao>
                </ans:cabecalho>  
            ";
    }

    /**
     * Suporta 3.03.03
     * @return string
     */
    public
    function getCabecalho30303($convenio, $randomNumber)
    {
        $tipoTransaaco = 'ENVIO_LOTE_GUIAS';
        $dataRegistroTransacao = date('Y-m-d');
        $horaRegistroTransacao = date('H:s:i');
        $tipoCodigo = $convenio->tiss_tipocodigo;
        $codcliconvenio = $convenio->tiss_codcliconvenio;
        $registroANS = !empty($convenio->tiss_registroans) ? str_replace('-', '', $convenio->tiss_registroans) : '000000';
        $padrao = '3.03.03';
        return
            "
              <ans:mensagemTISS xmlns:ans='http://www.ans.gov.br/padroes/tiss/schemas'>
                <ans:cabecalho>
                    <ans:identificacaoTransacao>
                        <ans:tipoTransacao>" . $tipoTransaaco . "</ans:tipoTransacao>
                        <ans:sequencialTransacao>" . $randomNumber . "</ans:sequencialTransacao>
                        <ans:dataRegistroTransacao>" . $dataRegistroTransacao . "</ans:dataRegistroTransacao>
                        <ans:horaRegistroTransacao>" . $horaRegistroTransacao . "</ans:horaRegistroTransacao>
                    </ans:identificacaoTransacao>
                    <ans:origem>
                        <ans:identificacaoPrestador>
                            <ans:$tipoCodigo>" . $codcliconvenio . "</ans:$tipoCodigo>
                        </ans:identificacaoPrestador>
                    </ans:origem>
                    <ans:destino>
                        <ans:registroANS>" . $registroANS . "</ans:registroANS>
                    </ans:destino>
                    <ans:Padrao>" . $padrao . "</ans:Padrao>
                </ans:cabecalho>  
            ";
    }

    /**
     * Suporta 3.02.00
     * @return string
     */
    public
    function getPrestadorParaOperadora30200($atendimentos, $convenio, $randomNumber)
    {
        $configTable = TableRegistry::get('Configuracoes');
        $solicitanteTable = TableRegistry::get('Solicitantes');
        $configuracao = $configTable->getFirstConfiguracao();

        $bodyXml =
            "    <ans:prestadorParaOperadora>
                    <ans:loteGuias>
                    <ans:numeroLote>" . $randomNumber . "</ans:numeroLote>
                    <ans:guiasTISS>
            ";

        $atendimentoAtual = 0;
        $atendimentoAnterior = 0;
        foreach ($atendimentos as $atendimento) {
            $atendimentoAtual = $atendimento->id;
            if ($atendimentoAtual != $atendimentoAnterior) {

                $solicitante = $solicitanteTable->get($atendimento->solicitante_id);
                $cbos = !empty($solicitante->tiss_cbos) ? $solicitante->tiss_cbos : 201115;
                $coselhoProfissional = 1;
                $tipoAtendimento = $atendimento->tiss_tipoatendimento;
                $codigoTabela = !empty($convenio->tiss_codtabpreco) ? $convenio->tiss_codtabpreco : '00';
                $observacao = !empty($atendimento->observacao) ? $atendimento->observacao : $atendimento->id;
                $registroANS = !empty($convenio->tiss_registroans) ? str_replace('-', '', $convenio->tiss_registroans) : '000000';
                $solicitanteNome = $solicitante->nome;
                $exacutanteContratadoNome = $configuracao->nome_login;
                $numeroConselhoProfissional = $solicitante->conselho;
                $uf = !empty($solicitante->estado) ? $solicitante->estado : '50';
                $numeroGuiaPrestador = $atendimento->id;
                $numeroGuiaOperadora = $atendimento->guia;
                $dataAtendimento = date_format($atendimento->data, 'Y-m-d');
                $codigoPrestadorNaOperadora = $solicitante->id;
                $cnes = $configuracao->tiss_cnes;
                $caraterAtendimento = 1;
                $indicacaoClinica = 'Z00';
                $numeroCarteira = !empty($atendimento->cliente->matricula) ? $atendimento->cliente->matricula : '0000000000000000';
                $atendimentoRn = 'N';
                $nomeBeneficiario = $atendimento->cliente->nome;
                $indicacaoAcidente = 2;
                $codcliconvenio = $convenio->tiss_codcliconvenio;

                foreach ($atendimento->atendimento_procedimentos as $ap) {
                    $numeroGuiaOperadora = !empty($ap->nr_guia) ? $ap->nr_guia : 'Não Informado';
                    $senha = !empty($ap->autorizacao_senha) ? $ap->autorizacao_senha : 'Não Informado';
                    $dataAutorizacao = !empty($ap->dt_autorizacao) ? date_format($ap->dt_autorizacao, 'Y-m-d') : date_format($atendimento->data, 'Y-m-d');
                }

                $bodyXml .=
                    "
                            <ans:guiaSP-SADT>
                                <ans:cabecalhoGuia>
                                    <ans:registroANS>" . $registroANS . "</ans:registroANS>
                                    <ans:numeroGuiaPrestador>" . $numeroGuiaPrestador . "</ans:numeroGuiaPrestador>
                                </ans:cabecalhoGuia>
                                <ans:dadosAutorizacao>
                                    <ans:numeroGuiaOperadora>" . $numeroGuiaOperadora . "</ans:numeroGuiaOperadora>
                                    <ans:dataAutorizacao>" . $dataAutorizacao . "</ans:dataAutorizacao>
                                    <ans:senha>" . $senha . "</ans:senha>
                                </ans:dadosAutorizacao>
                                <ans:dadosBeneficiario>
                                    <ans:numeroCarteira>" . $numeroCarteira . "</ans:numeroCarteira>
                                    <ans:atendimentoRN>" . $atendimentoRn . "</ans:atendimentoRN>
                                    <ans:nomeBeneficiario>" . $nomeBeneficiario . "</ans:nomeBeneficiario>
                                </ans:dadosBeneficiario>
                                <ans:dadosSolicitante>
                                    <ans:contratadoSolicitante>
                                        <ans:codigoPrestadorNaOperadora>" . $codcliconvenio . "</ans:codigoPrestadorNaOperadora>
                                        <ans:nomeContratado>" . $solicitanteNome . "</ans:nomeContratado>
                                    </ans:contratadoSolicitante>
                                    <ans:profissionalSolicitante>
                                        <ans:nomeProfissional>" . $solicitanteNome . "</ans:nomeProfissional>
                                        <ans:conselhoProfissional>" . $coselhoProfissional . "</ans:conselhoProfissional>
                                        <ans:numeroConselhoProfissional>" . $numeroConselhoProfissional . "</ans:numeroConselhoProfissional>
                                        <ans:UF>" . $uf . "</ans:UF>
                                        <ans:CBOS>" . $cbos . "</ans:CBOS>
                                    </ans:profissionalSolicitante>
                                </ans:dadosSolicitante>
                                <ans:dadosSolicitacao>
                                    <ans:dataSolicitacao>" . $dataAtendimento . "</ans:dataSolicitacao>
                                    <ans:caraterAtendimento>" . $caraterAtendimento . "</ans:caraterAtendimento>
                                    <ans:indicacaoClinica>" . $indicacaoClinica . "</ans:indicacaoClinica>
                                </ans:dadosSolicitacao>
                                <ans:dadosExecutante>
                                    <ans:contratadoExecutante>
                                        <ans:codigoPrestadorNaOperadora>" . $codcliconvenio . "</ans:codigoPrestadorNaOperadora>
                                        <ans:nomeContratado>" . $exacutanteContratadoNome . "</ans:nomeContratado>
                                    </ans:contratadoExecutante>
                                    <ans:CNES>" . $cnes . "</ans:CNES>
                                </ans:dadosExecutante>
                                <ans:dadosAtendimento>
                                    <ans:tipoAtendimento>" . $tipoAtendimento . "</ans:tipoAtendimento>
                                    <ans:indicacaoAcidente>" . $indicacaoAcidente . "</ans:indicacaoAcidente>
                                </ans:dadosAtendimento>
                                <ans:procedimentosExecutados>
                ";
                $total_procedimentos = 0;
                $total_materiais = 0;
                foreach ($atendimento->atendimento_procedimentos as $ap) {
                    $total_procedimentos += $ap->total;
                    $total_materiais += $ap->valor_material;

                    $codigoProcedimento = $ap->codigo;
                    $descricaoProcedimento = $ap->procedimento->nome;
                    $quantidadeExecutada = $ap->quantidade;
                    $viaAcesso = 1;
                    $tecnicaUtilizada = 1;
                    $reducaoAcrescimo = !empty($ap->percentual_cobranca) ? $ap->percentual_cobranca : 1;
                    $valorUnitario = number_format($ap->valor_fatura, 2, '.', '');
                    $valorTotal = number_format($ap->total, 2, '.', '');

                    $bodyXml .=
                        "
                                    <ans:procedimentoExecutado>
                                        <ans:dataExecucao>" . $dataAtendimento . "</ans:dataExecucao>
                                        <ans:procedimento>
                                            <ans:codigoTabela>" . $codigoTabela . "</ans:codigoTabela>
                                            <ans:codigoProcedimento>" . $codigoProcedimento . "</ans:codigoProcedimento>
                                            <ans:descricaoProcedimento>" . $descricaoProcedimento . "</ans:descricaoProcedimento>
                                        </ans:procedimento>
                                        <ans:quantidadeExecutada>" . $quantidadeExecutada . "</ans:quantidadeExecutada>
                                        <ans:viaAcesso>" . $viaAcesso . "</ans:viaAcesso>
                                        <ans:tecnicaUtilizada>" . $tecnicaUtilizada . "</ans:tecnicaUtilizada>
                                        <ans:reducaoAcrescimo>" . $reducaoAcrescimo . "</ans:reducaoAcrescimo>
                                        <ans:valorUnitario>" . $valorUnitario . "</ans:valorUnitario>
                                        <ans:valorTotal> " . $valorTotal . "</ans:valorTotal>
                                    </ans:procedimentoExecutado>
                    ";
                }
                $atendimentoAnterior = $atendimento->id;


                $totalGeral = number_format(($total_procedimentos + $total_materiais), 2, '.', '');

                $bodyXml .=
                    "               </ans:procedimentosExecutados>
                                <ans:observacao>" . $observacao . "</ans:observacao>
                                <ans:valorTotal>
                                    <ans:valorProcedimentos>" . number_format($total_procedimentos, 2, '.', '') . "</ans:valorProcedimentos>
                                    <ans:valorMateriais>" . number_format($total_materiais, 2, '.', '') . "</ans:valorMateriais>
                                    <ans:valorMedicamentos>0.00</ans:valorMedicamentos>
                                    <ans:valorTotalGeral>" . $totalGeral . "</ans:valorTotalGeral>
                                </ans:valorTotal>
                             </ans:guiaSP-SADT>
                ";
            }
        }
        $bodyXml .=
            "       
                    </ans:guiasTISS>
                    </ans:loteGuias>
                </ans:prestadorParaOperadora>
            ";
        return $bodyXml;

    }

    /**
     * Suporta 3.03.01, 3.03.02, 3.03.03
     * @return string
     */
    public
    function getPrestadorParaOperadora30302($atendimentos, $convenio, $randomNumber)
    {
        $configTable = TableRegistry::get('Configuracoes');
        $solicitanteTable = TableRegistry::get('Solicitantes');
        $configuracao = $configTable->getFirstConfiguracao();

        $bodyXml =
            "    <ans:prestadorParaOperadora>
                    <ans:loteGuias>
                    <ans:numeroLote>" . $randomNumber . "</ans:numeroLote>
                    <ans:guiasTISS>
            ";

        $atendimentoAtual = 0;
        $atendimentoAnterior = 0;
        foreach ($atendimentos as $atendimento) {
            $atendimentoAtual = $atendimento->id;
            if ($atendimentoAtual != $atendimentoAnterior) {

                $solicitante = $solicitanteTable->get($atendimento->solicitante_id);
                $cbos = !empty($solicitante->tiss_cbos) ? $solicitante->tiss_cbos : 201115;
                $coselhoProfissional = !empty($atendimento->solicitante->siglas) ? $atendimento->solicitante->siglas : '01';
                $tipoAtendimento = $atendimento->tiss_tipoatendimento;
                $codigoTabela = !empty($convenio->tiss_codtabpreco) ? $convenio->tiss_codtabpreco : '00';
                $observacao = !empty($atendimento->observacao) ? $atendimento->observacao : $atendimento->id;
                $registroANS = !empty($convenio->tiss_registroans) ? str_replace('-', '', $convenio->tiss_registroans) : '000000';
                $solicitanteNome = $solicitante->nome;
                $numeroConselhoProfissional = $solicitante->conselho;
                $uf = !empty($solicitante->estado) ? $solicitante->estado : '50';
                $numeroGuiaPrestador = $atendimento->id;
                $exacutanteContratadoNome = $configuracao->nome_login;
                $numeroGuiaOperadora = $atendimento->guia;
                $dataAtendimento = date_format($atendimento->data, 'Y-m-d');
                $codigoPrestadorNaOperadora = $solicitante->id;
                $cnes = $configuracao->tiss_cnes;
                $caraterAtendimento = 1;
                $indicacaoClinica = 'Z00';
                $numeroCarteira = !empty($atendimento->cliente->matricula) ? $atendimento->cliente->matricula : '0000000000000000';
                $atendimentoRn = 'N';
                $nomeBeneficiario = $atendimento->cliente->nome;
                $indicacaoAcidente = 2;
                $codcliconvenio = $convenio->tiss_codcliconvenio;

                foreach ($atendimento->atendimento_procedimentos as $ap) {
                    $numeroGuiaOperadora = !empty($ap->nr_guia) ? $ap->nr_guia : 'Não Informado';
                    $senha = !empty($ap->autorizacao_senha) ? $ap->autorizacao_senha : 'Não Informado';
                    $dataAutorizacao = !empty($ap->dt_autorizacao) ? date_format($ap->dt_autorizacao, 'Y-m-d') : date_format($atendimento->data, 'Y-m-d');
                }

                $bodyXml .=
                    "
                            <ans:guiaSP-SADT>
                                <ans:cabecalhoGuia>
                                    <ans:registroANS>" . $registroANS . "</ans:registroANS>
                                    <ans:numeroGuiaPrestador>" . $numeroGuiaPrestador . "</ans:numeroGuiaPrestador>
                                </ans:cabecalhoGuia>
                                <ans:dadosAutorizacao>
                                    <ans:numeroGuiaOperadora>" . $numeroGuiaOperadora . "</ans:numeroGuiaOperadora>
                                    <ans:dataAutorizacao>" . $dataAutorizacao . "</ans:dataAutorizacao>
                                    <ans:senha>" . $senha . "</ans:senha>
                                </ans:dadosAutorizacao>
                                <ans:dadosBeneficiario>
                                    <ans:numeroCarteira>" . $numeroCarteira . "</ans:numeroCarteira>
                                    <ans:atendimentoRN>" . $atendimentoRn . "</ans:atendimentoRN>
                                    <ans:nomeBeneficiario>" . $nomeBeneficiario . "</ans:nomeBeneficiario>
                                </ans:dadosBeneficiario>
                                <ans:dadosSolicitante>
                                    <ans:contratadoSolicitante>
                                        <ans:codigoPrestadorNaOperadora>" . $codcliconvenio . "</ans:codigoPrestadorNaOperadora>
                                        <ans:nomeContratado>" . $solicitanteNome . "</ans:nomeContratado>
                                    </ans:contratadoSolicitante>
                                    <ans:profissionalSolicitante>
                                        <ans:nomeProfissional>" . $solicitanteNome . "</ans:nomeProfissional>
                                        <ans:conselhoProfissional>" . $coselhoProfissional . "</ans:conselhoProfissional>
                                        <ans:numeroConselhoProfissional>" . $numeroConselhoProfissional . "</ans:numeroConselhoProfissional>
                                        <ans:UF>" . $uf . "</ans:UF>
                                        <ans:CBOS>" . $cbos . "</ans:CBOS>
                                    </ans:profissionalSolicitante>
                                </ans:dadosSolicitante>
                                <ans:dadosSolicitacao>
                                    <ans:dataSolicitacao>" . $dataAtendimento . "</ans:dataSolicitacao>
                                    <ans:caraterAtendimento>" . $caraterAtendimento . "</ans:caraterAtendimento>
                                    <ans:indicacaoClinica>" . $indicacaoClinica . "</ans:indicacaoClinica>
                                </ans:dadosSolicitacao>
                                <ans:dadosExecutante>
                                    <ans:contratadoExecutante>
                                        <ans:codigoPrestadorNaOperadora>" . $codcliconvenio . "</ans:codigoPrestadorNaOperadora>
                                        <ans:nomeContratado>" . $exacutanteContratadoNome . "</ans:nomeContratado>
                                    </ans:contratadoExecutante>
                                    <ans:CNES>" . $cnes . "</ans:CNES>
                                </ans:dadosExecutante>
                                <ans:dadosAtendimento>
                                    <ans:tipoAtendimento>" . $tipoAtendimento . "</ans:tipoAtendimento>
                                    <ans:indicacaoAcidente>" . $indicacaoAcidente . "</ans:indicacaoAcidente>
                                </ans:dadosAtendimento>
                                <ans:procedimentosExecutados>
                ";
                $total_procedimentos = 0;
                $total_materiais = 0;
                foreach ($atendimento->atendimento_procedimentos as $ap) {
                    $total_procedimentos += $ap->total;
                    $total_materiais += $ap->valor_material;

                    $codigoProcedimento = $ap->codigo;
                    $descricaoProcedimento = $ap->procedimento->nome;
                    $quantidadeExecutada = $ap->quantidade;
                    $viaAcesso = 1;
                    $tecnicaUtilizada = 1;
                    $reducaoAcrescimo = !empty($ap->percentual_cobranca) ? $ap->percentual_cobranca : 1;
                    $valorUnitario = number_format($ap->valor_fatura, 2, '.', '');
                    $valorTotal = number_format($ap->total, 2, '.', '');

                    $bodyXml .=
                        "
                                    <ans:procedimentoExecutado>
                                        <ans:dataExecucao>" . $dataAtendimento . "</ans:dataExecucao>
                                        <ans:procedimento>
                                            <ans:codigoTabela>" . $codigoTabela . "</ans:codigoTabela>
                                            <ans:codigoProcedimento>" . $codigoProcedimento . "</ans:codigoProcedimento>
                                            <ans:descricaoProcedimento>" . $descricaoProcedimento . "</ans:descricaoProcedimento>
                                        </ans:procedimento>
                                        <ans:quantidadeExecutada>" . $quantidadeExecutada . "</ans:quantidadeExecutada>
                                        <ans:viaAcesso>" . $viaAcesso . "</ans:viaAcesso>
                                        <ans:tecnicaUtilizada>" . $tecnicaUtilizada . "</ans:tecnicaUtilizada>
                                        <ans:reducaoAcrescimo>" . $reducaoAcrescimo . "</ans:reducaoAcrescimo>
                                        <ans:valorUnitario>" . $valorUnitario . "</ans:valorUnitario>
                                        <ans:valorTotal> " . $valorTotal . "</ans:valorTotal>
                                    </ans:procedimentoExecutado>
                    ";
                }
                $atendimentoAnterior = $atendimento->id;


                $totalGeral = number_format(($total_procedimentos + $total_materiais), 2, '.', '');

                $bodyXml .=
                    "               </ans:procedimentosExecutados>
                                <ans:observacao>" . $observacao . "</ans:observacao>
                                <ans:valorTotal>
                                    <ans:valorProcedimentos>" . number_format($total_procedimentos, 2, '.', '') . "</ans:valorProcedimentos>
                                    <ans:valorMateriais>" . number_format($total_materiais, 2, '.', '') . "</ans:valorMateriais>
                                    <ans:valorMedicamentos>0.00</ans:valorMedicamentos>
                                    <ans:valorTotalGeral>" . $totalGeral . "</ans:valorTotalGeral>
                                </ans:valorTotal>
                             </ans:guiaSP-SADT>
                ";
            }
        }
        $bodyXml .=
            "       
                    </ans:guiasTISS>
                    </ans:loteGuias>
                </ans:prestadorParaOperadora>
            ";
        return $bodyXml;

    }

    /**
     * Suporta 3.02.00, 3.03.01, 3.03.02, 3.03.03
     * @return string
     */
    public
    function getEpilogo()
    {
        return
            "    <ans:epilogo>
                    <ans:hash>" . '0' . "</ans:hash>
                </ans:epilogo>
            </ans:mensagemTISS>
            ";
    }

}

