<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\I18n\Time;

class DashboardComponent extends Component
{
    //Componentes externos para serem usados.
    public $components = ['Model'];

    //Função para trabalhar os dados do gráfico
    public function intermediateGraphicData($billsToPay, $billsToReceive, $invoicesToReceive, $date = null)
    {
        $bankMoves = $this->Model->getModelData('FinBancoMovimentos', 'getAllOpenedMovesBank', null, $date);
        $actualBalance = $this->workWithBankMovesData($bankMoves)[0];

        //Pegando o ultimo dia do mês para usar na iteração
        if (isset($date)){
            $lastDayMonth = intval(date('t', strtotime($date)));
        } else {
            $lastDayMonth = intval(date('t'));
        }
        $result = $this->workWithBillsData($billsToPay->toArray(), $billsToReceive->toArray(), $invoicesToReceive->toArray(), $actualBalance);
        
        return $result;
    }
    
    //Função para ordenação das contas à pagar
    public function workWithBillsData($billsToPay, $billsToReceive, $invoicesToReceive, $actualBalance)
    {
        //Pegando o ultimo dia do mês para usar na iteração
        $lastDayMonth = intval(date('t'));

        for ($i = 0; $i < $lastDayMonth; $i++) {
            $day = $i < 9 ? '0' . ($i + 1) : $i + 1;
            $day = (string) $day;

            $pays[] = floatVal(0.00);
            $receives[] = floatVal(0.00);
            $invoices[] = floatVal(0.00);
            $days[] = $day;

            $diaTabela = intval($day);
            foreach ($billsToPay as $key => $bill) {
                $diaBanco = intval($billsToPay[$key]->dia);

                if ($diaBanco === $diaTabela) {
                    $pays[$i] = !empty($billsToPay[$key]->contasPagar) ? floatVal($billsToPay[$key]->contasPagar) : $pays[$i];
                }
            }

            foreach ($invoicesToReceive as $key => $bill) {
                $diaBanco = intval($invoicesToReceive[$key]->dia);

                if ($diaBanco === $diaTabela) {
                    $invoices[$i] = !empty($invoicesToReceive[$key]->contasReceberConvenio) ? floatVal($invoicesToReceive[$key]->contasReceberConvenio) : $invoices[$i];
                }
            }

            foreach ($billsToReceive as $key => $move) {
                $diaBanco = intval($billsToReceive[$key]->dia);
                if ($diaBanco === $diaTabela) {
                    $receives[$i] = !empty($billsToReceive[$key]->contasReceber) ? floatVal($billsToReceive[$key]->contasReceber) : $pays[$i];
                }
            }
            
            $projectedBalance[] = $i === 0 ? $actualBalance + (($invoices[$i] + $receives[$i]) - $pays[$i]) 
            : $projectedBalance[$i - 1] + (($invoices[$i] + $receives[$i]) - $pays[$i]);
        }

        return [$pays, $receives, $invoices, $days, $projectedBalance];
    }

    //Função para iteração de soma de saldos.
    private function workWithBankMovesData($bankMoves)
    {
        $resultBankMoves = [];
        $resultBankMoves[0] = 0;
        $resultBankMoves[1] = 0;

        foreach ($bankMoves as $move) {
            $resultBankMoves[0] += floatval($move->soma_movimentos_saldos);
            $resultBankMoves[1] += $move->saldoInicial;
        }
        return $resultBankMoves;
    }
}
