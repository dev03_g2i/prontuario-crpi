<?php
/**
 * Created by PhpStorm.
 * User: Iohan
 * Date: 04/07/2017
 * Time: 11:20
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use DateTime;

class FaturamentoComponent extends Component
{

    public $components = ['Data'];

    public function ap()
    {
        $atendimento_procedimento = TableRegistry::get('AtendimentoProcedimentos');
        return $atendimento_procedimento;
    }

    public function atendimentoProcedimentos($situacao_fatura = null, $data_inicio = null, $data_fim = null,
                                             $unidade = null, $origem = null, $convenios = null, $grupos = null,
                                             $procedimentos = null, $medicos = null, $controle = null, $ordenar_por = null)
    {
        $atendimento_procedimento = TableRegistry::get('AtendimentoProcedimentos');


        $query = $atendimento_procedimento->find('all', [
            'contain' => [
                'Atendimentos' => ['Clientes', 'Convenios', 'InternacaoCaraterAtendimento'], 'Procedimentos', 'MedicoResponsaveis', 'AtendimentoProcedimentoEquipes'
            ],
        ])->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1]);

        if (!empty($situacao_fatura)) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_fatura_id' => $situacao_fatura]);
        }

        if (!empty($data_inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $data_inicio);
            $query->andWhere(['Atendimentos.data >= ' => $now->format('Y-m-d') . " 00:00:00"]);
        }
        if (!empty($data_fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $data_fim);
            $query->andWhere(['Atendimentos.data <= ' => $now->format('Y-m-d') . " 00:00:00"]);
        }

        if (!empty($unidade)) {
            $query->andWhere(['Atendimentos.unidade_id' => $unidade]);
        }

        if (!empty($origem)) {
            $query->andWhere(['Atendimentos.origen_id' => $origem]);
        }

        if (!empty($convenios)) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $convenios]);
        }

        if (!empty($grupos)) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $grupos]);
        }

        if (!empty($procedimentos)) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $procedimentos]);
        }

        if (!empty($medicos)) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $medicos]);
        }

        if (!empty($controle)) {
            $controle = '%' . $controle . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }

        if(!empty($ordenar_por)){
            $query->orderAsc($ordenar_por);
        }

        $query->order(['Procedimentos.grupo_id' => 'ASC', 'AtendimentoProcedimentos.ordenacao' => 'ASC']);

        return $query;
    }

    public function filterAtendProc($requestQuery)
    {
        $this->request->query = $requestQuery;

        $atendimento_procedimento = TableRegistry::get('AtendimentoProcedimentos');
        $query = $atendimento_procedimento->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos', 'MedicoResponsaveis', 'Procedimentos', 'SituacaoFaturas', 'SituacaoRecebimentos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id <>' => 'NULL']);//Faturado


        if (!empty($this->request->query('data_inicio'))) {
            $data_inicio = $this->Data->DataSQL($this->request->query['data_inicio']);
            $query->andWhere(['Atendimentos.data >= ' => $data_inicio]);
        }
        if (!empty($this->request->query('data_fim'))) {
            $data_fim = $this->Data->DataSQL($this->request->query['data_fim']);
            $query->andWhere(['Atendimentos.data <= ' => $data_fim]);
        }
        if (!empty($this->request->query('unidade_id'))) {
            $query->andWhere(['Atendimentos.unidade_id' => $this->request->query('unidade_id')]);
        }

        if (!empty($this->request->query('origen_id'))) {
            $query->andWhere(['Atendimentos.origen_id' => $this->request->query('origen_id')]);
        }

        if (!empty($this->request->query('convenios'))) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $this->request->query('convenios')]);
        }

        if (!empty($this->request->query('grupos'))) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $this->request->query('grupos')]);
        }
        if (!empty($this->request->query('procedimentos'))) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $this->request->query('procedimentos')]);
        }

        if (!empty($this->request->query('medicos'))) {
            $query->andWhere(['AtendimentoProcedimentos.medico_id IN ' => $this->request->query('medicos')]);
        }

        if (!empty($this->request->query('controle'))) {
            $controle = '%' . $this->request->query('controle') . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $controle]);
        }
        if (!empty($this->request->query('situacao_recebimento_id'))) {
            $cobranca = $this->request->query('cobranca');
            $query->andWhere(['AtendimentoProcedimentos.' . $cobranca => $this->request->query('situacao_recebimento_id')]);
        }

        $query->order(['Procedimentos.grupo_id' => 'ASC', 'AtendimentoProcedimentos.ordenacao' => 'ASC']);

        return $query;
    }

    /**
     * Filtro Faturamentos/equipes
     *
     * @param array $data
     * @return void
     */
    public function filterAtendProcToEquipes($data = array())
    {
        $ap_equipes = TableRegistry::get('AtendimentoProcedimentoEquipes');
        $query = $ap_equipes->find('all', [
            'contain' => [
                'AtendimentoProcedimentos' => ['Atendimentos' => ['Clientes', 'Convenios', 'Users', 'InternacaoCaraterAtendimento'], 'Procedimentos' => ['GrupoProcedimentos']], 'MedicoResponsaveis', 'TipoEquipes', 'TipoEquipegrau'
            ],
        ])
        ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1]);

        if (!empty($data['situacao_fatura_id'])) {
            $query->andWhere(['AtendimentoProcedimentos.situacao_fatura_id' => $data['situacao_fatura_id']]);
        }

        if (!empty($data['data_inicio'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $data['data_inicio']);
            $query->andWhere(['Atendimentos.data >= ' => $now->format('Y-m-d') . " 00:00:00"]);
        }
        if (!empty($data['data_fim'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $data['data_fim']);
            $query->andWhere(['Atendimentos.data <= ' => $now->format('Y-m-d') . " 00:00:00"]);
        }

        if (!empty($data['unidade_id'])) {
            $query->andWhere(['Atendimentos.unidade_id' => $data['unidade_id']]);
        }

        if (!empty($data['origen_id'])) {
            $query->andWhere(['Atendimentos.origen_id' => $data['origen_id']]);
        }

        if (!empty($data['convenios'])) {
            $query->andWhere(['Atendimentos.convenio_id IN ' => $data['convenios']]);
        }

        if (!empty($data['grupos'])) {
            $query->andWhere(['Procedimentos.grupo_id IN ' => $data['grupos']]);
        }

        if (!empty($data['procedimentos'])) {
            $query->andWhere(['AtendimentoProcedimentos.procedimento_id IN ' => $data['procedimentos']]);
        }

        if (!empty($data['medicos'])) {
            $medicos = $data['medicos'];
            $query->andWhere(['AtendimentoProcedimentoEquipes.profissional_id IN ' => $medicos]);
        }

        if (!empty($data['controle'])) {
            $controle = '%' . $controle . '%';
            $query->andWhere(['AtendimentoProcedimentos.controle LIKE ' => $data['controle']]);
        }



        if(!empty($data['ordenar_por'])){
//            $ordenar_por = $data['ordenar_por'];
//            if($ordenar_por == 1){// Atendimento
//                $query->order(['Atendimentos.id' => 'ASC']);
//            }elseif($ordenar_por == 2){// Data
//                $query->order(['Atendimentos.data' => 'ASC', 'Atendimentos.id' => 'ASC']);
//            }elseif($ordenar_por == 3){// Paciente
//                $query->order(['Clientes.nome' => 'ASC', 'Atendimentos.id' => 'ASC']);
//            }

            $query->order($data['ordenar_por'] . ' ASC');

        }

        if(!empty($data['tipo_equipe'])){
            $query->andWhere(['AtendimentoProcedimentoEquipes.tipo_equipe_id' => $data['tipo_equipe']]);
        }

        return $query;
    }

    public function getVersoesXML()
    {
        return [
            3 => 'SDAT - 3.02.00',
            1 => 'SDAT - 3.03.01',
            2 => 'SDAT - 3.03.02',
            4 => 'SDAT - 3.03.03'
        ];
    }

    public function getValorNaoFaturado($data = null) 
    {   
        $thisDate = $data->format('Y-m-d'); 
        $oneYearAgoDate = $data->subMonth(12)->format('Y-m-d');
        
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendimentoProcedimentos = $atendimentoProcedimentos->find('all')
        ->select(['total' => 'sum(AtendimentoProcedimentos.total + AtendimentoProcedimentos.valor_matmed)'])
        ->contain(['Atendimentos' => ['Convenios']])
        ->where(['Atendimentos.data >=' => $oneYearAgoDate,
                'Atendimentos.data <=' => $thisDate])
        ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1,'Atendimentos.situacao_id' => 1])
        ->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id is null'])
        ->andWhere(['Convenios.tipo_id' => 2])->first();

        if ($atendimentoProcedimentos->total == null) 
            $atendimentoProcedimentos->total = 0;

        return $atendimentoProcedimentos->total;
    }

    public function getQtNaoFaturado($data = null) 
    {   
        $thisDate = $data->format('Y-m-d'); 
        $oneYearAgoDate = $data->subMonth(12)->format('Y-m-d');

        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendimentoProcedimentos = $atendimentoProcedimentos->find('all')
        ->select(['total' => 'count(*)'])
        ->contain(['Atendimentos' => ['Convenios']])
        ->where(['Atendimentos.data >=' => $oneYearAgoDate,
                'Atendimentos.data <=' => $thisDate])
        ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1,'Atendimentos.situacao_id' => 1])
        ->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id is null'])
        ->andWhere(['Convenios.tipo_id' => 2])->first();

        return $atendimentoProcedimentos->total;
    }

    public function getPorcentualNaoFaturado($data = null) 
    {   
        $thisDate = $data->format('Y-m-d'); 
        $oneYearAgoDate = $data->subMonth(12)->format('Y-m-d');

        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendimentoProcedimentos = $atendimentoProcedimentos->find('all')
        ->select(['total' => 'count(AtendimentoProcedimentos.id)'])
        ->contain(['Atendimentos' => ['Convenios']])
        ->where(['Atendimentos.data >=' => $oneYearAgoDate,
                'Atendimentos.data <=' => $thisDate])
        ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1,'Atendimentos.situacao_id' => 1])
        ->andWhere(['AtendimentoProcedimentos.fatura_encerramento_id is null'])
        ->andWhere(['Convenios.tipo_id' => 2])->first();

        $atendimentoProcedimentos2 = TableRegistry::get('AtendimentoProcedimentos');
        $atendimentoProcedimentos2 = $atendimentoProcedimentos2->find('all')
        ->select(['qtTotal' => 'count(AtendimentoProcedimentos.id)'])
        ->contain(['Atendimentos' => ['Convenios']])
        ->where(['Atendimentos.data >=' => $oneYearAgoDate,
                'Atendimentos.data <=' => $thisDate])
        ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1,'Atendimentos.situacao_id' => 1])
        ->andWhere(['Convenios.tipo_id' => 2])->first();

        return ($atendimentoProcedimentos->total)/$atendimentoProcedimentos2->qtTotal;
    }

    public function calculaTotalProfissional($ap_id)
    {
        $ap_equipes = TableRegistry::get('AtendimentoProcedimentoEquipes');
        $query = $ap_equipes->get($ap_id,[
            'contain' => ['AtendimentoProcedimentos']
        ]);
        $total = 0;
        if(!empty($query->atendimento_procedimento)){
            $porcentagem = $query->porcentagem / 100;
            $total = ($query->atendimento_procedimento->total + $query->atendimento_procedimento->valor_caixa) * $porcentagem;
        }
        return $total;
    }

    /**
     * Função que calcula o total do profissional a partir de um ID do atendimento procedimento
     * @param $atendimentoProcedimentoId
     * @return float|int valor total da equipe
     */
    public function calculaTotalProfissionalPorAtendimentoProcedimento($atendimentoProcedimentoId)
    {
        $atendimentoProcedimentoEquipeTable = TableRegistry::get('AtendimentoProcedimentoEquipes');
        $query = $atendimentoProcedimentoEquipeTable->find('all')
            ->contain(['AtendimentoProcedimentos'])
            ->where(['AtendimentoProcedimentoEquipes.atendimento_procedimento_id' => $atendimentoProcedimentoId]);

        $total = 0.00;
        foreach ($query as $q){
            $porcentagem = $q->porcentagem / 100;
            $total += ($q->atendimento_procedimento->total + $q->atendimento_procedimento->valor_caixa) * $porcentagem;
        }

        return $total;
    }
}