<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use DateTime;

class MovimentosComponent extends Component
{
    public $components = ['Data', 'Date'];

    public function filterMovesIndex($query)
    {
        $FinMovimentos = TableRegistry::get('FinMovimentos');

        $order = !empty($query['direction']) ? $query['direction'] : null;
        $sort = !empty($query['sort']) ? $query['sort'] : null;
        $situacao = !empty($query['situacao']) ? $query['situacao'] : null;
        $banco = !empty($query['banco']) ? $query['banco'] : null;
        $movimento = !empty($query['movimento']) ? $query['movimento'] : null;
        $inicio = !empty($query['inicio']) ? $this->Date->formatDateForSql($query['inicio']) : null;
        $fim = !empty($query['fim']) ? $this->Date->formatDateForSql($query['fim']) : null;

        if (isset($movimento) && isset($situacao) && isset($banco)) {
            $finMovimentos = $FinMovimentos->find()->contain(['FinPlanoContas', 'FinFornecedores', 'FinBancos', 'FinContasPagar', 'FinContabilidades', 'Clientes', 'FinBancoMovimentos']);
        }

        if (isset($inicio) && isset($fim)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.data >=' => $inicio, 'FinMovimentos.data <=' => $fim]);
        } else if (isset($inicio)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.data >=' => $inicio]);
        } else if (isset($fim)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.data <=' => $fim]);
        }

        if (isset($banco))
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.fin_banco_id' => $banco]);

        if (isset($movimento)) {
            //Pegando o saldo inicial da movimentação
            $bancoMovimento = $FinMovimentos->FinBancoMovimentos->find()
                ->select(['FinBancoMovimentos.saldoInicial'])
                ->where(['FinBancoMovimentos.id' => $movimento])
                ->first();

            $saldoInicial = isset($bancoMovimento) ? $bancoMovimento->saldoInicial : 0;

            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.fin_banco_movimento_id' => $movimento]);
        }

        if (isset($finMovimentos)) {
            //Clonando o objeto para não perder a referência original
            $movimentosParaSaldos = clone($finMovimentos);
            $totalDebito = $movimentosParaSaldos->select(['total_debito' => 'SUM(FinMovimentos.debito)'])->first();
            $totalDebito = $totalDebito->total_debito;

            $totalCredito = $movimentosParaSaldos->select(['total_credito' => 'SUM(FinMovimentos.credito)'])->first();
            $totalCredito = $totalCredito->total_credito;

            $saldoFinal = $movimentosParaSaldos->select(['saldo_final' => 'SUM(FinMovimentos.credito) - SUM(FinMovimentos.debito)'])->first();
            $saldoFinal = $saldoInicial + $saldoFinal->saldo_final;

            $finMovimentos = $finMovimentos->order(['FinMovimentos.data' => 'asc', 'FinMovimentos.fin_banco_id' => 'asc', 'FinMovimentos.fin_contabilidade_id']);
            $fimMovimentos = $this->getFinalBalance($finMovimentos->toArray(), $saldoInicial);

            return [$finMovimentos, $saldoInicial, $saldoFinal, $totalCredito, $totalDebito];
        }

        return [null, null, null, null, null];
    }

    public function filterMovesGeneral($query, $finMovimentos, $finMovimentosTransfs)
    {
        $inicio = !empty($query['inicio']) ? $this->Date->formatDateForSql($query['inicio']) : null;
        $fim = !empty($query['fim']) ? $this->Date->formatDateForSql($query['fim']) : null;
        $banco = !empty($query['banco']) ? $query['banco'] : null;
        $contabilidade = !empty($query['contabilidade']) ? $query['contabilidade'] : null;
        $planoconta = !empty($query['planoconta']) ? $query['planoconta'] : null;
        $fornecedor = !empty($query['fornecedor']) ? $query['fornecedor'] : null;
        $cliente = !empty($query['cliente']) ? $query['cliente'] : null;
        $documento = !empty($query['documento']) ? $query['documento'] : null;

        $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.data >=' => $inicio, 'FinMovimentos.data <=' => $fim]);

        if (isset($planoconta)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.fin_plano_conta_id' => $planoconta]);
            $finMovimentosTransfs = $finMovimentosTransfs->andWhere(['FinMovimentos.fin_plano_conta_id' => $planoconta]);
        }

        if (isset($banco)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.fin_banco_id' => $banco]);
            $finMovimentosTransfs = $finMovimentosTransfs->andWhere(['FinMovimentos.fin_banco_id' => $banco]);
            
            $lastMoviment = new Time($inicio);
            $lastMoviment = $lastMoviment->format('m/Y');
            //Pegando o saldo inicial da movimentação
            $FinMovimentos = TableRegistry::get('FinMovimentos');

            $bancoMovimento = $FinMovimentos->FinBancoMovimentos->find()
                ->contain(['FinBancos'])
                ->select(['FinBancoMovimentos.saldoInicial'])
                ->where(['FinBancoMovimentos.fin_banco_id' => $banco, 'FinBancoMovimentos.data_movimento' => $lastMoviment])
                ->first();

            $saldoInicial = isset($bancoMovimento) ? $bancoMovimento->saldoInicial : 0;
        }

        if (isset($cliente)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.cliente_id' => $cliente]);
            $finMovimentosTransfs = $finMovimentosTransfs->andWhere(['FinMovimentos.cliente_id' => $cliente]);
        }

        if (isset($fornecedore)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.fin_fornecedor_id' => $fornecedor]);
            $finMovimentosTransfs = $finMovimentosTransfs->andWhere(['FinMovimentos.fin_fornecedor_id' => $fornecedor]);
        }

        if (isset($contabilidade)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.fin_contabilidade_id' => $contabilidade]);
            $finMovimentosTransfs = $finMovimentosTransfs->andWhere(['FinMovimentos.fin_contabilidade_id' => $contabilidade]);
        }

        if (isset($documento)) {
            $finMovimentos = $finMovimentos->andWhere(['FinMovimentos.documento LIKE' => "%$documento%"]);
            $finMovimentosTransfs = $finMovimentosTransfs->andWhere(['FinMovimentos.documento LIKE' => "%$documento%"]);
        }

        $finMovimentos = $finMovimentos->order(['FinMovimentos.data' => 'asc', 'FinMovimentos.fin_banco_id' => 'asc', 'FinMovimentos.fin_contabilidade_id']);

        $saldoInicial = isset($saldoInicial) ? $saldoInicial : 0;

        return [$finMovimentos, $saldoInicial, $finMovimentosTransfs];
    }

    private function workWithFinMovesData($moves)
    {
        $lastBalance = $this->FinMovimentos->FinBancoMovimentos->getLastMonthFinalBalance();

        foreach ($moves as $key => $move) {
            if ($key === 0)
                $move->saldo = ($move->credito - $move->debito) + $lastBalance;
            else
                $move->saldo = ($move->credito - $move->debito) + $moves[$key - 1]->saldo;
        }
        return $moves;
    }

    private function getBalances($date = null)
    {
        $now = new Time();
        $initialDate = $now->format('m/Y');
    }

    private function getFinalBalance($finMovimentos, $saldoInicial)
    {
        foreach ($finMovimentos as $key => $finMovimento) {
            $finMovimento->saldo = $finMovimento->credito - $finMovimento->debito;
            if ($key === 0)
                $finMovimento->saldo = $saldoInicial + $finMovimento->saldo;
            else
                $finMovimento->saldo = $saldoAnterior + $finMovimento->saldo;

            $saldoAnterior = $finMovimento->saldo;
        }

        return $finMovimentos;
    }

    public function filtroMovimentosRelatorios($query, $request)
    {
        $query->andWhere(['FinMovimentos.situacao' => 1]);

        if (!empty($request['situacao'])) {//1 = aberta 2 = encerrada
            if ($request['situacao'] == 1) {
                $query->andWhere(['FinBancoMovimentos.status' => 'Aberto']);
            } else if ($request['situacao'] == 2) {
                $query->andWhere(['FinBancoMovimentos.status' => 'Encerrado']);
            }
        }

        if (!empty($request['movimento'])) {
            $query->andWhere(['FinMovimentos.fin_banco_movimento_id' => $request['movimento']]);
        }

        if (!empty($request['banco'])) {
            $query->andWhere(['FinMovimentos.fin_banco_id' => $request['banco']]);
        }

        if (!empty($request['inicio'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $request['inicio']);
            $query->andWhere(['FinMovimentos.data >=' => $now->format('Y-m-d')]);
        }

        if (!empty($request['fim'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $request['fim']);
            $query->andWhere(['FinMovimentos.data <=' => $now->format('Y-m-d')]);
        }

        //$query->order(['FinMovimentos.data DESC']);

        $query->order(['FinMovimentos.data' => 'asc', 'FinMovimentos.fin_banco_id' => 'asc', 'FinMovimentos.fin_contabilidade_id']);

        return $query;
    }

    public function calculaSaldoMovimentos($movimentos, $saldoInicial)
    {
        $movimentos = $movimentos->toArray();

        for ($i = 0; $i < sizeof($movimentos); $i++) {
            if ($i == 0) {
                $movimentos[$i]->saldo = $saldoInicial + $movimentos[$i]->credito - $movimentos[$i]->debito;
            } else {
                $movimentos[$i]->saldo = $movimentos[$i - 1]->saldo + $movimentos[$i]->credito - $movimentos[$i]->debito;
            }
        }

        return $movimentos;
    }
}