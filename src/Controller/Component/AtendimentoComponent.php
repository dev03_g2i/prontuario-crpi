<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Filesystem\File;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use DateTime;

class AtendimentoComponent extends Component
{
    public $components = ['Data', 'Configuracao'];

    public function gerarXML($atendProcId, $method)
    {
        if (!empty($atendProcId)) {
            $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
            $atendProc = $atendimentoProcedimentos->get($atendProcId, [
                'contain' => ['Atendimentos' => ['Clientes', 'Convenios'], 'Procedimentos', 'Users', 'MedicoResponsaveis']
            ]);
            //Configuração da tag site_identifier
            if($this->Configuracao->getConfigMvSiteIdentifier() == 1){
                $siteIdenfifier = $atendProc->atendimento->id;
            } else {
                $siteIdenfifier = $this->Configuracao->getValorMvSiteIdentifier();
            }
            //Configuração da tag referring_physician_details->last_name
            if($this->Configuracao->getConfigMvSolicitante() == 1){
                $solicitante_id = $atendProc->solicitante_id;
            } else {
                $solicitante_id = $atendProc->atendimento->solicitante_id;
            }

            $modeloXML = "<report>
    <message_datetime>" . $this->Data->dateTimestamp($atendProc->atendimento->data) . "</message_datetime>
    <message_id>" . $atendProc->id . "</message_id>
    <encoding>UTF-8</encoding>
    <version>11.3.2.0</version>
    <site_identifier>" . $siteIdenfifier . "</site_identifier>
    <order_details>
        <accession_number>" . $atendProc->id . "</accession_number>
        <study_date>" . $this->Data->dateTimestamp($atendProc->atendimento->created) . "</study_date>
        <study_description>" . $atendProc->procedimento->nome . "</study_description>
        <requested_preocedure_id>" . $atendProc->procedimento_id . "</requested_preocedure_id>
        <requested_preocedure_name>" . $atendProc->procedimento->nome . "</requested_preocedure_name>
    </order_details>
    <patient_details>
        <patient_id>" . $atendProc->atendimento->cliente_id . "</patient_id>
        <issuer_of_patient_id>" . $atendProc->atendimento->convenio->nome . "</issuer_of_patient_id>
        <first_name/>
        <middle_name/>
        <last_name>" . $atendProc->atendimento->cliente->nome . "</last_name>
        <birth_date>" . $this->Data->dateTimestamp($atendProc->atendimento->cliente->nascimento) . "</birth_date>
        <sex>" . $atendProc->atendimento->cliente->sexo . "</sex>
    </patient_details>
    <referring_physician_details>
        <title/>
        <first_name/>
        <middle_name/>
        <last_name>" . $this->getSolicitante($solicitante_id) . "</last_name>
        <code/>
    </referring_physician_details>
    <reading_physician_details>
        <first_name/>
        <middle_name/>
        <last_name>" . $atendProc->medico_responsavei->nome . "</last_name>
        <title/>
        <code>" . $atendProc->medico_responsavei->id . "</code>
    </reading_physician_details>
    <report_details>
        <document_name>" . $atendProc->id . ".rtf</document_name>
        <document_type>rtf</document_type>
        <is_full_report>No</is_full_report>
        <internal_report_version/>
        <method>" . $method . "</method>
        <report_sign_datetime>" . $this->Data->dateTimestamp($atendProc->created) . "</report_sign_datetime>
    </report_details>
</report>";
            $caminho = WWW_ROOT . 'files/xml/atendimento/' . $method[0] . '-' . $atendProc->id . '.xml';
            $file = new File($caminho);
            $file->create();
            $file->write($modeloXML);
            $file->close();
        }
    }

    public function procedimentos($atendimento_id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendProc = $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos'])
            ->where(['AtendimentoProcedimentos.atendimento_id' => $atendimento_id])
            ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);
        $procedimentos = [];
        foreach ($atendProc as $ap) {
            $procedimentos[$ap->id] = $ap->id.' - '.$ap->procedimento->nome;
        }
        return $procedimentos;
    }

    public function getSolicitante($id)
    {
        $solicitantes = TableRegistry::get('Solicitantes');
        $soli = $solicitantes->get($id);
        if (!empty($soli->nome)) {
            return $soli->nome;
        } else {
            return null;
        }
    }

    public function calculaPrevEntrega($proc_id)
    {
        $procedimento = TableRegistry::get('Procedimentos');
        $proc = $procedimento->get($proc_id);
        $now = new Time();
        $now->addDays($proc->dias_entrega);
        // Se for domingo adicionar mais um dia.
        if ($now->format('l') == 'Sunday') {
            $now->addDays(1);
        }
        return $now->format('Y-m-d H:i');

    }

    public function etiqueta($ids)
    {
        $atendimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendimentos = $atendimentos->find('all')
            ->contain(['Atendimentos' => ['Clientes', 'Convenios'],
			'MedicoResponsaveis',
			'Procedimentos'])
            ->where(['AtendimentoProcedimentos.id IN' => $ids]);

        $json['cliente'][] = null;
        foreach ($atendimentos as $atendimento_procedimento) {
            $idade = new Time ($atendimento_procedimento->atendimento->cliente->nascimento);
            $diff = $idade->diff(new DateTime());
            $anos = $diff->format("%y") . ' anos ' . $diff->format("%m") . ' meses';

            $json['cliente'][] = [
                'cliente' => $atendimento_procedimento->atendimento->cliente->nome,
                'cpf' => $atendimento_procedimento->atendimento->cliente->cpf,
				'cliente_nascimento' => !empty($atendimento_procedimento->atendimento->cliente->nascimento) ? date_format($atendimento_procedimento->atendimento->cliente->nascimento,'d/m/Y') : '',
                'convenio' => $atendimento_procedimento->atendimento->convenio->nome,
                'created' => $atendimento_procedimento->atendimento->created,
				'data' => date_format($atendimento_procedimento->atendimento->data, "d/m/Y"),
				'medico' => $atendimento_procedimento->medico_responsavei->nome_laudo,
				'crm' => $atendimento_procedimento->medico_responsavei->conselho_laudo,
				'procedimento' => $atendimento_procedimento->procedimento->nome,
            ];
        }
        return $json;
    }

    /** Ordena os procedimentos de acordo com a regra e ou tipo_ordenacao(inserção, maior_valor)
     * @param array $procedimentos
     * @param $regra
     * @return array
     */
    public function sortOrdenacao($procedimentos = array())
    {
        $convenio_regras = TableRegistry::get('ConvenioRegracalculo');
        $regras = $convenio_regras->find('all')->where(['ConvenioRegracalculo.situacao_id' => 1]);
        if(!empty($procedimentos)){

            foreach ($regras as $regra){
                if($regra->tipo_ordenacao == 2){
                    uasort($procedimentos, function ($a, $b) {
                        return $a['valor_fatura'] < $b['valor_fatura'];
                    });
                }
                $count = 0;
                foreach ($procedimentos as $k => $v){
                    if($v['regra'] == $regra->id){
                        $procedimentos[$k]['ordenacao'] = $count +=1;
                    }
                }
            }
        }
        return $procedimentos;
    }

    /**
     * Ordena os procedimentos com regras de maior valor.
     *
     * @param Integer $atendimento_id
     * @return void
     */
    public function sortProcedimentosPerTotal($atendimento_id)
    {
        $convenio_regras = TableRegistry::get('ConvenioRegracalculo');
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $grupo_procedimentos = TableRegistry::get('GrupoProcedimentos');
        $regras = $convenio_regras->find('all')->where(['ConvenioRegracalculo.situacao_id' => 1, 'ConvenioRegracalculo.tipo_ordenacao' => 2]);
        $grupos = $grupo_procedimentos->find('all')
            ->where(['GrupoProcedimentos.situacao_id' => 1]);

        $ids = [];
        foreach($regras as $r)
        {
            $ids[] = $r->id;
        }

        if(!empty($ids))
        {
            $atendimento_procedimentos = $atendimentoProcedimentos->find('all')
                ->contain(['Procedimentos'])
                ->where(['AtendimentoProcedimentos.atendimento_id' => $atendimento_id])
                ->andWhere(['AtendimentoProcedimentos.regra IN ' => $ids])->toArray();
        
            if(!empty($atendimento_procedimentos)){
                uasort($atendimento_procedimentos, function ($a, $b) {
                    return $a['total'] < $b['total'];
                });
                foreach($grupos as $grupo){
                    $count = 0;
                    foreach ($atendimento_procedimentos as $ap){
                        if($grupo->id == $ap->procedimento->grupo_id){
                            $ap['ordenacao'] = $count+=1;
                            $atendimentoProcedimentos->save($ap);
                        }
                    }
                }
            }
        }
    }

    /**
     * Substitui as variaveis dos modelos pelos dados corretos
     */
    public function gerarModelo($modelo_id, $atendimento_id, $data_emissao)
    {
        $modelo_documento = TableRegistry::get('AtendimentoModeloDocumentos');
        $atendimentos = TableRegistry::get('Atendimentos');
        $dataEmissao = new Time($data_emissao);

        $modelo = $modelo_documento->get($modelo_id);
        $atendimento = $atendimentos->get($atendimento_id, [
            'contain' => ['Clientes', 'Convenios', 'TipoAtendimentos', 'Solicitantes', 'Users',
            'InternacaoAcomodacao', 'InternacaoMotivoAlta', 
            'ContasReceber' => ['TipoDocumento'],
            'AtendimentoProcedimentos' => ['Procedimentos']]
        ]);

        $dados = [
            /* Dados Paciente */
            '{idade}' => $atendimento->cliente->idade,
            '{cpf}' => $atendimento->cliente->cpf,
            '{rua}' => $atendimento->cliente->endereco,
            '{numero}' =>  $atendimento->cliente->numero,
            '{cidade}' => $atendimento->cliente->cidade,
            '{estado}' => $atendimento->cliente->estado,
            '{bairro}' => $atendimento->cliente->bairro,
            '{complemento}' => $atendimento->cliente->complemento,
            '{nome_paciente}' => $atendimento->cliente->nome,
            '{num_prontuario}' => $atendimento->cliente->id,
            /* Dados Atendimento */
            '{num_atendimento}' => $atendimento->id,
            '{dt_atendimento}' => $atendimento->data,
            '{hora_atendimento}' => $atendimento->hora_formatada,
            '{convenio_nome}' => $atendimento->convenio->nome,
            '{tipo_atendimento}' => $atendimento->tipo_atendimento->nome,
            '{nome_solicitante}' => $atendimento->solicitante->nome,
            '{usuario_responsavel}' => $atendimento->user->nome,
            '{total_pagar}' => number_format($atendimento->total_geral,2,',','.'),
            '{dia_atendimento}' => $atendimento->dia_atendimento,
            '{nome_mes_atendimento}' =>$this->Data->NomeMes($atendimento->data),
            '{num_mes_atendimento}' => $atendimento->num_mes_atendimento,
            '{ano_atendimento}' => $atendimento->ano_atendimento,
            '{data_doc}' => $dataEmissao->format('d/m/Y'),
            '{nome_mes_doc}' => $this->Data->NomeMes($data_emissao),
            '{num_mes_doc}' => $dataEmissao->format('m'),
            '{ano_doc}' => $dataEmissao->format('Y'),
            /* Dados Internação */
            '{internacao_acomodacao}' => ($atendimento->has('internacao_acomodacao')? $atendimento->internacao_acomodacao->descricao : null) ,
            '{internacao_previa}' => $atendimento->internacao_previa,
            '{internacao_data}' => $atendimento->internacao_previa_data,
            '{internacao_dthora}' => $atendimento->internacao_data_hora_entrada,
            '{internacao_alta}' => ($atendimento->has('internacao_motivo_alta')? $atendimento->internacao_motivo_alta->descricao : null) ,
            '{internacao_alta_dthora}' => $atendimento->internacao_data_hora_saida,
            /* Dados Procedimentos */
            '{tabela_cod_desc}' => $this->tableProcedimentos('{tabela_cod_desc}', $atendimento),
            '{tabela_cod_desc_valor_fat}' => $this->tableProcedimentos('{tabela_cod_desc_valor_fat}', $atendimento),
            '{tabela_todos_valor_caixa}' => $this->tableProcedimentos('{tabela_todos_valor_caixa}', $atendimento),
            '{tabela_todos_valor_caixa_matmed}' => $this->tableProcedimentos('{tabela_todos_valor_caixa_matmed}', $atendimento),
            /* Dados Financeiro */
            '{tabela_financeiro}' => $this->tableFinanceiro($atendimento->contas_receber), 
        ];
        //return $atendimento;
        return $this->replace($dados, $modelo->descricao);
    }

    public function replace($colecao, $text) {
        $result = $text;
        foreach ($colecao as $item => $value) {
            $result= str_replace($item,$value,$result);
        }

        return $result;
    }

    /**
     * Gera uma tabela de acordo com a variavel passada. ex: {tabela_cod_desc}
     *
     * @param String $variavel
     * @param Object $atendimento
     * @return void
     */
    public function tableProcedimentos($variavel, $atendimento)
    {
        $th = "";
        $td = "";
        switch($variavel)
        {
            case '{tabela_cod_desc}':
                    $th .= "<th>Cód</th>";
                    $th .= "<th>Descrição</th>";
                foreach ($atendimento->atendimento_procedimentos as $p) {
                    $td .= "<tr>";
                    $td .= "<td>".$p->procedimento->id."</td>";
                    $td .= "<td>".$p->procedimento->nome."</td>";    
                    $td .= "</tr>";
                }
                break;
            case '{tabela_cod_desc_valor_fat}':
                    $th .= "<th>Cód</th>";
                    $th .= "<th>Descrição</th>";
                    $th .= "<th>Valor fatura</th>";
                foreach ($atendimento->atendimento_procedimentos as $p) {
                    $td .= "<tr>";
                    $td .= "<td>".$p->procedimento->id."</td>";
                    $td .= "<td>".$p->procedimento->nome."</td>";
                    $td .= "<td>".number_format($p->valor_fatura,2,',','.')."</td>";    
                    $td .= "</tr>";
                }
                break;
            case '{tabela_todos_valor_caixa}':
                    $th .= "<th>Cód</th>";
                    $th .= "<th>Descrição</th>";
                    $th .= "<th>Valor fatura</th>"; 
                    $th .= "<th>Valor caixa</th>";
                foreach ($atendimento->atendimento_procedimentos as $p) {
                    $td .= "<tr>";
                    $td .= "<td>".$p->procedimento->id."</td>";
                    $td .= "<td>".$p->procedimento->nome."</td>";
                    $td .= "<td>".number_format($p->valor_fatura,2,',','.')."</td>";    
                    $td .= "<td>".number_format($p->valor_caixa,2,',','.')."</td>";
                    $td .= "</tr>";
                }
                break;
            case '{tabela_todos_valor_caixa_matmed}':
                    $th .= "<th>Cód</th>";
                    $th .= "<th>Descrição</th>";
                    $th .= "<th>Valor fatura</th>"; 
                    $th .= "<th>Valor caixa</th>";
                    $th .= "<th>Valor matmed</th>";
                foreach ($atendimento->atendimento_procedimentos as $p) {
                    $td .= "<tr>";
                    $td .= "<td>".$p->procedimento->id."</td>";
                    $td .= "<td>".$p->procedimento->nome."</td>";
                    $td .= "<td>".number_format($p->valor_fatura,2,',','.')."</td>";    
                    $td .= "<td>".number_format($p->valor_caixa,2,',','.')."</td>";
                    $td .= "<td>".number_format($p->valor_matmed,2,',','.')."</td>";
                    $td .= "</tr>";
                }
                break;
        }
        $table = "";
        $table .= "<div class='table table-responsive'>";
        $table .= "<table cellspacing='1' cellpadding='10' class='table' border='1' width='100%'>";
        $table .= "<thead>";
        $table .= "<tr>";
        $table .= $th;
        $table .= "</tr>";
        $table .= "</thead>";
        $table .= "<tbody>";
        $table .= $td;
        $table .= "</tbody>";
        $table .= "</table>";
        $table .= "</div>";
        return $table;
    }

    /**
     * Gera tabela para os Dados Financeiro
     *
     * @param Object $contas_receber
     * @return void
     */
    public function tableFinanceiro($contas_receber)
    {
        $table = "";
        $table .= "<div class='table table-responsive'>";
        $table .= "<table cellspacing='1' cellpadding='10' class='table' border='1' width='100%'>";
        $table .= "<thead>";
        $table .= "<tr>";
        $table .= "<th>Vencimento</th>";
        $table .= "<th>Tipo Documento</th>";
        $table .= "<th>Numero Documento</th>";
        $table .= "<th>Valor</th>";
        $table .= "</tr>";
        $table .= "</thead>";
        $table .= "<tbody>";

        foreach ($contas_receber as $c) {
            $table .= "<tr>";
            $table .= "<td>".$c->vencimento."</td>";
            $table .= "<td>".@$c->tipo_documento->descricao."</td>";
            $table .= "<td>".$c->numerodocumento."</td>";
            $table .= "<td>".number_format($c->valor,2,',','.')."</td>";
            $table .= "</tr>";
        }
        $table .= "</tbody>";
        $table .= "</table>";
        $table .= "</div>";
        return $table;
    }
    /******************************************/
    
}