<?php
/**
 * Created by PhpStorm.
 * User: Iohan
 * Date: 04/07/2017
 * Time: 11:20
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use DateTime;

class ContaPagarComponent extends Component
{

    public $components = ['Data'];

    public function filtroContasPagar($query, $request)
    {
        $query->andWhere(['FinContasPagar.situacao' => 1]);

        if (!empty($request['inicio'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $request['inicio']);
            $query->andWhere(['FinContasPagar.vencimento >=' => $now->format('Y-m-d')]);
        } else {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', date('01/m/Y'));
            $query->andWhere(['FinContasPagar.vencimento >=' => $now->format('Y-m-d')]);
        }

        if (!empty($request['fim'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $request['fim']);
            $query->andWhere(['FinContasPagar.vencimento <=' => $now->format('Y-m-d')]);
        } else {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', date('t/m/Y'));
            $query->andWhere(['FinContasPagar.vencimento <=' => $now->format('Y-m-d')]);
        }

        if (!empty($request['situacao'])) {
            if ($request['situacao'] === '1') {
                $query->andWhere(['FinContasPagar.data_pagamento IS NOT NULL']);
            } else if ($request['situacao'] === '2') {
                $query->andWhere(['FinContasPagar.data_pagamento IS NULL']);
            }
        }

        if (!empty($request['planoconta'])) {
            $query->andWhere(['FinContasPagar.fin_plano_conta_id IN' => $request['planoconta']]);
        }

        if (!empty($request['documento'])) {
            debug($request['documento']);
            $query->andWhere(['FinContasPagar.numero_documento LIKE ' => '%'.$request['documento'].'%']);
            debug($query->toArray());
        }

        if (!empty($request['fornecedor'])) {
            $query->andWhere(['FinContasPagar.fin_fornecedor_id IN' => $request['fornecedor']]);
        }

        if (!empty($request['contabilidade'])) {
            $query->andWhere(['FinContasPagar.fin_contabilidade_id' => $request['contabilidade']]);
        }

        if (!empty($request['status_conta'])) {
            if ($request['status_conta'] == 1) {
                $query->andWhere(['FinContasPagar.situacao' => 1]);
            } else if ($request['status_conta'] == 2) {
                $query->andWHere(['FinContasPagar.situacao' => 2]);
            }
        }

        return $query;
    }
}
