<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

//Component criado minimizar o uso de linhas com chamadas para models
class ModelComponent extends Component {

    //Função para busca em models.
    public function getModelData($model, $function, $data = null, $data2 = null)
    {   
        if ($model) {
            return $result = TableRegistry::get($model)->$function($data, $data2);
        } else {
            return false;
        }
    }
}