<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class FinGrupoContabilidadesComponent extends Component
{
    public function addCompanies($group, $companies)
    {
        $FinGrupoContabilidades = TableRegistry::get('FinGrupoContabilidades');
        $companiesFallBack = [];
        foreach($companies as $key => $company)
        {
            $finGrupoContabilidades = $FinGrupoContabilidades->newEntity();
            $newGroupCompanies = [];
            $newGroupCompanies['fin_grupo_id'] = $group;
            $newGroupCompanies['fin_contabilidade_id'] = $company;
            $finGrupoContabilidades = $FinGrupoContabilidades->patchEntity($finGrupoContabilidades, $newGroupCompanies);
            if ($FinGrupoContabilidades->save($finGrupoContabilidades)) {
                $companiesFallBack[] = $finGrupoContabilidades;
            } else {
                foreach($companiesFallBack as $key => $companyFallBack)
                {
                    $FinGrupoContabilidades->delete($companyFallBack);
                }
                return false;
            }
        }
        return true;
    }

    public function editCompanies($group, $companies)
    {
        $FinGrupoContabilidades = TableRegistry::get('FinGrupoContabilidades');
        
        //Pegando grupo contabilidades desse grupo
        $existentGroupCompanies = $FinGrupoContabilidades->find()->where(['FinGrupoContabilidades.fin_grupo_id' => $group]);
        
        //Excluindo grupos que possuem à esse grupo
        $FinGrupoContabilidades->deleteAll(['FinGrupoContabilidades.fin_grupo_id' => $group]);

        foreach($companies as $key => $company)
        {
            $finGrupoContabilidades = $FinGrupoContabilidades->newEntity();
            $newGroupCompanies = [];
            $newGroupCompanies['fin_grupo_id'] = $group;
            $newGroupCompanies['fin_contabilidade_id'] = $company;
            $finGrupoContabilidades = $FinGrupoContabilidades->patchEntity($finGrupoContabilidades, $newGroupCompanies);
            if ($FinGrupoContabilidades->save($finGrupoContabilidades)) {
                $companiesFallBack[] = $finGrupoContabilidades;
            } else {
                foreach($companiesFallBack as $key => $companyFallBack)
                {
                    $FinGrupoContabilidades->delete($companyFallBack);
                }
                foreach($existentGroupCompanies as $existentGroupCompany)
                {
                    $FinGrupoContabilidades->save($existentGroupCompanies);
                }
                return false;
            }
        }
        return true;
    }
}