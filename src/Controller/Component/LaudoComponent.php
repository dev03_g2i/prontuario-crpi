<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class LaudoComponent extends Component
{
    public $components = array('Data');

    public function filterAtendProc($query, $requestQuery)
    {
        /* ID do atendimento */
        if(!empty($requestQuery['ficha'])){
            $query->andWhere(['AtendimentoProcedimentos.atendimento_id' => (int)$requestQuery['ficha']]);
        }
        if(!empty($requestQuery['situacao'])){
            $query->andWhere(['AtendimentoProcedimentos.situacao_laudo_id' => (int)$requestQuery['situacao']]);
        }
        /* ID do atendimentoProcediemnto */
        if(!empty($requestQuery['arquivo_integracao'])){
            $query->andWhere(['AtendimentoProcedimentos.id' => (int)$requestQuery['arquivo_integracao']]);
        }
        if(!empty($requestQuery['profissional'])){
            $query->andWhere(['AtendimentoProcedimentos.medico_id' => $requestQuery['profissional']]);
        }
        if(!empty($requestQuery['paciente'])){
            $query->andWhere(['Clientes.nome LIKE ' => '%'.$requestQuery['paciente'].'%']);
        }
        /* Atalhos */
        if(!empty($requestQuery['filtro_atalho'])){
            $situacao = $requestQuery['filtro_atalho'];
            /* Situacções para tecnicos [user_diagnostico = 0] */
            if($situacao == 1){
                $query->andWhere([$requestQuery['tipo_data'] => date('Y-m-d')]);// Hoje
            }elseif($situacao == 2){
                $query->andWhere(['AtendimentoProcedimentos.situacao_laudo_id' => 100]);// Em espera
            }elseif ($situacao == 3){
                $query->andWhere(['AtendimentoProcedimentos.situacao_laudo_id > ' => 100]);// Maior que Em espera
            }elseif ($situacao == 4){// Periodos
                if (!empty($requestQuery['inicio'])) {
                    $query->andWhere([$requestQuery['tipo_data'] . ' >=' => $this->Data->DataSQL($requestQuery['inicio']) . ' 00:00:00']);
                }
                if (!empty($requestQuery['fim'])) {
                    $query->andWhere([$requestQuery['tipo_data'] . ' <=' => $this->Data->DataSQL($requestQuery['fim']) . ' 23:59:59']);
                }
            }
            /*******************************/
            /* Situações para Radiologista [user_diagnostico = 1] */
            if($situacao == 5){
                $query->andWhere(['AtendimentoProcedimentos.situacao_laudo_id <= ' => 600]);// Menor que Corrigir
            }elseif ($situacao == 6){
                $query->andWhere(
                    ['AtendimentoProcedimentos.situacao_laudo_id <= ' => 600],// Menor que Corrigir e dt_entrega menor que data de hoje
                    ['AtendimentoProcedimentos.dt_entrega <' => date('Y-m-d')]
                );
            }elseif ($situacao == 7){
                $query->andWhere(['AtendimentoProcedimentos.situacao_laudo_id >= ' => 800]);// Menor que Assinado
            }
            /*************************************************************/
        }

        /*if(!empty($requestQuery['tipo_filtro']) && $requestQuery['tipo_filtro'] == 1){// Hoje
            $query->andWhere([$requestQuery['tipo_data'] . ' >=' => date('Y-m-d') . ' 00:00:00']);
            $query->andWhere([$requestQuery['tipo_data'] . ' <=' => date('Y-m-d') . ' 23:59:59']);
        }elseif (!empty($requestQuery['tipo_filtro']) && $requestQuery['tipo_filtro'] == 2){// Amanha
            $hoje = new Time();
            $amanha = $hoje->addDay(1);
            $query->andWhere([$requestQuery['tipo_data'] . ' >=' => $amanha->format('Y-m-d') . ' 00:00:00']);
            $query->andWhere([$requestQuery['tipo_data'] . ' <=' => $amanha->format('Y-m-d') . ' 23:59:59']);
        }elseif (!empty($requestQuery['tipo_filtro']) && $requestQuery['tipo_filtro'] == 2) {// Abertos
            $query->andWhere(['AtendimentoProcedimentos.situacao_laudo_id' => 2]);// Em espera
        }elseif (!empty($requestQuery['tipo_filtro']) && $requestQuery['tipo_filtro'] == 4) {// Periodo
            if (!empty($requestQuery['inicio'])) {
                $query->andWhere([$requestQuery['tipo_data'] . ' >=' => $this->Data->DataSQL($requestQuery['inicio']) . ' 00:00:00']);
            }
            if (!empty($requestQuery['fim'])) {
                $query->andWhere([$requestQuery['tipo_data'] . ' <=' => $this->Data->DataSQL($requestQuery['fim']) . ' 23:59:59']);
            }
        }elseif (!empty($requestQuery['tipo_filtro']) && $requestQuery['tipo_filtro'] == 5) {// Atrasados
            $hoje = new Time();
            $ontem = $hoje->subDay(1);
            $query->andWhere([$requestQuery['tipo_data'] . ' >=' => $ontem->format('Y-m-d') . ' 00:00:00']);
            $query->andWhere([$requestQuery['tipo_data'] . ' <=' => $ontem->format('Y-m-d') . ' 23:59:59']);
        }*/
        if(!empty($requestQuery['grupo_procedimento_id'])){
            $query->andWhere(['Procedimentos.grupo_id' => $requestQuery['grupo_procedimento_id']]);
        }
        if(!empty($requestQuery['tipo_data'])){
            $query->order([$requestQuery['tipo_data'] => 'DESC']);
        }

        return $query;
    }
}