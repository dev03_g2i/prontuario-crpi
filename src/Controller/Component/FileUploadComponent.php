<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
class FileUploadComponent extends Component {

    const DIRECTORY_FILES_UPLOAD = 'files';

    /**
     * Faz o upload de um arquivo e retorna seu path.
     * @param  Array() 		$data Array cotendo os dados do arquivo.
     * @return String       path do arquivo salvo na aplocação.
     */
    public function uploadFile($data = null){

        if(!empty($data)){
            $fileName 		= $data['name'];
            $temp_file_name = $data['tmp_name'];
            $type 			= $data['type'];
            $file_size 		= $data['size'];

            $dir = dirname(dirname(dirname(dirname(__FILE__)))) . DS . "app" . DS . "webroot" . DS . self::DIRECTORY_FILES_UPLOAD;

            if( !is_dir($dir)){
                mkdir($dir, 0755, true);
            }

            if(is_uploaded_file($temp_file_name)) {
                $filePath = self::DIRECTORY_FILES_UPLOAD.String::uuid().'-'.$fileName;
                move_uploaded_file($temp_file_name, $filePath);
                return $filePath;
            }
        }

        return null;
    }

    public function send($from, $to) {
        $dir = dirname(dirname(dirname(dirname(__FILE__)))) . DS . "app" . DS . "webroot" . DS . self::DIRECTORY_FILES_UPLOAD;
        $to = $dir. DS ."{$to}";

        if( !is_dir($dir) )
            mkdir($dir);

        if( move_uploaded_file($from, $to) )
            return true;

        return false;
    }

    public function getExt($file) {
        $tmp = explode('.', $file);
        return strtolower($tmp[count($tmp)-1]);
    }

    public function getDirectory($file) {
        $tmp = explode('/', $file);
        unset($tmp[count($tmp)-1]);
        return implode('/', $tmp);
    }

    public function uploadFilePath($data = null,$path){

        if(!empty($data)){
            $fileName 		= $data['name'];
            $temp_file_name = $data['tmp_name'];
            $type 			= $data['type'];
            $file_size 		= $data['size'];


            $dir = dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."img". DS ."files".DS.$path;
            $aux = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//            $filesDir = $dir. DS ."{$to}";


            if( !is_dir($dir) ){
                mkdir($dir, 0755, true);
            }

            if(is_uploaded_file($temp_file_name)) {
                $nome = "files".DS.$path.DS.time().'-'.$fileName;
                $filePath = "img".DS."files".DS.$path.DS.time().'-'.$fileName;
                move_uploaded_file($temp_file_name, $filePath);
                return $nome;

            }

        }

        return null;
    }

}