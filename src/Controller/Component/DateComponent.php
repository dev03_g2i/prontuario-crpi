<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\I18n\Time;
use DateTime;

class DateComponent extends Component {

    //Função pública para pegar o ultimo dia do mês atual, ou do mes que passar como parâmetro.
    public function getLastDayOfMonth($date = null)
    {
        if (!$date){
            $date = new Time();
            $lastDayOfMonth = date("t", strtotime($date));
        } else {
            $lastDayOfMonth = date("t", strtotime($date));
        }
        return $lastDayOfMonth;
    }

    public function getYesterday($date = null)
    {
        if (!$date){
            $date = new Time();
            $yesterday = date('Y-m-d', strtotime($date .' -1 day'));
        } else {
            $yesterday = date('Y-m-d', strtotime($date .' -1 day'));
        }
        return $yesterday;
    }

    function formatDateForSql($date = null) {
        $format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';
        if ($date != null && preg_match($format, $date, $parts)) {
            return $parts[3].'-'.$parts[2].'-'.$parts[1];
        }
        return false;
    }

    public function getMonthsNames(){
        $months = array(
            '01' => "Janeiro",
            '02' => "Fevereiro",
            '03' => "Março",
            '04' => "Abril",
            '05' => "Maio",
            '06' => "Junho",
            '07' => "Julho",
            '08' => "Agosto",
            '09' => "Setembro",
            '10' => "Outubro",
            '11' => "Novembro",
            '12' => "Dezembro"
        );
        return $months;
    }

    public function yearsList()
    {
        $now = new Time();
        $actualYear = $now->addYear(10)->format('Y');

        $rangeYear = range(2000, $actualYear);
        $years =[];
        foreach ($rangeYear as $k => $v){
            $years[$v] = $v;
        }
        return $years;
    }
}