<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * ConfiguracaoAgenda component
 */
class ConfiguracaoAgendaComponent extends Component
{

    private $configuracaoAgenda = "";

    public function configuracaoAgenda()
    {
        if (empty($this->configuracaoAgenda)) {
            $this->configuracaoAgenda = TableRegistry::get('ConfiguracaoAgenda')->find()->first();
        }

        return $this->configuracaoAgenda;
    }

    /**
     * 1ª Documentação - 27/01/2021, por Mateus Ragazzi
     * Função que verifica se os Avisos e Particularidades da Agenda virão abertas ou fechadas.
     * Por padrão, eles virão abertos.
     * 
     * @return true|false define se o frontend vai deixar aberto ou não os recados.
     */
    public function mostraAvisosAgendaAberto()
    {
        return $this->configuracaoAgenda()->mostra_avisos_agenda_aberto;
    }

}
