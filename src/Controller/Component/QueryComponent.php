<?php
/**
 * Created by PhpStorm.
 * User: Desenvolvimento02
 * Date: 02/06/2017
 * Time: 08:57
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\ConnectionManager;

class QueryComponent extends Component
{
    function connDefault()
    {
        return $conn = ConnectionManager::get('default');
    }

    function insertTempFaturamento($conn, $and)
    {
        $query = "
            INSERT INTO temp_faturamentos (
            id, selecionado, atendimento_id, unidade, convenio_id, convenio, profissional, data_atendimento, hora, 
            paciente, matricula, codigo_tabela, procedimento, guia, senha, quantidade, valor_fatura, valor_caixa, 
            valor_material, total, created, modified, situacao_id, situacao_fatura_id
            )
            SELECT ap.id, CASE WHEN ap.situacao_fatura_id = 1 THEN 0 ELSE 1 END , atendimento_id, u.id, con.id, con.nome, mr.nome, a.data, a.hora, c.nome, c.matricula, ap.codigo, p.nome, ap.nr_guia, ap.autorizacao_senha,
              ap.quantidade, ap.valor_fatura, ap.valor_caixa, ap.valor_material, ap.total, ap.created, ap.modified, ap.situacao_id, ap.situacao_fatura_id
            
            FROM atendimento_procedimentos ap
            INNER JOIN atendimentos a 
            ON(a.id = ap.atendimento_id)
            INNER JOIN procedimentos p 
            ON (p.id = ap.procedimento_id)
            INNER JOIN clientes c 
            ON(c.id = a.cliente_id)
            INNER JOIN convenios con 
            ON (con.id = a.convenio_id)
            INNER JOIN unidades u 
            ON (u.id = a.unidade_id)
            INNER JOIN medico_responsaveis mr
            ON (mr.id = ap.medico_id)
            WHERE ap.situacao_id = 1 AND a.situacao_id = 1 AND p.situacao_id = 1 AND ap.status_faturamento = 0
        ";
        $string = trim(preg_replace('/\s+/', ' ', $query));
        $busca = "{$string} {$and}";
        return $conn->query($busca);
    }

    function insertFaturaEncerramento($conn, $user_id, $mesReferencia, $anoReferencia, $mesAno, $dataVencimento)
    {
        //Campo valor_equipe está recebendo o valor de padrão de 0.00
        //OBS.: Embora o campo na tabela fatura_encarramentos tenha o nome valor_mat_med, ele puxa o valor valor_material da tabela temp_faturamentos que por sua vez puxa o valor valor_material da tabela atendimento_procedimentos
        $query = "
            INSERT INTO fatura_encerramentos (
                competencia, vencimento, observacao, data_encerramento, total_bruto, num_exames, convenio_id, 
                unidade_id, taxas, descontos, created, modified, qtd_total, user_id, situacao_id, situacao_faturaencerramento_id, valor_mat_med, valor_equipe, competencia_mes, competencia_ano
            ) SELECT  
                '$mesAno', '$dataVencimento', null, now(), SUM(total + valor_material), null, convenio_id, unidade, null, null, now(), now(), SUM(quantidade), $user_id, situacao_id = 1, 1, SUM(valor_material), 0.00, '$mesReferencia', '$anoReferencia'
            FROM temp_faturamentos  WHERE selecionado = 1;
            SET @last_insert_id = LAST_INSERT_ID();
            UPDATE atendimento_procedimentos ap JOIN temp_faturamentos tf 
            ON ap.id = tf.id SET ap.situacao_fatura_id = 2, ap.status_faturamento = 1, ap.fatura_encerramento_id = @last_insert_id WHERE tf.selecionado = 1;
            TRUNCATE TABLE temp_faturamentos;
        ";

        return $conn->execute($query);
    }

    function deleteAllTempFaturamento($conn)
    {
        $query = " TRUNCATE TABLE temp_faturamentos ";
        return $conn->query($query);
    }

    function insertTempProdutividadeProfissionais($conn, $and)
    {
        //Retirado o join com laudos da query
        //INNER JOIN laudos la
        //ON(ap.id = la.atendimento_procedimento_id)
        $query = "
   INSERT INTO temp_produtividadeprofissionais (
            id, atendimento_id, convenio_id, convenio, profissional, data_atendimento_formatada,data_atendimento, hora, 
            paciente, codigo, procedimento, quantidade, valor_fatura, valor_caixa, 
            valor_material, total, created, modified, situacao_id, perc_recebimento, perc_imposto, valor_prod_fatura,
             valor_prod_caixa, valor_prodrec_convenio, valor_recebido, valor_prodclin_recebimento, valor_prodcaixa_clinica,
             producao_pg_dt, valor_caixa_original,usa_valor_original, solicitante_id, solicitante, grupo_procedimento)
            SELECT ap.id, atendimento_id, con.id, con.nome, mr.nome, DATE_FORMAT(a.data ,'%d/%m/%Y') ,a.data, a.hora, c.nome, ap.codigo, p.nome,
              ap.quantidade, ap.valor_fatura, ap.valor_caixa, ap.valor_material, ap.total, ap.created, ap.modified, 
              ap.situacao_id, pp.perc_recebimento, pp.perc_imposto, (ap.total  * (1-pp.perc_imposto/100)*(pp.perc_recebimento/100)), 
              (ap.valor_caixa  * (1-pp.perc_imposto/100)*(pp.perc_recebimento/100)), 
              (ap.valor_recebido  * (1-pp.perc_imposto/100)*(pp.perc_recebimento/100)), 
              valor_recebido, ((ap.valor_recebido * (1-pp.perc_imposto/100))*(1-pp.perc_recebimento/100)), 
              ((ap.valor_caixa * (1-pp.perc_imposto/100))*(1-pp.perc_recebimento/100)), ap.producao_pg_dt,
		      IF(ap.vl_caixa_original IS NULL, 0, ap.vl_caixa_original), con.usa_valor_original,
		      a.solicitante_id, s.nome, gps.nome
            FROM atendimento_procedimentos ap
            INNER JOIN atendimentos a 
            ON(a.id = ap.atendimento_id)
            INNER JOIN solicitantes s
            ON (a.solicitante_id = s.id)
            INNER JOIN procedimentos p 
            ON (p.id = ap.procedimento_id)
            INNER JOIN grupo_procedimentos gps
            ON (p.grupo_id = gps.id)
            INNER JOIN clientes c 
            ON(c.id = a.cliente_id)
            INNER JOIN convenios con 
            ON (con.id = a.convenio_id)
            INNER JOIN unidades u 
            ON (u.id = a.unidade_id)
            INNER JOIN medico_responsaveis mr
            ON (mr.id = ap.medico_id)
            LEFT JOIN produtividade_profissionais pp
            ON (pp.procedimento_id = ap.procedimento_id AND pp.convenio_id = a.convenio_id AND pp.profissional_id = ap.medico_id)
            WHERE ap.situacao_id = 1 AND a.situacao_id = 1 AND p.situacao_id = 1
        ";
        $string = trim(preg_replace('/\s+/', ' ', $query));
        $busca = "{$string} {$and}";
        return $conn->query($busca);
    }

    function clearTemp($conn, $table)
    {
        $query = " TRUNCATE TABLE $table ";
        return $conn->query($query);
    }

    function recReceberTodos($faturaid, $dataRecebimento, $situacao, $column_valor, $conn)
    {
        $query = "UPDATE atendimento_procedimentos ap SET ap.dt_recebimento = '$dataRecebimento', ap.situacao_recebimento_id = $situacao, ap.valor_recebido = ap.$column_valor WHERE ap.fatura_encerramento_id = $faturaid";
        return $conn->query($query);
    }

    function prodReceberTodos($data, $user_id, $user_dt, $conn)
    {
        $query = "UPDATE atendimento_procedimentos ap JOIN temp_produtividadeprofissionais tpf 
            ON ap.id = tpf.id SET ap.producao_pg_dt = '" . $data . "', ap.producao_pg_user = '" . $user_id . "', ap.producao_pg_dtuser = '" . $user_dt . "' WHERE ap.producao_pg_dt IS NULL;
            ";
        return $conn->query($query);
    }

    function matmedReceberTodos($dateRec, $atendimento_id, $conn)
    {
        $query = "UPDATE fatura_matmed fm SET fm.dt_recebimento = '$dateRec', fm.qtd_recebida = fm.quantidade, fm.vl_recebido = fm.total WHERE fm.atendimento_id = $atendimento_id";
        return $conn->query($query);
    }

    function operacaoHorarios($query)
    {
        if (!empty($query))
            return $this->connDefault()->query($query);
        else
            return true;
    }

    function selectTabelaResultadosHistoricosAbertos($ano, $plano_conta, $classificacao_conta, $tipo)
    {
        $conn = $this->connDefault();
        $query = "
            SELECT fpc.`id` as plano_conta_id , fcc.`tipo`, fpc.`nome`, fcc.`descricao` AS classificacao_conta, fcc.`id` AS classificacao_id,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 1) THEN (fm.credito - fm.debito) END), 2) AS janeiro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 2) THEN (fm.credito - fm.debito) END), 2) AS fevereiro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 3) THEN (fm.credito - fm.debito) END), 2) AS marco,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 4) THEN (fm.credito - fm.debito) END), 2) AS abril, 
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 5) THEN (fm.credito - fm.debito) END), 2) AS maio,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 6) THEN (fm.credito - fm.debito) END), 2) AS junho,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 7) THEN (fm.credito - fm.debito) END), 2) AS julho,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 8) THEN (fm.credito - fm.debito) END), 2) AS agosto,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 9) THEN (fm.credito - fm.debito) END), 2) AS setembro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 10) THEN (fm.credito - fm.debito) END), 2) AS outubro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 11) THEN (fm.credito - fm.debito) END), 2) AS novembro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 12) THEN (fm.credito - fm.debito) END), 2) AS dezembro,
            ROUND(SUM(fm.credito - fm.debito), 2) AS total 
            FROM fin_movimentos fm
            JOIN fin_plano_contas fpc ON fpc.`id` = fm.`fin_plano_conta_id`
            JOIN fin_classificacao_contas fcc ON fcc.id = fpc.`fin_classificacao_conta_id`
            WHERE YEAR(fm.`data`) = '$ano'
            AND fcc.considera = 1
            AND fm.situacao = 1 ";

        if (!empty($plano_conta)) {
            $query .= " AND fpc.id = $plano_conta ";
        }

        if (!empty($classificacao_conta)) {
            $query .= " AND fcc.id = $classificacao_conta ";
        }

        if (!empty($tipo)) {
            $query .= " AND fcc.tipo = $tipo ";
        }
        $query .= "
            GROUP BY fpc.`id`
            ORDER BY fcc.`id`, fpc.`nome` ASC;
        ";
        return $conn->query($query);
    }

    function selectTabelaResultadosHistoricos($ano, $plano_conta, $classificacao_conta, $tipo)
    {
        $conn = $this->connDefault();
        $query = "
            SELECT fpc.`id` , fcc.`tipo`, fcc.`descricao` as nome,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 1) THEN (fm.credito - fm.debito) END), 2) AS janeiro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 2) THEN (fm.credito - fm.debito) END), 2) AS fevereiro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 3) THEN (fm.credito - fm.debito) END), 2) AS marco,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 4) THEN (fm.credito - fm.debito) END), 2) AS abril, 
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 5) THEN (fm.credito - fm.debito) END), 2) AS maio,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 6) THEN (fm.credito - fm.debito) END), 2) AS junho,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 7) THEN (fm.credito - fm.debito) END), 2) AS julho,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 8) THEN (fm.credito - fm.debito) END), 2) AS agosto,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 9) THEN (fm.credito - fm.debito) END), 2) AS setembro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 10) THEN (fm.credito - fm.debito) END), 2) AS outubro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 11) THEN (fm.credito - fm.debito) END), 2) AS novembro,
            ROUND(SUM(CASE WHEN (MONTH(fm.data) = 12) THEN (fm.credito - fm.debito) END), 2) AS dezembro,
            ROUND(SUM(fm.credito - fm.debito), 2) AS total 
            FROM fin_movimentos fm
            JOIN fin_plano_contas fpc ON fpc.`id` = fm.`fin_plano_conta_id`
            JOIN fin_classificacao_contas fcc ON fcc.id = fpc.`fin_classificacao_conta_id`
            WHERE YEAR(fm.`data`) = '$ano'
            AND fcc.considera = 1
            AND fm.situacao = 1 
        ";

        if (!empty($plano_conta)) {
            $query .= " AND fpc.id = $plano_conta ";
        }

        if (!empty($classificacao_conta)) {
            $query .= " AND fcc.id = $classificacao_conta ";
        }

        if (!empty($tipo)) {
            $query .= " AND fcc.tipo = $tipo ";
        }

        $query .= "
            GROUP BY fcc.`id`
            ORDER BY fcc.`descricao` ASC;
        ";


        return $conn->query($query);
    }
}


?>