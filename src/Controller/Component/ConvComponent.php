<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ConvComponent extends Component
{
    public function getListaTipoCodigo()
    {
        return array('CNPJ' => 'CNPJ', 'cpf' => 'cpf', 'codigoPrestadorNaOperadora' => 'codigoPrestadorNaOperadora', 'conselhoProfissional' => 'conselhoProfissional');
    }

}