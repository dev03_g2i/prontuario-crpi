<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class PermissoesComponent extends Component
{
    private $grupo_controllers;
    private $actions = [];


    /**
     * @return mixed
     */
    public function getGrupoControllers()
    {
        return $this->grupo_controllers;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function iniciar($grupo_id){
        $this->GrupoControladores = TableRegistry::get('GrupoControladores');
                $grupo_controlador = $this->GrupoControladores->find('all')
                    ->contain(['Controladores', 'GrupoControladorAcoes' => ['Acoes', 'conditions' => ['GrupoControladorAcoes.acesso' => 0]]])
                    ->where(['GrupoControladores.grupo_id' => $grupo_id]);
                $this->grupo_controllers = $grupo_controlador;
                $this->get_acoes();
    }


    public function get_acoes(){
        $grupo_controlador = $this->getGrupoControllers();
            foreach ($grupo_controlador as $g){
                if(!empty($g->grupo_controlador_acoes)){
                    foreach ($g->grupo_controlador_acoes as $grupo_acoes) {
                        if (!empty($grupo_acoes->aco)){
                            if(substr_count($grupo_acoes->aco->acao, ',')){
                                $acts = explode(',',$grupo_acoes->aco->acao);
                                foreach ($acts as $act) {
                                    $this->actions[] = $g->controladore->controlador.'/'.$act;
                                }
                            }else{
                                $this->actions[] = $g->controladore->controlador.'/'.$grupo_acoes->aco->acao;
                            }
                        }
                    }
                }
            }

    }

}