<?php
/**
 * Created by PhpStorm.
 * User: dev02
 * Date: 18/08/2017
 * Time: 09:30
 */

namespace App\Controller\Component;


use Cake\Controller\Component;
use Cake\Database\Expression\IdentifierExpression;
use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class EstatisticaComponent extends Component
{
    public function getMeses()
    {
        $meses = [
            1 => 'qtdJan',
            2 => 'qtdFev',
            3 => 'qtdMar',
            4 => 'qtdAbr',
            5 => 'qtdMai',
            6 => 'qtdJun',
            7 => 'qtdJul',
            8 => 'qtdAgo',
            9 => 'qtdSet',
            10 => 'qtdOut',
            11 => 'qtdNov',
            12 => 'qtdDez'
        ];
        return $meses;
    }

    public function getMonthName()
    {
        $meses = [
            'Janeiro', 
            'Fevereiro', 
            'Março', 
            'Abril', 
            'Maio', 
            'Junho', 
            'Julho', 
            'Agosto', 
            'Setembro', 
            'Outubro', 
            'Novembro', 
            'Dezembro'
        ];
        return $meses;
    }

    public function getTipoEstatisticas($page)
    {   
        if ($page === 'rh') {
            $tipos = [
                8 => 'Registro geral',
                9 => 'Registro x Grupo de Procedimento',
                10 => 'Agendamentos',
                11 => 'Agendamentos x Agenda',
                12 => 'Laudos Digitados',
                13 => 'Laudos Digitados x Grupo',
                14 => 'Laudos Assinados',
                15 => 'Laudos Assinados x Grupo'
            ];
        } else {
            $tipos = [
                1 => 'Grupo Procedimento x Procedimento',
                2 => 'Grupo Procedimento x Convênios',
                3 => 'Grupo Procedimento',
                4 => 'Grupo Procedimento x Origens',
                5 => 'Grupo Procedimento x Unidades',
                6 => 'Grupo Procedimento x Solicitantes',
                7 => 'Solicitantes'
            ];
        }
        return $tipos;
    }

    public function getAnos()
    {
        $now = Time::now();
        $anos = [
            $now->subYear(1)->year => $now->year,
            $now->addYear(1)->year => $now->year,
            $now->addYear(1)->year => $now->year,

        ];
        return $anos;
    }

    public function getConvenios()
    {
        $convenio = TableRegistry::get('Convenios');
        return $convenio->find('list')
            ->where(['Convenios.situacao_id' => 1])
            ->order(['Convenios.nome ASC']);

    }

    public function getGrupos($tipo)
    {
        $grupos = TableRegistry::get('GrupoProcedimentos');
        return $grupos->find($tipo)
            ->where(['GrupoProcedimentos.situacao_id' => 1]);
    }

    public function getGrupoAgend() 
    {
        $grupos = TableRegistry::get('GrupoAgendas');
        return $grupos->find()
            ->where(['GrupoAgendas.situacao_id' => 1]);
    }

    public function getUnidades()
    {
        $unidades = TableRegistry::get('Unidades');
        return $unidades->find('list')
            ->where(['Unidades.situacao_id' => 1]);
    }

    //Método para pegar os atendimentos procedimentos separados por procedimento
    public function getAtendProcPorGrupoProc($id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos'])
            ->where(['GrupoProcedimentos.id' => $id])
            ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1])
            ->group(['Procedimentos.nome'])
            ->order(['GrupoProcedimentos.nome ASC']);
    }

    //Método para pegar os atendimentos procedimentos separados por convênio
    public function getGrupoProcPorConvenio($id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos' => ['Convenios']])
            ->where(['GrupoProcedimentos.id' => $id])
            ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Convenios.situacao_id' => 1])
            ->group(['Convenios.nome'])
            ->order(['Convenios.nome ASC']);
    }

    //Método para pegar os atendimentos procedimentos
    public function getGrupoProc()
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->group(['GrupoProcedimentos.nome'])
            ->order(['GrupoProcedimentos.nome ASC']);
    }

    //Método para pegar os atendimentos procedimentos separados por origem
    public function getGrupoProcPorOrigem($id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos' => ['Origens']])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Origens.situacao_id' => 1])
            ->andWhere(['Atendimentos.origen_id IS NOT NULL'])
            ->andWhere(['GrupoProcedimentos.id' => $id])
            ->group(['Origens.nome'])
            ->order(['Origens.nome ASC']);
    }

    //Método para pegar os atendimentos procedimentos separados por unidade
    public function getGrupoProcPorUnidade($id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos' => ['Unidades']])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['GrupoProcedimentos.id' => $id])
            ->andWhere(['Unidades.situacao_id' => 1])
            ->group(['Unidades.nome'])
            ->order(['Unidades.nome ASC']);
    }

    //Método para pegar os atendimentos procedimentos separados por solicitante
    public function getGrupoProcPorSolicitante($id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos' => ['Solicitantes']])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['GrupoProcedimentos.id' => $id])
            ->andWhere(['Solicitantes.situacao_id' => 1])
            ->group(['Solicitantes.nome'])
            ->order(['Solicitantes.nome ASC']);
    }

    //Método para pegar os Solicitantes rankeados 100
    public function getSolicitantes()
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos'=> ['Solicitantes']])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Solicitantes.situacao_id' => 1])
            ->group(['Solicitantes.nome'])
            ->limit(100);
    }
    
    public function getAtendProcs()
    {   
        $atendimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos' => ['Users']])
            ->group(['Atendimentos.user_id']);
    }

    public function getAtendProcsGrup($id)
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        return $atendimentoProcedimentos->find('all')
            ->contain(['Procedimentos' => ['GrupoProcedimentos'], 'Atendimentos' => ['Users']])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['GrupoProcedimentos.id' => $id])
            ->group(['Atendimentos.user_id']);
    }

    public function getAgendas()
    {   
        $agendas = TableRegistry::get('Agendas');

        return $agendas->find('all')
            ->contain(['Users'])
            ->group(['Agendas.user_id'])
            ->order(['Agendas.created']);
    }

    public function getAgendasGrup($id)
    {   
        $agendas = TableRegistry::get('Agendas');

        return $agendas->find('all')
            ->contain(['GrupoAgendas', 'Users'])
            ->where(['Agendas.grupo_id' => $id])
            ->group(['Agendas.user_id'])
            ->order(['Agendas.created']);
    }

    public function getLaudos($type)
    {   
        $laudos = TableRegistry::get('Laudos');
        if ($type === 'LaudosDigitados' ) {
            return $laudos->find('all')
                ->contain(['AtendimentoProcedimentos' => ['Atendimentos', 'Procedimentos' => ['GrupoProcedimentos']], 'Users'])
                ->where(['Laudos.situacao_id' => 500])
                ->group(['Laudos.user_id'])
                ->order(['Laudos.created']);
        } else {
            return $laudos->find('all')
                ->contain(['AtendimentoProcedimentos' => ['Atendimentos', 'Procedimentos' => ['GrupoProcedimentos']], 'UsersAssinado'])
                ->where(['Laudos.situacao_id' => 900])
                ->group(['Laudos.assinado_por'])
                ->order(['Laudos.dt_assinatura']);
        }
    }

    public function getLaudoForGroup($id, $type)
    {   
        $laudos = TableRegistry::get('Laudos');
        if ($type === 'GrpLaudoDigitado' ) {
            return $laudos->find('all')
                ->contain(['AtendimentoProcedimentos' => ['Atendimentos', 'Procedimentos' => ['GrupoProcedimentos']], 'Users'])
                ->where(['Laudos.situacao_id' => 500])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1])
                ->andWhere(['GrupoProcedimentos.id' => $id])
                ->group(['Laudos.user_id'])
                ->order(['Laudos.created']);
        } else {
            return $laudos->find('all')
                ->contain(['AtendimentoProcedimentos' => ['Atendimentos', 'Procedimentos' => ['GrupoProcedimentos']], 'UsersAssinado'])
                ->where(['Laudos.situacao_id' => 900])
                ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1])
                ->andWhere(['GrupoProcedimentos.id' => $id])
                ->group(['Laudos.assinado_por'])
                ->order(['Laudos.dt_assinatura']);
        }
    }
    
    public function getLaudosForGroup($type, $id)
    {   
        $laudos = TableRegistry::get('Laudos');
        if ($type === 'LaudosDigitados' ) {
            return $laudos->find('all')
                ->contain(['AtendimentoProcedimentos' => ['Atendimentos', 'Procedimentos' => ['GrupoProcedimentos']], 'Users'])
                ->where(['Laudos.situacao_id' => 500])
                ->andWhere(['GrupoProcedimentos.id' => $id])
                ->group(['Laudos.user_id'])
                ->order(['Laudos.created']);
        } else {
            return $laudos->find('all')
                ->contain(['AtendimentoProcedimentos' => ['Atendimentos', 'Procedimentos' => ['GrupoProcedimentos']], 'UsersAssinado'])
                ->where(['Laudos.situacao_id' => 900])
                ->andWhere(['GrupoProcedimentos.id' => $id])
                ->group(['Laudos.assinado_por'])
                ->order(['Laudos.dt_assinatura']);
        }
    }

    //Método para fazer o filtro dos anos
    public function filtroAnos($anos, $query, $type)
    {   
        if ($type && $type === 'Agendas') {
            $query->andWhere(['YEAR(Agendas.created) IN' => $anos]);
        } else if ($type && $type === 'LaudosDigitados') {
            $query->andWhere(['YEAR(Laudos.created) IN' => $anos]);
        } else if ($type && $type === 'LaudosAssinados') {
            $query->andWhere(['YEAR(Laudos.dt_assinatura) IN' => $anos]);
        } else {
            $query->andWhere(['YEAR(Atendimentos.data) IN' => $anos]);
        }
        return $query;
    }

    //Método para fazer o filtro da unidade
    public function filtroUnidades($unidade, $query)
    {   
        $query->andWhere(['Atendimentos.unidade_id' => $unidade]);
        return $query;
    }

    //Método para fazer o filtro de convenios
    public function filtroConvenios($convenios, $query, $type)
    {   
        if ($type && $type == 'Agendas') {
            $query->andWhere(['Agendas.convenio_id IN' => $convenios]);
        } else {
            $query->andWhere(['Atendimentos.convenio_id IN' => $convenios]);
        }
        return $query;
    }

    //Método para fazer o filtro dos grupos
    public function filtroGrupos($grupos, $query)
    {
        $query->andWhere(['GrupoProcedimentos.id IN' => $grupos]);
        return $query;
    }

    //Método para montar a query com os totais dos meses de cada procedimento (Quantidade)
    public function totaisMeses($query, $type)
    {   
        if ($type && $type == 'Agendas') {
            $meses = $this->getMeses();
            for ($i = 1; $i <= 12; $i++) {
                $qtdMes = $query->newExpr()
                    ->addCase(
                        $query->newExpr()->add(['MONTH(Agendas.created)' => $i])
                    );
                $querySelect = $query->select([
                    $meses[$i] => $query->func()->sum($qtdMes)
                ]);
            }
        } else {
            $meses = $this->getMeses();
            for ($i = 1; $i <= 12; $i++) {
                $qtdMes = $query->newExpr()
                    ->addCase(
                        $query->newExpr()->add(['MONTH(Atendimentos.data)' => $i]),
                        [new IdentifierExpression('(AtendimentoProcedimentos.quantidade)')]
                    );

                $querySelect = $query->select([
                    $meses[$i] => $query->func()->sum($qtdMes)
                ]);
            }
        }
        return $querySelect;
    }

    //Método para montar a query com os totais dos meses de cada procedimento (Valor)
    public function totaisMesesValores($query)
    {   
        $meses = $this->getMeses();

        for ($i = 1; $i <= 12; $i++) {
            $qtdMes = $query->newExpr()
                ->addCase(
                    $query->newExpr()
                        ->add(['MONTH(Atendimentos.data)' => $i]),
                    [new IdentifierExpression('AtendimentoProcedimentos.valor_caixa + AtendimentoProcedimentos.total')]
                );

            $querySelect = $query->select([
                $meses[$i] => $query->func()->sum($qtdMes)
            ]);
        }
        return $querySelect;
    }

    //Método para fazer a média dos procedimentos, seja valor ou quantidade
    public function mediaProcedimento($q)
    {
        $meses = $this->getMeses();
        $media = 0.00;
        $aux = 0;
        foreach ($meses as $mes) {
            if ($q->$mes != null) {
                $aux++;
                $media += $q->$mes;
            }
        }
        if ($aux == 0) {
            $media = 0.00;
        } else {
            $media = ($media /$aux);
        }
        return ($media);
    }

    //Método para fazer a soma dos meses, seja valor ou quantidade
    public function somaMes($t)
    {
        $meses = $this->getMeses();
        foreach ($meses as $mes) {
            $soma[$mes] = 0;
            foreach ($t as $grupo) {
                $soma[$mes] += $grupo->$mes;
            }
        }
        return $soma;
    }

    //Método para calcular o total da somos de todos os meses por grupo
    public function totalSomaMes($grupoSomas){
        $i = 0;
        foreach ($grupoSomas as $somas){
            $totalSoma[$i] = 0;
            foreach ($somas as $soma){
                $totalSoma[$i] += $soma;
            }
            $i ++;
        }
        return $totalSoma;
    }

    public function nomeTabela($nomeTabela, $ano, $tipo_resultado){
        if ($tipo_resultado == 1) {
            $nomeTabela = $nomeTabela . ' / ' . 'Quantidade';
        } else {
            $nomeTabela = $nomeTabela . ' / ' . 'Valor';
        }
        foreach ($ano as $a) {
            $nomeTabela = $nomeTabela . ' / ' . $a;
        }

        return $nomeTabela;
    }
}