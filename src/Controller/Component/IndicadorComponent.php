<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class IndicadorComponent extends Component
{
    public $components = array('Data');

    public function atendimentoProcedimento()
    {
        $atendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        return $atendimentoProcedimentos;
    }
    public function grupoProcedimento()
    {
        $grupo = TableRegistry::get('GrupoProcedimentos');
        return $grupo;
    }
    public function atendimentos()
    {
        $atendimento = TableRegistry::get('Atendimentos');
        return $atendimento;
    }

    public function searchAtendimentoPerMonth($mes, $codigo_medico, $grupo_id)
    {
        $query = $this->atendimentoProcedimento()->find('all')
            ->select(['quantidade'=>'sum(AtendimentoProcedimentos.quantidade)'])
            ->contain(['Procedimentos', 'Atendimentos'])
            ->group('Atendimentos.id')
            ->where(['MONTH(Atendimentos.data)' => $mes])
            ->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.grupo_id' => $grupo_id]);

            if(!empty($codigo_medico)){
                $query->andWhere(['AtendimentoProcedimentos.medico_id' => $codigo_medico]);
            }
        $query = $query->toArray();
        $quantidade = (!empty($query[0]->quantidade)) ? $query[0]->quantidade : 0;
        return $quantidade;
    }

    public function searchValoresPerMonth($mes, $codigo_medico, $grupo_id)
    {
        $query = $this->atendimentoProcedimento()->find('all')
            ->select([
                'valor_caixa'=>'sum(AtendimentoProcedimentos.valor_caixa)',
                'total'=>'sum(AtendimentoProcedimentos.total)',
                'valor_material'=>'sum(AtendimentoProcedimentos.valor_material)',
            ])
            ->contain(['Procedimentos', 'Atendimentos'])
            ->where(['MONTH(Atendimentos.data)' => $mes])
            /*->andWhere(['Atendimentos.situacao_id' => 1])
            ->andWhere(['Procedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1])*/
            ->andWhere(['Procedimentos.grupo_id' => $grupo_id]);

        if(!empty($codigo_medico)){
            $query->andWhere(['AtendimentoProcedimentos.medico_id' => $codigo_medico]);
        }

        $query_select = $query->select([
            'valor_caixa' => $query->func()->sum('valor_caixa'),
            'total' => $query->func()->sum('total'),
            'valor_material' => $query->func()->sum('valor_material'),
        ])->toArray();

        $valor_caixa = ($query_select[0]->valor_caixa != null) ? $query_select[0]->valor_caixa : 0;
        $total = ($query_select[0]->total != null) ? $query_select[0]->total : 0;
        $valor_material = ($query_select[0]->valor_material != null) ? $query_select[0]->valor_material : 0;
        $resultado = number_format($valor_caixa + $total + $valor_material, 2, '.', '');
        return $resultado;
    }

    public function searchAgendamentoPerMonth($mes, $codigo_agenda)
    {
        $Agendas = TableRegistry::get('Agendas');
        $query = $Agendas->find('all')
            ->where(['Agendas.situacao_id' => 1, 'MONTH(Agendas.inicio)' => $mes])
            ->andWhere(['Agendas.situacao_agenda_id' => 10]);// Atendidos

        if(!empty($codigo_agenda)){
            $query->andWhere(['Agendas.grupo_id' => $codigo_agenda]);
        }

        return $query->count();
    }

    public function getProcedimentosPerGrupo($mes, $ano)
    {
        $grupos = $this->grupoProcedimento()->find('all')->where(['GrupoProcedimentos.situacao_id' => 1]);
        $dataSets = [];
        foreach ($grupos as $g){
            $grupo_proc = [];
            $qtd = [];
            foreach ($this->Data->getDaysMonth($ano, $mes) as $day){
                $query = $this->atendimentoProcedimento()->find('all')
                    ->select(['quantidade' => 'sum(AtendimentoProcedimentos.quantidade)'])
                    ->contain(['Procedimentos', 'Atendimentos'])
                    ->where(['AtendimentoProcedimentos.situacao_id' => 1])
                    ->andWhere(['DAY(Atendimentos.data)' => $day])
                    ->andWhere(['MONTH(Atendimentos.data)' => $mes])
                    ->andWhere(['YEAR(Atendimentos.data)' => $ano])
                    ->andWhere(['Atendimentos.situacao_id' => 1])
                    ->andWhere(['Procedimentos.situacao_id' => 1])
                    ->andWhere(['Procedimentos.grupo_id' => $g->id]);

                $query = $query->toArray();

                $qtd[] = (!empty($query[0]->quantidade)) ? $query[0]->quantidade : 0;
            }
            $grupo_proc['data'] = $qtd;
            $grupo_proc['label'] = $g->nome;
            $grupo_proc['fill'] = false;
            $grupo_proc['borderColor'] = $color = '#'.substr(md5(rand()), 0, 6);
            $dataSets[] = $grupo_proc;
        }
        return $dataSets;
    }

    public function getAtendimentosPerGrupo($mes, $ano)
    {
        $grupos = $this->grupoProcedimento()->find('all')->where(['GrupoProcedimentos.situacao_id' => 1]);
        $dataSets = [];
        foreach ($grupos as $g){
            $grupo_proc = [];
            $qtd = [];
            foreach ($this->Data->getDaysMonth($ano, $mes) as $day){
                $query = $this->atendimentoProcedimento()->find('all')
                    ->group('Atendimentos.id')
                    ->contain(['Procedimentos', 'Atendimentos'])
                    ->where(['AtendimentoProcedimentos.situacao_id' => 1])
                    ->andWhere(['DAY(Atendimentos.data)' => $day])
                    ->andWhere(['MONTH(Atendimentos.data)' => $mes])
                    ->andWhere(['YEAR(Atendimentos.data)' => $ano])
                    ->andWhere(['Atendimentos.situacao_id' => 1])
                    ->andWhere(['Procedimentos.situacao_id' => 1])
                    ->andWhere(['Procedimentos.grupo_id' => $g->id]);

                $qtd[] = $query->count();
            }
            $grupo_proc['data'] = $qtd;
            $grupo_proc['label'] = $g->nome;
            $grupo_proc['fill'] = false;
            $grupo_proc['borderColor'] = $color = '#'.substr(md5(rand()), 0, 6);
            $dataSets[] = $grupo_proc;
        }
        return $dataSets;

    }

}