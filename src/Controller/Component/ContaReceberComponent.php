<?php
/**
 * Created by PhpStorm.
 * User: Iohan
 * Date: 04/07/2017
 * Time: 11:20
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use DateTime;

class ContaReceberComponent extends Component
{

    public $components = ['Data'];

    public function filtroContasReceber($query, $request)
    {
        $query->andWhere(['FinContasReceber.situacao' => 1]);

        if (!empty($request['inicio'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $request['inicio']);
            $query->andWhere(['FinContasReceber.vencimento >=' => $now->format('Y-m-d')]);
        } else {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', date('01/m/Y'));
            $query->andWhere(['FinContasReceber.vencimento >=' => $now->format('Y-m-d')]);
        }

        if (!empty($request['fim'])) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $request['fim']);
            $query->andWhere(['FinContasReceber.vencimento <=' => $now->format('Y-m-d')]);
        } else {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', date('t/m/Y'));
            $query->andWhere(['FinContasReceber.vencimento <=' => $now->format('Y-m-d')]);
        }

        if (!empty($request['planoconta'])) {
            $query->andWhere(['FinContasReceber.fin_plano_conta_id IN' => $request['planoconta']]);
        }

        if (!empty($request['cliente'])) {
            $query->andWhere(['FinContasReceber.cliente_id IN' => $request['cliente']]);
        }

        if (!empty($request['contabilidade'])) {
            $query->andWhere(['FinContasReceber.fin_contabilidade_id IN' => $request['contabilidade']]);
        }

        if (!empty($request['status_conta'])) {
            if ($request['status_conta'] == 1) {
                $query->andWhere(['FinContasReceber.data_pagamento IS NOT NULL']);
            } else if ($request['status_conta'] == 2) {
                $query->andWHere(['FinContasReceber.data_pagamento IS NULL']);
            }
        }

        return $query;
    }
}