<?php
namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Mailer\Email;
use Cake\I18n\Time;
class IcsComponent extends Component {
    const DIRECTORY_FILES_UPLOAD = 'anexos/';
    public function path(){
        $path = dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."anexos". DS ."ics";
        $dir = new Folder($path);
        if(!$dir->path){
            $dir->create($path,0755);
        }
        return $dir->path;
    }

    public function create($file_name,$data){
        $path = $this->path();
        $file = new File($path.DS.$file_name,true);

        if($file->writable()){
            $file->write($data);
            $file->close();
        }

        return dirname(dirname(dirname(dirname(__FILE__)))) . DS . "webroot" . DS ."anexos". DS ."ics".DS.$file_name;
    }

    public function dateToCalTime($data) {
        $data = new Time($data);
        $now = $data->format('Y-m-d H:i:s');
        return date('Ymd\THis', strtotime($now));
    }

    public function dateToEnd($data) {
        $data = new Time($data);
        $data->addHour(1);
        $now = $data->format('Y-m-d H:i:s');
        return date('Ymd\THis', strtotime($now));
    }

    public function getUid($id){
        return time().$id.time().$id.time();
    }

    public function dateToCal($data) {
        $data = new Time($data);
        $now = $data->format('Y-m-d H:i:s');
        return date('Ymd\Tgis\Z', strtotime($now));
    }

    public function send($dados, $to = array()){

        $fim = empty($dados->end) ? $this->dateToEnd($dados->start) : $this->dateToCalTime($dados->end);
$load="BEGIN:VCALENDAR
PRODID:-//G2i Medical//G2i 1.0 MIMEDIR//EN
VERSION:2.0
METHOD:REQUEST
X-MS-OLK-FORCEINSPECTOROPEN:TRUE
BEGIN:VTIMEZONE
TZID:Central Brazilian Standard Time
BEGIN:STANDARD
DTSTART:".$this->dateToCalTime($dados->start)."
TZOFFSETFROM:-0300
TZOFFSETTO:-0300
END:STANDARD
END:VTIMEZONE
BEGIN:VEVENT
CLASS:PUBLIC
CREATED:".$this->dateToCal(date('Y-m-d H:i:s'))."
DESCRIPTION:".$dados->descricao."\n
DTEND;TZID=\"Central Brazilian Standard Time\":".$this->dateToCalTime($dados->start)."
DTSTAMP:".$this->dateToCal(date('Y-m-d H:i:s'))."
DTSTART;TZID=\"Central Brazilian Standard Time\":".$fim."
LAST-MODIFIED:".$this->dateToCal(date('Y-m-d H:i:s'))."
LOCATION:".$dados->local."
ORGANIZER;CN=contato@g2isolucoes.com.br:mailto:contato@g2isolucoes.com.br
PRIORITY:5
SEQUENCE:0
SUMMARY;LANGUAGE=pt-br:".$dados->titulo."
TRANSP:OPAQUE
UID:".$this->getUid($dados->id)."
X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//E
	N\">\n<HTML>\n<HEAD>\n<META NAME=\"Generator\" CONTENT=\"MS Exchange Server ve
	rsion rmj.rmm.rup.rpr\">\n<TITLE></TITLE>\n</HEAD>\n<BODY>\n<!-- Converted 
	from text/rtf format -->\n\n<P DIR=LTR><SPAN LANG=\"pt-br\"><FONT FACE=\"Cali
	bri\">teste</FONT></SPAN><SPAN LANG=\"pt-br\"></SPAN></P>\n\n</BODY>\n</HTML>
X-MICROSOFT-CDO-BUSYSTATUS:TENTATIVE
X-MICROSOFT-CDO-IMPORTANCE:1
X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY
X-MICROSOFT-DISALLOW-COUNTER:FALSE
X-MS-OLK-AUTOSTARTCHECK:FALSE
X-MS-OLK-CONFTYPE:0
BEGIN:VALARM
TRIGGER:-PT15M
ACTION:DISPLAY
DESCRIPTION:Reminder
END:VALARM
END:VEVENT
END:VCALENDAR";

        $filename="invite_".$dados->model.$dados->id.".ics";
        $file = $this->create($filename,$load);

        $email = new Email();
        $mensagem = "";
        $mensagem.="";

        $email->attachments([
            'invite_cn.ics' => [
                'file' => $file,
                'mimetype' => 'text/calendar',
                'contentId' => $this->getUid($dados->id)
            ]
        ],[
            'invite_.ics' => [
                'file' => $file,
                'mimetype' => 'text/calendar',
                'contentId' =>$this->getUid($dados->id)
            ]
        ]);


        $email->from(['contato@g2isolucoes.com.br' => 'G2i Medical'])
            ->domain('http://g2isolucoes.com.br/iea/prontuario/')
            ->emailFormat('html')
            ->template('invite')
            ->subject($dados->titulo);

        $aux=1;
        foreach ($to as $item) {
            if($aux<=1) {
                $email->to($item['email'], $item['nome']);
            }else{
                $email->cc($item['email'], $item['nome']);
            }
            $aux++;
        }

        $email->send($mensagem);



    }
}