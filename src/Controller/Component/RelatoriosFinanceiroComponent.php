<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use DateTime;

class RelatoriosFinanceiroComponent extends Component
{

    public function getRelatoriosContasPagar()
    {
        return [
            1 => 'Relatório Geral',
            2 => 'Relatório por Vencimento'
        ];
    }

    public function getRelatoriosMovimentos()
    {
        return [
//            1 => 'Relatório Geral',
            2 => 'Relatório de Movimentações'
        ];
    }

    public function getRelatoriosMovimentosGeral()
    {
        return [
            1 => 'Relatório Geral',
            3 => 'Demonstrativo Resultado - Analítico',
            4 => 'Demonstrativo Resultado - Analítico e %Part.',
            2 => 'Demonstrativo Resultado - Sintético',
            5 => 'Demonstrativo Resultado - Sintético e %Part.'
        ];
    }

}