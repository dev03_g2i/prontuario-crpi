<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/06/2016
 * Time: 09:30
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Session;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use DateTime;

class RelatoriosComponent extends Component
{

    public $components = array('Data', 'Json', 'Configuracao');

    /**
     * This function returns a json for a report according to the parameters passed to it
     * Essa função retorna um json para construcao de relatório de acordo com o parametros passados
     * @param $medico
     * @param $inicio
     * @param $fim
     * @return $json
     * @author Iohan
     */
    public function caixa_executante($medico = null, $inicio = null, $fim = null, $convenio = null,
                                     $periodo = null)
    {
        $model = TableRegistry::get('AtendimentoProcedimentos');
        $atendimento_procedimentos = $model->find('all')
            ->contain([
                'Atendimentos' => [
                    'ContasReceber' => [
                        'strategy' => 'select',
                        'conditions' => ['ContasReceber.status_id' => 1]
                    ],
                    'Clientes',
                    'Convenios',
                    'Users',
                    'ConfiguracaoPeriodos'
                ],

                'Procedimentos',
                'MedicoResponsaveis'
            ]);
        $atendimento_procedimentos->where(['Atendimentos.situacao_id' => 1, 'AtendimentoProcedimentos.situacao_id' => 1]);
        $atendimento_procedimentos->order(['Atendimentos.id ASC']);

        if (!empty($medico))
            $atendimento_procedimentos->andWhere(['AtendimentoProcedimentos.medico_id' => $medico]);
        if (!empty($convenio))
            $atendimento_procedimentos->andWhere(['Atendimentos.convenio_id' => $convenio]);

        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento_procedimentos->andWhere(['Atendimentos.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
        }
        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento_procedimentos->andWhere(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
        }
        if (!empty($periodo))
            $atendimento_procedimentos->andWhere(['Atendimentos.configuracao_periodo_id = ' => $periodo]);

        $aux = -1;
        $json['atendimentos'] = null;
        /**
         * Laço para percorrer AtendimentoProcedimentos
         */
        foreach ($atendimento_procedimentos as $pa) {

            $valor = 0;
            /** Roda o laço para cada ContasReceber existente para esse determinado Atendimento, somando seus valores.
             * Logo após seta o ID do Atendimento no auxiliar para garantir que o mesmo atendimento não seja somado 2 vezes
             */
            if ($pa->atendimento->id != $aux) {
                foreach ($pa->atendimento->contas_receber as $cr)
                    $valor += $cr->valor;
                $aux = $pa->atendimento->id;
            }
            $json['atendimentos'][] = [
                'atendimento' => $pa->atendimento->id,
                'data' => $this->Data->data_sms($pa->atendimento->data),
                'paciente' => $pa->atendimento->cliente->nome,
                'convenio' => $pa->atendimento->convenio->nome,
                'nnf' => empty($pa->atendimento->conta_receber->numfiscal) ? '' : $pa->atendimeto->conta_receber->numfiscal,
                'procedimento' => $pa->procedimento->nome,
                'valor' => empty($pa->valor_caixa) ? 0 : $pa->valor_caixa,
                'caixa' => $valor,
                'executante' => $pa->medico_responsavei->nome,
                'usuario' => $pa->atendimento->user->nome,
                'preco_fatura' => $pa->valor_fatura,
                'quantidade' => $pa->quantidade
            ];
        }
        $json['alias'][] = [
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim
        ];
        return $json;
    }

    public function relatorio_recebimento($medico = null, $inicio = null, $fim = null, $convenio = null, $periodo = null, $user = null, $receita = null, $tituloReport = null)
    {
        $dataSourceFinanceiro = ConnectionManager::get('financeiro');
        $model = TableRegistry::get('Atendimentos');
        $usuarioTable = TableRegistry::get('Users');
        $atendimento = $model->find('all')
            ->contain([
                'ContasReceber' => [
                    'TipoPagamento',
                    'UserReg'
                ],
                'Clientes',
                'Convenios',
                'Users',
                'ConfiguracaoPeriodos',
                'AtendimentoProcedimentos' => [
                    'conditions' => ['AtendimentoProcedimentos.situacao_id' => 1],
                    'Procedimentos',
                    'MedicoResponsaveis'
                ]
            ])
            ->join([
                'table' => $dataSourceFinanceiro->config()['database'] . '.contasreceber',
                'alias' => 'cr',
                'type' => 'RIGHT',
                'conditions' => 'cr.extern_id = Atendimentos.id',
            ])
            ->where(['cr.status_id' => 1])
            ->order('Atendimentos.id ASC');

        $atendimento->andWhere(['Atendimentos.situacao_id' => 1]);
        /*        if (!empty($medico))
                    $atendimento->andWhere(['AtendimentoProcedimentos.medico_id' => $medico]);*/
        if (!empty($convenio))
            $atendimento->andWhere(['Atendimentos.convenio_id' => $convenio]);

        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento->andWhere(['cr.data >=' => $now->format('Y-m-d')]);
        }

        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento->andWhere(['cr.data <=' => $now->format('Y-m-d')]);
        }
        if (!empty($periodo))
            $atendimento->andWhere(['Atendimentos.configuracao_periodo_id = ' => $periodo]);
        if (!empty($user)) {
            $atendimento->andWhere(['cr.user_created' => $user]);
        }

        $json['atendimentos'] = null;
        $aux = 0;
        $auxId = -1;
        $total_recebido_geral = 0;
        $taotal_atendimentos_geral = 0;
        $usuarioContasAtual = -1;
        foreach ($atendimento as $a) {
            $pro = null;
            $cont = null;
            $total = 0.00;
            $total_recebido = 0.00;

            if ($auxId != $a->id) { //Controlador de atendimento
                $auxId = $a->id;
                $aux = 0; //Se for atendimento diferente, reseta o contador de contas
                $usuarioContasAtual = -1; //Se for atendimento diferente, reseta o controlador de usuário contas
            }
            if ($usuarioContasAtual != $a->contas_receber[$aux]->user_reg->id) { //Controlador de usuário contas
                $usuarioContasAtual = $a->contas_receber[$aux]->user_reg->id;
                if (!empty($a->atendimento_procedimentos)) {
                    foreach ($a->atendimento_procedimentos as $pa) {
                        $pro[] = [
                            'procedimento' => $pa->procedimento->nome,
                            'valor' => empty($pa->valor_caixa) ? 0 : $pa->valor_caixa,
                            'executante' => $pa->medico_responsavei->nome,
                            'preco_fatura' => $pa->valor_fatura,
                            'quantidade' => $pa->quantidade
                        ];
                        $total += (float)empty($pa->valor_caixa) ? 0.00 : (float)$pa->valor_caixa;
                    }
                    if (!empty($a->contas_receber)) {
                        foreach ($a->contas_receber as $cr) {
                            if ($cr->user_created == $user || (empty($user) && $cr->user_created == $a->contas_receber[$aux]->user_created)) {
                                $cont[] = [
                                    'pagamento' => $cr->tipo_pagamento->descricao,
                                    'valor' => $cr->valor,
                                    'parcela' => $cr->parcela,
                                    'user' => $cr->user_reg->nome
                                ];
                                $total_recebido += $cr->valor;
                            }
                        }
                    } else {
                        $cont[] = [
                            'pagamento' => 'Sem recebimento',
                            'valor' => 0.00,
                            'parcela' => 0.00,
                            'user' => 'Sem responsável'
                        ];
                    }
                    if ($total != 0) {
                        //'usuario' => $a->user->nome
                        if (!empty($user)) {
                            $usuario = $usuarioTable->get($user);
                            $usuarioContas = $usuario->nome;
                        } else {
                            $usuarioContas = $a->contas_receber[$aux]->user_reg->nome;
                        }

                        if (!empty($cont) || !$receita) {
                            $json['atendimentos'][] = [
                                'procedimentos' => $pro,
                                'contas_receber' => $cont,
                                'atendimento' => $a->id,
                                'data' => $this->Data->data_sms($a->data),
                                'paciente' => $a->cliente->nome,
                                'convenio' => $a->convenio->nome,
                                'nnf' => empty($a->conta_receber->numfiscal) ? '' : $a->conta_receber->numfiscal,
                                'usuario' => $usuarioContas,
                                'total' => $total,
                                'total_recebido' => $total_recebido
                            ];
                        }
                    }

                }
            }

            $total_recebido_geral += $total_recebido;
            $taotal_atendimentos_geral += $total;
            $aux++;
        }

        $json['alias'][] = [
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim,
            'titulo_report' => $tituloReport,
            'total_recebido_geral' => $total_recebido_geral,
            'total_atendimentos_geral' => $taotal_atendimentos_geral
        ];
        return $json;
    }


    public function relatorio_recebimento_individual($medico = null, $inicio = null, $fim = null, $convenio = null, $periodo = null, $user = null, $receita = null, $tituloReport = null)
    {
        $dataSourceFinanceiro = ConnectionManager::get('financeiro');
        $model = TableRegistry::get('Atendimentos');
        $usuarioTable = TableRegistry::get('Users');
        $atendimento = $model->find('all')
            ->contain([
                'ContasReceber' => [
                    'TipoPagamento',
                    'UserReg'
                ],
                'Clientes',
                'Convenios',
                'Users',
                'ConfiguracaoPeriodos',
                'AtendimentoProcedimentos' => [
                    'conditions' => ['AtendimentoProcedimentos.situacao_id' => 1],
                    'Procedimentos',
                    'MedicoResponsaveis'
                ]
            ])
            ->join([
                'table' => $dataSourceFinanceiro->config()['database'] . '.contasreceber',
                'alias' => 'cr',
                'type' => 'RIGHT',
                'conditions' => 'cr.extern_id = Atendimentos.id',
            ])
            ->where(['cr.status_id' => 1])
            ->order('Atendimentos.id ASC');

        $atendimento->andWhere(['Atendimentos.situacao_id' => 1]);
        /*        if (!empty($medico))
                    $atendimento->andWhere(['AtendimentoProcedimentos.medico_id' => $medico]);*/
        if (!empty($convenio))
            $atendimento->andWhere(['Atendimentos.convenio_id' => $convenio]);

        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento->andWhere(['cr.data >=' => $now->format('Y-m-d')]);
        }

        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento->andWhere(['cr.data <=' => $now->format('Y-m-d')]);
        }
        if (!empty($periodo))
            $atendimento->andWhere(['Atendimentos.configuracao_periodo_id = ' => $periodo]);
        if (!empty($user)) {
            $atendimento->andWhere(['cr.user_created' => $user]);
        }

        $json['atendimentos'] = null;
        $aux = 0;
        $auxId = -1;
        $total_recebido_geral = 0;
        $taotal_atendimentos_geral = 0;
        foreach ($atendimento as $a) {
            $pro = null;
            $cont = null;
            $total = 0.00;
            $total_recebido = 0.00;

            if ($auxId != $a->id) {
                $auxId = $a->id;
                $aux = 0;


                if (!empty($a->atendimento_procedimentos)) {

                    foreach ($a->atendimento_procedimentos as $pa) {
                        $pro[] = [
                            'procedimento' => $pa->procedimento->nome,
                            'valor' => empty($pa->valor_caixa) ? 0 : $pa->valor_caixa,
                            'executante' => $pa->medico_responsavei->nome,
                            'preco_fatura' => $pa->valor_fatura,
                            'quantidade' => $pa->quantidade
                        ];
                        $total += (float)empty($pa->valor_caixa) ? 0.00 : (float)$pa->valor_caixa;
                    }
                    if (!empty($a->contas_receber)) {
                        foreach ($a->contas_receber as $cr) {
                            if ($cr->user_created == $user || (empty($user) && $cr->user_created == $a->contas_receber[$aux]->user_created)) {
                                $cont[] = [
                                    'pagamento' => $cr->tipo_pagamento->descricao,
                                    'valor' => $cr->valor,
                                    'parcela' => $cr->parcela,
                                    'user' => $cr->user_reg->nome
                                ];
                                $total_recebido += $cr->valor;
                            }
                        }
                    } else {
                        $cont[] = [
                            'pagamento' => 'Sem recebimento',
                            'valor' => 0.00,
                            'parcela' => 0.00,
                            'user' => 'Sem responsável'
                        ];
                    }
                    if ($total != 0) {
                        //'usuario' => $a->user->nome
                        if (!empty($user)) {
                            $usuario = $usuarioTable->get($user);
                            $usuarioContas = $usuario->nome;
                        } else {
                            $usuarioContas = $a->contas_receber[$aux]->user_reg->nome;
                        }

                        if (!empty($cont) || !$receita) {
                            $json['atendimentos'][] = [
                                'procedimentos' => $pro,
                                'contas_receber' => $cont,
                                'atendimento' => $a->id,
                                'data' => $this->Data->data_sms($a->data),
                                'paciente' => $a->cliente->nome,
                                'convenio' => $a->convenio->nome,
                                'nnf' => empty($a->conta_receber->numfiscal) ? '' : $a->conta_receber->numfiscal,
                                'usuario' => $usuarioContas,
                                'total' => $total,
                                'total_recebido' => $total_recebido
                            ];
                        }
                    }

                }
            }
            $total_recebido_geral += $total_recebido;
            $taotal_atendimentos_geral += $total;
            $aux++;
        }

        $json['alias'][] = [
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim,
            'titulo_report' => $tituloReport,
            'total_recebido_geral' => $total_recebido_geral,
            'total_atendimentos_geral' => $taotal_atendimentos_geral
        ];
        return $json;
    }


    public function caixa_analitico_geral($medico = null, $inicio = null, $fim = null, $convenio = null, $periodo = null, $user = null, $receita = null, $tituloReport = null)
    {
        $model = TableRegistry::get('Atendimentos');
        $atendimento = $model->find('all')
            ->contain([
                'ContasReceber' => [
                    'TipoPagamento',
                    'UserReg'
                ],
                'Clientes',
                'Convenios',
                'Users',
                'ConfiguracaoPeriodos',
                'AtendimentoProcedimentos' => [
                    'conditions' => ['AtendimentoProcedimentos.situacao_id' => 1],
                    'Procedimentos',
                    'MedicoResponsaveis'
                ]
            ]);
        $atendimento->where(['Atendimentos.situacao_id' => 1]);
        /*        if (!empty($medico))
                    $atendimento->andWhere(['AtendimentoProcedimentos.medico_id' => $medico]);*/
        if (!empty($convenio))
            $atendimento->andWhere(['Atendimentos.convenio_id' => $convenio]);

        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento->andWhere(['Atendimentos.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
        }

        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento->andWhere(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
        }
        if (!empty($periodo))
            $atendimento->andWhere(['Atendimentos.configuracao_periodo_id = ' => $periodo]);
        if (!empty($user)) {
//            $user_model = TableRegistry::get('Users');
//            $user_logado = $user_model->get($user);
            $atendimento->andWhere(['Atendimentos.user_id =' => $user]);
        }

        $json['atendimentos'] = null;
        $total_recebido_geral = 0;
        $taotal_atendimentos_geral = 0;
        foreach ($atendimento as $a) {
            $pro = null;
            $cont = null;
            $total = 0.00;
            $total_recebido = 0.00;
            if (!empty($a->atendimento_procedimentos)) {
                foreach ($a->atendimento_procedimentos as $pa) {
                    $pro[] = [
                        'procedimento' => $pa->procedimento->nome,
                        'valor' => empty($pa->valor_caixa) ? 0 : $pa->valor_caixa,
                        'executante' => $pa->medico_responsavei->nome,
                        'preco_fatura' => $pa->valor_fatura,
                        'quantidade' => $pa->quantidade
                    ];
                    $total += (float)empty($pa->valor_caixa) ? 0.00 : (float)$pa->valor_caixa;
                }
                if (!empty($a->contas_receber)) {
                    foreach ($a->contas_receber as $cr) {
                        $cont[] = [
                            'pagamento' => $cr->tipo_pagamento->descricao,
                            'valor' => $cr->valor,
                            'parcela' => $cr->parcela,
                            'user' => $cr->user_reg->nome
                        ];
                        $total_recebido += $cr->valor;
                    }
                } else {
                    $cont[] = [
                        'pagamento' => 'Sem recebimento',
                        'valor' => 0.00,
                        'parcela' => 0.00,
                        'user' => 'Sem responsável'
                    ];
                }
                if ($total != 0) {
                    if (!empty($cont) || !$receita)
                        $json['atendimentos'][] = [
                            'procedimentos' => $pro,
                            'contas_receber' => $cont,
                            'atendimento' => $a->id,
                            'data' => $this->Data->data_sms($a->data),
                            'paciente' => $a->cliente->nome,
                            'convenio' => $a->convenio->nome,
                            'nnf' => empty($a->conta_receber->numfiscal) ? '' : $a->conta_receber->numfiscal,
                            'usuario' => $a->user->nome,
                            'total' => $total,
                            'total_recebido' => $total_recebido
                        ];
                }
            }
            $total_recebido_geral += $total_recebido;
            $taotal_atendimentos_geral += $total;
        }

        $json['alias'][] = [
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim,
            'titulo_report' => $tituloReport,
            'total_recebido_geral' => $total_recebido_geral,
            'total_atendimentos_geral' => $taotal_atendimentos_geral
        ];

        return $json;
    }

    public function caixa_devedor($inicio = null, $fim = null, $user = null, $periodo = null, $nomeRelatorio)
    {
        $model = TableRegistry::get('Atendimentos');
        $atendimento = $model->find('all')
            ->contain([
                'ContasReceber' => [
                    'TipoPagamento',
                    'UserReg',
                    'strategy' => 'select',
                    'conditions' => ['ContasReceber.status_id' => 1]
                ],
                'Clientes',
                'Convenios',
                'Users',
                'ConfiguracaoPeriodos',
                'AtendimentoProcedimentos' => [
                    'conditions' => ['AtendimentoProcedimentos.situacao_id' => 1],
                    'Procedimentos',
                    'MedicoResponsaveis'
                ]
            ]);
        $atendimento->where(['Atendimentos.situacao_id' => 1]);
        if (!empty($convenio)) {
            $atendimento->andWhere(['Atendimentos.convenio_id' => $convenio]);
        }
        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento->andWhere(['Atendimentos.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
        }
        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento->andWhere(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
        }
        if (!empty($periodo))
            $atendimento->andWhere(['Atendimentos.configuracao_periodo_id = ' => $periodo]);
        if (!empty($user)) {
            $atendimento->andWhere(['Atendimentos.user_id =' => $user]);
        }
        $json['atendimentos'] = null;
        $totalRelatorio = 0;
        foreach ($atendimento as $a) {
            $pro = null;
            $cont = null;
            $total = 0;
            $proc = 0;
            foreach ($a->atendimento_procedimentos as $pa) {
                $pro[] = [
                    'procedimento' => $pa->procedimento->nome,
                    'valor' => empty($pa->valor_caixa) ? 0 : $pa->valor_caixa,
                    'executante' => $pa->medico_responsavei->nome,
                    'preco_fatura' => $pa->valor_fatura,
                    'quantidade' => $pa->quantidade
                ];
                $proc += empty($pa->valor_caixa) ? 0 : $pa->valor_caixa;
            }
            foreach ($a->contas_receber as $cr) {
                $cont[] = [
                    'pagamento' => $cr->tipo_pagamento->descricao,
                    'valor' => $cr->valor,
                    'parcela' => $cr->parcela,
                    'user' => $cr->user_reg->nome
                ];
                $total += $cr->valor;
            }
            if ($a->TotalAreceber > 0) {
                //if (empty($cont) && $proc > 0) { //Condição Feita para pegar somente os atendimentos que não tem um registro de contas a receber e também para procedimento cujo o valor é maior que 0
                $json['atendimentos'][] = [
                    'procedimentos' => $pro,
                    'contas_receber' => $cont,
                    'atendimento' => $a->id,
                    'data' => $this->Data->data_sms($a->data),
                    'paciente' => $a->cliente->nome,
                    'convenio' => $a->convenio->nome,
                    'nnf' => empty($a->conta_receber->numfiscal) ? '' : $a->conta_receber->numfiscal,
                    'usuario' => $a->user->nome,
                    'total' => $proc
                ];
                $totalRelatorio += $proc;
            }
        }
        $json['alias'][] = [
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim,
            'total_relatorio' => $totalRelatorio,
            'nome_relatorio' => $nomeRelatorio
        ];
        return $json;
    }

    /**
     * This function returns @class AtendimentoProcedimentos according to the parameters passed to it
     * Essa função foi criada para buscar os @class AtendimentoProcedimentos conforme os filtros passados
     * @param $medico
     * @param $inicio
     * @param $fim
     * @return $atendimento_procedimentos
     * @author Iohan
     */
    public function atendimento_procedimentos($medico = null, $inicio = null, $fim = null, $convenio = null, $periodo = null)
    {
        $model = TableRegistry::get('AtendimentoProcedimentos');
        $atendimento_procedimentos = $model->find('all')
            ->contain([
                'Atendimentos' => [
                    'ContasReceber' => [
                        'strategy' => 'select',
                        'conditions' => ['ContasReceber.status_id' => 1]
                    ],
                    'Clientes',
                    'Convenios',
                    'Users',
                    'ConfiguracaoPeriodos'
                ],

                'Procedimentos',
                'MedicoResponsaveis'
            ]);
        $atendimento_procedimentos->where(['Atendimentos.situacao_id' => 1, 'AtendimentoProcedimentos.situacao_id' => 1]);

        if (!empty($medico))
            $atendimento_procedimentos->andWhere(['AtendimentoProcedimentos.medico_id IN' => $medico]);
        if (!empty($convenio))
            $atendimento_procedimentos->andWhere(['Atendimentos.convenio_id IN' => $convenio]);

        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento_procedimentos->andWhere(['Atendimentos.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
        }

        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento_procedimentos->andWhere(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
        } else
            $atendimento_procedimentos->andWhere(['Atendimentos.configuracao_periodo_id = ' => $periodo]);
        return $atendimento_procedimentos;
    }

    public function fatura($id)
    {
        $atendimento_procedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendimento_procedimentos = $atendimento_procedimentos->find('all')
            ->contain(['Atendimentos' => ['Convenios', 'Clientes'], 'MedicoResponsaveis', 'SituacaoRecebimentos', 'FaturaEncerramentos', 'Procedimentos' => ['GrupoProcedimentos']])
            ->where(['AtendimentoProcedimentos.fatura_encerramento_id ' => $id, 'AtendimentoProcedimentos.situacao_id' => 1]);
        $json['procedimentos'][] = null;
        $situacao_recebimento = TableRegistry::get('situacao_recebimentos');
        foreach ($atendimento_procedimentos as $a) {
            $situaca_matmed = null;
            if (!empty($a->situacao_recmatmed_id)) {
                $situaca_matmed = $situacao_recebimento->get($a->situacao_recmatmed_id);
                $situaca_matmed = $situaca_matmed->nome;
            }
            $json['procedimentos'][] = [
                'fatura' => $id,
                'atendimento' => $a->atendimento->id,
                'paciente' => $a->atendimento->cliente->nome,
                'data_atendimetno' => $this->Data->data_sms($a->atendimento->created),
                'procedimento' => $a->procedimento->nome,
                'codigo' => $a->codigo,
                'qnt' => $a->quantidade,
                'valor_fatura' => $a->valor_fatura,
                'dt_rec_fatura' => empty($a->dt_recebimento) ? '' : $this->Data->data_sms($a->dt_recebimento),
                'valor_rec_fatura' => $a->valor_caixa,
                'valor_caixa' => $a->valor_caixa,
                'valor_matmed' => $a->valor_matmed,
                'data_recebimento_matmed' => empty($a->data_matmed) ? '' : $this->Data->data_sms($a->data_matmed),
                'situacao_matmed' => empty($situaca_matmed) ? '' : $situaca_matmed,
                'situacao_fatura' => empty($a->situacao_recebimento->nome) ? '' : $a->situacao_recebimento->nome,
                'valor_rec_matmed' => $a->valor_rec_matmed,
                'convenio' => $a->atendimento->convenio->nome,
                'competencia' => $a->fatura_encerramento->competencia
            ];
        }
        return $json;
    }

    public function produtividade_solicitante($solicitante = null, $convenios = null, $inicio, $fim)
    {
        $now = new DateTime();
        $inicio = $now->createFromFormat('d/m/Y', $inicio);
        $fim = $now->createFromFormat('d/m/Y', $fim);
        $ProdutividadeSolicitante = TableRegistry::get('ProdutividadeSolicitante');
        $AtendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $atendimento_procedimentos = $AtendimentoProcedimentos->find('all')
            ->contain([
                'Solicitante',
                'Atendimento' => [
                    'Convenio',
                    'Clientes'
                ], 'Procedimentos'])
            ->where(['AtendimentoProcedimentos.situacao_id' => 1])
            ->andWhere(['AtendimentoProcedimentos.created >=' . $inicio->format('Y-m-d') . ' 00:00:00'])
            ->andWhere(['AtendimentoProcedimentos.created <=' . $fim->format('Y-m-d') . ' 23:59:59']);
        if (!empty($convenios)) {
            $atendimento_procedimentos->andWhere(['Atendimento.convenio_id IN' => $convenios]);
        }
        if (!empty($solicitante)) {
            $atendimento_procedimentos->andWhere(['AtendimentoProcedimentos.solicitante_id IN' => $solicitante]);
        }
        $json['alias'] = [
            'inicio' => $inicio,
            'fim' => $fim
        ];
        $json['produtividade'][] = null;

        foreach ($atendimento_procedimentos as $ap) {
            $produtividade = $ProdutividadeSolicitante->find('first')
                ->where(['ProdutividadeSolicitante.convenio_id' => $ap->atendimento->convenio_id])
                ->andWhere(['ProdutividadeSolicitante.procedimento_id' => $ap->preocedimento_id]);
            if ($produtividade->count() > 0) {
                $total = $ap->valor_caixa + $ap->total;
                $repasse = ($total - ($produtividade->percentual_desconto * $total)) * $produtividade->percentual_repasse;

                $json['produtividade'][] = [
                    'atendimento' => $ap->atendimento->id,
                    'data_atendimento' => $this->Data->sms($ap->created),
                    'cliente' => $ap->atendimento->cliente->nome,
                    'procedimento' => $ap->procedimento->nome,
                    'quantidade' => $ap->quantidade,
                    'valor_caixa' => $ap->valor_caixa,
                    'valor_fatura' => $ap->total,
                    'total' => $ap->valor_caixa + $ap->total,
                    'porcentagem_desconto' => $produtividade->percentual_desconto . ' %',
                    'porcentagem_repasse' => $produtividade->poercentagem_repasse . ' %',
                    'repasse' => $repasse,
                    'solicitante' => $ap->solicitante->nome,
                    'solicitante_id' => $ap->solicitante_id
                ];
            }
        }
        return $json;
    }

    public function atendimento_solicitante($solicitante = null, $inicio = null, $fim = null)
    {
        $model = TableRegistry::get('Atendimentos');
        $atendimento = $model->find('all')
            ->contain([
                'ContasReceber' => [
                    'TipoPagamento',
                    'Users',
                    'strategy' => 'select',
                    'conditions' => ['ContasReceber.status_id' => 1]
                ],
                'Clientes',
                'Convenios',
                'Users',
                'ConfiguracaoPeriodos',
                'Solicitantes',
                'AtendimentoProcedimentos' => [
                    'conditions' => ['AtendimentoProcedimentos.situacao_id' => 1],
                    'Procedimentos',
                    'MedicoResponsaveis'
                ]
            ]);
        $atendimento->where(['Atendimentos.situacao_id' => 1]);
        if (!empty($solicitante))
            $atendimento->andWhere(['Atendimentos.solicitante_id' => $solicitante]);
        if (!empty($inicio)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $inicio);
            $atendimento->andWhere(['Atendimentos.data >=' => $now->format('Y-m-d') . " 00:00:00"]);
        }
        if (!empty($fim)) {
            $now = new DateTime();
            $now = $now->createFromFormat('d/m/Y', $fim);
            $atendimento->andWhere(['Atendimentos.data <=' => $now->format('Y-m-d') . " 23:59:59"]);
        }
        $json['atendimentos'] = null;
        foreach ($atendimento as $a) {
            $pro = null;
            foreach ($a->atendimento_procedimentos as $pa) {
                $pro[] = [
                    'procedimento' => $pa->procedimento->nome,
                    'valor' => empty($pa->valor_caixa) ? 0 : $pa->valor_caixa,
                    'executante' => $pa->medico_responsavei->nome,
                    'preco_fatura' => $pa->valor_fatura,
                    'quantidade' => $pa->quantidade
                ];
            }
            $json['atendimentos'][] = [
                'procedimentos' => $pro,
                'atendimento' => $a->id,
                'data' => $this->Data->data_sms($a->data),
                'paciente' => $a->cliente->nome,
                'convenio' => $a->convenio->nome,
                'nnf' => empty($a->conta_receber->numfiscal) ? '' : $a->conta_receber->numfiscal,
                'usuario' => $a->user->nome,
                'solicitante' => $a->solicitante->nome
            ];
        }
        $json['alias'][] = [
            'inicio' => empty($inicio) ? '' : $inicio,
            'fim' => empty($fim) ? '' : $fim
        ];
        return $json;
    }

    public function getTiposRelatorioProdutividadeExecutante()
    {
        return [
            2 => 'Executante - Analítico',
            4 => 'Executante - Excel',
            1 => 'Executante - Geral',
            5 => 'Executante - Simplificado Por Grupo Procedimento',
            6 => 'Executante - Sintético Por Grupo Procedimento'
        ];
    }

    public function getTiposRelatorioProdutividadeSolicitante()
    {
        return [
            3 => 'Solicitante - Geral'
        ];
    }

    public function getTipoRelatorioFechamentoCaixa()
    {
        $configuracaoRelCaixa = TableRegistry::get('ConfiguracaoRelCaixaUsuario')->find()
            ->contain(['ConfiguracaoRelCaixa'])
            ->where(['ConfiguracaoRelCaixaUsuario.user_id' => $_SESSION['Auth']['User']['id']])
            ->order(['ConfiguracaoRelCaixa.id'])
            ->toArray();
        
        $array = array();
        foreach($configuracaoRelCaixa as $configuracao){
            $array[$configuracao->configuracao_rel_caixa->id] = $configuracao->configuracao_rel_caixa->descricao;
        }
        return $array;
    }

    public function getTiposRelatoriosFaturamento()
    {
        $array = [
            4 => 'Excel',
            8 => 'Fusex',
            9 => 'GAB/SARAM',
            2 => 'Geral Convênio',
            7 => 'Ordem Guia/IMPCG',
            1 => 'Profissional Executante x Convênios',
            5 => 'Totalizado Por Convênio x Grupo Procedimento',
            11 => 'Totalizado Por Pacientes',
            6 => 'Totalizado Por Pacientes C/ Valores de Caixa',
            3 => 'Totalizado Por Procedimento',
            10 => 'Totalizado Por Procedimento - Tabela/Unimed'
        ];

        if ($this->Configuracao->mostraOpcoesEquipes()) {
            $array[12] = 'Geral Convênio C/ Total Equipe';
            $array[13] = 'Totalizado Por Pacientes C/ Total Equipe';
        }

        return $array;
    }

    public function getTiposRelatoriosFaturamentoEquipes()
    {
        return [
            4 => 'Excel',
            2 => 'Geral Convênio',
            1 => 'Profissional Executante x Convênios',
            3 => 'Tipo Instrumentador',
            11 => 'Totalizado Por Pacientes',
            6 => 'Totalizado Por Pacientes C/ Valores de Caixa',
        ];
    }
}