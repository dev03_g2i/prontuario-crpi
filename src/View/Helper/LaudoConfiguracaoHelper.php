<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class LaudoConfiguracaoHelper extends Helper
{
    public function configuracao()
    {
        $config = TableRegistry::get('LaudosConfiguracoes');
        return $config->find()->first();
    }

    public function laudarQtdFilmes()
    {
        if($this->configuracao()->laudar_qtd_filmes == 1)
            return true;
    }

    public function laudarQtdImagens()
    {
        if($this->configuracao()->laudar_qtd_imagens == 1)
            return true;
    }

    public function laudarQtdPapeis()
    {
        if($this->configuracao()->laudar_qtd_papeis == 1)
            return true;
    }
    public function laudarCabecalho()
    {
        if($this->configuracao()->laudar_cabecalho == 1)
            return true;
    }
    public function laudarNumeracaoPaginas()
    {
        if($this->configuracao()->laudar_numeracao_paginas == 1)
            return true;
    }
}

?>