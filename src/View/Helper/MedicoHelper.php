<?php
namespace App\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class MedicoHelper extends Helper {

    public function medico($id)
    {
        $medico = TableRegistry::get('MedicoResponsaveis');
        return $medico->get($id)    ;
    }
    // Busca o nome do medico por ID
    public function getMedicoName($id)
    {
        return $this->medico($id)->nome;
    }
}

?>