<?php
namespace App\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\I18n\Time;

class AgendaHelper extends Helper
{
    public function check_atendimento($id)
    {
        $atendimento = TableRegistry::get('Atendimentos');
        $adent = $atendimento->find('all')
            ->where(['Atendimentos.situacao_id'=>1])
            ->andWhere(['Atendimentos.agenda_id'=>$id])->first();
        if(!empty($adent->id)){
            return $adent->id;
        }else{
            return null;
        }
    }

    public function getGrupoAgenda($id)
    {
        $grupoAgenda = TableRegistry::get('GrupoAgendas');
        $gp = $grupoAgenda->get($id);
        if(!empty($gp->nome)){
            return $gp->nome;
        }else {
            return null;
        }
    }
}