<?php
namespace App\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class ConvenioHelper extends Helper {


    public function findConvenio()
    {
        $query = TableRegistry::get('Convenios');
        return $query;
    }
    // Usa ou não aba guia no cadastro de procedimento
    public function usaTiss($convenio_id)
    {
        $convenio = $this->findConvenio()->get($convenio_id);
        if($convenio->usa_tiss == 1){
            return true;
        }else{
            return false;
        }
    }
    // Usa ou não aba cobrança no cadastro de procedimento
    public function usaCobranca($convenio_id)
    {
        $convenio = $this->findConvenio()->get($convenio_id);
        if($convenio->usa_cobranca == 1){
            return true;
        }else{
            return false;
        }
    }

    // Adiciona required ou não no campo numero guia
    public function requiredNumeroGuia($convenio_id)
    {
        $convenio = $this->findConvenio()->get($convenio_id);
        if($convenio->obrigatorio_nr_guia == 1){
            return true;
        }else{
            return false;
        }
    }
    // Adiciona required ou não no campo autorização senha
    public function requiredAutorizacaoSenha($convenio_id)
    {
        $convenio = $this->findConvenio()->get($convenio_id);
        if($convenio->obrigatorio_autorizacao_senha == 1){
            return true;
        }else{
            return false;
        }
    }
    // Adiciona required ou não no campo Carteira paciente
    public function requiredCarteiraPaciente($convenio_id)
    {
        $convenio = $this->findConvenio()->get($convenio_id);
        if($convenio->obrigatorio_carteira_paciente == 1){
            return true;
        }else{
            return false;
        }
    }

    // Verifica se o convenio é -1/Sistema
    public function verificaConvenioSistema($convenio_id)
    {
        if($convenio_id == '-1')
            return false;
        else
            return true;
    }
}

?>