<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class ConfiguracaoHelper extends Helper
{
    /**
     * Tabela configuracoes
     */
    public function configuracoes()
    {
        $configuracoes = TableRegistry::get('Configuracoes');
        return $configuracoes->find()->first();
    }

    /**
     * Tabela configuracao_internacao
     */
    public function configuracoesInternacao()
    {
        $configuracao_internacao = TableRegistry::get('ConfiguracaoInternacao');
        return $configuracao_internacao->find()->first();
    }

    /**
     * Tabela configuracao_cadpaciente
     */
    public function configuracoesCadPaciente()
    {
        $configuracao_cadpaciente = TableRegistry::get('ConfiguracaoCadpaciente');
        return $configuracao_cadpaciente->find()->first();
    }

    /**
     * Tabela configuracao_produtividade
     */
    public function configuracoesProdutividade()
    {
        $configuracao_produtividade = TableRegistry::get('ConfiguracaoProdutividade');
        return $configuracao_produtividade->find()->first();
    }

    /**
     * Tabela users
     */
    public function user()
    {
        $users = TableRegistry::get('Users');
        return $users->find()->first();
    }

    // Configuração para usar MatMed
    public function MatMedFatura()
    {
        if ($this->configuracoes()->matmed_fatura == 0) {
            return 'hidden';
        }
    }

    // Configuração para usar desconto nos atendimentos
    public function editDesconto()
    {
        if ($this->configuracoes()->edit_desconto == 1) {
            return false;
        } else {
            return true;
        }
    }

    // Configuração para usar desconto nos atendimentos
    public function editValorCaixa()
    {
        if ($this->configuracoes()->edit_valor_caixa == 1) {
            return false;
        } else {
            return true;
        }

    }

    // Configuração para usar orientações dos procedimentos da agenda
    public function showOrientacao()
    {
        if ($this->configuracoes()->orientacao_proc_agenda == 0) {
            return 'hidden';
        }
    }

    // Configuração para mostrar previsão de entrega dos atendimentos
    public function showPrevEntrega()
    {
        if ($this->configuracoes()->atendimento_dt_entrega == 0) {
            return 'hidden';
        }
    }

    // Configuração para mostrar os Laudos
    public function showLaudos()
    {
        if ($this->configuracoes()->utiliza_laudos == 0) {
            return 'hidden';
        }
    }

    // Mostra ou não aba Diagnóstico no cadastro do procedimento
    public function usaDiagnostico()
    {
        if ($this->configuracoes()->utiliza_laudos == 1) {
            return true;
        } else {
            return false;
        }
    }


    public function usaChat()
    {
        $chat = isset($_SESSION['Auth']['User']['chat_module']) ? $_SESSION['Auth']['User']['chat_module'] : 0;

        if ($this->configuracoes()->chat_module == 1 && (isset($chat) && $chat == 1)) {
            return true;
        } else {
            return false;
        }
    }

    public function usaFinanceiro()
    {
        $financier = isset($_SESSION['Auth']['User']['financier_module']) ? $_SESSION['Auth']['User']['financier_module'] : 0;

        if ($this->configuracoes()->financier_module == 1 && (isset($financier) && $financier == 1)) {
            return true;
        } else {
            return false;
        }
    }

    public function mostraFluxo()
    {
        $mostraFluxo = isset($_SESSION['Auth']['User']['mostra_fluxo_module']) ? $_SESSION['Auth']['User']['mostra_fluxo_module'] : 0;

        if ($this->configuracoes()->financier_module == 1 && (isset($mostraFluxo) && $mostraFluxo == 1)) {
            return true;
        } else {
            return false;
        }
    }

    public function getNomeUser()
    {
        return (string)$_SESSION['Auth']['User']['nome'];
    }

    public function getEmailUser()
    {
        if (isset($_SESSION['Auth']['User']['email']) && !empty($_SESSION['Auth']['User']['email'])) 
            return $_SESSION['Auth']['User']['email'];
        else 
            return 'Não tem e-mail';
    }

    public function usaConvenioCadastroContrato()
    {
        if ($this->configuracoes()->convenio_cadastro_contrato == 1) {
            return true;
        } else {
            return false;
        }
    }


    // Configuração mostrar valores PrecoProcedimentos
    public function showUco()
    {
        if ($this->configuracoes()->usa_uco == 0) {
            return 'hidden';
        }
    }

    public function showFilme()
    {
        if ($this->configuracoes()->usa_filme == 0) {
            return 'hidden';
        }
    }

    public function showPorte()
    {
        if ($this->configuracoes()->usa_porte == 0) {
            return 'hidden';
        }
    }
    /*************************************************/
    // Configuração de obrigatoriedade para o campo indicação clientes
    public function requiredIndicacao()
    {
        if ($this->configuracoes()->indicacao_obrigatoria == 1) {
            return true;
        } else {
            return false;
        }
    }

    // Retorna o id do plano de contas setado na configuracao
    public function IdPlanoContas()
    {
        return $this->configuracoes()->caixa_idplano_contas;
    }

    // Mostra ou não o campo Plano de contas
    public function showPlanoContas()
    {
        if ($this->configuracoes()->mostra_caixa_plano == 0) {
            return 'hidden';
        }
    }

    public function getMostraPlanoContas()
    {
        return $this->configuracoes()->mostra_caixa_plano;
    }

    // Edita ou não Data de Entrega contas receber
    public function editCaixaDtEntrega()
    {
        if ($this->configuracoes()->caixa_editdt_entrega == 0) {
            return true;
        } else {
            return false;
        }
    }

    // Mostra ou não o titulo na impressão de documentos
    public function showDocTitulo()
    {
        if ($this->configuracoes()->emissao_doc_ocultatitulo == 0) {
            return false;
        } else {
            return true;
        }
    }

    /** mostra Agenda Lista
     * @return string
     */
    public function showAgendaLista()
    {
        if ($this->configuracoes()->agenda_interface == 2)
            return 'hidden';
    }

    /** mostra Agenda Calendario
     * @return string
     */
    public function showAgendaCalendario()
    {
        if ($this->configuracoes()->agenda_interface == 1)
            return 'hidden';
    }

    /** mostra Geração de horários
     * @return string
     */
    public function showGeracaoHorarios()
    {
        if ($this->configuracoes()->agenda_interface_lista == 2)
            return 'hidden';
    }

    /** mostra formulario de solicitantes
     * @return string
     */
    public function showSolicitante()
    {
        if ($this->configuracoes()->mostra_solicitante_procedimentos == 1)
            return true;
        else
            return false;
    }

    /** mostra campo filme_reais
     * @return string
     */
    public function showFilmeReais()
    {
        if ($this->configuracoes()->usa_filme_reais == 0) {
            return 'hidden';
        }
    }

    /**
     * @see Mostra a coluna de controle financeiro nas telas de view e edit do atendimento
     */
    public function showControleFinanceiroOnAtendimentos()
    {
        if ($this->configuracoes()->controle_finsetor_atendproc == 0) {
            return 'hidden';
        }
    }

    /**
     * @see Mostra o campo de controle financeiro na tela de cadastro de atendimento procedimento
     */
    public function mostraCampoControleFinanceiroOnProcedimento()
    {
        if ($this->configuracoes()->controle_finsetor_atendproc == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @see Mostra a coluna de controle financeiro nas telas de index das Contas a receber
     */
    public function showControleFinanceiroOnContasReceber()
    {
        if ($this->configuracoes()->controle_financsetor_receita == 0) {
            return 'hidden';
        }
    }

    /**
     * @see Mostra o campo de controle financeiro na tela de cadastro das contas a receber
     */
    public function mostraCampoControleFinanceiroOnContasReceber()
    {
        if ($this->configuracoes()->controle_financsetor_receita == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Mostra a área de internacao no cadastro do atendimento
     */
    public function mostraAreaInternacao()
    {
        if ($this->configuracoes()->internacao_habilitada == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Mostra o campo carater_atendimento
     * Se for = 1 pega a lista da tabela, se for = 0 pega o valor do campo carater_atendimento_padrao
     */
    public function mostraCampoCaraterAtendimentoOnAtendimentos()
    {
        if ($this->configuracoesInternacao()->carater_atendimento == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getValorCaraterAtendimentoPadrao()
    {
        return $this->configuracoesInternacao()->carater_atendimento_padrao;
    }

    /**
     * Mostra o campo internacao_acomodacao
     * Se for = 1 pega a lista da tabela, se for = 0 pega o valor do campo carater_atendimento_padrao
     */
    public function mostraInternacaoAcomodacaoOnAtendimentos()
    {
        if ($this->configuracoesInternacao()->acomodacao == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getValorInternacaoAcomodacaoPadrao()
    {
        return $this->configuracoesInternacao()->acomodacao_padrao;
    }

    /**
     * Mostra o campo motivo_alta_padrao
     * Se for = 1 pega a lista da tabela, se for = 0 pega o valor do campo carater_atendimento_padrao
     */
    public function mostraMotivoAltaOnAtendimentos()
    {
        if ($this->configuracoesInternacao()->motivo_alta == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function getValorMotivoAltaPadrao()
    {
        return $this->configuracoesInternacao()->motivo_alta_padrao;
    }

    public function usaEquipe()
    {
        if ($this->configuracoes()->usa_equipe == 0) {
            return 'hidden';
        } else {
            return null;
        }
    }

    /**
     * Mostra o campo religiao
     * 1 = Não mostra na tela de cadastro e nao obrigatorio, 2 = mostra e nao obrigatório, 3 = mostra e obrigatório
     */
    public function mostraReligiaoOnClientes()
    {
        return $this->configuracoesCadPaciente()->religiao;
    }

    /** Mostra o campo complemento do @class AtendimentoProcedimentos
     * 0 - Não mostra | 1 - mostra
     * @return void
     */
    public function mostraProcedimentoComplemento()
    {
        if ($this->configuracoes()->procedimento_mostra_complemento == 1)
            return true;
    }

    /**
     * Função para definir se a opção de produtividade aparece no menu superior
     */
    public function mostraMenuProdutividade()
    {
        if ($this->configuracoesProdutividade()->usa_produtividade == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Função para definir se a opção de produtividade->executantes aparece no menu superior
     */
    public function mostraMenuProdutividadeExecutante()
    {
        if ($this->configuracoesProdutividade()->produtividade_executante == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Função para definir se a opção de produtividade->solicitantes aparece no menu superior
     */
    public function mostraMenuProdutividadeSolicitante()
    {
        if ($this->configuracoesProdutividade()->produtividade_solicitante == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Função que retorna a configuração de arquivos para o cadastro dos pacientes
     */
    public function getConfigArquivo()
    {
        return $this->configuracoesCadPaciente()->config_arquivo;
    }

    /**
     * Função que retorna a configuração de rg para o cadastro dos pacientes
     */
    public function getConfigRg()
    {
        return $this->configuracoesCadPaciente()->config_rg;
    }

    /**
     * Função que retorna a configuração de cid para o cadastro dos pacientes
     */
    public function getConfigCid()
    {
        return $this->configuracoesCadPaciente()->config_cid;
    }

    /**
     * Função que retorna a configuração de profissao para o cadastro dos pacientes
     */
    public function getConfigProfissao()
    {
        return $this->configuracoesCadPaciente()->config_profissao;
    }

    /**
     * Função que retorna a configuração de endereço para o cadastro dos pacientes
     */
    public function getConfigEndereco()
    {
        return $this->configuracoesCadPaciente()->config_endereco;
    }

    /**
     * Função que retorna a configuração de email para o cadastro dos pacientes
     */
    public function getConfigEmail()
    {
        return $this->configuracoesCadPaciente()->config_email;
    }

    /**
     * Função que retorna a configuração de matricula e validade para o cadastro dos pacientes
     */
    public function getConfigMatriculaValidade()
    {
        return $this->configuracoesCadPaciente()->config_matricula_validade;
    }

    /**
     * Função que retorna a configuração de convenio para o cadastro dos pacientes
     */
    public function getConfigConvenio()
    {
        return $this->configuracoesCadPaciente()->config_convenio;
    }

    /**
     * Função que retorna a configuração de medico para o cadastro dos pacientes
     */
    public function getConfigMedico()
    {
        return $this->configuracoesCadPaciente()->config_medico;
    }

    /**
     * Função que retorna a configuração de nome dos pais para o cadastro dos pacientes
     */
    public function getConfigNomePais()
    {
        return $this->configuracoesCadPaciente()->config_pais_nome;
    }

    /**
     * Função que retorna a configuração de profissão dos pais para o cadastro dos pacientes
     */
    public function getConfigProfissaoPais()
    {
        return $this->configuracoesCadPaciente()->config_pais_profissao;
    }

    /**
     * Função que retorna a configuração de idade dos pais para o cadastro dos pacientes
     */
    public function getConfigIdadePais()
    {
        return $this->configuracoesCadPaciente()->config_pais_idade;
    }

    /**
     * Função que retorna a configuração de nome conjuge para o cadastro dos pacientes
     */
    public function getConfigNomeConjuge()
    {
        return $this->configuracoesCadPaciente()->config_nome_conjuge;
    }

    /**
     * Função que retorna a configuração de estado_civil para o cadastro dos pacientes
     */
    public function getConfigEstadoCivil()
    {
        return $this->configuracoesCadPaciente()->config_estado_civil;
    }

    /**
     * Função que retorna a configuração de naturalidade para o cadastro dos pacientes
     */
    public function getConfigNaturalidade()
    {
        return $this->configuracoesCadPaciente()->config_naturalidade;
    }

    /**
     * Função que retorna a configuração de nacionalidade para o cadastro dos pacientes
     */
    public function getConfigNacionalidade()
    {
        return $this->configuracoesCadPaciente()->config_nacionalidade;
    }

    public function hablitaAtendimentoDocs()
    {
        $configuracoes = TableRegistry::get('ConfiguracaoAtendimentoDocs');
        $config = $configuracoes->find()->first();
        if($config->habilitar === 1)
            return true;
        else
            return false;
    }

    /* Configuracao estoque */
    public function configuracaoEstoque()
    {
        $configuracoes = TableRegistry::get('ConfiguracaoEstoque');
        return $configuracoes->find()->first();
    }
    public function habilitarEstoque()
    {
        if($this->configuracaoEstoque()->habilitado == 0){
            return false;
        }else{
            return true;
        }
    }
    /**************************/
    /* Configuracao Agenda */
    public function configuracaoAgenda()
    {
        $configuracoes = TableRegistry::get('ConfiguracaoAgenda');
        return $configuracoes->find()->first();
    }
    public function idadeObrigatorio()
    {
        if($this->configuracaoAgenda()->idade_obrigatorio == 0)
        {
            return false;
        }else{
            return true;
        }
    }
    /***********************/

    /** Configurações da Tabela Users */
    /**
     * 1ª Documentação - 12/03/2021, por Fernanda Taso
     * o que faz: Mostra/esconde o menu superior SUS, por Padrão = mostra
     * Usado em: Element - menu_ispinia2.ctp
     */
    public function mostraMenuSuperiorSUS(){
        return $this->user()->mostra_menu_superior_relatorio_sus;
    }
}

?>