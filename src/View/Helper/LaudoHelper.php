<?php
namespace App\View\Helper;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class LaudoHelper extends Helper
{
    public function laudo()
    {
        $laudo = TableRegistry::get('Laudos');
        return $laudo;
    }
    public function assinadoPor($id)
    {
        $users = TableRegistry::get('Users');
        $user = $users->get($id);
        if(!empty($user->nome)){
            return $user->nome;
        }else{
            return null;
        }
    }

    public function verificaImagem($id)
    {
        $this->AtendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');

        $db = ConnectionManager::get('oviyam');
        if(false){
            $this->Study  = TableRegistry::get('Study');
            $atendProc = $this->AtendimentoProcedimentos->get($id, [
                'contain' => ['Procedimentos' => ['GrupoProcedimentos']]
            ]);

            $study = $this->Study->find('all')
                ->contain(['Series'])
                ->where(['Study.accession_no' => $atendProc->atendimento_id])
                ->join([
                    'table' => 'series',
                    'alias' => 'Series',
                    'type' => 'LEFT',
                    'conditions' => [
                        'Study.pk = Series.study_fk'
                    ]])
                ->andWhere(['Series.modality' => $atendProc->procedimento->grupo_procedimento->modalidadedicom ])->first();
            if(!empty($study)){
                return $study->study_iuid;
            }else {
                return null;
            }
        }

    }

    public function urlImageLaudo()
    {
        $this->Configuracoes  = TableRegistry::get('Configuracoes');
        $url_image_laudo = $this->Configuracoes->find()->select('url_image_laudo')->first();
        return $url_image_laudo->url_image_laudo;
    }

    public function gerarUrlImage($idImagem)
    {
        if(!empty($idImagem)){
            $url = $this->urlImageLaudo().'oviyam2/viewer.html?studyUID='.$idImagem.'&serverName=PACSG2I';
            return $url;
        }else {
            return 'javascript:void(0)';
        }
    }

    public function gerarUrlImageAr($idimagem)
    {
        if(!empty($idimagem)){
            return $this->urlImageLaudo().'weasis-pacs-connector/viewer?studyUID='.$idimagem;
        }else {
            return 'javascript:void(0)';
        }
    }

    public function existeLaudo($atendProcId)
    {
        $laudo = $this->laudo()->find('all')->where(['Laudos.atendimento_procedimento_id' => $atendProcId])->count();
        if($laudo > 0) {
            return true;
        }else {
            return false;
        }
    }

    public function verificaAssinatura($laudo_id)
    {
        if(!empty($laudo_id)){
            $query = $this->laudo()->get($laudo_id);
            if(!empty($query->assinado_por)){
                return true;
            }else{
                return false;
            }
        }
    }
}