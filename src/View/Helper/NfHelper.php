<?php
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;

class NfHelper extends Helper
{
    /**
     * Tabela configuracoes
     */
    public function configNf()
    {
        $config_nfs = TableRegistry::get('ConfiguracaoNf');
        return $config_nfs->find()->first();
    }

    /**
     * Verifica se existe o pdf da nota fiscal
     */
    public function verificaPdf($numero_nfse)
    {
        $pasta_pdf = @$this->configNf()->pasta_pdf;
        $filename = WWW_ROOT . $pasta_pdf . '/' . $numero_nfse . '.pdf';
        if (file_exists($filename)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * retorna a url do pdf
     */
    public function urlPdf($numero_nfse)
    {
        $pasta_pdf = @$this->configNf()->pasta_pdf;
        if($this->verificaPdf($numero_nfse))
            return '/webroot/'. $pasta_pdf .'/'. $numero_nfse . '.pdf';
    }
}