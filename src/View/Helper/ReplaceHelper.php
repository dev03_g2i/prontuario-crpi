<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06/10/2016
 * Time: 08:53
 */
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\I18n\Time;

class ReplaceHelper extends Helper{
    /**
     * @param $colecao (array contendo os campos a ser trocado no texto ex: {nome} troca pelo nome do paciente)
     * @param $text (texto onde tem os campos de referencia {referencia})
     * @return mixed
     */
    public function replace($colecao, $text) {
        $result = $text;
        foreach ($colecao as $item => $value) {
           $result= str_replace($item,$value,$result);
        }

        return $result;
    }
}
