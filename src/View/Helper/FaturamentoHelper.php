<?php
namespace App\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Dompdf\Exception;

class FaturamentoHelper extends Helper {

    public function ap()
    {
        $AtendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        return $AtendimentoProcedimentos;
    }

    public function somaValorRecebido($faturaid)
    {   
        $AtendimentoProcedimentos = TableRegistry::get('AtendimentoProcedimentos');
        $query = $AtendimentoProcedimentos->find()
            ->select('valor_recebido')
            ->where(['AtendimentoProcedimentos.fatura_encerramento_id' => $faturaid])
            ->andWhere(['AtendimentoProcedimentos.situacao_id' => 1]);
        $query_select = $query->select(['valor_recebido' => $query->func()->sum('valor_recebido')])->toArray();
        
        $total_recebido = number_format(($query_select[0]->valor_recebido) ? $query_select[0]->valor_recebido: '0.00', 2, '.', '');

        return $total_recebido;
    }

    public function calculoValorPrevisto($convenioId, $total_bruto)
    {   
        $ConvenioImpostosParametro = TableRegistry::get('ConvenioImpostosParametro');

        $convenioImpostosParametro = $ConvenioImpostosParametro->find()
            ->where(['id_convenio' => $convenioId]);
        
        $totalTax = 0;

        foreach($convenioImpostosParametro as $cP) {
            $totalTax += $cP->porcentual;
        }
        
        $total_previsto = $total_bruto - ($total_bruto * ($totalTax/100));
        $total_previsto = number_format($total_previsto, 2, '.', '');
        return $total_previsto;
    }

    public function calculoTotalNfs($faturaid)
    {   
        $FaturaNfs = TableRegistry::get('FaturaNfs');

        $faturaNfs = $FaturaNfs->find()
            ->where(['fatura_encerramento_id' => $faturaid]);
        
        $totalLiquid = 0;

        foreach($faturaNfs as $q) {
            $totalLiquid += $q->valor_liquido;
        }

        $totalLiquid = number_format($totalLiquid, 2, '.', '');
        return $totalLiquid;
    }

    public function situacaoRecebimento($id)
    {
        $situacaoRecebimento = TableRegistry::get('SituacaoRecebimentos');
        $sitRec = $situacaoRecebimento->find()->where(['SituacaoRecebimentos.id' => $id])->first();

        if(!empty($sitRec)){
            return $sitRec->nome;
        }else {
            return null;
        }
    }

    public function getSituationName($id) 
    {   
        $situacao_faturaEncerramentos = TableRegistry::get('SituacaoFaturaencerramentos');

        $situacao_faturaEncerramentos = $situacao_faturaEncerramentos->get($id);

        return $situacao_faturaEncerramentos->nome;
    }

    /**
     * Calcula o total dos profissionais - tela equipes
     *
     * @param Integer $ap_id - atendimento_procedimento_id
     * @return void
     */
    public function calculaTotalProfissional($ap_id)
    {
        $ap_equipes = TableRegistry::get('AtendimentoProcedimentoEquipes');
        $query = $ap_equipes->get($ap_id,[
            'contain' => ['AtendimentoProcedimentos']
        ]);
        $total = 0;
        if(!empty($query->atendimento_procedimento)){
            $porcentagem = $query->porcentagem / 100;
            $total = ($query->atendimento_procedimento->total + $query->atendimento_procedimento->valor_caixa) * $porcentagem;
        }
        return $total;
    }

    /**
     * Mostra ou não o menu Pré-faturamentos e Relatórios (Profissionais)
     * 0 - Não mostra | 1 - mostra
     *
     * @return void
     */
    public function mostraFaturaEquipe()
    {
        $config_faturamentos = TableRegistry::get('ConfiguracaoFaturamentos');
        $config = $config_faturamentos->find()->first();
        if($config->tela_fatura_equipe == 1)
            return true;
    }

    /**
     * Mostra ou não preço instrumentador
     * 0 - Não mostra | 1 - mostra
     *
     * @return void
     */
    public function mostraTabelaPrecoInstrumentador()
    {
        $config_faturamentos = TableRegistry::get('ConfiguracaoFaturamentos');
        $config = $config_faturamentos->find()->first();
        if($config->tabela_preco_instrumentador == 0)
            return 'hidden';
    }

}

?>