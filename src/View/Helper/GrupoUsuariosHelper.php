<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\View\View;

/**
 * GrupoUsuarios helper
 */
class GrupoUsuariosHelper extends Helper
{
    /**
     * 1ª Documentação - 15/04/2020, por Mateus Ragazzi
     * Retorna a tupla de configuração para o usuário logado
     * Utilizado pelos métodos desta classe.
     * 
     * @return Object|null contém as configurações do grupo que o usuário pertence.
     */
    private function grupoUsuarios()
    {
        $user = $_SESSION['Auth']['User'];
        if (!empty($user)) {
            return TableRegistry::get('GrupoUsuarios')->find()->where(['id' => $user['grupo_id']])->first();
        }
        return null;
    }
    /** 
     * @author Fernando Kendy
     * @since 30/09/2021
     * @return Query| 
     * 
     * Retorna a consulta base da tabela GrupoUsuario para ser chamada pela Table posteriormente.
     */
    private function grupoUsuariosTable()
    {
        $user = $_SESSION['Auth']['User'];
        if (!empty($user)) {
            return TableRegistry::get('GrupoUsuarios');
        }
        return null;
    }
   
    
    /**
     * 
     * Mostra/esconde botão de "Salvar"
     * Utilizado em Laudos/laudar.ctp
     *
     * @return true|false true se a configuração estiver ativa (1), caso contrário false (0)
     */
    public function mostraBotaoSalvarEmLaudar()
    {
        return $this->grupoUsuarios()->mostra_botao_laudo_edit_add_salvar;
    }

    /**
     *
     * Mostra/esconde botão de "Assinar"
     * Utilizado em Laudos/laudar.ctp
     *
     * @return true|false true se a configuração estiver ativa (1), caso contrário false (0)
     */
    public function mostraBotaoAssinarEmLaudar()
    {
        return $this->grupoUsuarios()->mostra_botao_laudo_edit_assinar;
    }
}
