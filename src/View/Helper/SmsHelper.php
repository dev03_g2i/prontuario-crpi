<?php
namespace App\View\Helper;
use Cake\View\Helper;

class SmsHelper extends Helper {

    function verificaSms($sms, $resposta, $cliente)
    {
        $icon = [];
        if($sms != '' && $cliente != '-1'){
            if($sms === '000'){// Enviado
                $icon = ['icon' => "<i class='fa fa-paper-plane'></i>", 'msg' => 'Enviado', 'tipo' => 'enviado'];
            }elseif($sms === '120'){// Recebida
                $icon = ['icon' => "<i class='fa fa-flag'></i>", 'msg' => 'Recebida', 'tipo' => 'recebida'];
            }elseif($sms === '1000'){// Respondido
                $icon = ['icon' => "<i class='fa fa-whatsapp'></i>", 'msg' => 'Respondido', 'tipo' => 'respondido'];
                if($resposta === '1'){ //Confirmado
                    $icon = ['icon' => "<i class='fa fa-thumbs-o-up'></i>", 'msg' => 'Confirmado', 'tipo' => 'confirmado'];
                }
            }elseif($sms === '0'){//Agendamento não enviado nem pro sistema WEB
                $icon = ['icon' => "<i class='fa fa-times'></i>", 'msg' => 'Não Enviado', 'tipo' => 'Não Enviado Sistema SMS'];
            }elseif($sms === '-10'){//Agedamento já foi varrido pelo sistema WEB
                $icon = ['icon' => "<i class='fa fa-cloud-upload'></i>", 'msg' => 'Enviado Sistema SMS', 'tipo' => 'Enviado Sistema SMS'];
            }
            if(!empty($icon)){
                return "<a href='javascript:void(0)' toggle = 'tooltip', data-placement = 'bottom', title = '".$icon['msg']."', escape = 'false'>".$icon['icon']."</a>";
            }
        }else {
            return null;
        }
    }

}

?>