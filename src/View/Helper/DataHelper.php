<?php
namespace App\View\Helper;
use Cake\View\Helper;
use Cake\I18n\Time;

class DataHelper extends Helper{
    public function dataSQL($data){
        $date = new Time($data);
        return $date->format('d/m/Y H:m');
    }

    public function idade($nascimento) {
        $idade = new Time($nascimento);
        $diff = $idade->diff(new \DateTime());
        return $diff->format("%y"). ' anos '.$diff->format("%m"). ' meses' ;
    }

    public function diasSemana($day=null){
        $dias = [
            0 => 'Domingo',
            1 => 'Segunda',
            2 => 'Terça',
            3 => 'Quarta',
            4 => 'Quinta',
            5 => 'Sexta',
            6 => 'Sábado'
        ];
        if(!empty($day) || $day === 0){
           return $dias[$day];
        }else {
            return $dias;
        }
    }

    public function periodos($periodo=null){
        $periodos = [
            1 => 'Manhã',
            2 => 'Tarde',
            3 => 'Noite'
        ];

        if(!empty($periodo) || $periodo === 0){
            return $periodos[$periodo];
        }else {
            return $periodos;
        }
    }

    public function weeksMysql($day=null){
        $dias = [
            0 => 'Segunda',
            1 => 'Terça',
            2 => 'Quarta',
            3 => 'Quinta',
            4 => 'Sexta',
            5 => 'Sábado',
            6 => 'Domingo'
        ];
        if(!empty($day) || $day === 0){
            return $dias[$day];
        }else {
            return $dias;
        }
    }

}
