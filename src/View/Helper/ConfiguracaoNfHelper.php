<?php

namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;

class ConfiguracaoNfHelper extends Helper
{
    public function configuracao()
    {
        $config = TableRegistry::get('ConfiguracaoNf');
        return $config->find()->first();
    }

    public function hibilitacao()
    {
        if($this->configuracao()->habilitacao == 0)
        {
            return false;
        }else {
            return true;
        }
    }
}

?>