<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir .DS. 'Helvetica',
    'bold' => $fontDir .DS. 'Helvetica-Bold',
    'italic' => $fontDir .DS. 'Helvetica-Oblique',
    'bold_italic' => $fontDir .DS. 'Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir .DS. 'Times-Roman',
    'bold' => $fontDir .DS. 'Times-Bold',
    'italic' => $fontDir .DS. 'Times-Italic',
    'bold_italic' => $fontDir .DS. 'Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir .DS. 'Times-Roman',
    'bold' => $fontDir .DS. 'Times-Bold',
    'italic' => $fontDir .DS. 'Times-Italic',
    'bold_italic' => $fontDir .DS. 'Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir .DS. 'Courier',
    'bold' => $fontDir .DS. 'Courier-Bold',
    'italic' => $fontDir .DS. 'Courier-Oblique',
    'bold_italic' => $fontDir .DS. 'Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir .DS. 'Helvetica',
    'bold' => $fontDir .DS. 'Helvetica-Bold',
    'italic' => $fontDir .DS. 'Helvetica-Oblique',
    'bold_italic' => $fontDir .DS. 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir .DS. 'ZapfDingbats',
    'bold' => $fontDir .DS. 'ZapfDingbats',
    'italic' => $fontDir .DS. 'ZapfDingbats',
    'bold_italic' => $fontDir .DS. 'ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir .DS. 'Symbol',
    'bold' => $fontDir .DS. 'Symbol',
    'italic' => $fontDir .DS. 'Symbol',
    'bold_italic' => $fontDir .DS. 'Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir .DS. 'Times-Roman',
    'bold' => $fontDir .DS. 'Times-Bold',
    'italic' => $fontDir .DS. 'Times-Italic',
    'bold_italic' => $fontDir .DS. 'Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir .DS. 'Courier',
    'bold' => $fontDir .DS. 'Courier-Bold',
    'italic' => $fontDir .DS. 'Courier-Oblique',
    'bold_italic' => $fontDir .DS. 'Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir .DS. 'Courier',
    'bold' => $fontDir .DS. 'Courier-Bold',
    'italic' => $fontDir .DS. 'Courier-Oblique',
    'bold_italic' => $fontDir .DS. 'Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir .DS. 'DejaVuSans-Bold',
    'bold_italic' => $fontDir .DS. 'DejaVuSans-BoldOblique',
    'italic' => $fontDir .DS. 'DejaVuSans-Oblique',
    'normal' => $fontDir .DS. 'DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir .DS. 'DejaVuSansMono-Bold',
    'bold_italic' => $fontDir .DS. 'DejaVuSansMono-BoldOblique',
    'italic' => $fontDir .DS. 'DejaVuSansMono-Oblique',
    'normal' => $fontDir .DS. 'DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir .DS. 'DejaVuSerif-Bold',
    'bold_italic' => $fontDir .DS. 'DejaVuSerif-BoldItalic',
    'italic' => $fontDir .DS. 'DejaVuSerif-Italic',
    'normal' => $fontDir .DS. 'DejaVuSerif',
  ),
  'comic sans' => array(
    'normal' => $fontDir .DS. '2d6bb4c3fe4a14152783c2297f73e607',
  ),
  'comic sans ms' => array(
    'normal' => $fontDir .DS. 'feb67a7d0cd56f8a58fba8081f0182f0',
  ),
  'raleway' => array(
    'normal' => $fontDir .DS. '1676b43d911d87a6f23b9a001570078a',
  ),
  'benchnine' => array(
    'normal' => $fontDir .DS. '8220aa00735a003ca1c253f321032065',
  ),
  'roboto condensed' => array(
    'italic' => $fontDir .DS. 'a9c5a7a5a6109028cd57fce2d6f2b723',
    'normal' => $fontDir .DS. '1c28195e9a72687e5402e0eb2d52f80b',
  ),
  'fontawesome' => array(
    'normal' => $fontDir .DS. '221c8712badcf2483c7617898894be67',
  ),
  'glyphicons halflings' => array(
    'normal' => $fontDir .DS. '11981e2d4cdc3a729efe5d80a4a953d8',
  ),
) ?>