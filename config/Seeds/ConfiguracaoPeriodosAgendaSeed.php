<?php

use Migrations\AbstractSeed;

/**
 * ConfiguracaoPeriodosAgenda seed.
 */
class ConfiguracaoPeriodosAgendaSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nome' => 'Todos',
                'situacao_id' => 1,
                'hora_inicial' => '00:00:00',
                'hora_final' => '23:59:59'
            ],
            [
                'nome' => 'Manhã',
                'situacao_id' => 1,
                'hora_inicial' => '00:00:00',
                'hora_final' => '11:59:59'
            ],
            [
                'nome' => 'Tarde',
                'situacao_id' => 1,
                'hora_inicial' => '12:00:00',
                'hora_final' => '23:59:59'
            ]
        ];

        $table = $this->table('configuracao_periodos_agenda');
        $table->insert($data)->save();
    }
}
