<?php
use Migrations\AbstractMigration;

class InsertNumParcelasTipoPagamento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE fin_tipo_pagamento ADD COLUMN num_parcelas int(11) not null default 0');
    }

    public function down()
    {
        $this->execute('ALTER TABLE fin_tipo_pagamento DROP COLUMN num_parcelas');
    }
}
