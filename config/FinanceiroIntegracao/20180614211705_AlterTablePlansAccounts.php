<?php
use Migrations\AbstractMigration;

class AlterTablePlansAccounts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_plano_contas` ADD FOREIGN KEY (`fin_classificacao_conta_id`) REFERENCES `fin_classificacao_contas`(`id`);');
    }

    public function down()
    {
        $this->execute('drop from fin_plano_contas');
    }
}
