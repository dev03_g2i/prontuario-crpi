<?php
use Migrations\AbstractMigration;

class AlterDataFromFinancierToInchiridionPlanAccountsToPay extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_contas_pagar_planejamentos` ADD FOREIGN KEY (`fin_fornecedor_id`) REFERENCES `fin_fornecedores`(`id`), ADD FOREIGN KEY (`fin_contabilidade_id`) REFERENCES `fin_contabilidades`(`id`), ADD FOREIGN KEY (`fin_plano_conta_id`) REFERENCES `fin_plano_contas`(`id`);');
    }

    public function down()
    {
        $this->execute('drop from fin_contas_pagar_planejamentos');
    }
}
