<?php
use Migrations\AbstractMigration;

class CreateDatabaseFinanceiro extends AbstractMigration
{
    public function up()
    {
        $this->table('fin_abatimentos')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->create();

        $this->table('fin_abatimentos_contas_pagar')
            ->addColumn('fin_abatimento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contas_pagar_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('valor', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addIndex(
                [
                    'fin_abatimento_id',
                ]
            )
            ->addIndex(
                [
                    'fin_contas_pagar_id',
                ]
            )
            ->create();

        $this->table('fin_abatimentos_contas_receber')
            ->addColumn('fin_abatimento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contas_receber_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('percentual', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addIndex(
                [
                    'fin_abatimento_id',
                ]
            )
            ->addIndex(
                [
                    'fin_contas_receber_id',
                ]
            )
            ->create();

        $this->table('fin_banco_movimentos')
            ->addColumn('fin_banco_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('mes', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('ano', 'string', [
                'default' => null,
                'limit' => 4,
                'null' => true,
            ])
            ->addColumn('status', 'string', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('saldoInicial', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('saldoFinal', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('data', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('encerradoDt', 'timestamp', [
                'default' => '0000-00-00 00:00:00',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('encerradoPor', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_movimento', 'string', [
                'default' => null,
                'limit' => 7,
                'null' => true,
            ])
            ->addColumn('fin_movimento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'fin_banco_id',
                ]
            )
            ->create();

        $this->table('fin_bancos')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('agencia', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('conta', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('limite', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('saldo', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('mostra_fluxo', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mostra_saldo_geral', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('fin_classificacao_contas')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('considera', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('operacao_transferencia', 'integer', [
                'comment' => '0 - NÃO, 1 - SIM',
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'tipo',
                ]
            )
            ->create();

        $this->table('fin_contabilidade_bancos')
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_banco_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'fin_banco_id',
                    'fin_contabilidade_id',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('fin_contabilidades')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('descricao', 'text', [
                'default' => null,
                'limit' => 4294967295,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('rua', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('numero', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('uf', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->create();

        $this->table('fin_contas_pagar')
            ->addColumn('data', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('juros', 'float', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('multa', 'float', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('data_pagamento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_fornecedor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('complemento', 'text', [
                'default' => null,
                'limit' => 4294967295,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('valor_bruto', 'float', [
                'default' => null,
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('desconto', 'float', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('fin_tipo_pagamento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_tipo_documento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('numero_documento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('data_cadastro', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('parcela', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('fin_contas_pagar_planejamento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->addIndex(
                [
                    'fin_fornecedor_id',
                ]
            )
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->addIndex(
                [
                    'fin_tipo_documento_id',
                ]
            )
            ->addIndex(
                [
                    'fin_tipo_pagamento_id',
                ]
            )
            ->create();

        $this->table('fin_contas_pagar_planejamentos')
            ->addColumn('periodicidade', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('plazo_determinado', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('valor_parcela', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('cliente_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_fornecedor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('complemento', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('dia_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('a_vista', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('correcao_monetaria', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('proxima_geracao', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ultima_geracao', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('geradas', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo_pgmt', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo_dcmt', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('ultimo_gerado', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mes_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('ultima_atualizacao', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_documento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('primeiro_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mumero_documento', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->addIndex(
                [
                    'correcao_monetaria',
                ]
            )
            ->addIndex(
                [
                    'tipo_pgmt',
                ]
            )
            ->addIndex(
                [
                    'tipo_dcmt',
                ]
            )
            ->addIndex(
                [
                    'fin_fornecedor_id',
                ]
            )
            ->addIndex(
                [
                    'cliente_id',
                ]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->create();

        $this->table('fin_contas_receber')
            ->addColumn('data', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('valor', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cliente_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => '1',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('complemento', 'text', [
                'default' => null,
                'limit' => 4294967295,
                'null' => true,
            ])
            ->addColumn('data_pagamento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('documento', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('numdoc', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('parcela', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('forma_pagamento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('boleto_mensagem', 'integer', [
                'default' => null,
                'limit' => 200,
                'null' => true,
            ])
            ->addColumn('boleto_mensagem_livre', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => true,
            ])
            ->addColumn('boleto_barras', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => true,
            ])
            ->addColumn('boletolinhadigitavel', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => true,
            ])
            ->addColumn('boleto_nosso_numero', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => true,
            ])
            ->addColumn('juros', 'float', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('multa', 'float', [
                'default' => '0.00',
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('boleto_cedente', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('numero_fiscal', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('data_fiscal', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('valor_bruto', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('desconto', 'decimal', [
                'default' => '0.00',
                'null' => false,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('correcao_monetaria', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_tipo_pagamento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_tipo_documento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('numero_documento', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('fin_planejamento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('impresso', 'integer', [
                'default' => 0,
                'limit' => 4,
                'null' => true,
            ])
            ->addColumn('valorPagar', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 15,
                'scale' => 2,
            ])
            ->addColumn('vencimento_boleto', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('perjuros', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('permulta', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_cadastro', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('sis_antigo_cx_boleto', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('sis_antigo_cx', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('extern_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('controle_financeiro_id', 'integer', [
                'default' => '-1',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('origem_registro', 'integer', [
                'comment' => '1 = Origem Ficha , 2 = Origem encerramento faturamento',
                'default' => '1',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fatura_id', 'integer', [
                'comment' => 'Campo para guardar o id da fatura gerado na rotina do prontuário',
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->addIndex(
                [
                    'correcao_monetaria',
                ]
            )
            ->addIndex(
                [
                    'cliente_id',
                ]
            )
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->addIndex(
                [
                    'fin_tipo_documento_id',
                ]
            )
            ->addIndex(
                [
                    'fin_tipo_pagamento_id',
                ]
            )
            ->create();

        $this->table('fin_contas_receber_planejamentos')
            ->addColumn('periodicidade', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('plazo_determinado', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('valor_parcela', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('cliente_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_fornecedor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('complemento', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('dia_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('a_vista', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('correcao_monetaria', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('proxima_geracao', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ultima_geracao', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('geradas', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo_pgmt', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo_dcmt', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('ultimo_gerado', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mes_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('ultima_atualizacao', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_documento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('primeiro_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mumero_documento', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->addIndex(
                [
                    'correcao_monetaria',
                ]
            )
            ->addIndex(
                [
                    'tipo_pgmt',
                ]
            )
            ->addIndex(
                [
                    'tipo_dcmt',
                ]
            )
            ->addIndex(
                [
                    'fin_fornecedor_id',
                ]
            )
            ->addIndex(
                [
                    'cliente_id',
                ]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->create();

        $this->table('fin_fornecedores')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('celular', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('uf', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => true,
            ])
            ->addColumn('numero', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('rua', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 150,
                'null' => true,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('observacao', 'text', [
                'default' => null,
                'limit' => 4294967295,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('banco', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('agencia', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('conta', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('operacao', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->create();

        $this->table('fin_grupo_contabilidades')
            ->addColumn('fin_grupo_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'fin_grupo_id',
                    'fin_contabilidade_id',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->create();

        $this->table('fin_grupos')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'descricao',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('fin_movimentos')
            ->addColumn('data', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_fornecedor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('complemento', 'text', [
                'default' => null,
                'limit' => 4294967295,
                'null' => true,
            ])
            ->addColumn('documento', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('credito', 'float', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('debito', 'float', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('fin_banco_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => '1',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('categoria', 'integer', [
                'default' => '1',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('cliente_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contas_pagar_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_banco_movimento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_criacao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_alteracao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('criado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('alterado_por', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->addIndex(
                [
                    'fin_banco_movimento_id',
                ]
            )
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->create();

        $this->table('fin_planejamentos')
            ->addColumn('periodicidade', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('plazo_determinado', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('valor_parcela', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('cliente_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_fornecedor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_contabilidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('complemento', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('dia_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('a_vista', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fin_plano_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('correcao_monetaria', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('proxima_geracao', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ultima_geracao', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('geradas', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo_pgmt', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('tipo_dcmt', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('ultimo_gerado', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mes_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('ultima_atualizacao', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_documento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('primeiro_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('mumero_documento', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->addIndex(
                [
                    'correcao_monetaria',
                ]
            )
            ->addIndex(
                [
                    'tipo_pgmt',
                ]
            )
            ->addIndex(
                [
                    'tipo_dcmt',
                ]
            )
            ->addIndex(
                [
                    'fin_fornecedor_id',
                ]
            )
            ->addIndex(
                [
                    'cliente_id',
                ]
            )
            ->addIndex(
                [
                    'fin_contabilidade_id',
                ]
            )
            ->addIndex(
                [
                    'fin_plano_conta_id',
                ]
            )
            ->create();

        $this->table('fin_plano_contas')
            ->addColumn('fin_classificacao_conta_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('situacao', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('rateio', 'integer', [
                'default' => '2',
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'fin_classificacao_conta_id',
                ]
            )
            ->addIndex(
                [
                    'situacao',
                ]
            )
            ->create();

        $this->table('fin_situacoes')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('dataCadastro', 'timestamp', [
                'default' => 'CURRENT_TIMESTAMP',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('status', 'boolean', [
                'default' => true,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('fin_status')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->create();

        $this->table('fin_tipo_classificacao')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('status', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'status',
                ]
            )
            ->create();

        $this->table('fin_tipo_documento')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('status_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('usado', 'string', [
                'default' => 'Ambas',
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'status_id',
                ]
            )
            ->create();

        $this->table('fin_tipo_pagamento')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('fin_status_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('usado', 'string', [
                'default' => 'Ambas',
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('taxa', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('fin_tipo_documento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('id_plano_contas_receita', 'integer', [
                'default' => '-10',
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('id_plano_contas_saida', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('taxa01', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('taxa02', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('taxa03', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('taxa04', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('taxa05', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('taxa06', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addIndex(
                [
                    'fin_status_id',
                ]
            )
            ->addIndex(
                [
                    'id_plano_contas_receita',
                ]
            )
            ->addIndex(
                [
                    'id_plano_contas_saida',
                ]
            )
            ->create();

        $this->table('fin_fornecedores')
            ->addForeignKey(
                'fin_plano_conta_id',
                'fin_plano_contas',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('fin_fornecedores')
            ->dropForeignKey(
                'fin_plano_conta_id'
            );

        $this->dropTable('fin_abatimentos');
        $this->dropTable('fin_abatimentos_contas_pagar');
        $this->dropTable('fin_abatimentos_contas_receber');
        $this->dropTable('fin_banco_movimentos');
        $this->dropTable('fin_bancos');
        $this->dropTable('fin_classificacao_contas');
        $this->dropTable('fin_contabilidade_bancos');
        $this->dropTable('fin_contabilidades');
        $this->dropTable('fin_contas_pagar');
        $this->dropTable('fin_contas_pagar_planejamentos');
        $this->dropTable('fin_contas_receber');
        $this->dropTable('fin_contas_receber_planejamentos');
        $this->dropTable('fin_fornecedores');
        $this->dropTable('fin_grupo_contabilidades');
        $this->dropTable('fin_grupos');
        $this->dropTable('fin_movimentos');
        $this->dropTable('fin_planejamentos');
        $this->dropTable('fin_plano_contas');
        $this->dropTable('fin_situacoes');
        $this->dropTable('fin_status');
        $this->dropTable('fin_tipo_classificacao');
        $this->dropTable('fin_tipo_documento');
        $this->dropTable('fin_tipo_pagamento');
    }
}
