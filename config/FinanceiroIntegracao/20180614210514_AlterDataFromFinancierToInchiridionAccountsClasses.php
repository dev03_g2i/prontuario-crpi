<?php
use Migrations\AbstractMigration;

class AlterDataFromFinancierToInchiridionAccountsClasses extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_classificacao_contas` ADD FOREIGN KEY (`criado_por`) REFERENCES `users`(`id`), ADD FOREIGN KEY (`alterado_por`) REFERENCES `users`(`id`);');
    }

    public function down()
    {
        $this->execute('drop from fin_classificacao_contas');
    }
}
