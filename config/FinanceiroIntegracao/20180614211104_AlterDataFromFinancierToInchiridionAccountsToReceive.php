<?php
use Migrations\AbstractMigration;

class AlterDataFromFinancierToInchiridionAccountsToReceive extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_contas_receber` ADD FOREIGN KEY (`fin_plano_conta_id`) REFERENCES `fin_plano_contas`(`id`), ADD FOREIGN KEY (`cliente_id`) REFERENCES `clientes`(`id`), ADD FOREIGN KEY (`fin_contabilidade_id`) REFERENCES `fin_contabilidades`(`id`);');
    }

    public function down()
    {
        $this->execute('drop from fin_contas_receber');
    }
}
