<?php
use Migrations\AbstractMigration;

class AlterTableMoves extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_movimentos` ADD FOREIGN KEY (`fin_plano_conta_id`) REFERENCES `fin_plano_contas`(`id`), ADD FOREIGN KEY (`fin_fornecedor_id`) REFERENCES `fin_fornecedores`(`id`), ADD FOREIGN KEY (`fin_banco_id`) REFERENCES `fin_bancos`(`id`), ADD FOREIGN KEY (`fin_banco_movimento_id`) REFERENCES `fin_banco_movimentos`(`id`), ADD FOREIGN KEY (`fin_contabilidade_id`) REFERENCES `fin_contabilidades`(`id`), ADD FOREIGN KEY (`criado_por`) REFERENCES `users`(`id`), ADD FOREIGN KEY (`alterado_por`) REFERENCES `users`(`id`);');
    }

    public function down()
    {
        $this->execute('drop from fin_movimentos');
    }
}
