<?php
use Migrations\AbstractMigration;

class AlterDataFromFinancierToInchiridionMovesBank extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_banco_movimentos` DROP COLUMN fin_movimento_id;');
        $this->execute('ALTER TABLE `fin_banco_movimentos` ADD FOREIGN KEY (`fin_banco_id`) REFERENCES `fin_bancos`(`id`); ');
        $this->execute('ALTER TABLE `fin_banco_movimentos` ADD FOREIGN KEY (`por`) REFERENCES `users`(`id`), ADD FOREIGN KEY (`encerradoPor`) REFERENCES `users`(`id`); ');
    }
    
    public function down()
    {
        $this->execute('delete from fin_banco_movimentos;');
    }
}