<?php
use Migrations\AbstractMigration;

class UpdateClassificacaoContasConsideraBool extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('UPDATE fin_classificacao_contas set considera = 0 where considera = 2');
    }

    public function down()
    {
        $this->execute('UPDATE fin_classificacao_contas set considera = 2 where considera = 0');
    }
}
