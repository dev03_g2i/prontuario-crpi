<?php
use Migrations\AbstractMigration;

class AlterTableAccountsGroup extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `fin_grupo_contabilidades` ENGINE=INNODB;');
    }

    public function down()
    {
        $this->execute('drop from fin_grupo_contabilidades');
    }
}
