<?php
use Migrations\AbstractMigration;

class InsertConfiguracoesToLaudosConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('laudos_configuracoes', [
            'id' => 1,
            'laudar_qtd_imagens' => 1,
            'laudar_qtd_filmes' => 1,
            'laudar_qtd_papeis' => 1,
            'laudar_cabecalho' => 1,
            'laudar_numeracao_paginas' => 1,
        ]);
    }
    public function down()
    {
        $this->execute("DELETE FROM laudos_configuracoes WHERE id = 1");
    }
}
