<?php
use Migrations\AbstractMigration;

class AlterRegraToConvenioRegracalculo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("UPDATE convenio_regracalculo SET apelido = '100/70/50 - Integral' WHERE id = 3");
    }
    public function dow()
    {
        $this->execute("UPDATE convenio_regracalculo SET apelido = 'Percentual' WHERE id = 3");
    }
}
