<?php
use Migrations\AbstractMigration;

class AddQuantidadeRecebidaToFaturaMatmed extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_matmed');
        $table->addColumn('qtd_recebida', 'integer', [
            'limit' => 11,
            'null' => true,
            'default' => null
        ])->addColumn('vl_recebido', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ])->addColumn('dt_recebimento', 'date', [
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
