<?php
use Migrations\AbstractMigration;

class AddCorSalaesperaToTipoAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_agendas');
        $table->addColumn('cor_salaespera', 'string', [
            'default' => null,
            'limit' => 7,
            'null' => true,
        ]);
        $table->update();
    }
}
