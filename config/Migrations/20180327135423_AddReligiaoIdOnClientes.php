<?php

use Migrations\AbstractMigration;

class AddReligiaoIdOnClientes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('clientes');
        $table->addColumn('religiao_id', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addForeignKey(
            'religiao_id',
            'religiao',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
