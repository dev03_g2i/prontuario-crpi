<?php
use Migrations\AbstractMigration;

class AlterNotNullInsertCompetencia extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN competencia varchar(7) NULL;');
        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN competencia_mes varchar(2) NULL;');
        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN competencia_ano varchar(4) NULL;');
    }
}
