<?php
use Migrations\AbstractMigration;

class NewSituacaoAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('situacao_agendas', [
            'id' => 1500,
            'nome' => 'Reservar',
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'cor' => '#ff9f32',
            'cor_atendimento' => '#ff9f32',
            'ocultar' => 0,
            'valida_horario' => 1
        ]);
    }

    public function down()
    {
        $this->execute("DELETE FROM situacao_agendas WHERE id = 1500");
    }
}
