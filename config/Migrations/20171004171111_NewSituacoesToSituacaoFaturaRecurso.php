<?php

use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class NewSituacoesToSituacaoFaturaRecurso extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('situacao_fatura_recursos', [
                [
                    'id' => 1,
                    'nome' => 'Recurso Encaminhado',
                    'user_id' => 1,
                    'situacao_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 2,
                    'nome' => 'Recurso Recebido',
                    'user_id' => 1,
                    'situacao_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 3,
                    'nome' => 'Recurso Recebido Parcial',
                    'user_id' => 1,
                    'situacao_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                ],
                [
                    'id' => 4,
                    'nome' => 'Recurso Rejeitado',
                    'user_id' => 1,
                    'situacao_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                ]
            ]

        );
    }

    public function down()
    {
        $this->execute('DELETE FROM situacao_fatura_recursos WHERE 1=1');
    }
}
