<?php
use Migrations\AbstractMigration;

class CreateTipoEquipes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_equipes');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255
        ])->addColumn('codigo_tiss', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 4
        ])->addColumn('user_insert', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('user_update', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11,
        ])->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        $table = $this->table('tipo_equipes');
        $table->addForeignKey(
            'user_insert',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
