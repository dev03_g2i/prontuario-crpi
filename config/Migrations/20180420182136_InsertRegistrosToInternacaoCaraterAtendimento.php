<?php

use Migrations\AbstractMigration;

class InsertRegistrosToInternacaoCaraterAtendimento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('internacao_carater_atendimento',
            [
                'descricao' => 'Eletivo',
                'codigotiss' => 'Geral',
                'situacao_id' => 1,
                'user_reg' => 1,
                'user_update' => 1,
                'created' => '2018-03-16 00:00:00',
                'modified' => '2018-03-16 00:00:00'
            ]);

        $this->insert('internacao_carater_atendimento',
            [
                'descricao' => 'Urgencia',
                'codigotiss' => 'Geral',
                'situacao_id' => 1,
                'user_reg' => 1,
                'user_update' => 1,
                'created' => '2018-03-16 00:00:00',
                'modified' => '2018-03-16 00:00:00'
            ]);
    }
}
