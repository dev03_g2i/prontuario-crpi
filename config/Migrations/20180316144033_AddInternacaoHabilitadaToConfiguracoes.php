<?php
use Migrations\AbstractMigration;

class AddInternacaoHabilitadaToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('internacao_habilitada', 'integer', [
            'limit' => 10,
            'comment' => 'Configuração para habilitar a funcionalidade de internação',
            'default' => 0
        ]);
        $table->update();
    }
}
