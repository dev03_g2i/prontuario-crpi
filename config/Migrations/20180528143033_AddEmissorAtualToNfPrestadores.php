<?php
use Migrations\AbstractMigration;

class AddEmissorAtualToNfPrestadores extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('nf_prestadores');
        $table->addColumn('emissor_atual', 'integer', [
            'null' => true,
            'limit' => 11,
            'default' => 1,
            'comment' => '0 - não | 1 - sim',
        ])->addColumn('default_cod_atividade', 'string', [
            'null' => true,
            'limit' => 9,
            'default' => '863050400',
        ])->addColumn('default_nome_atividade', 'string', [
            'null' => true,
            'limit' => 100,
            'default' => 'INFORMAR',
        ])->addColumn('default_tributacao', 'integer', [
            'null' => true,
            'limit' => 11,
            'default' => 84,
        ])->addColumn('default_aliquota_atividade', 'integer', [
            'null' => true,
            'limit' => 11,
            'default' => 5,
        ])->addColumn('default_servicos_tributacao', 'integer', [
            'null' => true,
            'limit' => 11,
            'default' => 1,
            'comment' => '1 - Tributavel'
        ]);
        $table->update();
    }
}
