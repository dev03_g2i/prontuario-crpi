<?php

use Migrations\AbstractMigration;

class InsertRegistrosToInternacaoAcomodacao extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('internacao_acomodacao',
            [
                'descricao' => 'Enfermaria',
                'codigotiss' => 'Geral',
                'situacao_id' => 1,
                'user_reg' => 1,
                'user_update' => 1,
                'created' => '2018-04-20 00:00:00',
                'modified' => '2018-04-20 00:00:00'
            ]
        );

        $this->insert('internacao_acomodacao',
            [
                'descricao' => 'Apartamento',
                'codigotiss' => 'Geral',
                'situacao_id' => 1,
                'user_reg' => 1,
                'user_update' => 1,
                'created' => '2018-04-20 00:00:00',
                'modified' => '2018-04-20 00:00:00'
            ]

        );
    }
}
