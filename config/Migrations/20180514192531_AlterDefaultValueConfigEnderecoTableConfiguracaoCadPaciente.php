<?php

use Migrations\AbstractMigration;

class AlterDefaultValueConfigEnderecoTableConfiguracaoCadPaciente extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_cadpaciente');
        $table->removeColumn('config_endereco');

        $table->addColumn('config_endereco', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (Abrange cep, até cidade - exceto complemento)'
        ]);
        $table->update();
    }
}
