<?php
use Migrations\AbstractMigration;

class UpdateVlCaixaOriginalToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute(' UPDATE atendimento_procedimentos ap SET vl_caixa_original =  ap.valor_caixa WHERE 1=1');
    }

    public function down()
    {
        $this->execute(' UPDATE atendimento_procedimentos ap SET vl_caixa_original =  0.00 WHERE 1=1');
    }
}
