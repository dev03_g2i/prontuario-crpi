<?php
use Migrations\AbstractMigration;

class AddOrdenacaoPorRegraToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('ordenacao_por_regra', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'comment' => 'Ordenação dos procedimentos por regra'
        ]);
        $table->update();
    }
}
