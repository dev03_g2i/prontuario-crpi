<?php
use Migrations\AbstractMigration;

class AddSituacaoIdToNfsTomadores extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('nf_tomadores');
        $table->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
        ])
        ->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->update();
    }
}
