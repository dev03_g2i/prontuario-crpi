<?php

use Migrations\AbstractMigration;

class CreateInternacaoLeito extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('internacao_leito');
        $table->addColumn('descricao', 'string', [
            'limit' => 255,
        ]);
        $table->addColumn('setor_id', 'integer', [
            'default' => 1,
            'null' => true
        ]);
        $table->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'null' => true
        ]);
        $table->addColumn('cor_mapa', 'string', [
            'limit' => 255,
            'null' => true
        ]);
        $table->addColumn('cor_painel', 'string', [
            'limit' => 255,
            'null' => true
        ]);
        $table->addColumn('user_reg', 'integer', [
            'limit' => 20,
            'null' => true
        ]);
        $table->addColumn('user_update', 'integer', [
            'limit' => 20,
            'null' => true
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true
        ]);

        $table
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'user_reg',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'user_update',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'setor_id',
                'internacao_setor',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->create();
    }
}
