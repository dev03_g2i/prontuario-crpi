<?php
use Migrations\AbstractMigration;

class AddMastologiaIdToClienteHistoricos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('cliente_historicos');
        $table->addColumn('mastologia_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ]);
        $table->addIndex([
            'mastologia_id',
        ], [
            'name' => true,
            'unique' => false,
        ])
            ->addForeignKey(
                'mastologia_id',
                'mastologias',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }
}
