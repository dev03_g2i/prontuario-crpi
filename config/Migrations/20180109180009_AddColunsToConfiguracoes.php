<?php

use Migrations\AbstractMigration;

class AddColunsToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('telefone', 'string', [
            'default' => null,
            'null' => true,
            'length' => 25
        ]);
        $table->addColumn('celular', 'string', [
            'default' => null,
            'null' => true,
            'length' => 25
        ]);
        $table->addColumn('fax', 'string', [
            'default' => null,
            'null' => true,
            'length' => 25
        ]);
        $table->addColumn('cep', 'string', [
            'default' => null,
            'null' => true,
            'length' => 25
        ]);
        $table->update();
    }
}
