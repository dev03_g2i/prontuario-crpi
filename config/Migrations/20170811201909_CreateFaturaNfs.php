<?php
use Migrations\AbstractMigration;

class CreateFaturaNfs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_nfs');
        $table->addColumn('data', 'date', [
            'null' => false
        ])
            ->addColumn('valor_bruto', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('imposto_nao_retido', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('imposto_retido', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('descontos', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('valor_liquido', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('obs', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('unidade_id', 'integer', [
                'default' => 1,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('fatura_encerramento_id', 'integer', [
                'default' => 1,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => 1,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('numero_nf', 'text', [
                'default' => null,
                'null' => false,
            ])
            ->create();

        $table = $this->table('fatura_nfs');
        $table->addForeignKey(
            'unidade_id',
            'unidades',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'fatura_encerramento_id',
                'fatura_encerramentos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }
}
