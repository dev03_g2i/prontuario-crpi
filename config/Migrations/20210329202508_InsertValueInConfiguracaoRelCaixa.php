<?php
use Migrations\AbstractMigration;

class InsertValueInConfiguracaoRelCaixa extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        if($this->hasTable('configuracao_rel_caixa')){
            $this->execute("INSERT INTO configuracao_rel_caixa (id, descricao, nome_report, created, user_created)
                VALUES (1, 'Caixa Executante', 'Caixa Executante', now(), 1),
                 (2, 'Relatório de Registros', 'Relatório de Registros', now(), 1),
                 (3, 'Relatório de Registros (Fatores/Particulares) x Recebimento - Individual', 'Relatório de Registros (Fatores/Particulares) x Recebimento - Individual', now(), 1),
                 (4, 'Recebimento Individual', 'Recebimento Individual', now(), 1),
                 (5, 'Recebimento Geral', 'Recebimento Geral', now(), 1),
                 (6, 'Relatório de Atendimento Devedores - Individual', 'Relatório de Atendimento Devedores - Individual', now(), 1),
                 (7, 'Relatório de Atendimento Devedores - Geral', 'Relatório de Atendimento Devedores - Geral', now(), 1)"
            );

        }
    }
}
