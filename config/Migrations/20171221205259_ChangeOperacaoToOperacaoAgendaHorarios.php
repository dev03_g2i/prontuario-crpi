<?php
use Migrations\AbstractMigration;

class ChangeOperacaoToOperacaoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('DELETE FROM operacao_agenda_horarios WHERE id = 1');
        $this->insert('operacao_agenda_horarios', [
            [
                'id' => 1,
                'nome' => 'Gerar Horários',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ]);
    }

    public function down()
    {
        $this->execute('DELETE FROM operacao_agenda_horarios WHERE id = 1');
    }
}
