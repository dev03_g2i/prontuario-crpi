<?php
use Migrations\AbstractMigration;

class InsertRegra5ToConvenioRegrarecalculo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("UPDATE convenio_regracalculo SET apelido = '70% Uco + Porte - 100% Filmes' WHERE id = 5");
    }

    public function down()
    {
        $this->execute("DELETE FROM convenio_regracalculo WHERE id = 5");
    }
}
