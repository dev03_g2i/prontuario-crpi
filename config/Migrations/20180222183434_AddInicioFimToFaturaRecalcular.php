<?php
use Migrations\AbstractMigration;

class AddInicioFimToFaturaRecalcular extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_recalcular');
        $table->removeColumn('periodo');
        $table->addColumn('inicio', 'date', [
            'null' => true,
            'default' => null
        ])->addColumn('fim', 'date', [
            'null' => true,
            'default' => null
        ]);
        $table->update();
    }
}
