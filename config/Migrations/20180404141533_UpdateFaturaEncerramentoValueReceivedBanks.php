<?php
use Migrations\AbstractMigration;

class UpdateFaturaEncerramentoValueReceivedBanks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('fatura_encerramentos')
        ->addColumn('vl_received_bank', 'decimal', [
            'default' =>  null,
            'precision' => 9,
            'scale' => 3
        ]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('fatura_encerramentos')->removeColumn('vl_received_bank');
        $table->update();
    }
}
