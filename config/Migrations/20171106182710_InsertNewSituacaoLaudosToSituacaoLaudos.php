<?php
use Migrations\AbstractMigration;

class InsertNewSituacaoLaudosToSituacaoLaudos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('situacao_laudos', [
            [
                'id' => 110,
                'nome' => 'Em sala',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
                'ordenar' => 110
            ]   
        ]);
    }
    public function down()
    {
        $this->execute('DELETE FROM situacao_laudos WHERE id = 110 AND id = 200');
    }
}
