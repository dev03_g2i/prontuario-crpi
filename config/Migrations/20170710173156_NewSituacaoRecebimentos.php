<?php
use Migrations\AbstractMigration;

class NewSituacaoRecebimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('situacao_recebimentos',array(
            'id' => 1,
            'nome' => 'Faturado',
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
        ));
    }
}
