<?php

use Migrations\AbstractMigration;

class AddColumnsToConfiguracaoCadpaciente extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_cadpaciente');
        $table->addColumn('config_arquivo', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_rg', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_cid', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_profissiao', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_endereco', 'integer', [
            'limit' => 11,
            'default' => 1,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (Abrange cep, até cidade - exceto complemento)'
        ]);

        $table->addColumn('config_email', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_matricula_validade', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange os dois)'
        ]);

        $table->addColumn('config_convenio', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_medico', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório'
        ]);

        $table->addColumn('config_pais_nome', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange nome pai e mae)'
        ]);

        $table->addColumn('config_pais_profissao', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório  (abrange prof. pai e mae)'
        ]);

        $table->addColumn('config_pais_idade', 'integer', [
            'limit' => 11,
            'default' => 2,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange idade pai e mae)'
        ]);

        $table->addColumn('config_nome_conjuge', 'integer', [
            'limit' => 11,
            'default' => 1,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange idade pai e mae)'
        ]);

        $table->addColumn('config_estado_civil', 'integer', [
            'limit' => 11,
            'default' => 1,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange idade pai e mae)'
        ]);

        $table->addColumn('config_naturalidade', 'integer', [
            'limit' => 11,
            'default' => 1,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange idade pai e mae)'
        ]);

        $table->addColumn('config_nacionalidade', 'integer', [
            'limit' => 11,
            'default' => 1,
            'comment' => '1 - Não mostra na tela, 2 -  Mostra e Não Obrigatório, 3 - Mostra e Obrigatório (abrange idade pai e mae)'
        ]);

        $table->update();
    }
}
