<?php
use Migrations\AbstractMigration;

class AddIntegracaoMvSiteIdentifierValorOnConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('integracao_mv_site_identifier_valor', 'string', [
            'limit' => 255,
            'comment' => 'Valor que será definido para o campo site_identifier do xml de integração caso a configuração do campo integracao_mv_site_identifier seja 1'
        ]);
        $table->update();
    }
}
