<?php
use Migrations\AbstractMigration;

class AddPacienteMatrvalidadeToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('paciente_matrvalidade', 'integer', [
            'default' => null,
            'comment' => '0 - Nao obrigatorio / 1 - Obrigatorio',
            'limit' => 11,
            'null' => true,
        ]);
        $table->update();
    }
}
