<?php
use Migrations\AbstractMigration;

class AddCaixaIdPlanoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('caixa_idplano_contas', 'integer', [
            'default' => 283,
            'limit' => 11,
            'null' => true,
            'comment' => '283 - Receita Tratamento'
        ])->addColumn('mostra_caixa_plano', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 -Sim | 1 - Não'
        ]);
        $table->update();
    }
}
