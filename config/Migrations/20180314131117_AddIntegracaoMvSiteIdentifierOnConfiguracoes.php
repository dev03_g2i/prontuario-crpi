<?php
use Migrations\AbstractMigration;

class AddIntegracaoMvSiteIdentifierOnConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('integracao_mv_site_identifier', 'integer', [
            'limit' => 10,
            'comment' => 'Se for 1 pega o numero da ficha (atendimento), se for 0 vai pegar o valor do campo integracao_mv_site_identifier_valor para colocar no xml de integração',
            'default' => 1
        ]);
        $table->update();
    }
}
