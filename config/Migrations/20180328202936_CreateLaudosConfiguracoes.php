<?php
use Migrations\AbstractMigration;

class CreateLaudosConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('laudos_configuracoes');
        $table->addColumn('laudar_qtd_imagens', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])->addColumn('laudar_qtd_filmes', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])->addColumn('laudar_qtd_papeis', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])->addColumn('laudar_cabecalho', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])->addColumn('laudar_numeracao_paginas', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ]);
        $table->create();
    }
}
