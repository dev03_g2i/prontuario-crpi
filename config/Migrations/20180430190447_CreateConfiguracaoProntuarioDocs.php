<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoProntuarioDocs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_prontuario_docs');
        $table->addColumn('nome_modelo', 'string', [
            'limit' => 255,
            'default' => null,
            'null' => true,
            'comment' => 'modelo01, modelo02..'
        ])->addColumn('nome_logo', 'string', [
            'limit' => 255,
            'default' => null,
            'null' => true,
            'comment' => 'logo.png, logo.jpg..'
        ]);
        $table->create();
    }
}
