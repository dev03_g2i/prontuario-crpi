<?php
use Migrations\AbstractMigration;

class AddModeloImpressaoToLaudosConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('laudos_configuracoes');
        $table->addColumn('modelo_impressao', 'string', [
            'default' => null,
            'limit' => 15,
            'null' => true,
        ]);
        $table->update();
    }
}
