<?php
use Migrations\AbstractMigration;

class AddMostraSolicitanteProcedimentoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('mostra_solicitante_procedimentos', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => true,
            'comment' => '1 - Sim | 2 - Não'
        ]);
        $table->update();
    }
}
