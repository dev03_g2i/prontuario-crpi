<?php
use Migrations\AbstractMigration;

class InsertCompetenciaMesAndAno extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('fatura_encerramentos');

        $table->addColumn('competencia_mes', 'string', [
            'limit' => 2,
            'null' => false
        ])->addColumn('competencia_ano', 'string', [
            'limit' => 4,
            'null' => false
        ]);
        
        $table->update();

        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN competencia varchar(7) NOT NULL;');
    }

    public function down()
    {
        $table = $this->table('fatura_encerramentos');

        $table->removeColumn('competencia_mes')
        ->removeColumn('competencia_ano');

        $table->update();

        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN competencia varchar(7)  NULL;');
    }
}
