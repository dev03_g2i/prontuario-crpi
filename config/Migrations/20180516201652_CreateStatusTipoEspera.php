<?php
use Migrations\AbstractMigration;

class CreateStatusTipoEspera extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('status_tipo_espera');
        $table->addColumn('descricao', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ])
        ->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('user_id', 'integer', [
            'default' => 1,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        
        $table = $this->table('status_tipo_espera');
        $table->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
