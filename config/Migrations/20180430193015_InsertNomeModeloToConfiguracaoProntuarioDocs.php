<?php
use Migrations\AbstractMigration;

class InsertNomeModeloToConfiguracaoProntuarioDocs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('configuracao_prontuario_docs', [
            'id' => 1,
            'nome_modelo' => 'pront_doc01',
            'nome_logo' => 'logo.jpg',
        ]);
    }
    public function down()
    {
        $this->execute("DELETE FROM configuracao_prontuario_docs WHERE id = 1");
    }
}
