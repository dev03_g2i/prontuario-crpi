<?php

use Migrations\AbstractMigration;

class TransferenciaConfigsParaCadClientesConfig extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        //$this->execute("ALTER TABLE configuracoes DROP COLUMN restringe_cpf");
        //$this->execute("ALTER TABLE configuracoes DROP COLUMN duplica_cpf");
        //$this->execute("ALTER TABLE configuracoes DROP COLUMN idade_cpf");
        $this->execute("ALTER TABLE configuracao_cadpaciente ADD COLUMN restringe_cpf int(11) DEFAULT 1 COMMENT '1 - Sim, 2 - Não'");
        $this->execute("ALTER TABLE configuracao_cadpaciente ADD COLUMN duplica_cpf varchar(14) DEFAULT '000.000.000-00' ");
        $this->execute("ALTER TABLE configuracao_cadpaciente ADD COLUMN idade_cpf int(11) DEFAULT 12 ");
    }

    public function down()
    {

    }
}
