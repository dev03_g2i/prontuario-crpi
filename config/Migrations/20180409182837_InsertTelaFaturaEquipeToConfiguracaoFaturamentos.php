<?php
use Migrations\AbstractMigration;

class InsertTelaFaturaEquipeToConfiguracaoFaturamentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('configuracao_faturamentos',[
            'id' => 1,
            'tela_fatura_equipe' => 0,
        ]);
    }

    public function down()
    {
        $this->execute("DELETE FROM confituracao_faturamentos WHERE id = 1");
    }
}
