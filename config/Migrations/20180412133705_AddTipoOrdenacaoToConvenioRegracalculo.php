<?php
use Migrations\AbstractMigration;

class AddTipoOrdenacaoToConvenioRegracalculo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('convenio_regracalculo');
        $table->addColumn('tipo_ordenacao', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => true,
            'comment' => '1 - Ordem de inserção | 2 - Ordem'
        ]);
        $table->update();
    }
}
