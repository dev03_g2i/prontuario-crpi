<?php
use Migrations\AbstractMigration;

class CreateProdutividadeProfissionais extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // Create
        $table = $this->table('produtividade_profissionais');
        $table->addColumn('convenio_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false
        ])
            ->addColumn('profissional_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('procedimento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('perc_recebimento', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('perc_imposto', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ]);
        $table->create();

        // addForeignKey
        $table = $this->table('produtividade_profissionais');
        $table->addForeignKey(
            'convenio_id',
            'convenios',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'profissional_id',
                'medico_responsaveis',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'procedimento_id',
                'procedimentos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        // dropForeignKey


    }
}
