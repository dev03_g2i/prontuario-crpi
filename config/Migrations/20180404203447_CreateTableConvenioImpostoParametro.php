<?php
use Migrations\AbstractMigration;

class CreateTableConvenioImpostoParametro extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('convenio_impostos_parametro');

        $table->addColumn('id_convenio', 'integer', [
            'limit' => 11
        ])
        ->addColumn('id_tipo_imposto', 'integer', [
            'limit' => 11
        ])
        ->addColumn('porcentual', 'decimal', [
            'default' =>  null,
            'precision' => 11,
            'scale' => 2
        ])
        ->addColumn('retido', 'integer', [
            'default' => 0
        ])
        ->addColumn('observacao', 'text', [
            'default' => null,
            'null' => true
        ])
        ->addColumn('user_created', 'integer', [
            'default' => null,
            'limit' => 11
        ])
        ->addColumn('user_updated', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])
        ->addColumn('situacao_id', 'integer', [
            'default' => 1
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->create();
    }

    public function down()
    {
        $table = $this->table('convenio_impostos_parametro');
        $table->drop();
    }
}
