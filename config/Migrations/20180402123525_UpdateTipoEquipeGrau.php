<?php
use Migrations\AbstractMigration;

class UpdateTipoEquipeGrau extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('UPDATE tipo_equipegrau te SET porcentual = 100 WHERE id=1');
        $this->execute('UPDATE tipo_equipegrau te SET porcentual = 30 WHERE id=2');
        $this->execute('UPDATE tipo_equipegrau te SET porcentual = 20 WHERE id=3');
        $this->execute('UPDATE tipo_equipegrau te SET porcentual = 20 WHERE id=4');
        $this->execute('UPDATE tipo_equipegrau te SET porcentual = 20 WHERE id=5');

    }
    public function down()
    {
        $this->execute('UPDATE tipo_equipegrau te SET porcentual = 0');
    }
}
