<?php
use Migrations\AbstractMigration;

class InsertConfiguracaoRelCaixaUsuario extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("
            INSERT INTO configuracao_rel_caixa_usuario (
                user_id,
                configuracao_rel_caixa_id
                )
                SELECT
                    configuracao_nova.user_id,
                    configuracao_nova.config_rel_id
                FROM (
                    SELECT u.id AS user_id, config_rel.id AS config_rel_id FROM users AS u
                    CROSS JOIN configuracao_rel_caixa AS config_rel
                    WHERE u.situacao_id = 1
                    AND config_rel.situacao_id = 1
                ) AS configuracao_nova
        ");
    }
}
