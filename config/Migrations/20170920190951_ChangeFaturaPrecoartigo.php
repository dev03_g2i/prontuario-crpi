<?php
use Migrations\AbstractMigration;

class ChangeFaturaPrecoartigo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('SET foreign_key_checks=0');
        $table = $this->table('fatura_precoartigo');
        $table->addForeignKey(
            'artigo_id',
            'estq_artigos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
