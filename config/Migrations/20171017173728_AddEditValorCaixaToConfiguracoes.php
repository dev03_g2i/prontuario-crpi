<?php
use Migrations\AbstractMigration;

class AddEditValorCaixaToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('edit_valor_caixa', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment'=> '1 - Sim | 0 - Não'
        ]);
        $table->update();
    }
}
