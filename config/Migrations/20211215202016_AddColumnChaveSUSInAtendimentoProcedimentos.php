<?php
use Migrations\AbstractMigration;

class AddColumnChaveSUSInAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        if($this->hasTable('atendimento_procedimentos')){
            $table = $this->table('atendimento_procedimentos');
        
            if (!$table->hasColumn('chave_sus')) {
                $table->addColumn('chave_sus', 'string', [
                    'null' => true,
                    'default' => null,
                    'limit' => 20,
                    'comment' => 'nº da guia para atendimento/procedimento SUS'
                ]);
            }
        
            $table->update();
        }
    }
}
