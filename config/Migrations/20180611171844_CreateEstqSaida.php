<?php
use Migrations\AbstractMigration;

class CreateEstqSaida extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('estq_saida');
        $table->addColumn('data', 'date', [
            'default' => null,
            'limit' => 11,
            'null' => false
        ])->addColumn('tipo_movimento_id', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => false
        ])->addColumn('atendimento_procedimento_id', 'integer', [
            'default' => false,
            'limit' => 11,
            'null' => false
        ])->addColumn('exportado_fatura', 'integer', [
            'default' => 0,//não
            'limit' => 11,
            'null' => true,
            'comment' => '1 - exportado | 0 - não'
        ])->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => false
        ])->addColumn('user_insert', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => false
        ])->addColumn('user_update', 'integer', [
            'default' => false,
            'limit' => 11,
            'null' => false
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->create();

        $table = $this->table('estq_saida');
        $table->addForeignKey(
            'tipo_movimento_id',
            'estq_tipo_movimento',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'atendimento_procedimento_id',
            'atendimento_procedimentos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'user_insert',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
