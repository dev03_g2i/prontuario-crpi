<?php
use Migrations\AbstractMigration;

class AddTipoAgendaIdToAgendaPeriodos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agenda_periodos');
        $table->addColumn('tipo_agenda_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])->addForeignKey(
            'tipo_agenda_id',
            'tipo_agendas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
