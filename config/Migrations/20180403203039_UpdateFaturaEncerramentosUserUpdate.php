<?php
use Migrations\AbstractMigration;

class UpdateFaturaEncerramentosUserUpdate extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('fatura_encerramentos')->addColumn('user_update', 'integer', [
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('fatura_encerramentos')->removeColumn('user_update');
        $table->update();
    }
}
