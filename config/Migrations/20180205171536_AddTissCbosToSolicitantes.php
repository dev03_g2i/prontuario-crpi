<?php

use Migrations\AbstractMigration;

class AddTissCbosToSolicitantes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('solicitantes');
        $table->addColumn('tiss_cbos', 'string', [
            'default' => '',
            'limit' => 255
        ]);
        $table->update();
    }
}
