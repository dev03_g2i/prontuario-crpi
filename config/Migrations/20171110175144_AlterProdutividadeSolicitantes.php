<?php
use Migrations\AbstractMigration;

class AlterProdutividadeSolicitantes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('produtividade_solicitantes');
        $table->addForeignKey('solicitante_id', 'solicitantes', 'id',[
            'delete'=> 'NO_ACTION',
            'update'=> 'NO_ACTION'
        ]);
        $table->addForeignKey('convenio_id', 'convenios', 'id',[
            'delete'=> 'NO_ACTION',
            'update'=> 'NO_ACTION'
        ]);
        $table->save();
    }
}
