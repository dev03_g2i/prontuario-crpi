<?php

use Migrations\AbstractMigration;

class AddValorParticularTabelaToTempFaturamento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('temp_produtividadeprofissionais');
        $table->addColumn('valor_caixa_original', 'decimal', [
            'default' => null,
            'null' => true,
            'precision' => 11,
            'scale'=>2,
            'comment' => 'Coluna para guardar preço original na tabela preco procedimento'
        ]);
        $table->addColumn('usa_valor_original', 'boolean',[
            'default'=>null,
            'null'=>true,
            'comment'=>'Zero para não, Um para sim => Essa coluna serve para saber se determinado convênio considerar 
            o valor original de tabela ou o valor real recebido no calculo de produtividade'
        ]);
        $table->update();
    }
}
