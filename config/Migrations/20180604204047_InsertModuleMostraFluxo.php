<?php
use Migrations\AbstractMigration;

class InsertModuleMostraFluxo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE users ADD COLUMN mostra_fluxo_module INT(11) NOT NULL DEFAULT 0;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE users DROP COLUMN mostra_fluxo_module;');
    }
}
