<?php
use Migrations\AbstractMigration;

class AddFichaModeloEspecificoProcedimento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE `procedimentos`  ADD COLUMN `ficha_atendimento_impressa_url_modelo` VARCHAR(255) NULL;');
    }
}
