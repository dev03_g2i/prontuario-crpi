<?php
use Migrations\AbstractMigration;

class CreateSolicitanteCbo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('solicitante_cbo');
        $table
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false
            ])
            ->addColumn('codigo_tiss', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 4
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => 1,
                'null' => false,
                'limit' => 11,
            ])
            ->addColumn('user_insert', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('user_update', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false,
            ]);
        $table->create();

        // addForeignKey
        $table = $this->table('solicitante_cbo');
        $table->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            ['update' => 'RESTRICT','delete' => 'RESTRICT']
        )
        ->addForeignKey(
            'user_insert',
            'users',
            'id',
            ['update' => 'RESTRICT','delete' => 'RESTRICT']
        )
        ->addForeignKey(
            'user_update',
            'users',
            'id',
            ['update' => 'RESTRICT','delete' => 'RESTRICT']
        );
        $table->update();
    }
}