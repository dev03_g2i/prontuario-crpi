<?php

use Migrations\AbstractMigration;

class InsertReligiaoPadraoOnRelegiao extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('religiao',
            [
                'id' => 1,
                'descricao' => 'Não Informado',
                'situacao_id' => 1,
                'user_reg' => 1,
                'user_update' => 1,
                'created' => '2018-03-27 00:00:00',
                'modified' => '2018-03-27 00:00:00'
            ]);
    }
}
