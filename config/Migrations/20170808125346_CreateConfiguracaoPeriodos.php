<?php

/**
 * Criando tabela configuracao_peridos
 * @Author Iohan
 */

use Migrations\AbstractMigration;

class CreateConfiguracaoPeriodos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_periodos');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('inicio_agenda', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('fim_agenda', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('inicio_atendimento', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('fim_atendimento', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
        ]);
        $table->addColumn('user_id','integer');
        $table->create();

        /**
         * Add Foreing Key
         */
        $table = $this->table('configuracao_periodos');
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
