<?php
use Migrations\AbstractMigration;

class CreateFaturaKit extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_kit');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255
        ])
            ->addColumn('criado_por', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('alterado_por', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 255
            ])
            ->addColumn('procedimento_id', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('fatura_kit');
        $table->addForeignKey(
            'procedimento_id',
            'procedimentos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
