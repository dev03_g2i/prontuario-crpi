<?php

use Migrations\AbstractMigration;

class UpdateValidaHorarioOnSituacaoAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute(' UPDATE situacao_agendas sa SET valida_horario = 1 WHERE ocultar = 0');
    }

}
