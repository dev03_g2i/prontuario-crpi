<?php
use Migrations\AbstractMigration;

class InsertColorsToGrupos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(1,1,1,1)" WHERE nome like "Raio-X"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(30,180,200,1)" WHERE nome like "Ultrassonografia"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(180,30,255,1)" WHERE nome like "Tomografia Computadorizada"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(70,240,10,1)" WHERE nome like "Densitometria"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(130,20,20,1)" WHERE nome like "Mamografia"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(10,255,20,1)" WHERE nome like "RX - Contrastado"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(20,10,255,1)" WHERE nome like "Ultrassonografia Doppler"');
        $this->execute('UPDATE grupo_procedimentos te SET color = "rgba(13,64,167,1)" WHERE nome like "MAT/MED"');
    }
    public function down()
    {
        $this->execute('UPDATE grupo_procedimentos te SET color = ""');
    }
 }
