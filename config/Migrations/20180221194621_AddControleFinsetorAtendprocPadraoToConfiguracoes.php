<?php
use Migrations\AbstractMigration;

class AddControleFinsetorAtendprocPadraoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('controle_finsetor_atendproc_padrao', 'integer', [
            'default' => '-1',
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
