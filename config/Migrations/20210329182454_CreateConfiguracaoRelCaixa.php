<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoRelCaixa extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        if(!$this->hasTable('configuracao_rel_caixa')){
            $table = $this->table('configuracao_rel_caixa');
            $table->addColumn('descricao','string',[
                'null' => true,
                'default' => null,
            ]);

            $table->addColumn('nome_report','string',[
                'null' => true,
                'default' => null,
            ]);

            $table->addColumn('situacao_id','integer',[
                'null' => true,
                'default' => 1,
            ]);

            $table->addColumn('created', 'datetime',
                ['default' => 'CURRENT_TIMESTAMP', 'null' => true
            ]);

            $table->addColumn('modified','datetime',[
                'null' => true,
                'default' => null,
            ]);

            $table->addColumn('user_created', 'integer', ['null' => true]);
            $table->addColumn('user_modified', 'integer', ['null' => true]);
            
            $table->addForeignKey('situacao_id', 'situacao_cadastros', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);
            $table->addForeignKey('user_created', 'users', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);
            $table->addForeignKey('user_modified', 'users', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);
            $table->create();
        }
    }
}
