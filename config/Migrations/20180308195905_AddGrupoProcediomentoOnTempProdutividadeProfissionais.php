<?php
use Migrations\AbstractMigration;

class AddGrupoProcediomentoOnTempProdutividadeProfissionais extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('temp_produtividadeprofissionais');
        $table->addColumn('grupo_procedimento', 'string', [
            'limit' => 255,
        ]);
        $table->update();
    }
}
