<?php
use Migrations\AbstractMigration;

class AddDiaSemanaToAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agendas');
        $table->addColumn('dia_semana', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Domingo, 1 - Segunda...6 - Sabado'
        ]);
        $table->update();
    }
}
