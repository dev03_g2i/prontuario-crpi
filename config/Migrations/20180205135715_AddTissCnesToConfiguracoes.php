<?php
use Migrations\AbstractMigration;

class AddTissCnesToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');

        $table->addColumn('tiss_cnes', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->update();
    }
}
