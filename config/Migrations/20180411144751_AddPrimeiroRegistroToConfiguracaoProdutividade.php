<?php
use Migrations\AbstractMigration;

class AddPrimeiroRegistroToConfiguracaoProdutividade extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('configuracao_produtividade', [
            'id' => 1,
            'usa_produtividade' => 1,
            'produtividade_executante' => 1,
            'produtividade_solicitante' => 1
        ]);
    }
}
