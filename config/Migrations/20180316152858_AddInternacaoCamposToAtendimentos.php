<?php

use Migrations\AbstractMigration;

class AddInternacaoCamposToAtendimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimentos');
        $table->addColumn('carater_atendimento_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('internacao_acomoda_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('internacao_previa', 'enum', [
            'values' => ['Não', 'Sim'],
            'null' => true
        ]);
        $table->addColumn('internacao_previa_data', 'date', [
            'null' => true
        ]);
        $table->addColumn('internacao_data_hora_entrada', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('internacao_setor_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('internacao_leito_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('internacao_motivo_alta_id', 'integer', [
            'limit' => 11,
            'null' => true
        ]);
        $table->addColumn('internacao_data_hora_saida', 'datetime', [
            'default' => null,
            'null' => true
        ]);

        $table
            ->addForeignKey(
                'carater_atendimento_id',
                'internacao_carater_atendimento',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'internacao_acomoda_id',
                'internacao_acomodacao',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'internacao_setor_id',
                'internacao_setor',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'internacao_leito_id',
                'internacao_leito',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'internacao_motivo_alta_id',
                'internacao_motivo_alta',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
