<?php
use Migrations\AbstractMigration;

class InsertTipoEquipegraus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('tipo_equipegrau', [
            [
            'codigo_tiss' => '00',
            'descricao' => 'Cirurgião',
            'user_insert' => 1,
            'situacao_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            ],
            [            
            'codigo_tiss' => '01',
            'descricao' => 'Primeiro Auxiliar',
            'user_insert' => 1,
            'situacao_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '02',
                'descricao' => 'Segundo Auxiliar',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '03',
                'descricao' => 'Terceiro Auxiliar',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '04',
                'descricao' => 'Quarto Auxiliar',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '05',
                'descricao' => 'Instrumentador',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '06',
                'descricao' => 'Anestesista',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '07',
                'descricao' => 'Auxiliar de Anestesista',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '08',
                'descricao' => 'Consultor',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '09',
                'descricao' => 'Perfusionista',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '10',
                'descricao' => 'Pediatra na sala de parto',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '11',
                'descricao' => 'Auxiliar SADT',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '12',
                'descricao' => 'Clínico',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '13',
                'descricao' => 'Intensivista',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'codigo_tiss' => '99',
                'descricao' => 'Não Aplicável',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    public function down()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS=0;");
        $this->execute("TRUNCATE tipo_equipegrau;");
    }
}
