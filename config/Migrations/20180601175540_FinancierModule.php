<?php
use Migrations\AbstractMigration;

class FinancierModule extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE configuracoes ADD COLUMN financier_module int(11) not null default 0');
        $this->execute('ALTER TABLE users ADD COLUMN financier_module int(11) not null default 0');
    }

    public function down()
    {
        $this->execute('ALTER TABLE configuracoes DROP COLUMN financier_module');
        $this->execute('ALTER TABLE users DROP COLUMN financier_module');
    }
}
