<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_agenda');
        $table->addColumn('reservado', 'integer', [
            'default' => 2,// Usuario criador e adin
            'limit' => 11,
            'null' => true,
            'comment' => '1 - todos alteram | 2 - usuário criador e admin',
        ]);
        $table->create();
    }
}
