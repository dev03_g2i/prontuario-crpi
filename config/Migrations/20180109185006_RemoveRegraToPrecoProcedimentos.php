<?php
use Migrations\AbstractMigration;

class RemoveRegraToPrecoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('preco_procedimentos');
        $table->removeColumn('regra');
    }
}
