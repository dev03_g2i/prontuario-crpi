<?php
use Migrations\AbstractMigration;

class UpdateSolicitantePutCbo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {   
        $table = $this->table('solicitantes');
        $table->removeColumn('tiss_cbos');
        $table->addColumn('cbo_id', 'integer', [
            'default' => -1,
            'null' => false,
            'limit' => 11,
        ]);
        $table->update();
        
        //add foreign key
        $table = $this->table('solicitantes');
        $table->addForeignKey(
            'cbo_id',
            'solicitante_cbo',
            'id',
            ['update' => 'RESTRICT','delete' => 'RESTRICT']
        );
        $table->update();
    }
    
    public function down()
    {
        $table = $this->table('solicitantes');
        $table->removeColumn('cbo_id');
        $table->addColumn('tiss_cbos', 'integer', [
            'default' => 0,
            'null' => false,
            'limit' => 11,
        ]);
    }
}
