<?php
use Migrations\AbstractMigration;

class CreateAnotacaoAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('anotacao_agendas');
        $table->addColumn('descricao', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ])->addColumn('data', 'date', [
            'default' => null,
            'null' => false,
        ])->addColumn('grupo_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'null' => false,
            'limit' => 11,
        ])->addColumn('user_id', 'integer', [
            'default' => 1,
            'null' => false,
            'limit' => 11,
        ])->addColumn('user_update', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        $table = $this->table('anotacao_agendas');
        $table->addForeignKey(
            'grupo_id',
            'grupo_agendas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();

    }
}
