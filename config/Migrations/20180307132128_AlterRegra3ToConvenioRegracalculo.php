<?php
use Migrations\AbstractMigration;

class AlterRegra3ToConvenioRegracalculo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("UPDATE convenio_regracalculo SET apelido = '100/70 - Integral' WHERE id = 3");
    }

    public function down()
    {
        $this->execute("UPDATE convenio_regracalculo SET apelido = '100/70/50 - Integral' WHERE id = 3");
    }
}
