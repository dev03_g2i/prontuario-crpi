<?php
use Migrations\AbstractMigration;

class AddAtendimentoIdToFaturaMatmed extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('SET foreign_key_checks=0');
        $table = $this->table('fatura_matmed');
        $table->addColumn('atendimento_id', 'integer', [
           'default' => null,
            'limit' => 11,
            'null' => true
        ])->addForeignKey(
            'atendimento_id',
            'atendimentos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
