<?php
use Migrations\AbstractMigration;

class ChangeCorTipoAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_agendas');
        $table->changeColumn('cor', 'string', [
            'default' => '#ffffff',
            'limit' => 7,
            'null' => true
        ]);
        $table->update();
        $this->execute(" UPDATE tipo_agendas SET cor = '#ffffff' WHERE 1=1 ");
    }
}
