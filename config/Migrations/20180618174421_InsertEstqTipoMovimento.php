<?php
use Migrations\AbstractMigration;

class InsertEstqTipoMovimento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('estq_tipo_movimento', [
            [
                'id' => 1,
                'descricao' => 'Saida',
                'user_id' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'descricao' => 'Discarte',
                'user_id' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'descricao' => 'Perda',
                'user_id' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ]);
    }

    public function down()
    {
        $this->execute("DELETE FROM estq_tipo_movimento WHERE id IN(1,2,3)");
    }
}
