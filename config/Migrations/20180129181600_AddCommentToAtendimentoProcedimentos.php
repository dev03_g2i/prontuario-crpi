<?php
use Migrations\AbstractMigration;

class AddCommentToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->changeColumn('solicitante_id', 'integer', [
            'null' => true,
            'limit' => 11,
            'comment' => 'verifica a configuração de solicitantes procedimentos - se habilitada salva solicitante selecionado no cadastro dos procedimentos, se não, salva o solicitante do atendimento'
        ]);
        $table->update();
    }
}
