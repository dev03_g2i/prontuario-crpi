<?php
use Migrations\AbstractMigration;

class InsertOrdemToSituacaoLaudos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('UPDATE situacao_laudos sl SET ordenar = sl.id WHERE 1=1');
    }

    public function down()
    {
        $this->execute('UPDATE situacao_laudos sl SET ordenar = NULL WHERE 1=1');
    }
}
