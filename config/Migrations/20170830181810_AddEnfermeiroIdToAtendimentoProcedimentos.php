<?php
use Migrations\AbstractMigration;

class AddEnfermeiroIdToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('enfermeiro_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ])
            ->addColumn('tecnico_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->addColumn('prioridade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->addColumn('revisor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->addForeignKey(
                'enfermeiro_id',
                'enfermeiros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'tecnico_id',
                'tecnicos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'prioridade_id',
                'prioridades',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
        ;
        $table->update();

    }
}
