<?php
use Migrations\AbstractMigration;

class InsertChatConfig extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('chat_module', 'integer', [
            'default' => 0,
            'null' => false,
            'limit' => 11,
            'comment' => '0 - Não | 1 - Sim'
        ]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('configuracoes');
        $table->removeColumn('chat_module');
        $table->update();
    }
}
