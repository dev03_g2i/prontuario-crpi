<?php
use Migrations\AbstractMigration;

class AddTissAtendimentoToAtendimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimentos');
        $table->addColumn('tiss_tipoatendimento', 'string', [
            'default' => '05',
            'limit' => 10
        ]);
        $table->update();
    }
}
