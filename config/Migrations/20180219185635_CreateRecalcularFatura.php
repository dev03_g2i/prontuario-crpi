<?php
use Migrations\AbstractMigration;

class CreateRecalcularFatura extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_recalcular');
        $table->addColumn('convenio_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
            ])->addColumn('periodo', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 100
            ])
            ->addColumn('user_created', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])->addColumn('user_update', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('fatura_recalcular');
        $table->addForeignKey(
            'convenio_id',
            'convenios',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'user_created',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
