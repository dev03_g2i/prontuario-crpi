<?php
use Migrations\AbstractMigration;

class CreateEstqKitArtigo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("
            CREATE TABLE `estq_kit_artigo` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `fatura_kit_id` int(11) NOT NULL,
            `artigo_id` int(11) NOT NULL,
            `quantidade` int(11) NOT NULL,
            `user_id` int(11) NOT NULL,
            `situacao_id` int(11) NOT NULL,
            `created` datetime NOT NULL,
            `modified` datetime NOT NULL,
            PRIMARY KEY (`id`)
            ) CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `estq_kit_artigo`;");
    }
}
