<?php
use Migrations\AbstractMigration;

class AddAgendaHorarioIdToAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agendas');
        $table->addColumn('agenda_horario_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])->addForeignKey(
            'agenda_horario_id',
            'grupo_agenda_horarios',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
