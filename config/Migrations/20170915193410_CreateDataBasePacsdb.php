<?php
use Migrations\AbstractMigration;

class CreateDataBasePacsdb extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        // Provisorio
        $this->execute('CREATE DATABASE pacsdb');
        $this->execute('use pacsdb');
        $this->execute('CREATE TABLE phinxlog ( version bigint(20), migration_name varchar(100), start_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, end_time TIMESTAMP, breakpoint tinyint(1))');
        $this->execute('CREATE TABLE study ( accession_no INT(11) NULL, pk INT(11) NULL , study_iuid INT(11) NULL )');
        $this->execute('CREATE TABLE series ( modality VARCHAR(20) NULL , study_fk INT(11) NULL )');
        $this->execute('CREATE TABLE instance ( modality VARCHAR(20) )');
    }
}
