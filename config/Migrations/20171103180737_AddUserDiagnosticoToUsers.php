<?php
use Migrations\AbstractMigration;

class AddUserDiagnosticoToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('user_diagnostico', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Tecnico | 1 - Radiologista'
        ]);
        $table->update();
    }
}
