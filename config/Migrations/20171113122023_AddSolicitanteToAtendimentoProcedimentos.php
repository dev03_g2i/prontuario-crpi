<?php
use Migrations\AbstractMigration;

class AddSolicitanteToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('solicitante_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ]);
        $table->addForeignKey('solicitante_id', 'solicitantes', 'id', [
            'update' => 'NO_ACTION',
            'delete' => 'NO_ACTION'
        ]);
        $table->update();
    }
}
