<?php
use Migrations\AbstractMigration;

class AddPrevEntregaAtendimentoToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('prev_entrega_proc', 'datetime', [
            'default' => null,
            'null' => true,
            'comment' => 'Previsão de entrega dos Procedimentos'
        ]);
        $table->update();
    }
}
