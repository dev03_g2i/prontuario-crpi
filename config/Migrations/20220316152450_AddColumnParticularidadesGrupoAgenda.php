<?php

use Migrations\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class AddColumnParticularidadesGrupoAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('grupo_agendas');
        if (!$table->hasColumn('particularidades')) {
            $table->addColumn('particularidades', 'text', ['limit' => MysqlAdapter::TEXT_LONG, 'null' => true, 'default' => null])->save();
        }
    }
}
