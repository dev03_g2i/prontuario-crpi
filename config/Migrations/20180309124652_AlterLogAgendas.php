<?php
use Migrations\AbstractMigration;

class AlterLogAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('log_agendas');
        $table->removeColumn('inicio');
        $table->removeColumn('cliente_id');
        $table->removeColumn('tipo_id');
        $table->removeColumn('situacao_agenda_id');
        $table->removeColumn('user_id');
        $table->removeColumn('obs');
        $table->removeColumn('procedimentos');
        $table->removeColumn('situacao_id');
        $table->addColumn('log', 'text', [
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
