<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoProdutividadeTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_produtividade');

        $table->addColumn('usa_produtividade', 'integer', [
            'default' => 1,
            'null' => true,
            'comment' => '0 = Não, 1 = Sim'
        ]);

        $table->addColumn('produtividade_executante', 'integer', [
            'default' => 1,
            'null' => true,
            'comment' => '0 = Não, 1 = Sim'
        ]);

        $table->addColumn('produtividade_solicitante', 'integer', [
            'default' => 1,
            'null' => true,
            'comment' => '0 = Não, 1 = Sim'
        ]);

        $table->create();
    }
}
