<?php
use Migrations\AbstractMigration;

class AddColumnGrupoUsuarioBotoesLaudo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $laudosConfig = $this->table('grupo_usuarios');

        if (!$laudosConfig->hasColumn('mostra_botao_laudo_edit_add_salvar')) {
            $laudosConfig->addColumn('mostra_botao_laudo_edit_add_salvar', 'integer', ['null' => true, 'default' => 0, 'comment' => '0 - não, 1 - sim.']);
        }

        if (!$laudosConfig->hasColumn('mostra_botao_laudo_edit_assinar')) {
            $laudosConfig->addColumn('mostra_botao_laudo_edit_assinar', 'integer', ['null' => true, 'default' => 0, 'comment' => '0 - não, 1 - sim.']);
        }

        if (!$laudosConfig->hasColumn('tela_edicao_laudo_mostra_botao_recusa_aceite_laudo')) {
            $laudosConfig->addColumn('tela_edicao_laudo_mostra_botao_recusa_aceite_laudo', 'integer', ['null' => true, 'default' => 1, 'comment' => '0 - não, 1 - sim.']);
        }
        $laudosConfig->update();
    }
}
