<?php
use Migrations\AbstractMigration;

class AlterTableAgentaTelCategoryAndObservationToText extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE agenda_tel MODIFY COLUMN category text(400) NULL;');
        $this->execute('ALTER TABLE agenda_tel MODIFY COLUMN observation text(1000) NULL;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE agenda_tel MODIFY COLUMN category varchar(255) NULL;');
        $this->execute('ALTER TABLE agenda_tel MODIFY COLUMN observation text(255) NULL;');
    }
}
