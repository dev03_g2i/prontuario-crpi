<?php
use Migrations\AbstractMigration;

class AddProducaoPgDtToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('producao_pg_dt', 'date', [
            'default' => null,
            'null' => true,
            'comment' => 'Pagamento Produção'
        ]);
        $table->addColumn('producao_pg_user', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'comment' => 'Pagamento Produção'
        ]);
        $table->addColumn('producao_pg_dtuser', 'datetime', [
            'default' => null,
            'null' => true,
            'comment' => 'Pagamento Produção'
        ]);
        $table->update();
    }
}
