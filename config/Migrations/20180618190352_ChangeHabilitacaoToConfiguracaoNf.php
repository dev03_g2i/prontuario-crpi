<?php
use Migrations\AbstractMigration;

class ChangeHabilitacaoToConfiguracaoNf extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_nf');
        $table->removeColumn('habilitacao')->save();
        $table->addColumn('habilitacao', 'integer', [
            'limit' => 11,
            'default' => 0,// Nao habilitado
            'null' => true,
            'comment' => '0 - não habilitado | 1 - habilitado'
        ])->update();
    }
}
