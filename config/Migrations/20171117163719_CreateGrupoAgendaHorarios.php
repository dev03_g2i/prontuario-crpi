<?php
use Migrations\AbstractMigration;

class CreateGrupoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('grupo_agenda_horarios');
        $table->addColumn('dia_semana', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
            'comment' => '1-segunda/2-terça/...0-Domingo'
        ])->addColumn('data_inicio', 'date', [
            'default' => null,
            'null' => false,
        ])->addColumn('data_fim', 'date', [
            'default' => null,
            'null' => false,
        ])->addColumn('hora_inicio', 'time', [
            'default' => null,
            'null' => false,
        ])->addColumn('hora_fim', 'time', [
            'default' => null,
            'null' => false,
        ])->addColumn('intervalo', 'time', [
            'default' => null,
            'null' => false,
        ])->addColumn('grupo_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('tipo_agenda_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => false
        ])->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ])->addForeignKey(
            'grupo_id',
            'grupo_agendas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'tipo_agenda_id',
            'tipo_agendas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->create();
    }
}
