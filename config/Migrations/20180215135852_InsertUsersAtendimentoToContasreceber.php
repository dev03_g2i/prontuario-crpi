<?php

use Cake\Datasource\ConnectionManager;
use Migrations\AbstractMigration;

class InsertUsersAtendimentoToContasreceber extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $dataSourceDefault = ConnectionManager::get('default');
        $dataSourceFinanceiro = ConnectionManager::get('financeiro');
        $database_default = $dataSourceDefault->config()['database'];
        $database_financeiro = $dataSourceFinanceiro->config()['database'];
        $this->execute("UPDATE $database_financeiro.contasreceber cr JOIN $database_default.atendimentos a ON cr.extern_id = a.id SET cr.user_created = a.user_id, cr.user_modified = a.user_update, cr.created = a.created, cr.modified = a.modified");
    }

    public function down()
    {

    }
}
