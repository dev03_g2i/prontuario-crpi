<?php
use Migrations\AbstractMigration;

class AddImprimirFichaSemreceberToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('imprimir_ficha_semreceber', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não libera | 1 - libera imprimir ficha'
        ]);
        $table->update();
    }
}
