<?php
use Migrations\AbstractMigration;

class AddProcedimetoToProdutividadeSolicitantes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('produtividade_solicitantes');
        $table->addColumn('procedimento_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ]);
        $table->addForeignKey('procedimento_id', 'procedimentos', 'id', [
            'delete' => 'NO_ACTION',
            'update' => 'NO_ACTION'
        ]);
        $table->update();
    }
}
