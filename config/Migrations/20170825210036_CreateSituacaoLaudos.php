<?php
use Migrations\AbstractMigration;

class CreateSituacaoLaudos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('situacao_laudos');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255
        ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();
    }
}
