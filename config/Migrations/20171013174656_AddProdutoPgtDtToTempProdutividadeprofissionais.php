<?php
use Migrations\AbstractMigration;

class AddProdutoPgtDtToTempProdutividadeprofissionais extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('temp_produtividadeprofissionais');
        $table->addColumn('producao_pg_dt', 'date', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
