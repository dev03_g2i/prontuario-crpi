<?php
use Migrations\AbstractMigration;

class AddValoresToPrecoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('preco_procedimentos');
        $table->addColumn('uco', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->addColumn('porte', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->addColumn('filme', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->update();
    }
}
