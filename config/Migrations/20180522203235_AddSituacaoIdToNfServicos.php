<?php
use Migrations\AbstractMigration;

class AddSituacaoIdToNfServicos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('nf_servicos');
        $table->removeColumn('valor_unitario');
        $table->removeColumn('valor_total');
        $table->addColumn('valor_unitario', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ])->addColumn('valor_total', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ])->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
        ])
        ->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->update();
    }
}
