<?php
use Migrations\AbstractMigration;

class CreateTextos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('textos');
        $table
            ->addColumn('codigo', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('rtf', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true
            ])
            ->addColumn('texto', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true
            ])
            ->addColumn('texto_html', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ])
            ->addColumn('grupo_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('textos');
        $table->addForeignKey(
            'grupo_id',
            'texto_grupos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }
}
