<?php

use Migrations\AbstractMigration;

class AddTissFieldsToConvenio extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('convenios');
        $table->addColumn('tiss_registroans', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->addColumn('tiss_codcliconvenio', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->addColumn('tiss_tipocodigo', 'enum', [
            'values' => ['CNPJ', 'cpf', 'codigoPrestadorNaOperadora', 'conselhoProfissional']
        ]);

        $table->addColumn('tiss_codtabpreco', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->addColumn('tiss_codtabmedicamento', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->addColumn('tiss_codtabmaterial', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->addColumn('tiss_codtaxas', 'string', [
            'default' => '',
            'limit' => 255
        ]);

        $table->update();
    }
}
