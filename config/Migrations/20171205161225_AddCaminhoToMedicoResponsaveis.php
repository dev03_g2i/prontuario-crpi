<?php
use Migrations\AbstractMigration;

class AddCaminhoToMedicoResponsaveis extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('medico_responsaveis');
        $table->addColumn('caminho', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])->addColumn('caminho_dir', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ])->addColumn('descricao_anexo', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->update();
    }
}
