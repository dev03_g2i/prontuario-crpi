<?php
use Migrations\AbstractMigration;

class AddControleFinanceiroToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('controle_financeiro_id', 'integer', [
            'default' => -1,
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
