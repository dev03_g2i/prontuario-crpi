<?php
use Migrations\AbstractMigration;

class AddReporFichaPacienteToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('repor_ficha_paciente', 'string', [
            'limit' => 255,
            'comment' => 'Configuração para setar qual vai ser a ficha de cadastro do paciente',
            'default' => 'ficha_cadastro_paciente01.mrt'
        ]);
        $table->update();
    }
}
