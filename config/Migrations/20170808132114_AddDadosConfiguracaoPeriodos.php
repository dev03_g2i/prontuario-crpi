<?php
use Migrations\AbstractMigration;
use Cake\I18n\Time;

/**
 * Class AddDadosConfiguracaoPeriodos
 * @Author Iohan
 */
class AddDadosConfiguracaoPeriodos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('configuracao_periodos_agenda', [
            [
                'id' => 1,
                'nome' => 'Manhã',
                'inicio_agenda' => new Time('07:00:00'),
                'fim_agenda' => new Time('12:59:59'),
                'inicio_atendimento' => new Time('07:00:00'),
                'fim_atendimento' => new Time('12:59:59'),
                'created' => Time::now(),
                'modified' => Time::now(),
                'user_id' => 1,
            ],
            [
                'id' => 2,
                'nome' => 'Tarde',
                'inicio_agenda' => new Time('13:00:00'),
                'fim_agenda' => new Time('18:59:59'),
                'inicio_atendimento' => new Time('13:00:00'),
                'fim_atendimento' => new Time('18:59:59'),
                'created' => Time::now(),
                'modified' => Time::now(),
                'user_id' => 1,
            ],
            [
                'id' => 3,
                'nome' => 'Noite',
                'inicio_agenda' => new Time('19:00:00'),
                'fim_agenda' => new Time('23:59:59'),
                'inicio_atendimento' => new Time('19:00:00'),
                'fim_atendimento' => new Time('23:59:59'),
                'created' => Time::now(),
                'modified' => Time::now(),
                'user_id' => 1,
            ]
        ]);
    }
}
