<?php
use Migrations\AbstractMigration;

class InsertTipoEspera extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('tipo_esperaagenda', [
            [
                'id' => 1,
                'descricao' => 'Vaga',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'descricao' => 'Antecipação',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ]);
    }
    public function down()
    {
        $this->execute('DELETE FROM tipo_esperaagenda WHERE id IN(1,2)');
    }
}
