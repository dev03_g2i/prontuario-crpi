<?php
use Migrations\AbstractMigration;

class CreateProcedimentoCaddetalhes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('procedimento_caddetalhes');
        $table->addColumn('nome', 'string', [
            'limit' => '255',
            'null' => false,
            'default' => null,
        ])->addColumn('situacao_id', 'integer', [
            'limit' => 11,
            'default' => null,
            'null' => false,
        ])->addColumn('user_id', 'integer', [
            'limit' => 11,
            'default' => null,
            'null' => false,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
