<?php
use Migrations\AbstractMigration;

class InsertTipoEsperaToTipoEspera extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS=0");
        $this->execute("TRUNCATE TABLE tipo_esperaagenda");
        $this->insert('tipo_esperaagenda', [
            [
            'id' => 1,
            'descricao' => 'Em espera',
            'situacao_id' => 1,
            'user_id' => 1,
            'status_tipoespera_id' => 1,
            'created' => date('Y-m-d'),
            'modified' => date('Y-m-d'),
            ],
            [
            'id' => 2,
            'descricao' => 'Finalizado',
            'situacao_id' => 1,
            'user_id' => 1,
            'status_tipoespera_id' => 1,
            'created' => date('Y-m-d'),
            'modified' => date('Y-m-d'),
            ]
        ]);
    }
    public function down()
    {
        $this->execute("DELETE FROM tipo_esperaagenda WHERE id IN(1,2)");
    }
}
