<?php
use Migrations\AbstractMigration;

class InsertConfNfs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('INSERT INTO configuracao_nf (pasta_pdf, habilitacao) VALUES ("teste", "1")');
    }

    public function down()
    {
        $this->execute('DELETE FROM configuracao_nf where pasta_pdf LIKE "teste"');
    }
}
