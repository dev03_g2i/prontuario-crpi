<?php
use Migrations\AbstractMigration;

class AddControleFinancsetorReceitaPadraoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('controle_financsetor_receita_padrao', 'integer', [
            'default' => '-1',
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
