<?php
use Migrations\AbstractMigration;

class CreateFaturaMatmed extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $table = $this->table('fatura_matmed');
        $table->addColumn('data', 'datetime', [
            'default' => null,
            'null' => false
        ])
            ->addColumn('artigo_id', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('quantidade', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('vl_custos', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('vl_venda', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('vl_custo_medico', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('origin', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('codigo_tiss', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 10
            ])
            ->addColumn('codigo_tuss', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 10
            ])
            ->addColumn('codigo_convenio', 'string', [
                'default' => null,
                'null' => true,
                'limit' => 10
            ])
            ->addColumn('atendimento_procedimento_id', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('fatura_matmed');
        $table->addForeignKey(
            'atendimento_procedimento_id',
            'atendimento_procedimentos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
