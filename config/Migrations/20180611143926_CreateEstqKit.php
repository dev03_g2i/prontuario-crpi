<?php
use Migrations\AbstractMigration;

class CreateEstqKit extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("DROP TABLE IF EXISTS `estq_kit`;");
        $this->execute("
            CREATE TABLE `estq_kit` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `nome` varchar(255) NOT NULL,
            `procedimento_id` int(11) DEFAULT NULL,
            `user_id` int(11) NOT NULL,
            `situacao_id` int(11) NOT NULL,
            `created` datetime NOT NULL,
            `modified` datetime NOT NULL,
            `user_update` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `procedimento_id` (`procedimento_id`),
            KEY `user_id` (`user_id`),
            KEY `situacao_id` (`situacao_id`),
            FOREIGN KEY (`procedimento_id`) REFERENCES `procedimentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY (`situacao_id`) REFERENCES `situacao_cadastros` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) CHARSET=utf8;
        ");
    }

    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `estq_kit`;");
    }
}
