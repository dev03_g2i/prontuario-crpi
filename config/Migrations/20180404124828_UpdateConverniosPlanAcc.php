<?php
use Migrations\AbstractMigration;

class UpdateConverniosPlanAcc extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('convenios');
        $table->addColumn('planAcc_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ]);
        $table->update();
    }

    public function down()
    {   
        $table = $this->table('convenios');
        $table->removeColumn('planAcc_id');
        $table->update();
    }
}
