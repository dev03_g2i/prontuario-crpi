<?php
use Migrations\AbstractMigration;

class InsertGrupoAgendaDefaultToGrupoAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('grupo_agendas', [
            'id' => -1,
            'nome' => 'G2i',
            'situacao_id' => 2,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'medico_id' => null,
            'intervalo' => '00:20:00',
            'inicio' => '08:00:00',
            'fim' => '18:00:00',
        ]);
    }
}
