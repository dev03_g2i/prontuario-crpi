<?php
use Migrations\AbstractMigration;

class CreateAgendaTel extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('agenda_tel');
        $table
            ->addColumn('name', 'string', [
                'limit' => 150,
                'null' => false
            ])
            ->addColumn('address', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('district', 'string', [
                'limit' => 75,
                'null' => true
            ])
            ->addColumn('zipCode', 'string', [
                'limit' => 10,
                'null' => true
            ])
            ->addColumn('city', 'string', [
                'limit' => 55,
                'null' => true
            ])
            ->addColumn('state', 'string', [
                'limit' => 55,
                'null' => true
            ])
            ->addColumn('phones', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('email', 'string', [
                'limit' => 55,
                'null' => true
            ])
            ->addColumn('category', 'string', [
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('observation', 'text', [
                'limit' => 255,
                'null' => true
            ]);
        $table->create();
    }

    public function down()
    {
        $table = $this->table('agenda_tel');
        $table->drop();
    }
}
