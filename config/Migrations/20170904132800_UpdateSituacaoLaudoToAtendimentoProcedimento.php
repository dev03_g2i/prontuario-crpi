<?php
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class UpdateSituacaoLaudoToAtendimentoProcedimento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = TableRegistry::get('AtendimentoProcedimentos');
        $table->query()->update()->set(['situacao_laudo_id' => 2])->where(['1=1'])->execute();
    }
}
