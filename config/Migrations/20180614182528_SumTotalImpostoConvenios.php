<?php
use Migrations\AbstractMigration;

class SumTotalImpostoConvenios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('alter table convenios add column total_imposto decimal(6,2) not null default 0');
        $this->execute('UPDATE convenios c INNER JOIN(SELECT convenio_id, SUM(porcentual) "sumu" FROM convenio_impostos_parametro where convenio_impostos_parametro.situacao_id = 1 GROUP BY convenio_id) cip ON c.id = cip.convenio_id SET c.total_imposto = cip.sumu;');
    }

    public function down()
    {
        $this->execute('alter table convenios drop column total_imposto;');
    }
}
