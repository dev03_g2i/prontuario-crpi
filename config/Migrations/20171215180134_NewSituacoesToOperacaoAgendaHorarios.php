<?php
use Migrations\AbstractMigration;

class NewSituacoesToOperacaoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('operacao_agenda_horarios', [
            [
                'id' => 1,
                'nome' => 'Agendar',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'nome' => 'Bloquear',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'nome' => 'Desbloquear',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 4,
                'nome' => 'Excluir',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ]);
    }

    public function down()
    {
        $this->execute('DELETE FROM operacao_agenda_horarios WHERE id IN (1,2,3, 4)');
    }
}
