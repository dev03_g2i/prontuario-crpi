<?php
use Migrations\AbstractMigration;

class NewSituacaoLaudos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS=0;");
        $this->execute("TRUNCATE TABLE situacao_laudos;");
        $this->insert('situacao_laudos',
            [
                [
                    'id' => 200,
                    'nome' => 'Realizado',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 100,
                    'nome' => 'Em espera',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 600,
                    'nome' => 'Corrigir',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 300,
                    'nome' => 'Audio Gravado',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 400,
                    'nome' => 'Em digitação',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 500,
                    'nome' => 'Digitado',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 700,
                    'nome' => 'Revisado',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 800,
                    'nome' => 'Assinado',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 900,
                    'nome' => 'Impresso',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 1000,
                    'nome' => 'Envelopado',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ],
                [
                    'id' => 2000,
                    'nome' => 'Entregue',
                    'situacao_id' => 1,
                    'user_id' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ]
            ]
        );
    }
}
