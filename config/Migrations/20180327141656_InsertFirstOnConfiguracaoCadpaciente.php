<?php
use Migrations\AbstractMigration;

class InsertFirstOnConfiguracaoCadpaciente extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('configuracao_cadpaciente',
            [
                'id' => 1,
                'religiao' => 1,
                'indicao' => 3
            ]);
    }
}
