<?php
use Migrations\AbstractMigration;

class InsertBancoInConvenios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('convenios');
        $table->addColumn('bank_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ])
        ->addColumn('pymt_date', 'integer', [
            'null' => true,
            'limit' => 11
        ])
        ->addColumn('pymt_hist', 'integer', [
            'limit' => 11,
            'null' => true
        ])
        ->addColumn('tel', 'string', [
            'limit' => 255,
            'null' => true
        ])
        ->addColumn('contact', 'string', [
            'limit' => 255,
            'null' => true
        ]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('convenios');
        $table->removeColumn('bank_id')
        ->removeColumn('pymt_date')
        ->removeColumn('pymt_hist')
        ->removeColumn('tel')
        ->removeColumn('contact');
        $table->update();
    }
}
