<?php
use Migrations\AbstractMigration;

class CreateFaturaRecursos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_recursos');
        $table
            ->addColumn('data', 'date', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('valor', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('obs', 'text', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('situacao_fatura_recurso_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('atendimento_procedimento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('fatura_recursos');
        $table->addForeignKey(
            'situacao_fatura_recurso_id',
            'situacao_fatura_recursos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'atendimento_procedimento_id',
                'atendimento_procedimentos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
