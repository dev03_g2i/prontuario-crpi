<?php
use Migrations\AbstractMigration;

class AddAnotacaoToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('anotacao_laudo', 'text', [
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
