<?php
use Migrations\AbstractMigration;

class InsertSolicitanteCbo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('solicitante_cbo', [
            'id' => -1,
            'descricao' => 'Não informado',
            'codigo_tiss' => null,
            'situacao_id' => 1,
            'user_insert' => 1,
            'created' => date("Y-m-d H:i:s"),
            'user_update' => 1,
            'modified' => date("Y-m-d H:i:s")
        ]);
    }
    public function down()
    {
        $this->execute("DELETE FROM solicitante_cbo WHEREid id = -1");
    }
}
