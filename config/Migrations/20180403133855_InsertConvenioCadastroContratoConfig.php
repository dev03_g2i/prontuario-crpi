<?php
use Migrations\AbstractMigration;

class InsertConvenioCadastroContratoConfig extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('convenio_cadastro_contrato', 'integer', [
            'default' => 0,
            'null' => false,
            'limit' => 11,
            'comment' => '0 - Não | 1 - Sim'
        ]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('configuracoes');
        $table->removeColumn('convenio_cadastro_contrato');
        $table->update();
    }
}
