<?php
use Migrations\AbstractMigration;

class InsertPorcentagemGrupoGrau extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('tipo_equipegrau');
        $table->addColumn('porcentual', 'integer', [
            'default' => 0,
            'null' => false,
            'limit' => 11,
        ]);
        $table->update();
    }
    public function down()
    {
        $table = $this->table('tipo_equipegrau');
        $table->removeColumn('porcentual');
        $table->update();
    }
}
