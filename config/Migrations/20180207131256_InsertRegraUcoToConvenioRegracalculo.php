<?php
use Migrations\AbstractMigration;

class InsertRegraUcoToConvenioRegracalculo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('convenio_regracalculo', [
            'id' => 4,
           'apelido' => '100/700 - Uco',
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
        ]);

    }
    public function down()
    {
        $this->execute('DELETE FROM convenio_regracalculo WHERE id = 4');
    }
}
