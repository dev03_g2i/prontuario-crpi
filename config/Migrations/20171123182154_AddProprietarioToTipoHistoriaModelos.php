<?php
use Migrations\AbstractMigration;

class AddProprietarioToTipoHistoriaModelos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_historia_modelos');
        $table->addColumn('proprietario', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
            'comment' => 'Vinculado ao medico responsavel'
        ]);
        $table->update();
    }
}
