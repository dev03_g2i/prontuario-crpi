<?php
use Migrations\AbstractMigration;

class InsertReservadoToConfiguracaoAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('configuracao_agenda', [
            'id' => 1,
            'reservado' => 2,
        ]);
    }

    public function down()
    {
        $this->execute('DELETE FROM configuracao_agenda WHERE id = 1');
    }
}
