<?php
use Migrations\AbstractMigration;

class AddCamposObrigatoriosToConvenios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('convenios');
        $table->addColumn('usa_tiss', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])
            ->addColumn('usa_cobranca', 'integer', [
                'default' => 0,
                'limit' => 11,
                'null' => true,
                'comment' => '0 - Não | 1 - Sim'
            ])
            ->addColumn('obrigatorio_nr_guia', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])->addColumn('obrigatorio_autorizacao_senha', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ])->addColumn('obrigatorio_carteira_paciente', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ]);
        $table->update();
    }
}
