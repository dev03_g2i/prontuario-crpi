<?php
use Migrations\AbstractMigration;

class CreateTempProdutividadeprofissionais extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('temp_produtividadeprofissionais');
        $table->addColumn('atendimento_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ])
            ->addColumn('convenio_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->addColumn('convenio', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('profissional', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('data_atendimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true
            ])
            ->addColumn('hora', 'time', [
                'default' => null,
                'limit' => null,
                'null' => true
            ])
            ->addColumn('paciente', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('codigo', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true
            ])
            ->addColumn('procedimento', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('quantidade', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->addColumn('valor_fatura', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('valor_caixa', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('valor_material', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('total', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('perc_recebimento', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('perc_imposto', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2
            ])
            ->addColumn('valor_prod_fatura', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('valor_prod_caixa', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('valor_prodrec_convenio', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ]);

        $table->create();
    }
}
