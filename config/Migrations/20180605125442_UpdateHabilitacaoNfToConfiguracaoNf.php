<?php
use Migrations\AbstractMigration;

class UpdateHabilitacaoNfToConfiguracaoNf extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('configuracao_nf');
        $table->changeColumn('habilitacao', 'integer', [
            'limit' => 11,
            'null' => true,
            'default' => 0,
            'comment' => '0 - habilitado | 1 - não habilitado',
        ])->save();
        
    }
    
}
