<?php
use Migrations\AbstractMigration;

class CreateAgendaFilaespera extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agenda_filaespera');
        $table
        ->addColumn('paciente', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ])
        ->addColumn('fone', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 14,
        ])
        ->addColumn('convenio_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('tipo_espera_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('grupo_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])
        ->addColumn('dt_agendado', 'datetime', [
            'default' => null,
            'null' => true,
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        $table = $this->table('agenda_filaespera');
        $table->addForeignKey(
            'convenio_id',
            'convenios',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'tipo_espera_id',
            'tipo_esperaagenda',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'grupo_id',
            'grupo_agendas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
