<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoRelCaixaUsuario extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        if(!$this->hasTable('configuracao_rel_caixa_usuario')){
            $table = $this->table('configuracao_rel_caixa_usuario');

            $table->addColumn('user_id', 'integer',[
                'null' => true,
                'default' => null
            ]);

            $table->addColumn('configuracao_rel_caixa_id', 'integer',[
                'null' => true,
                'default' => null
            ]);
            $table->addForeignKey('user_id', 'users', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);
            $table->addForeignKey('configuracao_rel_caixa_id', 'configuracao_rel_caixa', 'id', ['delete' => 'NO_ACTION', 'update' => 'NO_ACTION']);
            $table->create();
        }
    }
}
