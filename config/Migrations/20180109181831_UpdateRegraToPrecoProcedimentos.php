<?php
use Migrations\AbstractMigration;

class UpdateRegraToPrecoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('UPDATE preco_procedimentos pp SET pp.convenio_regracalculo_id = CASE WHEN pp.regra <> 1 THEN 2 ELSE 1 END WHERE 1=1');
    }
    public function down()
    {
        $this->execute('UPDATE preco_procedimentos pp SET pp.convenio_regracalculo_id = 1 WHERE 1=1');
    }
}
