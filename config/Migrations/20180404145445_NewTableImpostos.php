<?php
use Migrations\AbstractMigration;

class NewTableImpostos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('convenio_tipoimposto')
        ->addColumn('descricao', 'string', [
            'default' => null,
            'limit' => 255
        ])
        ->addColumn('valor_inicial', 'decimal', [
            'default' =>  null,
            'precision' => 11,
            'scale' => 2
        ])
        ->addColumn('valor_final', 'decimal', [
            'default' => null,
            'precision' => 11,
            'scale' => 2
        ])
        ->addColumn('porcentual', 'decimal', [
            'default' => null,
            'precision' => 5,
            'scale' => 2
        ])
        ->addColumn('retido', 'integer', [
            'default' => 0
        ])
        ->addColumn('observacao', 'text', [
            'default' => null,
            'null' => true
        ])
        ->addColumn('user_created', 'integer', [
            'default' => null,
            'limit' => 11
        ])
        ->addColumn('user_updated', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])
        ->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true
        ])
        ->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->create();
    }

    public function down()
    {
        $table = $this->table('convenio_tipoimposto');
        $table->drop();
    }
}
