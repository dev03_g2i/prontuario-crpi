<?php
use Migrations\AbstractMigration;

class AddValoresToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('valor_recebido', 'decimal', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('dt_recebimento', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
