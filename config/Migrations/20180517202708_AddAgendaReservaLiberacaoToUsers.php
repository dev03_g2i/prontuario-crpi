<?php
use Migrations\AbstractMigration;

class AddAgendaReservaLiberacaoToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('agenda_reserva_liberacao', 'integer', [
            'default' => 1,// usuario sem permissao
            'limit' => 11,
            'null' => true,
            'comment' => '1 - usuario sem permissao | 2 - usuario permissao liberar',
        ]);
        $table->update();
    }
}
