<?php
use Migrations\AbstractMigration;

class AddDataAtendimentoFormatadaToTempProdutividadeprofissionais extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('temp_produtividadeprofissionais');
        $table->addColumn('data_atendimento_formatada', 'string', [
            'limit' => 255,
            'null' => true,
        ]);
        $table->update();
    }
}
