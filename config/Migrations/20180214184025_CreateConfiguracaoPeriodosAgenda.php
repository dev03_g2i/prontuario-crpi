<?php

use Migrations\AbstractMigration;

class CreateConfiguracaoPeriodosAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_periodos_agenda');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'null' => false,
        ]);
        $table->addColumn('hora_inicial', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('hora_final', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->create();

        $this->insert('configuracao_periodos_agenda',
            [
                'id' => 1,
                'nome' => 'Todos',
                'situacao_id' => 1,
                'hora_inicial' => '00:00:00',
                'hora_final' => '23:59:59'
            ]);

        $this->insert('configuracao_periodos_agenda',
            [
                'id' => 2,
                'nome' => 'Manhã',
                'situacao_id' => 1,
                'hora_inicial' => '00:00:00',
                'hora_final' => '11:59:59'
            ]);

        $this->insert('configuracao_periodos_agenda',
            [
                'id' => 3,
                'nome' => 'Tarde',
                'situacao_id' => 1,
                'hora_inicial' => '12:00:00',
                'hora_final' => '23:59:59'
            ]);
    }
}
