<?php
use Migrations\AbstractMigration;

class UpdadteAtendimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimentos');
        $table->addColumn('configuracao_periodo_id','integer',[
            'default' => null,
            'null' => true
        ]);
        $table->addForeignKey(
            'configuracao_periodo_id',
            'configuracao_periodos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
