<?php
use Migrations\AbstractMigration;

class AddIntegracaoMvSolicitanteToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('integracao_mv_solicitante', 'integer', [
            'limit' => 10,
            'comment' => 'Caso for 1 vai puxar o solicitante do atendimento procedimento, caso for 0 vai puxar o solicitante do atendimento',
            'default' => 0
        ]);
        $table->update();
    }
}
