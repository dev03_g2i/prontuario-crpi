<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoEstoque extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_estoque');
        $table->addColumn('habilitado', 'integer', [
            'null' => true,
            'limit' => 11,
            'default' => 0,// Não
            'comment' => '0 - Não habilitado | 1 - habilitado'
        ]);
        $table->create();
    }
}
