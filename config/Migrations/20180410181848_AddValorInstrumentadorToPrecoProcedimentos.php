<?php
use Migrations\AbstractMigration;

class AddValorInstrumentadorToPrecoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('preco_procedimentos');
        $table->addColumn('valor_instrumentador', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->update();
    }
}
