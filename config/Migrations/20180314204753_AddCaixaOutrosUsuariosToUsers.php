<?php
use Migrations\AbstractMigration;

class AddCaixaOutrosUsuariosToUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table->addColumn('caixa_outros_usuarios', 'integer', [
            'limit' => 10,
            'comment' => 'Configuração para definir se o usuário vai poder imprirmir relatório de outros usuários. 0 - não, 1 - sim',
            'default' => 0
        ]);
        $table->update();
    }
}
