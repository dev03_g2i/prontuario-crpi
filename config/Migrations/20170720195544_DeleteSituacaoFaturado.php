<?php
use Cake\ORM\TableRegistry;
use Migrations\AbstractMigration;

class DeleteSituacaoFaturado extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = TableRegistry::get('SituacaoFaturas');
        $situacaoFatura = $table->get(3);
        $situacaoFatura->situacao_id = 2;

        $table->save($situacaoFatura);
    }
}
