<?php
use Migrations\AbstractMigration;

class InsertInternacaoOnTipoAtendimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('tipo_atendimentos',
            [
                'id' => 10,
                'nome' => 'Internação',
                'situacao_id' => 2,
                'user_id' => 1,
                'created' => '2018-03-20 00:00:00',
                'modified' => '2018-03-20 00:00:00'
            ]);

    }
}
