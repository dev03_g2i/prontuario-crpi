<?php
use Migrations\AbstractMigration;

class AddSmsConfirmacaoToGrupoAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('grupo_agendas');
        $table->addColumn('sms_confirmacao', 'integer', [
            'default' => 0,
            'limit' => 4,
            'null' => true,
            'comment' => '0 Não Enviar - 1 Enviar'
        ])
            ->addColumn('sms_txt', 'string', [
                'limit' => 500,
                'null' => true,
                'comment' => 'Ex.: Confirmar  a consulta com o Dr. Teste para o dia @dia as @hora.'
            ]);
        $table->update();
    }
}
