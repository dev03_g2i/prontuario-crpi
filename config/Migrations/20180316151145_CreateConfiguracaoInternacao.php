<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoInternacao extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_internacao');
        $table->addColumn('carater_atendimento', 'integer', [
            'limit' => 11,
            'default' => 0,
            'null' => true
        ]);
        $table->addColumn('carater_atendimento_padrao', 'integer', [
            'limit' => 100,
            'default' => 0,
            'null' => true
        ]);
        $table->addColumn('acomodacao', 'integer', [
            'limit' => 11,
            'default' => 0,
            'null' => true
        ]);
        $table->addColumn('acomodacao_padrao', 'integer', [
            'limit' => 100,
            'default' => 0,
            'null' => true
        ]);
        $table->addColumn('motivo_alta', 'integer', [
            'limit' => 11,
            'default' => 0,
            'null' => true
        ]);
        $table->addColumn('motivo_alta_padrao', 'integer', [
            'limit' => 100,
            'default' => 0,
            'null' => true
        ]);
        $table->create();
    }
}
