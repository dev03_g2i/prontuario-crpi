<?php
use Migrations\AbstractMigration;

class AddMostraMenuSuperiorRelatorioSUSInUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        
        if (!$table->hasColumn('mostra_menu_superior_relatorio_sus')) {
            $table->addColumn('mostra_menu_superior_relatorio_sus', 'integer', [
                'null' => true,
                'default' => 1,
                'comment' => '0 - não mostra, 1 - mostra. 1=Padrão. Mostra/esconde o menu superior de relatório do SUS'
            ]);
        }
        
        $table->update();
    }
}
