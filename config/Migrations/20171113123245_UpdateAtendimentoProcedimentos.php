<?php
use Migrations\AbstractMigration;

class UpdateAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('UPDATE atendimento_procedimentos ap SET ap.solicitante_id = (SELECT a.`solicitante_id` FROM atendimentos a WHERE a.id = ap.`atendimento_id`)');
    }
}
