<?php
use Migrations\AbstractMigration;

class CreateAtendimentoProcedimentoEquipes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimento_equipes');
        $table->addColumn('ordem', 'integer', [
            'default' => null,
            'null' => true,
        ])->addColumn('atendimento_procedimento_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('profissional_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('tipo_equipe_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('tipo_equipegrau_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('porcentagem', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11,
        ])->addColumn('configuracao_apuracao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('obs', 'text', [
            'default' => null,
            'null' => true
        ])->addColumn('user_insert', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('user_update', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11,
        ])->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        $table = $this->table('atendimento_procedimento_equipes');
        $table->addForeignKey(
            'profissional_id',
            'medico_responsaveis',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'tipo_equipe_id',
            'tipo_equipes',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'tipo_equipegrau_id',
            'tipo_equipegrau',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'configuracao_apuracao_id',
            'configuracao_apuracao',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'user_insert',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
