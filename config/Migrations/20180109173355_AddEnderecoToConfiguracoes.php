<?php

use Migrations\AbstractMigration;

class AddEnderecoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('endereco', 'string', [
            'default' => null,
            'null' => true,
            'length' => 255
        ]);
        $table->addColumn('cnpj', 'string',[
            'default' => null,
            'null' => true,
            'length' => 255
        ]);
        $table->update();
    }
}
