<?php
use Migrations\AbstractMigration;

class AddFichaImpressaAtendimentoOnConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('ficha_impressa_atendimento', 'string', [
            'default' => 'ficha_atendimento_prot_rodape.mrt',
            'limit' => 255,
            'null' => true,
            'comment' => 'Campo para configurar qual será o mrt padrão para imprirmir a ficha'
        ]);
        $table->update();
    }
}
