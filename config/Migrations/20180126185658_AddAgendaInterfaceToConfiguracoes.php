<?php
use Migrations\AbstractMigration;

class AddAgendaInterfaceToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('agenda_interface', 'integer', [
            'default' => 3,// Lista/calendario
            'limit' => 11,
            'null' => true,
            'comment' => '1 - Lista | 2 - Calendario | 3 - ambos'
        ])->addColumn('agenda_interface_lista', 'integer', [
            'default' => 1,// Usa geração
            'limit' => 11,
            'null' => true,
            'comment' => '1 - Usa geração | 2 - Não usa'
        ]);
        $table->update();
    }
}
