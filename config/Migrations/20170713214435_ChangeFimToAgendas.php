<?php
use Migrations\AbstractMigration;

class ChangeFimToAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agendas');
        $table->changeColumn('fim', 'datetime',[
            'null' => false
        ]);
        $table->update();
    }
}
