<?php
use Migrations\AbstractMigration;

class ChangeLogToLogLaudos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute("TRUNCATE TABLE log_laudos;");
        $table = $this->table('log_laudos');
        $table->removeColumn('log');
        $table->addColumn('texto', 'text', [
            'default' => null,
            'null' => true,
        ])->addColumn('rtf', 'text', [
            'default' => null,
            'null' => true,
        ])->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])->addColumn('assinado_por', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])->addColumn('dt_assinatura', 'datetime', [
            'default' => null,
            'null' => true,
        ])->addColumn('texto_html', 'text', [
            'default' => null,
            'null' => true,
        ])->addColumn('imagens', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])->addColumn('filme', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])->addColumn('papel', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ])->addColumn('rtf_html', 'text', [
            'default' => null,
            'null' => true,
        ])->addColumn('paginas', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11
        ]);
        $table->update();
    }
}
