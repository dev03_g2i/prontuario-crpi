<?php
use Migrations\AbstractMigration;

class CreateEstqSaidaNovo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("
        DROP TABLE IF EXISTS `estq_saida`;
            CREATE TABLE `estq_saida` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `data` date NOT NULL,
            `artigo_id` int(11) DEFAULT NULL,
            `quantidade` int(11) DEFAULT NULL,
            `vl_custos` decimal(11,2) DEFAULT '0.00',
            `vl_venda` decimal(11,2) DEFAULT '0.00',
            `vl_custo_medico` decimal(11,2) DEFAULT '0.00',
            `origin` int(11) DEFAULT NULL,
            `codigo_tiss` varchar(10) DEFAULT NULL,
            `codigo_tuss` varchar(10) DEFAULT NULL,
            `codigo_convenio` varchar(10) DEFAULT NULL,
            `tipo_movimento_id` int(11) NOT NULL,
            `atendimento_procedimento_id` int(11) DEFAULT NULL,
            `user_id` int(11) NOT NULL,
            `situacao_id` int(11) NOT NULL,
            `created` datetime NOT NULL,
            `modified` datetime NOT NULL,
            `atendimento_id` int(11) DEFAULT NULL,
            `total` decimal(11,2) DEFAULT '0.00',
            `qtd_recebida` int(11) DEFAULT NULL,
            `vl_recebido` decimal(11,2) DEFAULT '0.00',
            `dt_recebimento` date DEFAULT NULL,
            `exportado_fatura` int DEFAULT 0 COMMENT '1 - exportado | 0 - não',
            PRIMARY KEY (`id`),
            KEY `atendimento_procedimento_id` (`atendimento_procedimento_id`),
            KEY `user_id` (`user_id`),
            KEY `situacao_id` (`situacao_id`),
            KEY `atendimento_id` (`atendimento_id`),
            FOREIGN KEY (`atendimento_procedimento_id`) REFERENCES `atendimento_procedimentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY (`situacao_id`) REFERENCES `situacao_cadastros` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY (`atendimento_id`) REFERENCES `atendimentos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
            FOREIGN KEY (`tipo_movimento_id`) REFERENCES `estq_tipo_movimento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
            ) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
        ");
    }
    public function down()
    {
        $this->execute("DROP TABLE IF EXISTS `estq_saida`;");
    }
}
