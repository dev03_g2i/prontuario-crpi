<?php
use Migrations\AbstractMigration;

class NewTipoHistoriaModelos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('tipo_historia_modelos', [
            'id' => 3,
            'nome' => 'Mastologia',
            'modelo' => 'Mastologia Formulario',
            'tipo_historia_id' => 1,
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'tipo_conteudo' => 2,
            'texto_procedimento' => 'Mastologia',
            'controller' => 'Mastologias',
            'action' => 'nadd'
        ]);
    }
}
