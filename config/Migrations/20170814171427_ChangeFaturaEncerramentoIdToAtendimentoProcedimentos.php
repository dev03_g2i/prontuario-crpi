<?php
use Migrations\AbstractMigration;

class ChangeFaturaEncerramentoIdToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $table = $this->table('atendimento_procedimentos');
        $table->addForeignKey(
            'fatura_encerramento_id',
            'fatura_encerramentos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->update();
    }
}
