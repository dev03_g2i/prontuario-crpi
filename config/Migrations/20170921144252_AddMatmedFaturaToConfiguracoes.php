<?php
use Migrations\AbstractMigration;

class AddMatmedFaturaToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('matmed_fatura', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => 'Lançar MatMed  - 0=Nao/1=Sim'
        ]);
        $table->update();
    }
}
