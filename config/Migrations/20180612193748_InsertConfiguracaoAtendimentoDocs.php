<?php
use Migrations\AbstractMigration;

class InsertConfiguracaoAtendimentoDocs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('configuracao_atendimento_docs', [
            'id' => 1,
            'habilitar' => 1,
        ]);
    }

    public function down()
    {
        $this->execute('DELETE FROM configuracao_atendimento_docs WHERE id = 1');
    }
}
