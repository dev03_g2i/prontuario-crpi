<?php
use Migrations\AbstractMigration;

class CreateFaturaKitartigo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_kitartigo');
        $table->addColumn('fatura_kit_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
        ])
            ->addColumn('artigo_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('quantidade', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('fatura_kitartigo');
        $table->addForeignKey(
            'artigo_id',
            'estq_artigos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )
            ->addForeignKey(
                'fatura_kit_id',
                'fatura_kit',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
