<?php
use Migrations\AbstractMigration;

class InsertStatusTipoEspera extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('status_tipo_espera', [
            [
                'id' => 1,
                'descricao' => 'Aberto',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'descricao' => 'Concluido',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'descricao' => 'Cancelado',
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ]);
    }
    public function down()
    {
        $this->execute('DELETE FROM status_tipo_espera WHERE id IN(1,2,3)');
    }
}
