<?php
use Migrations\AbstractMigration;

class AddUsaValorOriginalToConvenios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('convenios');
        $table->addColumn('usa_valor_original', 'boolean', [
            'default' => 0,
            'null' => false,
        ]);
        $table->update();
    }
}
