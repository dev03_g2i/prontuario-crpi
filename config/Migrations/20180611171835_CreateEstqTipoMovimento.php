<?php
use Migrations\AbstractMigration;

class CreateEstqTipoMovimento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('estq_tipo_movimento');
        $table->addColumn('descricao', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255
        ])->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        $table = $this->table('estq_tipo_movimento');
        $table->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
