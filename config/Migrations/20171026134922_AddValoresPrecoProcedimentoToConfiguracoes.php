<?php
use Migrations\AbstractMigration;

class AddValoresPrecoProcedimentoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('usa_filme', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => 'PrecoProcedimento - 0=Nao/1=Sim'
        ])->addColumn('usa_porte', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => 'PrecoProcedimento - 0=Nao/1=Sim'
        ])->addColumn('usa_uco', 'integer', [
                'default' => 0,
                'limit' => 11,
                'null' => true,
                'comment' => 'PrecoProcedimento - 0=Nao/1=Sim'
            ])
        ;
        $table->update();
    }
}
