<?php
use Migrations\AbstractMigration;

class ChangeDuplicaCpfToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->changeColumn('duplica_cpf', 'string', [
            'limit' => 14,
            'null' => true,
            'default' => null
        ]);
        $table->update();
    }
}
