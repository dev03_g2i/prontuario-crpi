<?php
use Migrations\AbstractMigration;

class NewRegrasToConvenioRegracalculo extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('convenio_regracalculo', [
            [
                'id' => 1,
                'apelido' => 'Normal',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'apelido' => 'Quantidade',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'apelido' => 'Percentual',
                'situacao_id' => 1,
                'user_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        ]);
    }

    public function down()
    {
        $this->execute('DELETE FROM convenio_regracalculo WHERE id IN (1,2)');
    }
}
