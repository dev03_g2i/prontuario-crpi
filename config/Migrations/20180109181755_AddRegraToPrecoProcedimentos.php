<?php
use Migrations\AbstractMigration;

class AddRegraToPrecoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('preco_procedimentos');
        $table->addColumn('convenio_regracalculo_id', 'integer', [
            'default' => 1, // Normal
            'null' => false,
            'limit' => 11
        ]);
        $table->addForeignKey(
            'convenio_regracalculo_id',
            'convenio_regracalculo',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
