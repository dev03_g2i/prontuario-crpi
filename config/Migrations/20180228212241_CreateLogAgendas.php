<?php
use Migrations\AbstractMigration;

class CreateLogAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('log_agendas');
        $table->addColumn('inicio', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('agenda_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('cliente_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('tipo_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('situacao_agenda_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('obs', 'string', [
            'default' => null,
            'null' => true,
            'limit' => 255
        ])->addColumn('procedimentos', 'text', [
            'default' => null,
            'null' => true,
        ])->addColumn('situacao_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
