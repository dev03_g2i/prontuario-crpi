<?php
use Migrations\AbstractMigration;

class AddUtilizaLaudoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('utiliza_laudos', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '1 - Sim | 0 - Não'
        ]);
        $table->update();
    }
}
