<?php
use Migrations\AbstractMigration;

class AddVlCaixaOriginalToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('vl_caixa_original', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->update();
    }
}
