<?php
use Migrations\AbstractMigration;

class InsertApurarPara extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('configuracao_apuracao', [
            [
                'id' => 1,
                'nome' => 'CPF Profissional',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'nome' => 'Estabelecimento',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    public function down()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS=0;");
        $this->execute("TRUNCATE configuracao_apuracao;");
    }
}
