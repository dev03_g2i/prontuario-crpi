<?php
use Migrations\AbstractMigration;

class ChangeSmsToAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agendas');
        $table
            ->changeColumn('sms', 'string', [
                'limit' => 50,
                'null' => true
            ])
            ->changeColumn('sms_situacao', 'string', [
                'default' => 0,
                'null' => true,
                'limit' => 11
            ])
            ->update();


    }
}
