<?php
use Migrations\AbstractMigration;

class InsertControleSistemaToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('controle_financeiro', [
            'id' => -1,
            'descricao' => 'Sistema',
            'usa_atendproc' => 0,
            'usa_receita' => 0,
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
        ]);

    }
    public function down()
    {
        $this->execute('DELETE FROM controle_financeiro WHERE id = -1');
    }
}
