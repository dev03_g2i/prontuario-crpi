<?php
use Migrations\AbstractMigration;

class AddProntuarioPermissaoVisualizacaoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('prontuario_permissao_visualizacao', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => true,
            'comment' => '1 - todos os profissionais podem ver de qualquer um | 2 - Apenas os dele'
        ]);
        $table->update();
    }
}
