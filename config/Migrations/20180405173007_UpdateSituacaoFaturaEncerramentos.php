<?php
use Migrations\AbstractMigration;

class UpdateSituacaoFaturaEncerramentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("UPDATE situacao_faturaencerramentos SET nome='Faturado - Aberto' WHERE id=1");
    }

    public function down()
    {
        $this->execute("UPDATE situacao_faturaencerramentos SET nome='Faturado' WHERE id=1");
    }
}
