<?php
use Migrations\AbstractMigration;

class AddGrupoProcedimentoIdToFatraRecalcular extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_recalcular');
        $table->addColumn('grupo_procedimento_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
