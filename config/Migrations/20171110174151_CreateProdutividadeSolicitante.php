<?php
use Migrations\AbstractMigration;

class CreateProdutividadeSolicitante extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('produtividade_solicitantes');
        $table->addColumn('convenio_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('solicitante_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('percentual_desconto', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->addColumn('percentual_repasse', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->create();

    }
}
