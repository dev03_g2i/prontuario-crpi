<?php
use Migrations\AbstractMigration;

class InsertTipoEquipes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('tipo_equipes', [
            [
                'nome' => 'Não aplicavel',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'nome' => 'Cirurgião',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'nome' => 'Primeiro Auxiliar',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'nome' => 'Segundo Auxiliar',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'nome' => 'Anestesista',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'nome' => 'Instrumentador',
                'user_insert' => 1,
                'situacao_id' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ],
        ]);
    }

    public function down()
    {
        $this->execute("SET FOREIGN_KEY_CHECKS=0;");
        $this->execute("TRUNCATE tipo_equipes;");
    }
}
