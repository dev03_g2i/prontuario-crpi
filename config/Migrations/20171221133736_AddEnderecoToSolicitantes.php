<?php
use Migrations\AbstractMigration;

class AddEnderecoToSolicitantes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('solicitantes');
        $table->addColumn('cep', 'string', [
            'default' => null,
            'limit' => 9,
            'null' => true
        ])->addColumn('endereco', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->addColumn('numero', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true
        ])->addColumn('bairro', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->addColumn('estado', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->addColumn('cidade', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->addColumn('complemento', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->addColumn('fone1', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ])->addColumn('fone2', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true
        ]);
        $table->update();
    }
}
