<?php
use Migrations\AbstractMigration;

class AddUltimaDataGeradaToGrupoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('grupo_agenda_horarios');
        $table->addColumn('ultima_dt_gerada', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
