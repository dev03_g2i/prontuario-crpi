<?php
use Migrations\AbstractMigration;

class CreateAtendimentoDocumentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_documentos');
        $table->addColumn('valor_total', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ])->addColumn('meses', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11,
        ])->addColumn('valor_parcela', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2
        ])->addColumn('vencimento', 'integer', [
            'default' => null,
            'null' => true,
            'limit' => 11,
        ])->addColumn('primeiro_pagamento', 'date', [
            'default' => null,
            'null' => true
        ])->addColumn('data_contrato', 'date', [
            'default' => null,
            'null' => true
        ])->addColumn('texto_contrato', 'text', [
            'default' => null,
            'null' => true
        ])
        ->addColumn('atendimento_modelo_documento_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('atendimento_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('responsavel_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])->addColumn('situacao_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();

        $table = $this->table('atendimento_documentos');
        $table->addForeignKey(
            'atendimento_modelo_documento_id',
            'atendimento_modelo_documentos',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
            )->addForeignKey(
                'atendimento_id',
                'atendimentos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
