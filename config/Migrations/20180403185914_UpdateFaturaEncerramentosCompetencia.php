<?php
use Migrations\AbstractMigration;

class UpdateFaturaEncerramentosCompetencia extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("ALTER TABLE fatura_encerramentos MODIFY competencia VARCHAR(7)");
    }

    public function down()
    {
        $this->execute("ALTER TABLE fatura_encerramentos MODIFY competencia INT(4)");
    }
}
