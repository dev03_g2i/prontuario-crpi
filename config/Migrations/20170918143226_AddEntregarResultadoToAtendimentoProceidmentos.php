<?php
use Migrations\AbstractMigration;

class AddEntregarResultadoToAtendimentoProceidmentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('retirado_por', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
            'comment' => 'Laudos / Entregar Resultado'
        ])->addColumn('entregue_por', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
            'comment' => 'Laudos / Entregar Resultado'
        ])->addColumn('dt_entrega_resultado', 'datetime', [
            'default' => null,
            'null' => true,
            'comment' => 'Laudos / Entregar Resultado'
        ])
        ;
        $table->update();
    }
}
