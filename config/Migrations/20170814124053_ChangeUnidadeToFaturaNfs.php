<?php
use Migrations\AbstractMigration;

class ChangeUnidadeToFaturaNfs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $table = $this->table('fatura_nfs');
        $table
            ->changeColumn('unidade_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false
            ])
            ->update();
    }
}
