<?php
use Migrations\AbstractMigration;

class ChangeControllerActionToTipoHistoriaModelos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_historia_modelos');
        $table->changeColumn('controller', 'string', [
            'null' => true
        ])
            ->changeColumn('action', 'string', [
                'null' => true
            ])
        ->update();
    }
}
