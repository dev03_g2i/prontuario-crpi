<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoCadpaciente extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_cadpaciente');
        $table->addColumn('religiao', 'integer', [
            'limit' => 11,
            'null' => true,
            'comment' => '1=nao mostra na tela de cadastro e nao obrigatorio, 2, mostra e nao obrig, 3-mostra e obrigatório - Padrão  = 1'
        ]);
        $table->addColumn('indicao', 'integer', [
            'limit' => 11,
            'null' => true,
            'comment' => '1=nao mostra e nao obrigatório, 2, mostra e nao obrig, 3-mostra e obrigatorio  - Padrão = 3'
        ]);
        $table->create();
    }
}
