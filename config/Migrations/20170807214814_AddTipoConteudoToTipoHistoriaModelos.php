<?php
use Migrations\AbstractMigration;

class AddTipoConteudoToTipoHistoriaModelos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_historia_modelos');
        $table
            ->addColumn('tipo_conteudo', 'integer', [
                'default' => 1,
                'limit' => 11,
                'null' => true,
                'comment' => '1 - Tipo texto / 2 - Tipo formulario',
            ])
            ->addColumn('texto_formulario', 'text', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('texto_procedimento', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true
            ])
            ->addColumn('controller', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => null
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => null
            ])
            ->update();
    }
}
