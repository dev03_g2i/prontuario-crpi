<?php
use Migrations\AbstractMigration;

class NewSituacaoAgendaId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $data = [
            'id' => 1000,
            'nome' => 'Bloqueio',
            'situacao_id' => 1,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'cor' => '#FF0000',
            'cor_atendimento' => '#FF0000',
            'ocultar' => 0
        ];
        $this->insert('situacao_agendas', $data);
    }
}
