<?php
use Migrations\AbstractMigration;

class CreateConvenioIndicetabelas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('convenio_indicetabelas');
        $table->addColumn('convenio_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11
        ])
            ->addColumn('grupo_procedimento_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('honorario_porte', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('operacional_uco', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('filme', 'decimal', [
                'default' => '0.00',
                'null' => true,
                'precision' => 11,
                'scale' => 2,
            ])
            ->addColumn('user_insert', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('user_insert_dt', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('user_update', 'integer', [
                'default' => null,
                'null' => true,
                'limit' => 11
            ])
            ->addColumn('user_update_dt', 'datetime', [
                'default' => null,
                'null' => true
            ])
            ->addColumn('situacao_id', 'integer', [
                'default' => null,
                'null' => false,
                'limit' => 11
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => false
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => false
            ]);
        $table->create();

        $table = $this->table('convenio_indicetabelas');
        $table->addForeignKey(
            'convenio_id',
            'convenios',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
            )
            ->addForeignKey(
                'grupo_procedimento_id',
                'grupo_procedimentos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_insert',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->update();
    }
}
