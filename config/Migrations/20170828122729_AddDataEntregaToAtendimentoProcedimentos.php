<?php
use Migrations\AbstractMigration;

class AddDataEntregaToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('dt_entrega', 'datetime', [
            'default' => null,
            'null' => true,
            'comment' => 'Laudos'
        ]);
        $table->update();
    }
}
