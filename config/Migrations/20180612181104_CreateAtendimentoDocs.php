<?php
use Migrations\AbstractMigration;

class CreateAtendimentoDocs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_atendimento_docs');
        $table->addColumn('habilitar', 'integer', [
            'default' => 0,//não
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ]);
        $table->create();
    }
}
