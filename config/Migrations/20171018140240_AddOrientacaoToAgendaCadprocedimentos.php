<?php
use Migrations\AbstractMigration;

class AddOrientacaoToAgendaCadprocedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agenda_cadprocedimentos');
        $table->addColumn('orientacao', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
