<?php
use Migrations\AbstractMigration;

class ChatModuleUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("ALTER TABLE users ADD COLUMN chat_module int(1) DEFAULT 0 COMMENT '0 - Não | 1 - Sim'");
    }

    public function down()
    {
        $this->execute("ALTER TABLE users DROP COLUMN chat_module");
    }
}
