<?php
use Migrations\AbstractMigration;

class CreateNfs extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute("
        DROP TABLE IF EXISTS `nf_servicos`;
        create table `nf_servicos` (
          `id` int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `nota_fiscal_id` int (11),
          `tributavel` tinyint (1),
          `descricao` varchar (1500),
          `quantidade` int (11),
          `valor_unitario` Decimal (13),
          `valor_total` Decimal (13)
        ) DEFAULT CHARSET=utf8;
        
        SET sql_mode='NO_AUTO_VALUE_ON_ZERO';
        DROP TABLE IF EXISTS `nf_tributacoes`;
        CREATE TABLE `nf_tributacoes` (
          `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `descricao` varchar(200) DEFAULT NULL
        ) DEFAULT CHARSET=utf8;
        
        insert into `nf_tributacoes`(`id`,`descricao`) values (0,'Isenta'),
        (1,'Imune'),
        (2,'DepositoEmJuizo'),
        (3,'NaoIncide'),
        (5,'TributavelSimples'),
        (6,'TributavelFixo'),
        (7,'NaoTributavel'),
        (8,'TributacaoMEI'),
        (84,'Tributavel');
        
        DROP TABLE IF EXISTS `nf_tipo_recolhimentos`;
        CREATE TABLE `nf_tipo_recolhimentos` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        `descricao` varchar(200) DEFAULT NULL
        ) DEFAULT CHARSET=utf8;
        
        insert  into `nf_tipo_recolhimentos`(`id`,`descricao`) values 
        (1,'RetidoNaFonte'),
        (65,'Recolher');
        
        DROP TABLE IF EXISTS `nf_operacoes`;
        CREATE TABLE `nf_operacoes` (
          `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `descricao` varchar(200) DEFAULT NULL
        ) DEFAULT CHARSET=utf8;
        
        insert  into `nf_operacoes`(`id`,`descricao`) values 
        (1,'ComDedução'),
        (2,'Isenta'),
        (3,'Devolução'),
        (4,'Intermediação'),
        (65,'SemDedução');
        
        DROP TABLE IF EXISTS `nf_situacao_notas`;
        CREATE TABLE `nf_situacao_notas` (
          `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `descricao` varchar(200) DEFAULT NULL
        ) DEFAULT CHARSET=utf8;
        
        insert  into `nf_situacao_notas`(`id`,`descricao`) values (-1,'Digitação');
        insert  into `nf_situacao_notas`(`id`,`descricao`) values (0,'Em Espera');
        insert  into `nf_situacao_notas`(`id`,`descricao`) values
        (1,'Em Processamento'),
        (2,'Com Erros'),
        (3,'Corrigida'),
        (4,'Autorizada'),
        (5,'Impressa'),
        (6,'Enviado Email'),
        (7,'Erro Ao Enviar Email'),
        (9,'Cancelada');
        
        DROP TABLE IF EXISTS `nf_prestadores`;
        CREATE TABLE `nf_prestadores` (
          `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `razao_social` varchar(200) DEFAULT NULL,
          `inscricao_municipal` varchar(200) DEFAULT NULL,
          `cpfcnpj` varchar(18) DEFAULT NULL,
          `ddd` varchar(2) DEFAULT NULL,
          `telefone` varchar(9) DEFAULT NULL,
          `email` varchar(50) DEFAULT NULL,
          `cpfcnpj_intermediario` varchar(18) DEFAULT NULL,
          `tipo_logradouro_id` int(11) DEFAULT NULL,
          `endereco` varchar(200) DEFAULT NULL,
          `numero` int(11) DEFAULT NULL,
          `complemento` varchar(200) DEFAULT NULL,
          `tipo_bairro_id` int(11) DEFAULT NULL,
          `bairro` varchar(200) DEFAULT NULL,
          `cep` varchar(9) DEFAULT NULL,
          `cidade_id` int(11) DEFAULT NULL,
          `atual` tinyint(1) DEFAULT 0,
          `cnae` varchar(200) DEFAULT NULL,
          `descricao_cnae` text DEFAULT NULL,
          `tributacao_id` int(11) DEFAULT 84,
          `aliquota_atividade` decimal(11,4) DEFAULT 5.0000
        ) DEFAULT CHARSET=utf8;
        
        insert  into `nf_prestadores`(`id`,`razao_social`,`inscricao_municipal`,`cpfcnpj`,`ddd`,`telefone`,`email`,`cpfcnpj_intermediario`,`tipo_logradouro_id`,`endereco`,`numero`,`complemento`,`tipo_bairro_id`,`bairro`,`cep`,`cidade_id`,`atual`,`cnae`,`descricao_cnae`,`tributacao_id`,`aliquota_atividade`) values 
        (1,'CAIRO CENTRO AVANCADO DE IMPLANTE E REABILITACAO ORAL S/S LTDA','00096906005','031655710000174','67','33224900','suporte@grupog2i.com.br','0',0,'Rua Jeriba',265,'-',0,'Chacara Cachoiera','79040120',9051,0,NULL,NULL,84,'5.0000');
        
        DROP TABLE IF EXISTS `nf_tomadores`;
        CREATE TABLE `nf_tomadores` (
          `id` int(11)  NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `razao_social` varchar(200) DEFAULT NULL,
          `inscricao_municipal` varchar(200) DEFAULT NULL,
          `cpfcnpj` varchar(18) DEFAULT NULL,
          `doc_tomador_estrangeiro` varchar(200) DEFAULT NULL,
          `tipo_logradouro_id` int(11) DEFAULT NULL,
          `logradouro` varchar(200) DEFAULT NULL,
          `numero` int(11) DEFAULT NULL,
          `complemento` varchar(200) DEFAULT NULL,
          `tipo_bairro_id` int(11) DEFAULT NULL,
          `bairro` varchar(200) DEFAULT NULL,
          `cidade_id` int(11) DEFAULT NULL,
          `cep` varchar(12) DEFAULT NULL,
          `email` varchar(50) DEFAULT NULL,
          `ddd` varchar(2) DEFAULT NULL,
          `telefone` varchar(20) DEFAULT NULL,
          `texto_nf` text DEFAULT NULL,
          `cliente_id` int(11) DEFAULT NULL,
          `responsavel_id` int(11) DEFAULT NULL,
          `tipo_tomador` int(11) DEFAULT NULL COMMENT '1 = Paciente / 2 = Responsavel / 3 - Outros'
        ) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
        
        DROP TABLE IF EXISTS `nfs`;
        CREATE TABLE `nfs` (
          `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `prestador_id` int(11) DEFAULT 1,
          `tomador_id` int(11) DEFAULT NULL,
          `ficha` int(11) DEFAULT NULL,
          `situacao_nota_id` int(11) DEFAULT 0,
          `situacao_rps` int(11) DEFAULT 78,
          `descricao_rps` varchar(500) DEFAULT NULL,
          `assinatura` varchar(500) DEFAULT NULL,
          `autorizacao` varchar(500) DEFAULT NULL,
          `autorizacao_cancelamento` varchar(500) DEFAULT NULL,
          `numero_lote` int(11) DEFAULT 0,
          `numero_rps` int(11) DEFAULT 0,
          `numero_nfse` int(11) DEFAULT 0,
          `data_emissao_rps` datetime DEFAULT current_timestamp(),
          `data_processamento` datetime DEFAULT NULL,
          `data_cancelamento` datetime DEFAULT NULL,
          `mot_cancelamento` text DEFAULT NULL,
          `serie_rps` varchar(200) DEFAULT 'NF',
          `serie_rps_substituido` varchar(200) DEFAULT NULL,
          `numero_rps_substituido` bigint(20) DEFAULT NULL,
          `numero_nfse_substituido` bigint(20) DEFAULT NULL,
          `data_emissao_nfse_substituida` datetime DEFAULT NULL,
          `serie_prestacao` varchar(20) DEFAULT '99',
          `cnae` varchar(200) DEFAULT NULL,
          `descricao_cnae` text DEFAULT NULL,
          `tipo_recolhimento_id` int(11) DEFAULT 65,
          `cidade_id` int(11) DEFAULT null,
          `operacao_id` int(11) DEFAULT 65,
          `tributacao_id` int(11) DEFAULT 84,
          `aliquota_atividade` decimal(11,4) DEFAULT 5.0000,
          `aliquota_pis` decimal(11,4) DEFAULT 0.0000,
          `aliquota_cofins` decimal(11,4) DEFAULT 0.0000,
          `aliquota_inss` decimal(11,4) DEFAULT 0.0000,
          `aliquota_ir` decimal(11,4) DEFAULT 0.0000,
          `aliquota_csll` decimal(11,4) DEFAULT 0.0000,
          `valor_pis` decimal(11,2) DEFAULT 0.00,
          `valor_cofins` decimal(11,2) DEFAULT 0.00,
          `valor_inss` decimal(11,2) DEFAULT 0.00,
          `valor_ir` decimal(11,2) DEFAULT 0.00,
          `valor_csll` decimal(11,2) DEFAULT 0.00,
          `valor_total_servicos` decimal(11,2) DEFAULT 0.00,
          `valor_total_deducoes` decimal(11,2) DEFAULT 0.00,
          `enviar_email` tinyint(1) DEFAULT 0,
          `nfse_arquivo` varchar(200) DEFAULT NULL,
          KEY `situacao_nota_id` (`situacao_nota_id`),
          KEY `prestador_id` (`prestador_id`),
          KEY `tomador_id` (`tomador_id`),
          KEY `tipo_recolhimento_id` (`tipo_recolhimento_id`),
          KEY `operacao_id` (`operacao_id`),
          KEY `tributacao_id` (`tributacao_id`),
          CONSTRAINT `nota_fiscais_ibfk_1` FOREIGN KEY (`situacao_nota_id`) REFERENCES `nf_situacao_notas` (`id`),
          CONSTRAINT `nota_fiscais_ibfk_2` FOREIGN KEY (`prestador_id`) REFERENCES `nf_prestadores` (`id`),
          CONSTRAINT `nota_fiscais_ibfk_3` FOREIGN KEY (`tomador_id`) REFERENCES `nf_tomadores` (`id`),
          CONSTRAINT `nota_fiscais_ibfk_5` FOREIGN KEY (`tipo_recolhimento_id`) REFERENCES `nf_tipo_recolhimentos` (`id`),
          CONSTRAINT `nota_fiscais_ibfk_6` FOREIGN KEY (`operacao_id`) REFERENCES `nf_operacoes` (`id`),
          CONSTRAINT `nota_fiscais_ibfk_7` FOREIGN KEY (`tributacao_id`) REFERENCES `nf_tributacoes` (`id`)
        ) DEFAULT CHARSET=utf8;
            ");
    }

    public function down() 
    {
         $this->execute("
            DROP TABLE IF EXISTS `nfs`;
            DROP TABLE IF EXISTS `nf_prestadores`;
            DROP TABLE IF EXISTS `nf_tomadores`;
            DROP TABLE IF EXISTS `nf_operacoes`;
            DROP TABLE IF EXISTS `nf_situacao_notas`;
            DROP TABLE IF EXISTS `nf_tipo_recolhimentos`;
            DROP TABLE IF EXISTS `nf_tributacoes`;
            DROP TABLE IF EXISTS `nf_servicos`;
         ");
    }
}
