<?php
use Migrations\AbstractMigration;

class AddGrupoIdToGrupoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('grupo_agenda_horarios');
        $table->addForeignKey(
            'grupo_id',
            'grupo_agendas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->update();
    }
}
