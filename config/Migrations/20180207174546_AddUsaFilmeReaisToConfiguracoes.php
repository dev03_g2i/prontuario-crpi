<?php
use Migrations\AbstractMigration;

class AddUsaFilmeReaisToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('usa_filme_reais', 'integer', [
            'default' => 1,// sim
            'limit' => 11,
            'null' => true,
            'comment' => '0 - Não | 1 - Sim'
        ]);
        $table->update();
    }
}
