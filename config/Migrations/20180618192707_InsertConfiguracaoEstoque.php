<?php
use Migrations\AbstractMigration;

class InsertConfiguracaoEstoque extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('configuracao_estoque', [
            'id' => 1,
            'habilitado' => 0,
        ]);
    }
    public function down()
    {
        $this->execute("DELETE FROM configuracao_estoque WHERE id = 1");
    }
}
