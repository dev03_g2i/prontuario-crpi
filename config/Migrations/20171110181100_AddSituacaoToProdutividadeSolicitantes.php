<?php
use Migrations\AbstractMigration;

class AddSituacaoToProdutividadeSolicitantes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('produtividade_solicitantes');
        $table->addColumn('situacao_id','integer',[
            'default' =>1,
            'limit' => 11,
            'null' => false
        ]);
        $table->addForeignKey('situacao_id','situacao_cadastros','id',[
            'delete'=> 'NO_ACTION',
            'update'=> 'NO_ACTION'
        ]);
        $table->update();
    }
}
