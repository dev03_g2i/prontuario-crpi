<?php
use Migrations\AbstractMigration;

class AddFilmeReaisToAtendimentoProcedimentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_procedimentos');
        $table->addColumn('filme_reais', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2,
            'comment' => 'filme_reais - preco_procedimentos'
        ])->addColumn('uco', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2,
            'comment' => 'uco - preco_procedimentos'
        ])->addColumn('porte', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2,
            'comment' => 'porte - preco_procedimentos'
        ])->addColumn('filme', 'decimal', [
            'default' => '0.00',
            'null' => true,
            'precision' => 11,
            'scale' => 2,
            'comment' => 'filme - preco_procedimentos'
        ]);
        $table->update();
    }
}
