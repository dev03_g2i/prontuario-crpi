<?php
use Migrations\AbstractMigration;

class CreateOperacaoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('operacao_agenda_horarios');
        $table->addColumn('nome', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false
        ])
            ->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'limit' => 11,
            'null' => false
        ])->addColumn('user_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ])->addForeignKey(
            'situacao_id',
            'situacao_cadastros',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        )->addForeignKey(
            'user_id',
            'users',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->create();
    }
}
