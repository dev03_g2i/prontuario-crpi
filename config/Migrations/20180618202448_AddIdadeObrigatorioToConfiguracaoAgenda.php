<?php
use Migrations\AbstractMigration;

class AddIdadeObrigatorioToConfiguracaoAgenda extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_agenda');
        $table->addColumn('idade_obrigatorio', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '1 - sim | 0 - não'
        ]);
        $table->update();
    }
}
