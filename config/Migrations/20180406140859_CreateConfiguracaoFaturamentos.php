<?php
use Migrations\AbstractMigration;

class CreateConfiguracaoFaturamentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_faturamentos');
        $table->addColumn('tela_fatura_equipe', 'integer', [
            'limit' => 11,
            'null' => true,
            'default' => 0,
            'comment' => '0 - Não mostra | 1 - mostra',
        ]);
        $table->create();
    }
}
