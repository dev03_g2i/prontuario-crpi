<?php
use Migrations\AbstractMigration;

class AddFoneProvisorio2ToAgendas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('agendas');
        $table->addColumn('fone_provisorio2', 'string', [
            'default' => null,
            'limit' => 14,
            'null' => true,
            'after' => 'fone_provisorio'
        ]);
        $table->update();
    }
}
