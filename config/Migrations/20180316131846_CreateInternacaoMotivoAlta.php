<?php

use Migrations\AbstractMigration;

class CreateInternacaoMotivoAlta extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('internacao_motivo_alta');
        $table->addColumn('descricao', 'string', [
            'limit' => 255,
            'null' => true
        ]);
        $table->addColumn('codigotiss', 'string', [
            'limit' => 10,
            'null' => true
        ]);
        $table->addColumn('situacao_id', 'integer', [
            'default' => 1,
            'null' => true
        ]);
        $table->addColumn('user_reg', 'integer', [
            'limit' => 20,
            'null' => true
        ]);
        $table->addColumn('user_update', 'integer', [
            'limit' => 20,
            'null' => true
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => true
        ]);

        $table
            ->addForeignKey(
                'situacao_id',
                'situacao_cadastros',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'user_reg',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )->addForeignKey(
                'user_update',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            );
        $table->create();

        $this->insert('internacao_motivo_alta',
            [
                'id' => 0,
                'descricao' => 'Valor Padrão',
                'codigotiss' => 'Geral',
                'situacao_id' => 2,
                'user_reg' => 1,
                'user_update' => 1,
                'created' => '2018-03-16 00:00:00',
                'modified' => '2018-03-16 00:00:00'
            ]);
    }
}
