<?php
use Migrations\AbstractMigration;

class AlterFaturaEncerramentoAndConvenioTipoImposto extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->execute('ALTER TABLE convenio_tipoimposto ALTER valor_inicial SET DEFAULT 0;');
        $this->execute('ALTER TABLE convenio_tipoimposto ALTER valor_final SET DEFAULT 0;');
        $this->execute('ALTER TABLE fatura_encerramentos ALTER vl_received_bank SET DEFAULT 0;');
        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN convenio_id INT NULL;');
    }

    public function down()
    {
        $this->execute('ALTER TABLE convenio_tipoimposto ALTER valor_inicial SET DEFAULT 0;');
        $this->execute('ALTER TABLE convenio_tipoimposto ALTER valor_final SET DEFAULT 0;');
        $this->execute('ALTER TABLE fatura_encerramentos ALTER vl_received_bank SET DEFAULT 0;');
        $this->execute('ALTER TABLE fatura_encerramentos MODIFY COLUMN convenio_id INT NOT NULL;');
    }
}
