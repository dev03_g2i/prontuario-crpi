<?php
use Migrations\AbstractMigration;

class InsertGrupoAgendaHorarioToGrupoAgendaHorarios extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('grupo_agenda_horarios', [
            'id' => -1,
            'dia_semana' => 1,
            'data_inicio' => '0000-00-00',
            'data_fim' => '0000-00-00',
            'hora_inicio' => '00:00:00',
            'hora_fim' => '00:00:00',
            'intervalo' => '00:00:00',
            'grupo_id' => '-1',
            'tipo_agenda_id' => 1,
            'situacao_id' => 2,
            'user_id' => 1,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'operacao_agenda_horario_id' => 1,
        ]);
    }
}
