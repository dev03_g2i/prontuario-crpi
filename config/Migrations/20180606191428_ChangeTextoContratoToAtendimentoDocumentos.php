<?php
use Migrations\AbstractMigration;

class ChangeTextoContratoToAtendimentoDocumentos extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('atendimento_documentos');
        $table->removeColumn('data_contrato')->removeColumn('texto_contrato')->save();
        
        $table->addColumn('data_emissao', 'date', [
            'default' => null,
            'null' => true,
        ])->addColumn('texto_documento', 'text', [
            'default' => null,
            'null' => true,
        ])->update();
    }
}
