<?php
use Migrations\AbstractMigration;

class AddUsaDescontoToConfiguracoes extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracoes');
        $table->addColumn('edit_desconto', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '1 - sim | 0 - não',

        ]);
        $table->update();
    }
}
