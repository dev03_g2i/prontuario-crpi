<?php
use Migrations\AbstractMigration;

class AlterCriadoPorToFaturaKit extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('fatura_kit');
        $table->removeColumn('criado_por');
        $table->removeColumn('alterado_por');
        $table->addColumn('user_update', 'integer', [
            'limit' => 11,
            'null' => true,
            'default' => null
        ]);
        $table->update();
    }
}
