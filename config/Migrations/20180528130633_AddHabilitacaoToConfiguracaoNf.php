<?php
use Migrations\AbstractMigration;

class AddHabilitacaoToConfiguracaoNf extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('configuracao_nf');
        $table->addColumn('habilitacao', 'integer', [
            'default' => 0,
            'limit' => 11,
            'null' => true,
            'comment' => '0 - não habilitado | 1 - habilitado',
        ]);
        $table->update();
    }
}
