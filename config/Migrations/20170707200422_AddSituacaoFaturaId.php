<?php
use Migrations\AbstractMigration;

class AddSituacaoFaturaId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('fatura_encerramentos')
            ->addColumn('situacao_faturaencerramento_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true
            ])
            ->addIndex(
                [
                    'situacao_faturaencerramento_id',
                ]
            )
            ->update();

        $this->table('fatura_encerramentos')
            ->addForeignKey(
                'situacao_faturaencerramento_id',
                'situacao_faturaencerramentos',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }
}
