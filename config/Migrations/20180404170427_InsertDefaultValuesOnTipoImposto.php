<?php
use Migrations\AbstractMigration;

class InsertDefaultValuesOnTipoImposto extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $this->insert('convenio_tipoimposto', [
            'id' => 1,
            'descricao' => 'ISS',
            'porcentual' => 5.00,
            'valor_inicial' => 0,
            'valor_final' => 999999999.99,
            'retido' => 0,
            'observacao' => null,
            'user_created' => 1,
            'user_updated' => null,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ]);
        $this->insert('convenio_tipoimposto', [
            'id' => 2,
            'descricao' => 'IR',
            'porcentual' => 1.50,
            'valor_inicial' => 0,
            'valor_final' => 999999999.99,
            'retido' => 0,
            'observacao' => null,
            'user_created' => 1,
            'user_updated' => null,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ]);
        $this->insert('convenio_tipoimposto', [
            'id' => 3,
            'descricao' => 'PIS',
            'porcentual' => 0.65,
            'valor_inicial' => 0,
            'valor_final' => 999999999.99,
            'retido' => 0,
            'observacao' => null,
            'user_created' => 1,
            'user_updated' => null,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ]);
        $this->insert('convenio_tipoimposto', [
            'id' => 4,
            'descricao' => 'Contr. Social',
            'porcentual' => 1.00,
            'valor_inicial' => 0,
            'valor_final' => 999999999.99,
            'retido' => 0,
            'observacao' => null,
            'user_created' => 1,
            'user_updated' => null,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ]);
        $this->insert('convenio_tipoimposto', [
            'id' => 5,
            'descricao' => 'COFINS',
            'porcentual' => 3.00,
            'valor_inicial' => 0,
            'valor_final' => 999999999.99,
            'retido' => 0,
            'observacao' => null,
            'user_created' => 1,
            'user_updated' => null,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ]);
        $this->insert('convenio_tipoimposto', [
            'id' => 6,
            'descricao' => 'CRF',
            'porcentual' => 4.65,
            'valor_inicial' => 0,
            'valor_final' => 999999999.99,
            'retido' => 0,
            'observacao' => null,
            'user_created' => 1,
            'user_updated' => null,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s')
        ]);
    }

    public function down()
    {   
        $this->execute("DELETE FROM convenio_tipoimposto WHERE id in (1,2,3,4,5,6)");
    }
}
