<?php
use Migrations\AbstractMigration;

class UpdateGruposInsertColor extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function up()
    {
        $table = $this->table('grupo_procedimentos');
        $table->addColumn('color', 'string', [
            'limit' => 23,
            'null' => false
        ]);
        $table->update();
    }

    public function down()
    {
        $table = $this->table('grupo_procedimentos');
        $table->removeColumn('color');
        $table->update();
    }
}
