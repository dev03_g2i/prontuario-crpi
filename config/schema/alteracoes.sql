
CREATE TABLE `apurar`(
	`id` int(11) NOT NULL  auto_increment ,
	`nome` varchar(255) COLLATE utf8_general_ci NOT NULL  ,
	`created` datetime NOT NULL  ,
	`modified` datetime NOT NULL  ,
	`user_id` int(11) NOT NULL  ,
	`user_up_id` int(11) NOT NULL  ,
	PRIMARY KEY (`id`) ,
	KEY `user_id`(`user_id`) ,
	KEY `user_up_id`(`user_up_id`) ,
	CONSTRAINT `apurar_ibfk_1`
	FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ,
	CONSTRAINT `apurar_ibfk_2`
	FOREIGN KEY (`user_up_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


ALTER TABLE `atendimento_procedimentos`
	ADD COLUMN `nr_guia` varchar(255)  COLLATE utf8_general_ci NULL after `controle` ,
	ADD COLUMN `dt_emissao_guia` datetime   NULL after `nr_guia` ,
	ADD COLUMN `autorizacao_senha` varchar(255)  COLLATE utf8_general_ci NULL after `dt_emissao_guia` ,
	ADD COLUMN `dt_autorizacao` datetime   NULL after `autorizacao_senha` ,
	ADD COLUMN `percentual_cobranca` double   NULL after `dt_autorizacao` ,
	ADD COLUMN `apurar_id` int(11)   NULL after `percentual_cobranca` ,
	ADD KEY `apurar_id`(`apurar_id`) ;
ALTER TABLE `atendimento_procedimentos`
	ADD CONSTRAINT `atendimento_procedimentos_ibfk_6`
	FOREIGN KEY (`apurar_id`) REFERENCES `apurar` (`id`) ;


ALTER TABLE `configuracoes`
	ADD COLUMN `usa_arquivo` int(11)   NULL COMMENT '1 = sim 2 = nao' after `url_imagens` ,
	ADD COLUMN `arquivo` int(11)   NULL after `usa_arquivo` ;

ALTER TABLE `grupo_agendas`
	ADD COLUMN `inicio` time   NULL after `intervalo` ,
	ADD COLUMN `fim` time   NULL after `inicio` ;
