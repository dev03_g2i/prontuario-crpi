<?php
use Migrations\AbstractMigration;

class InsertConvenioToClienteClone extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('cliente_clone', [
            'id' => -100,
            'nome' => 'Convênios',
            'telefone' => null,
            'celular' => null,
            'cep' => null,
            'endereco' => null,
            'uf' => null,
            'cidade' => null,
            'bairro' => null,
            'numero' => null,
            'complemento' => null,
            'email' => null,
            'rg' => 0,
            'datacadastro' => date('Y-m-d H:i:s'),
            'email_cobranca' => null,
            'observacao' => null,
            'status' => 3,
            'cod_sisAntigo' => null,
        ]);
    }
}
