<?php
use Migrations\AbstractMigration;

class AddUserCreatedToContasReceber extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('contasreceber');
        $table->addColumn('user_created', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])->addColumn('user_modified', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => true,
        ])->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false
        ])->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false
        ]);
        $table->update();
    }
}
