<?php

use Migrations\AbstractMigration;

class InsertFormaPagamentoToTipoPagamento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('planocontas', [
            'id' => -10,
            'classificacao' => 1,
            'status' => 3,
            'nome' => 'Forma Pagamento',
            'rateio' => 1
        ]);
    }
}
