<?php

use Migrations\AbstractMigration;

class IntsertConvenioToTipoPagamento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('tipo_pagamento', [
            'id' => -10,
            'descricao' => 'Convênios',
            'usado' => 'Ambas',
            'taxa' => null,
            'tipo_documento_id' => -10,
            'id_plano_contas_receita' => -10,
            'id_plano_contas_saida' => null,
            'taxa01' => 0.00,
            'taxa02' => 0.00,
            'taxa03' => 0.00,
            'taxa04' => 0.00,
            'taxa05' => 0.00,
            'taxa06' => 0.00,
        ]);
    }
}
