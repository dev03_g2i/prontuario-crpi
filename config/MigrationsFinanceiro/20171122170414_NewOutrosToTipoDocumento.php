<?php
use Migrations\AbstractMigration;

class NewOutrosToTipoDocumento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('tipo_documento', [
           'id' => -1,
            'descricao' => 'Outros',
            'status_id' => 1,
            'usado' => 'Ambas'
        ]);
    }
}
