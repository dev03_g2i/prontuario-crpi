<?php
use Migrations\AbstractMigration;

class AddTipoDocumentoIdToTipoP extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_pagamento');
        $table->addColumn('tipo_documento_id', 'integer', [
            'default' => -1,
            'limit' => 11,
            'null' => false,
        ]);
        $table->update();
    }
}
