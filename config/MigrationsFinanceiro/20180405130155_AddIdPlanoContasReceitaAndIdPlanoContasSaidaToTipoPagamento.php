<?php

use Migrations\AbstractMigration;

class AddIdPlanoContasReceitaAndIdPlanoContasSaidaToTipoPagamento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('tipo_pagamento');
        $table->addColumn('id_plano_contas_receita', 'integer',
            [
                'default' => -10,
                'limit' => 11,
                'null' => true,
            ]);

        $table->addColumn('id_plano_contas_saida', 'integer',
            [
                'limit' => 11,
                'null' => true,
            ]);

        $table->addForeignKey(
            'id_plano_contas_receita',
            'planocontas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );

        $table->addForeignKey(
            'id_plano_contas_saida',
            'planocontas',
            'id',
            [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT'
            ]
        );
        $table->update();
    }
}
