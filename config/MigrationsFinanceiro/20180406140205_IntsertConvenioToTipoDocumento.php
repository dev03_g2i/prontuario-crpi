<?php
use Migrations\AbstractMigration;

class IntsertConvenioToTipoDocumento extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->insert('tipo_documento', [
            'id' => -10,
            'descricao' => 'Convênios',
            'status_id' => 3,
            'usado' => 'Ambas'
        ]);
    }
}
