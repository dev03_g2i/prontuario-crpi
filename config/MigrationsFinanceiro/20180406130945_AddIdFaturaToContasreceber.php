<?php
use Migrations\AbstractMigration;

class AddIdFaturaToContasreceber extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('contasreceber');
        $table->addColumn('fatura_id', 'integer', [
            'limit' => 11,
            'null' => true,
            'comment' => 'Campo para guardar o id da fatura gerado na rotina do prontuário'
        ]);
        $table->update();
    }
}
